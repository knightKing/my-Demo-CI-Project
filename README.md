# HSBC AOFE

AO front-end project for HSBC Singapore.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for futhur development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Java8
Maven
WAS Liberty V17.0.0.1
IDE: Eclipse
```


### coding style

We have divided project into modules. please follow the same structure. 


```
test : This module will have the test
web : This module will have web related classes/configuration 
repository : This module will have all the repository related implementation 
repository-common : This module will have only repository interfaces that needs to be exposed 
common : This module must have domain classes or interfaces exposed to web layer only
service : This module will have code related to the service layer implementation

```
#### Expectations from all

`` do not write comments, only if you hate your colleagues`` :P <br/>

Please follow basic coding standards: <br/>
1) write clean and readable code.<br/>
2) Use functional approach where-ever possible (Java8).
3) Write behaviour specific functions, Nobody like Bulky one.\
4) Developer who is writing code, is responsible for writing unit test also (Must).
5) Swagger documentation is must.


Feel free to add any good thing here, which you want to share with all.
###### Best of luck



### Contributors
* **Anupam Vashisth** 
* **Sahil Verma** 
* **Anoop Singh** 
