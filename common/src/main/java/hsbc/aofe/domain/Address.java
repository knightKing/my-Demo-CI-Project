package hsbc.aofe.domain;

import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class Address implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5532553101494251385L;
	
	private long id;
	
	//@Pattern(regexp = "^[a-zA-Z0-9\\s]+$", message = "pattern")
	private String line1Address;
	
	//@Pattern(regexp = "^[a-zA-Z0-9\\s]+$", message = "pattern")
	private String line2Address;
	
	//@Pattern(regexp = "^[a-zA-Z0-9\\s]+$", message = "pattern")
	private String line3Address;
	
	//TODO: remove
	private String city;
	
	private CountryCode country;
	
	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "pattern")
	private String postalCode;
	
	//@NotNull
	private AddressType addressType;


    // - correspondence, permanent and residential have confusion

}
