package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AddressType {

    PERMANENT(1, "Permanent"),
    RESIDENTIAL(2, "Residential"),
    PREVIOUS(3, "Previous"),
    OFFICE(4, "Office");

    private long id;
    private String value;

}
