package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AoStatus {
	
	ALL("All"),
	APPRV("Approved by Bank"),
	DENED("Denied by Bank"),
	ACCEPT("Accepted by customer"),
	DECLN("Declined by customer"),
	PNDBD("Pending Boarding"),
	BOARD("On-boarding"),
	CMPLT("Completed"),
	CANCL("Cancelled");
	
	String AoStatus;

}
