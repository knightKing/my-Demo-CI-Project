package hsbc.aofe.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import hsbc.aofe.validationgroups.BranchJourneyGroup;
import hsbc.aofe.validationgroups.CustomerJourneyGroup;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@JsonIgnoreProperties
@Data
@ApiModel("APPLICANT DTO")
public class Applicant implements Serializable {

    private static final long serialVersionUID = 6340121918910125805L;

    private Long id;

    //@NotNull(message = "required", groups = { BranchJourneyGroup.class })
    private Title salutation;
    
    private ApplicantType type = ApplicantType.PRIMARY;

    //@NotNull(message = "required", groups = { BranchJourneyGroup.class })
    @Size(max = 100, message = "maxlength")
    @Email
    @Pattern(regexp = "^(([^<>()\\[\\]\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$",
    message = "pattern")
    private String email;

    //@NotNull(message = "required", groups = { BranchJourneyGroup.class })
    @Pattern(regexp = "^[0-9]*$", message = "pattern")
    private String dialCodeMobile;

    //@NotNull(message = "required", groups = { BranchJourneyGroup.class })
    @Min(value = 10000000L, message = "minlength")
    @Max(value = 9999999999L, message = "maxlength")
    private Long mobile;

    //IdentificationType
    //@NotNull(message = "{IDENTIFICATION_TYPE_REQUIRED_ERROR}", groups = { BranchJourney.class })
    private DocumentName idDocType;

    // Identification document number
    //@NotNull(message = "{ID_NO_REQUIRED_ERROR}")
    @Pattern(regexp = ".*([a-zA-Z0-9]$)", message = "pattern")
    private String idValue;
    
    @Valid
    private Name name;

    //@NotNull(message = "required", groups = { BranchJourneyGroup.class })
    @Pattern(regexp = "[MF]", message = "pattern")
    @Size(min = 1, max = 1, message = "Size must be 1 character.")
    private String gender;
    
    //@NotNull(message = "required", groups = { BranchJourneyGroup.class })
    @Pattern(regexp = "^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)[0-9][0-9]", message = "pattern")
    private String dateOfBirth;
    
    //@NotNull(message = "required", groups = { CustomerJourneyGroup.class })
    @Valid
    private List<CreditCard> cards;
    
    @Valid
    private ApplicantAdditionalInfo additionalInfo;
    
    private List<Document> documents;
    private ApplicantDeclarations applicantDeclarations;
    
    @Valid
    private BeneficiaryDetails beneficiaryDetails;

}
