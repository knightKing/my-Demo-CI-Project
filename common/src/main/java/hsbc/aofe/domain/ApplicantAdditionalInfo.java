package hsbc.aofe.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@JsonIgnoreProperties
@Data
public class ApplicantAdditionalInfo implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1010319930770036663L;
    
    //@NotNull(message ="required", groups = {CustomerJourneyGroup.class})
    @Size(min = 2, max = 255, message = "Size must be between 2 and 255 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "pattern")
    private String mothersMaidenName;
    
    private CountryCode passportCountry;
    
    @Size(max = 20, message = "maxlength")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "pattern")
    private String passportNumber;
    
    private boolean multipleNationality = false;
    private CountryCode nationality2;
    private CountryCode nationality3;
    
    //@NotNull(groups = {CustomerJourneyGroup.class}, message = "required")
    private CountryCode taxResidenceCountry1;
    private CountryCode taxResidenceCountry2;
    private CountryCode taxResidenceCountry3;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private NumberOfDependents noOfDependents;
    
    //@NotNull(message = "required", groups = { CustomerJourneyGroup.class })
    private EducationalLevel educationLevel;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private MaritalStatus maritalStatus;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private HomeOwnership homeOwnership;
    
    //TODO: remove
    private String correspondenceOn;
    
    @Pattern(regexp = "^[0-9]*$", message = "pattern")
    private String dialCodeHomePhone;
    
    @Min(value = 60000000L, message = "minlength")
    @Max(value = 6999999999L, message = "maxlength")
    private Long homePhone;
    
    @Min(value = 60000000L, message = "minlength")
    @Max(value = 6999999999L, message = "maxlength")
    private Long officePhone;
    
    //TODO: remove 
    private Long officeExtn;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private EmployementStatus employmentType;
    
    private Long monthsToEmpPassExpiry;
    private Long annualIncome;
    private Long lengthOfService;
    private Occupation occupation;
    
    @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "pattern")
    private String position;
    
    @Pattern(regexp = "^[a-zA-Z0-9\\s]+$", message = "pattern")
    private String companyName;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private boolean isResidentialAddressSameAsPrimary = true;
    
    private boolean residentialAddIdenticalToPermAddress = true;
    
    @Valid
    private List<Address> address;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private Long monthsAtResidentialAddress; // TODO ?
    
    @Min(value = 100000L, message = "minlength")
    @Max(value = 999999L, message = "maxlength")
    private Long officePostalCode;
    
    private Long timeAtPreviousEmployer;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private Boolean holdingPublicPosition;
    
    @Size(max = 40, message = "maxlength")
    @Pattern(regexp = "^[a-zA-Z0-9\\s]+$", message = "pattern")
    private String publicPositionDetails;
    
    private String autoDebitFromAccountNumber;
    
    //@NotNull(message = "required", groups = { CustomerJourneyGroup.class } )
    private CountryCode countryOfBirth;
    
	@Size(min = 2, max = 40, message = "Size must be between 2 and 40 characters")
	@Pattern(regexp = "^[a-zA-Z\\s]+$", message = "pattern")
    private String otherName;
    
    private RelationshipWithPrimary relationshipWithPrimary = RelationshipWithPrimary.SELF;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class })
	@Size(min = 2, max = 19, message = "Size must be between 2 and 19 characters")
	@Pattern(regexp = ".*([a-zA-Z\\s]$)", message = "pattern")
    private String nameOnCard;
    
	private LocalDate idIssueDate;
    private LocalDate idExpiryDate;
    private CountryCode countryOfIdIssue;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private CountryCode nationality;
    
    private NatureOfBusiness industryType;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private Boolean associateOfPublicPositionHolder;
    
    @Size(max = 50, message = "maxlength")
    @Pattern(regexp = "^[a-zA-Z0-9\\s]+$", message = "pattern")
    private String associateOfPublicPositionHolderDetails;

    private boolean introducedByHSBCCardHolder = false;
    
    @Size(max = 35, message = "maxlength")
    @Pattern(regexp = "^[a-zA-Z0-9\\s]+$", message = "pattern")
    private String introducedByHSBCCardHolderReferrerID;
    private Integer verifiedIncome;

}
