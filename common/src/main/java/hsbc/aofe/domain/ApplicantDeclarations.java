package hsbc.aofe.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class ApplicantDeclarations implements Serializable {

    private boolean consentContactMe = false;
    private boolean consentPersonalData = false;
    private boolean consentMarketing = true;
    private boolean consentAccountOpening = false;
    private boolean sharedCreditLimit = false;
    private Boolean balanceTransferOnCreditCard;

}
