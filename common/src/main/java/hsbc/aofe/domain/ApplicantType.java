package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApplicantType {
	PRIMARY,SUPPLEMENTARY;
}
