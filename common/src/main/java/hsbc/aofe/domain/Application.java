package hsbc.aofe.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties
@Data
public class Application implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 245008874862534035L;

	private Long id;
	private String arn;
	private String aoRefNumber;
	
	@Valid
	private Applicant primaryApplicant;
	
	private Boolean addSupplimentaryCard;
	
	private ChannelType initiatedBy;
	
	private boolean allocateExistingLimitToNew; 
	
	@Pattern(regexp = ".*([0-9]$)", message = "pattern")
	private String existingCardNumber;
	
	@Pattern(regexp = ".*([0-9]$)", message = "pattern")
	private String atmLinkedAcNumber;
	
    private Long howMuchAutoDebit; // changing type form int to long

    private List<Applicant> supplementaryApplicant;

    private boolean createAutoDebitFromAccount;

    private ApplicationStaffDetails staffDetails;

    /* Application Declarations */
    private boolean receiveEStatement = false;
    private boolean agreeToGenericConsent = false;
    private boolean transactingOnMyOwn = false;
    private boolean wouldLikeToOpenNewAccount = false;
    // private ApplicantAdditionalInfo additionalDetails;
    // TODO: In AO so , so added, Values yet to be mapped with repo.
    private String additionalIncome;
    private boolean docCompletenessIndicator = false;
    private boolean employmentCheckIndicator = false;
    private boolean incomeAutomatedCalcIndicator = false;	
    private boolean essentialDataPreFilledIndicator=true;
}
