package hsbc.aofe.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import hsbc.aofe.validationgroups.BranchJourneyGroup;
import lombok.Data;
@JsonIgnoreProperties
@Data
public class ApplicationStaffDetails implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5277690115540474803L;
	
	private String nameOfStaff;
	private String creditCardNumber;
	
	//@NotNull(message="required", groups = {BranchJourneyGroup.class})
	private SourceCode sourceCode;
	
	//@NotNull(message="required", groups = {BranchJourneyGroup.class})
	private String voucherCode;
	
	//@NotNull(message="required", groups = {BranchJourneyGroup.class})
	private String referralId;
	
	private String remarks;
	private String approvedBy;
	
	//@NotNull(message = "required", groups = {BranchJourneyGroup.class})
	private Long preApprovedLimit;
	
	//@NotNull(message = "required", groups = {BranchJourneyGroup.class})
	//@Pattern(regexp = ".*([a-zA-Z0-9\\s]$)", message = "pattern")
	private String location;
	private Boolean ecpfAuthorized;
	
	//TODO: default values of next three fields are to be confirmed and changed.
	//@NotNull(message = "required", groups = {BranchJourneyGroup.class})
	private MarketSectorCode marketSectorCode;
	
	//@NotNull(message = "required", groups = {BranchJourneyGroup.class})
	private GhoClassification ghoCode;
	
	//@NotNull(message = "required", groups = {BranchJourneyGroup.class})
	private SpecialInstructionIndicator splInstIndicator;
	
	
	private String promoCode;
}
