package hsbc.aofe.domain;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum BankNameForBalanceTransfer {

    THE_ROYAL_BANK_OF_SCOTLAND_NV(1, "010", "The Royal Bank Of Scotland Nv"),
    BANGKOK_BANK_PUBLIC_COMPANY_LTD(2, "047", "Bangkok Bank Public Company Ltd"),
    PT_BANK_NEGARA_INDONESIA_PERSERO_TBK(3, "056", "Pt Bank Negara Indonesia Persero Tbk"),
    BANK_OF_AMERICA_NA(4, "065", "Bank Of America Na"),
    BANK_OF_CHINA_LIMITED(5, "083", "Bank Of China Limited"),
    THE_BANK_OF_EAST_ASIA_LTD(6, "092", "The Bank Of East Asia Ltd"),
    BANK_OF_INDIA(7, "108", "Bank Of India"),
    SINGAPORE_ISLAND_BANK_LIMITED(8, "117", "Singapore Island Bank Limited"),
    THE_BANK_OF_TOKYO_MITSUBISHI_UFJ_LTD(9, "126", "The Bank Of Tokyo Mitsubishi Ufj Ltd"),
    STANDARD_CHARTERED_BANK(10, "144", "Standard Chartered Bank"),
    JP_MORGAN_CHASE_BANK_NA(11, "153", "Jp Morgan Chase Bank Na"),
    DEVELOPMENT_BANK_OF_SINGAPORE(12, "171", "Development Bank Of Singapore"),
    FAR_EASTERN_BANK(13, "199", "Far Eastern Bank"),
    CITIBANK_NA(14, "214", "Citibank Na"),
    HSBC_BANK_SINGAPORE_LIMITED(15, "232", "Hsbc Bank Singapore Limited"),
    INDIAN_BANK(16, "241", "Indian Bank"),
    INDIAN_OVERSEAS_BANK(17, "250", "Indian Overseas Bank"),
    HL_BANK(18, "287", "HL Bank"),
    MALAYAN_BANKING_BERHAD(19, "302", "Malayan Banking Berhad"),
    OVERSEA_CHINESE_BANKING_CORPORATION_LTD(20, "339", "Oversea Chinese Banking Corporation Ltd"),
    INTESA_SANPAOLO_SPA(21, "350", "Intesa Sanpaolo Spa"),
    UCO_BANK(22, "357", "Uco Bank"),
    RHB_BANK_BERHAD(23, "366", "Rhb Bank Berhad"),
    UNITED_OVERSEAS_BANK_LTD(24, "375", "United Overseas Bank Ltd"),
    DRESDNER_BANK_AG(25, "393", "Dresdner Bank Ag"),
    BNP_PARIBAS(26, "418", "Bnp Paribas"),
    DEUTSCHE_BANK_AG(27, "463", "Deutsche Bank Ag"),
    SUMITOMO_MITSUI_BANKING_CORPORATION(28, "472", "Sumitomo Mitsui Banking Corporation"),
    KOREA_EXCHANGE_BANK(29, "490", "Korea Exchange Bank"),
    NORDEA_BANK_FINLAND_PLC(30, "518", "Nordea Bank Finland Plc"),
    SKANDINAVISKA_ENSKILDA_BANKEN_AB_PUBL(31, "527", "Skandinaviska Enskilda Banken Ab Publ"),
    COMMERZBANK_AG(32, "606", "Commerzbank Ag"),
    MIZUHO_CORPORATE_BANK_LTD(33, "621", "Mizuho Corporate Bank Ltd"),
    DNB_NOR_BANK_ASA(34, "737", "Dnb Nor Bank Asa"),
    FIRST_COMMERCIAL_BANK(35, "764", "First Commercial Bank"),
    STATE_BANK_OF_INDIA(36, "791", "State Bank Of India"),
    AUSTRALIA_NEW_ZEALAND_BANKING_GROUP_LTD(37, "931", "Australia New Zealand Banking Group Ltd"),
    CIMB_BANK_BERHAD(38, "986", "Cimb Bank Berhad");

    private int code;
    private String value;
    private String meaning;

    private static final Map lookup = new HashMap();
    private static final Map lookupValues = new HashMap();

    static {
        for (BankNameForBalanceTransfer d : BankNameForBalanceTransfer.values()) {
            lookup.put(d.getValue(), d);
        }
        for (BankNameForBalanceTransfer d : BankNameForBalanceTransfer.values()) {
            lookupValues.put(d, d.getValue());
        }
    }

    public static BankNameForBalanceTransfer getEnum(String value) {
        return (BankNameForBalanceTransfer) lookup.get(value);
    }

    public static String get(BankNameForBalanceTransfer d) {
        return (String) lookupValues.get(d);
    }

    public String getValue() {
        return value;
    }


}
