package hsbc.aofe.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import hsbc.aofe.validationgroups.CustomerJourneyGroup;
import lombok.Data;

@JsonIgnoreProperties
@Data
public class BeneficiaryDetails implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String ccKey;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "pattern")
    @Size(max = 35, message = "maxlength")
    private String beneficiaryAccountName;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    @Size(min = 1, max = 19, message = "Size must be between 1 and 19")
    private String beneficiaryAccountNumber;
	
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    private BankNameForBalanceTransfer beneficiaryBank;
    
    //@NotNull(message = "required", groups = {CustomerJourneyGroup.class})
    @Min(value = 1000, message = "minvalue")
    @Digits(integer = 10, fraction=0, message = "maxlength")
    private BigInteger amountToBeTransferred;

}