package hsbc.aofe.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class CardDeclaration implements Serializable {

    private boolean bankToAssignCreditLimit = true;
}
