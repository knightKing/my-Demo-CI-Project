package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChannelType {
	//All is not a value that is pushed to AO
	ALL("All","All"),
	BR("BRANCH","B"),
	TS("TELE_SALES","T"),
	S("ROAD_SHOW","S"),
	TP("TPSA","P"),
	CU("CUSTOMER","D");
	
	String channel;
	String aoValue;
}
