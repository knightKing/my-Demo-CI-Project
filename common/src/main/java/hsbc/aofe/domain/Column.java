package hsbc.aofe.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 6/13/2017.
 *****************************************
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Column {

    /**
     * This represents the column data (having number of rows equals to the data in the table)
     */
    @JsonProperty("FD")
    private ColumnData columnData;

    /**
     * This represents the column name
     */
    @JsonProperty("FN")
    private String columnName;

}
