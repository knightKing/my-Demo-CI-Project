package hsbc.aofe.domain;

import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 6/13/2017.
 *****************************************
 */

/**
 * This represents a list rows for a particular column {@code hsbc.{@link Column.FN}}
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ColumnData {

    @JsonProperty("R")
    private LinkedList<Row> Row;
}
