package hsbc.aofe.domain;

import java.io.Serializable;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class CreditCard implements Serializable {

    private static final long serialVersionUID = -4957782718734581750L;
    private String key;
    private String name;
    private String imageURL;
    private String type = "TRAVEL CARD";
    private Boolean eligibleForCustomer;
    private Long minSalary;
    private Long rewardPoints;
    private Long rewardPointScale;
    private String complimentaryStuff = "Complimentary Stuff";
    private Long annualFee;
    private String annualFeeCurrency;
	
	@Min(value = 500L, message = "minvalue")
    private Long limit;
    private Long limitMinimumOfInfo;

   	private CardDeclaration cardDeclaration;
}