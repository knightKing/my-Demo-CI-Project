package hsbc.aofe.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class Dashboard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5112460063769351038L;
	
	private BigDecimal totalCount;
	private String customerName;
	private String idValue;
	private String cardName;
	private String channel;
	private String staffId;
	private Timestamp applicationCreatedOn;
	private String ngStatus;
	private String aoRefNo;
	private String aoStatus;
	private Timestamp aoSubmissinDate;
	private String internalRemarks = null ;
	private String ffAgentName;
	private String arn;
	private String email;
	private BigDecimal mobile;
	private String dialCodeMobile;
	
}
