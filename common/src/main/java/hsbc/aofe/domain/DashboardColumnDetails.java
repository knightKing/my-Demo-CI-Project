package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DashboardColumnDetails {
	
	CUSTOMERNAME("CONCAT(APP_DETAILS.FIRSTNAME, CONCAT(' ',APP_DETAILS.LASTNAME))"),
	IDVALUE("APP_DETAILS.IDVALUE"),
	CARDNAME(" LISTAGG(CC_M.KEY,',') WITHIN GROUP (ORDER BY CC_M.KEY)"),
	CHANNEL("CM.NAME"),
	STAFFID("SD.STAFFID"),
	APPLICATIONCREATEDON("TO_CHAR(CC_APP.CREATEDON,'DD-MON-YYYY HH24:MI:SS')"),
	NGSTATUS("NG.KEY"),
	AOREFNO("CC_APP.AOREFNUM"),
	AOSTATUS("SGHAO.PROD_STAT_CDE"),
	AOSUBMISSINDATE("CC_APP.AOSUBMISSIONDATE"),
	INTERNALREMARKS("IR.REMARKS AS INTERNALREMARKS"),
	FFAGENTNAME("CC_APP.FULFILMENTAGENT"),
	ARN("CC_APP.ARN"),
	MOBILE("APP_DETAILS.MOBILE"),
	EMAIL("APP_DETAILS.EMAIL");
	
	
	String columnName;

}
