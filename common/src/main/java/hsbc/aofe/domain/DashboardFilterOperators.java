package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DashboardFilterOperators {

	lte("<="),
	lt("<"),
	gte(">="),
	eq("="),
	neq(""),
	gt(">"),
	LIKE("like");

	String operator;
	
}
