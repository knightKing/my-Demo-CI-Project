package hsbc.aofe.domain;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class DashboardListing {


	private List<Dashboard> dashboards;
	private BigDecimal totalCount;
}
