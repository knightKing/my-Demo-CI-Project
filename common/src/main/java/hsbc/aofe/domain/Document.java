package hsbc.aofe.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class Document implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2472457611478823809L;
	private DocumentGroup docGroup;
	private DocumentName docName;
	private List<Image> images;
	private long numberOfImages;
	
}
