package hsbc.aofe.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DocumentGroup {

	IDENTITY(1, "Identity"), INCOME(2, "Income"), NATIONALITY(3, "Nationality"), OTHERS(4, "Others"), ESIGNATURE(5, "eSignature"),
	DEEDPOLL(6,"DeedPoll"), DECLARATION(7,"Declaration"), APPLICATION(8, "Application");

	private long id;
	private String value;

}
