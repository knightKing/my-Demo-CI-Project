package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DocumentName {

    NRIC(1, "NRIC", "I"),
    PASSPORT(2, "PASSPORT", "P"),
    MALAYSIAN_IDENTITY_CARD(3, "Malaysian Identity Card", "O"),
    OTHERS(4, "OTHERS", "X"),
    EMPLOYMENT_PASS(5, "EMPLOYMENT_PASS", "P"),
    ECPF(6, "ECPF", "X"),
    NOTICE_OF_ASSESSMENT(7, "NOTICE OF ASSESSMENT", "X"),
    SIGNATURE(8, "SIGNATURE", "X"),
    PAYSLIP(9, "PAYSLIP", "X"),
    DEEDPOLL(10, "DEEDPOLL", "X"),
    LOI(11, "LOI", "X"),
    FFDECLARATION(12, "FFDECLARATION", "X"),
    APPLICATIONPDF(13, "APPLICATIONPDF", "X");


    private long id;
    private String value;
    private String aoValue;

}
