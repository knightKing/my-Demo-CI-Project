package hsbc.aofe.domain;

import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 6/13/2017.
 *****************************************
 */
@Data
public class EcpfContentResource {

    /**
     * This is a list of columns and their respective data in the table
     */
    @JsonProperty("F")
    private LinkedList<Column> columns;

    /**
     * status of request
     * ERRORCODE =  0 signifies success
     */
    @JsonProperty("ERRORCODE")
    private int errorCode;

    /**
     * This represents the number of pages in the attached PDF document
     * TODO: need to confirm
     */
    @JsonProperty("NUM")
    private String numOfPagesInDocument;

}
