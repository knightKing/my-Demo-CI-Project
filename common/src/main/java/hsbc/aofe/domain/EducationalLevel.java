package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EducationalLevel {
    NONE("001", "None/Primary", "None/Primary"),
    VOCATIONAL("002", "Vocational/Technical", "Vocational/Technical"),
    SECONDARY("003", "Secondary/Post-Sec",  "Secondary/Post-Sec"),
    UNIVERSITY("004", "University/Teritiary", "University"),
    POSTGRADUATE("005", "Postgraduate", "Postgraduate");

    private final String id;
    private final String value;
    private final String meaning;

}