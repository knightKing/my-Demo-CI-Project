package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmployementStatus {
	
	SELF_EMPLOYED(1, "Self-Employed","S", "Self-Employed"),
	EMPOYED_FULL_TIME(2, "Employed Full Time","F", "Employed Full Time"),
	EMPLOYED_PART_TIME(3, "Employed Part Time","A", "Employed Part Time"),
	SALES_COMMISSION_BASED(4, "Sales-Commission Based","G", "Sales/Commission Based"),
	UNEMPLOYED(5, "Unemployed","92", "Unemployed"),
	RETIRED(6, "Retired","93", "Retired"),
	STUDENT(7, "Student","88", "Student"),
	HOUSEWIFE(8, "HouseWife","79", "HouseWife");
	
	private final long id;
	private final String value;
	private String aoValue;
	private String meaning;
}
