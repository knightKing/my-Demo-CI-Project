package hsbc.aofe.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class ExportDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2927577800249327221L;
	
	private String docType;
	private String docName;
	private byte[] document;
}
