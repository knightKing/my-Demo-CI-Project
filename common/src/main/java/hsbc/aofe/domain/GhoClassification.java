package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GhoClassification {
	
	DIPLOMAT_SOLE(1,"DIP"),
	MINOR_SOLE(2,"MRS"),
	PERSONAL(3,"PRS"),
	PERSONAL_CUSTOMER_OVERSEAS(4,"PSO"),
	STAFF(5,"STF"),
	STAFF_SPOUSE(6,"STS"),
	TSY_OVERSEAS_PERSONAL_CUSTOMER(7,"TPO");

	private long id;
	private String value;

}
