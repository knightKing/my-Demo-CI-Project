package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HomeOwnership {

    RENTING(1, "Renting", "7", "Renting"),
    LOAN(2, "Loan/Mortgaged properties", "6", "Loan/Mortgaged"),
    LIVINGWITHPARENTS(3, "Living with parents", "9", "Parents'"),
    FULLYOWNED(4, "Fully owned", "1", "Fully owned"),
    COMPANYRESIDENCE(5, "Company residence", "0", "Company Residence");

    private long id;
    private String value;
    private String aoValue;
    private String meaning;

}
