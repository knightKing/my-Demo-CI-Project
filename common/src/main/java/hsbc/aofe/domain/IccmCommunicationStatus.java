package hsbc.aofe.domain;

import lombok.Getter;

@Getter
public enum IccmCommunicationStatus {

	NEW,
	SEND,
	FAIL
		
}
