package hsbc.aofe.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotBlank;
import lombok.Data;

@Data
@JsonIgnoreProperties
public class Image implements Serializable{

	private String name;
	private byte[] image;
	private long index;
	private boolean isExtracted;
	private List<OcrData> ocrExtracted;
}
