package hsbc.aofe.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class IpAccessDetails implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = -2353380612100228899L;
	
	private String ipAddress;
	private String url;
	private Timestamp accessTime;

}
