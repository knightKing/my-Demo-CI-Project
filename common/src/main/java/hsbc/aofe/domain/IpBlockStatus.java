package hsbc.aofe.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties
@Data
public class IpBlockStatus implements Serializable{
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 7317123147330730631L;
	private String ipAddress;
    private Boolean isBlocked;

}
