package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MaritalStatus {
	NOTGIVEN(0, "Not Given"),SINGLE(1, "Single"), MARRIED(2, "Married"), WIDOWED(3, "Widowed"), DIVORCED(4, "Divorced");

	private long id;
	private String value;

}
