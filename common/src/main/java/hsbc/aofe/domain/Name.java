package hsbc.aofe.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import hsbc.aofe.validationgroups.BranchJourneyGroup;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Name implements Serializable {

	private static final long serialVersionUID = 1102851585215593734L;

	//@NotNull(message = "required", groups = {BranchJourneyGroup.class })
	@Size(min = 2, max = 35, message = "Size must be between 2 and 35 characters")
	@Pattern(regexp = ".*([a-zA-Z\\s]$)", message = "pattern")
	private String firstName;

	@Size(max = 35, message = "maxlength")
	@Pattern(regexp = ".*([a-zA-Z\\s]$)", message = "pattern")
	private String middleName;

	//@NotNull(message = "required", groups = { BranchJourneyGroup.class })
	@Size(min = 2, max = 35, message = "Size must be between 2 and 35 characters")
	@Pattern(regexp = ".*([a-zA-Z\\s]$)", message = "pattern")
	private String lastName;

}
