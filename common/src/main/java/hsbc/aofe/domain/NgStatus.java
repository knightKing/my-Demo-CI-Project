package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NgStatus {

    INPROGRESS(1, "IN-PROGRESS", "In progress"),
    SUBMITTEDTOAO(2, "SUBMITTED-TO-AO", "Submitted to ao"),
	DISCARDED(3, "DISCARDED", "Discarded"), 
	DELETED(4, "DELETED", "Deleted"), 
    PENDINGFORREVIEW(6, "PENDING-FOR-REVIEW", "Pending for review"),
    PENDINGWITHCUSTOMER(7, "PENDING-WITH-CUSTOMER", "Pending with customer"),
    ALL(8,"ALL", "All");

    private long id;
    private String value;
    private String meaning;

}
