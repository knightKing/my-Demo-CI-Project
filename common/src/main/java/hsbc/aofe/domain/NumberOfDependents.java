package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NumberOfDependents {

    ZERO(0, "00"),
    ONE(1, "01"),
    TWO(2, "02"),
    THREE(3, "03"),
    FOUR(4, "04"),
    FIVE(5, "05"),
    SIX(6, "06"),
    SEVEN(7, "07"),
    EIGHT(8, "08"),
    NINE(9, "09"),
    GREATER_THAN_NINE(10, "10");

    private int code;
    private String value;
}
