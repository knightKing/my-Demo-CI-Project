package hsbc.aofe.domain;

import java.time.LocalDate;

import lombok.Data;
import lombok.experimental.Builder;

/*****************************************
 * Created by NEWGEN SOFTWARE LTD.
 *****************************************
 */
@Data
@Builder
public class OCRContentResource<T> {

    /**
     * extracted data from the IPS
     */
    private T extractedData;
    /**
     * This will be extracted from the IPS
     */
    private String CPF_Account_Number;

    private String employmentStatus;
    private String NRIC_number;
    private String nationality;
    private LocalDate NRIC_issueDate;
    private LocalDate dateOfBirth;
}
