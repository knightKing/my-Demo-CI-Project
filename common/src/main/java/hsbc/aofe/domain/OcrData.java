package hsbc.aofe.domain;

import lombok.Data;

@Data
public class OcrData {

	private String key;
	private String value;
}
