package hsbc.aofe.domain;

import java.util.List;

import hsbc.aofe.exception.OcrException;
import lombok.Data;
@Data
public class OcrProcessedException {

	OcrException exception;
}
