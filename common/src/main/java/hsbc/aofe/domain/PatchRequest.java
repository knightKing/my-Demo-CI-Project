package hsbc.aofe.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

/*****************************************
 * Created by NEWGEN SOFTWARE LTD.
 *****************************************
 */

@Data
@JsonIgnoreProperties
public class PatchRequest implements Serializable {

    /**
     * request should be send to customer
     */
    private boolean toCustomer = false;  // default false

    /**
     * Validations to be done or not
     */
    private boolean validations = false; // default false


    private boolean forReview = false; // default false
    private boolean toAO = false; // default false

    /**
     * patch to apply on the object
     */
    private JsonNode patch;

}
