package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RelationshipWithPrimary {

	BROTHER_OR_SISTER("Brother or Sister", 1, "B"),
	CHILDREN("Children", 2, "C"), 
	COLLEAGUE("Colleague", 3, "E"), 
	FRIEND("Friend", 4, "F"), 
	OTHERS("Others", 5, "O"), 
	PARENT("Parent", 6, "P"), 
	RELATIVE("Relative", 7, "R"), 
	SPOUSE("Spouse", 8, "S"), 
	SELF("Self", 1, "X");

	private String value;
	private long id;
	private String aoValue;

}
