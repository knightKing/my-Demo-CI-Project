package hsbc.aofe.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties
public class Remarks implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1663074428516392558L;

	private String staffId;
	private String dateTime;
	private List<String> remark;

}
