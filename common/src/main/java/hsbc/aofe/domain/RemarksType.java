package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RemarksType {

    FOR_FA_SEND_TO_CUSTOMER("FOR_FA_SEND_TO_CUSTOMER"),
    FOR_FA_DECLARATION("FOR_FA_DECLARATION"),
    FOR_FA_INTERNAL("FOR_FA_INTERNAL");

    private String value;

}
