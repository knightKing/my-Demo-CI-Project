package hsbc.aofe.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;



/*****************************************
 * Created by sahil.verma on 6/13/2017.
 *****************************************
 */

/**
 * This represents a particlar row of a table
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Row {

    /**
     * This represend the rowdata
     */
    @JsonProperty("RD")
    private String rowData;

    @JsonProperty("NUM")
    private int rowNumber;

}
