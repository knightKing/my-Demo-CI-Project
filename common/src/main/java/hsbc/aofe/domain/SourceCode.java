package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SourceCode {

	_0A(1,"0A"),
	_0B(2,"0B"),
	_0E(3,"0E"),
	_0K(4,"0K"),
	_0L(5,"0L"),
	_0R(6,"0R"),
	_0S(7,"0S"),
	_0T(8,"0T"),
	_0W(9,"0W"),
	_0X(10,"0X"),
	_0Y(11,"0Y"),
	_0Z(12,"0Z"),
	_1A(13,"1A"),
	_1B(14,"1B"),
	_1E(15,"1E"),
	_1K(16,"1K"),
	_1L(17,"1L"),
	_1R(18,"1R"),
	_1S(19,"1S"),
	_1T(20,"1T"),
	_1W(21,"1W"),
	_1X(22,"1X"),
	_1Y(23,"1Y"),
	_1Z(24,"1Z"),
	_2B(25,"2B"),
	_2E(26,"2E"),
	_2K(27,"2K"),
	_2L(28,"2L"),
	_2R(29,"2R"),
	_2S(30,"2S"),
	_2T(31,"2T"),
	_2W(32,"2W"),
	_2X(33,"2X"),
	_2Y(34,"2Y"),
	_2Z(35,"2Z"),
	_3B(36,"3B"),
	_3E(37,"3E"),
	_3K(38,"3K"),
	_3L(39,"3L"),
	_3R(40,"3R"),
	_3S(41,"3S"),
	_3T(42,"3T"),
	_3W(43,"3W"),
	_3X(44,"3X"),
	_3Y(45,"3Y"),
	_3Z(46,"3Z"),
	_4B(47,"4B"),
	_4E(48,"4E"),
	_4K(49,"4K"),
	_4L(50,"4L"),
	_4R(51,"4R"),
	_4S(52,"4S"),
	_4T(53,"4T"),
	_4W(54,"4W"),
	_4X(55,"4X"),
	_4Y(56,"4Y"),
	_4Z(57,"4Z"),
	_5B(58,"5B"),
	_5E(59,"5E"),
	_5K(60,"5K"),
	_5L(61,"5L"),
	_5R(62,"5R"),
	_5S(63,"5S"),
	_5T(64,"5T"),
	_5W(65,"5W"),
	_5X(66,"5X"),
	_5Y(67,"5Y"),
	_5Z(68,"5Z"),
	_6B(69,"6B"),
	_6E(70,"6E"),
	_6K(71,"6K"),
	_6L(72,"6L"),
	_6R(73,"6R"),
	_6S(74,"6S"),
	_6T(75,"6T"),
	_6W(76,"6W"),
	_6X(77,"6X"),
	_6Y(78,"6Y"),
	_6Z(79,"6Z"),
	_7B(80,"7B"),
	_7E(81,"7E"),
	_7K(82,"7K"),
	_7L(83,"7L"),
	_7R(84,"7R"),
	_7S(85,"7S"),
	_7T(86,"7T"),
	_7W(87,"7W"),
	_7X(88,"7X"),
	_7Y(89,"7Y"),
	_7Z(90,"7Z"),
	_8B(91,"8B"),
	_8E(92,"8E"),
	_8K(93,"8K"),
	_8L(94,"8L"),
	_8R(95,"8R"),
	_8S(96,"8S"),
	_8T(97,"8T"),
	_8W(98,"8W"),
	_8X(99,"8X"),
	_8Y(100,"8Y"),
	_8Z(101,"8Z"),
	_9B(102,"9B"),
	_9E(103,"9E"),
	_9K(104,"9K"),
	_9L(105,"9L"),
	_9R(106,"9R"),
	_9S(107,"9S"),
	_9T(108,"9T"),
	_9W(109,"9W"),
	_9X(110,"9X"),
	_9Y(111,"9Y"),
	_9Z(112,"9Z"),
	_A0(113,"A0"),
	_A1(114,"A1"),
	_A2(115,"A2"),
	_A3(116,"A3"),
	_A4(117,"A4"),
	_A5(118,"A5"),
	_A6(119,"A6"),
	_A7(120,"A7"),
	_A8(121,"A8"),
	_A9(122,"A9"),
	_AA(123,"AA"),
	_AB(124,"AB"),
	_AC(125,"AC"),
	_AD(126,"AD"),
	_AE(127,"AE"),
	_AF(128,"AF"),
	_AG(129,"AG"),
	_AH(130,"AH"),
	_AI(131,"AI"),
	_AJ(132,"AJ"),
	_AK(133,"AK"),
	_AL(134,"AL"),
	_AM(135,"AM"),
	_AN(136,"AN"),
	_AO(137,"AO"),
	_AP(138,"AP"),
	_AQ(139,"AQ"),
	_AR(140,"AR"),
	_AS(141,"AS"),
	_AT(142,"AT"),
	_AU(143,"AU"),
	_AV(144,"AV"),
	_AW(145,"AW"),
	_AX(146,"AX"),
	_AY(147,"AY"),
	_AZ(148,"AZ"),
	_B0(149,"B0"),
	_B1(150,"B1"),
	_B2(151,"B2"),
	_B3(152,"B3"),
	_B4(153,"B4"),
	_B5(154,"B5"),
	_B6(155,"B6"),
	_B7(156,"B7"),
	_B8(157,"B8"),
	_B9(158,"B9"),
	_BA(159,"BA"),
	_BB(160,"BB"),
	_BC(161,"BC"),
	_BD(162,"BD"),
	_BE(163,"BE"),
	_BF(164,"BF"),
	_BG(165,"BG"),
	_BH(166,"BH"),
	_BI(167,"BI"),
	_BJ(168,"BJ"),
	_BK(169,"BK"),
	_BL(170,"BL"),
	_BM(171,"BM"),
	_BN(172,"BN"),
	_BO(173,"BO"),
	_BP(174,"BP"),
	_BQ(175,"BQ"),
	_BR(176,"BR"),
	_BS(177,"BS"),
	_BT(178,"BT"),
	_BU(179,"BU"),
	_BV(180,"BV"),
	_BW(181,"BW"),
	_BX(182,"BX"),
	_BY(183,"BY"),
	_BZ(184,"BZ"),
	_CA(185,"CA"),
	_D0(186,"D0"),
	_D1(187,"D1"),
	_D2(188,"D2"),
	_D3(189,"D3"),
	_D4(190,"D4"),
	_D5(191,"D5"),
	_D6(192,"D6"),
	_D7(193,"D7"),
	_D8(194,"D8"),
	_D9(195,"D9"),
	_DA(196,"DA"),
	_DB(197,"DB"),
	_DC(198,"DC"),
	_DD(199,"DD"),
	_DE(200,"DE"),
	_DF(201,"DF"),
	_DG(202,"DG"),
	_DH(203,"DH"),
	_DI(204,"DI"),
	_DJ(205,"DJ"),
	_DK(206,"DK"),
	_DL(207,"DL"),
	_DM(208,"DM"),
	_DN(209,"DN"),
	_DO(210,"DO"),
	_DP(211,"DP"),
	_DQ(212,"DQ"),
	_DR(213,"DR"),
	_DS(214,"DS"),
	_DT(215,"DT"),
	_DU(216,"DU"),
	_DV(217,"DV"),
	_DW(218,"DW"),
	_DX(219,"DX"),
	_DY(220,"DY"),
	_DZ(221,"DZ"),
	_E0(222,"E0"),
	_E1(223,"E1"),
	_E2(224,"E2"),
	_E3(225,"E3"),
	_E4(226,"E4"),
	_E5(227,"E5"),
	_E6(228,"E6"),
	_E7(229,"E7"),
	_E8(230,"E8"),
	_E9(231,"E9"),
	_EA(232,"EA"),
	_EB(233,"EB"),
	_EC(234,"EC"),
	_ED(235,"ED"),
	_EE(236,"EE"),
	_EF(237,"EF"),
	_EG(238,"EG"),
	_EH(239,"EH"),
	_EI(240,"EI"),
	_EJ(241,"EJ"),
	_EK(242,"EK"),
	_EL(243,"EL"),
	_EM(244,"EM"),
	_EN(245,"EN"),
	_EO(246,"EO"),
	_EP(247,"EP"),
	_EQ(248,"EQ"),
	_ER(249,"ER"),
	_ES(250,"ES"),
	_ET(251,"ET"),
	_EU(252,"EU"),
	_EV(253,"EV"),
	_EW(254,"EW"),
	_EX(255,"EX"),
	_EY(256,"EY"),
	_EZ(257,"EZ"),
	_J0(258,"J0"),
	_J1(259,"J1"),
	_J2(260,"J2"),
	_J31(261,"J3"),
	_J4(262,"J4"),
	_J5(263,"J5"),
	_JA(264,"JA"),
	_JB(265,"JB"),
	_JC(266,"JC"),
	_JD(267,"JD"),
	_JE(268,"JE"),
	_JF(269,"JF"),
	_JG(270,"JG"),
	_JH(271,"JH"),
	_JI(272,"JI"),
	_JJ(273,"JJ"),
	_JK(274,"JK"),
	_JL(275,"JL"),
	_JM(276,"JM"),
	_JN(277,"JN"),
	_JO(278,"JO"),
	_JP(279,"JP"),
	_JQ(280,"JQ"),
	_JR(281,"JR"),
	_JS(282,"JS"),
	_JT(283,"JT"),
	_JU(284,"JU"),
	_JV(285,"JV"),
	_JW(286,"JW"),
	_JX(287,"JX"),
	_JY(288,"JY"),
	_JZ(289,"JZ"),
	_K0(290,"K0"),
	_K1(291,"K1"),
	_K2(292,"K2"),
	_K3(293,"K3"),
	_K4(294,"K4"),
	_K5(295,"K5"),
	_K6(296,"K6"),
	_K7(297,"K7"),
	_K8(298,"K8"),
	_K9(299,"K9"),
	_KA(300,"KA"),
	_KB(301,"KB"),
	_KC(302,"KC"),
	_KD(303,"KD"),
	_KE(304,"KE"),
	_KF(305,"KF"),
	_KG(306,"KG"),
	_KH(307,"KH"),
	_KI(308,"KI"),
	_KJ(309,"KJ"),
	_KK(310,"KK"),
	_KL(311,"KL"),
	_KM(312,"KM"),
	_KN(313,"KN"),
	_KO(314,"KO"),
	_KP(315,"KP"),
	_KQ(316,"KQ"),
	_KR(317,"KR"),
	_KS(318,"KS"),
	_KT(319,"KT"),
	_KU(320,"KU"),
	_KV(321,"KV"),
	_KW(322,"KW"),
	_KX(323,"KX"),
	_KY(324,"KY"),
	_KZ(325,"KZ"),
	_L0(326,"L0"),
	_L1(327,"L1"),
	_L2(328,"L2"),
	_L3(329,"L3"),
	_L4(330,"L4"),
	_L5(331,"L5"),
	_L6(332,"L6"),
	_L7(333,"L7"),
	_L8(334,"L8"),
	_L9(335,"L9"),
	_LA(336,"LA"),
	_LB(337,"LB"),
	_LC(338,"LC"),
	_LD(339,"LD"),
	_LE(340,"LE"),
	_LF(341,"LF"),
	_LG(342,"LG"),
	_LH(343,"LH"),
	_LI(344,"LI"),
	_LJ(345,"LJ"),
	_LK(346,"LK"),
	_LL(347,"LL"),
	_LM(348,"LM"),
	_LN(349,"LN"),
	_LO(350,"LO"),
	_LP(351,"LP"),
	_LQ(352,"LQ"),
	_LR(353,"LR"),
	_LS(354,"LS"),
	_LT(355,"LT"),
	_LU(356,"LU"),
	_LV(357,"LV"),
	_LW(358,"LW"),
	_LX(359,"LX"),
	_LY(360,"LY"),
	_LZ(361,"LZ"),
	_MA(362,"MA"),
	_MB(363,"MB"),
	_MC(364,"MC"),
	_MD(365,"MD"),
	_ME(366,"ME"),
	_MF(367,"MF"),
	_MG(368,"MG"),
	_MH(369,"MH"),
	_MI(370,"MI"),
	_MJ(371,"MJ"),
	_MK(372,"MK"),
	_ML(373,"ML"),
	_MM(374,"MM"),
	_MN(375,"MN"),
	_MO(376,"MO"),
	_MP(377,"MP"),
	_MQ(378,"MQ"),
	_MR(379,"MR"),
	_MS(380,"MS"),
	_MT(381,"MT"),
	_MU(382,"MU"),
	_MV(383,"MV"),
	_MW(384,"MW"),
	_MX(385,"MX"),
	_MY(386,"MY"),
	_MZ(387,"MZ"),
	_OA(388,"OA"),
	_OB(389,"OB"),
	_OC(390,"OC"),
	_OD(391,"OD"),
	_OE(392,"OE"),
	_OF(393,"OF"),
	_OG(394,"OG"),
	_OH(395,"OH"),
	_OI(396,"OI"),
	_OJ(397,"OJ"),
	_OK(398,"OK"),
	_OL(399,"OL"),
	_OM(400,"OM"),
	_ON(401,"ON"),
	_OO(402,"OO"),
	_OP(403,"OP"),
	_OQ(404,"OQ"),
	_OR(405,"OR"),
	_OS(406,"OS"),
	_OT(407,"OT"),
	_OU(408,"OU"),
	_OV(409,"OV"),
	_OW(410,"OW"),
	_OX(411,"OX"),
	_OY(412,"OY"),
	_OZ(413,"OZ"),
	_PA(414,"PA"),
	_PB(415,"PB"),
	_PC(416,"PC"),
	_PD(417,"PD"),
	_PE(418,"PE"),
	_PF(419,"PF"),
	_PG(420,"PG"),
	_PH(421,"PH"),
	_PI(422,"PI"),
	_PJ(423,"PJ"),
	_PK(424,"PK"),
	_PL(425,"PL"),
	_PM(426,"PM"),
	_PN(427,"PN"),
	_PO(428,"PO"),
	_PP(429,"PP"),
	_PQ(430,"PQ"),
	_PR(431,"PR"),
	_PS(432,"PS"),
	_PT(433,"PT"),
	_PU(434,"PU"),
	_PV(435,"PV"),
	_PW(436,"PW"),
	_PX(437,"PX"),
	_PY(438,"PY"),
	_PZ(439,"PZ"),
	_R0(440,"R0"),
	_R1(441,"R1"),
	_R2(442,"R2"),
	_R3(443,"R3"),
	_R4(444,"R4"),
	_R5(445,"R5"),
	_R6(446,"R6"),
	_R7(447,"R7"),
	_R8(448,"R8"),
	_R9(449,"R9"),
	_RA(450,"RA"),
	_RB(451,"RB"),
	_RC(452,"RC"),
	_RD(453,"RD"),
	_RE(454,"RE"),
	_RF(455,"RF"),
	_RG(456,"RG"),
	_RH(457,"RH"),
	_RI(458,"RI"),
	_RJ(459,"RJ"),
	_RK(460,"RK"),
	_RL(461,"RL"),
	_RM(462,"RM"),
	_RN(463,"RN"),
	_RO(464,"RO"),
	_RP(465,"RP"),
	_RQ(466,"RQ"),
	_RR(467,"RR"),
	_RS(468,"RS"),
	_RT(469,"RT"),
	_RU(470,"RU"),
	_RV(471,"RV"),
	_RW(472,"RW"),
	_RX(473,"RX"),
	_RY(474,"RY"),
	_RZ(475,"RZ"),
	_SA(476,"SA"),
	_SB(477,"SB"),
	_SC(478,"SC"),
	_SD(479,"SD"),
	_SE(480,"SE"),
	_SF(481,"SF"),
	_SG(482,"SG"),
	_SH(483,"SH"),
	_SI(484,"SI"),
	_SJ(485,"SJ"),
	_SK(486,"SK"),
	_SL(487,"SL"),
	_SM(488,"SM"),
	_SN(489,"SN"),
	_SO(490,"SO"),
	_SP(491,"SP"),
	_SQ(492,"SQ"),
	_SR(493,"SR"),
	_SS(494,"SS"),
	_ST(495,"ST"),
	_SU(496,"SU"),
	_SV(497,"SV"),
	_SW(498,"SW"),
	_SX(499,"SX"),
	_SY(500,"SY"),
	_SZ(501,"SZ"),
	_T0(502,"T0"),
	_T1(503,"T1"),
	_T2(504,"T2"),
	_T3(505,"T3"),
	_T4(506,"T4"),
	_T5(507,"T5"),
	_T6(508,"T6"),
	_T7(509,"T7"),
	_T8(510,"T8"),
	_T9(511,"T9"),
	_TA(512,"TA"),
	_TB(513,"TB"),
	_TC(514,"TC"),
	_TD(515,"TD"),
	_TE(516,"TE"),
	_TF(517,"TF"),
	_TG(518,"TG"),
	_TH(519,"TH"),
	_TI(520,"TI"),
	_TJ(521,"TJ"),
	_TK(522,"TK"),
	_TL(523,"TL"),
	_TM(524,"TM"),
	_TN(525,"TN"),
	_TO(526,"TO"),
	_TP(527,"TP"),
	_TQ(528,"TQ"),
	_TR(529,"TR"),
	_TS(530,"TS"),
	_TT(531,"TT"),
	_TU(532,"TU"),
	_TV(533,"TV"),
	_TW(534,"TW"),
	_TX(535,"TX"),
	_TY(536,"TY"),
	_TZ(537,"TZ"),
	_W0(538,"W0"),
	_W1(539,"W1"),
	_W2(540,"W2"),
	_W3(541,"W3"),
	_W4(542,"W4"),
	_W5(543,"W5"),
	_W6(544,"W6"),
	_W7(545,"W7"),
	_W8(546,"W8"),
	_W9(547,"W9"),
	_WA(548,"WA"),
	_WB(549,"WB"),
	_WC(550,"WC"),
	_WD(551,"WD"),
	_WE(552,"WE"),
	_WF(553,"WF"),
	_WG(554,"WG"),
	_WH(555,"WH"),
	_WI(556,"WI"),
	_WJ(557,"WJ"),
	_WK(558,"WK"),
	_WL(559,"WL"),
	_WM(560,"WM"),
	_WN(561,"WN"),
	_WO(562,"WO"),
	_WP(563,"WP"),
	_WQ(564,"WQ"),
	_WR(565,"WR"),
	_WS(566,"WS"),
	_WT(567,"WT"),
	_WU(568,"WU"),
	_WV(569,"WV"),
	_WW(570,"WW"),
	_WX(571,"WX"),
	_WY(572,"WY"),
	_WZ(573,"WZ"),
	_X0(574,"X0"),
	_X1(575,"X1"),
	_X2(576,"X2"),
	_X3(577,"X3"),
	_X4(578,"X4"),
	_X5(579,"X5"),
	_X6(580,"X6"),
	_X7(581,"X7"),
	_X8(582,"X8"),
	_X9(583,"X9"),
	_XA(584,"XA"),
	_XB(585,"XB"),
	_XC(586,"XC"),
	_XD(587,"XD"),
	_XE(588,"XE"),
	_XF(589,"XF"),
	_XG(590,"XG"),
	_XH(591,"XH"),
	_XI(592,"XI"),
	_XJ(593,"XJ"),
	_XK(594,"XK"),
	_XL(595,"XL"),
	_XM(596,"XM"),
	_XN(597,"XN"),
	_XO(598,"XO"),
	_XP(599,"XP"),
	_XQ(600,"XQ"),
	_XR(601,"XR"),
	_XS(602,"XS"),
	_XT(603,"XT"),
	_XU(604,"XU"),
	_XV(605,"XV"),
	_XW(606,"XW"),
	_XX(607,"XX"),
	_XY(608,"XY"),
	_XZ(609,"XZ"),
	_Y8(610,"Y8"),
	_Y9(611,"Y9"),
	_YA(612,"YA"),
	_YB(613,"YB"),
	_YC(614,"YC"),
	_YD(615,"YD"),
	_YE(616,"YE"),
	_YF(617,"YF"),
	_YG(618,"YG"),
	_YH(619,"YH"),
	_YI(620,"YI"),
	_YJ(621,"YJ"),
	_YK(622,"YK"),
	_YL(623,"YL"),
	_YM(624,"YM"),
	_YN(625,"YN"),
	_YO(626,"YO"),
	_YP(627,"YP"),
	_YQ(628,"YQ"),
	_YR(629,"YR"),
	_YS(630,"YS"),
	_YT(631,"YT"),
	_YU(632,"YU"),
	_YV(633,"YV"),
	_YW(634,"YW"),
	_YX(635,"YX"),
	_YY(636,"YY"),
	_YZ(637,"YZ"),
	_Z0(638,"Z0"),
	_Z1(639,"Z1"),
	_Z2(640,"Z2"),
	_Z3(641,"Z3"),
	_Z4(642,"Z4"),
	_Z5(643,"Z5"),
	_Z6(644,"Z6"),
	_Z7(645,"Z7"),
	_Z8(646,"Z8"),
	_Z9(647,"Z9"),
	_ZA(648,"ZA"),
	_ZB(649,"ZB"),
	_ZC(650,"ZC"),
	_ZD(651,"ZD"),
	_ZE(652,"ZE"),
	_ZF(653,"ZF"),
	_ZG(654,"ZG"),
	_ZH(655,"ZH"),
	_ZI(656,"ZI"),
	_ZJ(657,"ZJ"),
	_ZK(658,"ZK"),
	_ZL(659,"ZL"),
	_ZM(660,"ZM"),
	_ZN(661,"ZN"),
	_ZO(662,"ZO"),
	_ZP(663,"ZP"),
	_ZQ(664,"ZQ"),
	_ZR(665,"ZR"),
	_ZS(666,"ZS"),
	_ZT(667,"ZT"),
	_ZV(668,"ZV"),
	_ZW(669,"ZW"),
	_ZX(670,"ZX"),
	_ZY(671,"ZY"),
	_ZZ(672,"ZZ");
	
	private long id;
	private String value;
	

}

