package hsbc.aofe.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SpecialInstructionIndicator {
	
	ADVANCE(1, "J"),
	HML_BUNDLING(2, "H"),
	OTHERS(3, "N"),
	BUNDLED_PRODUCT(4, "B"),
	SECURED_CREDIT_CARD(5, "S"),
	VIP(6, "V"),
	SURVIVAL_CREDIT_CARD(7, "SC");
	
	
	private long id;
	private String value;
}
