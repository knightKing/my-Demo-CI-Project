package hsbc.aofe.domain;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Title {
	
	DOCTOR(1, "DOCTOR"), MADAM(2, "MADAM"), MISS(3, "MISS"), MR(4, "MR"), MRS(5, "MRS"), MS(6, "MS"), Other(7, "Other");

	private int code;
	
	private String value;

}
