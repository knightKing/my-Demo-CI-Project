package hsbc.aofe.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

import org.javatuples.Triplet;

import lombok.Data;

@Data
public class TransformedData {
	Map<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData;
	
	String cpfAccountNumber;
}
