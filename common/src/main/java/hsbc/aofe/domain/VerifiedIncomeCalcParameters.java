package hsbc.aofe.domain;

import lombok.Data;

import java.time.LocalDate;

@Data
public class VerifiedIncomeCalcParameters {

	private LocalDate dateOfBirth;
	private String nationality;
	private String idValue;
	private String employmenttype;
	private LocalDate dateOfIssue;
}
