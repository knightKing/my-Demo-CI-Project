package hsbc.aofe.exception;

import lombok.Data;

@Data
public class AOException extends RuntimeException {

	private static final long serialVersionUID = 3820304502172576219L;
	
	private String errorString;
	private int errorCode;

	public AOException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public AOException(final String message) {
		super(message);
	}
}
