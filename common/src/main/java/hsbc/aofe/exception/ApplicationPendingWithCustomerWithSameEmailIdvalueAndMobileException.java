package hsbc.aofe.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ApplicationPendingWithCustomerWithSameEmailIdvalueAndMobileException extends RuntimeException {/**
	 * 
	 */
	private static final long serialVersionUID = -1092200251787720584L;

	private String field;
	
}
