package hsbc.aofe.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ApplicationSubmittedToAOException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3823108532169118717L;
	private String field;

}
