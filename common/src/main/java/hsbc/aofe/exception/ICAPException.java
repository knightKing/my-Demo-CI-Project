package hsbc.aofe.exception;

public class ICAPException extends Exception {

    public ICAPException(String message) {
        super(message);
    }
}
