package hsbc.aofe.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class IccmRequestTimedOutException extends RuntimeException {
	/**
	* 
	*/
	private static final long serialVersionUID = 5215192928054084731L;

	private String field;

}
