package hsbc.aofe.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InvalidOtpException extends RuntimeException {/**
	 * 
	 */
	private static final long serialVersionUID = -822813841376381774L;

	private String field;
	
}
