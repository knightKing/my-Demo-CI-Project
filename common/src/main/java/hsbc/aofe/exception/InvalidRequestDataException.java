package hsbc.aofe.exception;

/*****************************************
 * Created by sahil.verma on 7/1/2017.
 *****************************************
 */
public class InvalidRequestDataException extends Exception {

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public InvalidRequestDataException(String message) {
        super(message);
    }

}
