package hsbc.aofe.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OcrException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	String message;

}
