package hsbc.aofe.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProbableRoboticSpamException  extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5512763067402338571L;
	/**
	 * 
	 */
	private String field;

}