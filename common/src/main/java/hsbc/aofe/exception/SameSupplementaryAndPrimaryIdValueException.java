package hsbc.aofe.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SameSupplementaryAndPrimaryIdValueException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4806795102718344891L;
	
	private String field;

}
