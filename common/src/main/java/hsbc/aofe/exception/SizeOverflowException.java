package hsbc.aofe.exception;

public class SizeOverflowException extends RuntimeException {

    private static final long serialVersionUID = 3820304502172576212L;

    /**
     * Constructs a new uploaded input files size overflow exception with the specified detail message and cause.
     * <p>
     * Note that the detail message associated with cause is not automatically incorporated in this runtime exception's detail
     * message.
     * </p>
     *
     * @param message the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     * @param cause   the cause (which is saved for later retrieval by the {@link #getCause()} method). (A null value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public SizeOverflowException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new uploaded input files size overflow exception with the specified detail message.
     *
     * @param message the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     */
    public SizeOverflowException(final String message) {
        super(message);
    }
}
