package hsbc.aofe.service;

import java.io.IOException;
import java.util.List;

import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.databind.JsonNode;

import hsbc.aofe.domain.Applicant;
import hsbc.aofe.exception.InvalidRequestDataException;

public interface ApplicantService {

    public Applicant findApplicantById(Long id);

    public List<Applicant> findAllApplicants();

    public Applicant updateApplicant(String arn, Applicant applicant);
    
    public Applicant updateBeneficiary(String arn, Applicant applicant);

    public void deleteApplicantById(Long id);

    public List<Applicant> saveApplicant(List<Applicant> applicants, String arn);

    Applicant savePrimaryApplicant(String arn, JsonNode patchToApply, BindingResult bindingResult, String channel) throws IOException, InvalidRequestDataException;

	public void deleteSuppApplicant(String arn, int index);

	public void deleteAllSuppApplicants(String arn);

}
