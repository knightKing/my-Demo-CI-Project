package hsbc.aofe.service;

import com.fasterxml.jackson.databind.JsonNode;
import hsbc.aofe.domain.*;
import hsbc.aofe.exception.ICAPException;
import hsbc.aofe.exception.InvalidRequestDataException;
import hsbc.aofe.exception.OcrException;
import hsbc.aofe.exception.SizeOverflowException;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public interface ApplicationService {

    public List<Application> findAllApplications();

    public Application createApplication(String uid, String channelAuthority, Application application);

    public void deleteApplicationById(String arn);

    public Application findApplicationByARN(String arn);

    public List<Document> getPrimaryApplicantDocuments(String arn);

    public Image getPrimaryApplicantImages(String arn, String documentgroup, long index);

    public List<Document> getSuppApplicantDocBySuppId(String arn, long supplimentaryId);

    public Image getSuppApplicantImageById(String arn, long supplimentaryId, long index, String documentGroup);

    public List<Document> createPrimaryApplDoc(String arn, String documentGroup, List<Document> documents,HttpSession session) throws SizeOverflowException, OcrException, IOException, ICAPException;

    public void deletePrimaryApplDoc(String arn, String documentGroup, long index);

    public void deleteSupplementaryApplDoc(String arn, long supplementaryId, String documentGroup, long imageIndex);

    public void updateSupplementaryApplDoc(String idOfApplication, long supplementaryId, String documentGroup, List<Document> documents) throws SizeOverflowException, IOException, ICAPException;

    void updateApplicationStatus(String arn, NgStatus status, String uid, String channel);

    Application updateApplication(String arn, JsonNode patchToApply, BindingResult bindingResult, String channel) throws IOException, InvalidRequestDataException;

    public void saveStaffDetails(String arn, ApplicationStaffDetails staffDetails);

    public String getApplicationStatus(String idType, String idValue);
    
    public void applicationSaveAndExit(String arn, String uid, String channel);

    void setEssentialDataPreFilledIndicator(boolean essentialDataPreFilledIndicator,String arn);

}
