package hsbc.aofe.service;

import java.io.IOException;
import java.util.List;

import hsbc.aofe.domain.ExportDocument;

public interface CreateExcelSheetService {
	
	public ExportDocument createCompleteExcelSheet(List<String> sortList, List<String> filterList, String uid, String channelAuthority, String userName) throws IOException;

}
