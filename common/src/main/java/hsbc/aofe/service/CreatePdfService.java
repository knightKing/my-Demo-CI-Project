package hsbc.aofe.service;

import java.io.File;

import hsbc.aofe.domain.Application;

public interface CreatePdfService {
	
	public void createApplicationPdf(String arn);

}
