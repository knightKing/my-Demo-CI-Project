package hsbc.aofe.service;

import java.util.List;

import hsbc.aofe.domain.CreditCard;

/*****************************************
 * Created by sahil.verma on 6/6/2017.
 *****************************************
 */
public interface CreditCardService {

    /**
     * This will return all the credit card's available in the master-table CREDITCARDMASTER
     */
    List<CreditCard> getAllCreditCards();

	List<CreditCard> getCardsForDigitalJourney();

}
