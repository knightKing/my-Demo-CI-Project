package hsbc.aofe.service;

import java.sql.Timestamp;
import java.util.List;

import hsbc.aofe.domain.IpAccessDetails;

public interface IpAccessService {

	public void saveIpAddressDetails(String ipAddress, Timestamp accessTime, String url);
	
	public List<IpAccessDetails> getIpAddressDetails(String ipAddress);
}
