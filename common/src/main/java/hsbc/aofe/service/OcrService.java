package hsbc.aofe.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import hsbc.aofe.domain.Image;
import hsbc.aofe.domain.OcrData;
import hsbc.aofe.domain.OcrProcessedException;

public interface OcrService {

	public List<OcrData> getEcpfOcrdata(Image image,String docType,String arn,HttpSession session) throws ParserConfigurationException, SAXException,IOException;
	
	public List<Image> getOcrdData(List<Image> images, String docType);
	
	public OcrProcessedException saveMatchedEcpfCompanyName(String arn, boolean isMatched, HttpSession session);
	
}
