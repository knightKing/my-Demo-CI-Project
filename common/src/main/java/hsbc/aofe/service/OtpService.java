package hsbc.aofe.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;


public interface OtpService {

	public String otpValidate(String idType, String idValue, String dialCodeMobile, Long mobile, String email, String otp);

	public String generateOneTimePassword(String idType, String idValue, String dialCodeMobile, Long mobile, String email) 
																								throws InvalidKeyException, NoSuchAlgorithmException;
	
	public String getApplicationToResume(String idType, String idValue, String dialCodeMobile, Long mobile, String email);
	

}
