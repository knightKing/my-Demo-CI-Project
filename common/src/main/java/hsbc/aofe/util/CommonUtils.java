package hsbc.aofe.util;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created by somebody on 7/2/2017.
 */
public abstract class CommonUtils {

    private CommonUtils() {
        // prevent instantiation
    }

    public static LocalDate convertStringToLocalDate(String dateOfBirth) {
        try {
            if (!StringUtils.isEmpty(dateOfBirth)) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                return LocalDate.parse((String) dateOfBirth, formatter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static LocalDate convertStringWithDiffFormatToLocalDate(String dateOfBirth) {
        try {
            if (!StringUtils.isEmpty(dateOfBirth)) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                return LocalDate.parse((String) dateOfBirth, formatter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertLocalDateToString(LocalDate dateOfBirth) {
        try {
            if (dateOfBirth != null) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                return dateOfBirth.format(formatter);
            }
        } catch (Exception e) {
            //TODO: add logger here
            e.printStackTrace();
        }
        return null;
    }

    public static <T> Optional<T> resolveIfNull(Supplier<T> resolver) {
        try {
            T result = resolver.get();
            return Optional.ofNullable(result);
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    public static String convertToString(Boolean value) {
        BidiMap<Boolean, String> bidiMap = new DualHashBidiMap<>();
        bidiMap.put(true,"Y");
        bidiMap.put(false,"N");
        bidiMap.put(null,"X");

        return bidiMap.get(value);
    }

    public static Boolean convertToBoolean(String value) {
        BidiMap<Boolean, String> bidiMap = new DualHashBidiMap<>();
        bidiMap.put(true,"Y");
        bidiMap.put(false,"N");
        bidiMap.put(null,"X");

        return bidiMap.getKey(value);

    }
}
