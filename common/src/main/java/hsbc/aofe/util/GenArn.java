package hsbc.aofe.util;

import java.util.Random;

public class GenArn {
	int length = 6;

	public static char[] genArn(int len) {

		String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String Small_chars = "abcdefghijklmnopqrstuvwxyz";
		String numbers = "0123456789";

		String values = Capital_chars + Small_chars + numbers;

		// Using random method
		Random rndm_method = new Random();

		char[] genratedArn = new char[len];

		for (int i = 0; i < len; i++) {

			genratedArn[i] = values.charAt(rndm_method.nextInt(values.length()));

		}
		return genratedArn;
	}

}
