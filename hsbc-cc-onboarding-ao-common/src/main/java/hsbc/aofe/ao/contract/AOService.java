package hsbc.aofe.ao.contract;

public interface AOService {
	
	String initiateAoSubmission(String arn);
}
