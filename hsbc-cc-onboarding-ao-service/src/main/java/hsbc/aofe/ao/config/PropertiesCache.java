package hsbc.aofe.ao.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

//TODO :Add Javadoc

@Slf4j
public class PropertiesCache {

    private static Properties properties = new Properties();

    private PropertiesCache() {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties");
        log.debug("*** Loading...... properties from properties file. ***");
        try {
            properties.load(in);
        } catch (IOException e) {
            log.debug("Exception occured whlie loading properties file.");
            e.printStackTrace();
        }
    }

    /* using Bill pugh solution. */
    private static class LazyHolder {
        private static final PropertiesCache INSTANCE = new PropertiesCache();
    }

    public static PropertiesCache getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * Get property by a single key
     *
     * @param key
     * @return
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * get all the properties available
     *
     * @return
     */
    public Set<String> getAllPropertyNames() {
        return properties.stringPropertyNames();
    }

    /**
     * check whether properties contains this key
     *
     * @param key
     * @return
     */
    public boolean containsKey(String key) {
        return properties.containsKey(key);
    }

}

