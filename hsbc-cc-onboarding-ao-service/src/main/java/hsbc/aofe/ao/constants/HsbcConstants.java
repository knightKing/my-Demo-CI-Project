package hsbc.aofe.ao.constants;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.AVR;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.DU6;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SVI;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.VP1;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.VPC;

public interface HsbcConstants {

    interface Parameters {
        interface Card {
            interface Common {
                String ESDS_TOKEN = "0";
                String WIRE_ACTION = "SendActionGAD";
                String CONTINUE_TO_NEXT_PAGE = "Continue";
                String SUBMIT_IDCL = "submit";
                String PREFERRED_LANGUAGE = "en";
                String SUBMIT_PAGE_DETAILS = "1";
                String SINGAPORE_COUNTRY_CODE = "SG";
                String BLANK_VALUE = "";
                String NONE_VALUE = "NONE";
                String TRUE_VALUE = "true";
                String FALSE_VALUE = "false";
                String PREFERRED_LANGUAGE_CAPS = "ENG";
                String YES_VALUE = "Yes";
                String SINGLE_CARD = "SingleCard";
                String DUAL_CARD = "DualCard";
                String SVI="SVI";
                String VPC="VPC";
                String AVR="AVR";
                String DU6="DU6";
                String VP1="VP1";
                String BASIC_ACCOUNT_MSC1_VALUE="01000";
                String ADVANCE_ACCOUNT_MSC1_VALUE="01670";
                String PREMIER_ACCOUNT_MSC1_VALUE="01680";
                String REVOLUTION_ACCOUNT_MSC1_VALUE="01400";
                String BASIC_ACCOUNT_IPSEG_VALUE="1000";
                String ADVANCE_ACCOUNT_IPSEG_VALUE="1040";
                String PREMIER_ACCOUNT_IPSEG_VALUE="1080";
                String REVOLUTION_ACCOUNT_IPSEG_VALUE="1000";
                String balanceTxSchemeMap_AVR="30130";
        		String balanceTxSchemeMap_DU6="30109";
        		String balanceTxSchemeMap_SVI="30109";
        		String balanceTxSchemeMap_VP1="30130";
        		String balanceTxSchemeMap_VPC="30109";

            }

            interface Single {
                interface AoEntry {
                    String HSBC_OHD_BUNDLE_ID = "00000";
                    String HSBC_OHD_CURRENCY_CODE = "SGD";
                    String FLOW_ID = "AOSGPCC001";
                    String LAUNCH_COMMAND = "SCM";
                    String SITE_ID = "2G_SGH_U";
                    String DEFAULT_PROMOTION_CODE = "DEF0000001";
                    String FLOW_ID_DUAL = "AOSGPCC005";
                }

                interface PersonalDetails {
                    String JSF_SEQUENCE = "1";
                    String USER_PREFS = "";
                }

                interface EmploymentDetails {
                    String ANNUAL_SALARY_CURRENCY = "SGD";
                    String JSF_SEQUENCE = "2";
                    String SEND_ACTION_GAD = "SendActionGAD";
                }

                interface SupplementaryCardDetails {
                    String JSF_SEQUENCE = "6";
                    String MAX_PERMISSIBLE_CARD_HOLDERS = "9";
                    String BLANK_FOR_NO_SUPP_CARD = "";
                    String DEFAULT_TRUE_FOR_NO_SUPP_CARD = "true";
                    String DEFAULT_NONE_FOR_NO_SUPP_CARD = "NONE";
                    String DEFAULT_SEC_CARD_COUNTER = "1";

                }

                interface ReviewDetails {
                    String UIC = "";
                    String WIRE_ACTION = "SendActionGAD";
                    String PREFERRED_LANGUAGE = "ENG";
                    String CONSENT_ON = "on";
                    String CONTINUE_SUBMIT_FOR_APPROVAL = "Submit for Approval";
                    String JSF_SEQUENCE = "5";
                }

                interface DecisionConfigureCardDetails {
                    String WIRE_ACTION = "SendActionProducConfig";
                    String JSF_SEQUENCE = "1";
                    String DISPATCH_METHOD_EMAIL = "EMAIL";

                }

            }
        }
    }
}
