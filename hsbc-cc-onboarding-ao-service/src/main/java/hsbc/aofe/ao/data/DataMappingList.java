package hsbc.aofe.ao.data;

import java.util.List;

import org.apache.http.NameValuePair;

public class DataMappingList {

	public List<NameValuePair> commonValuesList;
	public List<NameValuePair> staticValuesList;
	public List<NameValuePair> dynamicValuesList;
	public List<NameValuePair> userValuesList;
	public List<NameValuePair> nonMandatoryValuesList;
	
	public List<NameValuePair> getCommonValuesList() {
		return commonValuesList;
	}
	public void setCommonValuesList(List<NameValuePair> commonValuesList) {
		this.commonValuesList = commonValuesList;
	}
	public List<NameValuePair> getStaticValuesList() {
		return staticValuesList;
	}
	public void setStaticValuesList(List<NameValuePair> staticValuesList) {
		this.staticValuesList = staticValuesList;
	}
	public List<NameValuePair> getDynamicValuesList() {
		return dynamicValuesList;
	}
	public void setDynamicValuesList(List<NameValuePair> dynamicValuesList) {
		this.dynamicValuesList = dynamicValuesList;
	}
	public List<NameValuePair> getUserValuesList() {
		return userValuesList;
	}
	public void setUserValuesList(List<NameValuePair> userValuesList) {
		this.userValuesList = userValuesList;
	}
	public List<NameValuePair> getNonMandatoryValuesList() {
		return nonMandatoryValuesList;
	}
	public void setNonMandatoryValuesList(List<NameValuePair> nonMandatoryValuesList) {
		this.nonMandatoryValuesList = nonMandatoryValuesList;
	}
	
	
	
}
