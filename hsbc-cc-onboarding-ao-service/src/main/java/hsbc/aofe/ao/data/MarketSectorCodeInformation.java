package hsbc.aofe.ao.data;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MarketSectorCodeInformation {
	
	//basic,advance,premier,revolution
	private String type;
	private String msc1Code;
	private int priority;
	private List<String> products;
	private String ipSegType;
	
	

}
