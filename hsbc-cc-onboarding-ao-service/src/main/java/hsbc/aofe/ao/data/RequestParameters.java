package hsbc.aofe.ao.data;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;

import lombok.extern.slf4j.Slf4j;

//TODO :Add Javadoc
@Slf4j
public class RequestParameters {

	
	public String edsToken;
	public String url;
	public String cookies;
	public String cbaProductTrackerIDListString;
	public String cardTypeList;
	public String productName;
	public String supplementaryCard;
	public String dispatchMethod;
	public String productList1;
	public HttpContext localContext;
	public HttpClientBuilder client;
	public String aoApplicationReferenceNumber="";
	public String redirectionUrl;
	public String responseStepAoEntryDetails;
	public String responseStep1PersonalDetails;
	public String responseStep2EmploymentDetails;
	public String responseStep3SupplementaryDetails;
	public String responseStep4ReviewDetails;
	public String responseStep5DecisionDetails;
	public String responseRedirectStep1;
	public String responseRedirectStep5;
	public String responseRedirectStep6;
	public String responseContinueJs;
	//Dual card 
	public String responseStep5BothDecisionDetails;
	public String responseStep5Card1DecisionDetails;
	public String responseStep5Card2DecisionDetails;
	public String responseRedirectStep5Card1;
	public String responseRedirectStep5Card2;
	public String numberOfCards;
	public String productList1Second;
	public String productNameSecond;
	public String supplementaryCardSecond;
	public String firstCard;
	public String secondCard;
	public String firstCardForBalanceTransfer;
	public String secondCardForBalanceTransfer;
	public boolean dispatchMethodDisableCheck;
	public String singleCardForSupplementary;
	public String singleCardSecondForSupplementary;
	public boolean creditCardDeclineCheck;
	public String channelFlag;
	public String promoCode;
	
	
	
	public String getChannelFlag() {
		return channelFlag;
	}

	public void setChannelFlag(String channelFlag) {
		this.channelFlag = channelFlag;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	
	
	public boolean isCreditCardDeclineCheck() {
		return creditCardDeclineCheck;
	}

	public void setCreditCardDeclineCheck(boolean creditCardDeclineCheck) {
		this.creditCardDeclineCheck = creditCardDeclineCheck;
	}

	public String getSingleCardForSupplementary() {
		return singleCardForSupplementary;
	}

	public void setSingleCardForSupplementary(String singleCardForSupplementary) {
		this.singleCardForSupplementary = singleCardForSupplementary;
	}

	public String getSingleCardSecondForSupplementary() {
		return singleCardSecondForSupplementary;
	}

	public void setSingleCardSecondForSupplementary(String singleCardSecondForSupplementary) {
		this.singleCardSecondForSupplementary = singleCardSecondForSupplementary;
	}

	public boolean getDispatchMethodDisableCheck() {
		return dispatchMethodDisableCheck;
	}

	public void setDispatchMethodDisableCheck(boolean dispatchMethodDisableCheck) {
		this.dispatchMethodDisableCheck = dispatchMethodDisableCheck;
	}

	public String getFirstCardForBalanceTransfer() {
		return firstCardForBalanceTransfer;
	}

	public void setFirstCardForBalanceTransfer(String firstCardForBalanceTransfer) {
		this.firstCardForBalanceTransfer = firstCardForBalanceTransfer;
	}

	public String getSecondCardForBalanceTransfer() {
		return secondCardForBalanceTransfer;
	}

	public void setSecondCardForBalanceTransfer(String secondCardForBalanceTransfer) {
		this.secondCardForBalanceTransfer = secondCardForBalanceTransfer;
	}

	
	public String getFirstCard() {
		return firstCard;
	}

	public void setFirstCard(String firstCard) {
		this.firstCard = firstCard;
	}

	public String getSecondCard() {
		return secondCard;
	}

	public void setSecondCard(String secondCard) {
		this.secondCard = secondCard;
	}
	
	
	
	public String getSupplementaryCardSecond() {
		return supplementaryCardSecond;
	}

	public void setSupplementaryCardSecond(String supplementaryCardSecond) {
		this.supplementaryCardSecond = supplementaryCardSecond;
	}

	public String getProductNameSecond() {
		return productNameSecond;
	}

	public void setProductNameSecond(String productNameSecond) {
		this.productNameSecond = productNameSecond;
	}


	public String getProductList1Second() {
		return productList1Second;
	}

	public void setProductList1Second(String productList1Second) {
		this.productList1Second = productList1Second;
	}

	public String getCardTypeListSecond() {
		return cardTypeListSecond;
	}

	public void setCardTypeListSecond(String cardTypeListSecond) {
		this.cardTypeListSecond = cardTypeListSecond;
	}
	public String cardTypeListSecond;
	
	public String getNumberOfCards() {
		return numberOfCards;
	}

	public void setNumberOfCards(String numberOfCards) {
		this.numberOfCards = numberOfCards;
	}

	public String getResponseStep5BothDecisionDetails() {
		return responseStep5BothDecisionDetails;
	}

	public void setResponseStep5BothDecisionDetails(String responseStep5BothDecisionDetails) {
		this.responseStep5BothDecisionDetails = responseStep5BothDecisionDetails;
	}

	public String getResponseStep5Card1DecisionDetails() {
		return responseStep5Card1DecisionDetails;
	}

	public void setResponseStep5Card1DecisionDetails(String responseStep5Card1DecisionDetails) {
		this.responseStep5Card1DecisionDetails = responseStep5Card1DecisionDetails;
	}

	public String getResponseStep5Card2DecisionDetails() {
		return responseStep5Card2DecisionDetails;
	}

	public void setResponseStep5Card2DecisionDetails(String responseStep5Card2DecisionDetails) {
		this.responseStep5Card2DecisionDetails = responseStep5Card2DecisionDetails;
	}

	public String getResponseRedirectStep5Card1() {
		return responseRedirectStep5Card1;
	}

	public void setResponseRedirectStep5Card1(String responseRedirectStep5Card1) {
		this.responseRedirectStep5Card1 = responseRedirectStep5Card1;
	}

	public String getResponseRedirectStep5Card2() {
		return responseRedirectStep5Card2;
	}

	public void setResponseRedirectStep5Card2(String responseRedirectStep5Card2) {
		this.responseRedirectStep5Card2 = responseRedirectStep5Card2;
	}
	public String responseContinueSecondJs;
	

	
	public String getResponseContinueJs() {
		return responseContinueJs;
	}

	public void setResponseContinueJs(String responseContinueJs) {
		this.responseContinueJs = responseContinueJs;
	}

	public String getResponseContinueSecondJs() {
		return responseContinueSecondJs;
	}

	public void setResponseContinueSecondJs(String responseContinueSecondJs) {
		this.responseContinueSecondJs = responseContinueSecondJs;
	}

	public String getResponseRedirectStep1() {
		return responseRedirectStep1;
	}

	public void setResponseRedirectStep1(String responseRedirectStep1) {
		this.responseRedirectStep1 = responseRedirectStep1;
	}

	public String getResponseRedirectStep5() {
		return responseRedirectStep5;
	}

	public void setResponseRedirectStep5(String responseRedirectStep5) {
		this.responseRedirectStep5 = responseRedirectStep5;
	}

	public String getResponseRedirectStep6() {
		return responseRedirectStep6;
	}

	public void setResponseRedirectStep6(String responseRedirectStep6) {
		this.responseRedirectStep6 = responseRedirectStep6;
	}

	public String getResponseStepAoEntryDetails() {
		return responseStepAoEntryDetails;
	}

	public void setResponseStepAoEntryDetails(String responseStepAoEntryDetails) {
		this.responseStepAoEntryDetails = responseStepAoEntryDetails;
	}

	public String getResponseStep1PersonalDetails() {
		return responseStep1PersonalDetails;
	}

	public void setResponseStep1PersonalDetails(String responseStep1PersonalDetails) {
		this.responseStep1PersonalDetails = responseStep1PersonalDetails;
	}

	public String getResponseStep2EmploymentDetails() {
		return responseStep2EmploymentDetails;
	}

	public void setResponseStep2EmploymentDetails(String responseStep2EmploymentDetails) {
		this.responseStep2EmploymentDetails = responseStep2EmploymentDetails;
	}

	public String getResponseStep3SupplementaryDetails() {
		return responseStep3SupplementaryDetails;
	}

	public void setResponseStep3SupplementaryDetails(String responseStep3SupplementaryDetails) {
		this.responseStep3SupplementaryDetails = responseStep3SupplementaryDetails;
	}

	public String getResponseStep4ReviewDetails() {
		return responseStep4ReviewDetails;
	}

	public void setResponseStep4ReviewDetails(String responseStep4ReviewDetails) {
		this.responseStep4ReviewDetails = responseStep4ReviewDetails;
	}

	
	
	
	
	public String getResponseStep5DecisionDetails() {
		return responseStep5DecisionDetails;
	}

	public void setResponseStep5DecisionDetails(String responseStep5DecisionDetails) {
		this.responseStep5DecisionDetails = responseStep5DecisionDetails;
	}

	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String step1AoRedirectionUrl) {
		redirectionUrl = step1AoRedirectionUrl;
	}

	public String getAoApplicationReferenceNumber() {
		return aoApplicationReferenceNumber;
	}

	public void setAoApplicationReferenceNumber(String aoApplicationReferenceNumber) {
		this.aoApplicationReferenceNumber = aoApplicationReferenceNumber;
	}

	public HttpContext getLocalContext() {
		return localContext;
	}

	public void setLocalContext(HttpContext localContext) {
		this.localContext = localContext;
	}

	public HttpClientBuilder getClient() {
		return client;
	}

	public void setClient(HttpClientBuilder client) {
		this.client = client;
	}

	public  String getDispatchMethod() {
		return dispatchMethod;
	}

	public  void setDispatchMethod(String dispatchMethod) {
		this.dispatchMethod = dispatchMethod;
	}

	public  String getProductName() {
		return productName;
	}

	public  void setProductName(String productName) {
		this.productName = productName;
	}

	public  String getSupplementaryCard() {
		return supplementaryCard;
	}

	public  void setSupplementaryCard(String supplementaryCard) {
		this.supplementaryCard = supplementaryCard;
	}

	public  String getEdsToken() {
		return edsToken;
	}

	public  void setEdsToken(String edsToken) {
		this.edsToken = edsToken;
	}

	public  String getUrl() {
		return url;
	}

	public  void setUrl(String url) {
		this.url = url;
	}

	public String getCookies() {
		return cookies;
	}

	public void setCookies(String cookies) {
		this.cookies = cookies;
	}
	public  String getProductList1() {
		return productList1;
	}
	public  void setProductList1(String productList1) {
		this.productList1 = productList1;
	}

	public  String getCbaProductTrackerIDListString() {
		return cbaProductTrackerIDListString;
	}
	public  void setCbaProductTrackerIDListString(String cbaProductTrackerIDListString) {
		this.cbaProductTrackerIDListString = cbaProductTrackerIDListString;
	}
	
	public  String getCardTypeList() {
		return cardTypeList;
	}
	public  void setCardTypeList(String cardTypeList) {
		this.cardTypeList = cardTypeList;
	}
	
	
}
