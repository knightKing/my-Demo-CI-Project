package hsbc.aofe.ao.data;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.domain.*;
import hsbc.aofe.util.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.*;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.*;

@Slf4j
public class Step3SupplementaryDataDecorator {

    public List<NameValuePair> dataDecorator(RequestParameters params, Application application) {

        List<NameValuePair> nvpList = new ArrayList<>();
        int noOfSupp = 0;
        SuppApplicants suppApplicant = null;

        if (!CollectionUtils.isEmpty(application.getSupplementaryApplicant())) {
            noOfSupp = application.getSupplementaryApplicant().size();
            suppApplicant = new SuppApplicants();
        } else {
            suppApplicant = new SuppApplicants();
        }

        if (1 == noOfSupp) {
            Applicant firstSupplementary = application.getSupplementaryApplicant().get(0);
            supplementaryAddressCheck(firstSupplementary, application);
            populateFirstSupplimentry(suppApplicant, firstSupplementary);
        } else if (2 == noOfSupp) {
            Applicant firstSupp = application.getSupplementaryApplicant().get(0);
            supplementaryAddressCheck(firstSupp, application);
            populateFirstSupplimentry(suppApplicant, firstSupp);
            
            Applicant secondSupp = application.getSupplementaryApplicant().get(1);
            supplementaryAddressCheck(secondSupp, application);
            populateSecondSupplimentry(suppApplicant, secondSupp);
            
        } else if (3 == noOfSupp) {
            Applicant firstSupp = application.getSupplementaryApplicant().get(0);
            supplementaryAddressCheck(firstSupp, application);
            populateFirstSupplimentry(suppApplicant, firstSupp);
            

            Applicant secondSupp = application.getSupplementaryApplicant().get(1);
            supplementaryAddressCheck(secondSupp, application);
            populateSecondSupplimentry(suppApplicant, secondSupp);
            
            Applicant thirdSupp = application.getSupplementaryApplicant().get(2);
            supplementaryAddressCheck(thirdSupp, application);
            populateThirdSupplimentry(suppApplicant, thirdSupp);
            
        }

        PropertiesCache propertiesCache = PropertiesCache.getInstance();
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.first.name"), suppApplicant.getUser_scd_first_name()));//TODO :Value :SunitaDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.first.name1"), suppApplicant.getUser_scd_first_name1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.first.name2"), suppApplicant.getUser_scd_first_name2()));//TODO :Value :DEFAULT :


        //nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.last.name"),""));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.last.name"), suppApplicant.getUser_scd_last_name()));//TODO :Value :BhambaniDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.last.name1"), suppApplicant.getUser_scd_last_name1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.last.name2"), suppApplicant.getUser_scd_last_name2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.middle.name"), suppApplicant.getUser_scd_middle_name()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.middle.name1"), suppApplicant.getUser_scd_middle_name1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.middle.name2"), suppApplicant.getUser_scd_middle_name2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.max.permissible.card.holders"), MAX_PERMISSIBLE_CARD_HOLDERS));//TODO :Value :9DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.secondary.counter"), suppApplicant.getUser_scd_secondary_counter()));//TODO :CHECK TPSAValue :1DEFAULT :
        //nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.secondary.cardholder.indicator"), application.getAddSupplimentaryCard()==false||application.getAddSupplimentaryCard()==null?"N":"Y"));//TODO :A:Value Y/NValue :N FOR NO SUPPLEMENTARY CARDDEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.secondary.cardholder.indicator"), CollectionUtils.isEmpty(application.getSupplementaryApplicant()) ? "N" : "Y"));
        //nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.secondary.cardholder.indicator"),"N"));

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.type"), suppApplicant.getUser_scd_identification_type()));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.type.1"), suppApplicant.getUser_scd_identification_type_1()));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.type.2"), suppApplicant.getUser_scd_identification_type_2()));//TODO :Value :SunitaDEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.months"), suppApplicant.getUser_scd_dob_months()));//TODO :Value :NovemberDEFAULT : NONE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.date"), suppApplicant.getUser_scd_dob_date()));//TODO :Value :12DEFAULT : NONE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.year"), suppApplicant.getUser_scd_dob_year())); //TODO :Value :1970DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.cardholder.name"), suppApplicant.getUser_scd_cardholder_name()));//TODO :Value :Sunita BhambaniDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.cardholder.name.1"), suppApplicant.getUser_scd_cardholder_name_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.cardholdername.2"), suppApplicant.getUser_scd_cardholdername_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.nationality"), suppApplicant.getUser_scd_nationality()));//TODO :Value :SGDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.nationality.1"), suppApplicant.getUser_scd_nationality_1()));//TODO :Value :SGDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.nationality.2"), suppApplicant.getUser_scd_nationality_2()));//TODO :Value :SGDEFAULT :

        // check value home number only 1 and 2 there
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.homenumber.area.1"), suppApplicant.getUser_scd_homenumber_area_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.homenumber.area.2"), suppApplicant.getUser_scd_homenumber_area_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.homenumber.int.1"), suppApplicant.getUser_scd_homenumber_int_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.homenumber.int.2"), suppApplicant.getUser_scd_homenumber_int_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.homenumber.no1"), suppApplicant.getUser_scd_homenumber_no1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.homenumber.no2"), suppApplicant.getUser_scd_homenumber_no2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.title"), suppApplicant.getUser_scd_title()));//TODO :Value :MRSDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.title1"), suppApplicant.getUser_scd_title1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.title2"), suppApplicant.getUser_scd_title2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.sec.secondary.title.other"), suppApplicant.getUser_scd_sec_secondary_title_other()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.thirdtitleother"), suppApplicant.getUser_scd_thirdtitleother()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.titleother"), suppApplicant.getUser_scd_titleother()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.month.1"), suppApplicant.getUser_scd_dob_month_1()));//TODO :Value :NONEDEFAULT : NONE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.date.1"), suppApplicant.getUser_scd_dob_date_1()));//TODO :Value :NONEDEFAULT : NONE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.year.1"), suppApplicant.getUser_scd_dob_year_1())); //TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identication.issue.country.code"), suppApplicant.getUser_scd_identication_issue_country_code()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identication.issue.country.code.1"), suppApplicant.getUser_scd_identication_issue_country_code_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identication.issue.country.code.2"), suppApplicant.getUser_scd_identication_issue_country_code_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.number"), suppApplicant.getUser_scd_identification_number()));//TODO :Value :S7416076ZDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.number.1"), suppApplicant.getUser_scd_identification_number_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.number.2"), suppApplicant.getUser_scd_identification_number_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.type"), suppApplicant.getUser_scd_identification_type()));//TODO :Value :IDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.type.1"), suppApplicant.getUser_scd_identification_type_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.identification.type.2"), suppApplicant.getUser_scd_identification_type_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.1"), suppApplicant.getUser_scd_address_line_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.1.1"), suppApplicant.getUser_scd_address_line_1_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.1.2"), suppApplicant.getUser_scd_address_line_1_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.2"), suppApplicant.getUser_scd_address_line_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.2.1"), suppApplicant.getUser_scd_address_line_2_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.2.2"), suppApplicant.getUser_scd_address_line_2_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.3"), suppApplicant.getUser_scd_address_line_3()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.3.1"), suppApplicant.getUser_scd_address_line_3_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.address.line.3.2"), suppApplicant.getUser_scd_address_line_3_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.country"), suppApplicant.getUser_scd_country()));//TODO :Value :SGDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.country.1"), suppApplicant.getUser_scd_country_1()));//TODO :Value :SGDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.country.2"), suppApplicant.getUser_scd_country_2()));//TODO :Value :SGDEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.zipcode"), suppApplicant.getUser_scd_zipcode()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.zipcode.1"), suppApplicant.getUser_scd_permanent_zipcode_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.zipcode.2"), suppApplicant.getUser_scd_zipcode_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.month.2"), suppApplicant.getUser_scd_dob_month_2()));//TODO :Value :NONEDEFAULT : NONE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.dob.date.2"), suppApplicant.getUser_scd_dob_date_2()));//TODO :Value :NONEDEFAULT : NONE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.year.2"), suppApplicant.getUser_scd_dob_year_2())); //TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.gender"), suppApplicant.getUser_scd_gender()));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.gender.1"), suppApplicant.getUser_scd_gender_1()));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.gender.2"), suppApplicant.getUser_scd_gender_2()));//TODO :Value :FDEFAULT :

        //default value blank
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.have.my.hpr.status.2"), suppApplicant.getUser_scd_have_my_hpr_status_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.relationship.bank"), suppApplicant.getUser_scd_relationship_bank()));//TODO :Value :NDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.relationship.bank.1"), suppApplicant.getUser_scd_relationship_bank_1()));//TODO :Value :NDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.relationship.bank.2"), suppApplicant.getUser_scd_relationship_bank_2()));//TODO :Value :NDEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.relationship.bank.details"), suppApplicant.getUser_scd_relationship_bank_details()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.relationship.bank.details.1"), suppApplicant.getUser_scd_relationship_bank_details_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.relationship.bank.details.2"), suppApplicant.getUser_scd_relationship_bank_details_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.first.relationship.primary.app"), suppApplicant.getUser_scd_first_relationship_primary_app()));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.second.relationship.primary.app"), suppApplicant.getUser_scd_second_relationship_primary_app()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.third.relationship.primary.app"), suppApplicant.getUser_scd_third_relationship_primary_app()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.area.code"), suppApplicant.getUser_scd_mobile_number_area_code()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.area.code.1"), suppApplicant.getUser_scd_mobile_number_area_code_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.area.code.2"), suppApplicant.getUser_scd_mobile_number_area_code_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.int"), suppApplicant.getUser_scd_mobile_number_int()));//TODO :Value :91DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.int.1"), suppApplicant.getUser_scd_mobile_number_int_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.int.2"), suppApplicant.getUser_scd_mobile_number_int_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.no"), suppApplicant.getUser_scd_mobile_number_no()));//TODO :Value :9871235640DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.no.1"), suppApplicant.getUser_scd_mobile_number_no_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.mobile.number.no.2"), suppApplicant.getUser_scd_mobile_number_no_2()));//TODO :Value :DEFAULT :

        //blank value
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.area.code"), suppApplicant.getUser_scd_office_number_area_code()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.area.code.1"), suppApplicant.getUser_scd_office_number_area_code_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.area.code.2"), suppApplicant.getUser_scd_office_number_area_code_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.int"), suppApplicant.getUser_scd_office_number_int()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.int.1"), suppApplicant.getUser_scd_office_number_int_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.int.2"), suppApplicant.getUser_scd_office_number_int_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.no"), suppApplicant.getUser_scd_office_number_no()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.no.1"), suppApplicant.getUser_scd_office_number_no_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.office.number.no.2"), suppApplicant.getUser_scd_office_number_no_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.former.last.name"), suppApplicant.getUser_scd_former_last_name()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.former.last.name.1"), suppApplicant.getUser_scd_former_last_name_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.former.last.name.2"), suppApplicant.getUser_scd_former_last_name_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.have.any.former.names"), suppApplicant.getUser_scd_haveany_former_names()));//TODO :Value :NDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.have.any.former.names.1"), suppApplicant.getUser_scd_haveany_former_names_1()));//TODO :Value :NDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.have.any.former.names.2"), suppApplicant.getUser_scd_haveany_former_names_2()));//TODO :Value :NDEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.other.last.name"), suppApplicant.getUser_scd_other_last_name()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.other.last.name.1"), suppApplicant.getUser_scd_other_last_name_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.other.last.name.2"), suppApplicant.getUser_scd_other_last_name_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.supplementary.declaration.accept.box"), suppApplicant.getUser_scd_supplementary_declaration_accept_box()));//TODO :Value :TRUEDEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.company.name"), suppApplicant.getUser_scd_company_name()));//TODO :Value :BVB Asma SchoolDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.company.name.1"), suppApplicant.getUser_scd_company_name_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.company.name.2"), suppApplicant.getUser_scd_company_name_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.employment.status"), suppApplicant.getUser_scd_employment_status()));//TODO :Value :FDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.employment.status.1"), suppApplicant.getUser_scd_employment_status_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.employment.status.2"), suppApplicant.getUser_scd_employment_status_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.occupation"), suppApplicant.getUser_scd_occupation()));//TODO :Value :58DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.occupation.1"), suppApplicant.getUser_scd_occupation_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.occupation.2"), suppApplicant.getUser_scd_occupation_2()));//TODO :Value :DEFAULT :


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.addr.indicator"), suppApplicant.getUser_scd_permanent_addr_indicator()));//TODO :Value :TRUEDEFAULT : TRUE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.addr.indicator.1"), suppApplicant.getUser_scd_permanent_addr_indicator_1()));//TODO :Value :TRUEDEFAULT : TRUE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.addr.indicator.2"), suppApplicant.getUser_scd_permanent_addr_indicator_2()));//TODO :Value :TRUEDEFAULT : TRUE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.1"), suppApplicant.getUser_scd_permanent_address_line_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.1.1"), suppApplicant.getUser_scd_permanent_address_line_1_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.1.2"), suppApplicant.getUser_scd_permanent_address_line_1_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.2"), suppApplicant.getUser_scd_permanent_address_line_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.2.1"), suppApplicant.getUser_scd_permanent_address_line_2_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.2.2"), suppApplicant.getUser_scd_permanent_address_line_2_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.3"), suppApplicant.getUser_scd_permanent_address_line_3()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.3.1"), suppApplicant.getUser_scd_permanent_address_line_3_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.address.line.3.2"), suppApplicant.getUser_scd_permanent_address_line_3_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.country"), suppApplicant.getUser_scd_permanent_country()));//TODO :Value :SGDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.country.1"), suppApplicant.getUser_scd_permanent_country_1()));//TODO :Value :SGDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.country.2"), suppApplicant.getUser_scd_permanent_country_2()));//TODO :Value :SGDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line.1"), suppApplicant.getUser_scd_permanent_foreign_address_line_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line.1.1"), suppApplicant.getUser_scd_permanent_foreign_address_line_1_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line1.2"), suppApplicant.getUser_scd_permanent_foreign_address_line1_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line.2"), suppApplicant.getUser_scd_permanent_foreign_address_line_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line.2.1"), suppApplicant.getUser_scd_permanent_foreign_address_line_2_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line.2.2"), suppApplicant.getUser_scd_permanent_foreign_address_line_2_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line.3"), suppApplicant.getUser_scd_permanent_foreign_address_line_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line.3.1"), suppApplicant.getUser_scd_permanent_foreign_address_line_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.foreign.address.line3.2"), suppApplicant.getUser_scd_permanent_foreign_address_line_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.zipcode"), suppApplicant.getUser_scd_permanent_zipcode()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.zipcode.1"), suppApplicant.getUser_scd_permanent_zipcode_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.permanent.zipcode.2"), suppApplicant.getUser_scd_permanent_zipcode_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line.1"), suppApplicant.getUser_scd_residential_foreign_address_line_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line.1.1"), suppApplicant.getUser_scd_residential_foreign_address_line_1_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line1.2"), suppApplicant.getUser_scd_residential_foreign_address_line1_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line.2"), suppApplicant.getUser_scd_residential_foreign_address_line_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line2.1"), suppApplicant.getUser_scd_residential_foreign_address_line2_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line.2.2"), suppApplicant.getUser_scd_residential_foreign_address_line_2_2()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line.3"), suppApplicant.getUser_scd_residential_foreign_address_line_1_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line3.1"), suppApplicant.getUser_scd_residential_foreign_address_line_1_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.residential.foreign.address.line3.2"), suppApplicant.getUser_scd_residential_foreign_address_line_1_1()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.first.residential.addr.indicator"), suppApplicant.getUser_scd_permanent_addr_indicator()));//TODO :Value :TRUEDEFAULT : TRUE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.first.residential.addr.indicator.1"), suppApplicant.getUser_scd_permanent_addr_indicator_1()));//TODO :Value :TRUEDEFAULT : TRUE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.first.residential.addr.indicator.2"), suppApplicant.getUser_scd_permanent_addr_indicator_2()));//TODO :Value :TRUEDEFAULT : TRUE

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.second.residential.addr.indicator"), DEFAULT_TRUE_FOR_NO_SUPP_CARD));//TODO :Value :TRUEDEFAULT : TRUE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.second.residential.addr.indicator.1"), DEFAULT_TRUE_FOR_NO_SUPP_CARD));//TODO :Value :TRUEDEFAULT : TRUE
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.second.residential.addr.indicator.2"), DEFAULT_TRUE_FOR_NO_SUPP_CARD));//TODO :Value :TRUEDEFAULT : TRUE

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.nature.of.relationship"), suppApplicant.getUser_scd_nature_of_relationship()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.nature.of.relationship.1"), suppApplicant.getUser_scd_nature_of_relationship_1()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.nature.of.relationship.2"), suppApplicant.getUser_scd_nature_of_relationship_2()));//TODO :Value :DEFAULT :

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.senior.public.office"), suppApplicant.getUser_scd_senior_public_office()));//TODO :Value :NDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.senior.public.office.1"), suppApplicant.getUser_scd_senior_public_office_1()));//TODO :Value :NDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.senior.public.office.2"), suppApplicant.getUser_scd_senior_public_office_2()));//TODO :Value :NDEFAULT :


        // telephone blank
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.telephone.area"), suppApplicant.getUser_scd_telephone_area()));//TODO :Value :DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.telephone.int"), suppApplicant.getUser_scd_telephone_int()));//TODO :Value :11DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.telephone.no"), suppApplicant.getUser_scd_telephone_no()));//TODO :Value :23235656DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.cmd.continue.review.page"), CONTINUE_TO_NEXT_PAGE));//TODO :static commonValue :ContinueDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.jsf.sequence"), JSF_SEQUENCE));//TODO :static commonValue :6DEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.idcl"), SUBMIT_IDCL));//TODO :static commonValue :submitDEFAULT :
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.scd.submit"), SUBMIT_PAGE_DETAILS));//TODO :static commonValue :1DEFAULT :
        return nvpList;
    }


    public SuppApplicants populateFirstSupplimentry(SuppApplicants suppApplicant, Applicant applicantDetails) {
        String givenName = "";
        suppApplicant.setUser_scd_middle_name(DataMappingListUtil.getValidValue(applicantDetails.getName().getMiddleName()));
        if(applicantDetails.getName() != null){
        	if(applicantDetails.getName().getFirstName() != null){
        		givenName = applicantDetails.getName().getFirstName();
        		if(applicantDetails.getName().getMiddleName() != null){
        			givenName += " "+ applicantDetails.getName().getMiddleName();
        		}
        	}
        }
        suppApplicant.setUser_scd_first_name(givenName);
        suppApplicant.setUser_scd_middle_name("");

        suppApplicant.setUser_scd_last_name(DataMappingListUtil.getValidValue(applicantDetails.getName().getLastName()));
        LocalDate suppApplicantDob = CommonUtils.convertStringToLocalDate(applicantDetails.getDateOfBirth());
        suppApplicant.setUser_scd_dob_months(DataMappingListUtil.convertMonthToCamelCaseFormat(suppApplicantDob.getMonth().toString()));
        suppApplicant.setUser_scd_dob_date(DataMappingListUtil.convertsingleDigitToTwoDigit(String.valueOf(suppApplicantDob.getDayOfMonth())));
        suppApplicant.setUser_scd_dob_year(String.valueOf(suppApplicantDob.getYear()));
        suppApplicant.setUser_scd_cardholder_name(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNameOnCard()));
        suppApplicant.setUser_scd_nationality(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNationality() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getNationality().getValue()));
        suppApplicant.setUser_scd_title(DataMappingListUtil.getValidValue(applicantDetails.getSalutation() == null ? Title.DOCTOR.getValue() : applicantDetails.getSalutation().getValue()));


        suppApplicant.setUser_scd_permanent_addr_indicator(String.valueOf(applicantDetails.getAdditionalInfo().isResidentialAddressSameAsPrimary()));

        List<Address> addressList = applicantDetails.getAdditionalInfo().getAddress();
        log.debug("Address received for Supplimentary applicant 1 : {}", addressList);

        for (Address address : addressList) {
            switch (address.getAddressType()) {
                case RESIDENTIAL:
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        suppApplicant.setUser_scd_address_line_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_address_line_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_address_line_3(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_country(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_zipcode(DataMappingListUtil.getValidValue(address.getPostalCode()));
                        suppApplicant.setUser_scd_permanent_address_line_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_permanent_address_line_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_permanent_address_line_3(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_permanent_country(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_permanent_zipcode(DataMappingListUtil.getValidValue(address.getPostalCode()));
                    } else {
                        suppApplicant.setUser_scd_country(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_residential_foreign_address_line_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_residential_foreign_address_line_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_residential_foreign_address_line_3(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line_3(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_country(DataMappingListUtil.getValidValue(DataMappingListUtil.getValidValue(address.getCountry().getValue())));
                    }
                    break;
                default:
                    break;
            }
        }


        DocumentName documentName = applicantDetails.getIdDocType();
        suppApplicant.setUser_scd_identification_type(documentName.getAoValue());

        if (documentName.equals(DocumentName.EMPLOYMENT_PASS)) {
            suppApplicant.setUser_scd_identification_number(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPassportNumber()));
            suppApplicant.setUser_scd_identication_issue_country_code(applicantDetails.getAdditionalInfo().getPassportCountry() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPassportCountry().getValue()));
        } else if (documentName.equals(DocumentName.NRIC)) {
            suppApplicant.setUser_scd_identification_number(DataMappingListUtil.getValidValue(applicantDetails.getIdValue()));
        }

//check value
        suppApplicant.setUser_scd_first_relationship_primary_app(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getRelationshipWithPrimary().getAoValue()));
        suppApplicant.setUser_scd_gender(DataMappingListUtil.getValidValue(applicantDetails.getGender()));
        suppApplicant.setUser_scd_relationship_bank(applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolder() == null || applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolder() == false ? "N" : "Y");
        suppApplicant.setUser_scd_relationship_bank_details(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolderDetails()));

//int--country code mapping
        suppApplicant.setUser_scd_mobile_number_int(DataMappingListUtil.getValidValue(applicantDetails.getDialCodeMobile()));
        suppApplicant.setUser_scd_mobile_number_no(applicantDetails.getMobile() == 0 || applicantDetails.getMobile() == null ? BLANK_VALUE : applicantDetails.getMobile().toString());
        suppApplicant.setUser_scd_telephone_int(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getDialCodeHomePhone()));
        suppApplicant.setUser_scd_telephone_no(applicantDetails.getAdditionalInfo().getHomePhone() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getHomePhone().toString());
        suppApplicant.setUser_scd_office_number_int(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getDialCodeHomePhone()));
        suppApplicant.setUser_scd_office_number_no(applicantDetails.getAdditionalInfo().getHomePhone() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getHomePhone().toString());


//Former names
        suppApplicant.setUser_scd_former_last_name(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOtherName()));
        suppApplicant.setUser_scd_haveany_former_names(applicantDetails.getAdditionalInfo().getOtherName() == null ? "N" : "Y");
        suppApplicant.setUser_scd_other_last_name(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOtherName()));

        suppApplicant.setUser_scd_company_name(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getCompanyName()));
        suppApplicant.setUser_scd_employment_status(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getEmploymentType().getAoValue()));
        suppApplicant.setUser_scd_occupation(applicantDetails.getAdditionalInfo().getOccupation() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOccupation().getValue()));

//suppApplicant.setUser_scd_senior_public_office("N");
        suppApplicant.setUser_scd_senior_public_office(applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == null || applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == false ? "N" : "Y");

        suppApplicant.setUser_scd_nature_of_relationship(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPublicPositionDetails())); //TODO :,,Value:,);
        return suppApplicant;
    }

    public SuppApplicants populateSecondSupplimentry(SuppApplicants suppApplicant, Applicant applicantDetails) {
        String givenName = "";
        if(applicantDetails.getName() != null){
        	if(applicantDetails.getName().getFirstName() != null){
        		givenName = applicantDetails.getName().getFirstName();
        		if(applicantDetails.getName().getMiddleName() != null){
        			givenName += " "+ applicantDetails.getName().getMiddleName();
        		}
        	}
        }
    	
        suppApplicant.setUser_scd_first_name1(givenName);

        suppApplicant.setUser_scd_last_name1(DataMappingListUtil.getValidValue(applicantDetails.getName().getLastName()));
        suppApplicant.setUser_scd_middle_name1("");
        suppApplicant.setUser_scd_secondary_counter("2");

        LocalDate suppApplicantDob = CommonUtils.convertStringToLocalDate(applicantDetails.getDateOfBirth());
        suppApplicant.setUser_scd_dob_month_1(DataMappingListUtil.convertMonthToCamelCaseFormat(suppApplicantDob.getMonth().toString()));
        suppApplicant.setUser_scd_dob_date_1(DataMappingListUtil.convertsingleDigitToTwoDigit(String.valueOf(suppApplicantDob.getDayOfMonth())));
        suppApplicant.setUser_scd_dob_year_1(String.valueOf(suppApplicantDob.getYear()));

        suppApplicant.setUser_scd_cardholder_name_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNameOnCard()));
        suppApplicant.setUser_scd_nationality_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNationality() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getNationality().getValue()));
        suppApplicant.setUser_scd_title1(DataMappingListUtil.getValidValue(applicantDetails.getSalutation() == null ? Title.DOCTOR.getValue() : applicantDetails.getSalutation().getValue()));


        suppApplicant.setUser_scd_permanent_addr_indicator_1(String.valueOf(applicantDetails.getAdditionalInfo().isResidentialAddressSameAsPrimary()));
        suppApplicant.setUser_scd_second_residential_addr_indicator_1("true");


        DocumentName documentName = applicantDetails.getIdDocType();
        suppApplicant.setUser_scd_identification_type_1(documentName.getAoValue());
        if (documentName.equals(DocumentName.EMPLOYMENT_PASS)) {
            suppApplicant.setUser_scd_identification_number_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPassportNumber()));
            suppApplicant.setUser_scd_identication_issue_country_code_1(applicantDetails.getAdditionalInfo().getPassportCountry() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPassportCountry().getValue()));
        } else if (documentName.equals(DocumentName.NRIC)) {
            suppApplicant.setUser_scd_identification_number_1(DataMappingListUtil.getValidValue(applicantDetails.getIdValue()));
        }

        suppApplicant.setUser_scd_gender_1(DataMappingListUtil.getValidValue(applicantDetails.getGender()));

        suppApplicant.setUser_scd_relationship_bank_1(applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolder() == null || applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolder() == false ? "N" : "Y");
        suppApplicant.setUser_scd_relationship_bank_details_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolderDetails()));
        suppApplicant.setUser_scd_second_relationship_primary_app(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getRelationshipWithPrimary().getAoValue()));

        suppApplicant.setUser_scd_mobile_number_int_1(DataMappingListUtil.getValidValue(applicantDetails.getDialCodeMobile()));
        suppApplicant.setUser_scd_mobile_number_no_1(applicantDetails.getMobile() == null ? BLANK_VALUE : applicantDetails.getMobile().toString());
        suppApplicant.setUser_scd_office_number_int_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getDialCodeHomePhone()));
        suppApplicant.setUser_scd_office_number_no_1(applicantDetails.getAdditionalInfo().getHomePhone() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getHomePhone().toString());
        suppApplicant.setUser_scd_homenumber_int_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getDialCodeHomePhone()));
        suppApplicant.setUser_scd_homenumber_no1(applicantDetails.getAdditionalInfo().getHomePhone() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getHomePhone().toString());


        suppApplicant.setUser_scd_former_last_name_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOtherName()));
        suppApplicant.setUser_scd_haveany_former_names_1(applicantDetails.getAdditionalInfo().getOtherName() == null ? "N" : "Y");
        suppApplicant.setUser_scd_other_last_name_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOtherName()));

        suppApplicant.setUser_scd_company_name_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getCompanyName()));
        suppApplicant.setUser_scd_employment_status_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getEmploymentType().getAoValue()));
        suppApplicant.setUser_scd_occupation_1(applicantDetails.getAdditionalInfo().getOccupation() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOccupation().getValue()));

        List<Address> addressList = applicantDetails.getAdditionalInfo().getAddress();
        log.debug("Address received for Supplimentary applicant 2 : {}", addressList);

        for (Address address : addressList) {
            switch (address.getAddressType()) {
                case RESIDENTIAL:
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        suppApplicant.setUser_scd_address_line_1_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_address_line_2_1(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_address_line_3_1(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_country_1(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_zipcode_1(DataMappingListUtil.getValidValue(address.getPostalCode()));
                        suppApplicant.setUser_scd_residential_foreign_address_line_1_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_residential_foreign_address_line2_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_residential_foreign_address_line3_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_address_line_1_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_permanent_address_line_2_1(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_permanent_address_line_3_1(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_permanent_country_1(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_permanent_zipcode_1(DataMappingListUtil.getValidValue(address.getPostalCode()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line_1_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_foreign_address_line_2_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_foreign_address_line_3_1(BLANK_VALUE);
                    } else {
                        suppApplicant.setUser_scd_country_1(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_residential_foreign_address_line_1_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_residential_foreign_address_line2_1(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_residential_foreign_address_line3_1(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_address_line_1_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_address_line_2_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_address_line_3_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_zipcode_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_foreign_address_line_1_1(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line_2_1(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line_3_1(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_country_1(DataMappingListUtil.getValidValue(DataMappingListUtil.getValidValue(address.getCountry().getValue())));
                        suppApplicant.setUser_scd_permanent_address_line_1_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_address_line_2_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_address_line_3_1(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_zipcode_1(BLANK_VALUE);
                    }
                    break;
                default:
                    break;

            }
        }

	

	/*suppApplicant.setUser_scd_permanent_address_line_1_1("256");
    suppApplicant.setUser_scd_permanent_address_line_2_1("abcd street");
	suppApplicant.setUser_scd_permanent_address_line_3_1("");
	suppApplicant.setUser_scd_permanent_country_1("SG");
	suppApplicant.setUser_scd_permanent_zipcode_1("256341");
	suppApplicant.setUser_scd_permanent_foreign_address_line_1_1(BLANK_VALUE);
	suppApplicant.setUser_scd_permanent_foreign_address_line_2_1(BLANK_VALUE);
	suppApplicant.setUser_scd_permanent_foreign_address_line_3_1(BLANK_VALUE);  
	suppApplicant.setUser_scd_address_line_1_1("256");
 	suppApplicant.setUser_scd_address_line_2_1("abcd street");
 	suppApplicant.setUser_scd_address_line_3_1("");
 	suppApplicant.setUser_scd_country_1("SG");
 	suppApplicant.setUser_scd_zipcode_1("256341");
 	 suppApplicant.setUser_scd_residential_foreign_address_line_1_1(BLANK_VALUE);
	 suppApplicant.setUser_scd_residential_foreign_address_line2_1(BLANK_VALUE);
	 suppApplicant.setUser_scd_residential_foreign_address_line3_1(BLANK_VALUE);
	*/
        suppApplicant.setUser_scd_nature_of_relationship_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPublicPositionDetails()));
        suppApplicant.setUser_scd_senior_public_office_1(applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == null || applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == false ? "N" : "Y");
        //suppApplicant.setUser_scd_senior_public_office_1("N");


        return suppApplicant;
    }

    public SuppApplicants populateThirdSupplimentry(SuppApplicants suppApplicant, Applicant applicantDetails) {

        String givenName = "";
        if(applicantDetails.getName() != null){
        	if(applicantDetails.getName().getFirstName() != null){
        		givenName = applicantDetails.getName().getFirstName();
        		if(applicantDetails.getName().getMiddleName() != null){
        			givenName += " "+ applicantDetails.getName().getMiddleName();
        		}
        	}
        }
        suppApplicant.setUser_scd_first_name2(givenName);
        suppApplicant.setUser_scd_last_name2(DataMappingListUtil.getValidValue(applicantDetails.getName().getLastName()));
        suppApplicant.setUser_scd_middle_name2("");
        suppApplicant.setUser_scd_secondary_counter("3");
        LocalDate suppApplicantDob = CommonUtils.convertStringToLocalDate(applicantDetails.getDateOfBirth());

        suppApplicant.setUser_scd_dob_month_2(DataMappingListUtil.convertMonthToCamelCaseFormat(suppApplicantDob.getMonth().toString()));
        suppApplicant.setUser_scd_dob_date_2(DataMappingListUtil.convertsingleDigitToTwoDigit(String.valueOf(suppApplicantDob.getDayOfMonth())));
        suppApplicant.setUser_scd_dob_year_2(String.valueOf(suppApplicantDob.getYear()));


        suppApplicant.setUser_scd_cardholdername_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNameOnCard()));
        suppApplicant.setUser_scd_nationality_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNationality() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getNationality().getValue()));
        suppApplicant.setUser_scd_title2(DataMappingListUtil.getValidValue(applicantDetails.getSalutation() == null ? Title.DOCTOR.getValue() : applicantDetails.getSalutation().getValue()));


        suppApplicant.setUser_scd_permanent_addr_indicator_2(String.valueOf(applicantDetails.getAdditionalInfo().isResidentialAddressSameAsPrimary()));


        DocumentName documentName = applicantDetails.getIdDocType();
        suppApplicant.setUser_scd_identification_type_2(documentName.getAoValue());
        if (documentName.equals(DocumentName.EMPLOYMENT_PASS)) {
            suppApplicant.setUser_scd_identification_number_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPassportNumber()));
            suppApplicant.setUser_scd_identication_issue_country_code_2(applicantDetails.getAdditionalInfo().getPassportCountry() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPassportCountry().getValue()));
        } else if (documentName.equals(DocumentName.NRIC)) {
            suppApplicant.setUser_scd_identification_number_2(DataMappingListUtil.getValidValue(applicantDetails.getIdValue()));
        }

        suppApplicant.setUser_scd_third_relationship_primary_app(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getRelationshipWithPrimary().getAoValue()));
        suppApplicant.setUser_scd_gender_2(DataMappingListUtil.getValidValue(applicantDetails.getGender()));
        suppApplicant.setUser_scd_have_my_hpr_status_2("No");
        suppApplicant.setUser_scd_relationship_bank_2(applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolder() == null || applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolder() == false ? "N" : "Y");
        suppApplicant.setUser_scd_relationship_bank_details_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getAssociateOfPublicPositionHolderDetails()));

//to be edited when enum in place	
        suppApplicant.setUser_scd_mobile_number_int_2(DataMappingListUtil.getValidValue(applicantDetails.getDialCodeMobile()));
        suppApplicant.setUser_scd_mobile_number_no_2(applicantDetails.getMobile() == null ? BLANK_VALUE : applicantDetails.getMobile().toString());
        suppApplicant.setUser_scd_office_number_int_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getDialCodeHomePhone()));
        suppApplicant.setUser_scd_office_number_no_2(applicantDetails.getAdditionalInfo().getHomePhone() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getHomePhone().toString());
        suppApplicant.setUser_scd_homenumber_int_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getDialCodeHomePhone()));
        suppApplicant.setUser_scd_homenumber_no2(applicantDetails.getAdditionalInfo().getHomePhone() == null ? BLANK_VALUE : applicantDetails.getAdditionalInfo().getHomePhone().toString());


        suppApplicant.setUser_scd_former_last_name_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOtherName()));
        suppApplicant.setUser_scd_haveany_former_names_2(applicantDetails.getAdditionalInfo().getOtherName() == null ? "N" : "Y");
        suppApplicant.setUser_scd_other_last_name_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOtherName()));

        suppApplicant.setUser_scd_company_name_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getCompanyName()));
        suppApplicant.setUser_scd_employment_status_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getEmploymentType().getAoValue()));
        suppApplicant.setUser_scd_occupation_2(applicantDetails.getAdditionalInfo().getOccupation() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getOccupation().getValue()));


        List<Address> addressList = applicantDetails.getAdditionalInfo().getAddress();
        log.debug("Address received for Supplimentary applicant 3 : {}", addressList);

        for (Address address : addressList) {
            switch (address.getAddressType()) {
                case RESIDENTIAL:
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        suppApplicant.setUser_scd_address_line_1_2(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_address_line_2_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_address_line_3_2(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_country_2(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_zipcode_2(DataMappingListUtil.getValidValue(address.getPostalCode()));
                        suppApplicant.setUser_scd_residential_foreign_address_line1_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_residential_foreign_address_line_2_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_residential_foreign_address_line3_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_address_line_1_2(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_permanent_address_line_2_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_permanent_address_line_3_2(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_permanent_country_2(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_permanent_zipcode_2(DataMappingListUtil.getValidValue(address.getPostalCode()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line1_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_foreign_address_line_2_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_foreign_address_line3_2(BLANK_VALUE);
                    } else {
                        suppApplicant.setUser_scd_country_2(DataMappingListUtil.getValidValue(address.getCountry().getValue()));
                        suppApplicant.setUser_scd_residential_foreign_address_line1_2(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_residential_foreign_address_line_2_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_residential_foreign_address_line3_2(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_address_line_1_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_address_line_2_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_address_line_3_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_zipcode_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_foreign_address_line1_2(DataMappingListUtil.getValidValue(address.getLine1Address()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line_2_2(DataMappingListUtil.getValidValue(address.getLine2Address()));
                        suppApplicant.setUser_scd_permanent_foreign_address_line3_2(DataMappingListUtil.getValidValue(address.getLine3Address()));
                        suppApplicant.setUser_scd_country_2(DataMappingListUtil.getValidValue(DataMappingListUtil.getValidValue(address.getCountry().getValue())));
                        suppApplicant.setUser_scd_permanent_address_line_1_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_address_line_2_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_address_line_3_2(BLANK_VALUE);
                        suppApplicant.setUser_scd_permanent_zipcode_2(BLANK_VALUE);
                    }
                    break;
                default:
                    break;

            }
        }

	
	/*suppApplicant.setUser_scd_permanent_address_line_1_2("256");
    suppApplicant.setUser_scd_permanent_address_line_2_2("abcd street");
	suppApplicant.setUser_scd_permanent_address_line_3_2("");
	suppApplicant.setUser_scd_permanent_country_2("SG");
	suppApplicant.setUser_scd_permanent_zipcode_2("256341");
	suppApplicant.setUser_scd_permanent_foreign_address_line1_2(BLANK_VALUE);
	suppApplicant.setUser_scd_permanent_foreign_address_line_2_2(BLANK_VALUE);
	suppApplicant.setUser_scd_permanent_foreign_address_line3_2(BLANK_VALUE);  
	suppApplicant.setUser_scd_address_line_1_2("256");
 	suppApplicant.setUser_scd_address_line_2_2("abcd street");
 	suppApplicant.setUser_scd_address_line_3_2("");
 	suppApplicant.setUser_scd_country_2("SG");
 	suppApplicant.setUser_scd_zipcode_2("256341");
 	 suppApplicant.setUser_scd_residential_foreign_address_line1_2(BLANK_VALUE);
	 suppApplicant.setUser_scd_residential_foreign_address_line_2_2(BLANK_VALUE);
	 suppApplicant.setUser_scd_residential_foreign_address_line3_2(BLANK_VALUE);
	
	*/
        suppApplicant.setUser_scd_senior_public_office_2(applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == null || applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == false ? "N" : "Y");
        suppApplicant.setUser_scd_nature_of_relationship_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getPublicPositionDetails()));
//suppApplicant.setUser_scd_senior_public_office_2("N");
        return suppApplicant;
    }


    private void supplementaryAddressCheck(Applicant supplementaryApplicant, Application application) {

        if (CollectionUtils.isEmpty(supplementaryApplicant.getAdditionalInfo().getAddress()) && supplementaryApplicant.getAdditionalInfo().isResidentialAddressSameAsPrimary()) {
            List<Address> address = supplementaryApplicant.getAdditionalInfo().getAddress();

            if (supplementaryApplicant.getAdditionalInfo().getAddress() != null) {
                Optional<Address> isResidentialAddressPresentInSupp = address.stream()
                        .filter(x -> x.getAddressType().equals(AddressType.RESIDENTIAL))
                        .findFirst();
                if (!isResidentialAddressPresentInSupp.isPresent()) {
                    Address primaryResidentialAddress = getPrimaryApplicantResidentialAddress(application);
                    address.add(primaryResidentialAddress);
                    supplementaryApplicant.getAdditionalInfo().setAddress(address);
                }
            } else {
                List<Address> addresses = new ArrayList<>();
                Address primaryResidentialAddress = getPrimaryApplicantResidentialAddress(application);
                addresses.add(primaryResidentialAddress);
                supplementaryApplicant.getAdditionalInfo().setAddress(addresses);
            }
        }
    }

    private Address getPrimaryApplicantResidentialAddress(Application application) {
        return application.getPrimaryApplicant().getAdditionalInfo().getAddress().stream()
                .filter(x -> x.getAddressType().equals(AddressType.RESIDENTIAL))
                .findFirst().get();
    }

}