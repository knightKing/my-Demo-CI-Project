package hsbc.aofe.ao.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import hsbc.aofe.domain.Application;

public class Step4Data {
public List<NameValuePair>  dataList() {


List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
nvpList.add(new BasicNameValuePair("UIC",""));
nvpList.add(new BasicNameValuePair("WIRE_ACTION","SendActionGAD"));
nvpList.add(new BasicNameValuePair("ao-gad-ad-addressformatcode","HUB"));
nvpList.add(new BasicNameValuePair("ao-gad-cdd-preferredlanguage","ENG"));
nvpList.add(new BasicNameValuePair("ao-gad-cpd-contactpreferences","Y"));
nvpList.add(new BasicNameValuePair("ao-gad-cscdp-maxPermissibleCardHolders","9"));
nvpList.add(new BasicNameValuePair("ao-gad-cscdp-secondaryCounter","1"));
nvpList.add(new BasicNameValuePair("ao-gad-ead-employercountry_oh1","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-rad-residentialaddressline1","L 123"));
nvpList.add(new BasicNameValuePair("ao-gad-rad-residentialaddressline2","Lake Street"));
nvpList.add(new BasicNameValuePair("ao-gad-rad-residentialaddressline3","1"));
nvpList.add(new BasicNameValuePair("ao-gad-rad-residentialcountry","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-rad-residentialzipcode1","123456"));
nvpList.add(new BasicNameValuePair("ao-gad-radp-mypdpatermsdeclaration-checkbox","on"));
nvpList.add(new BasicNameValuePair("ao-gad-rcdd-citizenshiphidden","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-rcdd-countryofbirthhidden","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-rcdd-multiple-citizenship-text","N"));
nvpList.add(new BasicNameValuePair("ao-gad-rcdd-referral","No"));
nvpList.add(new BasicNameValuePair("ao-gad-rcdd-tax1hidden",""));
nvpList.add(new BasicNameValuePair("ao-gad-rcdd-tax2-text",""));
nvpList.add(new BasicNameValuePair("ao-gad-rcdd-tax3-text",""));
nvpList.add(new BasicNameValuePair("ao-gad-rciedd-consentcreditref","on"));
nvpList.add(new BasicNameValuePair("ao-gad-rcndt-prefix","MISS"));
nvpList.add(new BasicNameValuePair("ao-gad-rcpd-preferreddispatchmodetext","Mail"));
nvpList.add(new BasicNameValuePair("ao-gad-redd-employmentstatus","F"));
nvpList.add(new BasicNameValuePair("ao-gad-schd-nationality","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-schd-nationality1","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-schd-nationality2","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-sid-identificationtype",""));
nvpList.add(new BasicNameValuePair("ao-gad-sid-identificationtype1",""));
nvpList.add(new BasicNameValuePair("ao-gad-sid-identificationtype2",""));
nvpList.add(new BasicNameValuePair("ao-gad-srad-country","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-srad-country1","SG"));
nvpList.add(new BasicNameValuePair("ao-gad-srad-country2","SG"));
/*nvpList.add(new BasicNameValuePair("ao-gad-wt-businessLine","NA"));
nvpList.add(new BasicNameValuePair("ao-gad-wt-customerGroup","PFS"));
nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_1","Visa Platinum"));
nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_2","IB"));
nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_3","HBSP_Visa Platinum"));
nvpList.add(new BasicNameValuePair("ao-gad-wt-language","en"));
nvpList.add(new BasicNameValuePair("ao-gad-wt-promoCode","DEF0000001"));
nvpList.add(new BasicNameValuePair("ao-gad-wt-retrieveAppIndicator","N"));*/
nvpList.add(new BasicNameValuePair("ao-hbap-gad-ccead-creditcardemployercountry","SG"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentaddressline1","L 123"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentaddressline2","Lake Street"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentaddressline3",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentcountry","SG"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentforeignaddressline1",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentforeignaddressline2",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentforeignaddressline3",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-pad-permanentzipcode1","123456"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-prad-previousresidentialcountry","SG"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rad-residentialforeignaddressline1",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rad-residentialforeignaddressline2",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rad-residentialforeignaddressline3",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rcccad-creditcardsendmymailstohidden","E"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rcdd-relationshipbank","No"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rcnd-haveanyformernameshidden","No"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rcscdp-secondarycardholderindicatorhidden","N"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-redd-monthstext","0"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-redd-occupationhidden","9"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-redd-yearstext","5"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rlir-monthsyearsinresidencehidden","5"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rori-seniorpublicofficehidden","N"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rpad-permanentcountryhidden","SG"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rrac-residentialaddrindicatorhidden","true"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rscnd-haveanyformernames1hidden",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rscnd-haveanyformernames2hidden",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rscnd-haveanyformernameshidden",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rssri-seniorpublicoffice1text",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rssri-seniorpublicoffice2text",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-rssri-seniorpublicofficetext",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-seid-employmentstatus",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-seid-employmentstatus1",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-seid-employmentstatus2",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-spac-permanentaddrindicator","Yes"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-spac-permanentaddrindicator1","Yes"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-spac-permanentaddrindicator2","Yes"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-spad-permanentcountry",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-spad-permanentcountry1",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-spad-permanentcountry2",""));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-srac-firstresidentialaddrindicator","true"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-srac-firstresidentialaddrindicator1","true"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-srac-firstresidentialaddrindicator2","true"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-srac-secondresidentialaddrindicator","true"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-srac-secondresidentialaddrindicator1","true"));
nvpList.add(new BasicNameValuePair("ao-hbap-gad-srac-secondresidentialaddrindicator2","true"));
nvpList.add(new BasicNameValuePair("cmd_continue_accountapprovalupselloffered","Submit for Approval"));
nvpList.add(new BasicNameValuePair("jsf_sequence","5"));
nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7:reviewdeatils:_idcl","submit"));
nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:reviewdeatils_SUBMIT","1"));

return nvpList;

}
}
