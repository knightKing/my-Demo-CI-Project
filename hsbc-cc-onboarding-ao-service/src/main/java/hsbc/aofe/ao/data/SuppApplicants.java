package hsbc.aofe.ao.data;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.YES_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.NONE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.TRUE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SINGAPORE_COUNTRY_CODE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.DEFAULT_SEC_CARD_COUNTER;

import lombok.Data;

@Data
public class SuppApplicants {

    private String user_scd_first_name = BLANK_VALUE;
    private String user_scd_first_name1 = BLANK_VALUE;
    private String user_scd_first_name2 = BLANK_VALUE;
    private String user_scd_last_name = BLANK_VALUE;
    private String user_scd_last_name1 = BLANK_VALUE;
    private String user_scd_last_name2 = BLANK_VALUE;
    private String user_scd_middle_name = BLANK_VALUE;
    private String user_scd_middle_name1 = BLANK_VALUE;
    private String user_scd_middle_name2 = BLANK_VALUE;
    private String user_scd_secondary_counter = DEFAULT_SEC_CARD_COUNTER;
    /*
     * private String user_scd_max_permissible_card_holders=ao-gad-cscdp-
	 * maxPermissibleCardHolders = BLANK_VALUE ;
	 * 
	 * private String user_scd_secondary_cardholder_indicator=ao-gad-cscdp-
	 * secondarycardholderindicator = BLANK_VALUE ; private String #mm-dd-yyyy =
	 * BLANK_VALUE ;
	 */

    private String user_scd_dob_months = BLANK_VALUE;
    private String user_scd_dob_date = BLANK_VALUE;
    private String user_scd_dob_year = BLANK_VALUE;
    private String user_scd_cardholder_name = BLANK_VALUE;
    private String user_scd_cardholder_name_1 = BLANK_VALUE;
    private String user_scd_cardholdername_2 = BLANK_VALUE;
    private String user_scd_nationality = SINGAPORE_COUNTRY_CODE;
    private String user_scd_nationality_1 = SINGAPORE_COUNTRY_CODE;
    private String user_scd_nationality_2 = SINGAPORE_COUNTRY_CODE;
    private String user_scd_homenumber_area_1 = BLANK_VALUE;
    private String user_scd_homenumber_area_2 = BLANK_VALUE;
    private String user_scd_homenumber_int_1 = BLANK_VALUE;
    private String user_scd_homenumber_int_2 = BLANK_VALUE;
    private String user_scd_homenumber_no1 = BLANK_VALUE;
    private String user_scd_homenumber_no2 = BLANK_VALUE;
    private String user_scd_sec_secondary_title_other = BLANK_VALUE;
    private String user_scd_thirdtitleother = BLANK_VALUE;
    private String user_scd_title = BLANK_VALUE;
    private String user_scd_title1 = BLANK_VALUE;
    private String user_scd_title2 = BLANK_VALUE;
    // leave this value blank user_scd_titleother
    private String user_scd_titleother = BLANK_VALUE;

	/* private String # value was none in mm --dd = BLANK_VALUE ; */

    private String user_scd_dob_month_1 = NONE_VALUE;
    private String user_scd_dob_date_1 = NONE_VALUE;
    private String user_scd_dob_year_1 = BLANK_VALUE;
    private String user_scd_identication_issue_country_code = BLANK_VALUE;
    private String user_scd_identication_issue_country_code_1 = BLANK_VALUE;
    private String user_scd_identication_issue_country_code_2 = BLANK_VALUE;
    private String user_scd_identification_number = BLANK_VALUE;
    private String user_scd_identification_number_1 = BLANK_VALUE;
    private String user_scd_identification_number_2 = BLANK_VALUE;
    private String user_scd_identification_type = BLANK_VALUE;
    private String user_scd_identification_type_1 = BLANK_VALUE;
    private String user_scd_identification_type_2 = BLANK_VALUE;
    private String user_scd_address_line_1 = BLANK_VALUE;
    private String user_scd_address_line_1_1 = BLANK_VALUE;
    private String user_scd_address_line_1_2 = BLANK_VALUE;
    private String user_scd_address_line_2 = BLANK_VALUE;
    private String user_scd_address_line_2_1 = BLANK_VALUE;
    private String user_scd_address_line_2_2 = BLANK_VALUE;
    private String user_scd_address_line_3 = BLANK_VALUE;
    private String user_scd_address_line_3_1 = BLANK_VALUE;
    private String user_scd_address_line_3_2 = BLANK_VALUE;
    private String user_scd_country = BLANK_VALUE;
    private String user_scd_country_1 = BLANK_VALUE;
    private String user_scd_country_2 = BLANK_VALUE;
    private String user_scd_zipcode = BLANK_VALUE;
    private String user_scd_zipcode_1 = BLANK_VALUE;
    private String user_scd_zipcode_2 = BLANK_VALUE;
    private String user_scd_dob_month_2 = NONE_VALUE;
    private String user_scd_dob_date_2 = NONE_VALUE;
    private String user_scd_dob_year_2 = BLANK_VALUE;

	/*
	 * private String #nm = BLANK_VALUE ; private String
	 * ao-gad-wt-businessLine=ao-gad-wt-businessLine = BLANK_VALUE ; private
	 * String ao-gad-wt-customerGroup=ao-gad-wt-customerGroup = BLANK_VALUE ;
	 * private String ao-gad-wt-hide_1=Visa Platinum = BLANK_VALUE ; private
	 * String ao-gad-wt-hide_2=IB = BLANK_VALUE ; private String
	 * ao-gad-wt-hide_3=HBSP_Visa Platinum = BLANK_VALUE ; private String
	 * ao-gad-wt-language=en = BLANK_VALUE ; private String
	 * ao-gad-wt-promoCode=DEF0000001 = BLANK_VALUE ; private String
	 * ao-gad-wt-retrieveAppIndicator=N = BLANK_VALUE ;
	 */

	/* private String #user,#user = BLANK_VALUE ; */

    private String user_scd_first_relationship_primary_app = BLANK_VALUE;
    private String user_scd_gender = BLANK_VALUE;
    private String user_scd_gender_1 = BLANK_VALUE;
    private String user_scd_gender_2 = BLANK_VALUE;
    private String user_scd_have_my_hpr_status_2 = BLANK_VALUE;
    private String user_scd_relationship_bank = BLANK_VALUE;
    private String user_scd_relationship_bank_1 = BLANK_VALUE;
    private String user_scd_relationship_bank_2 = BLANK_VALUE;
    private String user_scd_relationship_bank_details = BLANK_VALUE;
    private String user_scd_relationship_bank_details_1 = BLANK_VALUE;
    private String user_scd_relationship_bank_details_2 = BLANK_VALUE;
    private String user_scd_second_relationship_primary_app = BLANK_VALUE;
    private String user_scd_third_relationship_primary_app = BLANK_VALUE;
    private String user_scd_mobile_number_area_code = BLANK_VALUE;
    private String user_scd_mobile_number_area_code_1 = BLANK_VALUE;
    private String user_scd_mobile_number_area_code_2 = BLANK_VALUE;
    private String user_scd_mobile_number_int = BLANK_VALUE;
    private String user_scd_mobile_number_int_1 = BLANK_VALUE;
    private String user_scd_mobile_number_int_2 = BLANK_VALUE;
    private String user_scd_mobile_number_no = BLANK_VALUE;
    private String user_scd_mobile_number_no_1 = BLANK_VALUE;
    private String user_scd_mobile_number_no_2 = BLANK_VALUE;
    private String user_scd_office_number_area_code = BLANK_VALUE;
    private String user_scd_office_number_area_code_1 = BLANK_VALUE;
    private String user_scd_office_number_area_code_2 = BLANK_VALUE;
    private String user_scd_office_number_int = BLANK_VALUE;
    private String user_scd_office_number_int_1 = BLANK_VALUE;
    private String user_scd_office_number_int_2 = BLANK_VALUE;
    private String user_scd_office_number_no = BLANK_VALUE;
    private String user_scd_office_number_no_1 = BLANK_VALUE;
    private String user_scd_office_number_no_2 = BLANK_VALUE;
    private String user_scd_former_last_name = BLANK_VALUE;
    private String user_scd_former_last_name_1 = BLANK_VALUE;
    private String user_scd_former_last_name_2 = BLANK_VALUE;
    private String user_scd_haveany_former_names = BLANK_VALUE;
    private String user_scd_haveany_former_names_1 = BLANK_VALUE;
    private String user_scd_haveany_former_names_2 = BLANK_VALUE;
    private String user_scd_other_last_name = BLANK_VALUE;
    private String user_scd_other_last_name_1 = BLANK_VALUE;
    private String user_scd_other_last_name_2 = BLANK_VALUE;
    private String user_scd_supplementary_declaration_accept_box = TRUE_VALUE; // on
    // or
    // true
    // check
    private String user_scd_company_name = BLANK_VALUE;
    private String user_scd_company_name_1 = BLANK_VALUE;
    private String user_scd_company_name_2 = BLANK_VALUE;
    private String user_scd_employment_status = BLANK_VALUE;
    private String user_scd_employment_status_1 = BLANK_VALUE;
    private String user_scd_employment_status_2 = BLANK_VALUE;
    private String user_scd_occupation = BLANK_VALUE;
    private String user_scd_occupation_1 = BLANK_VALUE;
    private String user_scd_occupation_2 = BLANK_VALUE;
    private String user_scd_permanent_addr_indicator = TRUE_VALUE;
    private String user_scd_permanent_addr_indicator_1 = TRUE_VALUE;
    private String user_scd_permanent_addr_indicator_2 = TRUE_VALUE;
    private String user_scd_permanent_address_line_1 = BLANK_VALUE;
    private String user_scd_permanent_address_line_1_1 = BLANK_VALUE;
    private String user_scd_permanent_address_line_1_2 = BLANK_VALUE;
    private String user_scd_permanent_address_line_2 = BLANK_VALUE;
    private String user_scd_permanent_address_line_2_1 = BLANK_VALUE;
    private String user_scd_permanent_address_line_2_2 = BLANK_VALUE;
    private String user_scd_permanent_address_line_3 = BLANK_VALUE;
    private String user_scd_permanent_address_line_3_1 = BLANK_VALUE;
    private String user_scd_permanent_address_line_3_2 = BLANK_VALUE;
    private String user_scd_permanent_country = SINGAPORE_COUNTRY_CODE;
    private String user_scd_permanent_country_1 = SINGAPORE_COUNTRY_CODE;
    private String user_scd_permanent_country_2 = SINGAPORE_COUNTRY_CODE;
    private String user_scd_permanent_foreign_address_line_1 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line_1_1 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line1_2 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line_2 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line_2_1 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line_2_2 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line_3 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line_3_1 = BLANK_VALUE;
    private String user_scd_permanent_foreign_address_line3_2 = BLANK_VALUE;
    private String user_scd_permanent_zipcode = BLANK_VALUE;
    private String user_scd_permanent_zipcode_1 = BLANK_VALUE;
    private String user_scd_permanent_zipcode_2 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line_1 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line_1_1 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line1_2 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line_2 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line2_1 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line_2_2 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line_3 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line3_1 = BLANK_VALUE;
    private String user_scd_residential_foreign_address_line3_2 = BLANK_VALUE;
    private String user_scd_first_residential_addr_indicator = BLANK_VALUE;
    private String user_scd_first_residential_addr_indicator_1 = BLANK_VALUE;
    private String user_scd_first_residential_addr_indicator_2 = BLANK_VALUE;
    private String user_scd_second_residential_addr_indicator = TRUE_VALUE;
    private String user_scd_second_residential_addr_indicator_1 = TRUE_VALUE;
    private String user_scd_second_residential_addr_indicator_2 = TRUE_VALUE;
    private String user_scd_nature_of_relationship = BLANK_VALUE;
    private String user_scd_nature_of_relationship_1 = BLANK_VALUE;
    private String user_scd_nature_of_relationship_2 = BLANK_VALUE;
    private String user_scd_senior_public_office = BLANK_VALUE;
    private String user_scd_senior_public_office_1 = BLANK_VALUE;
    private String user_scd_senior_public_office_2 = BLANK_VALUE;
    private String user_scd_telephone_area = BLANK_VALUE;
    private String user_scd_telephone_int = BLANK_VALUE;
    private String user_scd_telephone_no = BLANK_VALUE;
    private String user_rd_nationality = SINGAPORE_COUNTRY_CODE;
    private String user_rd_nationality1 = SINGAPORE_COUNTRY_CODE;
    private String user_rd_nationality2 = SINGAPORE_COUNTRY_CODE;
    private String user_rd_identificationtype = BLANK_VALUE;
    private String user_rd_identificationtype1 = BLANK_VALUE;
    private String user_rd_identificationtype2 = BLANK_VALUE;
    private String user_rd_country = SINGAPORE_COUNTRY_CODE;
    private String user_rd_country1 = SINGAPORE_COUNTRY_CODE;
    private String user_rd_country2 = SINGAPORE_COUNTRY_CODE;
    // private String user_rd_haveany_former_names_hidden_rcnd =
    // private String user_rd_secondary_cardholder_indicator_hidden= "";
	/*
	 * private String user_rd_months_text = private String
	 * user_rd_occupation_hidden = private String user_rd_years_text = private
	 * String user_rd_months_years_in_residence_hidden = private String
	 * user_rd_senior_public_office_hidden = private String
	 * user_rd_permanent_country_hidden = private String
	 * user_rd_residential_addr_indicator_hidden =
	 */
	/*
	 * private String user_rd_have_any_former_names_1_hidden = private String
	 * user_rd_haveany_former_names_2_hidden = private String
	 * user_rd_haveany_former_names_hidden_rscnd =
	 */
    private String user_rd_senior_public_office_1_text = BLANK_VALUE;
    private String user_rd_senior_public_office_2_text = BLANK_VALUE;
    private String user_rd_senior_public_office_text = BLANK_VALUE;
    private String user_rd_employment_status = BLANK_VALUE;
    private String user_rd_employment_status_1 = BLANK_VALUE;
    private String user_rd_employment_status_2 = BLANK_VALUE;
    private String user_rd_permanent_addr_indicator = YES_VALUE;
    private String user_rd_permanent_addr_indicator_1 = YES_VALUE;
    private String user_rd_permanent_addr_indicator_2 = YES_VALUE;
    private String user_rd_permanent_country = SINGAPORE_COUNTRY_CODE;
    private String user_rd_permanent_country_1 = SINGAPORE_COUNTRY_CODE;
    private String user_rd_permanent_country_2 = SINGAPORE_COUNTRY_CODE;
    private String user_rd_first_residential_addr_indicator = BLANK_VALUE;
    private String user_rd_first_residential_addr_indicator_1 = BLANK_VALUE;
    private String user_rd_first_residential_addr_indicator_2 = BLANK_VALUE;
    private String user_rd_second_residential_addr_indicator = TRUE_VALUE;
    private String user_rd_second_residential_addr_indicator_1 = TRUE_VALUE;
    private String user_rd_second_residential_addr_indicator_2 = TRUE_VALUE;
	

	/*
	 * private String user_scd_cmd_continue_review_page=cmd_continue_reviewpage
	 * = BLANK_VALUE ; private String user_scd_jsf_sequence=jsf_sequence =
	 * BLANK_VALUE ; private String
	 * user_scd_idcl=viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:
	 * captureSupplementCardholderDetails:_idcl = BLANK_VALUE ; private String
	 * user_scd_submit=viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:
	 * captureSupplementCardholderDetails_SUBMIT = BLANK_VALUE ; private String
	 * dynamic_scd_product_tracker_id_list_string =
	 * ao-hbap-gad-cscdp-cbaProductTrackerIDListString = BLANK_VALUE ; private
	 * String dynamic_product_list_1=productList1 = BLANK_VALUE ;
	 */

}
