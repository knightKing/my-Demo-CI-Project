package hsbc.aofe.ao.handlers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import hsbc.aofe.ao.config.PropertiesCache;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ApplicationIdAPEErrorHandler implements ErrorHandler {

	private ErrorHandler chain;

	@Override
	public void extractError(String html, String errorId, StringBuilder errorString) {

		Element csMainIdBlock = extractCsMainErrorBlock(html);

		if (null != csMainIdBlock) {
			Elements errorTextElements = csMainIdBlock.children();
			String errorText = errorTextElements.text();
			log.debug("Error extracted from AO response : " + errorText);
			System.out.println("Error extracted from AO response : " + errorText);
			errorString.append(errorText);
			errorText = "AO Error Response:".concat(errorText);
		} else {
			this.chain.extractError(html, null, errorString);
		}
	}

	@Override
	public void setNextChain(ErrorHandler nextChain) {
		this.chain = nextChain;

	}

	private Element extractCsMainErrorBlock(String html) {
		String extractCsMainErrorBlock = PropertiesCache.getInstance().getProperty("error.block.cs.main.id");
		Document doc = Jsoup.parse(html);
		try {
			Element csMainErrorsElementBlock = doc.getElementById(extractCsMainErrorBlock);
			return csMainErrorsElementBlock;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

}
