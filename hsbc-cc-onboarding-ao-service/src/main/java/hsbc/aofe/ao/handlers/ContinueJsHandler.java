package hsbc.aofe.ao.handlers;

import java.util.ArrayList;
import java.util.List;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.util.PageUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

@Slf4j
public class ContinueJsHandler implements Handler {

    public boolean execute(RequestParameters params, Application application) throws Exception {
        /*try {*/
        String applicationArn = application.getArn();
        log.info("STARTING: Step Continue Js handler execute method for arn {}", applicationArn);
        String hostUrl = PropertiesCache.getInstance().getProperty("host.url");

        List<NameValuePair> nvpList = new ArrayList<>();
        nvpList.add(new BasicNameValuePair("continue", "Continue"));

        HttpPost post = new HttpPost(params.getUrl());
        PageUtil.setRequestHeaders(post, params);
        post.setEntity(new UrlEncodedFormEntity(nvpList));

        log.info("Posting parameters for Continue JS handler for arn {}", applicationArn);
        HttpResponse response = params.getClient().build().execute(post, params.getLocalContext());

        String responseContinue = PageUtil.returnPostResponse(response, post);
        log.info("Response recorded for Continue JS handler for arn {}, Response: {}", applicationArn, responseContinue);

        String requestUrlContinueSecond = hostUrl.concat(RequestUtil.extractUrl(responseContinue, "wiringForm"));
        log.info("Url Extracted from Continue JS handler response for Continue JS second handler  for arn {}, Extracted Url: ", applicationArn, requestUrlContinueSecond);
        params.setUrl(requestUrlContinueSecond);

        log.info("FINISHED: Continue JS handler execute method for arn {}", applicationArn);
        /*} catch (Exception e) {

			log.error("Error in Continue Js Handler", e);
			//throw new AOException("Error in Continue Js Handler", e);
		}*/
        return Boolean.TRUE;
    }
}
