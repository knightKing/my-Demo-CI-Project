package hsbc.aofe.ao.handlers;

import java.util.ArrayList;
import java.util.List;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

@Slf4j
public class ContinueJsSecondHandler implements Handler {

    public boolean execute(RequestParameters params, Application application) throws Exception {
        /*try {*/
        String applicationArn = application.getArn();
        log.info("STARTING: Step ContinueJsSecondHandler execute method for arn {}", applicationArn);

        String hostUrl = PropertiesCache.getInstance().getProperty("host.url");
        PostRequest postRequest = new PostRequest();
        List<NameValuePair> nvpList = new ArrayList<>();

        nvpList.add(new BasicNameValuePair("continue", "Continue"));

        log.info("Posting parameters for ContinueJsSecondHandler for arn {}", applicationArn);
        String responseString = postRequest.postHeaderResponse(nvpList, params);

        log.info("Url :{} , Extracted from ContinueJsSecondHandler response for Redirect to Step 1 handler for arn {}", responseString, applicationArn);
        params.setRedirectionUrl(responseString);

        /*} catch (Exception e) {
            log.error("Error in Continue Js Second Handler", e);
			//throw new AOException("Error in Continue Js Second Handler", e);
		}*/
        log.info("FINISHED: ContinueJsSecondHandler execute method for arn {}", applicationArn);
        return Boolean.TRUE;
    }

}

