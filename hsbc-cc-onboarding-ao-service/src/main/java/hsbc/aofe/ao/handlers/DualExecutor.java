package hsbc.aofe.ao.handlers;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class DualExecutor {
	private List<Handler> handlers;
// work in progress for dual card
	public String execute(RequestParameters params, Application application) {

		String AOReferenceNumber = "";
		
		List<Handler> list = new LinkedList<Handler>();
		list.add(new LaunchTpsaLinkHandler());
		list.add(new StepAoEntryCardSpecsHandler());
		list.add(new ContinueJsHandler());
		list.add(new ContinueJsHandler());
		list.add(new ContinueJsSecondHandler());
		list.add(new RedirectToStep1Handler());
		list.add(new StepGad1PersonalDetailsHandler());
		list.add(new StepGad2EmpolymentDetailsHandler());
		
		list.add(new Step3WithDataDecor());
		
		//list.add(new Step3Trial());
		//list.add(new StepGad3SupplementaryDetailsHandler());
		//list.add(new StepGad4ReviewDetailsHandler());
		list.add(new Step4WithoutSupplementary());
		
		/*list.add(new RedirectToStep5Handler());
		list.add(new Step5ConfigureProductHandler());
		list.add(new RedirectToStep6Handler());*/
		handlers = Collections.unmodifiableList(list);

		try {
			for (Handler handler : handlers) {
				handler.execute(params, application);
				if (StringUtils.isNotEmpty(params.getAoApplicationReferenceNumber())) {
					AOReferenceNumber = params.getAoApplicationReferenceNumber();																		
				}
				log.debug(AOReferenceNumber);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			if (StringUtils.isNotEmpty(params.getAoApplicationReferenceNumber())) {
				AOReferenceNumber = params.getAoApplicationReferenceNumber();																		
			}
			log.debug(AOReferenceNumber);
			return AOReferenceNumber;
			//throw new AOException("Application could not be submitted to AO, Please contact your system administrator");
		}/* catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new AOException("Application could not be submitted to AO, Please contact your system administrator");
		}*/
		if (StringUtils.isNotEmpty(params.getAoApplicationReferenceNumber())) {
			AOReferenceNumber = params.getAoApplicationReferenceNumber();																		
		}
		log.debug(AOReferenceNumber);
		return AOReferenceNumber;

	}

}
