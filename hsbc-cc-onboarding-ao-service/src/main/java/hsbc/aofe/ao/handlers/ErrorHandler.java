package hsbc.aofe.ao.handlers;

public interface ErrorHandler {
	
public void extractError(String response, String errorId, StringBuilder errorString);
	
	void setNextChain(ErrorHandler nextChain);

}
