package hsbc.aofe.ao.handlers;

import java.util.HashMap;
import java.util.Map;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.constants.AOPageEnum;

public class ErrorProcessor {
	
	private ErrorHandler c1;
	Map<String,String> errorClassMap = new HashMap<String,String>();
	
	public ErrorProcessor() {
		//create the map
		
		errorClassMap.put(AOPageEnum.AOENTRY.name(), null);
		errorClassMap.put(AOPageEnum.SELECTCARD.name(), null);
		errorClassMap.put(AOPageEnum.CONTINUEJS.name(), null);
		errorClassMap.put(AOPageEnum.CONTINUESECONDJS.name(), null);
		errorClassMap.put(AOPageEnum.PERSONALDETAILS.name(), PropertiesCache.getInstance().getProperty("error.validation.personal.details.id"));
		errorClassMap.put(AOPageEnum.EMPLOYMENTDETAILS.name(), PropertiesCache.getInstance().getProperty("error.validation.employment.details.id"));
		errorClassMap.put(AOPageEnum.SUPPLEMENTARY.name(), null);
		errorClassMap.put(AOPageEnum.REVIEWDETAILS.name(), null);
		errorClassMap.put(AOPageEnum.DECISIONING.name(), null);
		errorClassMap.put(AOPageEnum.BALANCETRANSFER.name(), null);
		errorClassMap.put(AOPageEnum.CONFIGPRODUCT1.name(), null);
		errorClassMap.put(AOPageEnum.CONFIGPRODUCT2.name(), null);
		
		// initialize the chain
		this.c1 = new ValidationErrorHandler();
		ErrorHandler c2 = new ApplicationIdAPEErrorHandler();
		ErrorHandler c3 = new GeneralErrorHandler();

		// set the chain of responsibility
		c1.setNextChain(c2);
		c2.setNextChain(c3);
	}
	
	public String handleResponseError(String html, String page){
		
		StringBuilder builder = new StringBuilder();
		c1.extractError(html, errorClassMap.get(page), builder);
		
		return builder.toString();
	}

}
