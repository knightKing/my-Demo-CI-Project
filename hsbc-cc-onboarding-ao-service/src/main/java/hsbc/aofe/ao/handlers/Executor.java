package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SINGLE_CARD;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.HttpHostConnectException;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.exception.AOException;
import lombok.extern.slf4j.Slf4j;
@Slf4j
// TODO :Add Javadoc
public class Executor {

    private List<Handler> handlers;
    private List<Handler> handlerSingleDenied;
    private List<Handler> handlerDeclined1CardOfDual;

    public String execute(RequestParameters params, Application application) {

        String AOReferenceNumber = "";
        

        List<Handler> list = new LinkedList<Handler>();
        if(params.getNumberOfCards().equals(SINGLE_CARD)){
       //List for single card execution:
        list.add(new LaunchTpsaLinkHandler());
		list.add(new StepAoEntryCardSpecsHandler());
		list.add(new ContinueJsHandler());
		list.add(new ContinueJsSecondHandler());
		list.add(new RedirectToStep1Handler());
		list.add(new StepGad1PersonalDetailsHandler());
		list.add(new StepGad2EmpolymentDetailsHandler());
		list.add(new Step3WithDataDecor());				
		list.add(new Step4ReviewDetailsWdSuppHandler());		
		//list.add(new RedirectToStep5Handler());
		list.add(new Step5ConfigureProductHandler());
		//list.add(new RedirectToStep6Handler());
       
        }else{
        	 list.add(new LaunchTpsaLinkHandler());
     		list.add(new StepAoEntryCardSpecsHandler());
     		list.add(new ContinueJsHandler());
     		//list.add(new ContinueJsHandler());
     		list.add(new ContinueJsSecondHandler());
     		list.add(new RedirectToStep1Handler());
     		list.add(new StepGad1PersonalDetailsHandler());
     		list.add(new StepGad2EmpolymentDetailsHandler());
     		list.add(new Step3WithDataDecor());				
     		list.add(new Step4ReviewDetailsWdSuppHandler());		
     		//list.add(new RedirectToStep5Handler());
     		list.add(new Step5ConfigBothProductHandler());
     		//list.add(new RedirectToStep5ConfigProduct1Handler());
     		list.add(new Step5ConfigProduct1Handler());
     		//list.add(new RedirectToStep5Handler());
     		list.add(new Step5ConfigProduct1SecondHandler());
     		list.add(new Step5ConfigProduct2Handler());
     		//list.add(new RedirectToStep6Handler());
     		
     		
     		/*list.add(new Step5ConfigProduct1Handler());
     		list.add(new RedirectToStep5ConfigProduct2Handler());
     		list.add(new Step5ConfigProduct1SecondHandler());
     		list.add(new RedirectToStep5ConfigProduct2SecondHandler());
     		list.add(new Step5ConfigProduct2Handler());
     		list.add(new RedirectToStep6Handler());*/
        }
		
		
        List<Handler> listDeniedCaseSingleCard = new LinkedList<Handler>();
        listDeniedCaseSingleCard.add(new Step5ProductConfigDenied());
		
        
        List<Handler> listDeclined1CardOfDual = new LinkedList<Handler>();
        listDeclined1CardOfDual.add(new Step5ConfigProduct1Handler());
        
        
        
        handlers = Collections.unmodifiableList(list);
        handlerSingleDenied = Collections.unmodifiableList(listDeniedCaseSingleCard);
        handlerDeclined1CardOfDual = Collections.unmodifiableList(listDeclined1CardOfDual);
        
        try {
        	boolean result = true;
        	
        
        	for (Handler handler : handlers) {
                result = handler.execute(params, application);
               if(!result) {
            	   if(params.getNumberOfCards().equals(SINGLE_CARD)){
            	   for (Handler handlerDenied : handlerSingleDenied) {
            		   handlerDenied.execute(params, application);
                       
                       AOReferenceNumber = "Application is Denied by AO:"+params.getAoApplicationReferenceNumber();
                       log.debug(AOReferenceNumber);
                   }}
            	   else{
            		   // if flag=true
            		   // then add handler for dual for bt of success card
            		   //else denied
            		   if(params.isCreditCardDeclineCheck()){
            			   for (Handler handlerDeclined1CardOf2 : handlerDeclined1CardOfDual) {
            				   handlerDeclined1CardOf2.execute(params, application);
                               
                               AOReferenceNumber = "Application is Denied by AO:"+params.getAoApplicationReferenceNumber();
                               log.debug(AOReferenceNumber);
                           }
            		   }
            		   else{
            		   AOReferenceNumber = "Application is Denied by AO:"+params.getAoApplicationReferenceNumber();
            		   }
            	   }
                	break;
                }
                    AOReferenceNumber = RequestUtil.aoRefNumber(params);
                    log.debug(AOReferenceNumber);
                }
        	
			/*
			 * if(!result) {
			 * 
			 * }
			 */
		} catch (HttpHostConnectException excp) {
			AOException aoException = new AOException(
					"Unable to connect to AO, Please check network or proxy settings.");
			aoException.setErrorCode(4100);
			throw aoException;
		}

		catch (Exception e) {
			log.error(e.getMessage(), e);
			if (StringUtils.isNotEmpty(params.getAoApplicationReferenceNumber())) {
				AOReferenceNumber = "ERROR: " + params.getAoApplicationReferenceNumber();
			}
			log.debug(AOReferenceNumber);
			return AOReferenceNumber;
			// throw new AOException("Application could not be submitted to AO,
			// Please contact your system administrator");
		} /*
			 * catch (Exception e) { log.error(e.getMessage(), e); throw new
			 * AOException("Application could not be submitted to AO, Please contact your system administrator"
			 * ); }
			 */
       /* AOReferenceNumber = RequestUtil.aoRefNumber(params);*/
        log.debug(AOReferenceNumber);
       
        return AOReferenceNumber;

    }
}

