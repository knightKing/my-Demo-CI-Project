package hsbc.aofe.ao.handlers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import hsbc.aofe.ao.config.PropertiesCache;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GeneralErrorHandler implements ErrorHandler {
	
	private ErrorHandler chain;


	@Override
	public void setNextChain(ErrorHandler nextChain) {
		this.chain = nextChain;

	}
	
	@Override
	public void extractError(String html, String validationErrorId, StringBuilder errorString) {
		String portletIsDiabledId = PropertiesCache.getInstance().getProperty("error.block.cs.main.id");
	
			Element csMainIdBlock = extractCsMainErrorBlock(html);
			if(null!=csMainIdBlock){
				
			Element generalErrorElement = csMainIdBlock.getElementById(portletIsDiabledId);
			String errorText = generalErrorElement.children().text();
			log.debug("Error extracted from AO response : "+errorText);
			System.out.println("Error extracted from AO response : "+errorText);
			errorString.append(errorText);
			errorText="AO Error Response:".concat(errorText);
			}else{
				errorString=null;
			}

		

	}
	
	private Element extractCsMainErrorBlock(String html) {
		String extractCsMainErrorBlock = PropertiesCache.getInstance().getProperty("error.block.cs.main.id");
		Document doc = Jsoup.parse(html);
		try {
			Element csMainErrorsElementBlock = doc.getElementById(extractCsMainErrorBlock);			
			return csMainErrorsElementBlock;
		}
		catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}

	}

}
