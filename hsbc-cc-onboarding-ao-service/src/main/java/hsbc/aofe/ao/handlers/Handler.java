package hsbc.aofe.ao.handlers;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.domain.Application;

//TODO :Add Javadoc

public interface Handler {

	boolean execute(RequestParameters params, Application application) throws Exception;

}
