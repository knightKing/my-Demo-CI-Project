package hsbc.aofe.ao.handlers;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.GetRequest;
import hsbc.aofe.domain.Application;
import hsbc.aofe.exception.AOException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LaunchTpsaLinkHandler implements Handler {

    public boolean execute(RequestParameters params, Application application) throws Exception {

        GetRequest getRequestObj = new GetRequest();
        /*try {*/
        log.info("STARTING: Lauch TPSA Link Handler execute method for arn {}", application.getArn());
        params.setCookies(getRequestObj.getPageContent(params));
        log.info("FINISHED: Lauch TPSA Link Handler execute method for arn {}", application.getArn());
        /*} catch (Exception e) {
            log.error("Error in Launch Tpsa Link Handler", e);
			//throw new AOException("Error in Launch Tpsa Link Handler", e);
		}*/
        return Boolean.TRUE;
    }

}
