package hsbc.aofe.ao.handlers;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.GetRequest;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RedirectToStep1Handler implements Handler {

    @Override
    public boolean execute(RequestParameters params, Application application) throws Exception {

        String applicationArn = application.getArn();
        log.info("STRATING: Redirect to Step 1 handler execute method for applicationArn {}", applicationArn);

        /*try {*/
        GetRequest getRequestObj = new GetRequest();
        String responseString = getRequestObj.getPageRedirect(params);
        log.info("Response recorded for Redirect Step 1 for applicationArn {}, Response: {}", applicationArn, responseString);

        params.setResponseRedirectStep1(responseString);
        RequestUtil.extractDynamicParamsStep1(responseString, params);
        log.info("Parameters Extracted from Redirect To Step 1 Handler response for Step Gad 1 Personal Details Handler for applicationArn {}",
                applicationArn);
        /*}catch (Exception e) {
            log.error("Error in Redirect To Step1 Handler", e);
			//throw new AOException("Error in Redirect To Step1 Handler", e);
		}*/
        log.info("FINISHED:  Redirect To Step 1 Handler execute method for applicationArn {}", applicationArn);
        return Boolean.TRUE;
    }

}
