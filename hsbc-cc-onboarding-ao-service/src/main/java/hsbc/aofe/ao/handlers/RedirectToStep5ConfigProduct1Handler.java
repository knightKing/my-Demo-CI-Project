package hsbc.aofe.ao.handlers;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.GetRequest;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RedirectToStep5ConfigProduct1Handler implements Handler {
	public boolean execute(RequestParameters params, Application application) throws Exception {
		/*try {*/
			GetRequest getRequestObj = new GetRequest();
			log.info("Strating Redirect to Step 5 handler execute method for arn {}", application.getArn());
			String responseRedirect = getRequestObj.getPageRedirect(params);
			params.setResponseRedirectStep5Card1(responseRedirect);
			log.info("Response recorded for Redirect Step 5 for arn {}", application.getArn());
			boolean aoApplicationStatus = RequestUtil.applicationDeniedStatus(responseRedirect);
			if(aoApplicationStatus){
				log.info(
						"Application is denied by AO for AO reference number {} ",params.getAoApplicationReferenceNumber()+ "and arn {}"+
						application.getArn());
				log.info("Finished  AO execution for  {}", application.getArn());
				return false;
			}
			else{
				RequestUtil.extractDynamicParamsStep5Card1(responseRedirect, params);	
			}
						
			log.info(
					"Parameters Extracted from Redirect To Step 5 Handler response for Step 5 Configure Details Handler for arn {}",
					application.getArn());
			log.info("Finished  Redirect To Step 5 Handler execute method for arn {}", application.getArn());
		/*} catch (Exception e) {
			log.error("Error in Redirect To Step5 Handler", e);
			//throw new AOException("Error inRedirect To Step5 Handler", e);
		}*/
		return true;
	}

}

	
	
	
	
	/*String resp= postRequest.postPageResponse(nvpList,params);
	params.setResponseStep5DecisionDetails(resp);
	RequestUtil.extractDynamicParamsStep1(resp,params);
}
*/