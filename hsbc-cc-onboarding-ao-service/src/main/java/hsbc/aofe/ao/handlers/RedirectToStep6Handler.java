package hsbc.aofe.ao.handlers;

import org.apache.commons.lang3.StringUtils;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.GetRequest;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.exception.AOException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RedirectToStep6Handler implements Handler {
	public boolean execute(RequestParameters params, Application application) throws Exception {
	/*	try {*/
			log.info("Strating Redirect to Step 6 handler execute method for arn {}", application.getArn());
			GetRequest getRequestObj = new GetRequest();
			params.setResponseRedirectStep6(getRequestObj.getPageRedirect(params));
			log.info("Response recorded for Redirect Step 1 for arn {}", application.getArn());

			String applicationId = RequestUtil.extractApplicationIdFromCompletePage(params.getResponseRedirectStep6());
			if (StringUtils.isNotEmpty(applicationId)) {
				params.setAoApplicationReferenceNumber(applicationId);
			}
			System.out.println("Application Reference Number:"+applicationId);		
			log.debug(applicationId);
			log.info(
					"Application Reference Number Extracted from Redirect To Step 6 Handler response for returning  AO application reference number for arn {}",
					application.getArn());
			log.info("Finished  Redirect To Step 6 Handler execute method for arn {}", application.getArn());
		/*} catch (Exception e) {
			log.error("Error in Redirect To Step6 Handler", e);
			//throw new AOException("Error in Redirect To Step6 Handler", e);
		}*/
		return true;
	}

}
