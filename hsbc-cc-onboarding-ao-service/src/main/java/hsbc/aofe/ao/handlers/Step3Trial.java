package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.CONTINUE_TO_NEXT_PAGE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_IDCL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_PAGE_DETAILS;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.WIRE_ACTION;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.BLANK_FOR_NO_SUPP_CARD;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.DEFAULT_NONE_FOR_NO_SUPP_CARD;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.DEFAULT_TRUE_FOR_NO_SUPP_CARD;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.JSF_SEQUENCE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.MAX_PERMISSIBLE_CARD_HOLDERS;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.data.Step3TrialData;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.exception.AOException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Step3Trial  implements Handler {

	//TODO : DAFAULT VALUES THAT HAVE TO BE POSTED WHEN NO SUPPLEMENTARY CARD IS CHOSEN.
		public boolean execute(RequestParameters params, Application application) throws Exception {
			/*try {*/
			log.info("Strating Step GAD3 Supplementary Details handler execute method for arn {}",application.getArn());
			PostRequest postRequest = new PostRequest();
			List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
			Step3TrialData dataList = new Step3TrialData();
			nvpList = dataList.dataList();
			nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));			
						
			/*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.first.name")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	Sunita	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.first.name1"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.first.name2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.last.name")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	Bhambani	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.last.name1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.last.name2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.middle.name"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.middle.name1"),	BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.middle.name2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.max.permissible.card.holders"),	MAX_PERMISSIBLE_CARD_HOLDERS));	//TODO :		Value :	9	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.secondary.counter")	, "1"));	//TODO :	CHECK TPSA	Value :	1	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.secondary.cardholder.indicator"), application.getAddSupplimentaryCard()==true?"Y":"N"));	//TODO :A:	Value Y/N	Value :	N FOR NO SUPPLEMENTARY CARD	DEFAULT : 	

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														

			nvpList.add(new BasicNameValuePair("ao-gad-schd-nationality","SG"));
			nvpList.add(new BasicNameValuePair("ao-gad-schd-nationality1","SG"));
			nvpList.add(new BasicNameValuePair("ao-gad-schd-nationality2","SG"));
			
			
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.months")	, "NONE"));	//TODO :		Value :	November	DEFAULT : 	NONE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.date")	, "NONE"));	//TODO :		Value :	12	DEFAULT : 	NONE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.year")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	1970	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.cardholder.name")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	Sunita Bhambani	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.cardholder.name.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.cardholdername.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.nationality"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.nationality.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.nationality.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.homenumber.area.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.homenumber.area.2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.homenumber.int.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.homenumber.int.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.homenumber.no1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.homenumber.no2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.sec.secondary.title.other")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.thirdtitleother")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.title")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	MRS	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.title1")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.title2")	 ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.titleother")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.month.1"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	NONE	DEFAULT : 	NONE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.date.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	NONE	DEFAULT : 	NONE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.year.1"),	BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identication.issue.country.code")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identication.issue.country.code.1"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identication.issue.country.code.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identification.number")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	S7416076Z	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identification.number.1"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identification.number.2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identification.type")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	I	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identification.type.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.identification.type.2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.1.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.1.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.2")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.2.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.2.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.3")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.3.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.address.line.3.2")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.country")	, "SG"));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.country.1")	, "SG"));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.country.2")	, "SG"));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.zipcode")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.zipcode.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.zipcode.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.month.2"),	BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	NONE	DEFAULT : 	NONE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.dob.date.2")	 ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	NONE	DEFAULT : 	NONE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.year.2")	 ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.first.relationship.primary.app"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	P	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.gender")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	F	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.have.my.hpr.status.2"),	BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.relationship.bank")	 ,"N"));	//TODO :		Value :	N	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.relationship.bank.details")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.relationship.bank.details.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.relationship.bank.details.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.second.relationship.primary.app")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.third.relationship.primary.app")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.area.code")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.area.code.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.area.code.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.int")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	91	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.int.1") ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.int.2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.no")	 ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	9871235640	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.no.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.mobile.number.no.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.area.code")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.area.code.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.area.code.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.int")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.int.1")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.int.2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.no")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.no.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.office.number.no.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.former.last.name")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.former.last.name.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.former.last.name.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.haveany.former.names")	, "N"));	//TODO :		Value :	N	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.other.last.name")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.other.last.name.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.other.last.name.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.supplementary.declaration.accept.box"),	 ""));	//TODO :		Value :	TRUE	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.company.name")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	BVB Asma School	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.company.name.1")	 ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.company.name.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.employment.status"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	F	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.employment.status.1")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.employment.status.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.occupation")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	58	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.occupation.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.occupation.2")	 ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.addr.indicator")	,DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.addr.indicator.1")	 ,DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.addr.indicator.2")	, DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.1.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.1.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.2.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.2.2")	 , BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.3")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.3.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.address.line.3.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.country")	, "SG"));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.country.1")	, "SG"));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.country.2")	, "SG"));	//TODO :		Value :	SG	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line.1.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line1.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line.2.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line.2.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line.3")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line.3.1")	,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.foreign.address.line3.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.zipcode")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.zipcode.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.permanent.zipcode.2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line.1.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line1.2"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line.2")	 ,BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line2.1")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line.2.2")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line.3"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line3.1"),	 BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.residential.foreign.address.line3.2"),	BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.first.residential.addr.indicator"),	DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.first.residential.addr.indicator.1")	, DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.first.residential.addr.indicator.2")	, DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.second.residential.addr.indicator")	 ,DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.second.residential.addr.indicator.1")	, DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.second.residential.addr.indicator.2")	, DEFAULT_TRUE_FOR_NO_SUPP_CARD));	//TODO :		Value :	TRUE	DEFAULT : 	TRUE
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.nature.of.relationship")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.nature.of.relationship.1"),	BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.nature.of.relationship.2"),	BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.senior.public.office"),	 "N"));	//TODO :		Value :	N	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.telephone.area")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :		DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.telephone.int")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	11	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.telephone.no")	, BLANK_FOR_NO_SUPP_CARD));	//TODO :		Value :	23235656	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.cmd.continue.review.page"), CONTINUE_TO_NEXT_PAGE));	//TODO :	static common	Value :	Continue	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.jsf.sequence")	, JSF_SEQUENCE));	//TODO :	static common	Value :	6	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.idcl")	,SUBMIT_IDCL));	//TODO :	static common	Value :	submit	DEFAULT : 	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.scd.submit")	, SUBMIT_PAGE_DETAILS));	//TODO :	static common	Value :	1	DEFAULT : 		
*/	    //    nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.scd.product.tracker.id.list.string")	,params.getCbaProductTrackerIDListString()));	
	        nvpList.add(new BasicNameValuePair("cardTypeList", params.getCardTypeList()));		
	        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.product.list.1"), params.getProductList1()));	
			//DYNAMIC VALUES : yet to be added ao-hbap-gad-cscdp-cbaProductTrackerIDListString:08041335935827139,
	        nvpList.add(new BasicNameValuePair("ao-hbap-gad-cscdp-cbaProductTrackerIDListString",params.getCbaProductTrackerIDListString()));	
	        log.info("Posting parameters for Step GAD3 Supplementary Details handler for arn {}",application.getArn());
			params.setResponseStep3SupplementaryDetails(postRequest.postPageResponse(nvpList,params)); 
			log.info("Response recorded for  Step GAD3 Supplementary Details handler for arn {}",application.getArn());
			RequestUtil.extractDynamicParamsStep4(params.getResponseStep3SupplementaryDetails(),params);
			log.info("Parameters Extracted from Step GAD3 Supplementary Details handler response for Step Gad4 Review Details Handler for arn {}",application.getArn());
			log.info("Finished Step GAD3 Supplementary Details Handler execute method for arn {}",application.getArn());
			/*}catch (Exception e) {
				log.error("Error in Step Gad3 Supplementary Details Handler", e);
				//throw new AOException("Error in  Gad3 Supplementary Details Handler", e);
			}*/
			return true;
		}

	}
