package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SINGLE_CARD;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.data.Step3SupplementaryDataDecorator;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.CreditCard;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Step3WithDataDecor implements Handler {

	
		public boolean execute(RequestParameters params, Application application) throws Exception {
			/*try {*/
			log.info("Strating Step GAD3 Supplementary Details handler execute method for arn {}",application.getArn());
			PostRequest postRequest = new PostRequest();
			List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
			Step3SupplementaryDataDecorator dataList = new Step3SupplementaryDataDecorator();
			nvpList = dataList.dataDecorator(params, application);
			nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
			
			 if(params.getNumberOfCards().equals(SINGLE_CARD)){
				 nvpList.add(new BasicNameValuePair("cardTypeList", params.getCardTypeList()));
				 nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.product.list.1"), params.getProductList1()));	
			 }
			 else{
			        //since all supplementary have same cards so using 0th index
			        if(null == application.getSupplementaryApplicant() || application.getSupplementaryApplicant().get(0).getCards().size()==2){
			        	nvpList.add(new BasicNameValuePair("cardTypeList", params.getCardTypeList()));		
				        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.product.list.1"), params.getProductList1()));	
				        nvpList.add(new BasicNameValuePair("cardTypeList", params.getCardTypeListSecond()));		
				        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.product.list.1"), params.getProductList1Second()));
			        	
			        }else if(application.getSupplementaryApplicant().get(0).getCards().size()==1){
			        	CreditCard suppCard = application.getSupplementaryApplicant().get(0).getCards().get(0);
			        	if(suppCard.getKey().equalsIgnoreCase((params.getSingleCardForSupplementary()))){
			        		nvpList.add(new BasicNameValuePair("cardTypeList", params.getCardTypeList()));		
					        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.product.list.1"), params.getProductList1()));	
					       
				        		
			        	}
			        	else{
			        		 nvpList.add(new BasicNameValuePair("cardTypeList", params.getCardTypeListSecond()));		
						      nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.product.list.1"), params.getProductList1Second()));
			        	}
			        }
			        
			        
			        
			        
			 }
		    
			//DYNAMIC VALUES : yet to be added ao-hbap-gad-cscdp-cbaProductTrackerIDListString:08041335935827139,
	        nvpList.add(new BasicNameValuePair("ao-hbap-gad-cscdp-cbaProductTrackerIDListString",params.getCbaProductTrackerIDListString()));
	        
	        
	        log.info("Posting parameters for Step GAD3 Supplementary Details handler for arn {}",application.getArn());
			params.setResponseStep3SupplementaryDetails(postRequest.postPageResponse(nvpList,params)); 
			log.info("Response recorded for  Step GAD3 Supplementary Details handler for arn {}",application.getArn());
			 if(params.getNumberOfCards().equals(SINGLE_CARD)){
			RequestUtil.extractDynamicParamsStep4(params.getResponseStep3SupplementaryDetails(),params);}
			 else{
				 RequestUtil.extractDynamicParamsStep4DualCard(params.getResponseStep3SupplementaryDetails(),params); 
			 }
			log.info("Parameters Extracted from Step GAD3 Supplementary Details handler response for Step Gad4 Review Details Handler for arn {}",application.getArn());
			log.info("Finished Step GAD3 Supplementary Details Handler execute method for arn {}",application.getArn());
			/*}catch (Exception e) {
				log.error("Error in Step Gad3 Supplementary Details Handler", e);
				//throw new AOException("Error in  Gad3 Supplementary Details Handler", e);
			}*/
			return true;
		}

	}

