package hsbc.aofe.ao.handlers;


import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SINGAPORE_COUNTRY_CODE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SINGLE_CARD;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_IDCL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_PAGE_DETAILS;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.ReviewDetails.CONSENT_ON;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.ReviewDetails.CONTINUE_SUBMIT_FOR_APPROVAL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.ReviewDetails.JSF_SEQUENCE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.ReviewDetails.PREFERRED_LANGUAGE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.ReviewDetails.UIC;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.ReviewDetails.WIRE_ACTION;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.SupplementaryCardDetails.BLANK_FOR_NO_SUPP_CARD;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.CollectionUtils;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.constants.AOPageEnum;
import hsbc.aofe.ao.data.RequestParameters;


import hsbc.aofe.ao.data.Step4TrialData;
import hsbc.aofe.ao.data.SuppApplicants;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Address;
import hsbc.aofe.domain.Applicant;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.domain.DocumentName;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Step4ReviewDetailsWdSuppHandler implements Handler {
	
	public boolean execute(RequestParameters params,Application application) throws Exception {
		/*try {*/

		String hostUrl = PropertiesCache.getInstance().getProperty("host.url");
		log.info("Strating Step GAD4 Review Details handler execute method for arn {}",application.getArn());
		PostRequest postRequest = new PostRequest();
		List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
		
		Step4TrialData dataList = new Step4TrialData();
		//nvpList = dataList.csvDataList();
		nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
		
	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.uic"),UIC));	 		 //TODO :		Value:			
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constants.rd.wire.action"),WIRE_ACTION));		 //TODO :		Value:	SendActionGAD		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.address.format.code"),"HUB"));			 //TODO :	CHECK TPSA VALUE	Value:	HUB		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.preferred.language"),PREFERRED_LANGUAGE));			 //TODO :		Value:	ENG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.contact.preferences"), "Y"));			 //TODO :		Value:	Y		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.max.permissible.card.holders"),"9"));		 //TODO :		Value:	9		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.secondary.counter"), "1"));			 //TODO :		Value:	1		
		
		List<Address> addressList = application.getPrimaryApplicant().getAdditionalInfo().getAddress();
		for(Address address : addressList){
			switch (address.getAddressType()) {
			case PERMANENT:
				if(address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)){
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address()))); // how value will be mapped	//TODO : Value:	L 123
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.2"),DataMappingListUtil.getValidValue(address.getLine2Address()))); 	//TODO :// how value will be mapped, Value:	Lake Street
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address())));	//TODO :how value will be mapped, Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));	//TODO :how value will be mapped, Value:	SG  DD Values
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.country.hidden"),	DataMappingListUtil.getValidValue(address.getCountry().getValue())));
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.zipcode.1"), DataMappingListUtil.getValidValue(String.valueOf(address.getPostalCode()))));	//TODO :how value will be mapped, Value:	123456
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.1"),""));	//TODO :how value will be mapped, Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.2"), ""));	//TODO : how value will be mapped,Value:				        
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.3"), ""));	//TODO : how value will be mapped,Value:
				}else {
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));	//TODO :how value will be mapped, Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));	//TODO : how value will be mapped,Value:				        
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address())));	//TODO : how value will be mapped,Value:
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));	//TODO :how value will be mapped, Value:	SG  DD Values
			        
			        //Permanent country hidden check value ??????
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.country.hidden"),	 DataMappingListUtil.getValidValue(address.getCountry().getValue())));
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.1"),"")); // how value will be mapped	//TODO : Value:	L 123
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.2"),"")); 	//TODO :// how value will be mapped, Value:	Lake Street
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.3"), ""));	//TODO
				}
				break;
			case RESIDENTIAL:
				if(address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)){
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address()))); // check value for address 1	//TODO : Value:	L 123
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address()))); // check value for address 2	//TODO : Value:	abc
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address()))); // check value for address 3	//TODO : Value:	Lake Street
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));	//TODO : Value:	HOW TO PUT VALUE?? SG
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.zipcode.1"), DataMappingListUtil.getValidValue(String.valueOf(address.getPostalCode())))); //PRACHI TPSA how to take string from long postal code	//TODO : Value:	123456			        
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.1"),  ""));	//TODO : how value will be mapped, Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.2"), ""));//TODO :how value will be mapped, Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.3"),  ""));	//TODO : how value will be mapped,Value:
				}else {
					 nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));	//TODO : Value:	HOW TO PUT VALUE?? SG
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));	//TODO : how value will be mapped, Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));	//TODO :how value will be mapped, Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address())));	//TODO : how value will be mapped,Value:	
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.1"),"")); // check value for address 1	//TODO : Value:	L 123
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.2"), "")); // check value for address 2	//TODO : Value:	abc
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.3"), "")); // check value for address 3	//TODO : Value:	Lake Street			       
			        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.zipcode.1"), "")); //PRACHI TPSA how to take s
				}
				
		        break;
			case OFFICE:
				nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.employercountry_oh1"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));	//TODO :		Value:	SG		
				nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.credit.card.employer.country"),	DataMappingListUtil.getValidValue(address.getCountry().getValue())));	
				break;
			case PREVIOUS:
				nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.previous.residential.country"),DataMappingListUtil.getValidValue(address.getCountry().getValue())));//TODO :		Value:	SG		
		        break;
			default:
				break;
			}
	    }	
		
		
		/*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.1"), "56")); // how value will be mapped	//TODO : Value:	L 123
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.2"),"Tanglin Street")); 	//TODO :// how value will be mapped, Value:	Lake Street
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.address.line.3"), ""));	//TODO :how value will be mapped, Value:	
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.country"),"SG"));	//TODO :how value will be mapped, Value:	SG  DD Values
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.country.hidden"),	""));
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.zipcode.1"), "247964"));	//TODO :how value will be mapped, Value:	123456
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.1"),""));	//TODO :how value will be mapped, Value:	
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.2"), ""));	//TODO : how value will be mapped,Value:				        
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.permanent.foreign.address.line.3"), ""));	//TODO : how value will be mapped,Value:
		
        
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.1"), "56")); // check value for address 1	//TODO : Value:	L 123
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.2"), "Tanglin Street")); // check value for address 2	//TODO : Value:	abc
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.address.line.3"),"")); // check value for address 3	//TODO : Value:	Lake Street
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.country"), "SG"));	//TODO : Value:	HOW TO PUT VALUE?? SG
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.zipcode.1"), "247964")); //PRACHI TPSA how to take string from long postal code	//TODO : Value:	123456			        
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.1"),  ""));	//TODO : how value will be mapped, Value:	
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.2"), ""));//TODO :how value will be mapped, Value:	
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.residential.foreign.address.line.3"),  ""));	//TODO : how value will be mapped,Value:

        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.employercountry_oh1"), "SG"));	//TODO :		Value:	SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.credit.card.employer.country"),	""));	
        
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.rd.previous.residential.country"),"SG"));//TODO :		Value:	SG 
    	*/
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.mypdpatermsdeclaration-checkbox"), CONSENT_ON));			 //TODO :		Value:	on		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.citizenshiphidden"),	DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNationality().getValue(),"")));			 //TODO :	Value from DD values	Value:	SG		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.countryofbirthhidden"), "SG"));			 //TODO :	Value from DD values	Value:	SG		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.countryofbirthhidden"), DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getCountryOfBirth().toString())));
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.multiple-citizenship-text"),	 application.getPrimaryApplicant().getAdditionalInfo().isMultipleNationality()==true?"Y":"N"));			 //TODO :		Value:	N		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.referral"),	application.getPrimaryApplicant().getAdditionalInfo().isIntroducedByHSBCCardHolder() == false ?"No":"Yes"));	
		//TODO :		Value:	No		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.tax1hidden"),	 "SG"));			 //TODO :		Value:	SG
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.tax1hidden"),	  DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getTaxResidenceCountry1().getValue())));			 //TODO :		Value:	SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.tax2-text"),	 application.getPrimaryApplicant().getAdditionalInfo().getTaxResidenceCountry2()==null?"":application.getPrimaryApplicant().getAdditionalInfo().getTaxResidenceCountry2().getValue()));			 //TODO :	Value from DD values	Value:			
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.tax3-text"),	application.getPrimaryApplicant().getAdditionalInfo().getTaxResidenceCountry3()==null?"":application.getPrimaryApplicant().getAdditionalInfo().getTaxResidenceCountry2().getValue()));		 //TODO :		Value:			
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.consentcreditref"),CONSENT_ON));		 //TODO :	consent not mapped but on	Value:	on		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.prefix"),  DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getSalutation().getValue())));			 //TODO :	Value from DD values	Value:	MISS		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.preferreddispatchmodetext"),	 "Mail"));			 //TODO :	Static common	Value:	Mail		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.employmentstatus"),DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getEmploymentType().getAoValue())));
		//TODO :	Value: M/F	Value:	F			
		
		
		
	/*	nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality"), DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNationality().getValue(),"SG")));			 //TODO :	Static common	Value:	SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality1"), application.getPrimaryApplicant().getAdditionalInfo().getNationality2()==null?"SG":application.getPrimaryApplicant().getAdditionalInfo().getNationality2().getValue()));			 //TODO :	Static common	Value:	SG		
		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality2"), application.getPrimaryApplicant().getAdditionalInfo().getNationality3()==null?"SG":application.getPrimaryApplicant().getAdditionalInfo().getNationality3().getValue()));			 //TODO :		Value:	SG		
		*/
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.employmentstatus"),"F"));			 //TODO :	Value: M/F	Value:	F
		/*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality"),"SG"));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality1"), "SG"));		


		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality2"), "SG"));				
		*/

		
		
		
		/*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.identificationtype"),	 ""));			 //TODO :		Value:	I		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.identificationtype1"),""));			 //TODO :		Value:			
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.identificationtype2"),""));			 //TODO :		Value:			
		*/
		
		
		/*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.country"),	"SG"));			 //TODO :		Value:	SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.country1"),	"SG"));			 //TODO :		Value:	SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.country2"),	"SG"));			 //TODO :		Value:	SG		
		*///nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.businessLine"),"NA"));	 //TODO :	?? CHECK TYPSA FOR EVERY PAGE	Value:	NA		
		 //TODO :			SGD		
/*		nvpList.add(new BasicNameValuePair("ao-gad-wt-businessLine", "NA")); //TODO :,Non mandatory,Value:,NA
		nvpList.add(new BasicNameValuePair("ao-gad-wt-customerGroup", "PFS")); //TODO :,Non mandatory,Value:,PFS
		nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_1", "Visa Platinum")); //TODO :,Non mandatory,Value:,Visa Platinum
		nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_2", "IB")); //TODO :,Non mandatory,Value:,IB
		nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_3", "HBSP_Visa Platinum")); //TODO :,Non mandatory,Value:,HBSP_Visa Platinum
		
		nvpList.add(new BasicNameValuePair("ao-gad-wt-language", "en")); //TODO :,Non mandatory,Value:,en
		nvpList.add(new BasicNameValuePair("ao-gad-wt-promoCode", "DEF0000001")); //TODO :,Non mandatory,Value:,DEF0000001
		nvpList.add(new BasicNameValuePair("ao-gad-wt-retrieveAppIndicator", "N")); //TODO :,Non mandatory,Value:,N
*/		//TODO to be checked					

		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.credit.card.send.my.mails.to.hidden"),	 "A"));			 //TODO :			A		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.relationship.bank"),	 application.getPrimaryApplicant().getAdditionalInfo().getAssociateOfPublicPositionHolder()==true?"Yes":"No"));			 //TODO :			No		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.haveany.former.names.hidden.rscnd"),	application.getPrimaryApplicant().getAdditionalInfo().getOtherName()==null?"No":"Yes"));			 //TODO :			No		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.secondary.cardholder.indicator.hidden"),	CollectionUtils.isEmpty(application.getSupplementaryApplicant())?"N":"Y"));		 //TODO :			Y		
		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.secondary.cardholder.indicator.hidden"),	"N"));			 //TODO :			Y

SuppApplicants suppApplicant = null;

		int noOfSupp = 0;
		if (!CollectionUtils.isEmpty(application.getSupplementaryApplicant())) {
			noOfSupp = application.getSupplementaryApplicant().size();
			suppApplicant = new SuppApplicants();
		} else {
			suppApplicant = new SuppApplicants();
		}

		if (1 == noOfSupp) {
			populateFirstSupplimentry(suppApplicant, application.getSupplementaryApplicant().get(0));
		} else if (2 == noOfSupp) {
			populateFirstSupplimentry(suppApplicant, application.getSupplementaryApplicant().get(0));
			populateSecondSupplimentry(suppApplicant, application.getSupplementaryApplicant().get(1));
		} else if (3 == noOfSupp) {
			populateFirstSupplimentry(suppApplicant, application.getSupplementaryApplicant().get(0));
			populateSecondSupplimentry(suppApplicant, application.getSupplementaryApplicant().get(1));
			populateThirdSupplementry(suppApplicant, application.getSupplementaryApplicant().get(2));
		}
		

		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality"),suppApplicant.getUser_rd_nationality()));	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality1"), suppApplicant.getUser_rd_nationality1()));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.nationality2"), suppApplicant.getUser_rd_nationality2()));		
		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.identificationtype"),	suppApplicant.getUser_rd_identificationtype()));		 //TODO :		Value:	I		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.identificationtype1"),suppApplicant.getUser_rd_identificationtype1()));			 //TODO :		Value:			
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.identificationtype2"),suppApplicant.getUser_rd_identificationtype2()));			 //TODO :		Value:			
		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.country"),	suppApplicant.getUser_rd_country()));			 //TODO :		Value:	SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.country1"),	suppApplicant.getUser_rd_country1()));			 //TODO :		Value:	SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.country2"),	suppApplicant.getUser_rd_country2()));			 //TODO :		Value:	SG		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.businessLine"),"NA"));	 //TODO :	?? CHECK TYPSA FOR EVERY PAGE	Value:	NA		
		 //TODO :			SGD		
/*		nvpList.add(new BasicNameValuePair("ao-gad-wt-businessLine", "NA")); //TODO :,Non mandatory,Value:,NA
		nvpList.add(new BasicNameValuePair("ao-gad-wt-customerGroup", "PFS")); //TODO :,Non mandatory,Value:,PFS
		nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_1", "Visa Platinum")); //TODO :,Non mandatory,Value:,Visa Platinum
		nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_2", "IB")); //TODO :,Non mandatory,Value:,IB
		nvpList.add(new BasicNameValuePair("ao-gad-wt-hide_3", "HBSP_Visa Platinum")); //TODO :,Non mandatory,Value:,HBSP_Visa Platinum
		
		nvpList.add(new BasicNameValuePair("ao-gad-wt-language", "en")); //TODO :,Non mandatory,Value:,en
		nvpList.add(new BasicNameValuePair("ao-gad-wt-promoCode", "DEF0000001")); //TODO :,Non mandatory,Value:,DEF0000001
		nvpList.add(new BasicNameValuePair("ao-gad-wt-retrieveAppIndicator", "N")); //TODO :,Non mandatory,Value:,N
*/		//TODO to be checked					

		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.credit.card.send.my.mails.to.hidden"),	 "A"));			 //TODO :			A		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.relationship.bank"),	 application.getPrimaryApplicant().getAdditionalInfo().getAssociateOfPublicPositionHolder()==true?"Yes":"No"));			 //TODO :			No		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.haveany.former.names.hidden.rscnd"),	application.getPrimaryApplicant().getAdditionalInfo().getOtherName()==null?"No":"Yes"));			 //TODO :			No		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.secondary.cardholder.indicator.hidden"),	application.getAddSupplimentaryCard()==true?"Y":"N"));			 //TODO :			Y		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.secondary.cardholder.indicator.hidden"),CollectionUtils.isEmpty(application.getSupplementaryApplicant())?"N":"Y"));
			 //TODO :			Y
		/*Map<String, String> lengthOfServiceInYearsAndMonths = DataMappingListUtil.durationInYearsAndMonths(application.getPrimaryApplicant().getAdditionalInfo().getLengthOfService());
		Map<String, String> durationAtResidenceInMonthsAndYears = DataMappingListUtil.durationInYearsAndMonths(application.getPrimaryApplicant().getAdditionalInfo().getMonthsAtResidentialAddress());
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.months.text"),	 "03"));			 //TODO :	Value from DD values		3		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.occupation.hidden"),	"09"));			 //TODO :	Value from DD values		9		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.years.text"),	 "05"));			 //TODO :	Value from DD values		5		// LENGTH OF SERVICE
		
		//TODO to be checked
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.months.years.in.residence.hidden"), "06"));			 //TODO :	Value from DD values		6		
		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.senior.public.office.hidden"),"N"));			 //TODO :			N		
			 //TODO :			SG		
		//TODO to be checked
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.residential.addr.indicator.hidden"),	 "true"));			 //TODO :		?	true		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.have.any.former.names.1.hidden"),	 DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getOtherName())));			 //TODO :					
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.haveany.former.names.2.hidden"),	 DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getOtherName())));			 //TODO :					
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.haveany.former.names.hidden.rcnd"),	 application.getPrimaryApplicant().getAdditionalInfo().getOtherName()==null?"No":"Yes"));			 //TODO :			No		
		*/
		





		Map<String, String> lengthOfServiceInYearsAndMonths = DataMappingListUtil.durationInYearsAndMonths(application.getPrimaryApplicant().getAdditionalInfo().getLengthOfService());
		Map<String, String> durationAtResidenceInMonthsAndYears = DataMappingListUtil.durationInYearsAndMonths(application.getPrimaryApplicant().getAdditionalInfo().getMonthsAtResidentialAddress());
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.months.text"),	 lengthOfServiceInYearsAndMonths.get("month")));			 //TODO :	Value from DD values		3		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.occupation.hidden"),	application.getPrimaryApplicant().getAdditionalInfo().getOccupation()==null?BLANK_FOR_NO_SUPP_CARD:DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getOccupation().getValue())));			 //TODO :	Value from DD values		9		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.years.text"),	 lengthOfServiceInYearsAndMonths.get("year")));			 //TODO :	Value from DD values		5		// LENGTH OF SERVICE
		
		//TODO to be checked
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.months.years.in.residence.hidden"), durationAtResidenceInMonthsAndYears.get("year")));			 //TODO :	Value from DD values		6		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.senior.public.office.hidden"),application.getPrimaryApplicant().getAdditionalInfo().getHoldingPublicPosition()==null||application.getPrimaryApplicant().getAdditionalInfo().getHoldingPublicPosition()==false?"N":"Y"));	
		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.senior.public.office.hidden"),"N"));			 //TODO :			N		
			 //TODO :			SG		
		//TODO to be checked
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.residential.addr.indicator.hidden"),	DataMappingListUtil.getValidValue(String.valueOf(application.getPrimaryApplicant().getAdditionalInfo().isResidentialAddIdenticalToPermAddress()))));	
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.residential.addr.indicator.hidden"),	 "true"));			 //TODO :		?	true		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.have.any.former.names.1.hidden"),	 DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getOtherName())));			 //TODO :					
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.haveany.former.names.2.hidden"),	 DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getOtherName())));			 //TODO :					
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.haveany.former.names.hidden.rcnd"),	 application.getPrimaryApplicant().getAdditionalInfo().getOtherName()==null?"No":"Yes"));			 //TODO :			No		



		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.senior.public.office.1.text"),	suppApplicant.getUser_rd_senior_public_office_1_text()));			 //TODO :					
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.senior.public.office.2.text"),	suppApplicant.getUser_rd_senior_public_office_2_text()));			 //TODO :					
		//TODO to be checked
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.senior.public.office.text"),	suppApplicant.getUser_rd_senior_public_office_text()));				 //TODO :			No		
		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.employment.status"),	suppApplicant.getUser_rd_employment_status()));			 //TODO :			F		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.employment.status.1"),	suppApplicant.getUser_rd_employment_status_1()));			 //TODO :					
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.employment.status.2"),	suppApplicant.getUser_rd_employment_status_2()));				 //TODO :					
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.permanent.addr.indicator"),	suppApplicant.getUser_rd_permanent_addr_indicator()));				 //TODO :			Yes		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.permanent.addr.indicator.1"),	suppApplicant.getUser_rd_permanent_addr_indicator_1()));			 //TODO :			Yes		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.permanent.addr.indicator.2"),suppApplicant.getUser_rd_permanent_addr_indicator_2()));			 //TODO :			Yes		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.permanent.country"),	 suppApplicant.getUser_rd_country()));				 //TODO :			SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.permanent.country.1"),	suppApplicant.getUser_rd_country1()));		 //TODO :			SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.permanent.country.2"),	suppApplicant.getUser_rd_country2()));		 		 //TODO :			SG		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.first.residential.addr.indicator"), suppApplicant.getUser_rd_first_residential_addr_indicator()));		 //TODO :			true		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.first.residential.addr.indicator.1"),suppApplicant.getUser_rd_first_residential_addr_indicator_1()));			 //TODO :			true		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.first.residential.addr.indicator.2"), suppApplicant.getUser_rd_first_residential_addr_indicator_2()));		 //TODO :			true		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.second.residential.addr.indicator"),suppApplicant.getUser_rd_second_residential_addr_indicator()));			 //TODO :			true		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.second.residential.addr.indicator.1"), suppApplicant.getUser_rd_second_residential_addr_indicator_1()));		 //TODO :			true		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.second.residential.addr.indicator.2"), suppApplicant.getUser_rd_second_residential_addr_indicator_2()));		 //TODO :			true		
	
		/* if(params.getNumberOfCards().equals(SINGLE_CARD)){
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card"),params.getSupplementaryCard()));
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.2"),params.getSupplementaryCard()));	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.3"),params.getSupplementaryCard()));	
		//nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_reviewdeatils:_idJsp1442ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:ao-gad-rcscdp-creditCardProductList:0:ao-hbap-gad-rcscdp-productname",RequestParameters.getProductName()));		     	        
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.product.name"),params.getProductName()));
		 }
		 else{
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card"),params.getSupplementaryCard()));
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.2"),params.getSupplementaryCard()));	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.3"),params.getSupplementaryCard()));
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card"),params.getSupplementaryCardSecond()));
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card.2"),params.getSupplementaryCardSecond()));	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card.3"),params.getSupplementaryCardSecond()));
		//nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_reviewdeatils:_idJsp1442ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:ao-gad-rcscdp-creditCardProductList:0:ao-hbap-gad-rcscdp-productname",RequestParameters.getProductName()));		     	        
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.product.name"),params.getProductName()));
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.second.product.name"),params.getProductNameSecond()));
		 }*/
		 
		 
		 if(params.getNumberOfCards().equals(SINGLE_CARD)){
			 nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card"),params.getSupplementaryCard()));
	    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.2"),params.getSupplementaryCard()));	
	    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.3"),params.getSupplementaryCard()));		    				     	        
	    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.product.name"),params.getProductName()));
	    		
		 }
		 else{
		        //since all supplementary have same cards so using 0th index
		        if(null == application.getSupplementaryApplicant() || application.getSupplementaryApplicant().get(0).getCards().size()==2){
		        	nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card"),params.getSupplementaryCard()));
		    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.2"),params.getSupplementaryCard()));	
		    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.3"),params.getSupplementaryCard()));
		    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card"),params.getSupplementaryCardSecond()));
		    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card.2"),params.getSupplementaryCardSecond()));	
		    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card.3"),params.getSupplementaryCardSecond()));
		    		//nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_reviewdeatils:_idJsp1442ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:ao-gad-rcscdp-creditCardProductList:0:ao-hbap-gad-rcscdp-productname",RequestParameters.getProductName()));		     	        
		    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.product.name"),params.getProductName()));
		    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.second.product.name"),params.getProductNameSecond()));
		    		
		        	
		        }else if(application.getSupplementaryApplicant().get(0).getCards().size()==1){
		        	CreditCard suppCard = application.getSupplementaryApplicant().get(0).getCards().get(0);
		        	if(suppCard.getKey().equalsIgnoreCase((params.getSingleCardForSupplementary()))){
		        		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card"),params.getSupplementaryCard()));
			    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.2"),params.getSupplementaryCard()));	
			    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.3"),params.getSupplementaryCard()));
			    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.product.name"),params.getProductName()));
				       
			        		
		        	}
		        	else{
		        		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card"),params.getSupplementaryCardSecond()));
			    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card.2"),params.getSupplementaryCardSecond()));	
			    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.second.card.3"),params.getSupplementaryCardSecond()));
			    		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.second.product.name"),params.getProductNameSecond()));
		        	}
		        }
		        
		        
		        
		        
		 }
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.rd.continue.account.approval.up.sell.offered"), CONTINUE_SUBMIT_FOR_APPROVAL));	 //TODO :			Submit	for	Approval
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.rd.jsf.sequence"),JSF_SEQUENCE));		 //TODO :5		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.rd.idcl"),SUBMIT_IDCL));	 //TODO :			submit		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.rd.submit"),	SUBMIT_PAGE_DETAILS));	 //TODO :1	
		
		
		
		
	/*	nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card"),params.getSupplementaryCard()));
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.2"),params.getSupplementaryCard()));	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.supplementary.card.3"),params.getSupplementaryCard()));	
		//nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_reviewdeatils:_idJsp1442ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:ao-gad-rcscdp-creditCardProductList:0:ao-hbap-gad-rcscdp-productname",RequestParameters.getProductName()));		     	        
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("dynamic.rd.product.name"),params.getProductName()));
		*/
		
		
		/*nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_",params.getSupplementaryCard()));
	        nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_reviewdeatils:_idJsp1493ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:_idJsp1702ns_7_I9KAGO01G8R250IQP27VCE2GE7_:secondSupplementCardHolder_review:ao-hbap-gad-rscdp-second-cbaCardTypeList:0:ao-hbap-gad-rscdp-second-cbacardtype",params.getSupplementaryCard()));	
	        nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_reviewdeatils:_idJsp1493ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:_idJsp1952ns_7_I9KAGO01G8R250IQP27VCE2GE7_:thirdSupplementCardHolder_review:ao-hbap-gad-rscdp-third-cbaCardTypeList:0:ao-hbap-gad-rscdp-third-cbacardtype",params.getSupplementaryCard()));	
	        //nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GE7_reviewdeatils:_idJsp1442ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:ao-gad-rcscdp-creditCardProductList:0:ao-hbap-gad-rcscdp-productname",RequestParameters.getProductName()));		     	        
	        nvpList.add(new BasicNameValuePair("productList1",params.getProductName()));*/
		log.info("Posting parameters for Step GAD4 Review Details handler for arn {}",application.getArn());
		
		
/*
        List<NameValuePair> params1 = new ArrayList<NameValuePair>();

    	for (NameValuePair param : nvpList) {
    		if (param.getValue() != null) {
    			log.debug(("[Request] Adding parameter: " + param.getName()
    					+ " " + param.getValue()));
    			params1.add(param);
    			System.out.println(params1);
    		}
    	}
		*/
		
		
		
		/*if(params.getNumberOfCards().equals(SINGLE_CARD)){
			String resp4 = postRequest.postPageResponse(nvpList,params);
			params.setResponseStep4ReviewDetails(resp4);
			log.info("Response recorded for  Step GAD4 Review  Details handler for arn {}",application.getArn());
		
		}
		else{*/
			//String resp4 = postRequest.postHeaderResponse(nvpList,params);
			String resp4 = postRequest.postPageResponse5(nvpList,params,AOPageEnum.REVIEWDETAILS.name());
			params.setResponseStep4ReviewDetails(resp4);
			log.info("Response recorded for  Step GAD4 Review  Details handler for arn {}",application.getArn());
			//params.setRedirectionUrl(resp4);
					
		/*}*/
		
			try{
				RequestUtil.extractDynamicParamsStep5(resp4, params);
				
				
				
				}
				catch(NullPointerException e){
					 String requestUrlContinueSecond = hostUrl.concat(RequestUtil.extractUrl(resp4, "wiringForm"));
					 params.setUrl(requestUrlContinueSecond);
					return false;
				}
		
		log.info("Parameters Extracted from Step GAD4 Review  Details handler response for Step Gad4 Review Details Handler for arn {}",application.getArn());
		log.info("Finished Step GAD4 Review  Details Handler execute method for arn {}",application.getArn());
		/*}catch (Exception e) {
			log.error("Error in Step Gad4 Review Details Handler", e);
			//throw new AOException("Error in Step Gad4 Review Details Handler", e);
		}*/

		return true;
	}





private void populateFirstSupplimentry(SuppApplicants suppApplicant, Applicant applicantDetails) {
		 suppApplicant.setUser_rd_nationality(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNationality().getValue()));
		 
		 
		 
		 DocumentName documentName = applicantDetails.getIdDocType();
			
			if (documentName.equals(DocumentName.EMPLOYMENT_PASS)) {
				suppApplicant.setUser_rd_identificationtype(DocumentName.PASSPORT.getAoValue());		    
			} else if(documentName.equals(DocumentName.NRIC)){
				suppApplicant.setUser_rd_identificationtype(documentName.getAoValue());
			  }
		 
		 
		 suppApplicant.setUser_rd_employment_status(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getEmploymentType().getAoValue()));	
		 List<Address> addressList = applicantDetails.getAdditionalInfo().getAddress();
		 for (Address address : addressList) {
		     switch (address.getAddressType()) {		       
		         case RESIDENTIAL:{
	            	 suppApplicant.setUser_rd_country(DataMappingListUtil.getValidValue(address.getCountry().getValue()));		 
	             }break;
			default:
				break;
		        	 }
		         }
		 suppApplicant.setUser_rd_permanent_addr_indicator(applicantDetails.getAdditionalInfo().isResidentialAddressSameAsPrimary()==true?"Y":"N");
		 suppApplicant.setUser_rd_senior_public_office_text(applicantDetails.getAdditionalInfo().getHoldingPublicPosition()== null ||applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == false?"No":"Yes");
		 }


private void populateSecondSupplimentry(SuppApplicants suppApplicant, Applicant applicantDetails) {
	suppApplicant.setUser_rd_nationality1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNationality().getValue()));
	
DocumentName documentName = applicantDetails.getIdDocType();
	
	if (documentName.equals(DocumentName.EMPLOYMENT_PASS)) {
		suppApplicant.setUser_rd_identificationtype1(DocumentName.PASSPORT.getAoValue());		    
	} else if(documentName.equals(DocumentName.NRIC)){
		suppApplicant.setUser_rd_identificationtype1(documentName.getAoValue());
	  }

	 
	
	 suppApplicant.setUser_rd_employment_status_1(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getEmploymentType().getAoValue()));
	 List<Address> addressList = applicantDetails.getAdditionalInfo().getAddress();

	 for (Address address : addressList) {
	     switch (address.getAddressType()) {	         
             case RESIDENTIAL:{
            	 suppApplicant.setUser_rd_country1(DataMappingListUtil.getValidValue(address.getCountry().getValue()));		 
             }break;
		default:
			break;
	     }
	     }
	 suppApplicant.setUser_rd_permanent_addr_indicator_1(applicantDetails.getAdditionalInfo().isResidentialAddressSameAsPrimary()==true?"Y":"N");
	 suppApplicant.setUser_rd_senior_public_office_1_text(applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == null ||applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == false?"No":"Yes");
}


public void populateThirdSupplementry(SuppApplicants suppApplicant, Applicant applicantDetails){
	 suppApplicant.setUser_rd_nationality2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getNationality().getValue()));
	 
	 
	 
	 DocumentName documentName = applicantDetails.getIdDocType();
		
		if (documentName.equals(DocumentName.EMPLOYMENT_PASS)) {
			suppApplicant.setUser_rd_identificationtype(DocumentName.PASSPORT.getAoValue());		    
		} else if(documentName.equals(DocumentName.NRIC)){
			suppApplicant.setUser_rd_identificationtype2(documentName.getAoValue());
		  }
	 
	 	
	 suppApplicant.setUser_rd_employment_status_2(DataMappingListUtil.getValidValue(applicantDetails.getAdditionalInfo().getEmploymentType().getAoValue()));
	 List<Address> addressList = applicantDetails.getAdditionalInfo().getAddress();

	 for (Address address : addressList) {
	     switch (address.getAddressType()) {	         
	         case RESIDENTIAL:{
            	 suppApplicant.setUser_rd_country2(DataMappingListUtil.getValidValue(address.getCountry().getValue()));		 
             }break;
		default:
			break;
	         }
	 }
	 suppApplicant.setUser_rd_permanent_addr_indicator_2(applicantDetails.getAdditionalInfo().isResidentialAddressSameAsPrimary()==true?"Y":"N");	
	 suppApplicant.setUser_rd_senior_public_office_2_text(applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == null ||applicantDetails.getAdditionalInfo().getHoldingPublicPosition() == false?"No":"Yes");
 }



}

