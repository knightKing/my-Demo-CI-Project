package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.CONTINUE_TO_NEXT_PAGE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.FALSE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_IDCL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_PAGE_DETAILS;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.DISPATCH_METHOD_EMAIL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.JSF_SEQUENCE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.WIRE_ACTION;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.constants.AOPageEnum;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Step5ConfigBothProductHandler implements Handler {
	
	public boolean execute(RequestParameters params, Application application) throws Exception {
		/*try {*/
		log.info("Strating Step 5 Configure  Details handler execute method for arn {}",application.getArn());
		PostRequest postRequest = new PostRequest();
		List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
		//static value :0 
		nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.dispatch.method")	, "Y"));			 //TODO :		Value:	Y	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.wire.action")	, WIRE_ACTION));			 //TODO :		Value:	SendActionProducConfig	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.bndl.prd.id")	, BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.app.typ"),	BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.Cod")	, BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.trkr")	, BLANK_VALUE));	//TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, "on"));			 //TODO :		Value:	false	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.aodecampselPrdVal")	,BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.cmd.continue")	, CONTINUE_TO_NEXT_PAGE));			 //TODO :		Value:	Continue	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.jsf.sequence")	, JSF_SEQUENCE));			 //TODO :	Value:	1			
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.idcl") , SUBMIT_IDCL));			 //TODO :	Value:	submit	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.submit")	, SUBMIT_PAGE_DETAILS));			 //TODO :		Value:	1	
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.dispatch.method.email"),	 DISPATCH_METHOD_EMAIL));			 //TODO :	Non mandatory	Value:	EMAIL		
       // nvpList.add(new BasicNameValuePair(params.getDispatchMethod(), DISPATCH_METHOD_EMAIL));  //TODO : // ask values from Rahul!!

		log.info("Posting parameters for Step GAD5 Both cards Decision Details handler for arn {}",application.getArn());
		String resp = postRequest.postPageResponse5(nvpList,params,AOPageEnum.DECISIONING.name());
		params.setResponseStep5DecisionDetails(resp);
		log.info("Response recorded for  Step GAD5 Both cards Decision  Details handler for arn {}",application.getArn());
		
		boolean aoApplicationStatus = RequestUtil.applicationDeniedStatus(resp);
		if(aoApplicationStatus){
			log.info(
					"Application is denied by AO for AO reference number {} ",params.getAoApplicationReferenceNumber()+ "and arn {}"+
					application.getArn());
			log.info("Finished  AO execution for  {}", application.getArn());
			return false;
		}
		else{
			RequestUtil.extractDynamicParamsStep5Card1(resp, params);	
		}
		
		
		//params.setRedirectionUrl(resp);
		
/*		String resp= postRequest.postPageResponse(nvpList,params);
		params.setResponseStep5DecisionDetails(resp);
		RequestUtil.extractDynamicParamsStep5Card1(resp,params);*/
		//System.out.println(resp);
	//params.setRedirectionUrl(resp);
	//	log.info("Parameters Extracted from Step GAD5 Both cards Decision  Details handler response for Step Gad Review Details Handler for arn {}",application.getArn());
		log.info("Finished Step GAD5 Decision Both cards Details Handler execute method for arn {}",application.getArn());
		/*} catch (Exception e) {
			log.error("Error in Step5 Configure Product Handler", e);
			//throw new AOException("Error in Step5 Configure Product Handler", e);
		}*/
		
		
		if(params.isCreditCardDeclineCheck())
		{
			return false;
					}
		else{
			return true;	
		}
		
	}

}
