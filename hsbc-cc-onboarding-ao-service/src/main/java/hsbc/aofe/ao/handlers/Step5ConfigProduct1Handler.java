package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.CONTINUE_TO_NEXT_PAGE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.FALSE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.TRUE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_IDCL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_PAGE_DETAILS;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.DISPATCH_METHOD_EMAIL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.JSF_SEQUENCE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.WIRE_ACTION;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.constants.AOPageEnum;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.CreditCard;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Step5ConfigProduct1Handler implements Handler {
	
	public boolean execute(RequestParameters params, Application application) throws Exception {
		/*try {*/
		String cardSelectedForBT = null!=application.getPrimaryApplicant().getBeneficiaryDetails()?application.getPrimaryApplicant().getBeneficiaryDetails().getCcKey():BLANK_VALUE;
		String hostUrl = PropertiesCache.getInstance().getProperty("host.url");
		log.info("Strating Step 5 Configure  Details handler execute method for arn {}",application.getArn());
		/*params.setUrl("https://www.ap380.p2g.netd2.hsbc.com.hk/1/2/!ut/p/c5/jczLDoIwFATQb_EL7rWA1WVFLCgtUCgiG0Ki8RF5LAhGv966c2N0ZnkyAyWYtvV4OdXDpWvrGxRQzqpgsWU8wimfK-JgkMSE5q5HeGoZ3384Zsp-e7oOhEKTf9YuZ75NQ0ROjRPppESwHDGwfqx3UKBdpdd5Lx5DET7dMbs-EyKod5er412GEQoZxodc6SVjVuRsWpB-1xxhDyX9-qxn0Ddaj2c2eQENxY5I/dl3/d3/L0lDU0lKSmdwcGlRb0tVUSEvb0NvZ0FFSVFoakVDVUlnQUVBSXlGQU1od1VoUzRJQSEvNEMxYjlXX05yMGdDVWd4RW1SQ1V3ZyEhLzdfSTlLQUdPMDFHOFIyNTBJUVAyN1ZDRTJHVTYvZU9fUkI5MjY4MDA5NS8zNjQ4MTcxMzE4ODMvb3JnLmFwYWNoZS5teWZhY2VzLnBvcnRsZXQuTXlGYWNlc0dlbmVyaWNQb3J0bGV0LlZJRVdfSUQvJTBoYmFwJTBwcm9kY29uZmlnLWpzcHMlMHByb2Rjb25maWctc2NyZWVucyUwY29uZmlndXJlX3Byb2R1Y3RfcGFnZV9wcm9kdWN0b3B0aW9uLmpzcA!!/");
		params.setEdsToken("ESDS_TOKEN-configureProductPage1:0.93139315");
		params.setCookies("LB_COOKIE_1=157340170.23808.0000; JSESSIONID=00005289A2pRROaCr24OL5ziKiM:15jo3tf4q; CAMToken=/Cq7pnwtBI6NuwuoBRZ9BxqCAOE=; ");
		params.setDispatchMethod("viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product:_idJsp288ns_7_I9KAGO01G8R250IQP27VCE2GU6_:view_terms_and_conditions:ao-tnc-vtnc-dispatchmethod");
		*/String embossName = "viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product:_idJsp62ns_7_I9KAGO01G8R250IQP27VCE2GU6_:sequence-creditLimitOverride:_idJsp64ns_7_I9KAGO01G8R250IQP27VCE2GU6_:credit_limit_override:ao-pc-cld-embossname";
		PostRequest postRequest = new PostRequest();
		List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
		String cardKeyForbalanceTransfer ;
		
		if(application.getPrimaryApplicant().getBeneficiaryDetails()!=null && application.getPrimaryApplicant().getBeneficiaryDetails().getCcKey().equals(params.getFirstCardForBalanceTransfer())){
			cardKeyForbalanceTransfer=application.getPrimaryApplicant().getBeneficiaryDetails().getCcKey();
			nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
			//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.dispatch.method")	, "Y"));			 //TODO :		Value:	Y	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.wire.action")	, WIRE_ACTION));			 //TODO :		Value:	SendActionProducConfig	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.bndl.prd.id")	, ""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.app.typ"),	""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.Cod")	, ""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.trkr")	, ""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.amount"),	application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getBeneficiaryDetails().getAmountToBeTransferred())));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.and.number")	,application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getBeneficiaryDetails().getBeneficiaryAccountNumber())));			 //TODO :	Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:application.getPrimaryApplicant().getBeneficiaryDetails().getBeneficiaryBank().getValue()));			 //TODO :		Value:		
			//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,"N"));			 //TODO :	Value:	N	
			//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,BankNameForBalanceTransfer.THE_ROYAL_BANK_OF_SCOTLAND_NV.getValue()));			 //TODO :		Value:
			
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,application.getPrimaryApplicant().getApplicantDeclarations().getBalanceTransferOnCreditCard()==true?"Y":"N"));			 //TODO :	Value:	N
			/*if(params.getDispatchMethodDisableCheck()==true){
				nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, FALSE_VALUE));		
				}
				else
				{*/
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, "on"));
				/*}*/
			 //TODO :		Value:	false	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.aodecampselPrdVal")	,BLANK_VALUE));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.cmd.continue.app.success")	, CONTINUE_TO_NEXT_PAGE));			 //TODO :		Value:	Continue	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.jsf.sequence")	, JSF_SEQUENCE));			 //TODO :	Value:	1	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.emboss.name")	, DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNameOnCard())));			 //TODO :		Value:	PRACHI	BHAMBANI
			nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product:_idJsp106ns_7_I9KAGO01G8R250IQP27VCE2GU6_:sequence-captureCreditCardBalanceTransfer:CBTcheckRequestDetail:credit_card_balance:ao-hbap-pc-cbt-balancetransferaccoutname"	,application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getBeneficiaryDetails().getBeneficiaryAccountName())));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.idcl") , SUBMIT_IDCL));			 //TODO :	Value:	submit	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.submit")	, SUBMIT_PAGE_DETAILS));			 //TODO :		Value:	1	
			/*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.dispatch.method.email"),	 DISPATCH_METHOD_EMAIL));			 //TODO :	Non mandatory	Value:	EMAIL		
	        nvpList.add(new BasicNameValuePair(params.getDispatchMethod(), DISPATCH_METHOD_EMAIL));  //TODO : // ask values from Rahul!!
*/
			
			//
			
			nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product:_idJsp106ns_7_I9KAGO01G8R250IQP27VCE2GU6_:sequence-captureCreditCardBalanceTransfer:CBTcheckRequestDetail:credit_card_balance:ao-hbap-pc-cbt-balancetransferscheme",RequestUtil.balanceTxSchemeMap.get(cardSelectedForBT)));
		}
		else{
			
			nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
			//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.dispatch.method")	, "Y"));			 //TODO :		Value:	Y	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.wire.action")	, WIRE_ACTION));			 //TODO :		Value:	SendActionProducConfig	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.bndl.prd.id")	, ""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.app.typ"),	""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.Cod")	, ""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.trkr")	, ""));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.amount"),""	));		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.and.number")	,""));		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,""));		 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,"N"));			 //TODO :	Value:	N	
			 //TODO :	Value:	N
			/*if(params.getDispatchMethodDisableCheck()==true){
				nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, FALSE_VALUE));		
				}
				else
				{*/
					nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, "on"));
				/*}*/
			 //TODO :		Value:	false	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.aodecampselPrdVal")	,BLANK_VALUE));			 //TODO :		Value:		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.cmd.continue.app.success")	, CONTINUE_TO_NEXT_PAGE));			 //TODO :		Value:	Continue	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.jsf.sequence")	, JSF_SEQUENCE));			 //TODO :	Value:	1	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.emboss.name")	, DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNameOnCard())));			 //TODO :		Value:	PRACHI	BHAMBANI
			nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product:_idJsp106ns_7_I9KAGO01G8R250IQP27VCE2GU6_:sequence-captureCreditCardBalanceTransfer:CBTcheckRequestDetail:credit_card_balance:ao-hbap-pc-cbt-balancetransferaccoutname"	,""));		
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.idcl") , SUBMIT_IDCL));			 //TODO :	Value:	submit	
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.submit")	, SUBMIT_PAGE_DETAILS));			 //TODO :		Value:	1	
			/*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.dispatch.method.email"),	 DISPATCH_METHOD_EMAIL));			 //TODO :	Non mandatory	Value:	EMAIL		
	        nvpList.add(new BasicNameValuePair(params.getDispatchMethod(), DISPATCH_METHOD_EMAIL));  //TODO : // ask values from Rahul!!
			*/
		}
		
		/*cardKeyForbalanceTransfer=application.getPrimaryApplicant().getBeneficiaryDetails().getCcKey();
		nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.dispatch.method")	, "Y"));			 //TODO :		Value:	Y	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.wire.action")	, WIRE_ACTION));			 //TODO :		Value:	SendActionProducConfig	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.bndl.prd.id")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.app.typ"),	""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.Cod")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.trkr")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.amount"),""	));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.and.number")	,""));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,""));		 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,"N"));			 //TODO :	Value:	N	
		 //TODO :	Value:	N
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, FALSE_VALUE));			 //TODO :		Value:	false	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.aodecampselPrdVal")	,BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.cmd.continue.app.success")	, CONTINUE_TO_NEXT_PAGE));			 //TODO :		Value:	Continue	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.jsf.sequence")	, "2"));			 //TODO :	Value:	1	
		nvpList.add(new BasicNameValuePair(embossName, DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNameOnCard())));			 //TODO :		Value:	PRACHI	BHAMBANI
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.accout.name")	,""));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.idcl") , SUBMIT_IDCL));			 //TODO :	Value:	submit	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.submit")	, SUBMIT_PAGE_DETAILS));			 //TODO :		Value:	1	
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.dispatch.method.email"),	 DISPATCH_METHOD_EMAIL));			 //TODO :	Non mandatory	Value:	EMAIL		
      //  nvpList.add(new BasicNameValuePair(params.getDispatchMethod(), DISPATCH_METHOD_EMAIL));  //TODO : // ask values from Rahul!!
*/		
		log.info("Posting parameters for Step GAD5 Product config 1 handler for arn {}",application.getArn());
		String resp = postRequest.postPageResponse5(nvpList, params,AOPageEnum.CONFIGPRODUCT1.name());
		//String resp = postRequest.postPageResponse(nvpList, params);
		params.setResponseStep5Card1DecisionDetails(resp);
		
		
		log.info("Response recorded for  Step GAD5 Product config 1 handler for arn {}",application.getArn());			
		//params.setRedirectionUrl(params.getResponseStep5DecisionDetails());
		
		 
		 
		/* boolean aoApplicationStatus = RequestUtil.applicationDeniedStatus(resp);
			if(aoApplicationStatus){
				log.info(
						"Application is denied by AO for AO reference number {} ",params.getAoApplicationReferenceNumber()+ "and arn {}"+
						application.getArn());
				log.info("Finished  AO execution for  {}", application.getArn());
				return false;
			}
			else{
				
				 
			}*/

		//params.setRedirectionUrl(resp);
		//crawl(params.getRedirectionUrl());
		log.info("Finished Step GAD5 Product config 1 execute method for arn {}",application.getArn());
		/*} catch (Exception e) {
			log.error("Error in Step5 Configure Product Handler", e);
			//throw new AOException("Error in Step5 Configure Product Handler", e);
		}*/
			//check for one card decline
		if(params.isCreditCardDeclineCheck())
		{
			String applicationId = RequestUtil.extractApplicationIdFromCompletePage(resp);
			if (StringUtils.isNotEmpty(applicationId)) {
				params.setAoApplicationReferenceNumber(applicationId);
			}
			
					}			
		else{
			String requestUrlContinueSecond = hostUrl.concat(RequestUtil.extractUrl(resp, "wiringForm"));
			 params.setUrl(requestUrlContinueSecond);
		}
		return true;
	}
	private void crawl(String url) throws IOException {

		Response response = Jsoup.connect(url).followRedirects(false).execute();

		System.out.println(response.statusCode() + " : " + url);

		if (response.hasHeader("location")) {
			String redirectUrl = response.header("location");
			crawl(redirectUrl);
		}

	}
}
