package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.CONTINUE_TO_NEXT_PAGE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.FALSE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_IDCL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_PAGE_DETAILS;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.DISPATCH_METHOD_EMAIL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.WIRE_ACTION;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.constants.AOPageEnum;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Step5ConfigProduct1SecondHandler implements Handler {
	
	public boolean execute(RequestParameters params, Application application) throws Exception {
		/*try {*/
		log.info("Strating Step 5 Configure  Details handler execute method for arn {}",application.getArn());
		
		PostRequest postRequest = new PostRequest();
		List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
		
		
		log.info("Posting parameters for Step GAD5 Decision Details handler for arn {}",application.getArn());
		String resp = postRequest.postPageResponse5(nvpList, params,AOPageEnum.CONFIGPRODUCT1.name());
		//String resp = postRequest.postPageResponse(nvpList, params);
		params.setResponseStep5Card1DecisionDetails(resp);
	
		
		log.info("Response recorded for  Step GAD4 Decision  Details handler for arn {}",application.getArn());			
		//params.setRedirectionUrl(params.getResponseStep5DecisionDetails());
		RequestUtil.extractDynamicParamsStep5Card2(resp,params);
		//params.setRedirectionUrl(resp);
		//crawl(params.getRedirectionUrl());
		log.info("Parameters Extracted from Step GAD5 Decision  Details handler response for Step Gad Review Details Handler for arn {}",application.getArn());
		log.info("Finished Step GAD5 Decision  Details Handler execute method for arn {}",application.getArn());
		/*} catch (Exception e) {
			log.error("Error in Step5 Configure Product Handler", e);
			//throw new AOException("Error in Step5 Configure Product Handler", e);
		}*/
		return true;
	}
	
}

