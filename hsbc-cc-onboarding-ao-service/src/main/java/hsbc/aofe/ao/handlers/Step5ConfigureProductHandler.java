package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.CONTINUE_TO_NEXT_PAGE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_IDCL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SUBMIT_PAGE_DETAILS;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.DISPATCH_METHOD_EMAIL;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.JSF_SEQUENCE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.DecisionConfigureCardDetails.WIRE_ACTION;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.FALSE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.TRUE_VALUE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.constants.AOPageEnum;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.BankNameForBalanceTransfer;
import hsbc.aofe.exception.AOException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Step5ConfigureProductHandler implements Handler {
	
	public boolean execute(RequestParameters params, Application application) throws Exception {
		/*try {*/
		log.info("Strating Step 5 Configure  Details handler execute method for arn {}",application.getArn());
		PostRequest postRequest = new PostRequest();
		String cardSelectedForBT = null!=application.getPrimaryApplicant().getBeneficiaryDetails()?application.getPrimaryApplicant().getBeneficiaryDetails().getCcKey():BLANK_VALUE;
		List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
		//static value :0 
		nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
	//	nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.dispatch.method")	, "Y"));			 //TODO :		Value:	Y	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.wire.action")	, WIRE_ACTION));			 //TODO :		Value:	SendActionProducConfig	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.bndl.prd.id")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.app.typ"),	""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.Cod")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.trkr")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.amount"),	application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getBeneficiaryDetails().getAmountToBeTransferred())));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.and.number")	,application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getBeneficiaryDetails().getBeneficiaryAccountNumber())));			 //TODO :	Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:application.getPrimaryApplicant().getBeneficiaryDetails().getBeneficiaryBank().getValue()));			 //TODO :		Value:		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,"N"));			 //TODO :	Value:	N	
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,BankNameForBalanceTransfer.THE_ROYAL_BANK_OF_SCOTLAND_NV.getValue()));			 //TODO :		Value:
		
		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,application.getPrimaryApplicant().getApplicantDeclarations().getBalanceTransferOnCreditCard()==true?"Y":"N"));			 //TODO :	Value:	N
		
	/*	if(params.getDispatchMethodDisableCheck()==true){
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, FALSE_VALUE));		
		}
		else
		{*/
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, "on"));
		/*}*/
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.aodecampselPrdVal")	,BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.cmd.continue")	, CONTINUE_TO_NEXT_PAGE));			 //TODO :		Value:	Continue	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.jsf.sequence")	, JSF_SEQUENCE));			 //TODO :	Value:	1	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.emboss.name")	, DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNameOnCard())));			 //TODO :		Value:	PRACHI	BHAMBANI
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.accout.name")	,application.getPrimaryApplicant().getBeneficiaryDetails()==null?BLANK_VALUE:DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getBeneficiaryDetails().getBeneficiaryAccountName())));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.idcl") , SUBMIT_IDCL));			 //TODO :	Value:	submit	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.submit")	, SUBMIT_PAGE_DETAILS));			 //TODO :		Value:	1	
		
		//balance transfer related changes
		
		if(null!=application.getPrimaryApplicant().getApplicantDeclarations() && application.getPrimaryApplicant().getApplicantDeclarations().getBalanceTransferOnCreditCard()){
			nvpList.add(new BasicNameValuePair("viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product:_idJsp298ns_7_I9KAGO01G8R250IQP27VCE2GU6_:sequence-captureCreditCardBalanceTransfer:CBTcheckRequestDetail:credit_card_balance:ao-hbap-pc-cbt-balancetransferscheme",RequestUtil.balanceTxSchemeMap.get(cardSelectedForBT)));
		}
		
		//nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.dispatch.method.email"),	 DISPATCH_METHOD_EMAIL));			 //TODO :	Non mandatory	Value:	EMAIL		
        //nvpList.add(new BasicNameValuePair(params.getDispatchMethod(), DISPATCH_METHOD_EMAIL));  //TODO : // ask values from Rahul!!

		
		
		
		
		
		
		/*nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
	//	nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.dispatch.method")	, "Y"));			 //TODO :		Value:	Y	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.wire.action")	, WIRE_ACTION));			 //TODO :		Value:	SendActionProducConfig	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.bndl.prd.id")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.app.typ"),	""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.Cod")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.trkr")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.amount"),""	));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.and.number")	,""));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,""));		 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,"N"));			 //TODO :	Value:	N	
		 //TODO :	Value:	N
		
		
		if(params.getDispatchMethodDisableCheck()==true){
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, FALSE_VALUE));		
		}
		else
		{
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, TRUE_VALUE));
		}
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, FALSE_VALUE));			 //TODO :		Value:	false	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.aodecampselPrdVal")	,BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.cmd.continue")	, CONTINUE_TO_NEXT_PAGE));			 //TODO :		Value:	Continue	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.jsf.sequence")	, JSF_SEQUENCE));			 //TODO :	Value:	1	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.emboss.name")	, DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNameOnCard())));			 //TODO :		Value:	PRACHI	BHAMBANI
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.accout.name")	,""));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.idcl") , SUBMIT_IDCL));			 //TODO :	Value:	submit	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.submit")	, SUBMIT_PAGE_DETAILS));			 //TODO :		Value:	1	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.dispatch.method.email"),	 DISPATCH_METHOD_EMAIL));			 //TODO :	Non mandatory	Value:	EMAIL		
        nvpList.add(new BasicNameValuePair(params.getDispatchMethod(), DISPATCH_METHOD_EMAIL));  //TODO : // ask values from Rahul!!
		*/
	
	/*	
		nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.dispatch.method")	, "Y"));			 //TODO :		Value:	Y	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.wire.action")	, WIRE_ACTION));			 //TODO :		Value:	SendActionProducConfig	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.bndl.prd.id")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.app.typ"),	""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.Cod")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.cnfg.prd.trkr")	, ""));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.amount"),"1000"	));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.and.number")	,"12345678"));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.other.bank")	,""));		 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.config.balance.transfer")	,"N"));			 //TODO :	Value:	N	
		 //TODO :	Value:	N
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.accept")	, FALSE_VALUE));			 //TODO :		Value:	false	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.aodecampselPrdVal")	,BLANK_VALUE));			 //TODO :		Value:		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.cmd.continue")	, CONTINUE_TO_NEXT_PAGE));			 //TODO :		Value:	Continue	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("constant.dd.jsf.sequence")	, JSF_SEQUENCE));			 //TODO :	Value:	1	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.emboss.name")	, DataMappingListUtil.getValidValue(application.getPrimaryApplicant().getAdditionalInfo().getNameOnCard())));			 //TODO :		Value:	PRACHI	BHAMBANI
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.balance.transfer.accout.name")	,""));		
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.idcl") , SUBMIT_IDCL));			 //TODO :	Value:	submit	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.submit")	, SUBMIT_PAGE_DETAILS));			 //TODO :		Value:	1	
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty	("user.dd.dispatch.method.email"),	 DISPATCH_METHOD_EMAIL));			 //TODO :	Non mandatory	Value:	EMAIL		
        nvpList.add(new BasicNameValuePair(params.getDispatchMethod(), DISPATCH_METHOD_EMAIL));  //TODO : // ask values from Rahul!!
		*/
		
		log.info("Posting parameters for Step GAD5 Decision Details handler for arn {}",application.getArn());
		params.setResponseStep5DecisionDetails(postRequest.postPageResponse5(nvpList,params,AOPageEnum.CONFIGPRODUCT.name()));
		log.info("Response recorded for  Step GAD4 Decision  Details handler for arn {}",application.getArn());			
		//params.setRedirectionUrl(params.getResponseStep5DecisionDetails());
		String applicationId = RequestUtil.extractApplicationIdFromCompletePage(params.getResponseStep5DecisionDetails());
		if (StringUtils.isNotEmpty(applicationId)) {
			params.setAoApplicationReferenceNumber(applicationId);
		}
		System.out.println("Application Reference Number:"+applicationId);		
		log.debug(applicationId);
		log.info(
				"Application Reference Number Extracted from Redirect To Step 6 Handler response for returning  AO application reference number for arn {}",
				application.getArn());
		log.info("Finished  Redirect To Step 6 Handler execute method for arn {}", application.getArn());
	
		log.info("Parameters Extracted from Step GAD5 Decision  Details handler response for Step Gad Review Details Handler for arn {}",application.getArn());
		log.info("Finished Step GAD5 Decision  Details Handler execute method for arn {}",application.getArn());
		/*} catch (Exception e) {
			log.error("Error in Step5 Configure Product Handler", e);
			//throw new AOException("Error in Step5 Configure Product Handler", e);
		}*/
		return true;
	}

}
