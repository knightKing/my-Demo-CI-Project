package hsbc.aofe.ao.handlers;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import hsbc.aofe.ao.constants.AOPageEnum;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Step5ProductConfigDenied implements Handler {
	
	public boolean execute(RequestParameters params,Application application) throws Exception {
		/*try {*/

			
		log.info("Strating Step GAD4 Review Details handler execute method for arn {}",application.getArn());
		PostRequest postRequest = new PostRequest();
		List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
		
		
		log.info("Posting parameters for Step GAD4 Review Details handler for arn {}",application.getArn());
		
		nvpList.add(new BasicNameValuePair("WIRE_ACTION","SendActionProducConfig"));

			String resp4 = postRequest.postPageResponse5(nvpList,params,AOPageEnum.CONFIGDENIED.name());
			params.setResponseStep4ReviewDetails(resp4);
			boolean aoApplicationStatus = RequestUtil.applicationDeniedStatus(resp4);
			if(aoApplicationStatus){
				log.info(
						"Application is denied by AO for AO reference number {} ",params.getAoApplicationReferenceNumber()+ "and arn {}"+
						application.getArn());
				System.out.println("Application is denied by AO for AO reference number {} "+params.getAoApplicationReferenceNumber());
				log.info("Finished  AO execution for  {}", application.getArn());
				
			}
					
		return true;
	}




}

