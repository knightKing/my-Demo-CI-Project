package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.*;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.AoEntry.*;

import java.util.*;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.constants.HsbcConstants;
import hsbc.aofe.ao.data.MarketSectorCodeInformation;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.des.TripleDes;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.CreditCard;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

@Slf4j
public class StepAoEntryCardSpecsHandler implements Handler {

    private static List<MarketSectorCodeInformation> msc1list = new ArrayList<MarketSectorCodeInformation>();
    public static Map<String, String> CBACardTypeMap = new HashMap<>();

    static {

        final List<String> ACCOUNT_BASIC = new ArrayList<String>();
        ACCOUNT_BASIC.add(HsbcConstants.Parameters.Card.Common.SVI);
        ACCOUNT_BASIC.add(HsbcConstants.Parameters.Card.Common.VPC);
        MarketSectorCodeInformation mscBasic = new MarketSectorCodeInformation("BASIC", BASIC_ACCOUNT_MSC1_VALUE, 3, ACCOUNT_BASIC, BASIC_ACCOUNT_IPSEG_VALUE);

        final List<String> ACCOUNT_ADVANCE = new ArrayList<String>();
        ACCOUNT_ADVANCE.add(HsbcConstants.Parameters.Card.Common.AVR);
        MarketSectorCodeInformation mscAdvance = new MarketSectorCodeInformation("ADVANCE", ADVANCE_ACCOUNT_MSC1_VALUE, 2, ACCOUNT_ADVANCE, ADVANCE_ACCOUNT_IPSEG_VALUE);

        final List<String> ACCOUNT_PREMIER = new ArrayList<String>();
        ACCOUNT_PREMIER.add(HsbcConstants.Parameters.Card.Common.DU6);
        MarketSectorCodeInformation mscPremier = new MarketSectorCodeInformation("PREMIER", PREMIER_ACCOUNT_MSC1_VALUE, 1, ACCOUNT_PREMIER, PREMIER_ACCOUNT_IPSEG_VALUE);

        final List<String> ACCOUNT_REVOLUTION = new ArrayList<String>();
        ACCOUNT_REVOLUTION.add(HsbcConstants.Parameters.Card.Common.VP1);
        MarketSectorCodeInformation mscRevolution = new MarketSectorCodeInformation("REVOLUTION", REVOLUTION_ACCOUNT_MSC1_VALUE, 4, ACCOUNT_REVOLUTION, REVOLUTION_ACCOUNT_IPSEG_VALUE);


        //since the arraylist maintains the insertion order, please add/remove carefully.
        //adding as per the superiority of cards from highest to lowest.
        msc1list.add(mscPremier);
        msc1list.add(mscAdvance);
        msc1list.add(mscBasic);
        msc1list.add(mscRevolution);

        CBACardTypeMap.put(VPC, "GVP");
        CBACardTypeMap.put(VP1, "GPW");
        CBACardTypeMap.put(AVR, "ADV");
        CBACardTypeMap.put(SVI, "VIC");
        CBACardTypeMap.put(DU6, "PMR,GVP");

    }

    public boolean execute(RequestParameters params, Application application) throws Exception {
        /* try { */
        String applicationArn = application.getArn();
        log.info("STARTING: Step AO Entry Card Specification Handler execute method for applicationArn {}", applicationArn);

        PostRequest postRequest = new PostRequest();
        List<NameValuePair> nvpList = new ArrayList<>();

        List<CreditCard> creditCards = application.getPrimaryApplicant().getCards();

        StringJoiner bundleIds = new StringJoiner(",");
        creditCards.stream().forEach(creditCard -> bundleIds.add(HSBC_OHD_BUNDLE_ID));
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.bundle.id"),
                String.valueOf(bundleIds)));

        StringJoiner currencyCodes = new StringJoiner(",");
        creditCards.stream().forEach(creditCard -> currencyCodes.add(HSBC_OHD_CURRENCY_CODE));
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.currency.code"),
                String.valueOf(currencyCodes)));

        StringJoiner preferedCreditLimits = new StringJoiner(",");
        creditCards.stream().forEach(creditCard -> {
            preferedCreditLimits.add(HSBC_OHD_CURRENCY_CODE);
            preferedCreditLimits.add(String.valueOf(DataMappingListUtil.resolveIfLimitIsNull(creditCard.getLimit())));
        });
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.prefered.credit.limit"),
                String.valueOf(preferedCreditLimits)));

        StringJoiner creditCardKeys = new StringJoiner(",");
        creditCards.stream().forEach(creditCard -> creditCardKeys.add(creditCard.getKey()));
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.product.id"),
                String.valueOf(creditCardKeys)));

        StringJoiner voucherCodes = new StringJoiner(",");
        creditCards.stream().forEach(creditCard -> {
            String voucherCode = DataMappingListUtil.getValidValue(application.getStaffDetails().getVoucherCode());
            voucherCodes.add(StringUtils.isNotEmpty(voucherCode) ? voucherCode : "AAAAA");
        });

        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.voucher.code"),
                String.valueOf(voucherCodes)));
        if (params.getNumberOfCards().equals(SINGLE_CARD)) {
            nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.flow.id"), FLOW_ID));
            nvpList.add(
                    new BasicNameValuePair("CBACardOccurCount", calculateCBACardOccurCount(creditCardKeys.toString())));
            nvpList.add(new BasicNameValuePair("CBACardType", calculateCBACardType(creditCardKeys.toString())));

            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_1", calculateMSC1(creditCardKeys.toString())));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_2", "00000"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_3", "00000"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_4", "PRS"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MULTI_APP_FLAG", "S"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_PROCESS_ID", "40003,40003"));
            //nvpList.add(new BasicNameValuePair("HSBC_OHD_PROMOTION_CODE", "DEF0000001"));
            nvpList.add(new BasicNameValuePair("ipSegType", calculateIpSeg(creditCardKeys.toString())));
			nvpList.add(new BasicNameValuePair("CBAIndicator","Y"));

        } else {

            nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.flow.id"),
                    FLOW_ID_DUAL));
            nvpList.add(new BasicNameValuePair("CBAIndicator","Y,Y"));
            nvpList.add(
            		
                    new BasicNameValuePair("CBACardOccurCount", calculateCBACardOccurCount(creditCardKeys.toString())));
            nvpList.add(new BasicNameValuePair("CBACardType", calculateCBACardType(creditCardKeys.toString())));

            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_1", calculateMSC1(creditCardKeys.toString())));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_2", "00000"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_3", "00000"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MSC_4", "PRS"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_MULTI_APP_FLAG", "S,S"));
            nvpList.add(new BasicNameValuePair("HSBC_OHD_PROCESS_ID", "40003,40003"));
            //nvpList.add(new BasicNameValuePair("HSBC_OHD_PROMOTION_CODE", "DEF0000001"));
			
            nvpList.add(new BasicNameValuePair("ipSegType", calculateIpSeg(creditCardKeys.toString())));
            

        }
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.launch.command"), LAUNCH_COMMAND));
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.user.entry.siteID"), SITE_ID));
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("token.entry.crm"), TripleDes.generateToken()));

        log.info("Setting url for Step AO Entry Card Specification handler execute method for applicationArn {}", applicationArn);
        params.setUrl(PropertiesCache.getInstance().getProperty("ao.entry.url"));

        log.info("Posting parameters for  Step AO Entry Card Specification handler for applicationArn {}", applicationArn);
        params.setResponseStepAoEntryDetails(postRequest.postPageResponse(nvpList, params));

        log.info("Recorded response of Step AO Entry Card Specification handler execute method for applicationArn {}", applicationArn);
        RequestUtil.extractDynamicParamsJsContinue(params.getResponseStepAoEntryDetails(), params);

        /*
         * }catch (Exception e) {
		 * log.error("Error in Step Ao Entry Card Specs Handler", e); //throw
		 * new AOException("Error inStep Ao Entry Card Specs Handler", e); }
		 */
        log.info("FINISHED: AO entry handler execute method for applicationArn {}", applicationArn);
        return Boolean.TRUE;
    }

    private String calculateMSC1(String commaSeparatedProductCodes) {
        List<String> productList = Arrays.asList(commaSeparatedProductCodes.split(","));
        //setting the priority to
        int priority = 100;
        String effectiveMsc1Code = "";


        for (String productCode : productList) {

            for (MarketSectorCodeInformation info : msc1list) {
                if (info.getProducts().contains(productCode) && info.getPriority() < priority) {
                    priority = info.getPriority();
                    effectiveMsc1Code = info.getMsc1Code();
                }

            }
        }

        return effectiveMsc1Code;

    }

    private String calculateCBACardOccurCount(String commaSeparatedProductCodes) {
        List<String> productList = Arrays.asList(commaSeparatedProductCodes.split(","));
        List<Integer> CBACardOccurCountList = new ArrayList<Integer>();


        StringJoiner cba = new StringJoiner(",");
        for (String productCode : productList) {

            if (productCode.equalsIgnoreCase(DU6)) {
                CBACardOccurCountList.add(2);
            } else {
                CBACardOccurCountList.add(1);
            }


        }
        CBACardOccurCountList.stream().forEach(product -> cba.add(product.toString()));
        return cba.toString();

    }

    
  //";" is the string joiner as at hsbc end ";" is used for split
    
    private String calculateCBACardType(String commaSeparatedProductCodes) {
        List<String> productList = Arrays.asList(commaSeparatedProductCodes.split(","));
        List<String> CBACardTypeList = new ArrayList<String>();


        StringJoiner cba = new StringJoiner(";");
        for (String productCode : productList) {


            CBACardTypeList.add(CBACardTypeMap.get(productCode));


        }
        CBACardTypeList.stream().forEach(product -> cba.add(product));
        return cba.toString();

    }

    private String calculateIpSeg(String commaSeparatedProductCodes) {
        List<String> productList = Arrays.asList(commaSeparatedProductCodes.split(","));
        //setting the priority to
        int priority = 100;
        String effectiveIpSegCode = "";


        for (String productCode : productList) {

            for (MarketSectorCodeInformation info : msc1list) {
                if (info.getProducts().contains(productCode) && info.getPriority() < priority) {
                    priority = info.getPriority();
                    effectiveIpSegCode = info.getIpSegType();
                }

            }
        }

        return effectiveIpSegCode;

    }

    public static void main(String s[]) {
        StepAoEntryCardSpecsHandler aps = new StepAoEntryCardSpecsHandler();
        final String card = "SVI";


    }

}
