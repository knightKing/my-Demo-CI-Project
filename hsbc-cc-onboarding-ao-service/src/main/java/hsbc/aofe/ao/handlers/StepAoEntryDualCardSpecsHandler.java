package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.AoEntry.HSBC_OHD_BUNDLE_ID;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.AoEntry.HSBC_OHD_CURRENCY_CODE;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.CreditCard;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StepAoEntryDualCardSpecsHandler implements Handler {
	
	
	//for reference
public boolean execute(RequestParameters params, Application application)  {
	try {
	log.info("Starting Step AO Entry Card Specification handler execute method for arn {}",application.getArn());
	PostRequest postRequest = new PostRequest();
	List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
    nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.bundle.id"), HSBC_OHD_BUNDLE_ID.concat(","+HSBC_OHD_BUNDLE_ID)));
    nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.currency.code"), HSBC_OHD_CURRENCY_CODE.concat(","+HSBC_OHD_CURRENCY_CODE)));//TODO : Single : SGD ; Dual : SGD,SGD
    /*if(application.getPrimaryApplicant().getCards().get(0).getLimit()==null){
    	log.error("Credit Card limit can not be null");
    	throw new AOException("Credit Card limit can not be null");
    }else{
    	nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.prefered.credit.limit"),"SGD,".concat(application.getPrimaryApplicant().getCards().get(0).getLimit().toString())));	
    }*/
    
    
    
    //GIVING NULL POINTER --- VALUE EXPECTED : Bank to assign credit limit 9999999999999 and actual value : 3000
   //nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.prefered.credit.limit"),"SGD,".concat(application.getPrimaryApplicant().getCards().get(0).getLimit().toString())));
   nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.prefered.credit.limit"),"SGD,3000"));
    //TODO : Please see if correct value is mapped - Credit Limit to be entered Single Card :SGD,3000 ; Dual Card :SGD,3000,SGD,2000
    
    StringJoiner creditCardKeys = new StringJoiner(",");
    List<CreditCard> creditCardList = application.getPrimaryApplicant().getCards();
    creditCardList.stream().forEach(cc -> creditCardKeys.add(cc.getKey()));
    
    
    
    
    if(creditCardKeys.toString().contentEquals("SV1")){
    	 nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.product.id"),"SVI"));
    }
    
    else{
    	nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.product.id"), creditCardKeys.toString()));
    }
  //nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.product.id"), creditCardKeys.toString())); //TODO :value should be for single card : VPC and for dual comma separated : VPC,VP1.
   // nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.product.id"),"SVI")); //TODO :value should be for single card : VPC and for dual comma separated : VPC,VP1.
  //  nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.promotion.code"), DEFAULT_PROMOTION_CODE));//TODO : promo code value wrt channel, if customer channel then fetched  from url else value for all other channels : DEF0000001
   nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.voucher.code"), DataMappingListUtil.getValidValue(application.getStaffDetails().getVoucherCode())));
 
   // nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.voucher.code"),""));
    nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.flow.id"), "AOSGPCC005"));
    nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.launch.command"), "SCM"));
    nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.entry.user.entry.siteID"), "2G_SGH_U")); 
	log.info("Setting url for Step AO Entry Card Specification handler execute method for arn {}",application.getArn());      	
    params.setUrl(PropertiesCache.getInstance().getProperty("ao.entry.url"));		
	log.info("Posting parameters for  Step AO Entry Card Specification handler for arn {}",application.getArn());
	params.setResponseStepAoEntryDetails(postRequest.postPageResponse(nvpList,params));
	log.info("Recorded response of Step AO Entry Card Specification handler execute method for arn {}",application.getArn());
	RequestUtil.extractDynamicParamsJsContinue(params.getResponseStepAoEntryDetails(),params);
	log.info("Extracting Response from AO entry handler execute method for Step JS continue handler for arn {}",application.getArn());	  
	log.info("Finished AO entry handler execute method for arn {}",application.getArn());
	}catch (Exception e) {
		log.error("Error in Step Ao Entry Card Specs Handler", e);
		//throw new AOException("Error inStep Ao Entry Card Specs Handler", e);
	}
	return true;
}

}
