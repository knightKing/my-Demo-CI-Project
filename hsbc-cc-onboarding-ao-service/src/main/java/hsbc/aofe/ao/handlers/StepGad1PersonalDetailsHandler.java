package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.*;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.PersonalDetails.JSF_SEQUENCE;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.PersonalDetails.USER_PREFS;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.*;
import hsbc.aofe.util.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.CollectionUtils;

@Slf4j
public class StepGad1PersonalDetailsHandler implements Handler {

    @Override
    public boolean execute(RequestParameters params, Application application) throws Exception {

        String applicationArn = application.getArn();
        log.info("STRATING: Step GAD1 Personal Details handler execute method for arn {}", applicationArn);
        String CONSTANTS_PD_WIRE_ACTION = "constants.pd.wire.action";
      /*  try {*/
        // Read data from source db or extract data
        PostRequest postRequest = new PostRequest();
        List<NameValuePair> nvpList = new ArrayList<>();

        nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));
        PropertiesCache propertiesCache = PropertiesCache.getInstance();
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty(CONSTANTS_PD_WIRE_ACTION), WIRE_ACTION));
        Applicant primaryApplicant = application.getPrimaryApplicant();
        ApplicantAdditionalInfo primaryApplicantAdditionalInfo = primaryApplicant.getAdditionalInfo();
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.citizenship"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getNationality().getValue(), BLANK_VALUE))); //TODO : Verify value : DD values shared via mail : value : SG	: Value:	SG
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.citzenship.2"), primaryApplicantAdditionalInfo.getNationality2() == null ? BLANK_VALUE : primaryApplicantAdditionalInfo.getNationality2().getValue())); //TODO : Verify value : DD values shared via mail	: Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.citzenship.3"), primaryApplicantAdditionalInfo.getNationality3() == null ? BLANK_VALUE : primaryApplicantAdditionalInfo.getNationality3().getValue())); //TODO : Verify value : DD values shared via mail	: Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.country.of.birth"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getCountryOfBirth().toString()))); //TODO : Verify value : DD values shared via mail : value : SG	 : Value:	SG
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.education.level"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getEducationLevel().getId()))); //TODO : Verify value : DD values shared via mail : : value : 3	 : Value:	3
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.gender.value"), DataMappingListUtil.getValidValue(primaryApplicant.getGender()))); //TODO : VALUE : M/F	//TODO : Value:	F
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.marital.status"), DataMappingListUtil.getValidValue(String.valueOf(primaryApplicantAdditionalInfo.getMaritalStatus().getId())))); //TODO : Verify value : : : value : 1	 : Value:	1
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.mother.maiden.name"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getMothersMaidenName())));    //TODO : Value:	Jeswani
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.multiple.citizenship"), primaryApplicantAdditionalInfo.isMultipleNationality() == true ? "Y" : "N")); // check	Y/N//TODO : Value:	N
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.preferred.language"), PREFERRED_LANGUAGE_CAPS));    //TODO : P:Value:	ENG static
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.type"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getHomeOwnership().getAoValue())));    //TODO : DD VALUES,Value:	1
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.tax1"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getTaxResidenceCountry1().getValue())));    //TODO : DD VALUES,Value:	SG
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.tax2"), primaryApplicantAdditionalInfo.getTaxResidenceCountry2() == null ? BLANK_VALUE : primaryApplicantAdditionalInfo.getTaxResidenceCountry2().getValue()));    //TODO :DD VALUES, Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.tax3"), primaryApplicantAdditionalInfo.getTaxResidenceCountry3() == null ? BLANK_VALUE : primaryApplicantAdditionalInfo.getTaxResidenceCountry3().getValue()));    //TODO :DD VALUES, Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.email.address"), DataMappingListUtil.getValidValue(primaryApplicant.getEmail())));    //TODO : Value:	prachi@gmail.com
        //Home number only country code	//TODO : Value:	11
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.home.number"), primaryApplicantAdditionalInfo.getHomePhone() == null || primaryApplicantAdditionalInfo.getHomePhone() == 0 ? BLANK_VALUE : primaryApplicantAdditionalInfo.getHomePhone().toString())); // check from ao tpsa and ui --- home number	//TODO : 23423423
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.home.area.code"), BLANK_VALUE)); // blank	//TODO : Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.home.country.code"), primaryApplicantAdditionalInfo.getDialCodeHomePhone() == null ? BLANK_VALUE : primaryApplicantAdditionalInfo.getDialCodeHomePhone()));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.mobile.area.code"), BLANK_VALUE)); // blank	//TODO : Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.mobile.number"), primaryApplicant.getMobile() == null || primaryApplicant.getMobile() == 0 ? BLANK_VALUE : primaryApplicant.getMobile().toString()));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.mobile.country.code"), primaryApplicant.getDialCodeMobile() == null ? BLANK_VALUE : primaryApplicant.getDialCodeMobile()));
        // Value for country code +91 etc	//TODO : Value:91
        // value for only mobile number without country code	//TODO : Value:	9876543210
        // nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.mobile.number"),"9630258741"));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.work.area.code"), BLANK_VALUE)); //blank	//TODO : Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.work.country.code"), primaryApplicantAdditionalInfo.getDialCodeHomePhone() == null ? BLANK_VALUE : primaryApplicantAdditionalInfo.getDialCodeHomePhone())); // check value	//TODO : Value:	11
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.work.extension"), BLANK_VALUE));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.work.number"), primaryApplicantAdditionalInfo.getHomePhone() == null || primaryApplicantAdditionalInfo.getHomePhone() == 0 ? BLANK_VALUE : primaryApplicantAdditionalInfo.getHomePhone().toString())); // check from ao tpsa and ui --- home number	//TODO : 23423423
        
        String givenName = "";
        if(primaryApplicant.getName() != null){
        	if(primaryApplicant.getName().getFirstName() != null){
        		givenName = primaryApplicant.getName().getFirstName();
        		if(primaryApplicant.getName().getMiddleName() != null){
        			givenName += " "+ primaryApplicant.getName().getMiddleName();
        		}
        	}
        }
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.first.name"), givenName));

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.last.name"), DataMappingListUtil.getValidValue(primaryApplicant.getName().getLastName())));    //TODO : Value:	Bhambani
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.middle.name"), "")); 
        //if Title is Other
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.prefix"), DataMappingListUtil.getValidValue(primaryApplicant.getSalutation().getValue()))); // check value tpsa link	//TODO : Value:	MISS
        if (primaryApplicant.getSalutation().getValue() == "Other") {
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.sufix"), DataMappingListUtil.getValidValue(primaryApplicant.getSalutation().getValue()))); // check value tpsa link	//TODO : Value:
        } else {
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.sufix"), BLANK_VALUE)); // check value tpsa link	//TODO : Value:
        }
        //contact preferences yet to be finalized
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.contact.preferences"), "Y")); // check value tpsa link	//TODO : Value:	Y
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.preferred.dispatch.mode"), "L")); // check value tpsa link	//TODO : Value:	L
        // country of issue is for passport and will be checked when enum is prepared or entity is ready

        DocumentName documentName = primaryApplicant.getIdDocType();
        if (documentName.equals(DocumentName.EMPLOYMENT_PASS)) {
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.country.of.issue"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getPassportCountry().getValue())));    //TODO : Value:	IF PASSPORT then DD values
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.identification.number"), BLANK_VALUE)); //  same value as for user.pd.passport.number	//TODO : Value:	S8049913B
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.id.type.select"), DocumentName.PASSPORT.getAoValue()));    //TODO : Value:	I , DD values NRIC
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.passport.number"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getPassportNumber()))); // same value as for user.pd.identification.number	//TODO : Value:	Passport no.

        } else if (documentName.equals(DocumentName.NRIC)) {
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.country.of.issue"), BLANK_VALUE));    //TODO : Value:	IF PASSPORT then DD values
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.identification.number"), DataMappingListUtil.getValidValue(primaryApplicant.getIdValue()))); //  same value as for user.pd.passport.number	//TODO : Value:	S8049913B
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.id.type.select"), documentName.getAoValue()));    //TODO : Value:	I , DD values NRIC
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.passport.number"), BLANK_VALUE)); // same value as for user.pd.identification.number	//TODO : Value:	Passport no.
        }


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.addr.indicator"), DataMappingListUtil.getValidValue(String.valueOf(primaryApplicantAdditionalInfo.isResidentialAddIdenticalToPermAddress())))); // Residential Address Indicator , TRUE/FALSE	//TODO : Value:	TRUE

        List<Address> addressList = primaryApplicantAdditionalInfo.getAddress();

        List<String> tempAddressList = new ArrayList<>();
        tempAddressList.add(AddressType.PERMANENT.getValue());
        tempAddressList.add(AddressType.OFFICE.getValue());
        tempAddressList.add(AddressType.PREVIOUS.getValue());
        tempAddressList.add(AddressType.RESIDENTIAL.getValue());
        for (Address address : addressList) {
            switch (address.getAddressType()) {
                case PERMANENT:
                    tempAddressList.remove(AddressType.PERMANENT.getValue());
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.permanent.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.permanent.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.permanent.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.permanent.zipcode.1"), DataMappingListUtil.getValidValue(String.valueOf(address.getPostalCode()))));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.permanent.foreign.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.permanent.foreign.address.line.2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.permanent.foreign.address.line.3"), BLANK_VALUE));
                    } else {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.permanent.foreign.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.permanent.foreign.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.permanent.foreign.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address())));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.permanent.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.permanent.address.line.2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.permanent.address.line.3"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.permanent.zipcode.1"), BLANK_VALUE));

                    }
                    break;
                case RESIDENTIAL:
                    tempAddressList.remove(AddressType.RESIDENTIAL.getValue());
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.zipcode.1"), DataMappingListUtil.getValidValue(String.valueOf(address.getPostalCode()))));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.residential.foreign.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.residential.foreign.address.line.2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.residential.foreign.address.line.3"), BLANK_VALUE));
                    } else {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.foreign.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.foreign.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residential.foreign.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address())));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.residential.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.residential.address.line.2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.residential.address.line.3"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.residential.zipcode.1"), BLANK_VALUE));

                    }
                    break;
                case PREVIOUS:
                    tempAddressList.remove(AddressType.PREVIOUS.getValue());
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue())));
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.zipcode.1"), DataMappingListUtil.getValidValue(address.getPostalCode())));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.previous.residential.foreign.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.previous.residential.foreign.address.line.2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.previous.residential.foreign.address.line.3"), BLANK_VALUE));

                    } else {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.foreign.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.foreign.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.foreign.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address())));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.previous.residential.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.previous.residential.address.line.2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.previous.residential.address.line.3"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.previous.residential.zipcode.1"), BLANK_VALUE));
                    }
                    break;
                case OFFICE:
                    tempAddressList.remove(AddressType.OFFICE.getValue());
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.country"), DataMappingListUtil.getValidValue(address.getCountry().getValue(), SINGAPORE_COUNTRY_CODE)));
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.address.line2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.zipcode.1"), DataMappingListUtil.getValidValue(address.getPostalCode())));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.employer.foreign.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.employer.foreign.address.line.2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.employer.foreign.address.line.3"), BLANK_VALUE));

                    } else {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.foreign.address.line.1"), DataMappingListUtil.getValidValue(address.getLine1Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.foreign.address.line.2"), DataMappingListUtil.getValidValue(address.getLine2Address())));
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.foreign.address.line.3"), DataMappingListUtil.getValidValue(address.getLine3Address())));
                        // adding blank values as this is mandatory to send values in AO
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.employer.address.line.1"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.employer.address.line2"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.employer.address.line.3"), BLANK_VALUE));
                        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.pd.employer.zipcode.1"), BLANK_VALUE));
                    }
                    break;
                default:
                    break;
            }
        }

        if (!CollectionUtils.isEmpty(tempAddressList)) {
            for (String string : tempAddressList) {
                if (AddressType.OFFICE.getValue().equalsIgnoreCase(string)) {

                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.address.line.1"), "")); // mails to check current office address line 1	//TODO : Value:	maxlength="35"
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.address.line2"), "")); // mails to check current office address line 2	//TODO : Value:	maxlength="35"
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.address.line.3"), ""));// mails to check current office address line 3	//TODO : Value:	maxlength="35"
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.country"), "SG")); // mails to check current office address line 1	//TODO : Value:	SG
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.zipcode.1"), "")); // check current office address line 1	//TODO : Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.foreign.address.line.1"), BLANK_VALUE)); // mails to mails to check current foreign office address line 1	//TODO : Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.foreign.address.line.2"), BLANK_VALUE)); // mails to check current foreign office address line 2	//TODO : Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.employer.foreign.address.line.3"), BLANK_VALUE)); //mails to check current foreign office address line 3	//TODO : Value:


                } else if (AddressType.PREVIOUS.getValue().equalsIgnoreCase(string)) {
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.address.line.1"), ""));    //TODO : how value will be mapped, Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.address.line.2"), ""));   //TODO : how value will be mapped, Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.address.line.3"), ""));   //TODO :how value will be mapped, Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.country"), ""));  //TODO :how value will be mapped, Value:	SG  DD Values
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.zipcode.1"), ""));
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.foreign.address.line.1"), BLANK_VALUE));    //TODO : how value will be mapped, Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.foreign.address.line.2"), BLANK_VALUE));    //TODO :how value will be mapped, Value:
                    nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.previous.residential.foreign.address.line.3"), BLANK_VALUE));    //TODO :how value will be mapped, Value:

                }
            }
        }

        //#dynamic non mandatory to be added 	//TODO :P: Value:	yet to be added
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.send.my.mails.to"), "A")); // or .getCorrespondenceOn() ? Value :Residence :A; OFFICE :E , Value if office then nly need office address otherwise not	//TODO : Value:	A
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.number.of.dependents"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getNoOfDependents().getValue())));    //TODO : Value:	1
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.relationship.bank"), primaryApplicantAdditionalInfo.getAssociateOfPublicPositionHolder() == null || primaryApplicantAdditionalInfo.getAssociateOfPublicPositionHolder() == false ? "N" : "Y")); //Any relationship with hsbc employee ?	//TODO : Value:	N
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.relationship.bank.details"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getAssociateOfPublicPositionHolderDetails()))); //Any relationship with hsbc employee details?	//TODO : Value:	 Staff/Director name cannot have more than 40 characters.
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.former.last.name"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getOtherName())));    //TODO : Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.have.any.former.names"), primaryApplicantAdditionalInfo.getOtherName() == null ? "N" : "Y")); // check value Y/N	//TODO : Value:	N
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.other.last.name"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getOtherName())));
        Map<String, String> durationAtResidenceInMonthsAndYears = DataMappingListUtil.durationInYearsAndMonths(primaryApplicantAdditionalInfo.getMonthsAtResidentialAddress());
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residence.duration.months"), DataMappingListUtil.convertsingleDigitToTwoDigit(durationAtResidenceInMonthsAndYears.get("month"))));    //TODO : Value:	3  DD Values
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.residence.duration.years"), DataMappingListUtil.convertsingleDigitToTwoDigit(durationAtResidenceInMonthsAndYears.get("year")))); // get years	//TODO : Value:	6  DD Values
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.submit.idcl"), SUBMIT_IDCL));    //TODO : Value:	submit
        LocalDate applicantDateOfBirth = CommonUtils.convertStringToLocalDate(primaryApplicant.getDateOfBirth());
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.dob.month"), DataMappingListUtil.convertMonthToCamelCaseFormat(applicantDateOfBirth.getMonth().toString()))); //Month	//TODO : Value:	August  DD Values
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.dob.date"), DataMappingListUtil.convertsingleDigitToTwoDigit(String.valueOf(applicantDateOfBirth.getDayOfMonth())))); // Date	//TODO : Value:	21  DD Values
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.dob.year"), String.valueOf(applicantDateOfBirth.getYear()))); // Year	//TODO : Value:	1993  DD Values
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.aaplication.receipt.date"), String.valueOf(LocalDate.now().getYear()))); //Current Year value i.e eg : 2017	//TODO : Value:	2017
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.submit"), SUBMIT_PAGE_DETAILS));    //TODO : Value:	1
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.continue.capture.employment.additional.details"), CONTINUE_TO_NEXT_PAGE));    //TODO : Value:	Continue
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.jsf.sequence"), JSF_SEQUENCE));    //TODO : Value:	1
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.user.prefs"), USER_PREFS)); // Recheck if static or not	//TODO : Value:
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.referral"), primaryApplicantAdditionalInfo.isIntroducedByHSBCCardHolder() == false ? "N" : "Y")); // check this is checkbox Y/N	//TODO : Value:	N
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.referral.details"), DataMappingListUtil.getValidValue(primaryApplicantAdditionalInfo.getIntroducedByHSBCCardHolderReferrerID())));    //TODO : Value:

        if (params.getChannelFlag().equalsIgnoreCase(ChannelType.CU.getAoValue())) {
            //Customer J
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.preapproved.limit"), BLANK_VALUE));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.special.instruction.indicator"), BLANK_VALUE));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.market.sector.code.1.group.text"),
                    MarketSectorCode.PERSONAL_MASS_MARKET_THOSE_NOT_DEFINED.getValue()));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.gho.classification.group.text"),
                    GhoClassification.PERSONAL.getValue()));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.source.of.application"),
                    application.getStaffDetails().getSourceCode() == null ? SourceCode._E5.getValue() : application.getStaffDetails().getSourceCode().getValue()));
        } else {
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.preapproved.limit"),
                    application.getStaffDetails().getPreApprovedLimit() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(application.getStaffDetails().getPreApprovedLimit())));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.special.instruction.indicator"),
                    application.getStaffDetails().getSplInstIndicator() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(application.getStaffDetails().getSplInstIndicator().getValue())));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.market.sector.code.1.group.text"),
                    DataMappingListUtil.getValidValue(application.getStaffDetails().getMarketSectorCode().getValue())));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.gho.classification.group.text"),
                    DataMappingListUtil.getValidValue(application.getStaffDetails().getGhoCode().getValue())));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.pd.source.of.application"),
                    application.getStaffDetails().getSourceCode() == null ? BLANK_VALUE : DataMappingListUtil.getValidValue(application.getStaffDetails().getSourceCode().getValue())));
        }
        //TODO : what is : application.getExistingCardNumber()
        log.info("Posting parameters for Step GAD1 Personal Details handler for arn {}", applicationArn);

        String respS1 = postRequest.postPageResponse(nvpList, params);
        params.setResponseStep1PersonalDetails(respS1);
        log.info("Response recorded for  Step GAD1 Personal Details handler for arn {}", applicationArn);
        RequestUtil.extractDynamicParamsStep2(respS1, params);
        log.info("Parameters Extracted from  Step GAD1 Personal Details handler response for Step GAD2 Employment Details Handler for arn {}", applicationArn);

       /* } catch (Exception e) {
            //String AoReferenceNumber = params.getAoApplicationReferenceNumber();
        	//log.error("Error in Step Gad1 Personal Details Handler", e);
        	//log.debug(AoReferenceNumber);
			System.out.println(AoReferenceNumber);
			log.error("Error in Step Gad1 Personal Details Handler", e);
			if (StringUtils.isNotEmpty(params.getAoApplicationReferenceNumber())) {
				String AoReferenceNumber = params.getAoApplicationReferenceNumber();
				log.debug(AoReferenceNumber);
				System.out.println(AoReferenceNumber);
			}					return AOReferenceNumber;	
            log.error("Error in Step Gad1 Personal Details Handler", e);
            //throw new AOException("Error in Step Gad1 Personal Details Handler", e);
        }*/
        log.info("FINISHED: Step GAD1 Personal Details Handler execute method for arn {}", applicationArn);
        return Boolean.TRUE;
    }

}
