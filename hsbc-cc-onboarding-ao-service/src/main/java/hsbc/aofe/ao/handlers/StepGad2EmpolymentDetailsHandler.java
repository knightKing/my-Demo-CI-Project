package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.*;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Single.EmploymentDetails.*;
import static hsbc.aofe.ao.util.DataMappingListUtil.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.CollectionUtils;

@Slf4j
public class StepGad2EmpolymentDetailsHandler implements Handler {

    @Override
    public boolean execute(RequestParameters params, Application application) throws Exception {
        /*try {*/
        String applicationArn = application.getArn();
        log.info("STRATING: Step GAD2 Employment Details handler execute method for applicationArn {}", applicationArn);

        PostRequest postRequest = new PostRequest();
        List<NameValuePair> nvpList = new ArrayList<>();
        nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));
        PropertiesCache propertiesCache = PropertiesCache.getInstance();
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("constant.ed.wire.action"), SEND_ACTION_GAD));

        Applicant primaryApplicant = application.getPrimaryApplicant();
        ApplicantAdditionalInfo primaryApplicantAdditionalInfo = primaryApplicant.getAdditionalInfo();
        List<Address> addressList = primaryApplicantAdditionalInfo.getAddress();
        if (!CollectionUtils.isEmpty(addressList)) {
            for (Address address : addressList) {
                if (address.getAddressType().equals(AddressType.OFFICE)) {
                    if (address.getCountry().getValue().equals(SINGAPORE_COUNTRY_CODE)) {
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.1"), getValidValue(address.getLine1Address()))); // mails to check current office address line 1	//TODO : Value:	maxlength="35"
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.2"), getValidValue(address.getLine2Address()))); // mails to check current office address line 2	//TODO : Value:	maxlength="35"
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.3"), getValidValue(address.getLine3Address()))); // mails to check current office address line 3	//TODO : Value:	maxlength="35"
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.country"), getValidValue(address.getCountry().getValue()))); // mails to check current office address line 1	//TODO : Value:	SG
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.zip.code.1"), getValidValue(String.valueOf(address.getPostalCode())))); // check current office address line 1	//TODO : Value:
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.1"), BLANK_VALUE)); // mails to mails to check current foreign office address line 1	//TODO : Value:
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.2"), BLANK_VALUE)); // mails to check current foreign office address line 2	//TODO : Value:
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.3"), BLANK_VALUE)); //mails to check current foreign office address line 3	//TODO : Value:
                    } else {

                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.1"), getValidValue(address.getLine1Address()))); // mails to mails to check current foreign office address line 1	//TODO : Value:
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.2"), getValidValue(address.getLine2Address()))); // mails to check current foreign office address line 2	//TODO : Value:
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.3"), getValidValue(address.getLine3Address()))); //mails to check current foreign office address line 3	//TODO : Value:
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.country"), getValidValue(address.getCountry().getValue()))); // mails to check current office address line 1	//TODO : Value:	SG
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.1"), BLANK_VALUE)); // mails to check current office address line 1	//TODO : Value:	maxlength="35"
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.2"), BLANK_VALUE)); // mails to check current office address line 2	//TODO : Value:	maxlength="35"
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.3"), BLANK_VALUE)); // mails to check current office address line 3	//TODO : Value:	maxlength="35"
                        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.zip.code.1"), BLANK_VALUE)); // check current office address line 1	//TODO : Value:
                    }
                }
            }
        } else {
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.1"), "")); // mails to check current office address line 1	//TODO : Value:	maxlength="35"
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.2"), "")); // mails to check current office address line 2	//TODO : Value:	maxlength="35"
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.address.line.3"), "")); // mails to check current office address line 3	//TODO : Value:	maxlength="35"
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.country"), "")); // mails to check current office address line 1	//TODO : Value:	SG
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.zip.code.1"), "")); // check current office address line 1	//TODO : Value:
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.1"), BLANK_VALUE)); // mails to mails to check current foreign office address line 1	//TODO : Value:
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.2"), BLANK_VALUE)); // mails to check current foreign office address line 2	//TODO : Value:
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employer.foreign.address.line.3"), BLANK_VALUE)); //mails to check current foreign office address line 3	//TODO : Value:

        }


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.additional.income"), getValidValue(application.getAdditionalIncome()))); //TODO :,,Value:,100
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employers.name"), getValidValue(primaryApplicantAdditionalInfo.getCompanyName()))); //TODO :,,Value:,Newgen Software Technologies
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employment.status"), getValidValue(primaryApplicantAdditionalInfo.getEmploymentType().getAoValue()))); //TODO :,Value:,F
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.gross.annual.salary"), primaryApplicantAdditionalInfo.getAnnualIncome() == null || primaryApplicantAdditionalInfo.getAnnualIncome() == 0 ? BLANK_VALUE : primaryApplicantAdditionalInfo.getAnnualIncome().toString())); //TODO :,,Value:,50000
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.gross.annual.salary.currency"), ANNUAL_SALARY_CURRENCY)); //TODO :,Static common,Value:,SGD
        //check value : business code and business description
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.job.title"), getValidValue(primaryApplicantAdditionalInfo.getPosition())));
        // if nature of business is from enum ; if it is chosen as others then description is given
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.nature.of.business.code"), primaryApplicantAdditionalInfo.getIndustryType() == null ? BLANK_VALUE : getValidValue(primaryApplicantAdditionalInfo.getIndustryType().getValue()))); //TODO :P:check tpsa,Value from DD values,Value:,3
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.nature.of.business.description"), ""));
        //nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ed.nature.of.business.description"), "")); //TODO :,,Value:,
        /*if(application.getPrimaryApplicant().getAdditionalInfo().getIndustryType().getValue()==NatureOfBusiness.OTHER.toString()){
            nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ed.nature.of.business.description"), "")); //TODO :,,Value:,
		}
		else{
			nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ed.nature.of.business.description"), "")); //TODO :,,Value:,
		}*/

        //Leave non mandatory values
        /*nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-businessLine), application.additionalInfo)); //TODO :,Non mandatory,Value:,NA
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-customerGroup), application.additionalInfo)); //TODO :,Non mandatory,Value:,PFS
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-hide_1), application.additionalInfo)); //TODO :,Non mandatory,Value:,Visa Platinum
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-hide_2), application.additionalInfo)); //TODO :,Non mandatory,Value:,IB
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-hide_3), application.additionalInfo)); //TODO :,Non mandatory,Value:,HBSP_Visa Platinum
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-language), application.additionalInfo)); //TODO :,Non mandatory,Value:,en
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-promoCode), application.additionalInfo)); //TODO :,Non mandatory,Value:,DEF0000001
		nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty(ao-gad-wt-retrieveAppIndicator), application.additionalInfo)); //TODO :,Non mandatory,Value:,N
*/
        /*Long val = application.getPrimaryApplicant().getAdditionalInfo().getTimeAtPreviousEmployer();*/
        Map<String, String> durationPreviousEmployerInYearsAndMonths = durationInYearsAndMonths(primaryApplicantAdditionalInfo.getTimeAtPreviousEmployer());

        Map<String, String> lengthOfServiceInYearsAndMonths =
                durationInYearsAndMonths(primaryApplicantAdditionalInfo.getLengthOfService());
        Map<String, String> remainingContractInYearsAndMonths =
                durationInYearsAndMonths(primaryApplicantAdditionalInfo.getMonthsToEmpPassExpiry());


        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.length.of.service.months"), getTwoDigitNumber(lengthOfServiceInYearsAndMonths.get("month"))));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.months.with.former.employer"), getTwoDigitNumber(durationPreviousEmployerInYearsAndMonths.get("month"))));
        //nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ed.occupation"),"09"));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.occupation"), primaryApplicantAdditionalInfo.getOccupation() == null ? BLANK_VALUE : getValidValue(primaryApplicantAdditionalInfo.getOccupation().getValue())));  //TODO :,,Value:,9

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.length.of.service.years"), getYearForMoreThan30YearCase(getTwoDigitNumber(lengthOfServiceInYearsAndMonths.get("year")))));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.length.of.service.years"), getYearForMoreThan30YearCase(getTwoDigitNumber(lengthOfServiceInYearsAndMonths.get("year")))));

        if (primaryApplicantAdditionalInfo.getMonthsToEmpPassExpiry() != null) {
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.remaining.contract.months"), getTwoDigitNumber(remainingContractInYearsAndMonths.get("month"))));
            nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.remaining.contract.years"), getYearForMoreThan30YearCase(getTwoDigitNumber(remainingContractInYearsAndMonths.get("year")))));
        }

        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.years.with.former.employer"), getTwoDigitNumber(durationPreviousEmployerInYearsAndMonths.get("year"))));
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.nature.of.relationship"), getValidValue(primaryApplicantAdditionalInfo.getPublicPositionDetails()))); //TODO :,,Value:,
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.senior.public.office"), primaryApplicantAdditionalInfo.getHoldingPublicPosition() == null || primaryApplicantAdditionalInfo.getHoldingPublicPosition() == false ? "N" : "Y")); //TODO :,Value : N/Y,Value:,N
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("constant.ed.continue.capture.supplementary.cardholder.details"), CONTINUE_TO_NEXT_PAGE)); //TODO :,Static common,Value:,Continue
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("constant.ed.jsf_sequence"), JSF_SEQUENCE)); //TODO :,static common,Value:,2
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("constant.ed.capture.additional.details"), SUBMIT_IDCL)); //TODO :,Static common,Value:,submit
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("constant.ed.capture.additional.details.submit"), SUBMIT_PAGE_DETAILS)); //TODO :,Static common,Value:,1
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ed.document.completeness.indicator"), application.isDocCompletenessIndicator() == true ? "Yes" : "No")); //TODO :,Yes/No,Value:,Yes
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ed.employment.check.indicator"), application.isEmploymentCheckIndicator() == true ? "Yes" : "No")); //TODO :,Yes/No,Value:,Yes
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ed.income.automated.calculation"), application.isIncomeAutomatedCalcIndicator() == true ? "Yes" : "No")); //TODO :,Yes/No,Value:,Yes
        
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.verify.income"), getValidIntValue(primaryApplicantAdditionalInfo.getVerifiedIncome()))); //TODO :,,Value:,28000 verified income that is calculated via formula
      /*  nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.document.completeness.indicator"), YES_VALUE)); //TODO :,Yes/No,Value:,Yes
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.employment.check.indicator"), YES_VALUE)); //TODO :,Yes/No,Value:,Yes
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("user.ed.income.automated.calculation"), YES_VALUE));*/
        nvpList.add(new BasicNameValuePair(propertiesCache.getProperty("EssentialDataVerifiedIndicator"), YES_VALUE));

        //TODO : Rahul to provide value: Mapping for Essential Data Prefilled Indicator

        log.info("Posting parameters for Step GAD2 Employment Details handler for applicationArn {}", applicationArn);
        String resp2 = postRequest.postPageResponse(nvpList, params);
        params.setResponseStep2EmploymentDetails(resp2);

        log.info("Response recorded for Step GAD2 Employment Details handler for applicationArn {}, response: {}", applicationArn, resp2);

        if (params.getNumberOfCards().equals(SINGLE_CARD)) {
            RequestUtil.extractDynamicParamsStep3(resp2, params);
        } else {
            RequestUtil.extractDynamicParamsDualCardStep3(resp2, params);
        }
        log.info("Parameters Extracted from Step GAD2 Employment Details handler response for Step Gad3 Supplementary Card Details Handler for applicationArn {}", applicationArn);

		/*}catch (Exception e) {
            log.error("Error in Step Gad2 Empolyment Details Handler", e);
			//throw new AOException("Error in Step Gad2 Empolyment Details  Handler", e);
		}*/
        log.info("FINISHED: Step GAD2 Employment Details Handler execute method for applicationArn {}", applicationArn);
        return Boolean.TRUE;
    }


    private String getYearForMoreThan30YearCase(String years) {
        return Integer.valueOf(years) > 30 ? "99" : years;
    }

}

