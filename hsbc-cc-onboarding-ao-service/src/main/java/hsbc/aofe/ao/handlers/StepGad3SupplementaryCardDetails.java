package hsbc.aofe.ao.handlers;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.ESDS_TOKEN;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.WIRE_ACTION;

import java.util.ArrayList;
import java.util.List;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.request.PostRequest;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

@Slf4j
public class StepGad3SupplementaryCardDetails implements Handler {

    //TODO : DAFAULT VALUES THAT HAVE TO BE POSTED WHEN NO SUPPLEMENTARY CARD IS CHOSEN.
    public boolean execute(RequestParameters params, Application application) throws Exception {
            /*try {*/
        DataMappingListUtil dataMappingUtil = new DataMappingListUtil();
        log.info("Strating Step GAD3 Supplementary Details handler execute method for arn {}", application.getArn());
        PostRequest postRequest = new PostRequest();
        List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
        nvpList.add(new BasicNameValuePair(params.getEdsToken(), ESDS_TOKEN));
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("user.ead.wire.action"), WIRE_ACTION));
        if (!application.getSupplementaryApplicant().isEmpty()) {

        } else {
            //All values will be blank in this case
                /*application.getSupplimentaryApplicant().isEmpty()?Blank:get(0);
				application.getSupplimentaryApplicant().size()>1?get(1):blank;
				application.getSupplimentaryApplicant().size()>2?get(2):blank;*/

        }

        //nvpList.add(new BasicNameValuePair("cardTypeList", params.getCardTypeList()));		
        nvpList.add(new BasicNameValuePair(PropertiesCache.getInstance().getProperty("dynamic.product.list.1"), params.getProductList1()));
        //DYNAMIC VALUES : yet to be added ao-hbap-gad-cscdp-cbaProductTrackerIDListString:08041335935827139,

        log.info("Posting parameters for Step GAD3 Supplementary Details handler for arn {}", application.getArn());
        params.setResponseStep3SupplementaryDetails(postRequest.postPageResponse(nvpList, params));
        log.info("Response recorded for  Step GAD3 Supplementary Details handler for arn {}", application.getArn());
        RequestUtil.extractDynamicParamsStep4(params.getResponseStep3SupplementaryDetails(), params);
        log.info("Parameters Extracted from Step GAD3 Supplementary Details handler response for Step Gad4 Review Details Handler for arn {}", application.getArn());
        log.info("Finished Step GAD3 Supplementary Details Handler execute method for arn {}", application.getArn());
			/*}catch (Exception e) {
				log.error("Error in Step Gad3 Supplementary Details Handler", e);
				//throw new AOException("Error in  Gad3 Supplementary Details Handler", e);
			}*/
        return true;
    }

}
