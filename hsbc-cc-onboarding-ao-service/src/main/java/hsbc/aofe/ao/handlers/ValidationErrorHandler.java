package hsbc.aofe.ao.handlers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ValidationErrorHandler implements ErrorHandler {

	private ErrorHandler chain;

	@Override
	public void setNextChain(ErrorHandler nextChain) {
		this.chain = nextChain;

	}

	@Override
	public void extractError(String html, String validationErrorId, StringBuilder errorString) {
		Document doc = Jsoup.parse(html);
		Element errorUl = null!=validationErrorId?doc.getElementById(validationErrorId):null;

		if (null != errorUl) {
			// errorUl iterate li of this
			Elements errorList = errorUl.children();

			String errorMessage = "";
			for (Element element : errorList) {
				log.debug(element.text());
				errorMessage = errorMessage.concat(element.text()).concat("\n");
				log.debug("Error extracted from AO response : " + errorMessage);
				System.out.println("Error extracted from AO response : " + errorMessage);
			}
			log.debug("Validation Error extracted from AO response : " + errorMessage);
			System.out.println("Validation Error extracted from AO response : " + errorMessage);
			errorString.append(errorMessage);
		} else {
			this.chain.extractError(html, null, errorString);
		}

	}

}
