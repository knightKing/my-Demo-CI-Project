package hsbc.aofe.ao.process;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.DUAL_CARD;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SINGLE_CARD;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.protocol.HttpClientContext;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.handlers.Executor;
import hsbc.aofe.ao.util.ClientBuilder;
import hsbc.aofe.ao.util.DataMappingListUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.exception.AOException;
import lombok.extern.slf4j.Slf4j;

//TODO :Add Javadoc
@Slf4j
public class AoProcess {

	public String processSingleCard(Application application) {
		String aoReferenceNumber = null;
		try {
			List<CreditCard> creditCards = application.getPrimaryApplicant().getCards();
			Executor processExecutor = new Executor();
			RequestParameters params = new RequestParameters();
			params.setNumberOfCards(SINGLE_CARD);
			params.setFirstCard(creditCards.get(0).getKey());
			params.setLocalContext(HttpClientContext.create());
			params.setClient(ClientBuilder.createBuilder());
			params.setPromoCode(DataMappingListUtil.getValidDefaultValue(application.getStaffDetails().getPromoCode(),"AAAAA"));
			params.setChannelFlag(DataMappingListUtil.getValidValue(application.getInitiatedBy() == null ? "" : application.getInitiatedBy().getAoValue()));
			params.setUrl(RequestUtil.aoUrl(params));		
			log.debug(params.getUrl());
			aoReferenceNumber = processExecutor.execute(params, application);
		} catch (AOException e) {
			throw e;
		} catch (Exception e) {
		    e.printStackTrace();
			throw new AOException("Error in AO submission process, Please contact your system administrator");
		}
		return aoReferenceNumber;
	}

	public String processDualCard(Application application) {
		String aoReferenceNumber = null;
		try {
			List<CreditCard> creditCards = application.getPrimaryApplicant().getCards();
			Executor processExecutor = new Executor();
			RequestParameters params = new RequestParameters();
			params.setNumberOfCards(DUAL_CARD);
			params.setFirstCard(creditCards.get(0).getKey());
			params.setSecondCard(creditCards.get(1).getKey());
			params.setLocalContext(HttpClientContext.create());
			params.setClient(ClientBuilder.createBuilder());
			params.setPromoCode(DataMappingListUtil.getValidDefaultValue(application.getStaffDetails().getPromoCode(),"AAAAA"));
			params.setChannelFlag(DataMappingListUtil.getValidValue(application.getInitiatedBy().getAoValue()));
			params.setUrl(RequestUtil.aoUrl(params));
			log.debug(params.getUrl());
			aoReferenceNumber = processExecutor.execute(params, application);
		} catch (AOException e) {
			throw e;
		} catch (Exception e) {
			throw new AOException("Error in AO submission process, Please contact your system administrator");
		}
		return aoReferenceNumber;
	}



}
