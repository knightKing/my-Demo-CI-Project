package hsbc.aofe.ao.request;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.des.TripleDes;
import hsbc.aofe.ao.util.PageUtil;
import hsbc.aofe.ao.util.RequestUtil;
//TODO :Add Javadoc
public class GetRequest {
	
	private static String USER_AGENT = "Mozilla/5.0";
	
	
	public String getPageContent(RequestParameters params) throws Exception {
		HttpGet request = new HttpGet(params.getUrl());	
		UsernamePasswordCredentials creds = new UsernamePasswordCredentials("hbapao", "apstaff1");
		request.addHeader("User-Agent", USER_AGENT);		
		request.addHeader(new BasicScheme().authenticate(creds, request, params.getLocalContext()));
		//request.addHeader("token",TripleDes.generateToken());
		HttpResponse response = params.getClient().build().execute(request,params.getLocalContext());				
		return RequestUtil.extractCookies(response);
  }	
	public String getPageRedirect(RequestParameters params) throws Exception {
		HttpGet request = new HttpGet(params.getRedirectionUrl());	
		UsernamePasswordCredentials creds = new UsernamePasswordCredentials("hbapao", "apstaff1");
		request.addHeader("User-Agent", USER_AGENT);
		request.setHeader("Cookie",params.getCookies());
		request.addHeader(new BasicScheme().authenticate(creds, request, params.getLocalContext()));
		HttpResponse response = params.getClient().build().execute(request,params.getLocalContext());		
		return PageUtil.returnGetResponse(response,request);	
  }	
}
