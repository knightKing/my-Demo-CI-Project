package hsbc.aofe.ao.request;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.LaxRedirectStrategy;

import hsbc.aofe.ao.data.RequestParameters;
import hsbc.aofe.ao.handlers.ErrorProcessor;
import hsbc.aofe.ao.util.PageUtil;
import hsbc.aofe.ao.util.RequestUtil;
import hsbc.aofe.exception.AOException;
import lombok.extern.slf4j.Slf4j;

//TODO :Add Javadoc
@Slf4j
public class PostRequest {
	
	ErrorProcessor errorProcessor;

		public String postPageResponse(List<NameValuePair> list,RequestParameters params) throws Exception {		
			HttpPost post = new HttpPost(params.getUrl());
			PageUtil.setRequestHeaders(post,params);
			post.setEntity(new UrlEncodedFormEntity(list));
			log.debug("List of params is : "+list);
			HttpResponse response = params.getClient().build().execute(post,params.getLocalContext());
			return PageUtil.returnPostResponse(response,post);
		}	
		
		public String postHeaderResponse(List<NameValuePair> list,RequestParameters params) throws Exception {		
			HttpPost post = new HttpPost(params.getUrl());
			PageUtil.setRequestHeaders(post,params);
			post.setEntity(new UrlEncodedFormEntity(list));
			log.debug("List of params is : "+list);
			HttpResponse response = params.getClient().build().execute(post,params.getLocalContext());
			// TODO : logging for response				
			return RequestUtil.extractRedirectUrl(response,post);
		}
		public String postPageResponse5(List<NameValuePair> list,RequestParameters params, String page) throws Exception {		
			HttpPost post = new HttpPost(params.getUrl());
			PageUtil.setRequestHeaders(post,params);
			post.setEntity(new UrlEncodedFormEntity(list));
			HttpResponse response = params.getClient().setRedirectStrategy(new LaxRedirectStrategy()).build().execute(post,params.getLocalContext());
			String strResponse = PageUtil.returnPostResponse(response,post);
			
			errorProcessor = new ErrorProcessor();
			//check response if there is any error
			/*String processedErrorString = errorProcessor.handleResponseError(strResponse, page);
			
			if(StringUtils.isNotEmpty(processedErrorString)){
				AOException aoException = new AOException("Error in AO submission");
				aoException.setErrorString(processedErrorString);
				aoException.setErrorCode(4101);//refer aofestatus.java for error codes.
				throw aoException;
			}*/
			
			return strResponse;
		}
			
	}

