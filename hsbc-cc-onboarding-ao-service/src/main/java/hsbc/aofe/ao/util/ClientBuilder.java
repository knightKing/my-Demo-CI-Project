package hsbc.aofe.ao.util;

import java.util.concurrent.TimeUnit;

import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.HttpClientBuilder;

import hsbc.aofe.ao.config.PropertiesCache;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;

//TODO :Add Javadoc
public class ClientBuilder {
	// to be moved to yml file
	private static final String PROXY_HOST = PropertiesCache.getInstance().getProperty("proxy.host");
	
	private static final String PROXY_ENABLE = PropertiesCache.getInstance().getProperty("proxy.enable");

	/**
	 * This function returns the HttpCLientBuilder after applying credentials
	 *
	 * @param username
	 * @param password
	 * @return CloseableHttpClient object
	 */
	public static HttpClientBuilder createBuilder() {
		ConnectionReuseStrategy reuseStrategy = new DefaultConnectionReuseStrategy();
		HttpClientConnectionManager connMngr = null;// TODO : verify value
		ConnectionKeepAliveStrategy keepAliveStrategy = new DefaultConnectionKeepAliveStrategy();
		
		if (PROXY_ENABLE.equals("true")) {
			 int PROXY_PORT =Integer.parseInt(PropertiesCache.getInstance().getProperty("proxy.port"));
			return HttpClientBuilder.create()
					.setDefaultCredentialsProvider(
							getCredentialsProvider(PropertiesCache.getInstance().getProperty("proxy.username"),
									PropertiesCache.getInstance().getProperty("proxy.password")))
					.setProxy(new HttpHost(PROXY_HOST, PROXY_PORT)).setConnectionManager(connMngr)
					.setConnectionReuseStrategy(reuseStrategy).setKeepAliveStrategy(keepAliveStrategy)
					.setConnectionTimeToLive(180, TimeUnit.SECONDS);
		} else {
			return HttpClientBuilder.create().setConnectionManager(connMngr).setConnectionReuseStrategy(reuseStrategy)
					.setKeepAliveStrategy(keepAliveStrategy).setConnectionTimeToLive(180, TimeUnit.SECONDS);
		}
	}

	/**
	 * This function set username and password required to access page using
	 * http-client
	 *
	 * @param username
	 * @param password
	 * @return CredentialsProvider object
	 */
	private static CredentialsProvider getCredentialsProvider(String username, String password) {

		/** set required credentials for the card url */
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
		credentialsProvider.setCredentials(AuthScope.ANY, credentials);
		return credentialsProvider;
	}

}
