package hsbc.aofe.ao.util;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.BLANK_VALUE;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.domain.Applicant;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.CollectionUtils;

public class DataMappingListUtil {

    public List<NameValuePair> returnUserValuesList(String Step) {
        // TODO: Move all list name value pair mapping here once all are working
        List<NameValuePair> userValuesList = new ArrayList<NameValuePair>();
        switch (Step) {

            case "step1":

                break;

            case "step2":

                break;

            case "step3":
                break;

            default:
                return userValuesList;
        }
        return userValuesList;
    }

    public static Map<String, String> durationInYearsAndMonths(Long numOfMonths) {

        Map<String, String> durationMap = new HashMap<>();
        long months = 0L;
        long years = 0L;

        if (null != numOfMonths) {
            months = numOfMonths % 12;
            years = (numOfMonths - months) / 12;
        }
        durationMap.put("year", String.valueOf(years));
        durationMap.put("month", String.valueOf(months));
        return durationMap;
    }

    public static String getTwoDigitNumber(String number) {
        return String.format("%02d", Long.valueOf(number));
    }

    public static Long resolveIfLimitIsNull(Long limit) {
        Long defaultLimit = 99999999L;
        return (limit == null) ? defaultLimit : limit;
    }

    public static String convertMonthToCamelCaseFormat(String month) {
        return month.substring(0, 1) + month.substring(1, month.length()).toLowerCase();
    }


    public BasicNameValuePair getBasicNameValuePair(String key, String value) {
        return new BasicNameValuePair(PropertiesCache.getInstance().getProperty(key), value);
    }

    public static String getValidValue(String value) {
        return StringUtils.isBlank(value) ? BLANK_VALUE : value;
    }

    public static String getValidValue(BigInteger value) {
        String aoValue = String.valueOf(value);
        return StringUtils.isBlank(aoValue) ? BLANK_VALUE : aoValue;
    }

    public static String getValidValue(String value, String defaultValue) {
        return StringUtils.isBlank(value) ? defaultValue : value;
    }

    /*public String getValidValue(CountryCode value,String defaultValue){
        return StringUtils.isAnyBlank(value)?BLANK_VALUE:defaultValue;
        //return value==null?defaultValue:value.toString();
    }*/
    public static String getValidValue(long value) {
        return (value == 0) ? BLANK_VALUE : (new Long(value)).toString();
    }
    
    public static String getValidIntValue(Integer value) {
        return (value == null || value == 0) ? BLANK_VALUE : String.valueOf(value);
    }


    public static boolean getValidValue(Applicant value) {
        return value == null ? false : true;
    }


    public static String convertsingleDigitToTwoDigit(String digit) {
        String finalDigit = null;
        if (digit.length() == 1) {
            finalDigit = "0" + String.valueOf(digit);
        } else {
            finalDigit = digit;
        }
        return finalDigit;

    }


    public static Applicant getValidValue(List<Applicant> value, int index) {
        if (!CollectionUtils.isEmpty(value)) {
            try {
                return value.get(index);
            } catch (ArrayIndexOutOfBoundsException e) {
                return null;
            }
        } else {
            return null;
        }

    }

	public static String getValidDefaultValue(String value, String defaultVal) {
		return StringUtils.isEmpty(value) ? defaultVal : value;

	}
    
    
}
