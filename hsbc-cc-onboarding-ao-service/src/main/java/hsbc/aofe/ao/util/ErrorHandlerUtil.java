package hsbc.aofe.ao.util;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import hsbc.aofe.ao.config.PropertiesCache;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ErrorHandlerUtil {

	public static void main(String[] args) {
		ErrorHandlerUtil errorHandlerUtil = new ErrorHandlerUtil();
		portletIsDisabledError(errorHandlerUtil.getFile("C://errorfiles//errorPortletIsDiabled.html"));
		applicationIdApeError(errorHandlerUtil.getFile("errorApplicationId.html"));
		applicationIdApeError(errorHandlerUtil.getFile("errorDecisionApplication.html"));		
		applicationIdApeError(errorHandlerUtil.getFile("errorAo-10-08-decision-error.html"));
		applicationIdApeError(errorHandlerUtil.getFile("errorAo-10-08-decision-error2.html"));
		applicationIdApeError(errorHandlerUtil.getFile("errorAo-open-account-error.html"));		
		applicationIdApeError(errorHandlerUtil.getFile("errorOpenAccount.html"));
		extractValidationError(errorHandlerUtil.getFile("errorValidationGenderPersonalDetails.html"),PropertiesCache.getInstance().getProperty("error.validation.personal.details.id"));		
		extractValidationError(errorHandlerUtil.getFile("errorValidationAllPersonalDetails.html"),PropertiesCache.getInstance().getProperty("error.validation.personal.details.id"));
		extractValidationError(errorHandlerUtil.getFile("errorValidationEmploymentDetails.html"),PropertiesCache.getInstance().getProperty("error.validation.employment.details.id"));	
		generalErrorOccured(errorHandlerUtil.getFile("errorGeneralErrorOccurred.html"));
	}

	private String getFile(String fileName) {

		StringBuilder result = new StringBuilder("");

		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result.toString();
	}

	// generic method for capturing validation error in AO at any page, yet to
	// be tested
	
	public static String extractValidationError(String html, String validationErrorId) {
		Document doc = Jsoup.parse(html);
		Element errorUl = doc.getElementById(validationErrorId);
		// errorUl iterate li of this
		Elements errorList = errorUl.children();

		String errorMessage = "";
		for (Element element : errorList) {
			log.debug(element.text());
			errorMessage = errorMessage.concat(element.text()).concat("\n");
			log.debug("Error extracted from AO response : "+errorMessage);
			System.out.println("Error extracted from AO response : "+errorMessage);
		}
		log.debug("Validation Error extracted from AO response : "+errorMessage);
		System.out.println("Validation Error extracted from AO response : "+errorMessage);
		return errorMessage;
		
	}

	// method for capturing portlet is diabled error

	public static String portletIsDisabledError(String html) {
		String portletIsDiabledId = PropertiesCache.getInstance().getProperty("error.block.cs.main.id");
		try {
			Element csMainIdBlock = extractCsMainErrorBlock(html);
			Element errorUl = csMainIdBlock.getElementById(portletIsDiabledId);		
			String spanText = errorUl.child(0).getElementsByTag("span").text();
			log.debug("Error extracted from AO response : "+spanText);
			System.out.println("Error extracted from AO response : "+spanText);
			spanText="AO Error Response:".concat(spanText);
			return spanText;
		}

		// error ouput <b>Diagnostic information: </b><strong>-1-0705-72086485
		// </strong><h3>System is temporarily unavailable. (Ref. ERR-RSNCD)</h3>
		// <h3>Application ID: 08283523722373349</h3><b>Service Name: (not
		// available)</b>

		catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	// error ouput <b>Diagnostic information: </b><strong>-1-0705-72086485
				// </strong><h3>System is temporarily unavailable. (Ref. ERR-RSNCD)</h3>
				// <h3>Application ID: 08283523722373349</h3><b>Service Name: (not
				// available)</b>	
	public static String applicationIdApeError(String html) {
		
		try {
			Element csMainIdBlock = extractCsMainErrorBlock(html);
			
			Elements errorTextElements = csMainIdBlock.children();
			String errorText = errorTextElements.text();
			log.debug("Error extracted from AO response : "+errorText);
			System.out.println("Error extracted from AO response : "+errorText);
			errorText="AO Error Response:".concat(errorText);
			return errorText;
		}

		catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	// method for capturing <b>General Error Occured Please Contact Administrator.</b>

	public static String generalErrorOccured(String html) {
		String portletIsDiabledId = PropertiesCache.getInstance().getProperty("error.block.cs.main.id");
	
		try {
			Element csMainIdBlock = extractCsMainErrorBlock(html);
			Element generalErrorElement = csMainIdBlock.getElementById(portletIsDiabledId);
			String errorText = generalErrorElement.children().text();
			log.debug("Error extracted from AO response : "+errorText);
			System.out.println("Error extracted from AO response : "+errorText);
			errorText="AO Error Response:".concat(errorText);
			return errorText;
		}

		catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	public static Element extractCsMainErrorBlock(String html) {
		String extractCsMainErrorBlock = PropertiesCache.getInstance().getProperty("error.block.cs.main.id");
		Document doc = Jsoup.parse(html);
		try {
			Element csMainErrorsElementBlock = doc.getElementById(extractCsMainErrorBlock);			
			return csMainErrorsElementBlock;
		}
		catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	
}