package hsbc.aofe.ao.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import hsbc.aofe.ao.data.RequestParameters;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PageUtil {
	// TODO :Add Javadoc
	private static final String USER_AGENT = "Mozilla/5.0";

	/**
	 * Header for the request.
	 */
	public static void setRequestHeaders(HttpPost request, RequestParameters params) {
		request.setHeader("User-Agent", USER_AGENT);
		request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		request.setHeader("Accept-Encoding", "gzip");
		request.setHeader("Accept-Language", "en-US,en;q=0.8");
		request.setHeader("Authorization", "Basic aGJhcGFvOmFwc3RhZmYx");
		request.setHeader("Cache-Control", "max-age=0");
		request.setHeader("Connection", "keep-alive");
		request.setHeader("Content-Type", "application/x-www-form-urlencoded");
		request.setHeader("Host", "www.ap380.p2g.netd2.hsbc.com.hk");
		request.setHeader("Origin", "https://www.ap380.p2g.netd2.hsbc.com.hk");
		request.setHeader("Cookie", params.getCookies());
	}

	public static String returnPostResponse(HttpResponse response, HttpPost post) throws IOException {
		log.debug("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}		
		String page = result.toString();
		//System.out.println(page);
		
		log.debug(page);
		//WriteToFile.writeIntoFile(page);
		rd.close();
		post.releaseConnection();
		return page;
	}

	public static String returnGetResponse(HttpResponse response, HttpGet request) throws IOException {
		log.debug("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}	
		String page = result.toString();
		log.debug(page);
		//WriteToFile.writeIntoFile(page);
		rd.close();
		request.releaseConnection();
		return page;
	}

}
