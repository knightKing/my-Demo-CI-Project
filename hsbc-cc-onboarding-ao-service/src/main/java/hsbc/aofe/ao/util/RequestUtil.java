package hsbc.aofe.ao.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import hsbc.aofe.ao.config.PropertiesCache;
import hsbc.aofe.ao.data.RequestParameters;
import lombok.extern.slf4j.Slf4j;

import static hsbc.aofe.ao.handlers.StepAoEntryCardSpecsHandler.CBACardTypeMap;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.AVR;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.DU6;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.SVI;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.VP1;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.VPC;

import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.balanceTxSchemeMap_AVR;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.balanceTxSchemeMap_DU6;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.balanceTxSchemeMap_SVI;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.balanceTxSchemeMap_VP1;
import static hsbc.aofe.ao.constants.HsbcConstants.Parameters.Card.Common.balanceTxSchemeMap_VPC;



/**
 * Utility class for request call methods.
 */

@Slf4j
public  class RequestUtil {

	
	//private  String url = null;
	
	private static String edsToken = null;
	public static String hostUrl ="https://www.ap380.p2g.netd2.hsbc.com.hk";
	
	//These values will be used for balance transfer page
	public static Map<String,String> balanceTxSchemeMap = new HashMap<String,String>();
	
	static{
		balanceTxSchemeMap.put(AVR,balanceTxSchemeMap_AVR);
		balanceTxSchemeMap.put(DU6,balanceTxSchemeMap_DU6);
		balanceTxSchemeMap.put(SVI,balanceTxSchemeMap_SVI);
		balanceTxSchemeMap.put(VP1,balanceTxSchemeMap_VP1);
		balanceTxSchemeMap.put(VPC,balanceTxSchemeMap_VPC);
	}
	
	
	/** 
	 * Method Common to all pages
	 * To parse response to into raw html 
	 * @param response : HttpResponse of a call
	 * @return doc : raw html document
	 */
	private static Document parseResponseToString(HttpResponse response){
		String page = response.toString();
		Document doc = Jsoup.parse(page);
		return doc;
	}
	
	/** 
	 * Method Common to all pages
	 * To parse response to into raw html and extract url
	 * @param response : String HttpResponse
	 * @param formName : String formname
	 * @return url : url is to be concatenated with host url
	 */
	public static String extractUrl(String response, String formName) {
		Document doc = Jsoup.parse(response);
		Element javaScriptPage = doc.getElementById(formName);
		return javaScriptPage.attr("action");
	}
	
	/** 
	 * Method for extracting redirection url from headers
	 * @param response : HttpResponse of a call
	 * @return locUrl : location url from headers is returned.
	 */
	public  static String extractRedirectUrl(HttpResponse response,HttpPost post) {
		Header locationUrl =  response.getFirstHeader("Location");
		String locUrl = locationUrl.getValue();			
		post.releaseConnection();	
		return locUrl;
	}
	
	/** 
	 * Method for extracting token from response
	 * @param response : String HttpResponse
	 * @return edsToken : eds token is a dynamic url parameter
	 */
	public static String extractEdsToken(String response, String edsTokenName) {
		Document doc = Jsoup.parse(response);
		Elements nameValue = doc.getElementsByAttributeValueContaining("name",edsTokenName);
		edsToken = nameValue.attr("name");
		return edsToken;
	}
	
	
	/** 
	 * Method for extracting token from response
	 * @param response : String HttpResponse
	 * @return edsToken : eds token is a dynamic url parameter
	 */
	public static String extractNonMandatoryValues(String response, String nameOfElement) {
		Document doc = Jsoup.parse(response);
		Elements nameValue = doc.getElementsByAttributeValue("name",nameOfElement);
		String elementValue = nameValue.attr("value");
		return elementValue;
	}
	
	/** 
	 * Method to extract all parameters both user and dynamic parameters
	 * Method  to add 2 lists 
	 * @param csvList : List<NameValuePair> list one
	 * @param dynamicList : List<NameValuePair> list two
	 * @return paramList : url parameters in the form of List<NameValuePair>
	 */
	public static List<NameValuePair> extractAllUrlParams(List<NameValuePair> csvList,List<NameValuePair>dynamicList) throws IOException {	
		List<NameValuePair> urlParameterList = new ArrayList<NameValuePair>(); 
		for (NameValuePair nameValuePair : csvList)  {
		urlParameterList.add(new BasicNameValuePair(nameValuePair.getName(),nameValuePair.getValue()));
		}
		for (NameValuePair nameValuePair : dynamicList)  {
			urlParameterList.add(new BasicNameValuePair(nameValuePair.getName(),nameValuePair.getValue()));
			}
		return 	urlParameterList;
	}
	
	/** 
	 * Method to extract all parameters both user and dynamic parameters
	 * Method  to add 2 lists 
	 * @param csvList : List<NameValuePair> list one
	 * @param dynamicList : List<NameValuePair> list two
	 * @return paramList : url parameters in the form of List<NameValuePair>
	 */
	public static String extractCookies(HttpResponse response) {
		String code = "";
		String codeLbCookie = "";
		String codeJsessionId = "";
		String codeCamToken = "";
		String cookies = null;
		Header[] headers = response.getAllHeaders();
		for (Header header : headers) {		
			if (header.getValue().contains("LB_COOKIE_1")){
				codeLbCookie = header.getValue().substring(header.getValue().indexOf("code=")+1);
			}
			if (header.getValue().contains("JSESSIONID")){
				codeJsessionId = header.getValue().substring(header.getValue().indexOf("code=")+1);
			}
			if (header.getValue().contains("CAMToken")){
				codeCamToken = header.getValue().substring(header.getValue().indexOf("code=")+1);
			}
		}
		// TODO : logging for response
		cookies= codeLbCookie.substring(0, 33).concat(codeJsessionId.substring(0, 49)).concat(codeCamToken.substring(0, 38));
		return cookies;	
	}
	
	
	/** 
	 * Method to extract Step 1 url in response of redirection call
	 * @param response : String response
	 * @return paramList : url parameters in the form of List<NameValuePair>
	 */
	public static String extractUrlJsContinue(String response) {	
		// TODO : formName will be moved to properties file
		String formName = "wiringForm";
		String url = hostUrl.concat(extractUrl(response,formName));
		return url;
	}
	

	//Step 1: To set dynamic params for step 1 from response redirection step
	public static void extractDynamicParamsJsContinue(String page,RequestParameters params){					
		params.setUrl(extractUrlJsContinue(page));	
	}
	
	
		
	/** 
	 * Method to extract Step 1 url in response of redirection call
	 * @param response : String response
	 * @return paramList : url parameters in the form of List<NameValuePair>
	 */
	public static String extractUrlPersonalDetails(String response) {	
		// TODO : formName will be moved to properties file
		String formName = "capturePersonalDetails";
		String url = hostUrl.concat(extractUrl(response,formName));
		return url;
	}

	/** 
	 * Method to extract Step 1 ESDS token in response of redirection call
	 * @param response : String response
	 * @return paramList : url parameters in the form of List<NameValuePair>
	 */
	//Step 1 parameters in response of redirection call
	public static String edsTokenPersonalDetails(String html){
		// TODO : nameValue will be moved to properties file
		String nameValue =  "ESDS_TOKEN-capturePersonalContactdtlPage1:";				
		return extractEdsToken(html,nameValue);	
	}
	
	
	//Step 1: To set dynamic params for step 1 from response redirection step
	public static void extractDynamicParamsStep1(String page,RequestParameters params){					
		params.setEdsToken(edsTokenPersonalDetails(page));
		params.setUrl(extractUrlPersonalDetails(page));	
		params.setAoApplicationReferenceNumber(extractApplicationIdFromGad1Page(page));
	}

	
	/** 
	 * Method to extract Step 2 url in response of redirection call
	 * @param response : String response
	 * @return paramList : url parameters in the form of List<NameValuePair>
	 */
	public static String extractUrlAdditionalDetails(String html) {	
		// TODO : formName will be moved to properties file
				String formName = "viewns".concat("_").concat("7").concat("_").concat("I9KAGO01G8R250IQP27VCE2GE7").concat("_").concat(":").concat("capture").concat("_").concat("additional").concat("_").concat("details");
				String url = hostUrl.concat(extractUrl(html,formName));
				return url;
			}
			
	//Step 2 parameters 
	public static String edsTokenAdditionalDetails(String html){
		// TODO : nameValue will be moved to properties file
				String nameValue =  "ESDS_TOKEN-captureEmploymentAdditionalDtlPage1:";				
				return extractEdsToken(html,nameValue);	
			}
	
	//To set dynamic params for step 2 from response step 1
	public static void extractDynamicParamsStep2(String page,RequestParameters params){					
		params.setEdsToken(edsTokenAdditionalDetails(page));
		params.setUrl(extractUrlAdditionalDetails(page));	
	}

	//Step 3 :Supplementary card details Page Dynamic Parmas from Step 2 response
	public static String productListTracker(String html){
		Document doc = Jsoup.parse(html);
		// TODO : input will be moved to properties file
		String input ="ao-hbap-gad-cscdp-cbaProductTrackerIDListString";
		Element javaScriptPage = doc.getElementById(input);
		String cbaProductTrackerIDListString = javaScriptPage.attr("value");
		return cbaProductTrackerIDListString;	
	}
	
	
	//Step 3 :Supplementary card details Page Dynamic Parmas from Step 2 response 
	//02290556198017828
	public static String productList1(String html){
		Document doc = Jsoup.parse(html);
		// TODO : inputId will be moved to properties file
		String inputId ="viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:captureSupplementCardholderDetails:ao-gad-cscdp-creditCardProductList:0:ao-gad-jond-product";
		Element javaScriptPage = doc.getElementById(inputId);
		String productList1 = javaScriptPage.attr("value");
		return productList1;		
	}
	
	//02290556198017828
	public static String productList1Second(String html){
		Document doc = Jsoup.parse(html);
		// TODO : inputId will be moved to properties file
		String inputId ="viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:captureSupplementCardholderDetails:ao-gad-cscdp-creditCardProductList:1:ao-gad-jond-product";
		Element javaScriptPage = doc.getElementById(inputId);
		String productList1 = javaScriptPage.attr("value");
		return productList1;		
	}
	
	//Step 3 :Supplementary card details Page Dynamic Parmas from Step 2 response
	public static String getDocumentdataSupplementaryCard(String html) {
		Document doc = Jsoup.parse(html);
		// TODO : formName will be moved to properties file
		String formName = "viewns".concat("_").concat("7").concat("_").concat("I9KAGO01G8R250IQP27VCE2GE7").concat("_").concat(":").concat("captureSupplementCardholderDetails");
		Element javaScriptPage = doc.getElementById(formName);
		String url = javaScriptPage.attr("action");
		return url;
		}
	
	//Step 3 parameters 

	public static String productName(String html){
		Document doc = Jsoup.parse(html);
		// TODO : inputId will be moved to properties file
		String inputId ="viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:reviewdeatils:_idJsp1493ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:ao-gad-rcscdp-creditCardProductList:0:ao-hbap-gad-rcscdp-productname";
		//Element javaScriptPage = doc.getElementById(inputId);
		Elements javaScriptPage = doc.getElementsByAttributeValue("name", "productList1");
		String productName = javaScriptPage.attr("value");
		
		return productName;		
	}
	
	public static String productNameSecond(String html){
		Document doc = Jsoup.parse(html);
		// TODO : inputId will be moved to properties file
		String inputId ="viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:reviewdeatils:_idJsp1493ns_7_I9KAGO01G8R250IQP27VCE2GE7_:review_supplementary_card_holder_review:ao-gad-rcscdp-creditCardProductList:1:ao-hbap-gad-rcscdp-productname";
		Element javaScriptPage = doc.getElementById(inputId);
		//Elements javaScriptPage = doc.getElementsByAttributeValue("name", "productList1");
		String productName = javaScriptPage.attr("value");
		return productName;		
	}
	
	//full prod name HSBC Visa Platinum Credit Card - VPC
	public static String productNameText(String html){
		Document doc = Jsoup.parse(html);
		// TODO : inputId will be moved to properties file
				Elements javaScriptPage = doc.getElementsByAttributeValue("for", "viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:captureSupplementCardholderDetails:ao-gad-cscdp-creditCardProductList:0:ao-gad-jond-product");
		String productNameText =  javaScriptPage.text();	
		//System.out.println(productNameText);
		return productNameText;		
	}
	
	//full prod name HSBC Visa Platinum Credit Card - VPC
	public static String productNameTextSecond(String html){
		Document doc = Jsoup.parse(html);
		// TODO : inputId will be moved to properties file	
		Elements javaScriptPage = doc.getElementsByAttributeValue("for", "viewns_7_I9KAGO01G8R250IQP27VCE2GE7_:captureSupplementCardholderDetails:ao-gad-cscdp-creditCardProductList:1:ao-gad-jond-product");
		String productNameTextSecond = javaScriptPage.text();
		//System.out.println(productNameTextSecond);
		return productNameTextSecond;		
	}
	
	
	 public static String cardTypeList(String html,RequestParameters params){
			//params.setProductList1(productList1(html));
		    String productNameText = productNameText(html);
		    String cardType = generateProductNameText(productNameText);			
			String cardTypeList = params.getProductList1()+"-"+cardType;
			// TO MAKE IT GENERIC according to card type
			return cardTypeList;
			
		}
	 public static String cardTypeListSecond(String html,RequestParameters params){
			//params.setProductList1(productList1Second(html));
		 	String productNameTextSecond = productNameTextSecond(html);
		    String cardTypeSecond = generateProductNameText(productNameTextSecond);
			String cardTypeListSecond = params.getProductList1Second()+"-"+cardTypeSecond;
			// TO MAKE IT GENERIC according to card type
			return cardTypeListSecond;
			
		}
	
	
	//Step 3 parameters 
	 public static String supplementaryCard(String html){
			//TO BE MADE generic for multiple cards
			String productNameText = productNameText(html);
		    String cardType = generateProductNameText(productNameText);	
			String supplementaryCard = productName(html)+"-"+cardType;
			return supplementaryCard;
			
		}
		
	 
		public static String supplementaryCardSecond(String html){
			String productNameTextSecond = productNameTextSecond(html);
		    String cardTypeSecond = generateProductNameText(productNameTextSecond);
			String supplementaryCard = productNameSecond(html)+"-"+cardTypeSecond;
			return supplementaryCard;			
		}
		
		
		//Step 3 parameters 
		 public static String supplementaryCardText(String html){
				//TO BE MADE generic for multiple cards
				String productNameText = productNameText(html);
			    String cardType = generateProductNameText(productNameText);				
				return cardType;
				
			}
			
		 
			public static String supplementaryCardTextSecond(String html){
				String productNameTextSecond = productNameTextSecond(html);
			    String cardTypeSecond = generateProductNameText(productNameTextSecond);
				return cardTypeSecond;
				
			}
		
		
		
		
	/*//Step 3 parameters 
	public static String supplementaryCardWithAtleastOne(String html){
		String supplementaryCard = productName(html).concat("-GVP");
		// TODO :TO MAKE IT GENERIC according to card type when at least 1 supplementary card is added.
		return supplementaryCard;
		
	}*/
	//Step 3 parameters 
	public static String extractUrlSupplmentaryDetails(String html) {
		// TODO : formName will be moved to properties file
				String formName = "viewns".concat("_").concat("7").concat("_").concat("I9KAGO01G8R250IQP27VCE2GE7").concat("_").concat(":").concat("captureSupplementCardholderDetails");
				String url = hostUrl.concat(extractUrl(html,formName));
				return url;
				}
							
			//Step 3 parameters 
	public static String edsTokenSupplementaryDetails(String html){
		// TODO : nameValue will be moved to properties file
				String nameValue =  "ESDS_TOKEN-supplementary_CardHolderPage1:";				
				return extractEdsToken(html,nameValue);	
				}
						
	//To set dynamic params for step 3 from response step 2
	public static void extractDynamicParamsStep3(String page,RequestParameters params){		
		params.setCbaProductTrackerIDListString(productListTracker(page));
		params.setProductList1(productList1(page));
		params.setEdsToken(edsTokenSupplementaryDetails(page));
		params.setUrl(extractUrlSupplmentaryDetails(page));	
		params.setCardTypeList(cardTypeList(page,params));
	}

	
	//To set dynamic params for step 3 from response step 2 for dual card
		public static void extractDynamicParamsDualCardStep3(String page,RequestParameters params){		
			params.setCbaProductTrackerIDListString(productListTracker(page));
			params.setProductList1(productList1(page));
			params.setProductList1Second(productList1Second(page));
			params.setEdsToken(edsTokenSupplementaryDetails(page));
			params.setUrl(extractUrlSupplmentaryDetails(page));	
			params.setCardTypeList(cardTypeList(page,params));
			params.setCardTypeListSecond(cardTypeListSecond(page,params));
			params.setSingleCardForSupplementary(supplementaryCardText(page));
			params.setSingleCardSecondForSupplementary(supplementaryCardTextSecond(page));
		}
	
	
	//Step 4 parameters in response of redirection call
	 public static String extractUrlReviewDetails(String html) {	
			// TODO : formName will be moved to properties file
			String formName = "viewns".concat("_").concat("7").concat("_").concat("I9KAGO01G8R250IQP27VCE2GE7").concat("_").concat(":").concat("reviewdeatils");
			String url = hostUrl.concat(extractUrl(html,formName));
			return url;
			}
			
			//Step 4 parameters in response of redirection call
	public static String edsTokenReviewDetails(String html){
		// TODO : nameValue will be moved to properties file
				String nameValue =  "ESDS_TOKEN-reviewApplicationDetailsPage1:";				
				return extractEdsToken(html,nameValue);	
			}
	
	//To set dynamic params for step 4 from response step 3
	public static void extractDynamicParamsStep4(String page,RequestParameters params){		
			params.setEdsToken(edsTokenReviewDetails(page));
			params.setUrl(extractUrlReviewDetails(page));				
			params.setProductName(productName(page));
			params.setSupplementaryCard(supplementaryCard(page));
		}

	
	//To set dynamic params for step 4 from response step 3 for dual card
		public static void extractDynamicParamsStep4DualCard(String page,RequestParameters params){		
				params.setEdsToken(edsTokenReviewDetails(page));
				params.setUrl(extractUrlReviewDetails(page));				
				params.setProductName(productName(page));
				params.setSupplementaryCard(supplementaryCard(page));
				params.setProductNameSecond(productNameSecond(page));
				params.setSupplementaryCardSecond(supplementaryCardSecond(page));
			}

	
	
	 public static String extractUrlDecisionDetails(String html) {		
			String formName = "viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product";
			String url = hostUrl.concat(extractUrl(html,formName));
			return url;
			}
	
	
	 public static boolean extractTextForDeclinedAccount(String html) {				
			String formName = "viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product";
			Document doc = Jsoup.parse(html);
			Element javaScriptPage = doc.getElementById(formName);
			String textInFormToBeMatched =  javaScriptPage.text();
			boolean value = textInFormToBeMatched.contains("account is declined.");
			return value;
			}
	
	 
	 
/*
	//Step 5 parameters 
	 public static String extractUrlDecisionDetails(String html) {	
		// TODO : formName will be moved to properties file.
				String formName = "viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product";
				String url = hostUrl.concat(extractUrl(html,formName));
				return url;
				}*/
				
	//Step 5 parameters 
	public static String edsTokenDecisionDetails(String html){
		// TODO : formName will be moved to properties file.
				String nameValue =  "ESDS_TOKEN-configureProductPage1:";				
				return extractEdsToken(html,nameValue);	
				}
	
	//Step 5 parameters 
	public static String dispatchMethodName(String html) {
				Document doc = Jsoup.parse(html);
				// TODO : "value" and "EMAIL" will be moved to properties file and constants
				Elements dispatchMethodName = doc.getElementsByAttributeValue("value", "EMAIL");
				String dispatchMethod = dispatchMethodName.attr("name");
				return dispatchMethod;
				}
	
	
	//
	//To set dynamic params for step 5 card1 from response dual card step 5 both cards
		public static void extractDynamicParamsStep5Card1(String page,RequestParameters params){					
			params.setEdsToken(edsTokenStep5Card1(page));
			params.setUrl(extractUrlDecisionDetails(page));	
			params.setFirstCardForBalanceTransfer(cardForBalanceTransfer(page));
			params.setDispatchMethod(dispatchMethodName(page));
		}
	
		
		//To set dynamic params for step 5 card2 from response dual card step 5 both cards
				public static void extractDynamicParamsStep5Card2(String page,RequestParameters params){					
					params.setEdsToken(edsTokenStep5Card1(page));
					params.setUrl(extractUrlDecisionDetails(page));	
					params.setSecondCardForBalanceTransfer(cardForBalanceTransfer(page));
					params.setDispatchMethod(dispatchMethodName(page));
				}
		/*public static String extractUrlStep5Card1(String html) {	
			// TODO : formName will be moved to properties file
					String formName = "viewns_7_I9KAGO01G8R250IQP27VCE2GU6_:configure_product";
					String url = hostUrl.concat(extractUrl(html,formName));
					return url;
				}*/
		public static String edsTokenStep5Card1(String html){
			// TODO : nameValue will be moved to properties file
					String nameValue =  "ESDS_TOKEN-captureEmploymentAdditionalDtlPage1:";				
					return extractEdsToken(html,nameValue);	
				}
	
		public static String cardForBalanceTransfer(String html) {
			Document doc = Jsoup.parse(html);
			
			Elements cardInputParams = doc.getElementsByAttributeValue("name", "PRODOPTION_INPUT_PARAMS");
			String  cardInputParamsValue = cardInputParams.attr("value");
			int beginIndex = 37;
			int endIndex = 40;
			String cardKeySelected = cardInputParamsValue.substring(beginIndex, endIndex);
			/*if(cardKey.equals("VP1")){
				cardKeySelected=cardKey;
			}*/		
			log.debug("First Card for balance transfer"+cardKeySelected);
			return cardKeySelected;
			}

		
	//Step 5 parameters 		
	public static void extractDynamicParamsStep5(String page,RequestParameters params){		
		params.setEdsToken(edsTokenDecisionDetails(page));
		params.setUrl(extractUrlDecisionDetails(page));
		params.setDispatchMethod(dispatchMethodName(page));
		params.setDispatchMethodDisableCheck(dispatchMethodDisableCheck(page));
		params.setCreditCardDeclineCheck(extractTextForDeclinedAccount(page));		
	}

	
	//Step 6 Extract application reference number
		public static String extractApplicationIdFromGad1Page(String html) {
			Document doc = Jsoup.parse(html);
			Element applicationRef = doc.getElementById("ao-gad-add-ForEmail");
			return applicationRef.attr("value");
			
			// TODO : "value" and "EMAIL" will be moved to properties file
			/*Document doc = Jsoup.parse(html);
			Element applicationRef = doc.getElementById("ao-gad-add-ForEmail");*/
			//Element applicationId = (Element) applicationRef.parent().parent().nextSibling().childNode(0); 
			//return applicationRef.text();
		}
	
	
	//Step 6 Extract application reference number
	public static String extractApplicationIdFromCompletePage(String html) {
		// TODO : "value" and "EMAIL" will be moved to properties file
		Document doc = Jsoup.parse(html);
		Element applicationRef = doc.getElementById("viewns_7_I9KAGO01G8R250IQP27VCE2GU7_:applicationConfirmationTermDeposit:ao-gad-cnd-datalist4:0:_idJsp25ns_7_I9KAGO01G8R250IQP27VCE2GU7_:application_confirmation:_idJsp26ns_7_I9KAGO01G8R250IQP27VCE2GU7_");
		Element applicationId = (Element) applicationRef.parent().parent().nextSibling().childNode(0); 
		return applicationId.text();
	}
	
	
    public static String aoRefNumber(RequestParameters params) {
        String aoRefNumberGeneratedFromTPSA = params.getAoApplicationReferenceNumber();
        return (StringUtils.isNotEmpty(aoRefNumberGeneratedFromTPSA) ? aoRefNumberGeneratedFromTPSA : StringUtils.EMPTY);
    }
	
	//denied application - dual card 
	
	public static boolean applicationDeniedStatus(String html) {
		// TODO : "value" and "EMAIL" will be moved to properties file
		Document doc = Jsoup.parse(html);
		try{
			Element productStatus = doc.getElementById("viewns_7_I9KAGO01G8R250IQP27VCE2GU7_:applicationConfirmationTermDeposit:ao-gad-cnd-datalist4:0:_idJsp35ns_7_I9KAGO01G8R250IQP27VCE2GU7_:application_details:_idJsp39ns_7_I9KAGO01G8R250IQP27VCE2GU7_");
			Element applicationDeniedStatus = (Element) productStatus.parent().parent().nextSibling().childNode(0);
			String deniedCase = applicationDeniedStatus.text();
			return true;
		}
		catch(NullPointerException e){
			return false;
		}
	}
	
	public static boolean  dispatchMethodDisableCheck (String html) {
		// TODO : "value" and "EMAIL" will be moved to properties file
		Document doc = Jsoup.parse(html);
		try{
			Element dispatchMethodDisable = doc.getElementById("ao-tnc-vtnc-accept");
			String dispatchMethodDisableStatus = dispatchMethodDisable.attr("disabled");
			boolean disabledCase = dispatchMethodDisableStatus.equals("disabled");			
			return disabledCase;
		}
		catch(NullPointerException e){
			return false;
		}
	}
	
	public static String generateProductNameText(String productCodeString){
		StringBuilder productNameText = new StringBuilder();
		
		String[] tempArr = productCodeString.split("-");
		String productCode = tempArr[tempArr.length-1].trim();
		productNameText.append(CBACardTypeMap.get(productCode));
		
		return productNameText.toString();
	}
	
	
	
	
	public static String aoUrl(RequestParameters params){
		String aoUrl = PropertiesCache.getInstance().getProperty("launch.tpsa.url");
		aoUrl = aoUrl.replace("<CHANNEL_FLAG>", params.getChannelFlag());
		aoUrl = aoUrl.replace("<PROMO_CODE>", params.getPromoCode());
		return aoUrl;
	}
	
}