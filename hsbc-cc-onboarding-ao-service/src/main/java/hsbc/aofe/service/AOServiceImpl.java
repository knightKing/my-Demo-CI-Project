package hsbc.aofe.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hsbc.aofe.ao.contract.AOService;
import hsbc.aofe.ao.process.AoProcess;
import hsbc.aofe.domain.Address;
import hsbc.aofe.domain.AddressType;
import hsbc.aofe.domain.Applicant;
import hsbc.aofe.domain.ApplicantAdditionalInfo;
import hsbc.aofe.domain.ApplicantDeclarations;
import hsbc.aofe.domain.ApplicantType;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.ApplicationStaffDetails;
import hsbc.aofe.domain.BeneficiaryDetails;
import hsbc.aofe.domain.CardDeclaration;
import hsbc.aofe.domain.CountryCode;
import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.domain.Document;
import hsbc.aofe.domain.DocumentGroup;
import hsbc.aofe.domain.DocumentName;
import hsbc.aofe.domain.EducationalLevel;
import hsbc.aofe.domain.EmployementStatus;
import hsbc.aofe.domain.GhoClassification;
import hsbc.aofe.domain.HomeOwnership;
import hsbc.aofe.domain.Image;
import hsbc.aofe.domain.MaritalStatus;
import hsbc.aofe.domain.MarketSectorCode;
import hsbc.aofe.domain.Name;
import hsbc.aofe.domain.NatureOfBusiness;
import hsbc.aofe.domain.NumberOfDependents;
import hsbc.aofe.domain.SourceCode;
import hsbc.aofe.domain.SpecialInstructionIndicator;
import hsbc.aofe.domain.Title;
import hsbc.aofe.repository.common.ApplicationRepositoryService;

@Slf4j
@Service
public class AOServiceImpl implements AOService {


    private ApplicationRepositoryService applicationRepositoryService;

    @Autowired
    public AOServiceImpl(ApplicationRepositoryService applicationRepositoryService) {
        this.applicationRepositoryService = applicationRepositoryService;
    }


    @Override
    public String initiateAoSubmission(String arn) {
        String aoReferenceNumber = "";
        Application application = applicationRepositoryService.findApplicationByARN(arn);
        log.debug("AO: Application Object received: {}", application);

        //Application application = getDummyApplication();
        AoProcess aoProcess = new AoProcess();
        if ((application.getPrimaryApplicant().getCards().size() > 1)) {
            aoReferenceNumber = aoProcess.processDualCard(application);
        } else {
            aoReferenceNumber = aoProcess.processSingleCard(application);
        }
        applicationRepositoryService.updateApplicationWithAoRefNumber(aoReferenceNumber, arn);
        return aoReferenceNumber;
    }



	/*private Application getDummyApplication(){
        Application app = new Application();
		CreditCard cc1 = new CreditCard();
		List<CreditCard> cards = new ArrayList<CreditCard>();
		CardDeclaration cardDeclaration = new CardDeclaration();
		ApplicationStaffDetails staffDetails = new ApplicationStaffDetails();
		//cc1.setName("HSBC Visa Platinum Credit Card");
		//cc1.setType("TRAVEL CARD");
		//cc1.setAnnualFee(500L);
		cc1.setAnnualFeeCurrency("SGD");
		cardDeclaration.setBankToAssignCreditLimit(false);
		//cc1.setComplimentaryStuff("Sample complimentary stuff");
		//cc1.setImageURL("http://imageurl");
		cc1.setKey("VPC");
		cc1.setLimit(3000L);
		//cc1.setLimitMinimumOfInfo(500L);
		//cc1.setMinSalary(500L);
		//cc1.setRewardPoints(5L);
		//cc1.setRewardPointScale(2L);
		//cc1.setCardDeclaration(cardDeclaration);
		cards.add(cc1);
		staffDetails.setVoucherCode("PLA2");
		app.setStaffDetails(staffDetails);

		return app;

	}*/

    private Application getDummyApplication() {
        CreditCard cc1 = new CreditCard();
        CreditCard cc2 = new CreditCard();
        List<CreditCard> cards = new ArrayList<CreditCard>();
        CardDeclaration cardDeclaration = new CardDeclaration();
        cardDeclaration.setBankToAssignCreditLimit(false);

        BeneficiaryDetails bd = new BeneficiaryDetails();
        bd.setCcKey("VPC");
        //  bd.setAmountToBeTransferred(500L); TODO
        bd.setBeneficiaryAccountName("beneficiaryAccountName");
        //   bd.setBeneficiaryAccountNumber("9876543210"); TODO
        //bd.setBeneficiaryBank("beneficiaryBank"); TODO: Prachi update according to latest changes


        cc1.setName("HSBC Visa Platinum Credit Card");
        cc1.setType("TRAVEL CARD");
        cc1.setAnnualFee(500L);
        cc1.setAnnualFeeCurrency("SGD");
        //cc1.setBankToAssignCreditLimit(false);
        cc1.setComplimentaryStuff("Sample complimentary stuff");
        cc1.setImageURL("http://imageurl");
        cc1.setKey("VPC");
        cc1.setLimit(3000L);
        cc1.setLimitMinimumOfInfo(500L);
        cc1.setMinSalary(500L);
        cc1.setRewardPoints(5L);
        cc1.setRewardPointScale(2L);
        cc1.setCardDeclaration(cardDeclaration);

        cc2.setName("HSBC Visa Infinite Credit Card");
        cc2.setType("TRAVEL CARD");
        cc2.setAnnualFee(500L);
        cc2.setAnnualFeeCurrency("SGD");
        //cc2.setBankToAssignCreditLimit(false);
        cc2.setComplimentaryStuff("Sample complimentary stuff");
        cc2.setImageURL("http://imageurl");
        cc2.setKey("VPI");
        cc2.setLimit(99999999L);
        cc2.setLimitMinimumOfInfo(500L);
        cc2.setMinSalary(500L);
        cc2.setRewardPoints(5L);
        cc2.setRewardPointScale(2L);

        cards.add(cc1);
        //cards.add(cc2);

        Applicant applicant = new Applicant();
        Applicant supplimentaryApplicant1 = new Applicant();
        Applicant supplimentaryApplicant2 = new Applicant();
        Applicant supplimentaryApplicant3 = new Applicant();
        List<Applicant> supplementaryApplicants = new ArrayList<Applicant>();
        ApplicantAdditionalInfo addInfo = new ApplicantAdditionalInfo();
        List<Address> addresses = new ArrayList<Address>();
        Address residentialAddress = new Address();
        Address permanentAddress = new Address();
        Address previousAddress = new Address();
        Address officeAddress = new Address();
        List<Image> images = new ArrayList<Image>();
        Image image1 = new Image();
        Image image2 = new Image();
        List<Document> documents = new ArrayList<Document>();
        Document nricDoc = new Document();
        Application app = new Application();
        app.getPrimaryApplicant().getAdditionalInfo().setVerifiedIncome(30000);
        app.setDocCompletenessIndicator(true);
        app.setEmploymentCheckIndicator(true);
        app.setIncomeAutomatedCalcIndicator(true);
        residentialAddress.setAddressType(AddressType.RESIDENTIAL);
        residentialAddress.setCity("Delhi");
        residentialAddress.setCountry(CountryCode.SG);
        residentialAddress.setId(0);
        residentialAddress.setLine1Address("L 123");
        residentialAddress.setLine2Address("Lake Street");
        residentialAddress.setLine3Address(null);
        residentialAddress.setPostalCode("110019");

        addresses.add(residentialAddress);

        permanentAddress.setAddressType(AddressType.PERMANENT);
        permanentAddress.setCity("Delhi"); // L3 city
        permanentAddress.setCountry(CountryCode.SG);
        permanentAddress.setId(0);
        permanentAddress.setLine1Address("L 123");
        permanentAddress.setLine2Address("Lake Street");
        permanentAddress.setLine3Address(null);
        permanentAddress.setPostalCode("110019");

        addresses.add(permanentAddress);


        previousAddress.setAddressType(AddressType.PREVIOUS);
        previousAddress.setCity(null);
        previousAddress.setCountry(CountryCode.SG);
        previousAddress.setId(0);
        previousAddress.setLine1Address(null);
        previousAddress.setLine2Address(null);
        previousAddress.setLine3Address(null);
        //previousAddress.setPostalCode();
        //Long longValue = null;
        //previousAddress.setPostalCode();

        addresses.add(previousAddress);


        officeAddress.setAddressType(AddressType.OFFICE);
        officeAddress.setCity("Singapore");
        officeAddress.setCountry(CountryCode.SG);
        officeAddress.setId(0);
        officeAddress.setLine1Address("462");
        officeAddress.setLine2Address("Carnland");
        officeAddress.setLine3Address(null);
        officeAddress.setPostalCode("110019");

        //officeAddress.setPostalCode(1L);

        addresses.add(officeAddress);

        image1.setImage(null);
        image1.setIndex(1);
        image1.setName("Whatsapp image");
        image2.setImage(null);
        image2.setIndex(2);
        image2.setName("Whatsapp image");
        images.add(image1);
        images.add(image2);

        nricDoc.setImages(images);
        nricDoc.setDocGroup(DocumentGroup.IDENTITY);
        nricDoc.setDocName(DocumentName.NRIC);
        nricDoc.setNumberOfImages(2);
        documents.add(nricDoc);
        addInfo.setEmploymentType(EmployementStatus.EMPOYED_FULL_TIME);
        addInfo.setAddress(addresses);
        addInfo.setAnnualIncome(50000L);
        addInfo.setAssociateOfPublicPositionHolder(false);
        //addInfo.setAssociateOfPublicPositionHolderDetails();
        addInfo.setAutoDebitFromAccountNumber("1234567890");
        addInfo.setCompanyName("NewGen");
        addInfo.setCorrespondenceOn("Residence");
        addInfo.setCountryOfBirth(CountryCode.SG);
        addInfo.setCountryOfIdIssue(CountryCode.SG);
        addInfo.setEducationLevel(EducationalLevel.SECONDARY);
        addInfo.setEmploymentType(EmployementStatus.EMPOYED_FULL_TIME);
        addInfo.setHoldingPublicPosition(false);
        addInfo.setHomeOwnership(HomeOwnership.FULLYOWNED);
        addInfo.setHomePhone(null);
        addInfo.setIdExpiryDate(null);
        addInfo.setIdIssueDate(null);
        addInfo.setIndustryType(NatureOfBusiness.ADVERTISING);
        addInfo.setIntroducedByHSBCCardHolder(false);
        //addInfo.setIntroducedByHSBCCardHolderReferrerID();
        addInfo.setMultipleNationality(false);
        addInfo.setLengthOfService(63L);
        addInfo.setMaritalStatus(MaritalStatus.SINGLE);
        ///addInfo.setMonthsToEmpPassExpiry(45L);
        addInfo.setMothersMaidenName("Jeswani");
        addInfo.setNameOnCard("Prachi Bhambani");
        addInfo.setNationality(CountryCode.SG);
        addInfo.setNationality2(null);
        addInfo.setNationality3(null);
        //Occupation needs to made with Enum
        //addInfo.setOccupation("09");
        //addInfo.setOfficeExtn(25456L);
        //addInfo.setOfficePhone();
        addInfo.setOfficePostalCode(15345454654L);

        //addInfo.setOtherName("othername");
        addInfo.setPosition("Position");
        //addInfo.setPublicPositionDetails("public position details");
        //addInfo.setRelationshipWithPrimary(RelationshipWithPrimary.PARENT);
        addInfo.setResidentialAddIdenticalToPermAddress(true);
        addInfo.setTaxResidenceCountry1(CountryCode.SG);
        addInfo.setTaxResidenceCountry2(null);
        addInfo.setTaxResidenceCountry3(null);
        //addInfo.setTimeAtPreviousEmployer(45L);
        addInfo.setNoOfDependents(NumberOfDependents.ONE);
        addInfo.setMonthsAtResidentialAddress(60L);
        //addInfo.setNoOfDependents(NumberOfDependents.ONE);
        //addInfo.setMonthsToEmpPassExpiry(17l);


        ApplicantDeclarations applicantDeclarations = new ApplicantDeclarations();
        applicantDeclarations.setConsentPersonalData(true);
        applicantDeclarations.setConsentMarketing(true);
        applicantDeclarations.setConsentContactMe(true);
        applicantDeclarations.setConsentAccountOpening(true);
        applicantDeclarations.setSharedCreditLimit(true);
        applicantDeclarations.setBalanceTransferOnCreditCard(true);

        applicant.setAdditionalInfo(addInfo);
        applicant.setApplicantDeclarations(applicantDeclarations);
        applicant.setDateOfBirth("21/08/1993");
        applicant.setCards(cards);
        applicant.setDocuments(documents);
        applicant.setEmail("prachi@gmail.com");
        applicant.setGender("Female");
        applicant.setId(0L);
        //applicant.setIdDocType("NRIC");
        applicant.setIdValue("S8049913B");
        applicant.setMobile(9630258741L);
        applicant.setName(new Name("Prachi", "", "Bhambani"));
        applicant.setSalutation(Title.MISS);
        applicant.setType(ApplicantType.PRIMARY);
        applicant.setBeneficiaryDetails(bd);

        supplimentaryApplicant1.setDateOfBirth(null);
        supplimentaryApplicant1.setCards(cards);
        supplimentaryApplicant1.setEmail("sunita@email.com");
        supplimentaryApplicant1.setGender("Female");
        supplimentaryApplicant1.setId(0L);
        //supplimentaryApplicant1.setIdDocType("NRIC");
        supplimentaryApplicant1.setIdValue("ABCDEFG");
        supplimentaryApplicant1.setMobile(9999999999L);
        supplimentaryApplicant1.setName(new Name("Sunita", "", "Bhambani"));
        supplimentaryApplicant1.setSalutation(Title.MISS);
        supplimentaryApplicant1.setType(ApplicantType.SUPPLEMENTARY);
        //supplimentaryApplicant1.


        supplimentaryApplicant2.setAdditionalInfo(addInfo);
        supplimentaryApplicant2.setDateOfBirth(null);
        supplimentaryApplicant2.setCards(cards);
        supplimentaryApplicant2.setEmail("email@email.com");
        supplimentaryApplicant2.setGender("Male");
        supplimentaryApplicant2.setId(0L);
        //supplimentaryApplicant2.setIdDocType("NRIC");
        supplimentaryApplicant2.setIdValue("ABCDEFG");
        supplimentaryApplicant2.setMobile(9999999999L);
        supplimentaryApplicant2.setName(new Name("Anoop", "Singh", "Khimani"));
        //supplimentaryApplicant2.setSalutation();
        supplimentaryApplicant2.setType(ApplicantType.SUPPLEMENTARY);

        supplimentaryApplicant3.setAdditionalInfo(addInfo);
        supplimentaryApplicant3.setDateOfBirth(null);
        supplimentaryApplicant3.setCards(cards);
        supplimentaryApplicant3.setEmail("email@email.com");
        supplimentaryApplicant3.setGender("Male");
        supplimentaryApplicant3.setId(0L);
        //supplimentaryApplicant3.setIdDocType("NRIC");
        supplimentaryApplicant3.setIdValue("ABCDEFG");
        supplimentaryApplicant3.setMobile(9999999999L);
        supplimentaryApplicant3.setName(new Name("Anoop", "Singh", "Khimani"));
        //supplimentaryApplicant3.setSalutation("Mr.");
        supplimentaryApplicant3.setType(ApplicantType.SUPPLEMENTARY);

        //supplementaryApplicants.add(supplimentaryApplicant1);
        //supplementaryApplicants.add(supplimentaryApplicant2);
        //supplementaryApplicants.add(supplimentaryApplicant3);


        ApplicationStaffDetails staffDetails = new ApplicationStaffDetails();

        //staffDetails.setApprovedBy(approvedBy);
        staffDetails.setGhoCode(GhoClassification.PERSONAL);
        staffDetails.setMarketSectorCode(MarketSectorCode.PERSONAL_MASS_MARKET_THOSE_NOT_DEFINED);
        //staffDetails.setNameOfStaff(nameOfStaff);
        //staffDetails.setReferralId();
        staffDetails.setSourceCode(SourceCode._0A);
        //staffDetails.setCreditCardNumber(creditCardNumber);
        //staffDetails.setRemarks(remarks);
        staffDetails.setSplInstIndicator(SpecialInstructionIndicator.OTHERS);
        staffDetails.setVoucherCode("PLA2");

        app.setStaffDetails(staffDetails);
        app.setAddSupplimentaryCard(false);
        app.setAgreeToGenericConsent(true);
        app.setAllocateExistingLimitToNew(true);
        app.setArn("ABCDE");
        app.setAtmLinkedAcNumber("1234567890");
        app.setCreateAutoDebitFromAccount(false);
        app.setExistingCardNumber("1234567896541236");
        app.setHowMuchAutoDebit(500L);
        app.setPrimaryApplicant(applicant);
        app.setReceiveEStatement(false);
        app.setSupplementaryApplicant(supplementaryApplicants);
        app.setTransactingOnMyOwn(false);
        return app;
    }
}
