package hsbc.aofe.ocr.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "imageExtraction")
public class ImageExtractionConfiguration {

	boolean webService;
	String serviceUrl;
	String nricServiceUrl;
}
