package hsbc.aofe.ocr.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import hsbc.aofe.ocr.strategy.ImageExtraction;
import hsbc.aofe.ocr.strategy.ImageExtractor;
import hsbc.aofe.ocr.strategy.PDFExtractor;

@Configuration
public class OcrConfiguration {

	
	@Bean
	@Qualifier("pdfExtractor")
	public PDFExtractor<String> pdfExtractor(){
		PDFExtractor<String> pdfExtractor=new PDFExtractor<>();		
		return pdfExtractor;		
	}
	
	@Bean
	@Qualifier("imageExtractor")
	public ImageExtractor<Object> imageExtractor(){
		ImageExtractor<Object> imageExtractor=new ImageExtractor<>();		
		return imageExtractor;		
	}
	
	@Bean
	@Qualifier("imageExtraction")
	public ImageExtraction<Object> imageExtraction(){
		ImageExtraction<Object> imageExtraction=new ImageExtraction<>();		
		return imageExtraction;		
	}
	
	
	
	
}
