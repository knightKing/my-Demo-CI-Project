package hsbc.aofe.ocr.extractor;

import java.util.Optional;

public interface OcrExtractor {

	public <T> Optional<T> extractFrom(byte[] ocrdImage , String docType, Class<T> cls);
	
	public <T> Optional<T> extractImages(byte[] frontImage , byte[] backImage ,String docType, Class<T> cls);

}
