package hsbc.aofe.ocr.extractor;

import hsbc.aofe.ocr.strategy.Extractor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
@Slf4j
@Service
public class OcrExtractorImpl implements OcrExtractor {

	@Autowired
	public OcrExtractorImpl(@Qualifier("pdfExtractor") Extractor pdfExtractor,
			@Qualifier("imageExtractor") Extractor<Object> imageExtractor,
			@Qualifier("imageExtraction") Extractor imageExtraction ) {
		Map<String, Extractor> map = new HashMap<>();
		map.put("ECPF", pdfExtractor);
		map.put("EMPLOYMENT_PASS", imageExtraction);
		map.put("NRIC",imageExtraction);
		vitalInfoMap = Collections.unmodifiableMap(map);
	}

	private final Map<String, Extractor> vitalInfoMap;

	@SuppressWarnings("unchecked")
	@Override
	public <T> Optional<T> extractFrom(byte[] ocrdImage, String docType, Class<T> cls) {
		log.debug("In OcrExtractorImpl here Strategy is selected the strategy is selected for docType" +docType);
		log.error("In OcrExtractorImpl here Strategy is selected the strategy is selected for docType" +docType);
		return (Optional<T>) Optional.ofNullable(vitalInfoMap.get(docType).extract(ocrdImage, docType));
	}

	@Override
	public <T> Optional<T> extractImages(byte[] frontImage, byte[] backImage, String docType, Class<T> cls) {
		log.debug("In OcrExtractorImpl here Strategy is selected the strategy is selected for docType" +docType);
		log.error("In OcrExtractorImpl here Strategy is selected the strategy is selected for docType" +docType);
		return (Optional<T>) Optional.ofNullable(vitalInfoMap.get(docType).extractImage(frontImage, backImage,docType));
	}

}
