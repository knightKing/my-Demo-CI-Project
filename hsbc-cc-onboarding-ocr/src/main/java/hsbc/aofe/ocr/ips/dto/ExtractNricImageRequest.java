package hsbc.aofe.ocr.ips.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Images" })
public class ExtractNricImageRequest implements Serializable {

	@JsonProperty("Images")
	@Valid
	private List<NricImage> nricImages = null;
	private static final long serialVersionUID = 1L;

	@JsonProperty("Images")
	public List<NricImage> getImages() {
		return nricImages;
	}

	@JsonProperty("Images")
	public void setImages(List<NricImage> nricImages) {
		this.nricImages = nricImages;
	}

}
