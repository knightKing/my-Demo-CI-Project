package hsbc.aofe.ocr.ips.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hsbc.aofe.domain.Item;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "IsSuccess",
    "DocumentType",
    "Error",
    "Items"
})
public class LeadToolsExtractedData implements Serializable
{

    @JsonProperty("IsSuccess")
    private Boolean isSuccess;
    @JsonProperty("DocumentType")
    private String documentType;
    @JsonProperty("Error")
    private Object error;
    @JsonProperty("Items")
    @Valid
    private List<Item> items = null;
    private final static long serialVersionUID = 9134547417830845408L;

    @JsonProperty("IsSuccess")
    public Boolean getIsSuccess() {
        return isSuccess;
    }

    @JsonProperty("IsSuccess")
    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    @JsonProperty("DocumentType")
    public String getDocumentType() {
        return documentType;
    }

    @JsonProperty("DocumentType")
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @JsonProperty("Error")
    public Object getError() {
        return error;
    }

    @JsonProperty("Error")
    public void setError(Object error) {
        this.error = error;
    }

    @JsonProperty("Items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("Items")
    public void setItems(List<Item> items) {
        this.items = items;
    }
}
