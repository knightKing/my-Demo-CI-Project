package hsbc.aofe.ocr.ips.dto;

import lombok.Data;

@Data
public class LeadToolsRequestDomain {

	String document;
	String documentType;
}
