package hsbc.aofe.ocr.ips.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Image", "ImageType" })
public class NricImage implements Serializable {

	@JsonProperty("Image")
	private String image;
	@JsonProperty("ImageType")
	private Integer imageType;
	private static final long serialVersionUID = 1L;
	
	
	 @JsonProperty("Image")
	    public String getImage() {
	        return image;
	    }

	    @JsonProperty("Image")
	    public void setImage(String image) {
	        this.image = image;
	    }

	    @JsonProperty("ImageType")
	    public Integer getImageType() {
	        return imageType;
	    }

	    @JsonProperty("ImageType")
	    public void setImageType(Integer imageType) {
	        this.imageType = imageType;
	    }
}
