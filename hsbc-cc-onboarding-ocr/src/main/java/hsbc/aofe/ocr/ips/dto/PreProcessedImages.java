package hsbc.aofe.ocr.ips.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "OutputImage",
    "Error",
    "ErrorCode"
})
public class PreProcessedImages implements Serializable
{

    @JsonProperty("OutputImage")
    private String outputImage;
    @JsonProperty("Error")
    private String error;
    @JsonProperty("ErrorCode")
    private Integer errorCode;
    private final static long serialVersionUID = -5877818914385753609L;

    @JsonProperty("OutputImage")
    public String getOutputImage() {
        return outputImage;
    }

    @JsonProperty("OutputImage")
    public void setOutputImage(String outputImage) {
        this.outputImage = outputImage;
    }

    @JsonProperty("Error")
    public String getError() {
        return error;
    }

    @JsonProperty("Error")
    public void setError(String error) {
        this.error = error;
    }

    @JsonProperty("ErrorCode")
    public Integer getErrorCode() {
        return errorCode;
    }

    @JsonProperty("ErrorCode")
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
}
