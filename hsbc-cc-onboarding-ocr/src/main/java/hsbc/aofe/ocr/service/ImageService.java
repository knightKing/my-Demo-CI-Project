package hsbc.aofe.ocr.service;

import hsbc.aofe.ocr.ips.dto.LeadToolsExtractedData;
import hsbc.aofe.ocr.ips.dto.PreProcessedImages;

public interface ImageService {

	LeadToolsExtractedData extractImageData(byte[] image,String documentType);
	
	PreProcessedImages extractNricImage(byte[] imageFront,byte[] imageBack);
}
