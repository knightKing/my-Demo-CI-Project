package hsbc.aofe.ocr.service;

import hsbc.aofe.ocr.config.ImageExtractionConfiguration;
import hsbc.aofe.ocr.ips.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageExtractionConfiguration imageExtractionConfiguration;

    private RestTemplate restTemplate;
    private DozerBeanMapper dozzerBeanMapper = new DozerBeanMapper();

    public ImageServiceImpl() {
        restTemplate = new RestTemplate();
    }

    @Override
    public LeadToolsExtractedData extractImageData(byte[] image, String documentType) {
        if (imageExtractionConfiguration.isWebService()) {
            log.debug("Leadtools activated");
            String uri = imageExtractionConfiguration.getServiceUrl();
            log.debug("Leadtools activated with uri " + uri);
            String encodeBase64 = new String(Base64.encodeBase64(image));
            log.debug("Leadtools activated with image byte array" + encodeBase64);
            LeadToolsRequestDomain imageDomain = new LeadToolsRequestDomain();
            imageDomain.setDocumentType(documentType);
            imageDomain.setDocument(encodeBase64);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<LeadToolsRequestDomain> request = new HttpEntity<>(imageDomain);

            ResponseEntity<LeadToolsExtractedData> response = restTemplate.exchange(uri, HttpMethod.POST, request,
                    LeadToolsExtractedData.class);
            log.debug("Leadtools response after hit is " + response);
            LeadToolsExtractedData leadToolsExtractedData = null;
            leadToolsExtractedData = dozzerBeanMapper.map(response.getBody(), LeadToolsExtractedData.class);
            log.debug("Leadtools extracted object is " + leadToolsExtractedData);
            if (leadToolsExtractedData != null) {
                return leadToolsExtractedData;
            }
            return new LeadToolsExtractedData();
        }
        log.debug("Leadtools deActivated");
        return null;

    }

    @Override
    public PreProcessedImages extractNricImage(byte[] imageFront, byte[] imageBack) {
        if (imageExtractionConfiguration.isWebService()) {
            log.debug("Nric Extraction starts");
            String nricServiceUrl = imageExtractionConfiguration.getNricServiceUrl();
            String frontImageBase64 = new String(Base64.encodeBase64(imageFront));
            String backImageBase64 = new String(Base64.encodeBase64(imageBack));
            NricImage nricFrontImage = new NricImage();
            nricFrontImage.setImage(frontImageBase64);
            nricFrontImage.setImageType(1);

            NricImage nricBackImage = new NricImage();
            nricBackImage.setImage(backImageBase64);
            nricBackImage.setImageType(2);

            List<NricImage> nricImages = new ArrayList<>();
            nricImages.add(nricFrontImage);
            nricImages.add(nricBackImage);
            ExtractNricImageRequest extractNricImageRequest = new ExtractNricImageRequest();
            extractNricImageRequest.setImages(nricImages);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<ExtractNricImageRequest> request = new HttpEntity<>(extractNricImageRequest);
            ResponseEntity<PreProcessedImages> responseEntity = restTemplate.exchange(nricServiceUrl, HttpMethod.POST, request,
                    PreProcessedImages.class);
            PreProcessedImages preProcessedImages = dozzerBeanMapper.map(responseEntity.getBody(), PreProcessedImages.class);
            return preProcessedImages;
        }
        return null;
    }


}
