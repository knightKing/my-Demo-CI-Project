package hsbc.aofe.ocr.strategy;

import hsbc.aofe.ocr.ips.dto.PreProcessedImages;
import hsbc.aofe.ocr.service.ImageService;
import hsbc.aofe.ocr.service.ImageServiceImpl;

public interface Extractor<T> {

	public T extract(byte[] ocrdImage,String documentType);

	default public Object extractImage(byte[] frontImage,byte[] backImage,String documentType){
		return null;		
	}
	
	
}
