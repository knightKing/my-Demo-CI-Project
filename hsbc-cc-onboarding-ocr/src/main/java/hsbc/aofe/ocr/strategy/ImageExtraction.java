package hsbc.aofe.ocr.strategy;

import com.google.common.collect.Multimap;
import hsbc.aofe.ocr.config.ImageExtractionConfiguration;
import hsbc.aofe.ocr.ips.dto.PreProcessedImages;
import hsbc.aofe.ocr.service.ImageService;
import hsbc.aofe.ocr.utility.ByteArrayCannotBeNullOrEmpty;
import hsbc.aofe.ocr.utility.ImageExtractionUtility;
import hsbc.aofe.ocr.utility.ParseOcrdImageResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.util.Map;

@Slf4j
@Component
public class ImageExtraction<T> implements Extractor {

    @Autowired
    ImageService imageServiceImpl;
    @Autowired
    ImageExtractionConfiguration imageExtractionConfiguration;

    static String base64 = null;

    @Override
    public Object extract(byte[] ocrdImage, String documentType) {
        return null;
    }

    public Object extractImage(byte[] frontImage, byte[] backImage, String documentType) {
        ParseOcrdImageResponse parseOcrdImageResponse = new ParseOcrdImageResponse();
        Multimap<String, Map<String, String>> imageTransform = null;
        if (imageExtractionConfiguration.isWebService()) {
                PreProcessedImages extractedNricImageData = imageServiceImpl.extractNricImage(frontImage, backImage);
                ImageExtraction<Object> imageExtraction = new ImageExtraction<>();
                Object extract = imageExtraction.extractData(extractedNricImageData.getOutputImage(), documentType);
                if (extract instanceof String) {
                    try {
                        imageTransform = parseOcrdImageResponse.imageTransform(extract.toString());
                    } catch (ParserConfigurationException | SAXException | IOException e) {
                        e.printStackTrace();
                    }
                }
                return imageTransform;
        }
        return null;
    }

    public Object extractData(String ocrdImage, String documentType) {
        String soapResponse = null;
        ImageExtractionUtility imageExtraction = new ImageExtractionUtility();
        try {
            soapResponse = imageExtraction.getSoapResponse(ocrdImage,documentType);
        } catch (SOAPException | ByteArrayCannotBeNullOrEmpty e) {
            e.printStackTrace();
        }
        return soapResponse;


    }

}
