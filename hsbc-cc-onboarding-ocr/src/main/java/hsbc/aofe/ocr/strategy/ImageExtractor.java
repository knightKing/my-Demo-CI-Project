package hsbc.aofe.ocr.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hsbc.aofe.ocr.ips.dto.LeadToolsExtractedData;
import hsbc.aofe.ocr.service.ImageService;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class ImageExtractor<LeadToolsExtractedData> implements Extractor {

	@Autowired
	ImageService imageService;

	@SuppressWarnings("unchecked")
	@Override
	public LeadToolsExtractedData extract(final byte[] ocrdImage,java.lang.String documentType) {
		log.debug("Inside the extraction function of ocr");
		return (LeadToolsExtractedData) imageService.extractImageData(ocrdImage, documentType);
		
	}


}
