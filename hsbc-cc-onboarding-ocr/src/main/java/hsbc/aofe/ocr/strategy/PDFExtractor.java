package hsbc.aofe.ocr.strategy;

import hsbc.aofe.ocr.utility.ByteArrayCannotBeNullOrEmpty;
import hsbc.aofe.ocr.utility.ParseSoap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.xml.soap.SOAPException;

@Slf4j
@Component
public class PDFExtractor<T> implements Extractor {

	@Override
	public Object extract(byte[] ocrdImage, java.lang.String documentType) {
		ParseSoap client = new ParseSoap();
		String xmlResponse = null;
		try {
			xmlResponse = client.getSoapResponse(ocrdImage);
		} catch (SOAPException | ByteArrayCannotBeNullOrEmpty e) {
			log.error("The exception in getting response from IPS is "+e);
			log.debug("The exception in getting response from IPS is "+e);
		}
		log.debug("The response from IPS for PDF Extraction is " + xmlResponse);
		log.error("The response from IPS for PDF Extraction is " + xmlResponse);
		return xmlResponse;
	}

}
