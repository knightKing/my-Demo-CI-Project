package hsbc.aofe.ocr.utility;

public class ByteArrayCannotBeNullOrEmpty extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ByteArrayCannotBeNullOrEmpty(String message) {
		super(message);
	}
	
}
