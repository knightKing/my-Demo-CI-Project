package hsbc.aofe.ocr.utility;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class ImageExtractionUtility {
    private static Properties properties;
    private static FileInputStream fileInputStream = null;

    /**
     * This function gets the soap response by consuming the soap web service of IPS.
     *
     * @param bytesArray this is input byte array of an image.
     * @return a soap message
     * @throws SOAPException
     * @throws ByteArrayCannotBeNullOrEmpty
     */
    public String getSoapResponse(String bytesArray, String docType) throws SOAPException, ByteArrayCannotBeNullOrEmpty {
        String xmlResposne = null;
        SOAPConnection soapConnection = null;
        SOAPMessage soapResponse = null;

        FileInputStream fileInputStream = null;

        try {
            Resource resource = new ClassPathResource("/ips.properties");
            properties = PropertiesLoaderUtils.loadProperties(resource);
            // Create SOAP Connection
            log.info("Creating SOAP Connection");
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = properties.getProperty("soap_server");    //"http://127.0.0.1:9100/Verification?wsdl";
            String hostname = properties.getProperty("hostname");
            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                    return hostname.equals(hostname);
                }
            });

            if (docType.equalsIgnoreCase("NRIC")) {
                soapResponse = soapConnection.call(createSOAPRequest(bytesArray), url);
            } else {
                soapResponse = soapConnection.call(createSOAPRequestForEP(bytesArray), url);
            }
            xmlResposne = getResponse(soapResponse);
            log.debug("Soap Response is length is :- " + xmlResposne.length());
            // Process the SOAP Response
            soapConnection.close();
            return xmlResposne;
        } catch (Exception e) {
            log.error("Exception in getting soap response:- " + e);
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    log.error("Exception in closing fileInputStream :- " + e);
                }
            }
        }
        return xmlResposne;
    }

    private static String getResponse(SOAPMessage soapMessage) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String strMsg = new String(out.toByteArray());
        strMsg = strMsg.replaceAll("&lt;", "<");
        strMsg = strMsg.replaceAll("&gt;", ">");
        return strMsg;
    }

    /**
     * This method creates the input or request for an soap web service.
     *
     * @param bytesArray
     * @return the soap message
     */
    private static SOAPMessage createSOAPRequest(String bytesArray) {
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();
            String serverURI = "http://newgen.co.in/Verification";
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration("Verification", serverURI);

            SOAPHeader header = envelope.getHeader();
            SOAPBody soapBody = envelope.getBody();

            SOAPElement soapBodyElem = soapBody.addChildElement("PreprocessingRequired", "Verification");
            soapBodyElem.addTextNode("N");
            header.appendChild(soapBodyElem);

            soapBodyElem = soapBody.addChildElement("Language", "Verification");
            soapBodyElem.addTextNode(properties.getProperty("Language"));
            header.appendChild(soapBodyElem);

            soapBodyElem = soapBody.addChildElement("FileName", "Verification");
            soapBodyElem.addTextNode("Test1.tif");
            header.appendChild(soapBodyElem);

            SOAPBody soapBody1 = envelope.getBody();
            SOAPElement soapBodyElem1 = soapBody1.addChildElement("ExtractNRICRequest", "Verification");
            SOAPElement soapBodyElem2 = soapBody1.addChildElement("FileByteStream", "Verification");

            soapBodyElem2.addTextNode(bytesArray);
            soapBodyElem1.addChildElement(soapBodyElem2);

            soapBody1.appendChild(soapBodyElem1);
            MimeHeaders headers = soapMessage.getMimeHeaders();

            //headers.addHeader("Content-Type","text/xml");
//                  headers.addHeader("charset","UTF-8");
            headers.addHeader("SOAPAction", "http://newgen.co.in/Verification/IVerification/ExtractNRIC");
            //headers.addHeader("Content-Length", "14928");
//                 headers.addHeader("Host", "127.0.0.1:9100");
            headers.addHeader("Connection", "Keep-Alive");
//                 headers.addHeader("Connection","Keep-Alive");
            soapMessage.saveChanges();
            log.debug("Soap Message in case of nric is ", soapMessage);
            return soapMessage;
        } catch (Exception e) {
            log.debug("Exception in creating soap message", e.getMessage());
            return null;
        }
    }


    /**
     * This method creates the input or request for an soap web service.
     *
     * @param bytesArray
     * @return the soap message
     */
    private static SOAPMessage createSOAPRequestForEP(String bytesArray) {
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();
            String serverURI = "http://newgen.co.in/Verification";
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration("Verification", serverURI);

            SOAPHeader header = envelope.getHeader();
            SOAPBody soapBody = envelope.getBody();

            SOAPElement soapBodyElem = soapBody.addChildElement("PreprocessingRequired", "Verification");
            soapBodyElem.addTextNode("N");
            header.appendChild(soapBodyElem);

            soapBodyElem = soapBody.addChildElement("Language", "Verification");
            soapBodyElem.addTextNode(properties.getProperty("Language"));
            header.appendChild(soapBodyElem);

            soapBodyElem = soapBody.addChildElement("FileName", "Verification");
            soapBodyElem.addTextNode("Test2.tif");
            header.appendChild(soapBodyElem);

            SOAPBody soapBody1 = envelope.getBody();
            SOAPElement soapBodyElem1 = soapBody1.addChildElement("ExtractEPMessage", "Verification");
            SOAPElement soapBodyElem2 = soapBody1.addChildElement("FileByteStream", "Verification");

            soapBodyElem2.addTextNode(bytesArray);
            soapBodyElem1.addChildElement(soapBodyElem2);

            soapBody1.appendChild(soapBodyElem1);
            MimeHeaders headers = soapMessage.getMimeHeaders();

            //headers.addHeader("Content-Type","text/xml");
//                  headers.addHeader("charset","UTF-8");
            headers.addHeader("SOAPAction", "http://newgen.co.in/Verification/IVerification/ExtractEP");
            //headers.addHeader("Content-Length", "14928");
//                 headers.addHeader("Host", "127.0.0.1:9100");
            headers.addHeader("Connection", "Keep-Alive");
//                 headers.addHeader("Connection","Keep-Alive");
            soapMessage.saveChanges();
            log.debug("soap message in case of ep is :- ", soapMessage);
            return soapMessage;
        } catch (Exception e) {
            log.debug("Exception in creating soap message", e.getMessage());
            return null;
        }


    }
}
