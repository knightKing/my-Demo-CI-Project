package hsbc.aofe.ocr.utility;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ParseOcrdImageResponse extends DefaultHandler implements Serializable {

    private static final long serialVersionUID = 1L;

    boolean isIdentityCardNoNode = false;
    boolean isDOBNode = false;
    boolean isCountryOfBirthNode = false;
    boolean isGenderNode = false;
    boolean isNameNode = false;
    boolean isFDNode = false;
    boolean isRaceNode = false;
    boolean isPNode = false;
    boolean isAddressNode = false;
    boolean isNRICNO = false;
    boolean isDOI = false;
    boolean isFin = false;
    boolean isEpDOB = false;
    boolean isDOE = false;
    boolean isEmployer = false;
    protected String fnValue = null;
    static protected String numValue = null;
    Multimap<String, Map<String, String>> extractedDataMap = ArrayListMultimap.create();


    public Multimap<String, Map<String, String>> imageTransform(String args) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        InputStream is = new ByteArrayInputStream(args.getBytes());
        saxParser.parse(is, this);
        return this.extractedDataMap;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        switch (qName) {

            case "P":
                switch (attributes.getValue("NUM")) {
                    case "1":
                        isPNode = true;
                        numValue = attributes.getValue("NUM");
                        break;

                    case "2":
                        isPNode = true;
                        numValue = attributes.getValue("NUM");
                        break;

                }
                break;
            case "F":
                switch (attributes.getValue("FN").toUpperCase()) {
                    case "IDENTITY CARD NO":
                        isIdentityCardNoNode = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "DOB":
                        isDOBNode = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "COUNTRY OF BIRTH":
                        isCountryOfBirthNode = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "GENDER":
                        isGenderNode = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "RACE":
                        isRaceNode = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "NAME":
                        isNameNode = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "ADDRESS":
                        isAddressNode = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "NRIC NO":
                        isNRICNO = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "DOI":
                        isDOI = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "FIN":
                        isFin = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "DATE OF BIRTH":
                        isEpDOB = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "DATE OF EXPIRY":
                        isDOE = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    case "EMPLOYER":
                        isEmployer = true;
                        fnValue = attributes.getValue("FN");
                        break;
                    default:
                        break;
                }
                break;
            case "FD":
                isFDNode = true;
                break;
            default:
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        switch (qName) {
            case "P":
                isPNode = false;
                break;
            case "F":
                isIdentityCardNoNode = false;
                isDOBNode = false;
                isCountryOfBirthNode = false;
                isGenderNode = false;
                isRaceNode = false;
                isNameNode = false;
                isAddressNode = false;
                isNRICNO = false;
                isDOI = false;
                isFin=false;
                isEpDOB = false;
                isDOE = false;
                isEmployer = false;
                break;
            case "FD":
                isFDNode = false;
                break;
            default:
                break;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        String valueFetchedFromXml = new String(ch, start, length);
        Map<String, String> nricExtractedMap = new HashMap<>();
        if (isPNode && isIdentityCardNoNode && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isDOBNode && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isCountryOfBirthNode && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isGenderNode && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isRaceNode && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isNameNode && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isAddressNode && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml.replaceAll("[^\\w\\s]", ""));
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isNRICNO && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if (isPNode && isDOI && isFDNode) {
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if(isPNode && isFin && isFDNode ){
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if(isPNode && isEpDOB && isFDNode ){
            nricExtractedMap.put("dob", valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if(isPNode && isDOE && isFDNode ){
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(), valueFetchedFromXml);
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }
        if(isPNode && isEmployer && isFDNode ){
            nricExtractedMap.put(fnValue.replace(" ", "_").toLowerCase(),
                    valueFetchedFromXml.replaceAll("[^\\w\\s]",""));
            this.extractedDataMap.put(numValue, nricExtractedMap);
        }

    }

}
