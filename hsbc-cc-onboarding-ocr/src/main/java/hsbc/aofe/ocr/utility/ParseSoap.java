package hsbc.aofe.ocr.utility;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import javax.xml.bind.DatatypeConverter;
import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * This is a soap web service client which is used to extract the data from a
 * pdf file by comsu,ing the Extraction,verification web service of IPS Product.
 *
 * @author shikhar.choudhary
 */
@Slf4j
public class ParseSoap {

    private static final String VERIFICATION = "Verification";
    private static final String PRIMARY_LOOKUP_COLUMN_AND_VALUE = "PrimaryLookupColumnAndValue";
    private static final String PREPROCESSINGREQUIRED = "PreprocessingRequired";
    private static final String LANGUAGE = "Language";
    private static final String FILE_NAME = "FileName";
    private static final String EXTRACT_MESSAGE = "ExtractMessage";
    private static final String FILE_BYTE_STREAM = "FileByteStream";
    private static final String SOAP_ACTION = "SOAPAction";
    //private static final Logger logger =Logger.getLogger(ParseSoap.class);
    private static Properties properties;
    private static FileInputStream fileInputStream = null;

    /**
     * This function gets the soap response by consuming the soap web service of IPS.
     *
     * @param bytesArray this is input byte array of an image.
     * @return a soap message
     * @throws SOAPException
     * @throws ByteArrayCannotBeNullOrEmpty
     */
    public String getSoapResponse(byte[] bytesArray) throws SOAPException, ByteArrayCannotBeNullOrEmpty {
        String xmlResposne = null;
        SOAPConnection soapConnection = null;
        SOAPMessage soapResponse = null;

        if (bytesArray == null || bytesArray.length == 0)
            throw new ByteArrayCannotBeNullOrEmpty("Image byte array can not be null or empty");

        FileInputStream fileInputStream = null;

        try {
            Resource resource = new ClassPathResource("/ips.properties");
            properties = PropertiesLoaderUtils.loadProperties(resource);
            // Create SOAP Connection
            log.info("Creating SOAP Connection");
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = properties.getProperty("soap_server");    //"http://127.0.0.1:9100/Verification?wsdl";
            String hostname = properties.getProperty("hostname");
            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                    log.debug("HostName equals hostname" + hostname.equals(hostname));
                    log.error("HostName equals hostname" + hostname.equals(hostname));
                    return hostname.equals(hostname);
                }
            });
            soapResponse = soapConnection.call(createSOAPRequest(bytesArray), url);
            xmlResposne = getResponse(soapResponse);
            log.info("Soap Response is :- " + xmlResposne);
            // Process the SOAP Response
            soapConnection.close();
            return xmlResposne;
        } catch (Exception e) {
            log.error("Exception in getting soap response:- " + e);
            log.debug("Exception in getting soap response:- " + e);
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    log.error("Exception in closing fileInputStream :- " + e);
                    log.debug("Exception in closing fileInputStream :- " + e);
                }
            }
        }
        return xmlResposne;
    }

    private static String getResponse(SOAPMessage soapMessage) throws Exception {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String strMsg = new String(out.toByteArray());
        strMsg = strMsg.replaceAll("&lt;", "<");
        strMsg = strMsg.replaceAll("&gt;", ">");
        return strMsg;
    }

    /**
     * This method creates the input or request for an soap web service.
     *
     * @param bytesArray
     * @return the soap message
     */
    private static SOAPMessage createSOAPRequest(byte[] bytesArray) {
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();
            String serverURI = "http://newgen.co.in/Verification";
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration(VERIFICATION, serverURI);

            SOAPHeader header = envelope.getHeader();
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem = soapBody.addChildElement(PRIMARY_LOOKUP_COLUMN_AND_VALUE, VERIFICATION);
            soapBodyElem.addTextNode("");
            header.appendChild(soapBodyElem);

            soapBodyElem = soapBody.addChildElement(PREPROCESSINGREQUIRED, VERIFICATION);
            soapBodyElem.addTextNode(properties.getProperty("PreprocessingRequired"));
            header.appendChild(soapBodyElem);

            soapBodyElem = soapBody.addChildElement(LANGUAGE, VERIFICATION);
            soapBodyElem.addTextNode(properties.getProperty("Language"));
            header.appendChild(soapBodyElem);

            soapBodyElem = soapBody.addChildElement(FILE_NAME, VERIFICATION);
            soapBodyElem.addTextNode("CPF_Sample.pdf");
            header.appendChild(soapBodyElem);

            SOAPBody soapBody1 = envelope.getBody();
            SOAPElement soapBodyElem1 = soapBody1.addChildElement(EXTRACT_MESSAGE, VERIFICATION);
            SOAPElement soapBodyElem2 = soapBody1.addChildElement(FILE_BYTE_STREAM, VERIFICATION);

            soapBodyElem2.addTextNode(DatatypeConverter.printBase64Binary(bytesArray));
            soapBodyElem1.addChildElement(soapBodyElem2);

            soapBody1.appendChild(soapBodyElem1);
            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", "http://newgen.co.in/Verification/IVerification/ExtractDocument");
            headers.addHeader("Connection", "Keep-Alive");
            soapMessage.saveChanges();
            log.info("Request SOAP Message:" + soapMessage);
            log.debug("Request SOAP Message:" + soapMessage);
            log.error("Request SOAP Message:" + soapMessage);
            return soapMessage;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            log.error("Exception in soap request: " + e);
            log.debug("Exception in soap request: " + e);
            return null;
        }

    }

}
