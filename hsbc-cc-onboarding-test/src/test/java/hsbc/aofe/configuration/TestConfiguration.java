package hsbc.aofe.configuration;
import java.util.ArrayList;
import java.util.List;

import hsbc.aofe.domain.Address;
import hsbc.aofe.domain.Applicant;
import hsbc.aofe.domain.ApplicantAdditionalInfo;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.ApplicationStaffDetails;
import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.domain.DocumentName;
import hsbc.aofe.domain.Title;


public class TestConfiguration {
	
	public static Application getDetailsApplication(){
		Application createdApllicationDetails = new Application();
		Applicant applicant=new Applicant();
		hsbc.aofe.domain.Name name =new hsbc.aofe.domain.Name();
		name.setFirstName("Michel");
		name.setLastName("Ross");			
		applicant.setSalutation(Title.MR);
		applicant.setGender("Male");
		applicant.setEmail("john@gmail.com");
		applicant.setMobile(987657689L);
		applicant.setDateOfBirth("28-06-85");
		applicant.setId(87696565L);
		applicant.setIdDocType(DocumentName.NRIC);
		applicant.setName(name);
		List<CreditCard> cards=new ArrayList<>();
		CreditCard creditCard=new CreditCard();
		creditCard.setKey("");
		creditCard.setAnnualFee(78965L);
		creditCard.setComplimentaryStuff("GiftVoucher");
		creditCard.setKey("ward");
		cards.add(creditCard);		
		applicant.setCards(cards);
		ApplicantAdditionalInfo applicantAdditionalInfo=new ApplicantAdditionalInfo() ;
		applicantAdditionalInfo.setAnnualIncome(9876L);
		applicantAdditionalInfo.setAssociateOfPublicPositionHolder(false);
		applicantAdditionalInfo.setAutoDebitFromAccountNumber("khy7i");
		List<Address> addresses=new ArrayList<>();
		Address address=new Address();
		address.setCity("Singapore");
		address.setId(3456L);
		address.setLine1Address("4, Leng Kee Road ");
		address.setLine2Address("#06-07 ");
		address.setLine3Address(" SiS Building");
		address.setPostalCode("159088");
		addresses.add(address);
		applicantAdditionalInfo.setAddress(addresses);
		applicant.setAdditionalInfo(applicantAdditionalInfo);
		createdApllicationDetails.setAddSupplimentaryCard(false);
		createdApllicationDetails.setAdditionalIncome("675478");
		createdApllicationDetails.setAgreeToGenericConsent(true);	
		createdApllicationDetails.setPrimaryApplicant(applicant);
		createdApllicationDetails.setArn("ABCDEG98");
		createdApllicationDetails.setHowMuchAutoDebit(876453L);
		createdApllicationDetails.setExistingCardNumber("HJNKy456");
		createdApllicationDetails.setIncomeAutomatedCalcIndicator(false);
		createdApllicationDetails.setEmploymentCheckIndicator(true);
		createdApllicationDetails.setReceiveEStatement(true);
		createdApllicationDetails.setTransactingOnMyOwn(false);
		//createdApllicationDetails.setVerifiedIncome("67853");
		ApplicationStaffDetails  applicationStaffDetails=new ApplicationStaffDetails();
		applicationStaffDetails.setApprovedBy("Alex");
		applicationStaffDetails.setCreditCardNumber("hjuyt5678990");
		applicationStaffDetails.setEcpfAuthorized(false);
		applicationStaffDetails.setLocation("Pandan Crescent ");
		applicationStaffDetails.setNameOfStaff("Shahna");
		applicationStaffDetails.setReferralId("juhy67");
		applicationStaffDetails.setPreApprovedLimit(20000000L);
		applicationStaffDetails.setRemarks("Priority Case");
		applicationStaffDetails.setVoucherCode("LPO7890");
		createdApllicationDetails.setStaffDetails(applicationStaffDetails);
		return createdApllicationDetails;
	}
}
