package hsbc.aofe.service;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import hsbc.aofe.configuration.TestConfiguration;
import hsbc.aofe.domain.Application;
import hsbc.aofe.exception.RequestAlreadyExistException;
import hsbc.aofe.repository.common.ApplicationRepositoryService;

public class ApplicationServiceImplTest {

	@Mock
	ApplicationRepositoryService applicationRepositoryService;

	@InjectMocks
	ApplicationServiceImpl applicationServiceImpl;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateApplication() {
		Application createdapplication = new Application();
		Application createdApllicationDetails = TestConfiguration.getDetailsApplication();
		Mockito.when(applicationRepositoryService.createApplication(Mockito.eq("uid"),Mockito.eq("channelAuthority"),Mockito.eq(createdapplication)))
				.thenReturn(createdApllicationDetails);
		Application response = applicationServiceImpl.createApplication("uid","channelAuthority",createdapplication);
		Assert.assertEquals(createdApllicationDetails, response);
		Assert.assertEquals("ABCDEG98", response.getArn());
		Assert.assertEquals("675478", response.getAdditionalIncome());
		Assert.assertNotNull(response.getPrimaryApplicant().getGender());
		Assert.assertNotNull(response.getPrimaryApplicant().getAdditionalInfo().getAddress());
		Assert.assertEquals("Shahna", response.getStaffDetails().getNameOfStaff());
		Assert.assertEquals("Pandan Crescent ", response.getStaffDetails().getLocation());
		Assert.assertEquals("LPO7890", response.getStaffDetails().getVoucherCode());
	
	}

	@Test
	public void testCreateApplicationNotNull() {
		Application application = TestConfiguration.getDetailsApplication();
		Mockito.when(applicationRepositoryService.createApplication(Mockito.eq("uid"),Mockito.eq("channelAuthority"),Mockito.eq(application))).thenReturn(application);
		Assert.assertNotNull(applicationServiceImpl.createApplication("uid","channelAuthority",application));
	}

	@Test
	public void testCreateApplicationWhenExceptionOccurs() {
		Application application = TestConfiguration.getDetailsApplication();
		Mockito.when(applicationRepositoryService.createApplication(Mockito.eq("uid"),Mockito.eq("channelAuthority"),Mockito.eq(application)))
				.thenThrow(new RequestAlreadyExistException(
						"Application already in-progress with provide NRIC ID / Passport #  / EP ID."));

		try {
			applicationServiceImpl.createApplication("uid","channelAuthority",application);
			Assert.fail("Exception Not thrown");
		} catch (Exception e) {
			Assert.assertTrue(e instanceof RequestAlreadyExistException);
			RequestAlreadyExistException ex = (RequestAlreadyExistException) e;
			Assert.assertEquals("Application already in-progress with provide NRIC ID / Passport #  / EP ID.",
					ex.getField());
		}
	}
}