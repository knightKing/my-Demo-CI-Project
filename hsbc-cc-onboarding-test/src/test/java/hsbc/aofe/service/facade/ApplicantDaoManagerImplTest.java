package hsbc.aofe.service.facade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.transaction.support.TransactionTemplate;

import hsbc.aofe.configuration.TestConfiguration;
import hsbc.aofe.domain.Application;
import hsbc.aofe.service.jparepository.AddressRepository;
import hsbc.aofe.service.jparepository.AddressTypeMasterRepository;
import hsbc.aofe.service.jparepository.ApplicantAdditionalInfoRepository;
import hsbc.aofe.service.jparepository.ApplicantAddressRepository;
import hsbc.aofe.service.jparepository.ApplicantDetailsRepository;
import hsbc.aofe.service.jparepository.ArnCcBridgeRepository;
import hsbc.aofe.service.jparepository.CCApplicationAdditionalInfoRepository;
import hsbc.aofe.service.jparepository.CCApplicationRepository;
import hsbc.aofe.service.jparepository.ChannelMasterRepository;
import hsbc.aofe.service.jparepository.CountryListMasterRepository;
import hsbc.aofe.service.jparepository.CreditCardMasterRepository;
import hsbc.aofe.service.jparepository.DocumentsMasterRepository;
import hsbc.aofe.service.jparepository.EducationalLevelMasterRepository;
import hsbc.aofe.service.jparepository.HomeOwnershipMasterRepository;
import hsbc.aofe.service.jparepository.JobTypeMasterRepository;
import hsbc.aofe.service.jparepository.NGStatusMasterRepository;
import hsbc.aofe.service.jparepository.SuppApplicantRepository;

public class ApplicantDaoManagerImplTest {
	@Mock
	private ApplicantDetailsRepository applicantRepository;
	@Mock
    private ApplicantAdditionalInfoRepository applicantAdditionalInfoRepository;
	@Mock
    private ApplicantAddressRepository applicantAddressRepository;
	@Mock
    private CreditCardMasterRepository creditCardMasterRepository;
	@Mock
    private ArnCcBridgeRepository arnCcBridgeRepository;
	@Mock
    private EducationalLevelMasterRepository educationalLevelMasterRepository;
	@Mock
    private HomeOwnershipMasterRepository homeOwnershipMasterRepository;
	@Mock
	private JobTypeMasterRepository jobTypeMasterRepository;
	@Mock
	private AddressTypeMasterRepository addressTypeMasterRepository;
	@Mock
	private CountryListMasterRepository countryListMasterRepository;
	@Mock
	private NGStatusMasterRepository ngStatusMasterRepository;
	@Mock
	private DocumentsMasterRepository documentsMasterRepository;
	@Mock
	private CCApplicationRepository ccApplicationRepository;
	@Mock
	private SuppApplicantRepository suppApplicantRepository;
	@Mock
	private CCApplicationAdditionalInfoRepository ccApplicationAdditionalInfoRepository;
	@Mock
	private AddressRepository addressRepository;
	@Mock
	private ChannelMasterRepository channelMasterRepository;
	@Mock
	private TransactionTemplate transactionTemplate;
	
	@InjectMocks
	private ApplicantDaoManagerImpl applicantDaoManagerImpl;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	TestConfiguration testConfiguration = new TestConfiguration();
	
	@Test
	public void testpopulateArnCCBridgeEntity() {
		Application createdApllicationDetails = testConfiguration.getDetailsApplication();
		Application response = applicantDaoManagerImpl.createApplicantUsingApplication("uid", "channelAuthority", createdApllicationDetails);
		Assert.assertNotNull(response);
		Assert.assertEquals("ABCDEG98", response.getArn());
		
	}
	
	@Test
	public void testpopulateArnCCBridgeEntitywithnullApplicationInfo() {
		Application createdApllicationDetails = testConfiguration.getDetailsApplication();
		createdApllicationDetails.getPrimaryApplicant().setAdditionalInfo(null);
		applicantDaoManagerImpl.createApplicantUsingApplication("uid", "channelAuthority", createdApllicationDetails);
		Assert.assertNotNull(applicantDaoManagerImpl.createApplicantUsingApplication("uid", "channelAuthority", createdApllicationDetails));
	}
		
}
