package hsbc.aofe.iupload.contract;

import java.util.List;

import hsbc.aofe.iupload.domain.IUploadDetail;
import hsbc.aofe.iupload.domain.IUploadRequestData;

public interface IUploadService {

    /**
     * This method is used to return ARNS from
     * {@link hsbc.aofe.iupload.entities.CCApplicationEntity} based on the input
     * status
     *
     * @param status
     * @return List of ARN
     */
    List<String> getARNsExcludingIUploadARNs(String status);

    /**
     * This method is used to return IccDetails based on the input arn
     *
     * @param arn
     * @return IccDetail
     */
    IUploadDetail getIUploadDetails(String arn);

    /**
     * This method is used to POST ICCRequestBody data to IUploadEntity
     *
     * @param iccRequestBody
     * @param arn
     */
    void saveStatus(IUploadRequestData iUploadRequestBody, String arn);
}
