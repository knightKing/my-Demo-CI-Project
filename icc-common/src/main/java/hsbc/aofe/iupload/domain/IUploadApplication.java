package hsbc.aofe.iupload.domain;

import java.io.Serializable;

import lombok.Data;

import java.io.Serializable;

import lombok.Data;

@Data
public class IUploadApplication implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5532553101494251366L;

    private Long id;
    private Long applicantId;

}
