package hsbc.aofe.iupload.domain;

import java.io.Serializable;
import java.util.List;

import hsbc.aofe.domain.DocumentName;
import lombok.Data;

@Data
public class IUploadDetail implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5532553101494251386L;

    private String id;
    private List<IUploadImage> images;
    private DocumentName idType;
    private String idValue;
    private String aoRefNumber;


}
