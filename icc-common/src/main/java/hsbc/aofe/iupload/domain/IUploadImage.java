package hsbc.aofe.iupload.domain;

import java.io.Serializable;

import hsbc.aofe.domain.DocumentGroup;
import lombok.Data;

@Data
public class IUploadImage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5532553101494251387L;

    private String imageName;
    private Long imageIndex;
    private byte[] photo;
    private Long applicantId;
    private DocumentGroup documentGroup;
    private String applicantName;

}