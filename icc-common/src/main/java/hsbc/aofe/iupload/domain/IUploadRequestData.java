package hsbc.aofe.iupload.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class IUploadRequestData implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6578411511737438264L;

    private String arn;
    private String uploadStatus;
    private String folderName;

}

