package hsbc.aofe.iupload.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "iupload")
public class IUploadConfigProperties {

    private final BatchFolder BatchFolder = new BatchFolder();
    private final MetadataConstants MetadataConstants = new MetadataConstants();

    @Getter
    @Setter
    public static class BatchFolder {
        private String localStorage;
        private Integer bundleCapacity;
    }

    @Getter
    @Setter
    public static class MetadataConstants {
        private String source;
        private String countryCode;
        private String entityCode;
        private String productTeam;
        private String customerGroup;
        private String productType;
        private String numberType;
        private String idType;
    }

}
