package hsbc.aofe.iupload.constants;

public enum FileExtensionConstants {
	
	PDF(".pdf"),
	RDY(".rdy"),
	XML(".xml"),
	ZIP(".zip");
	
	private String value;
	
	private FileExtensionConstants(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
