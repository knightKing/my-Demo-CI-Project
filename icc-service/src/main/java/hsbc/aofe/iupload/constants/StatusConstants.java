package hsbc.aofe.iupload.constants;

public enum StatusConstants {

    PENDING("PENDING"),
    DONE("DONE"),
    SUBMITTED_TO_AO("SUBMITTED-TO-AO");

	private String value;
	
	private StatusConstants(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
