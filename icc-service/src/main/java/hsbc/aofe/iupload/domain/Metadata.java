package hsbc.aofe.iupload.domain;

import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
public class Metadata {

	@XmlElement(name = "ProductTeam")
    private String productTeam;
	
	@XmlElement(name = "CountryCode")
    private String countryCode;
	
	@XmlElement(name = "EntityCode")
    private String entityCode;
	
	@XmlElement(name = "CustomerGroup")
    private String customerGroup;
	
	@XmlElement(name = "ProductType")
    private String productType;
	
	@XmlElement(name = "NumberType")
    private String numberType;
	
	@XmlElement(name = "NumberValue")
    private String numberValue;
	
	@XmlElement(name = "IDType")
    private String idType;
	
	@XmlElement(name = "IDValue")
    private String idValue;
	
	@XmlElement(name = "Reference")
    private String reference;
	
	@XmlElement(name = "DocumentDate")
    private String documentDate;
	
	@XmlElement(name = "Reference2")
    private String reference2;
	
	@XmlElement(name = "Reference3")
    private String reference3;
	
	@XmlElement(name = "FTPPath")
    private String ftpPath;

}
