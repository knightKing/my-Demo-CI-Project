package hsbc.aofe.iupload.scheduler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import hsbc.aofe.iupload.config.IUploadConfigProperties;
import hsbc.aofe.iupload.constants.FileExtensionConstants;
import hsbc.aofe.iupload.constants.StatusConstants;
import hsbc.aofe.iupload.contract.IUploadService;
import hsbc.aofe.iupload.domain.Batch;
import hsbc.aofe.iupload.domain.IUploadDetail;
import hsbc.aofe.iupload.domain.IUploadImage;
import hsbc.aofe.iupload.domain.IUploadRequestData;
import hsbc.aofe.iupload.domain.Metadata;
import hsbc.aofe.iupload.util.IUploadUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class IUploadScheduler {

	private IUploadService iUploadService;
	private IUploadConfigProperties iUploadConfigProperties;
	private JAXBContext jaxbContext;
	private int bundleCount;

	@Autowired
	public IUploadScheduler(IUploadService iUploadService, IUploadConfigProperties iUploadConfigProperties) {
		this.iUploadService = iUploadService;
		this.iUploadConfigProperties = iUploadConfigProperties;
		try {
			this.jaxbContext = JAXBContext.newInstance(Batch.class);
		} catch (JAXBException e) {
			log.debug("Error while creating jaxb context " + toString(e));
		}
	}

	/**
	 * IUploadScheduler method is responsible to create batchFolders for the
	 * applications that are SUBMITTED-TO-AO.
	 *
	 */
	@Scheduled(cron = "${iupload.scheduler.delay}")
	public void runScheduler() {
		log.info("Scheduler has been started at " + new Date());
		List<String> arnList = iUploadService.getARNsExcludingIUploadARNs(StatusConstants.SUBMITTED_TO_AO.getValue());
		if (CollectionUtils.isNotEmpty(arnList)) {
			log.info("Scheduler has started processing " + arnList.size() + " applications.");
			arnList.stream().forEach(arn -> processArn(arn));
		} else {
			log.info("No application found to be processed by scheduler.");
		}
	}

	/**
	 * This method is responsible for processing arn
	 * 
	 * @param arn
	 */
	private void processArn(String arn) {
		log.info("Scheduler has started processing arn " + arn);
		IUploadDetail iUploadDetail = iUploadService.getIUploadDetails(arn);

		if (CollectionUtils.isNotEmpty(iUploadDetail.getImages())) {
			Map<Integer, List<IUploadImage>> imageBundleMap = getImageBundleMap(iUploadDetail.getImages());
			imageBundleMap.keySet().stream().forEach(key -> {
				createBatchFoldersWithImageBundleMap(imageBundleMap.get(key), iUploadDetail, arn);
			});
			log.info("Scheduler has finished processing arn " + arn);
		} else {
			log.info("Scheduler can't process arn " + arn + ". No document found!");
		}
	}

	/**
	 * This method is used to create batch folders. Batch folder has an limit of
	 * 7 documents. So if an application contains more than 7 documents then
	 * there will be multiple folders for that application.
	 * 
	 * @param images
	 * @param iUploadDetail
	 * @param arn
	 */
	private void createBatchFoldersWithImageBundleMap(List<IUploadImage> images, IUploadDetail iUploadDetail,
			String arn) {

		log.info("Generating batch folder name for arn : " + arn);
		String batchFolderName = generateBatchFolderName();
		log.info("Batch folder name " + batchFolderName + " has been generated for arn : " + arn);

		String contextPath = File.separator + iUploadConfigProperties.getBatchFolder().getLocalStorage()
				+ File.separator;
		String batchFolder = "documents" + File.separator + batchFolderName;

		try {
			Map<String, String> ftpPathMap = addDocumentsToSubBatchFolder(images, batchFolder, contextPath, arn);

            log.info("Creating rdy file for arn : " + arn);
			addRDYFileToBatchFolder(batchFolderName, contextPath);
			log.info(batchFolderName + ".rdy file has been created for arn : " + arn);

			log.info("Creating xml file for arn : " + arn);
			Batch batch = initializeMetadata(images, ftpPathMap, batchFolderName, iUploadDetail);
			addXMLFileToBatchFolder(batchFolderName, contextPath, batch, jaxbContext);
			log.info(batchFolderName + ".xml file has been created for arn : " + arn);

			log.info("Updating IUpload table with " + batchFolderName + " for arn : " + arn);
			IUploadRequestData iUploadRequestData = new IUploadRequestData();
			iUploadRequestData.setFolderName(batchFolderName);
			iUploadRequestData.setUploadStatus(StatusConstants.DONE.getValue());
			iUploadService.saveStatus(iUploadRequestData, arn);
			log.info("Table updated successfully");
		} catch (Exception e) {
			log.error("Exception occurred while processing for arn : " + arn + " " + e);
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to create a Map containing bundles of image ids. A
	 * bundle contains at max 7 documents in it.
	 * 
	 * @param imageIds
	 * @return
	 */
	private Map<Integer, List<IUploadImage>> getImageBundleMap(List<IUploadImage> images) {
		int imageStoreCapacity = iUploadConfigProperties.getBatchFolder().getBundleCapacity();
		int bundles = getBundleNumbers(images.size(), imageStoreCapacity);
		bundleCount = 0;
		Map<Integer, List<IUploadImage>> imageBundleMap = new HashMap<>();
		for (int i = 0; i < bundles; i++) {
			int fromIndex = i * imageStoreCapacity;
			int toIndex = images.size() < ((i + 1) * imageStoreCapacity) ? images.size()
					: ((i + 1) * imageStoreCapacity);
			imageBundleMap.put(i + 1, images.subList(fromIndex, toIndex));
		}
		return imageBundleMap;
	}

	/**
	 * This method is used to provide the number of bundles that will be created
	 * based on available image ids for an application
	 * 
	 * @param size
	 * @param imageStoreCapacity
	 * @return
	 */
	private int getBundleNumbers(int size, int imageStoreCapacity) {
		if (size <= imageStoreCapacity) {
			return ++bundleCount;
		} else {
			++bundleCount;
			getBundleNumbers(size - imageStoreCapacity, imageStoreCapacity);
		}
		return bundleCount;
	}

	/**
	 * This method is used to generate batchFolder name that is the combination
	 * of countryCode+ entityCode + currentTimeStamp
	 * 
	 * @return
	 */
	private String generateBatchFolderName() {
		StringBuilder fileName = new StringBuilder(iUploadConfigProperties.getMetadataConstants().getCountryCode()
				+ iUploadConfigProperties.getMetadataConstants().getEntityCode());
		fileName.append(IUploadUtils.getCurrentTimeStampAsString());
		return fileName.toString();
	}

	/**
	 * This method is used to add documents to the batch folder
	 * 
	 * @param images
	 * @param batchFolder
	 * @param contextPath
	 * @param arn
	 * @return
	 */
	private Map<String, String> addDocumentsToSubBatchFolder(List<IUploadImage> images, String batchFolder,
			String contextPath, String arn) {
		log.info("Saving documents to BatchFolder " + batchFolder + " for arn : " + arn);
		Map<String, String> ftpPathMap = new HashMap<>();
		images.stream().forEach(image -> {
			if (image.getImageName() == null) {
				image.setImageName("");
			}

			String imageName = "";
			String imageExtension = "";

			if (image.getImageName() != null && image.getImageName().contains(".")) {
				int lastIndexOfDot = image.getImageName().lastIndexOf(".");
				imageName = image.getImageName().substring(0, lastIndexOfDot);
				imageExtension = image.getImageName().substring(lastIndexOfDot, image.getImageName().length());
			}
			String documentName = batchFolder + File.separator + imageName + "_" + image.getImageIndex() + "_"
					+ image.getApplicantId() + "_" + image.getDocumentGroup() + imageExtension;

			// this key generated to insert unique keys in ftpPathMap
			String ftpPathMapKey = image.getImageName() + "_" + image.getImageIndex() + "_" + image.getApplicantId()
					+ "_" + image.getDocumentGroup();
			ftpPathMap.put(ftpPathMapKey, documentName);
			try {
				IUploadUtils.addFileToBatchFolder(image.getPhoto(), contextPath + documentName);
			} catch (IOException e) {
				log.error("Exception occured while saving documents to batch folder " + batchFolder + " for arn : "
						+ arn + " " + e);
			}
		});
		log.info("Documents saved successfully in batchFolder " + batchFolder + " for arn : " + arn);
		return ftpPathMap;
	}

	/**
	 * This method is used to add RDY file to ZIP folder
	 * 
	 * @param batchFolderName
	 * @param zipOutputStream
	 * @throws IOException
	 */
	private void addRDYFileToBatchFolder(String batchFolderName, String contextPath) throws IOException {
		String rdyFileName = contextPath + batchFolderName + FileExtensionConstants.RDY.getValue();
		IUploadUtils.addEmptyFileToBatchFolder(rdyFileName);
	}

	/**
	 * This method is used to add XML file to ZIP folder
	 * 
	 * @param batchFolderName
	 * @param zipOutputStream
	 * @param batch
	 * @throws Exception
	 */
	private void addXMLFileToBatchFolder(String batchFolderName, String contextPath, Batch batch, JAXBContext jaxbContext) throws Exception {
		log.debug("Inside addXMLFileToBatchFolder method to create xml file " + batchFolderName + FileExtensionConstants.XML.getValue());
		String xmlFileName = contextPath + batchFolderName + FileExtensionConstants.XML.getValue();
		File xmlFile = new File(xmlFileName);
		xmlFile.getParentFile().mkdirs();
		xmlFile.createNewFile();
		log.debug("Marshalling metadata " + xmlFileName);
		IUploadUtils.marshalXMLFile(xmlFile, batch, jaxbContext);
		log.debug("Marshalling of metadata done successfully " + xmlFile.toString());
		log.debug("Closing addXMLFileToBatchFolder method");
	}

	/**
	 * This method is used to initialize meta data
	 * 
	 * @param images
	 * @param ftpPathMap
	 * @param batchFolderName
	 * @param iUploadDetail
	 * @return
	 */
	private Batch initializeMetadata(List<IUploadImage> images, Map<String, String> ftpPathMap, String batchFolderName,
			IUploadDetail iUploadDetail) {

		List<Metadata> metadataList = new ArrayList<>();
        log.debug("INITIALIZING: xml metadata creation");
		images.stream().forEach(image -> {
			Metadata metadata = new Metadata();
			log.debug("Product Team : {}", iUploadConfigProperties.getMetadataConstants().getProductTeam());
			metadata.setProductTeam(iUploadConfigProperties.getMetadataConstants().getProductTeam());
			log.debug("Country Code : {}", iUploadConfigProperties.getMetadataConstants().getCountryCode());
			metadata.setCountryCode(iUploadConfigProperties.getMetadataConstants().getCountryCode());
			log.debug("Entity Code : {}", iUploadConfigProperties.getMetadataConstants().getEntityCode());
			metadata.setEntityCode(iUploadConfigProperties.getMetadataConstants().getEntityCode());
			log.debug("Customer Group : {}", iUploadConfigProperties.getMetadataConstants().getCustomerGroup());
			metadata.setCustomerGroup(iUploadConfigProperties.getMetadataConstants().getCustomerGroup());
			log.debug("Product Type : {}", iUploadConfigProperties.getMetadataConstants().getProductType());
			metadata.setProductType(iUploadConfigProperties.getMetadataConstants().getProductType());
			log.debug("Number Type : {}", iUploadConfigProperties.getMetadataConstants().getNumberType());
			metadata.setNumberType(iUploadConfigProperties.getMetadataConstants().getNumberType());
			log.debug("Number Value : {}", iUploadDetail.getAoRefNumber());
			metadata.setNumberValue(checkForNull(iUploadDetail.getAoRefNumber()));
			log.debug("Id type : {}", iUploadConfigProperties.getMetadataConstants().getIdType());
			metadata.setIdType(iUploadConfigProperties.getMetadataConstants().getIdType());
			log.debug("Id Value : {}", iUploadDetail.getIdValue());
			metadata.setIdValue(checkForNull(iUploadDetail.getIdValue()));
			log.debug("Reference : {}", image.getDocumentGroup().getValue());
			metadata.setReference(checkForNull(image.getDocumentGroup().getValue()));
			log.debug("Document Date : {}", batchFolderName.substring(6, batchFolderName.length()));
			metadata.setDocumentDate(batchFolderName.substring(6, batchFolderName.length()));
			log.debug("Reference 2 : {}", batchFolderName + ":" + images.size());
			metadata.setReference2(batchFolderName + ":" + images.size());
			log.debug("Reference 3 : {}", image.getApplicantName());
			metadata.setReference3(checkForNull(image.getApplicantName()));
			String ftpPathMapKey = image.getImageName() + "_" + image.getImageIndex() + "_" + image.getApplicantId()
					+ "_" + image.getDocumentGroup();
			log.debug("Ftp Path : {}", ftpPathMap.get(ftpPathMapKey));
			metadata.setFtpPath(ftpPathMap.get(ftpPathMapKey));
			log.debug("Intialized Metadata properties for image " + image.getImageName() + " are : " + metadata);
			metadataList.add(metadata);
		});

		Batch batch = new Batch();
		batch.setSource(iUploadConfigProperties.getMetadataConstants().getSource());
		batch.setFile(metadataList);
        log.debug("INITIALIZATION COMPLETED SUCCESSFULLY.");
		return batch;
	}

	private String checkForNull(String property) {
		return property == null ? "" : property;
	}
	
	public static String toString(Object o) {
        if (o == null)
            return null;
        return new ReflectionToStringBuilder(o, new RecursiveToStringStyle()).toString();
    }

}
