package hsbc.aofe.iupload.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hsbc.aofe.iupload.contract.IUploadService;
import hsbc.aofe.iupload.domain.IUploadDetail;
import hsbc.aofe.iupload.domain.IUploadRequestData;
import hsbc.aofe.repository.common.IUploadRepositoryService;

@Service
public class IUploadServiceImpl implements IUploadService {
	
	@Autowired
	private IUploadRepositoryService iUploadRepositoryService;

	/**
	 * This method is used to return ARNS from based on the input status
	 *
	 * @param status
	 * @return List of ARN
	 */
	@Override
	public List<String> getARNsExcludingIUploadARNs(String status) {
		return iUploadRepositoryService.getARNsExcludingIUploadARNs(status);
	}

	/**
	 * This method is used to return IccDetails based on the input arn
	 *
	 * @param arn
	 * @return IccDetail
	 */
	@Override
	public IUploadDetail getIUploadDetails(String arn) {
		return iUploadRepositoryService.findApplicationByARN(arn);
	}

	@Override
	public void saveStatus(IUploadRequestData iUploadRequestBody, String arn) {
		iUploadRequestBody.setArn(arn);
		iUploadRepositoryService.saveStatus(iUploadRequestBody);
	}
}
