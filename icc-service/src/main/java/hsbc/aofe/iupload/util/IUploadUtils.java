package hsbc.aofe.iupload.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import hsbc.aofe.iupload.domain.Batch;
import hsbc.aofe.iupload.domain.Metadata;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IUploadUtils {

	/**
	 * This method creates an XML file and marshal it according to supplied
	 * Class
	 * 
	 * @param file
	 * @param batch
	 * @return
	 * @throws Exception
	 */
	public static void marshalXMLFile(File file, Batch batch, JAXBContext jaxbContext) throws Exception {
	
		log.debug("marshalXMLFile : Inside marshalXMLFile method");
		log.debug("Input File: " + toString(file));
		log.debug("Input batch : "+ toString(batch));
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		log.debug("marshalXMLFile : Document Builder Factory " + toString(dbFactory));
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		log.debug("marshalXMLFile : Document Builder " + toString(dBuilder));
		Document doc = dBuilder.newDocument();
		log.debug("marshalXMLFile : Document " + toString(doc));
		
		Element rootElementBatch = doc.createElement("Batch");
		log.debug("marshalXMLFile : rootElement " + toString(rootElementBatch));
		doc.appendChild(rootElementBatch);
		
		Element sourceNode = doc.createElement("Source");
		log.debug("marshalXMLFile : sourceNode " + toString(sourceNode));
		sourceNode.appendChild(doc.createTextNode(batch.getSource()));
		rootElementBatch.appendChild(sourceNode);
		
		List<Metadata> metadataList = batch.getFile();
		
		for(Metadata metadata : metadataList){
			Element fileNode = doc.createElement("File");
			log.debug("marshalXMLFile : fileNode " + toString(fileNode));
			rootElementBatch.appendChild(fileNode);
			
			Element productTeamNode = doc.createElement("ProductTeam");
			log.debug("marshalXMLFile : productTeamNode " + toString(productTeamNode));
			productTeamNode.appendChild(doc.createTextNode(metadata.getProductTeam()));
			fileNode.appendChild(productTeamNode);
			
			Element countryCodeNode = doc.createElement("CountryCode");
			log.debug("marshalXMLFile : countryCodeNode " + toString(countryCodeNode));
			countryCodeNode.appendChild(doc.createTextNode(metadata.getCountryCode()));
			fileNode.appendChild(countryCodeNode);
			
			Element entityCodeNode = doc.createElement("EntityCode");
			log.debug("marshalXMLFile : entityCodeNode " + toString(entityCodeNode));
			entityCodeNode.appendChild(doc.createTextNode(metadata.getEntityCode()));
			fileNode.appendChild(entityCodeNode);
			
			Element customerGroupNode = doc.createElement("CustomerGroup");
			log.debug("marshalXMLFile : customerGroupNode " + toString(customerGroupNode));
			customerGroupNode.appendChild(doc.createTextNode(metadata.getCustomerGroup()));
			fileNode.appendChild(customerGroupNode);	
			
			Element productTypeNode = doc.createElement("ProductType");
			log.debug("marshalXMLFile : productTypeNode " + toString(productTypeNode));
			productTypeNode.appendChild(doc.createTextNode(metadata.getProductType()));
			fileNode.appendChild(productTypeNode);
			
			Element numberTypeNode = doc.createElement("NumberType");
			log.debug("marshalXMLFile : numberTypeNode " + toString(numberTypeNode));
			numberTypeNode.appendChild(doc.createTextNode(metadata.getNumberType()));
			fileNode.appendChild(numberTypeNode);
			
			Element numberValueNode = doc.createElement("NumberValue");
			log.debug("marshalXMLFile : numberValueNode " + toString(numberValueNode));
			numberValueNode.appendChild(doc.createTextNode(metadata.getNumberValue()));
			fileNode.appendChild(numberValueNode);
			
			Element idTypeNode = doc.createElement("IDType");
			log.debug("marshalXMLFile : idTypeNode " + toString(idTypeNode));
			idTypeNode.appendChild(doc.createTextNode(metadata.getIdType()));
			fileNode.appendChild(idTypeNode);
			
			Element idValueNode = doc.createElement("IDValue");
			log.debug("marshalXMLFile : idValueNode " + toString(idValueNode));
			idValueNode.appendChild(doc.createTextNode(metadata.getIdValue()));
			fileNode.appendChild(idValueNode);
			
			Element referenceNode = doc.createElement("Reference");
			log.debug("marshalXMLFile : referenceNode " + toString(referenceNode));
			referenceNode.appendChild(doc.createTextNode(metadata.getReference()));
			fileNode.appendChild(referenceNode);
			
			Element documentDateNode = doc.createElement("DocumentDate");
			log.debug("marshalXMLFile : documentDateNode " + toString(documentDateNode));
			documentDateNode.appendChild(doc.createTextNode(metadata.getDocumentDate()));
			fileNode.appendChild(documentDateNode);
			
			Element reference2Node = doc.createElement("Reference2");
			log.debug("marshalXMLFile : reference2Node " + toString(reference2Node));
			reference2Node.appendChild(doc.createTextNode(metadata.getReference2()));
			fileNode.appendChild(reference2Node);
			
			Element reference3Node = doc.createElement("Reference3");
			log.debug("marshalXMLFile : reference3Node " + toString(reference3Node));
			reference3Node.appendChild(doc.createTextNode(metadata.getReference3()));
			fileNode.appendChild(reference3Node);
			
			Element ftpPathNode = doc.createElement("FTPPath");
			log.debug("marshalXMLFile : ftpPathNode " + toString(ftpPathNode));
			ftpPathNode.appendChild(doc.createTextNode(metadata.getFtpPath()));
			fileNode.appendChild(ftpPathNode);	
		}
		
		// write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        log.debug("marshalXMLFile : transformerFactory " + transformerFactory);
        Transformer transformer = transformerFactory.newTransformer();
        log.debug("marshalXMLFile : transformer " + transformer);
        DOMSource dmSource = new DOMSource(doc);
        log.debug("marshalXMLFile : dmSource " + dmSource);
        StreamResult result = new StreamResult(file);
        log.debug("marshalXMLFile : result " + result);
        transformer.transform(dmSource, result); 
        log.debug("marshalXMLFile : Data has been marshalled");
	}
	
	public static String toString(Object o) {
        if (o == null)
            return null;
        return new ReflectionToStringBuilder(o, new RecursiveToStringStyle()).toString();
    }

	/**
	 * This method is used to return current time stamp value as a string
	 *
	 * @return
	 */
	public static String getCurrentTimeStampAsString() {
		StringBuilder timestampString = new StringBuilder();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String[] splittedValues = timestamp.toString().split("-| |:|\\.");
		for (int i = 0; i < splittedValues.length; i++) {
			timestampString.append(splittedValues[i]);
		}
		return timestampString.toString();
	}

	/**
	 * This method is used to add supplied file to the BatchFolder
	 * 
	 * @param file
	 * @param filePath
	 * @throws IOException
	 */
	public static void addFileToBatchFolder(File file, String filePath) throws IOException {
	    log.debug("Inside addFileToBatchFolder method");
		try (FileInputStream fis = new FileInputStream(file)) {
			log.debug("addFileToBatchFolder : filePath " + filePath);
			File document = new File(filePath);
			log.debug("addFileToBatchFolder : parent file : " + document.getParentFile());
			document.getParentFile().mkdirs();
			document.createNewFile();

			FileOutputStream fos = new FileOutputStream(document);

			byte[] bytes = new byte[fis.available()];
			log.debug("addFileToBatchFolder : bytes length : " + bytes.length);
			int length;
			while ((length = fis.read(bytes)) > 0) {
				fos.write(bytes, 0, length);
			}
			
			fos.close();
			log.debug("Closing addFileToBatchFolder method.");
		}
	}

	/**
	 * This method is used to add supplied byte array to the BatchFolder
	 * 
	 * @param file
	 * @param filePath
	 * @throws IOException
	 */
	public static void addFileToBatchFolder(byte[] file, String filePath) throws IOException {
		
		File document = new File(filePath);
		document.getParentFile().mkdirs();
		document.createNewFile();
		FileOutputStream fos = new FileOutputStream(document);
		if(file != null){
			fos.write(file, 0, file.length);
		}
		fos.close();
	}

	/**
	 * This method is used to add a empty file to BatchFolder
	 * 
	 * @param filePath
	 * @throws IOException
	 */
	public static void addEmptyFileToBatchFolder(String filePath) throws IOException {
		File document = new File(filePath);
		document.getParentFile().mkdirs();
		document.createNewFile();
	}
}
