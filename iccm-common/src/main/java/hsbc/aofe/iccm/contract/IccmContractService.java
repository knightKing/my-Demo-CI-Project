package hsbc.aofe.iccm.contract;

import java.util.List;

public interface IccmContractService {

    void saveIccm(String event, String arn, String channel, String uid, List<String> remarks);

}
