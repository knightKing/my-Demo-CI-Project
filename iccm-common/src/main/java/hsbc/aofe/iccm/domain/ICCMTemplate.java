package hsbc.aofe.iccm.domain;

public enum ICCMTemplate {

    SENDTOCUSTOMER_EMAIL("LST2.GEN.NGEBREE001"),
    SENDTOCUSTOMER_EMAIL_FFA("LST2.GEN.NGPENDE001"),
    SENDTOCUSTOMER_SMS("LST2.GEN.NGEBRES001"),
    SENDTOCUSTOMER_SMS_FFA("LST2.GEN.NGPENDS001");

    private String templateValue;

    ICCMTemplate(String templateValue) {
        this.templateValue = templateValue;
    }

    public String getTemplateValue() {
        return templateValue;
    }
}
