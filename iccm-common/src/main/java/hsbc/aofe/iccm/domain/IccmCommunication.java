package hsbc.aofe.iccm.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class IccmCommunication implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5991644934838270870L;

    private String arn;
    private Long mobile;
    private String email;
    private String communicationText;
    private LocalDateTime sendWhen;
    private IccmCommunicationStatus status;
    private int retryAttempts;
    private int priority;

}
