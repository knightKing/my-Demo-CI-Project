package hsbc.aofe.iccm.domain;

import lombok.Getter;

@Getter
public enum IccmCommunicationStatus {

	NEW,
	SEND,
	FAIL
		
}
