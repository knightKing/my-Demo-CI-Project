package hsbc.aofe.iccm.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import hsbc.aofe.domain.IccmEvent;
import lombok.Data;

@Data
public class NotificationRequest implements Serializable {

    private static final long serialVersionUID = 5991644934838270870L;
    private IccmEvent Event ;
    private String arn;
    private String idValue;
    private String trackingField;
    private String mobile;
    private String dialCodeMobile;
    private String email;
    private String iccmTemplate;
    private String  salutation;
    private LocalDateTime sendWhen;
    private String status;
    private Long retryAttempts;
    private String priority;
    private String firstName;
    private String middleName;
    private String lastName;
    private String faremarkLine5;
    private String faremarkLine4;
    private String faremarkLine3;
    private String faremarkLine2;
    private String faremarkLine1;
    private String productCard1;
    private String productCard2;
    private String otp;
}
