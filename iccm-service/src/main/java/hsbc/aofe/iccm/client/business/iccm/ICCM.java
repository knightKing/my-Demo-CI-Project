package hsbc.aofe.iccm.client.business.iccm;

import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.bean._322.SendCommunicationMessageRequest;
import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.bean._322.SendCommunicationMessageResponse;
import hsbc.aofe.iccm.client.data.iccm.ICCMClient;

public class ICCM {

    private final ICCMClient client;
    private final ICCMRequestTransformer transformer;

    public ICCM(ICCMClient client, ICCMRequestTransformer transformer) {
        this.client = client;
        this.transformer = transformer;
    }

    public SendCommunicationMessageResponse sendCommunicationMessage(
            SendCommunicationMessageRequest sendCommunicationRequest) {
        return client.sendCommunicationMessage(sendCommunicationRequest);
    }

}
