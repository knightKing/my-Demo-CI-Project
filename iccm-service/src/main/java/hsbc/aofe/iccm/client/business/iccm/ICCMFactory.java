package hsbc.aofe.iccm.client.business.iccm;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import hsbc.aofe.iccm.client.data.iccm.ICCMClient;
import hsbc.aofe.iccm.config.ICCMConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class ICCMFactory {


    public static ICCM createICCM(String serviceURl) {
        ICCMClient client = new ICCMClient(toURL(serviceURl));
        ICCMRequestTransformer transformer = new ICCMRequestTransformerImpl();
        return new ICCM(client, transformer);
    }

    private static URL toURL(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

}
