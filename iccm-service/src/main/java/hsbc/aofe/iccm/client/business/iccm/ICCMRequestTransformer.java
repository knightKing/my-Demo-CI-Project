package hsbc.aofe.iccm.client.business.iccm;
import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.bean._322.SendCommunicationMessageRequest;

public interface ICCMRequestTransformer {

	public SendCommunicationMessageRequest toSendCommunicationMessageRequest(ICCMRequestEntry requestEntry);

}
