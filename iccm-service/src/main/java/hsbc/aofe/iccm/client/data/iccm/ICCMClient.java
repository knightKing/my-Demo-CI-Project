package hsbc.aofe.iccm.client.data.iccm;

import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Function;

import javax.xml.namespace.QName;

import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.bean._322.SendCommunicationMessageRequest;
import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.bean._322.SendCommunicationMessageResponse;
import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.service._322.ICCMCommService322;
import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.service._322.ICCMCommService322_Service;

@Slf4j
public class ICCMClient {


    private final URL endpointURL;
    private final Queue<ICCMCommService322> ports;

    public ICCMClient(URL endpointURL) {
        this.endpointURL = endpointURL;
        this.ports = new ConcurrentLinkedQueue<>();
    }

    public SendCommunicationMessageResponse sendCommunicationMessage(
            SendCommunicationMessageRequest request) {
        return request(p -> p.sendCommunicationMessage(request));
    }

    private ICCMCommService322 takePort() {
        ICCMCommService322 port = ports.poll();
        return port != null ? port : createPort(endpointURL);
    }

    private void putPort(ICCMCommService322 port) {
        ports.add(port);
    }

    private <R> R request(Function<ICCMCommService322, R> function) {
        ICCMCommService322 port = takePort();
        try {
            return function.apply(port);
        } finally {
            putPort(port);
        }
    }

    private static ICCMCommService322 createPort(URL endpointURL) {
        long time = System.currentTimeMillis();
        try {
            ICCMCommService322_Service iccService = new ICCMCommService322_Service(
                    endpointURL,
                    new QName(
                            "http://group.hsbc.com/bussvc/genericcomnmgmt/custcomnmgmt/service/322",
                            "ICCMCommService322"));
            return iccService.getICCMCommServicePort();
        } finally {
            log.debug("time to create ads port: "
                    + (System.currentTimeMillis() - time));
        }
    }

}
