package hsbc.aofe.iccm.config;

import javax.validation.Valid;
import javax.validation.constraints.Digits;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;


@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "hsbc.iccm-config")
@Validated
public class ICCMConfigProperties {

    /* service end point url.*/
    @NotEmpty
    private String serviceUrl;
    
    @NotEmpty
    private String enableProduction;

    @Valid
    private final IccmRequestConfig iccmRequestConfig = new IccmRequestConfig();

    @Valid
    private final IccmSchedularConfig iccmSchedularConfig = new IccmSchedularConfig();


    @Getter
    @Setter
    public static class IccmRequestConfig {

        @NotEmpty
        private String groupMemberCode;
        @NotBlank
        private String countryShortCode;
        @NotBlank
        private String communicationDeliveryModeCode;
        @NotBlank
        private String appID;
        @NotBlank
        private String languagePreferenceCode;
        @NotBlank
        private String commFlowIndicator;
        @NotBlank
        private String lineOfBusinessCode;
        @NotBlank
        private String segmentIdentifierCustomerGroup;
        @NotBlank
        private String otpTemplate;
    }

    @Getter
    @Setter
    public static class IccmSchedularConfig {

        @NotBlank
        private String firstSchedular;
        @NotBlank
        private String secondSchedular;
        @Digits(integer = 3, fraction = 0)
        private Long firstFollowUp;
        @Digits(integer = 3, fraction = 0)
        private Long secondFollowUp;
        @Digits(integer = 3, fraction = 0)
        private Long thirdFollowUp;
        @Digits(integer = 3, fraction = 0)
        private Long cancellationFollowUp;
        @Digits(integer = 3, fraction = 0)
        private Long roadshowDelay;
    }

}
