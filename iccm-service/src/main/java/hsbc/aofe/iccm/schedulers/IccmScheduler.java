package hsbc.aofe.iccm.schedulers;

import hsbc.aofe.domain.IccmEvent;
import hsbc.aofe.iccm.domain.NotificationRequest;
import hsbc.aofe.iccm.service.Notification;
import hsbc.aofe.repository.common.IccmRepositoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class IccmScheduler {

	@Autowired
	@Qualifier("emailService")
	private Notification emailNotification;
	
	@Autowired
	@Qualifier("smsService")
	private Notification smsNotification;
	
	private IccmRepositoryService iccmRepositoryService;


	@Autowired
	public IccmScheduler(IccmRepositoryService iccmRepositoryService) {
		this.iccmRepositoryService = iccmRepositoryService;
	}

	@Scheduled(cron = "${hsbc.iccm-config.iccm-schedular-config.firstSchedular}")
	public void setDetailsInIccmEventTable() {
		log.debug("schedular1");
		iccmRepositoryService.saveFollowUpApplications();
	}
	
	@Scheduled(cron = "${hsbc.iccm-config.iccm-schedular-config.secondSchedular}")
	public void sendMessage() {
		log.debug("schedular2");
		List<NotificationRequest> notificationRequests = iccmRepositoryService.getFollowUpApplications();
		for(NotificationRequest notificationRequest : notificationRequests) {
			try {
				if(IccmEvent.CANCELLATIONEMAIL.equals(notificationRequest.getEvent())
						|| IccmEvent.FOLLOWUPEMAIL.equals(notificationRequest.getEvent())
						|| IccmEvent.SENDTOCUSTOMEREMAIL.equals(notificationRequest.getEvent())) {
					emailNotification.processRequest(notificationRequest);
				} else {
					smsNotification.processRequest(notificationRequest);
				}
				iccmRepositoryService.updateSuccessIccm(notificationRequest);
			} catch (Exception e) {
				log.error("ICCM EMAIL/SMS service error for {}.", notificationRequest);
				iccmRepositoryService.updateFailedIccm(notificationRequest);
			}
		}
	}
}
