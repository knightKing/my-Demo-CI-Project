package hsbc.aofe.iccm.service;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.ChannelType;
import hsbc.aofe.exception.IccmRequestTimedOutException;
import hsbc.aofe.iccm.config.ICCMConfigProperties;
import hsbc.aofe.iccm.contract.IccmContractService;
import hsbc.aofe.iccm.domain.NotificationRequest;
import hsbc.aofe.repository.common.IccmRepositoryService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IccmContractServiceImpl implements IccmContractService {
	
	private IccmRepositoryService iccmRepositoryService;
	private static final String OTP = "OTP";
	private static final String EMAIL = "EMAIL";
	
	@Autowired
	private ICCMConfigProperties iccmConfigProperties;
	
	@Autowired
    @Qualifier("emailService")
    private Notification emailNotification;
    @Autowired
    @Qualifier("smsService")
    private Notification smsNotification;
    @Autowired
    @Qualifier("otpService")
    private Notification otpNotification;
	
	@Autowired
	public IccmContractServiceImpl(IccmRepositoryService iccmRepositoryService) {
		this.iccmRepositoryService = iccmRepositoryService;
	}

	@Override
	public void saveIccm(String event, String arn, String channel, String uidOtp, List<String> remarks) {
		
		if (ChannelType.S.getChannel().equalsIgnoreCase(channel)) {
			NotificationRequest notificationRequest = new NotificationRequest();
			notificationRequest.setTrackingField(arn + Instant.now().toEpochMilli());
			iccmRepositoryService.saveIccm(event, arn, channel, uidOtp, remarks, notificationRequest);
		} else {
			NotificationRequest notificationRequest = iccmRepositoryService.getIccmNotification(event, arn, channel, uidOtp, remarks);
			try {
				if (event.contains(OTP)) {
					notificationRequest.setOtp(uidOtp);
					otpNotification.processRequest(notificationRequest);
				} else if (event.contains(EMAIL)) {
					emailNotification.processRequest(notificationRequest);
				} else {
					smsNotification.processRequest(notificationRequest);
				}
				iccmRepositoryService.saveIccm(event, arn, channel, uidOtp, remarks, notificationRequest);
			} catch (Exception e) {
				if ("true".equalsIgnoreCase(iccmConfigProperties.getEnableProduction())) {
					throw new IccmRequestTimedOutException("Iccm Request Timed Out.");
				}
				log.error("ICCM EMAIL/SMS service error for {}.", arn, notificationRequest);
				log.error("Iccm Error Log", e);
			}
			
		}
	}
}
