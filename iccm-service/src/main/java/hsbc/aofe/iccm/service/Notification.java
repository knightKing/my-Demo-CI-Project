package hsbc.aofe.iccm.service;

import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.bean._322.SendCommunicationMessageResponse;
import hsbc.aofe.iccm.domain.NotificationRequest;

/*****************************************
 * Created by sahil.verma on 8/4/2017.
 *****************************************
 */
public interface Notification {

    SendCommunicationMessageResponse processRequest(NotificationRequest request);
}
