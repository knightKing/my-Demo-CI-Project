package hsbc.aofe.iccm.service;


import java.util.List;

import com.hsbc.group.bussvc.genericcomnmgmt.custcomnmgmt.bean._322.*;
import hsbc.aofe.iccm.client.business.iccm.ICCM;
import hsbc.aofe.iccm.client.business.iccm.ICCMFactory;
import hsbc.aofe.iccm.domain.NotificationRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hsbc.aofe.iccm.config.ICCMConfigProperties;


@Component
@Slf4j
public class SmsService implements Notification {

    private static final String SMS = "SMS";
    private static final String TEXT = "TEXT";
    private static final String ATTACHMENT_INDICATOR = "0";
    private static final String SEGMENT_IDENTIFIER_CUSTOMER_GROUP = "1000";


    private ICCMConfigProperties iccmConfigProperties;

    @Autowired
    public SmsService(ICCMConfigProperties iccmConfigProperties) {
        this.iccmConfigProperties = iccmConfigProperties;
    }

    @Override
    public SendCommunicationMessageResponse processRequest(NotificationRequest request) {

        CommunicationID communicationId = populateCommunicationId(request.getIdValue());
        CommunicationData communicationData = populateCommunicationData(request);
        CommunicationDestination communicationDestination = populateCommunicationDestination(request);
        CommunicationScheduling communicationScheduling = populateCommunicationScheduling(request);

        // request object population
        SendCommunicationMessageRequest messageRequest = new SendCommunicationMessageRequest();
        messageRequest.setCommunicationID(communicationId);
        messageRequest.setCommunicationData(communicationData);
        // populate communicationDestinations
        List<CommunicationDestination> communicationDestinations = messageRequest.getCommunicationDestination();
        communicationDestinations.add(communicationDestination);
        messageRequest.setCommunicationScheduling(communicationScheduling);

        // service call
        ICCM iccm = ICCMFactory.createICCM(iccmConfigProperties.getServiceUrl());
        log.info("ICCM Request message for SMS: {}", toString(messageRequest));
        SendCommunicationMessageResponse sendCommunicationMessageResponse = iccm.sendCommunicationMessage(messageRequest);
        log.info("ICCM Response message for SMS: {}", toString(sendCommunicationMessageResponse));

        Long responseCode = sendCommunicationMessageResponse.getResponseDetails() != null ? sendCommunicationMessageResponse.getResponseDetails().getResponseCode() : null;
        if (responseCode != null && responseCode != 0) {
            log.error("ICCM Request message for SMS: {}", toString(messageRequest));
            log.error("ICCM Response message for SMS: {}", toString(sendCommunicationMessageResponse));
            log.error("Error: Response code received from ICCM for tracing-field {},  Response code from ICCM: {}", request.getTrackingField(), responseCode);
            log.error("Error: Reason code received from ICCM for tracing-field {},  Reason code from ICCM: {}", request.getTrackingField(), toString(sendCommunicationMessageResponse.getReasonCode()));
        }

        return sendCommunicationMessageResponse;
    }

    private CommunicationScheduling populateCommunicationScheduling(NotificationRequest request) {

        CommunicationScheduling communicationScheduling = new CommunicationScheduling();
        communicationScheduling.setTrackingField(request.getTrackingField());
        return communicationScheduling;
    }

    private CommunicationData populateCommunicationData(NotificationRequest request) {

        CommunicationData communicationData = new CommunicationData();
        // values from properties file
        communicationData.setCommunicationDeliveryModeCode(iccmConfigProperties.getIccmRequestConfig().getCommunicationDeliveryModeCode());
        communicationData.setAppID(iccmConfigProperties.getIccmRequestConfig().getAppID());
        communicationData.setLanguagePreferenceCode(iccmConfigProperties.getIccmRequestConfig().getLanguagePreferenceCode());
        communicationData.setCommFlowIndicator(iccmConfigProperties.getIccmRequestConfig().getCommFlowIndicator());
        communicationData.setLineOfBusinessCode(iccmConfigProperties.getIccmRequestConfig().getLineOfBusinessCode());
        communicationData.setChannelCommunicationCode(SMS);
        communicationData.setDocumentFormatID(TEXT);
        communicationData.setAttachmentsIndicator(ATTACHMENT_INDICATOR);
        communicationData.setSegmentIdentifierCustomerGroup(SEGMENT_IDENTIFIER_CUSTOMER_GROUP);
        // template value
        communicationData.setTemplateDescription(request.getIccmTemplate());
        return communicationData;

    }


    private CommunicationID populateCommunicationId(String customerIdentificationNumber) {

        CommunicationID communicationID = new CommunicationID();
        communicationID.setGroupMemberCode(iccmConfigProperties.getIccmRequestConfig().getGroupMemberCode());
        communicationID.setCountryShortCode(iccmConfigProperties.getIccmRequestConfig().getCountryShortCode());
        communicationID.setCustomerIdentificationNumber(customerIdentificationNumber);
        return communicationID;
    }


    private CommunicationDestination populateCommunicationDestination(NotificationRequest request) {

        CommunicationDestination communicationDestination = new CommunicationDestination();
        communicationDestination.setTelephoneMobileNumber("+" + request.getDialCodeMobile() + request.getMobile());
        return communicationDestination;
    }

    public String toString(Object o) {
        if (o == null)
            return null;
        return new ReflectionToStringBuilder(o, new RecursiveToStringStyle()).toString();
    }

}
