package hsbc.aofe.iccm.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

@Slf4j
public class PropertyUtils {


    public static final String getProperty(String propertyName, Properties properties) {
        String value = StringUtils.trimToNull(properties.getProperty(propertyName));

        if (value == null) {
            throw new IllegalArgumentException("undefined property: " + propertyName);

        }

        return value;
    }

    public static final int getPropertyInt(String propertyName, Properties properties) {
        String value = getProperty(propertyName, properties);
        return Integer.parseInt(value);
    }

    public static Properties toProperties(File file) {
        try (InputStream in = new FileInputStream(file)) {
            Properties properties = new Properties();
            properties.load(in);

            log(properties);

            return properties;
        } catch (IOException e) {
            throw new IllegalArgumentException("error parsing properties : " + e.getMessage(), e);
        }
    }

    private static void log(Properties properties) {
        if (!log.isTraceEnabled()) {
            return;
        }

        for (Map.Entry<Object, Object> e : properties.entrySet()) {
            log.trace(e.getKey() + " = " + e.getValue());
        }
    }
}
