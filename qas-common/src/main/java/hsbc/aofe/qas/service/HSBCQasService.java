package hsbc.aofe.qas.service;

/**
 * Created by somebody on 5/13/2017.
 */
public interface HSBCQasService {
    /**
     * This function returns all the address suggestions after retrieving from QAS service
     *
     * @param searchForPinCode
     * @return
     * @throws Exception
     */
    public String getAddressFor(String searchForPinCode) throws Exception;
}
