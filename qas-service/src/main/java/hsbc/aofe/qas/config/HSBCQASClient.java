package hsbc.aofe.qas.config;

import hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.IQASCapture;
import hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.ProxySetting;
import hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.QASCapture;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by sahil.verma on 9/3/2017.
 */

@Slf4j
@Scope(value = "singleton")
@Component
public class HSBCQASClient {

    private IQASCapture qasCapture;

    @Autowired
    QASConfigProperties qasConfigProperties;

    @Autowired
    ProxyConfigProperties proxyConfigProperties;

    @Bean
    public IQASCapture createClient() {
        log.info("STARTED: QAS client object creation");
        try {
            if (proxyConfigProperties.getEnableProxy().equalsIgnoreCase("true")) {
                ProxySetting proxySetting = new ProxySetting();
                proxySetting.setProxyHost(proxyConfigProperties.getProxyHost());
                proxySetting.setProxyPort(proxyConfigProperties.getProxyPort());
                proxySetting.setProxyUser(proxyConfigProperties.getProxyUsername());
                proxySetting.setProxyPassword(proxyConfigProperties.getProxyPassword());
                this.qasCapture = new QASCapture(qasConfigProperties.getServiceEndpoint(), qasConfigProperties.getLicenseInfo().getUsername(),
                        qasConfigProperties.getLicenseInfo().getPassword(), qasConfigProperties.getLicenseInfo().getAuthToken(), proxySetting);
            } else {
                this.qasCapture = new QASCapture(qasConfigProperties.getServiceEndpoint(), qasConfigProperties.getLicenseInfo().getUsername(),
                        qasConfigProperties.getLicenseInfo().getPassword(), qasConfigProperties.getLicenseInfo().getAuthToken());
            }
            log.info("FINISHED: QAS client created successfully");
        } catch (Exception e) {
            log.error("Error creating bean for QAS: {}", e);
        }
        return qasCapture;
    }

    public IQASCapture getQasCapture() {
        return qasCapture;
    }

}
