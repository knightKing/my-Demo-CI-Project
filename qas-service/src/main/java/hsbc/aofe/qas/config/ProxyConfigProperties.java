package hsbc.aofe.qas.config;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/*****************************************
 * Created by sahil.verma on 5/26/2017.
 *****************************************
 */
@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "hsbc.proxy-configurations")
@Validated
public class ProxyConfigProperties {

    private String enableProxy;
    private String proxyHost;
    private String proxyPort;
    private String proxyUsername;
    private String proxyPassword;

    @PostConstruct
    public void init() {
        // initializing serviceEndpoint with default value
        if (StringUtils.isEmpty(enableProxy))
            enableProxy = "false";

        if (enableProxy.equalsIgnoreCase("true")) {
            if (StringUtils.isEmpty(proxyHost) || StringUtils.isEmpty(proxyPort)) {
                throw new RuntimeException("Please check proxy configuration specified in application.yml");
            }
        }
    }

}
