package hsbc.aofe.qas.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/*****************************************
 * Created by sahil.verma on 5/26/2017.
 *****************************************
 */
@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "hsbc.qas")
@Validated
public class QASConfigProperties {

    /* service end point url. */
    private String serviceEndpoint; // default = https://ws3.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx

    @Valid
    private final LicenseInfo licenseInfo = new LicenseInfo();

    /* this contains search configuration to customize the search. */
    @Valid
    private final SearchCofigurations searchCofigurations = new SearchCofigurations();

    @Getter
    @Setter
    public static class LicenseInfo {
        @NotEmpty
        private String authToken;
        private String username;
        private String password;
    }

    @Getter
    @Setter
    public static class SearchCofigurations {
        @NotEmpty
        private String countryId;
        @NotEmpty
        private String engineIntensity;
        @NotNull
        private Integer requestTimeout;
        @NotEmpty
        private String responseLayout;
    }

    @PostConstruct
    public void init() {
        // initializing serviceEndpoint with default value
        if (StringUtils.isEmpty(serviceEndpoint))
            serviceEndpoint = "https://ws3.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx";
    }

}
