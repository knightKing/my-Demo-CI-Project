package hsbc.aofe.qas.domain;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.EngineEnumType;
import lombok.Builder;
import lombok.Data;

/**
 * Created by somebody on 5/14/2017.
 */
@Data
@Builder
public class SearchBody {

    /**
     * This defines the data mapping to be used to search against
     */
    String countryId;
    /**
     * The engine type enables you to specify the engine, and any engine options, that will be used to perform the search.
     * <p>
     * The available values are:
     * (Singleline, Typedown, Verification,Intuitive, Keyfinder)
     */
    @Builder.Default
    String engine = EngineEnumType.SINGLELINE.value();

    /**
     * This defines whether the search results will be 'flattened' to a single picklist of deliverable results,
     * or output as (potentially multiple) hierarchical picklists of results that can be stepped into.
     */
    @Builder.Default
    Boolean flatten = true;

    /**
     * This defines how hard the search engine will work to obtain a match.
     * Higher intensity values may yield more results than lower intensity values,
     * but will also result in longer search times.
     * <p>
     * Exact. This does not allow many mistakes in the search term, but is the fastest.
     * Close. This allows some mistakes in the search term, and is the default setting.
     * Extensive. This allows many mistakes in the search term, and will take the longest.
     */
    String intensity;

    /**
     * The prompt set depends on the search engine used
     * <p>
     * A prompt set must be used if you are using a Partner Sourced dataset with the Single Line engine.
     */
    @Builder.Default
    String promptset = "";

    /**
     * This defines the threshold that is used to decide whether results will be returned in the picklist,
     * or whether an informational picklist result will be returned, requiring further refinement.
     * This element is only relevant when using hierarchical mode. The standard setting is 25 items.
     * minInclusive value=5, maxInclusive value=750
     */
    @Builder.Default
    Integer threshold = 700;

    /**
     * This defines the time threshold in milliseconds after which a search will abort.
     * A value of 0 signifies that searches will not timeout.
     * minInclusive value=0, maxInclusive value=600000
     */
    Integer timeout;

    /**
     * For Verification searches this element is required,
     * <p>
     * since the result may be returned directly as a final formatted address.
     * This means that you have to specify which layout will be used to format the results.
     * For searches with other engines, this is an optional element.
     * It is used with the AUE dataset to return picklists using the correct Form of address for the layout.
     */
    String layout;

    /**
     * This defines the search string that will be submitted to the server.
     * Different address information should be entered, depending on which search engine is being used.
     */
    String search;

    @Builder.Default
    Boolean formattedAddressInPicklist = true;
    @Builder.Default
    String requestTag = " ";

    /**
     * The localisation attribute is not used for any of the operations.
     */
    @Builder.Default
    String localisation = "";

}
