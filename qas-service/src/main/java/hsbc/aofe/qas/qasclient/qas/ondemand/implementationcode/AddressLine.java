	package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.AddressLineType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.LineContentType;

import java.io.Serializable;

    /**
     * Class to process AddressLineType object from web service
     * @author kahchingn
     * @version 1.0
     */
    final class AddressLine implements Serializable, IJSONSerializable {

        private static final long serialVersionUID = -8786928260845523987L;
        public static final String NONE;
        public static final String ADDRESS;
        public static final String NAME;
        public static final String ANCILLARY;
        public static final String DATAPLUS;
        private String m_Label;
        private String m_Line;
        private String m_LineType;
        private DataplusGroup[] m_DataplusGroups;
        private boolean m_IsTruncated;
        private boolean m_IsOverflow;

        static {
            NONE = LineContentType.NONE.value();
            ADDRESS = LineContentType.ADDRESS.value();
            NAME = LineContentType.NAME.value();
            ANCILLARY = LineContentType.ANCILLARY.value();
            DATAPLUS = LineContentType.DATA_PLUS.value();
        }

        /**
         * Default Constructor
         */
        public AddressLine() {
        }

        /**
         * Constructor take AddressLineType object to populate On Demand result
         * @param t
         */
        public AddressLine(AddressLineType t) {
            m_Label = t.getLabel();
            m_Line = t.getLine();
            m_LineType = t.getLineContent().value();
            m_IsTruncated = t.isTruncated();
            m_IsOverflow = t.isOverflow();
            DataplusGroup tGroup = null;
            if (t.getDataplusGroup() != null) {
                m_DataplusGroups = new DataplusGroup[t.getDataplusGroup().size()];
                for (int i = 0; i < t.getDataplusGroup().size(); i++) {
                    tGroup = new DataplusGroup(t.getDataplusGroup().get(i));

                    m_DataplusGroups[i] = tGroup;
                }
            }

        }

        /**
         * getter for m_DataplusGroups
         * @return m_DataplusGroups
         */
        public DataplusGroup[] getDataplusGroup() {
            return m_DataplusGroups;
        }

        /**
         * getter for m_Label
         * @return m_Label
         */
        public String getLabel() {
            return m_Label;
        }

        /**
         * getter for m_Line
         * @return m_Line
         */
        public String getLine() {
            return m_Line;
        }

        /**
         * getter for m_LineType
         * @return m_LineType
         */
        public String getLineType() {
            return m_LineType;
        }

        /**
         * getter for m_IsTruncated
         * @return m_IsTruncated
         */
        public boolean isTruncated() {
            return m_IsTruncated;
        }

        /**
         * getter for m_IsOverflow
         * @return m_IsOverflow
         */
        public boolean isOverflow() {
            return m_IsOverflow;
        }

        /**
         * Format the result in JSON format
         * @return jsonResult
         */
        @Override
        public String toJSON() {
            StringBuffer jsonResult = new StringBuffer();
            String separator = ",";
            jsonResult.append("{");

            jsonResult.append(String.format("\"Label\":\"%s\"", m_Label)).append(
                    separator);
            jsonResult.append(String.format("\"Line\":\"%s\"", m_Line)).append(
                    separator);
            jsonResult.append("\"DataplusGroups\":");
            if (m_DataplusGroups != null) {
                jsonResult.append("[");

                for (int i = 0; i < m_DataplusGroups.length; i++) {
                    jsonResult.append(m_DataplusGroups[i].toJSON());
                    if (i == (m_DataplusGroups.length -1)) {
                        break;
                    }
                    jsonResult.append(separator);
                }
                jsonResult.append("]").append(separator);
            } else {
                jsonResult.append("null").append(separator);
            }
            jsonResult.append(String.format("\"LineType\":\"%s\"", m_LineType)).append(
                    separator);
            jsonResult.append(String.format("\"IsTruncated\":%s", m_IsTruncated))
                    .append(separator);
            jsonResult.append(String.format("\"IsOverFlow\":%s", m_IsOverflow));

            jsonResult.append("}");

            return jsonResult.toString();
        }

    }
