package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QASearchOk;

import java.io.Serializable;

/**
 * Class to process QASearchOk object from web service
 * @author kahchingn
 * @version 1.0
 */
final class CanSearch implements Serializable, IJSONSerializable {

	private static final long serialVersionUID = 3745139144883226868L;
	private boolean m_Okay;
	private int m_QasErrorNumber;
	private String m_Message;

	/**
	 * Default constructor
	 */
	public CanSearch() {
	}
	
	/**
	 * Constructor take QASearchOk object to populate On Demand result
	 * @param cs
	 */
	public CanSearch(QASearchOk cs) {
		m_Okay = false;
		m_QasErrorNumber = 0;
		m_Message = null;
		m_Okay = cs.isIsOk();
		if (cs.getErrorCode() != null) {
			m_QasErrorNumber = Integer.parseInt(cs.getErrorCode());
		}
		if (cs.getErrorMessage() != null) {
			m_Message = (new StringBuilder(String.valueOf(cs.getErrorMessage())))
					.append(" [").append(m_QasErrorNumber).append("]")
					.toString();
		}
	}

	/**
	 * getter for m_Okay
	 * @return m_Okay
	 */
	public boolean isOk() {
		return m_Okay;
	}

	/**
	 * getter for m_Message
	 * @return m_Message
	 */
	public String getMessage() {
		return m_Message;
	}

	/**
	 * Format the result in JSON format
	 * @result jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
		String separator = ",";
		jsonResult.append("{");
		jsonResult.append(String.format("\"IsOk\":%s", this.m_Okay)).append(separator);
		jsonResult.append(String.format("\"Error\":%s", this.m_QasErrorNumber)).append(separator);
		jsonResult.append("\"ErrorMessage\":");
		if (m_Message != null) {
			jsonResult.append(String.format("\"%s\"", this.m_Message));
		} else {
			jsonResult.append("null");
		}
		
		jsonResult.append("}");
		
		return jsonResult.toString();
	}
}
