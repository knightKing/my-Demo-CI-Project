package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.DataplusGroupType;

/**
 * Class to process DataplusGroupType object from web service
 * @author kahchingn
 *
 */
final class DataplusGroup implements IJSONSerializable {

	private String m_GroupName;
	private String[] m_Items;
	
	/**
	 * Default constructor
	 */
	public DataplusGroup()
    {
    }
	
	/**
	 * Constructor take DataplusGroupType object to populate On Demand result
	 * @param t
	 */
	public DataplusGroup(DataplusGroupType t) {
		m_GroupName = t.getGroupName();
		m_Items = (String[])t.getDataplusGroupItem().toArray();
	}
	
	/**
	 * getter for m_GroupName
	 * @return m_GroupName
	 */
	public String getGroupName() {
		return m_GroupName;
	}

	/**
	 * setter for m_GroupName
	 * @param m_GroupName
	 */
	public void setGroupName(String m_GroupName) {
		this.m_GroupName = m_GroupName;
	}

	/**
	 * getter for m_Items
	 * @return m_Items
	 */
	public String[] getItems() {
		return m_Items;
	}

	/**
	 * setter for m_Items
	 * @param m_Items
	 */
	public void setItems(String[] m_Items) {
		this.m_Items = m_Items;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
		String separator = ",";
		jsonResult.append("{");
		
		jsonResult.append(String.format("\"Name\":\"%s\"", m_GroupName)).append(separator);
		jsonResult.append("\"Items\":");
		if (m_Items != null) {
			jsonResult.append("[");
			for (int i = 0; i < m_Items.length; i++) {
				jsonResult.append(String.format("\"%s\"", m_Items[i]));
				if (i == (m_Items.length - 1)) {
					break;
				}
				jsonResult.append(separator);
			}
			jsonResult.append("]");
		} else {
			jsonResult.append("null");
		}
		jsonResult.append("}");
		
		return jsonResult.toString();
	}

}
