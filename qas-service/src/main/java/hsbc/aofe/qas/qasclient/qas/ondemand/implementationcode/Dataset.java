package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QADataSet;

import java.io.Serializable;

/**
 * Class to process QADataSet object from web service
 * @author kahchingn
 * @version 1.0
 */
final class Dataset implements Serializable, Comparable<Object>, IJSONSerializable {

	private static final long serialVersionUID = -116534622142279640L;
	private String m_ID = null;
	private String m_Name = null;
	
	/**
	 * Default constructor
	 */
	public Dataset() {
	}

	/**
	 * Constructor take QADataSet object to populate On Demand result
	 * @param dataset
	 */
	public Dataset(QADataSet dataset) {
		m_ID = dataset.getID();
		m_Name = dataset.getName();
	}

	/**
	 * compare if the param object is a Dataset object
	 * @param obj
	 */
	public int compareTo(Object obj) {

		if (obj instanceof Dataset) {
			Dataset dset = (Dataset) obj;
			
			return getName().compareTo(dset.getName());
		} else {
			// Object is not a Dataset
			return -1;
		}
	}
	
	/**
	 * Create a Dataset[] object with QADataSet[] object
	 * @param aDataSets
	 * @return Dataset[]
	 */
	public static Dataset[] createArray(QADataSet[] aDataSets) {
		
		Dataset[] aResults = null;
		
		if (aDataSets != null) {
			int iSize = aDataSets.length;
			if (iSize > 0) {
				aResults = new Dataset[iSize];
				for (int i = 0; i < iSize; i++)
					aResults[i] = new Dataset(aDataSets[i]);
			}
		}
		return aResults;
	}
	
	/**
	 * get the Dataset from the Dataset[], if it's ID is matched with sDataID
	 * @param aDatasets
	 * @param sDataID
	 * @return Dataset
	 */
	public static Dataset findByID(Dataset[] aDatasets, String sDataID){
		for (int i = 0; i < aDatasets.length; i++) {
			if (aDatasets[i].getID().equalsIgnoreCase(sDataID)) {
				return aDatasets[i];
			}
		}
		return null;
	}

	/**
	 * getter for m_ID
	 * @return m_ID
	 */
	public String getID() {
		return m_ID;
	}

	/**
	 * setter for m_ID
	 * @param m_ID
	 */
	public void setID(String m_ID) {
		this.m_ID = m_ID;
	}

	/**
	 * getter for m_Name
	 * @return m_Name
	 */
	public String getName() {
		return m_Name;
	}

	/**
	 * setter for m_Name
	 * @param m_sName
	 */
	public void setName(String m_sName) {
		this.m_Name = m_sName;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append(String.format("\"ID\":\"%s\"", m_ID)).append(separator);
        jsonResult.append(String.format("\"Name\":\"%s\"", m_Name));

        jsonResult.append("}");

        return jsonResult.toString();
	}

}
