package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QAExampleAddress;

import java.io.Serializable;

/**
 * Class to process QAExampleAddress object from web service
 * @author kahchingn
 * @version 1.0
 */
final class ExampleAddress implements Serializable, IJSONSerializable {

	private static final long serialVersionUID = 5846359215107528360L;
	private String m_Comment = null;
	private FormattedAddress m_Address;
	
	/**
	 * Default constructor
	 */
	public ExampleAddress()
    {
    }
	
	/**
	 * Constructor take QAExampleAddress object to populate On Demand result
	 * @param add
	 */
	public ExampleAddress(QAExampleAddress add) {
		m_Comment = add.getComment();
		m_Address = new FormattedAddress(add.getAddress());
	}
	
	/**
	 * Create a ExampleAddress[] object with QAExampleAddress[] object
	 * @param aAddress
	 * @return
	 */
	public static ExampleAddress[] createArray(QAExampleAddress[] aAddress) {
		ExampleAddress[] aResults = null;
		if (aAddress != null) {
			int iSize = aAddress.length;
			if (iSize > 0) {
				aResults = new ExampleAddress[iSize];
				for (int i = 0; i < iSize; i++)
					aResults[i] = new ExampleAddress(aAddress[i]);
			}
		}
		
		return aResults;
	}
	
	/**
	 * getter for m_Comment
	 * @return
	 */
	public String getComment() {
		return m_Comment;
	}
	
	/**
	 * getter for m_Address.AddressLines
	 * @return m_Address.getAddressLines()
	 */
	public AddressLine[] getAddressLines() {
		return m_Address.getAddressLines();
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append("\"Address\":");
        if (m_Address != null)
        {
            jsonResult.append(m_Address.toJSON());
            jsonResult.append(separator);
        }
        else
        {
            jsonResult.append("null").append(separator);
        }
        jsonResult.append(String.format("\"Comment\":\"%s\"", m_Comment));
        jsonResult.append("}");

        return jsonResult.toString();
	}
}
