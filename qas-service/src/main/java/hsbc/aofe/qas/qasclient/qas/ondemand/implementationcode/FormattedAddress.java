package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.AddressLineType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.DPVStatusType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QAAddressType;

import java.util.List;

/**
 * Class to process QAAddressType object from web service
 * @author kahchingn
 * @version 1.0
 */
final class FormattedAddress implements java.io.Serializable, IJSONSerializable {

	private static final long serialVersionUID = 854246493602179027L;
	private AddressLine m_AddressLines[];
	private boolean m_IsOverflow;
	private boolean m_IsTruncated;
	private DPVStatusType m_DPVStatus;

	/**
	 * Default constructor
	 */
	public FormattedAddress()
    {
    }
	
	/**
	 * Constructor take QAAddressType object to populate On Demand result
	 * @param t
	 */
	public FormattedAddress(QAAddressType t) {
		m_IsOverflow = t.isOverflow();
		m_IsTruncated = t.isTruncated();
		m_DPVStatus = t.getDPVStatus();
		List<AddressLineType> aLines = t.getAddressLine();
		int iSize = aLines.size();
		if (iSize > 0) {
			m_AddressLines = new AddressLine[iSize];
			for (int i = 0; i < iSize; i++)
				m_AddressLines[i] = new AddressLine(
						(AddressLineType) aLines.get(i));

		}
	}

	/**
	 * getter for m_AddressLines.length
	 * @return 0 or m_AddressLines.length
	 */
	public int getLength() {
		if (m_AddressLines == null)
			return 0;
		else
			return m_AddressLines.length;
	}

	/**
	 * getter for m_AddressLines
	 * @return m_AddressLines
	 */
	public AddressLine[] getAddressLines() {
		return m_AddressLines;
	}

	/**
	 * setter for m_AddressLine
	 * @param a
	 * @param i
	 */
	public void setAddressLines(AddressLine a, int i) {
		m_AddressLines[i] = a;
	}

	/**
	 * getter for m_AddressLines at a specific index
	 * @param i
	 * @return m_AddressLines[i]
	 */
	public AddressLine getAddressLines(int i) {
		return m_AddressLines[i];
	}

	/**
	 * getter for m_IsOverflow
	 * @return m_IsOverflow
	 */
	public boolean isOverflow() {
		return m_IsOverflow;
	}

	/**
	 * getter for m_IsTruncated
	 * @return m_IsTruncated
	 */
	public boolean isTruncated() {
		return m_IsTruncated;
	}

	/**
	 * getter for m_DPVStatus
	 * @return m_DPVStatus
	 */
	public DPVStatusType getDPVStatus() {
		return m_DPVStatus;
	}
	
	/**
	 * setter for m_DPVStatus
	 * @param dpv
	 */
	public void setDPVStatus(DPVStatusType dpv) {
		m_DPVStatus = dpv;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
		String separator = ",";
		jsonResult.append("{");

		jsonResult.append("\"AddressLines\":");
		if (m_AddressLines != null) {
			jsonResult.append("[");

			for (int i = 0; i < m_AddressLines.length; i++) {
				jsonResult.append(m_AddressLines[i].toJSON());
				if (i == (m_AddressLines.length - 1)) {
					break;
				}
				jsonResult.append(separator);
			}
			jsonResult.append("]").append(separator);
		} else {
			jsonResult.append("null").append(separator);
		}
		jsonResult.append(String.format("\"DPVStatus\":\"%s\"", m_DPVStatus.value())).append(
				separator);
		jsonResult.append(String.format("\"IsTruncated\":%s", m_IsTruncated))
				.append(separator);
		jsonResult.append(String.format("\"IsOverFlow\":%s", m_IsOverflow));

		jsonResult.append("}");

		return jsonResult.toString();
	}
}
