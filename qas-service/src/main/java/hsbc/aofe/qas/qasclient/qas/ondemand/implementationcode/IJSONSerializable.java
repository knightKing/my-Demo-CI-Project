package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

/**
 * Interface to generate JSON format result
 * @author kahchingn
 * @version 1.0
 */
public interface IJSONSerializable {
	String toJSON() throws QasException;

}
