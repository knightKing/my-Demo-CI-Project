package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;


import hsbc.aofe.qas.domain.SearchBody;

/**
 * Interface for QASCapture
 *
 * @author kahchingn
 * @version 1.0
 */
public interface IQASCapture {

    /**
     * Invoke CanSearch
     *
     * @param countryId
     * @param engine
     * @param flatten
     * @param intensity
     * @param promptset
     * @param threshold
     * @param timeout
     * @param layout
     * @param localisation
     * @return an instance of CanSearch
     */
    public CanSearch CanSearch(String countryId, String engine, Boolean flatten, String intensity,
                               String promptset, Integer threshold, Integer timeout, String layout, String localisation) throws QasException;

    /**
     * Invoke Search
     *
     * @param searchBody
     * @return
     * @throws QasException
     */
    public SearchResult Search(SearchBody searchBody) throws QasException;

    /**
     * Invoke Refine
     *
     * @param moniker
     * @param refinement
     * @param layout
     * @param formattedAddressInPicklist
     * @param threshold
     * @param timeout
     * @param requestTag
     * @param localisation
     * @return an instance of Picklist
     */
    public Picklist Refine(String moniker, String refinement, String layout, Boolean formattedAddressInPicklist,
                           Integer threshold, Integer timeout, String requestTag, String localisation) throws QasException;

    /**
     * Invoke GetAddress
     *
     * @param moniker
     * @param layout
     * @param requestTag
     * @param localisation
     * @return an instance of FormattedAddress
     */
    public FormattedAddress GetAddress(String moniker, String layout, String requestTag, String localisation) throws QasException;

    /**
     * Invoke GetData
     *
     * @param localisation
     * @return an array of Dataset
     */
    public Dataset[] GetData(String localisation) throws QasException;

    /**
     * Invoke GetDataMapDetail
     *
     * @param countryId
     * @param localisation
     * @return an array of LicensedSet
     */
    public LicensedSet[] GetDataMapDetail(String countryId, String localisation) throws QasException;

    /**
     * Invoke GetLayouts
     *
     * @param countryId
     * @param localisation
     * @return an array of Layout
     */
    public Layout[] GetLayouts(String countryId, String localisation) throws QasException;

    /**
     * Invoke GetLicenseInfo
     *
     * @param countryId
     * @param layout
     * @param requestTag
     * @param localisation
     * @return an array of ExampleAddress
     */
    public ExampleAddress[] GetExampleAddresses(String countryId, String layout, String requestTag,
                                                String localisation) throws QasException;

    /**
     * Invoke GetLicenseInfo
     *
     * @param localisation
     * @return an array of LicensedSet
     */
    public LicensedSet[] GetLicenseInfo(String localisation) throws QasException;

    /**
     * Invoke GetPromptSet
     *
     * @param countryId
     * @param engine
     * @param flatten
     * @param promptset
     * @param threshold
     * @param timeout
     * @param localisation
     * @return an instance of PromptSet
     */
    public PromptSet GetPromptSet(String countryId, String engine, Boolean flatten,
                                  String promptset, Integer threshold, Integer timeout, String localisation) throws QasException;

    /**
     * Invoke GetSystemInfo
     *
     * @param localisation
     * @return an array of String
     */
    public String[] GetSystemInfo(String localisation) throws QasException;

}
