package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QALayout;

import java.io.Serializable;

/**
 * Class to process QALayout object from web service
 * @author kahchingn
 *
 */
final class Layout implements Serializable, IJSONSerializable {

	private static final long serialVersionUID = -1736822785389633505L;
	private String m_Name = null;
	private String m_Comment = null;
	
	/**
	 * Default constructor
	 */
	public Layout()
    {
    }
	
	/**
	 * Constructor take QALayout object to populate On Demand result
	 * @param layout
	 */
	public Layout(QALayout layout) {
		m_Name = layout.getName();
		m_Comment = layout.getComment();
	}
	
	/**
	 * Create a Layout[] object with QALayout[] object
	 * @param aLayouts
	 * @return
	 */
	public static Layout[] createArray(QALayout[] aLayouts) {
		Layout[] aResults = null;
		if (aLayouts != null) {
			int iSize = aLayouts.length;
			if (iSize > 0) {
				aResults = new Layout[iSize];
				for (int i = 0; i < iSize; i++)
					aResults[i] = new Layout(aLayouts[i]);
			}
		}
		
		return aResults;
	}
	
	/**
	 * get the layout   from the Layout[], if it's name is matched with sLayoutName
	 * @param aLayouts
	 * @param sLayoutName
	 * @return
	 */
	public static Layout findByName(Layout[] aLayouts, String sLayoutName) {
		for (int i = 0; i < aLayouts.length; i++) {
			if (aLayouts[i].getName().equalsIgnoreCase(sLayoutName)) {
				return aLayouts[i];
			}
		}
		return null;
	}

	/**
	 * getter for m_Name
	 * @return m_Name
	 */
	public String getName() {
		return m_Name;
	}

	/**
	 * getter for m_Comment
	 * @return m_Comment
	 */
	public String getComment() {
		return m_Comment;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append(String.format("\"Name\":\"%s\"", m_Name)).append(separator);
        jsonResult.append(String.format("\"Comment\":\"%s\"", m_Comment));

        jsonResult.append("}");

        return jsonResult.toString();
	}
	
	

}
