package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.LicenceWarningLevel;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QADataMapDetail;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QALicenceInfo;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QALicensedSet;

import java.io.Serializable;
import java.util.List;

/**
 * Class to process QALicensedSet object from web service
 * @author kahchingn
 * @version 1.0
 */
final class LicensedSet implements Serializable, IJSONSerializable {

	private static final long serialVersionUID = 6745732264081318747L;
	public static final String NONE;
    public static final String DATA_EXPIRING;
    public static final String LICENCE_EXPIRING;
    public static final String CLICKS_LOW;
    public static final String EVALUATION;
    public static final String NO_CLICKS;
    public static final String DATA_EXPIRED;
    public static final String EVAL_LICENCE_EXPIRED;
    public static final String FULL_LICENCE_EXPIRED;
    public static final String LICENCE_NOT_FOUND;
    public static final String DATA_UNREADABLE;
    public static final String LICENCE_ORDER[];
    private String m_ID;
	private String m_Description;
    private String m_Copyright;
    private String m_Version;
    private String m_BaseCountry;
    private String m_Status;
    private String m_Server;
    private String m_WarningLevel;
    private int m_DaysLeft;
    private int m_DataDaysLeft;
    private int m_LicenceDaysLeft;

    static 
    {
        NONE = LicenceWarningLevel.NONE.value();
        DATA_EXPIRING = LicenceWarningLevel.DATA_EXPIRING.value();
        LICENCE_EXPIRING = LicenceWarningLevel.LICENCE_EXPIRING.value();
        CLICKS_LOW = LicenceWarningLevel.CLICKS_LOW.value();
        EVALUATION = LicenceWarningLevel.EVALUATION.value();
        NO_CLICKS = LicenceWarningLevel.NO_CLICKS.value();
        DATA_EXPIRED = LicenceWarningLevel.DATA_EXPIRED.value();
        EVAL_LICENCE_EXPIRED = LicenceWarningLevel.EVAL_LICENCE_EXPIRED.value();
        FULL_LICENCE_EXPIRED = LicenceWarningLevel.FULL_LICENCE_EXPIRED.value();
        LICENCE_NOT_FOUND = LicenceWarningLevel.LICENCE_NOT_FOUND.value();
        DATA_UNREADABLE = LicenceWarningLevel.DATA_UNREADABLE.value();
        LICENCE_ORDER = (new String[] {
            NONE, DATA_EXPIRING, LICENCE_EXPIRING, CLICKS_LOW, EVALUATION, NO_CLICKS, DATA_EXPIRED, EVAL_LICENCE_EXPIRED, FULL_LICENCE_EXPIRED, LICENCE_NOT_FOUND, 
            DATA_UNREADABLE
        });
    }
    
    /**
     * Default constructor
     */
    public LicensedSet()
    {
    }
    
    /**
     * Constructor take QALicensedSet object to populate On Demand result
     * @param s
     */
    public LicensedSet(QALicensedSet s)
    {
        m_ID = s.getID();
        m_Description = s.getDescription();
        m_Copyright = s.getCopyright();
        m_Version = s.getVersion();
        m_BaseCountry = s.getBaseCountry();
        m_Status = s.getStatus();
        m_Server = s.getServer();
        m_WarningLevel = s.getWarningLevel().toString();
        m_DaysLeft = s.getDaysLeft().intValue();
        m_DataDaysLeft = s.getDataDaysLeft().intValue();
        m_LicenceDaysLeft = s.getLicenceDaysLeft().intValue();
    }
    
    /**
     * Create a LicensedSet[] object with QALicenceInfo[] object
     * @param info
     * @return
     */
    public static LicensedSet[] createArray(QALicenceInfo info)
    {
        LicensedSet aResults[] = (LicensedSet[])null;
        List<QALicensedSet> aLics = info.getLicensedSet();
        if(aLics != null)
        {
            int iSize = aLics.size();
            if(iSize > 0)
            {
            	aResults = new LicensedSet[iSize];
                for(int i = 0; i < iSize; i++)
                	aResults[i] = new LicensedSet((QALicensedSet)aLics.get(i));

            }
        }
        return aResults;
    }

    /**
     * Create a LicensedSet[] object with QADataMapDetail[] object
     * @param info
     * @return
     */
    public static LicensedSet[] createArray(QADataMapDetail info)
    {
        LicensedSet aResults[] = (LicensedSet[])null;
        List<QALicensedSet> aInfo = info.getLicensedSet();
        if(aInfo != null && aInfo.size() > 0)
        {
            aResults = new LicensedSet[aInfo.size()];
            for(int i = 0; i < aInfo.size(); i++)
                aResults[i] = new LicensedSet((QALicensedSet)aInfo.get(i));

        }
        return aResults;
    }
    
    /**
     * getter for m_ID
     * @return m_ID
     */
	public String getID() {
		return m_ID;
	}

	/**
	 * getter for m_Description
	 * @return m_Description
	 */
	public String getDescription() {
		return m_Description;
	}

	/**
	 * getter for m_Copyright
	 * @return m_Copyright
	 */
	public String getCopyright() {
		return m_Copyright;
	}

	/**
	 * getter for m_Version
	 * @return m_Version
	 */
	public String getVersion() {
		return m_Version;
	}

	/**
	 * getter for m_BaseCountry
	 * @return m_BaseCountry
	 */
	public String getBaseCountry() {
		return m_BaseCountry;
	}

	/** 
	 * getter for m_Status
	 * @return m_Status
	 */
	public String getStatus() {
		return m_Status;
	}

	/**
	 * getter for m_Server
	 * @return m_Server
	 */
	public String getServer() {
		return m_Server;
	}

	/**
	 * getter for m_WarningLevel
	 * @return m_WarningLevel
	 */
	public String getWarningLevel() {
		return m_WarningLevel;
	}

	/**
	 * getter for m_DaysLeft
	 * @return m_DaysLeft
	 */
	public int getDaysLeft() {
		return m_DaysLeft;
	}

	/**
	 * getter for m_DataDaysLeft
	 * @return m_DataDaysLeft
	 */
	public int getDataDaysLeft() {
		return m_DataDaysLeft;
	}

	/**
	 * getter for m_LicenceDaysLeft
	 * @return m_LicenceDaysLeft
	 */
	public int getLicenceDaysLeft() {
		return m_LicenceDaysLeft;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append(String.format("\"ID\":\"%s\"", m_ID)).append(separator);
        jsonResult.append(String.format("\"Description\":\"%s\"", m_Description)).append(separator);
        jsonResult.append(String.format("\"CopyRight\":\"%s\"", m_Copyright)).append(separator);
        jsonResult.append(String.format("\"Version\":\"%s\"", m_Version)).append(separator);
        jsonResult.append(String.format("\"BaseCountry\":\"%s\"", m_BaseCountry)).append(separator);
        jsonResult.append(String.format("\"Status\":\"%s\"", m_Status)).append(separator);
        jsonResult.append(String.format("\"Server\":\"%s\"", m_Server)).append(separator);
        jsonResult.append(String.format("\"WarningLevel\":\"%s\"", m_WarningLevel)).append(separator);
        jsonResult.append(String.format("\"DaysLeft\":\"%s\"", m_DaysLeft)).append(separator);
        jsonResult.append(String.format("\"DataDaysLeft\":\"%s\"", m_DataDaysLeft)).append(separator);
        jsonResult.append(String.format("\"LicenceDaysLeft\":\"%s\"", m_LicenceDaysLeft));

        jsonResult.append("}");

        return jsonResult.toString();
	}
    
}
