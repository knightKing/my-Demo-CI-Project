package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;


import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.PicklistEntryType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QAPicklistType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.PicklistEntryType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QAPicklistType;

import java.io.Serializable;
import java.util.List;

/**
 * Class to process QAExampleAddress object from web service
 *
 * @author kahchingn
 * @version 1.0
 */
public final class Picklist implements Serializable, IJSONSerializable {

    private static final long serialVersionUID = -4198302422059633192L;
    private int m_Total;
    private String m_Moniker;
    private String m_Prompt;
    private PicklistItem m_Items[];

    private boolean m_IsAutoStepinSafe;
    private boolean m_IsAutoStepinPastClose;
    private boolean m_IsAutoformatSafe;
    private boolean m_IsAutoformatPastClose;
    private boolean m_IsLargePotential;
    private boolean m_IsMaxMatches;
    private boolean m_AreMoreOtherMatches;
    private boolean m_IsOverThreshold;
    private boolean m_IsTimeout;

    /**
     * Default constructor
     */
    public Picklist() {
    }

    /**
     * Constructor take QAPicklistType object to populate On Demand result
     *
     * @param p
     */
    public Picklist(QAPicklistType p) {
        m_Total = p.getTotal().intValue();
        m_Moniker = p.getFullPicklistMoniker();
        m_Prompt = p.getPrompt();
        m_IsAutoStepinSafe = p.isAutoStepinSafe();
        m_IsAutoStepinPastClose = p.isAutoStepinPastClose();
        m_IsAutoformatSafe = p.isAutoFormatSafe();
        m_IsAutoformatPastClose = p.isAutoFormatPastClose();
        m_IsLargePotential = p.isLargePotential();
        m_IsMaxMatches = p.isMaxMatches();
        m_AreMoreOtherMatches = p.isMoreOtherMatches();
        m_IsOverThreshold = p.isOverThreshold();
        m_IsTimeout = p.isTimeout();
        List<PicklistEntryType> aItems = p.getPicklistEntry();
        if (aItems != null) {
            int iSize = aItems.size();
            if (iSize > 0) {
                m_Items = new PicklistItem[iSize];
                for (int i = 0; i < iSize; i++)
                    m_Items[i] = new PicklistItem((PicklistEntryType) aItems.get(i));

            }
        }
    }

    /**
     * getter for m_Total
     *
     * @return m_Total
     */
    public int getTotal() {
        return m_Total;
    }

    /**
     * getter for m_Moniker
     *
     * @return m_Moniker
     */
    public String getMoniker() {
        return m_Moniker;
    }

    /**
     * getter for m_Prompt
     *
     * @return m_Prompt
     */
    public String getPrompt() {
        return m_Prompt;
    }

    /**
     * getter for m_Items
     *
     * @return m_Items
     */
    public PicklistItem[] getItems() {
        return m_Items;
    }

    /**
     * getter for m_IsAutoStepinSafe
     *
     * @return m_IsAutoStepinSafe
     */
    public boolean isAutoStepinSafe() {
        return m_IsAutoStepinSafe;
    }

    /**
     * getter for m_IsAutoStepinPastClose
     *
     * @return m_IsAutoStepinPastClose
     */
    public boolean isAutoStepinPastClose() {
        return m_IsAutoStepinPastClose;
    }

    /**
     * check is Auto Step in single
     *
     * @return isAutoStepinSingle
     */
    public boolean isAutoStepinSingle() {
        return getItems().length == 1 && getItems()[0].canStep() && !getItems()[0].isInformation();
    }

    /**
     * getter for m_IsAutoformatSafe
     *
     * @return m_IsAutoformatSafe
     */
    public boolean isAutoformatSafe() {
        return m_IsAutoformatSafe;
    }

    /**
     * getter for m_IsAutoformatPastClose
     *
     * @return m_IsAutoformatPastClose
     */
    public boolean isAutoformatPastClose() {
        return m_IsAutoformatPastClose;
    }

    /**
     * check is Auto Format Single
     *
     * @return isAutoformatSingle
     */
    public boolean isAutoformatSingle() {
        return getItems().length == 1 && getItems()[0].isFullAddress() && !getItems()[0].isInformation();
    }

    /**
     * getter for m_IsLargePotential
     *
     * @return m_IsLargePotential
     */
    public boolean isLargePotential() {
        return m_IsLargePotential;
    }

    /**
     * getter for m_IsMaxMatches
     *
     * @return m_IsMaxMatches
     */
    public boolean isMaxMatches() {
        return m_IsMaxMatches;
    }

    /**
     * getter for m_AreMoreOtherMatches
     *
     * @return m_AreMoreOtherMatches
     */
    public boolean areMoreOtherMatches() {
        return m_AreMoreOtherMatches;
    }

    /**
     * getter for m_IsOverThreshold
     *
     * @return m_IsOverThreshold
     */
    public boolean isOverThreshold() {
        return m_IsOverThreshold;
    }

    /**
     * getter for m_IsTimeout
     *
     * @return m_IsTimeout
     */
    public boolean isTimeout() {
        return m_IsTimeout;
    }

    /**
     * Format the result in JSON format
     *
     * @return jsonResult
     */
    @Override
    public String toJSON() {
        StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append(String.format("\"Moniker\":\"%s\"", this.m_Moniker)).append(separator);
        jsonResult.append("\"Items\":");
        if (m_Items != null) {
            jsonResult.append("[");
            for (int i = 0; i < this.m_Items.length; i++) {
                jsonResult.append(m_Items[i].toJSON());
                if (i == (m_Items.length - 1)) {
                    break;
                }
                jsonResult.append(separator);
            }
            jsonResult.append("]").append(separator);
        } else {
            jsonResult.append("null").append(separator);
        }
        jsonResult.append(String.format("\"Prompt\":\"%s\"", this.m_Prompt)).append(separator);
        jsonResult.append(String.format("\"Total\":%s", this.m_Total)).append(separator);
        jsonResult.append(String.format("\"IsAutoStepinSafe\":%s", this.m_IsAutoStepinSafe)).append(separator);
        jsonResult.append(String.format("\"IsAutoStepinPastClose\":%s", m_IsAutoStepinPastClose)).append(separator);
        jsonResult.append(String.format("\"IsAutoformatSafe\":%s", m_IsAutoformatSafe)).append(separator);
        jsonResult.append(String.format("\"IsAutoformatPastClose\":%s", m_IsAutoformatPastClose)).append(separator);
        jsonResult.append(String.format("\"IsLargePotential\":%s", m_IsLargePotential)).append(separator);
        jsonResult.append(String.format("\"IsMaxMatches\":%s", m_IsMaxMatches)).append(separator);
        jsonResult.append(String.format("\"AreMoreOtherMatches\":%s", m_AreMoreOtherMatches)).append(separator);
        jsonResult.append(String.format("\"IsOverThreshold\":%s", m_IsOverThreshold)).append(separator);
        jsonResult.append(String.format("\"IsTimeout\":%s", m_IsTimeout));

        jsonResult.append("}");

        return jsonResult.toString();
    }
}
