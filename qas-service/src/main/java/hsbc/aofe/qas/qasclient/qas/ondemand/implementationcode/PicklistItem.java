package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;


import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.PicklistEntryType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.PicklistEntryType;

import java.io.Serializable;

public final class PicklistItem implements Serializable, IJSONSerializable {

    private static final long serialVersionUID = 6152842232266997023L;
    private String m_Text;
    private String m_PartialAddress;
    private String m_Postcode;
    private int m_Score;
    private String m_Moniker;

    private boolean m_IsFullAddress;
    private boolean m_IsCanStep;
    private boolean m_IsAliasMatch;
    private boolean m_IsPostcodeRecoded;
    private boolean m_IsCrossBorderMatch;
    private boolean m_IsDummyPOBox;
    private boolean m_IsName;
    private boolean m_IsInformation;
    private boolean m_IsWarnInformation;
    private boolean m_IsIncompleteAddress;
    private boolean m_IsUnresolvableRange;
    private boolean m_IsPhantomPrimaryPoint;
    private boolean m_IsMultiples;
    private boolean m_IsSubsidiaryData;
    private boolean m_IsExtendedData;
    private boolean m_IsEnhancedData;

    /**
     * Default constructor
     */
    public PicklistItem() {
    }

    /**
     * Constructor take PicklistEntryType object to populate On Demand result
     *
     * @param t
     */
    public PicklistItem(PicklistEntryType t) {
        m_Text = t.getPicklist();
        m_Postcode = t.getPostcode();
        m_Score = t.getScore().intValue();
        m_Moniker = t.getMoniker();
        m_PartialAddress = t.getPartialAddress();
        m_IsFullAddress = t.isFullAddress();
        m_IsCanStep = t.isCanStep();
        m_IsAliasMatch = t.isAliasMatch();
        m_IsPostcodeRecoded = t.isPostcodeRecoded();
        m_IsCrossBorderMatch = t.isCrossBorderMatch();
        m_IsDummyPOBox = t.isDummyPOBox();
        m_IsName = t.isName();
        m_IsInformation = t.isInformation();
        m_IsWarnInformation = t.isWarnInformation();
        m_IsIncompleteAddress = t.isIncompleteAddr();
        m_IsUnresolvableRange = t.isUnresolvableRange();
        m_IsPhantomPrimaryPoint = t.isPhantomPrimaryPoint();
        m_IsMultiples = t.isMultiples();
    }

    /**
     * getter for m_Text
     *
     * @return m_Text
     */
    public String getText() {
        return m_Text;
    }

    /**
     * getter for m_Postcode
     *
     * @return m_Potcode
     */
    public String getPostcode() {
        return m_Postcode;
    }

    /**
     * getter for m_Score
     *
     * @return m_Score
     */
    public int getScore() {
        return m_Score;
    }

    /**
     * getter for m_Moniker
     *
     * @return m_Moniker
     */
    public String getMoniker() {
        return m_Moniker;
    }

    /**
     * getter for m_PartialAddress
     *
     * @return m_PartialAddress
     */
    public String getPartialAddress() {
        return m_PartialAddress;
    }

    /**
     * getter for m_IsFullAddress
     *
     * @return m_IsFullAddress
     */
    public boolean isFullAddress() {
        return m_IsFullAddress;
    }

    /**
     * getter for m_IsCanStep
     *
     * @return m_IsCanStep
     */
    public boolean canStep() {
        return m_IsCanStep;
    }

    /**
     * getter for m_IsAliasMatch
     *
     * @return m_IsAliasMatch
     */
    public boolean isAliasMatch() {
        return m_IsAliasMatch;
    }

    /**
     * getter for m_IsPostcodeRecoded
     *
     * @return m_IsPostcodeRecoded
     */
    public boolean isPostcodeRecoded() {
        return m_IsPostcodeRecoded;
    }

    /**
     * getter for m_IsIncompleteAddress
     *
     * @return m_IsIncompleteAddress
     */
    public boolean isIncompleteAddress() {
        return m_IsIncompleteAddress;
    }

    /**
     * getter for m_IsUnresolvableRange
     *
     * @return m_IsUnresolvableRange
     */
    public boolean isUnresolvableRange() {
        return m_IsUnresolvableRange;
    }

    /**
     * getter for m_IsCrossBorderMatch
     *
     * @return m_IsCrossBorderMatch
     */
    public boolean isCrossBorderMatch() {
        return m_IsCrossBorderMatch;
    }

    /**
     * getter for m_IsDummyPOBox
     *
     * @return m_IsDummyPOBox
     */
    public boolean isDummyPOBox() {
        return m_IsDummyPOBox;
    }

    /**
     * getter for m_IsName
     *
     * @return m_IsName
     */
    public boolean isName() {
        return m_IsName;
    }

    /**
     * getter for m_IsInformation
     *
     * @return m_IsInformation
     */
    public boolean isInformation() {
        return m_IsInformation;
    }

    /**
     * getter for m_IsWarnInformation
     *
     * @return m_IsWarnInformation
     */
    public boolean isWarnInformation() {
        return m_IsWarnInformation;
    }

    /**
     * getter for m_IsPhantomPrimaryPoint
     *
     * @return m_IsPhantomPrimaryPoint
     */
    public boolean isPhantomPrimaryPoint() {
        return m_IsPhantomPrimaryPoint;
    }

    /**
     * getter for m_IsMultiples
     *
     * @return m_IsMultiples
     */
    public boolean isMultiples() {
        return m_IsMultiples;
    }

    /**
     * getter for m_IsSubsidiaryData
     *
     * @return m_IsSubsidiaryData
     */
    public boolean isIsSubsidiaryData() {
        return m_IsSubsidiaryData;
    }

    /**
     * setter for m_IsSubsidiaryData
     *
     * @param m_IsSubsidiaryData
     */
    public void setIsSubsidiaryData(boolean m_IsSubsidiaryData) {
        this.m_IsSubsidiaryData = m_IsSubsidiaryData;
    }

    /**
     * getter for m_IsExtendedData
     *
     * @return m_IsExtendedData
     */
    public boolean isIsExtendedData() {
        return m_IsExtendedData;
    }

    /**
     * setter for m_IsExtendedData
     *
     * @param m_IsExtendedData
     */
    public void setIsExtendedData(boolean m_IsExtendedData) {
        this.m_IsExtendedData = m_IsExtendedData;
    }

    /**
     * getter for m_IsEnhancedData
     *
     * @return m_IsEnhancedData
     */
    public boolean isIsEnhancedData() {
        return m_IsEnhancedData;
    }

    /**
     * m_IsEnhancedData
     *
     * @param m_IsEnhancedData
     */
    public void setIsEnhancedData(boolean m_IsEnhancedData) {
        this.m_IsEnhancedData = m_IsEnhancedData;
    }

    /**
     * Format the result in JSON format
     *
     * @return jsonResult
     */
    @Override
    public String toJSON() {
        StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append(String.format("\"IsAliasMatch\":%s", m_IsAliasMatch)).append(separator);
        jsonResult.append(String.format("\"IsCanStep\":%s", m_IsCanStep)).append(separator);
        jsonResult.append(String.format("\"IsCrossBorderMatch\":%s", m_IsCrossBorderMatch)).append(separator);
        jsonResult.append(String.format("\"IsDummyPOBox\":%s", m_IsDummyPOBox)).append(separator);
        jsonResult.append(String.format("\"IsEnhancedData\":%s", m_IsEnhancedData)).append(separator);
        jsonResult.append(String.format("\"IsExtendedData\":%s", m_IsExtendedData)).append(separator);
        jsonResult.append(String.format("\"IsFullAddress\":%s", m_IsFullAddress)).append(separator);
        jsonResult.append(String.format("\"IsIncompleteAddress\":%s", m_IsIncompleteAddress)).append(separator);
        jsonResult.append(String.format("\"IsInformation\":%s", m_IsInformation)).append(separator);
        jsonResult.append(String.format("\"IsMultiples\":%s", m_IsMultiples)).append(separator);
        jsonResult.append(String.format("\"IsName\":%s", m_IsName)).append(separator);
        jsonResult.append(String.format("\"IsPhantomPrimaryPoint\":%s", m_IsPhantomPrimaryPoint)).append(separator);
        jsonResult.append(String.format("\"IsPostcodeRecode\":%s", m_IsPostcodeRecoded)).append(separator);
        jsonResult.append(String.format("\"IsSubsidiaryData\":%s", m_IsSubsidiaryData)).append(separator);
        jsonResult.append(String.format("\"IsUnresolvableRange\":%s", m_IsUnresolvableRange)).append(separator);
        jsonResult.append(String.format("\"IsWarnInformation\":%s", m_IsWarnInformation)).append(separator);
        jsonResult.append(String.format("\"Score\":%s", m_Score)).append(separator);
        jsonResult.append(String.format("\"Moniker\":\"%s\"", m_Moniker)).append(separator);
        jsonResult.append(String.format("\"PartialAddress\":\"%s\"", m_PartialAddress)).append(separator);
        jsonResult.append(String.format("\"Postcode\":\"%s\"", m_Postcode)).append(separator);
        jsonResult.append(String.format("\"Text\":\"%s\"", m_Text));

        jsonResult.append("}");

        return jsonResult.toString();
    }
}
