package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import java.io.Serializable;

/**
 * Class to process PromptLine object from web service
 * @author kahchingn
 * @version 1.0
 */
final class PromptLine implements Serializable, IJSONSerializable {
	
	private static final long serialVersionUID = -8063373382271950562L;
	private String m_Prompt;
    private String m_Example;
    private int m_SuggestedInputLength = 0;
    
    /**
     * Default constructor
     */
    public PromptLine()
    {
    }
    
    /**
     * Constructor take PromptLine object to populate On Demand result
     * @param t
     */
    public PromptLine(hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.PromptLine t)
    {
        m_Prompt = t.getPrompt();
        m_Example = t.getExample();
        m_SuggestedInputLength = t.getSuggestedInputLength().intValue();
    }

    /**
     * getter for m_Prompt
     * @return m_Prompt
     */
	public String getPrompt() {
		return m_Prompt;
	}

	/**
	 * getter for m_Example
	 * @return m_Example
	 */
	public String getExample() {
		return m_Example;
	}

	/**
	 * getter for m_SuggestedInputLength
	 * @return m_SuggestedInputLength
	 */
	public int getSuggestedInputLength() {
		return m_SuggestedInputLength;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append(String.format("\"Prompt\":\"%s\"", m_Prompt)).append(separator);
        jsonResult.append(String.format("\"Example\":\"%s\"", m_Example)).append(separator);
        jsonResult.append(String.format("\"SuggestedInputLength\":%s", m_SuggestedInputLength));
        jsonResult.append("}");

        return jsonResult.toString();
	}
    
    
}
