package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.PromptSetType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QAPromptSet;

import java.io.Serializable;

/**
 * Class to process QAPromptSet object from web service
 * @author kahchingn
 * @version 1.0
 */
final class PromptSet implements Serializable, IJSONSerializable {

	private static final long serialVersionUID = -1030437234851569935L;
	public static final String ONELINE;
    public static final String DEFAULT;
    public static final String GENERIC;
    public static final String OPTIMAL;
    public static final String ALTERNATE;
    public static final String ALTERNATE2;
    public static final String ALTERNATE3;
    private PromptLine[] m_Lines;
    private boolean m_Dynamic;

    static 
    {
        ONELINE = PromptSetType.ONE_LINE.value();
        DEFAULT = PromptSetType.DEFAULT.value();
        GENERIC = PromptSetType.GENERIC.value();
        OPTIMAL = PromptSetType.OPTIMAL.value();
        ALTERNATE = PromptSetType.ALTERNATE.value();
        ALTERNATE2 = PromptSetType.ALTERNATE2.value();
        ALTERNATE3 = PromptSetType.ALTERNATE3.value();
    }
    
    /**
     * Default constructor
     */
    public PromptSet()
    {
    }
    
    /**
     * Constructor take QAPromptSet object to populate On Demand result
     * @param tPromptSet
     */
    public PromptSet(QAPromptSet tPromptSet) {
    	m_Dynamic = tPromptSet.isDynamic();
    	m_Lines = null;
    	if (tPromptSet.getLine() != null) {
    		int iSize = tPromptSet.getLine().size();
    		if (iSize > 0) {
    			m_Lines = new PromptLine[iSize];
    			for (int i = 0; i < iSize; i++)
    				m_Lines[i] = new PromptLine(tPromptSet.getLine().get(i));
    		}
    	}
    }
    
    /**
     * getter for m_Lines.Prompt
     * @return m_Lines.getPrompt
     */
    public String[] getLinePrompts() {
    	int iSize = m_Lines.length;
    	String[] asResults = new String[iSize];
    	
    	for (int i = 0; i < iSize; i++)
    		asResults[i] = m_Lines[i].getPrompt();
    	
    	return asResults;
    }
    
    /**
     * getter for m_Dynamic
     * @return m_Dynamic
     */
    public boolean isDynamic() {
    	return m_Dynamic;
    }
    
    /**
     * getter for m_Lines
     * @return m_Lines
     */
    public PromptLine[] getLines() {
    	return m_Lines;
    }

    /**
     * Format the result in JSON format
     * @return jsonResult
     */
	@Override
	public String toJSON() {
		StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");
        
        jsonResult.append("\"Lines\":");
        if (m_Lines != null)
        {
        	jsonResult.append("[");
        	
            for (int i = 0; i < this.m_Lines.length; i++)
            {
                jsonResult.append(m_Lines[i].toJSON());
                if (i == (m_Lines.length - 1)) {
                	break;
                }
                jsonResult.append(separator);
            }
            jsonResult.append("]").append(separator);
        }
        else
        {
            jsonResult.append("null").append(separator);
        }
        jsonResult.append(String.format("\"IsDynamic\":%s", m_Dynamic));

        jsonResult.append("}");

        return jsonResult.toString();
	}
}
