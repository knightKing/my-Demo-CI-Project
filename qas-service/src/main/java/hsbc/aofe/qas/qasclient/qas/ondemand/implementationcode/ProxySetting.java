package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

/**
 * Class to store proxy info
 * @author kahchingn
 * @version 1.0
 */
public class ProxySetting {
	
	private String proxyHost;
	private String proxyPort;
	private String proxyUser;
	private String proxyPassword;
	
	/**
	 * setter for proxyHost
	 * @param proxyHost
	 */
	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}
	
	/**
	 * getter for proxyHost
	 * @return proxyHost
	 */
	public String getProxyHost() {
		return proxyHost;
	}
	
	/**
	 * setter for proxyPort
	 * @param proxyPort
	 */
	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}
	
	/**
	 * getter for proxyHost
	 * @return proxyHost
	 */
	public String getProxyPort() {
		return proxyPort;
	}
	
	/**
	 * setter for proxyUser
	 * @param proxyUser
	 */
	public void setProxyUser(String proxyUser) {
		this.proxyUser = proxyUser;
	}
	
	/**
	 * getter for proxyUser
	 * @return proxyUser
	 */
	public String getProxyUser() {
		return proxyUser;
	}
	
	/**
	 * setter for proxyPassword
	 * @param proxyPassword
	 */
	public void setProxyPassword(String proxyPassword) {
		this.proxyPassword = proxyPassword;
	}
	
	/**
	 * getter for proxyPassword
	 * @return proxyPassword
	 */
	public String getProxyPassword() {
		return proxyPassword;
	}
	
	

}
