package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.domain.SearchBody;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.fault.XFireFault;
import org.codehaus.xfire.transport.http.CommonsHttpMessageSender;

import javax.xml.ws.Holder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Class to invoke OnDemand Webservice
 *
 * @author sahil.verma
 * @version 1.0
 */
@Slf4j
public class QASCapture implements IQASCapture {

    public static final String SINGLELINE;
    public static final String TYPEDOWN;
    public static final String VERIFICATION;
    public static final String KEYFINDER;
    public static final String EXACT;
    public static final String CLOSE;
    public static final String EXTENSIVE;
    private QAQueryHeader authentication;
    private Holder<QAInformation> holder;
    private QAPortType m_Port;
    private EngineType m_EngineType;
    @SuppressWarnings("unused")
    private QAPortType m_Config;

    static {
        SINGLELINE = EngineEnumType.SINGLELINE.value();
        TYPEDOWN = EngineEnumType.TYPEDOWN.value();
        VERIFICATION = EngineEnumType.VERIFICATION.value();
        KEYFINDER = EngineEnumType.KEYFINDER.value();
        EXACT = EngineIntensityType.EXACT.value();
        CLOSE = EngineIntensityType.CLOSE.value();
        EXTENSIVE = EngineIntensityType.EXTENSIVE.value();
    }

    /**
     * Default constructor
     */
    public QASCapture() {
        holder = new Holder<QAInformation>();
        m_Port = null;
        m_EngineType = null;
        m_Config = null;
    }

    /**
     * Initial QAS Webservice with custom endpoint, username and password
     *
     * @param endpoint
     * @param username
     * @param password
     * @param authToken
     * @throws Exception
     */
    public QASCapture(String endpoint, String username, String password, String authToken)
            throws Exception {
        holder = new Holder<QAInformation>();
        m_Port = null;
        m_EngineType = new EngineType();
        m_Config = null;
        QAQueryHeader qaQueryHeader = new QAQueryHeader();
        QAAuthentication authenticationInfo = new QAAuthentication();
        authenticationInfo.setUsername(username);
        authenticationInfo.setPassword(password);

        qaQueryHeader.setQAAuthentication(authenticationInfo);
        authentication = qaQueryHeader;

        QASOnDemandIntermediaryClient service = new QASOnDemandIntermediaryClient();
        try {
            m_Port = service.getQAPortType(endpoint);
            Client client = Client.getInstance(m_Port);
            Map<String, String> requestHeaders = new HashMap<String, String>();
            requestHeaders.put("Auth-Token", authToken);
            client.setProperty(CommonsHttpMessageSender.HTTP_HEADERS, requestHeaders);
        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }
    }

    /**
     * Initial QAS Webservice with custom endpoint, username, password and proxy
     *
     * @param endpoint
     * @param username
     * @param password
     * @param authToken
     * @param proxy
     * @throws Exception
     */
    public QASCapture(String endpoint, String username, String password, String authToken,
                      ProxySetting proxy) throws Exception {
        holder = new Holder<QAInformation>();
        m_Port = null;
        m_EngineType = new EngineType();
        m_Config = null;
        QAQueryHeader qaQueryHeader = new QAQueryHeader();
        QAAuthentication authenticationInfo = new QAAuthentication();
        authenticationInfo.setUsername(username);
        authenticationInfo.setPassword(password);

        qaQueryHeader.setQAAuthentication(authenticationInfo);
        authentication = qaQueryHeader;

        QASOnDemandIntermediaryClient service = new QASOnDemandIntermediaryClient();
        try {
            m_Port = service.getQAPortType(endpoint);
            if (proxy.getProxyHost() != null) {
                Client client = Client.getInstance(m_Port);
                // http proxy configuration
                client.setProperty("http.disable.proxy.utils", "true");
                client.setProperty("http.proxyHost", proxy.getProxyHost());
                client.setProperty("http.proxyPort", proxy.getProxyPort());
                client.setProperty("http.proxy.user", proxy.getProxyUser());
                client.setProperty("http.proxy.password", proxy.getProxyPassword());
                // https proxy configuration
                client.setProperty("https.disable.proxy.utils", "true");
                client.setProperty("https.proxyHost", proxy.getProxyHost());
                client.setProperty("https.proxyPort", proxy.getProxyPort());
                client.setProperty("https.proxy.user", proxy.getProxyUser());
                client.setProperty("https.proxy.password", proxy.getProxyPassword());

                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Auth-Token", authToken);
                client.setProperty(CommonsHttpMessageSender.HTTP_HEADERS, requestHeaders);
                log.info("QAS: Proxy configurations done.");
            }
        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }
    }

    /**
     * getter for m_Port
     *
     * @return m_Port
     */
    private QAPortType getServer() {
        return m_Port;
    }

    /**
     * setter for m_EngineType
     *
     * @param s
     */
    public void setEngineType(String s) {
        EngineType engineType = new EngineType();
        engineType.setValue(EngineEnumType.fromValue(s));
        m_EngineType = engineType;
    }

    /**
     * setter for m_EngineType properties
     *
     * @param flatten
     * @param intensity
     * @param promptSet
     * @param threshold
     * @param timeout
     */
    public void setEngineType(boolean flatten, String intensity,
                              String promptSet, int threshold, int timeout) {
        setFlatten(flatten);
        setEngineIntensity(intensity);
        setPromptSet(promptSet);
        setThreshold(threshold);
        setTimeout(timeout);
    }

    /**
     * setter for m_EngineType.Flatten
     *
     * @param flatten
     */
    private void setFlatten(boolean flatten) {
        m_EngineType.setFlatten(flatten);
    }

    /**
     * setter for m_EngineType.Intensity
     *
     * @param intensity
     */
    private void setEngineIntensity(String intensity) {
        m_EngineType.setIntensity(EngineIntensityType.fromValue(intensity));
    }

    /**
     * setter for m_EngineType.PromptSet
     *
     * @param promptSet
     */
    private void setPromptSet(String promptSet) {
        m_EngineType.setPromptSet(PromptSetType.fromValue(promptSet));

    }

    /**
     * setter for m_EngineType.Threshold
     *
     * @param threshold
     */
    private void setThreshold(int threshold) {
        m_EngineType.setThreshold(threshold);
    }

    /**
     * setter for m_EngineType.Timeout
     *
     * @param timeout
     */
    private void setTimeout(int timeout) {
        m_EngineType.setTimeout(timeout);
    }

    /**
     * getter for m_EngineType
     *
     * @return m_EngineType
     */
    public EngineType getEngineType() {
        return m_EngineType;
    }

    /**
     * set default engine type for m_EngineType
     *
     * @return m_EngineType
     */
    public EngineType getEngine() {
        if (m_EngineType == null)
            setEngineType(SINGLELINE);
        return m_EngineType;
    }


    /**
     * Invoke CanSearch
     *
     * @param countryId
     * @param engine
     * @param flatten
     * @param intensity
     * @param promptset
     * @param threshold
     * @param timeout
     * @param layout
     * @param localisation
     * @return
     * @throws QasException
     */
    public CanSearch CanSearch(String countryId, String engine, Boolean flatten, String intensity,
                               String promptset, Integer threshold, Integer timeout, String layout, String localisation) throws QasException {

        CanSearch result = null;
        QACanSearch param = new QACanSearch();
        EngineType engineType = new EngineType();

        if (!promptset.isEmpty()) {
            PromptSetType promptsetType = PromptSetType.fromValue(promptset);
            engineType.setPromptSet(promptsetType);
        }

        if (!engine.isEmpty()) {
            EngineEnumType engineEnumType = EngineEnumType.fromValue(engine);
            engineType.setValue(engineEnumType);
        }

        if (!intensity.isEmpty()) {
            EngineIntensityType intensityType = EngineIntensityType.fromValue(intensity);
            engineType.setIntensity(intensityType);
        }

        if (flatten != null) {
            engineType.setFlatten(flatten.booleanValue());
        }

        if (threshold != null) {
            engineType.setThreshold(threshold.intValue());
        }

        if (timeout != null) {
            engineType.setTimeout(timeout.intValue());
        }

        if (!countryId.isEmpty()) {
            param.setCountry(countryId);
        }

        if (!layout.isEmpty()) {
            param.setLayout(layout);
        }

        if (!localisation.isEmpty()) {
            param.setLayout(localisation);
        }

        param.setEngine(engineType);

        try {
            QASearchOk ok = getServer().doCanSearch(param, authentication,
                    holder);
            result = new CanSearch(ok);
        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return result;
    }

    /**
     * invoke doSearch
     *
     * @param searchBody
     * @return
     * @throws QasException
     */
    public SearchResult Search(SearchBody searchBody) throws QasException {

        SearchResult result = null;
        QASearch param = new QASearch();
        EngineType engineType = new EngineType();

        if (!searchBody.getPromptset().isEmpty()) {
            PromptSetType promptsetType = PromptSetType.fromValue(searchBody.getPromptset());
            engineType.setPromptSet(promptsetType);
        }

        if (!searchBody.getEngine().isEmpty()) {
            EngineEnumType engineEnumType = EngineEnumType.fromValue(searchBody.getEngine());
            engineType.setValue(engineEnumType);
        }

        if (!searchBody.getIntensity().isEmpty()) {
            EngineIntensityType intensityType = EngineIntensityType.fromValue(searchBody.getIntensity());
            engineType.setIntensity(intensityType);
        }

        if (searchBody.getFlatten() != null) {
            engineType.setFlatten(searchBody.getFlatten().booleanValue());
        }

        if (searchBody.getThreshold() != null) {
            engineType.setThreshold(searchBody.getThreshold().intValue());
        }

        if (searchBody.getTimeout() != null) {
            engineType.setTimeout(searchBody.getTimeout().intValue());
        }

        if (!searchBody.getCountryId().isEmpty()) {
            param.setCountry(searchBody.getCountryId());
        }

        if (!searchBody.getSearch().isEmpty()) {
            param.setSearch(searchBody.getSearch());
        }

        param.setEngine(engineType);

        if (!searchBody.getLayout().isEmpty()) {
            param.setLayout(searchBody.getLayout());
        }

        if (!searchBody.getRequestTag().isEmpty()) {
            param.setRequestTag(searchBody.getRequestTag());
        }

        if (!searchBody.getLocalisation().isEmpty()) {
            param.setLocalisation(searchBody.getLocalisation());
        }
        if (searchBody.getFormattedAddressInPicklist() != null) {
            param.setFormattedAddressInPicklist(searchBody.getFormattedAddressInPicklist());
        }

        try {
            QASearchResult searchResult = getServer()
                    .doSearch(param, authentication, holder);
            result = new SearchResult(searchResult);
        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }
        return result;
    }

    /**
     * invoke doRefine
     *
     * @param moniker
     * @param refinement
     * @param layout
     * @param formattedAddressInPicklist
     * @param threshold
     * @param timeout
     * @param requestTag
     * @param localisation
     * @return
     * @throws QasException
     */
    public hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.Picklist Refine(String moniker, String refinement, String layout, Boolean formattedAddressInPicklist,
                                                                                   Integer threshold, Integer timeout, String requestTag, String localisation) throws QasException {

        QARefine param = new QARefine();

        if (!moniker.isEmpty()) {
            param.setMoniker(moniker);
        }

        if (!refinement.isEmpty()) {
            param.setRefinement(refinement);
        }

        if (!layout.isEmpty()) {
            param.setLayout(layout);
        }

        if (formattedAddressInPicklist != null) {
            param.setFormattedAddressInPicklist(formattedAddressInPicklist);
        }

        if (threshold != null) {
            param.setThreshold(threshold);
        }

        if (timeout != null) {
            param.setTimeout(timeout);
        }

        if (!requestTag.isEmpty()) {
            param.setRequestTag(requestTag);
        }

        if (!localisation.isEmpty()) {
            param.setLocalisation(localisation);
        }

        hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.Picklist result = null;

        try {
            QAPicklistType picklist = getServer().doRefine(param,
                    authentication, holder).getQAPicklist();
            result = new hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.Picklist(picklist);
        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return result;
    }

    /**
     * ivnoke doGetAddress
     *
     * @param moniker
     * @param layout
     * @param requestTag
     * @param localisation
     * @return
     * @throws QasException
     */
    public FormattedAddress GetAddress(String moniker, String layout, String requestTag, String localisation)
            throws QasException {

        QAGetAddress param = new QAGetAddress();

        if (!layout.isEmpty()) {
            param.setLayout(layout);
        }

        if (!moniker.isEmpty()) {
            param.setMoniker(moniker);
        }

        if (!requestTag.isEmpty()) {
            param.setRequestTag(requestTag);
        }

        if (!localisation.isEmpty()) {
            param.setLocalisation(localisation);
        }

        FormattedAddress result = null;

        try {
            QAAddressType address = getServer().doGetAddress(param,
                    authentication, holder).getQAAddress();
            result = new FormattedAddress(address);
        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return result;
    }

    /**
     * invoke doGetData
     *
     * @param localisation
     * @return
     * @throws QasException
     */
    public Dataset[] GetData(String localisation) throws QasException {

        Dataset[] aResults = null;

        try {
            QAGetData getData = new QAGetData();

            if (!localisation.isEmpty()) {
                getData.setLocalisation(localisation);
            }

            QAData lDataSets = getServer().doGetData(getData, authentication,
                    holder);

            List<QADataSet> list = lDataSets.getDataSet();

            QADataSet[] aDataSets = new QADataSet[list.size()];
            Iterator<QADataSet> iterator = list.iterator();
            int i = 0;
            QADataSet element;
            while (iterator.hasNext()) {
                element = (QADataSet) iterator.next();
                aDataSets[i] = element;
                i++;
            }

            aResults = Dataset.createArray(aDataSets);

        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return aResults;
    }

    /**
     * invoke doGetDataMapDetail
     *
     * @param countryId
     * @param localisation
     * @return
     * @throws QasException
     */
    public LicensedSet[] GetDataMapDetail(String countryId, String localisation) throws QasException {

        LicensedSet[] aDatasets = null;

        try {
            QAGetDataMapDetail tRequest = new QAGetDataMapDetail();

            if (!countryId.isEmpty()) {
                tRequest.setDataMap(countryId);
            }

            if (!localisation.isEmpty()) {
                tRequest.setLocalisation(localisation);
            }

            QADataMapDetail tMapDetail = getServer().doGetDataMapDetail(
                    tRequest, authentication, holder);
            aDatasets = LicensedSet.createArray(tMapDetail);

        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return aDatasets;
    }

    /**
     * invoke doGetLayouts
     *
     * @param countryId
     * @param localisation
     * @return
     * @throws Exception
     */
    public Layout[] GetLayouts(String countryId, String localisation) throws QasException {

        Layout[] aResults = null;
        QAGetLayouts param = new QAGetLayouts();

        if (!countryId.isEmpty()) {
            param.setCountry(countryId);
        }

        if (!localisation.isEmpty()) {
            param.setLocalisation(localisation);
        }

        try {
            List<QALayout> list = getServer().doGetLayouts(param,
                    authentication, holder).getLayout();
            QALayout[] aLayouts = new QALayout[list.size()];

            Iterator<QALayout> iterator = list.iterator();
            int i = 0;
            QALayout element;
            while (iterator.hasNext()) {
                element = (QALayout) iterator.next();
                aLayouts[i] = element;
                i++;
            }
            aResults = Layout.createArray(aLayouts);

        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return aResults;
    }

    /**
     * invoke doGetExampleAddresses
     *
     * @param countryId
     * @param layout
     * @param requestTag
     * @param localisation
     * @return
     * @throws QasException
     */
    public ExampleAddress[] GetExampleAddresses(String countryId, String layout, String requestTag,
                                                String localisation) throws QasException {

        ExampleAddress[] aResults = null;

        QAGetExampleAddresses param = new QAGetExampleAddresses();

        if (!countryId.isEmpty()) {
            param.setCountry(countryId);
        }

        if (!layout.isEmpty()) {
            param.setLayout(layout);
        }

        if (!requestTag.isEmpty()) {
            param.setRequestTag(requestTag);
        }

        if (!localisation.isEmpty()) {
            param.setLocalisation(localisation);
        }

        try {
            List<QAExampleAddress> list = getServer().doGetExampleAddresses(
                    param, authentication, holder).getExampleAddress();
            QAExampleAddress[] aAddress = new QAExampleAddress[list.size()];

            Iterator<QAExampleAddress> iterator = list.iterator();
            int i = 0;
            QAExampleAddress element;
            while (iterator.hasNext()) {
                element = (QAExampleAddress) iterator.next();
                aAddress[i] = element;
                i++;
            }
            aResults = ExampleAddress.createArray(aAddress);

        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return aResults;
    }

    /**
     * invoke doGetLicenseInfo
     *
     * @param localisation
     * @return
     * @throws QasException
     */
    public LicensedSet[] GetLicenseInfo(String localisation) throws QasException {

        LicensedSet[] aResults = null;

        try {
            QAGetLicenseInfo getLicenseInfo = new QAGetLicenseInfo();

            if (!localisation.isEmpty()) {
                getLicenseInfo.setLocalisation(localisation);
            }

            QALicenceInfo info = getServer().doGetLicenseInfo(getLicenseInfo,
                    authentication, holder);
            aResults = LicensedSet.createArray(info);

        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }
        return aResults;
    }

    /**
     * invoke doGetPromptSet
     *
     * @param countryId
     * @param engine
     * @param flatten
     * @param promptset
     * @param threshold
     * @param timeout
     * @param localisation
     * @return
     * @throws QasException
     */
    public PromptSet GetPromptSet(String countryId, String engine, Boolean flatten,
                                  String promptset, Integer threshold, Integer timeout, String localisation) throws QasException {

        PromptSet result = null;
        QAGetPromptSet param = new QAGetPromptSet();
        EngineType engineType = new EngineType();

        if (!promptset.isEmpty()) {
            PromptSetType promptsetType = PromptSetType.fromValue(promptset);
            engineType.setPromptSet(promptsetType);
            param.setPromptSet(promptsetType);
        }

        if (!engine.isEmpty()) {
            EngineEnumType engineEnumType = EngineEnumType.fromValue(engine);
            engineType.setValue(engineEnumType);

        }

        if (flatten != null) {
            engineType.setFlatten(flatten);
        }

        if (threshold != null) {
            engineType.setThreshold(threshold);
        }

        if (timeout != null) {
            engineType.setTimeout(timeout);
        }

        if (!countryId.isEmpty()) {
            param.setCountry(countryId);
        }

        param.setEngine(engineType);

        if (!localisation.isEmpty()) {
            param.setLocalisation(localisation);
        }

        try {
            QAPromptSet tPromptSet = getServer().doGetPromptSet(param,
                    authentication, holder);
            result = new PromptSet(tPromptSet);

        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return result;
    }

    /**
     * invoke doGetSystemInfo
     *
     * @param localisation
     * @return
     * @throws QasException
     */
    public String[] GetSystemInfo(String localisation) throws QasException {

        String[] aResults = null;

        try {
            QAGetSystemInfo param = new QAGetSystemInfo();

            if (!localisation.isEmpty()) {
                param.setLocalisation(localisation);
            }

            QASystemInfo systemInfo = getServer().doGetSystemInfo(param,
                    authentication, holder);

            List<String> list = systemInfo.getSystemInfo();
            String[] results = new String[list.size()];

            Iterator<String> iterator = list.iterator();
            int i = 0;
            String element;
            while (iterator.hasNext()) {
                element = (String) iterator.next();
                results[i] = element;
                i++;
            }
            aResults = results;

        } catch (XFireRuntimeException fault) {
            mapQAFault(fault);
        }

        return aResults;
    }

    /**
     * to throw a QasException when error occurred upon invoking OnDemand WS
     *
     * @param XFireRuntimException
     */
    private void mapQAFault(XFireRuntimeException x) throws QasException {
        log.error("QAS: Error occurred in mapQAFault: {}", toString(x));
        XFireFault fault = (XFireFault) x.getCause();
        log.error("QAS: fault in mapQAFault : {}", toString(fault));
        String errorDetail;
        if (fault.getDetail().getChildText("ExceptionCode") != null) {
            if (fault
                    .getMessage()
                    .contains(
                            "External account cannot perform DoSearch with Flatten set to false"))
                errorDetail = fault.getMessage();
            else
                errorDetail = fault.getDetail().getChildText("ExceptionCode");
        } else {
            errorDetail = fault.getMessage();
        }
        throw new QasException("XFireRuntimeException",
                new String[]{errorDetail});
    }

    public String toString(Object o) {
        if (o == null)
            return null;
        return new ReflectionToStringBuilder(o, new RecursiveToStringStyle()).toString();
    }
}
