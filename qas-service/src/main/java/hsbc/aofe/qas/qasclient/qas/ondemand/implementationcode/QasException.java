package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Custom Exception Class
 * @author kahchingn
 * @version 1.0
 */
public class QasException extends Exception {

	private static final long serialVersionUID = 1408088426632379466L;
	public static final int CLIENT_ERROR = 0;
    public static final int UNKNOWN_SERVER_ERROR = 1;
    public static final int UNKNOWN_COMMS_ERROR = 2;
    public static final int XML_PARSING_ERROR = 3;
    public static final int WEBSPHERE_MQ_ERROR = 4;
    public static final int TCPIP_ERROR = 5;
    public static final int INSUFFICIENT_CREDITS = 6;
    public static final int USER_AUTHENTICATION_INVALID = 7;
    public static final int LICENSE_INVALID = 8;
    public static final int SERVER_UNAVAILABLE = 9;
    private int m_QasErrorNumber;
    private String errorDetail[];
    
    /**
     * Default constructor
     */
    public QasException()
    {
        m_QasErrorNumber = 0;
    }
    
    /**
     * Constructor with create error message = [error message] append [error detail]
     * @param msg
     * @param errorDetail
     */
    public QasException(String msg, String errorDetail[])
    {
        super((new StringBuilder(String.valueOf(msg))).append(" [").append(errorDetail[0]).append("]").toString());
        m_QasErrorNumber = UNKNOWN_SERVER_ERROR;
        this.errorDetail = errorDetail;
    }

    /**
     * Constructor with create error message = error message append error number
     * @param msg
     * @param iErrorNumber
     */
    public QasException(String msg, int iErrorNumber)
    {
        super((new StringBuilder(String.valueOf(msg))).append(" [").append(iErrorNumber).append("]").toString());
        m_QasErrorNumber = iErrorNumber;
    }

    /**
     * Constructor with create error message = [error message] append [error number] append [error detail]
     * @param msg
     * @param iErrorNumber
     * @param errorDetail
     */
    public QasException(String msg, int iErrorNumber, String errorDetail[])
    {
        super((new StringBuilder(String.valueOf(msg))).append(" [").append(iErrorNumber).append(": ").append(errorDetail[0]).append("]").toString());
        m_QasErrorNumber = iErrorNumber;
        this.errorDetail = errorDetail;
    }

    /**
     * getter for m_QasErrorNumber
     * @return m_QasErrorNumber
     */
    public int getErrorNumber()
    {
        return m_QasErrorNumber;
    }

    /**
     * return the full error message
     */
    public String toString()
    {
        return (new StringBuilder(String.valueOf(getClass().getName()))).append(" - ").append(getMessage()).toString();
    }

    /**
     * generate printStackTrace for the exception
     * @return printStackTrace for the result
     * @see Throwable.printStackTrace(PrintWritter)
     */
    public String stackTraceToString()
    {
        java.io.Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        printStackTrace(printWriter);
        return result.toString();
    }

    /**
     * getter for the error detail
     * @return errDetail
     */
    public String errorDetail()
    {
        String errDetail = "";
        if(errorDetail == null)
            return "";
        for(int i = 0; i < errorDetail.length; i++)
            errDetail = (new StringBuilder(String.valueOf(errDetail))).append(errorDetail[i]).append("\n").toString();

        return errDetail;
    }
    
}
