package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode;

import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QAAddressType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QAPicklistType;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.QASearchResult;
import hsbc.aofe.qas.qasclient.qas.ondemand_2011_03.VerifyLevelType;

/**
 * Class to process QASearchResult object from web service
 *
 * @author kahchingn
 * @version 1.0
 */
public final class SearchResult implements IJSONSerializable {

    public static final String None;
    public static final String Verified;
    public static final String InteractionRequired;
    public static final String PremisesPartial;
    public static final String StreetPartial;
    public static final String Multiple;
    public static final String VerifyPlace;
    public static final String VerifyStreet;
    private FormattedAddress m_Address;
    private Picklist m_Picklist;
    private String m_VerifyLevel;

    static {
        None = VerifyLevelType.NONE.value();
        Verified = VerifyLevelType.VERIFIED.value();
        InteractionRequired = VerifyLevelType.INTERACTION_REQUIRED.value();
        PremisesPartial = VerifyLevelType.PREMISES_PARTIAL.value();
        StreetPartial = VerifyLevelType.STREET_PARTIAL.value();
        Multiple = VerifyLevelType.MULTIPLE.value();
        VerifyPlace = VerifyLevelType.VERIFIED_PLACE.value();
        VerifyStreet = VerifyLevelType.VERIFIED_STREET.value();
    }

    /**
     * Default constructor
     */
    public SearchResult() {
    }

    /**
     * Constructor take QASearchResult object to populate On Demand result
     *
     * @param sr
     */
    public SearchResult(QASearchResult sr) {
        m_Address = null;
        m_Picklist = null;
        m_VerifyLevel = null;
        QAAddressType address = sr.getQAAddress();
        QAPicklistType picklist = sr.getQAPicklist();
        VerifyLevelType level = sr.getVerifyLevel();
        if (level != null) {
            m_VerifyLevel = level.value();
        }
        if (picklist != null) {
            m_Picklist = new Picklist(picklist);
        }
        if (address != null) {
            m_Address = new FormattedAddress(address);
        }
    }

    /**
     * getter for m_Address
     *
     * @return m_Address
     */
    public FormattedAddress getAddress() {
        return m_Address;
    }

    /**
     * getter for m_Picklist
     *
     * @return
     */
    public Picklist getPicklist() {
        return m_Picklist;
    }

    /**
     * getter for m_VerifyLevel
     *
     * @return
     */
    public String getVerifyLevel() {
        return m_VerifyLevel;
    }

    /**
     * Format the result in JSON format
     *
     * @return jsonResult
     */
    @Override
    public String toJSON() {
        StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        jsonResult.append("\"Address\":");
        if (m_Address != null) {
            jsonResult.append(m_Address.toJSON());
            jsonResult.append(separator);
        } else {
            jsonResult.append("null").append(separator);
        }
        jsonResult.append("\"Picklist\":");
        if (m_Picklist != null) {
            jsonResult.append(m_Picklist.toJSON());
            jsonResult.append(separator);
        } else {
            jsonResult.append("null").append(separator);
        }
        jsonResult.append(String.format("\"VerifyLevel\":\"%s\"", m_VerifyLevel));
        jsonResult.append("}");

        return jsonResult.toString();
    }
}
