package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.common;

import java.util.HashMap;

/**
 * Class to store method params
 * @author kahchingn
 * @version 1.0
 */
public class ParameterList {

	private static final String[] CAN_SEARCH_PARAM = { "countryId","layout", "promptset" };
	private static final String[] SEARCH_PARAM = { "countryId", "search",
			"promptset", "layout", "requestTag" };
	private static final String[] REFINE_PARAM = { "moniker", "refinement",
			"requestTag" };
	private static final String[] GET_ADDRESS_PARAM = { "moniker",
			"layout", "requestTag" };
	private static final String[] GET_DATA_PARAM = {};
	private static final String[] GET_DATASET_MAP_PARAM = { "countryId" };
	private static final String[] GET_LAYOUTS_PARAM = { "countryId" };
	private static final String[] GET_EXAMPLE_ADDRESSES_PARAM = { "countryId",
			"layout", "requestTag" };
	private static final String[] GET_LICENSE_INFO_PARAM = {};
	private static final String[] GET_PROMPTSET_PARAM = { "countryId",
			"promptset" };
	private static final String[] GET_SYSTEM_INFO_PARAM = {};
	
	public static final String CAN_SEARCH_METHOD = "CanSearch";
	public static final String SEARCH_METHOD = "Search";
	public static final String REFINE_METHOD = "Refine";
	public static final String GET_ADDRESS_METHOD = "GetAddress";
	public static final String GET_DATA_METHOD = "GetData";
	public static final String GET_DATASET_MAP_METHOD = "GetDataMapDetail";
	public static final String GET_LAYOUTS_METHOD = "GetLayouts";
	public static final String GET_EXAMPLE_ADDRESSES_METHOD = "GetExampleAddresses";
	public static final String GET_LICENSE_INFO_METHOD = "GetLicenseInfo";
	public static final String GET_PROMPTSET_METHOD = "GetPromptSet";
	public static final String GET_SYSTEM_INFO_METHOD = "GetSystemInfo";
	
	public static HashMap<String, String[]> paramList;
	
	static {
		paramList = new HashMap<String, String[]>();
		paramList.put(CAN_SEARCH_METHOD, CAN_SEARCH_PARAM);
		paramList.put(GET_DATA_METHOD, GET_DATA_PARAM);
		paramList.put(GET_LAYOUTS_METHOD, GET_LAYOUTS_PARAM);
		paramList.put(GET_DATASET_MAP_METHOD, GET_DATASET_MAP_PARAM);
		paramList.put(GET_EXAMPLE_ADDRESSES_METHOD, GET_EXAMPLE_ADDRESSES_PARAM);
		paramList.put(GET_ADDRESS_METHOD, GET_ADDRESS_PARAM);
		paramList.put(GET_LICENSE_INFO_METHOD, GET_LICENSE_INFO_PARAM);
		paramList.put(GET_PROMPTSET_METHOD, GET_PROMPTSET_PARAM);
		paramList.put(GET_SYSTEM_INFO_METHOD, GET_SYSTEM_INFO_PARAM);
		paramList.put(REFINE_METHOD, REFINE_PARAM);
		paramList.put(SEARCH_METHOD, SEARCH_PARAM);
	}
	
	/**
	 * Default constructor
	 */
	public ParameterList() {
		
	}

	/**
	 * provide parameter list based on methodName
	 * @param methodName
	 * @return paramList for the method
	 */
	public static String[] getParamList(String methodName) {
		
		return paramList.get(methodName);
	}
}
