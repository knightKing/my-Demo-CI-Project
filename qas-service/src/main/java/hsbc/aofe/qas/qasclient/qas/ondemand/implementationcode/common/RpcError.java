package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.common;

import hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.IJSONSerializable;
import hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.QasException;

import java.io.Serializable;

/**
 * Class to return error message to Web request
 * @author kahchingn
 * @version 1.0
 */
public class RpcError implements Serializable, IJSONSerializable {

	private static final long serialVersionUID = 1L;
	public String message;
	public String code = "0";

	/**
	 * constructor to store error message and error code
	 * @param message
	 * @param code
	 */
	public RpcError(String message, String code) {
		this.message = message;
		this.code = code;
	}
	
	/**
	 * getter for message
	 * @return message
	 */
	public String getMessage() {
		return this.message;
	}
	
	/**
	 * getter for code
	 * @return code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() throws QasException {
		
		StringBuffer jsonResult = new StringBuffer();
        String separator = ",";
        jsonResult.append("{");

        if (message.isEmpty()) {
        	jsonResult.append(String.format("\"message\":null")).append(separator);
        } else {
        	jsonResult.append(String.format("\"message\":\"%s\"", message)).append(separator);
        }
        jsonResult.append(String.format("\"code\":\"%s\"", code));

        jsonResult.append("}");

        return jsonResult.toString();
	}
}
