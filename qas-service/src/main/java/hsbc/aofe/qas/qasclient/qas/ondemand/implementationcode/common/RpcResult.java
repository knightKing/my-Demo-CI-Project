package hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.common;

import hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.IJSONSerializable;
import hsbc.aofe.qas.qasclient.qas.ondemand.implementationcode.QasException;

import java.io.Serializable;

/**
 * Class to return result to Web request
 * @author kahchingn
 * @version 1.0
 */
public class RpcResult implements Serializable, IJSONSerializable {

	private static final long serialVersionUID = 1L;
	public Object result;
	public Object error;
	
	/**
	 * Default constructor
	 */
	public RpcResult() {
	}
	
	/**
	 * Constructor to store the search result
	 * @param result
	 */
	public RpcResult(Object result) {
		this.result = result;
	}
	
	/**
	 * getter for result
	 * @return result
	 */
	public Object getResult() {
		return this.result;
	}
	
	/**
	 * getter for error
	 * @return error
	 */
	public Object getError() {
		return this.error;
	}

	/**
	 * Format the result in JSON format
	 * @return jsonResult
	 */
	@Override
	public String toJSON() throws QasException {
		
		StringBuffer jsonResult = new StringBuffer();
		try {
			
	        String separator = ",";
	        jsonResult.append("{\"result\":");
	        
	        if (this.result != null) {
		        boolean objIsArray = this.result.getClass().isArray();
		        
		        if (objIsArray)
		        {
		        	if ( this.result instanceof IJSONSerializable[] ) 
		        	{
			            IJSONSerializable[] arrayResult = (IJSONSerializable[])this.result;
			            jsonResult.append("[");
			            for (int i = 0; i < arrayResult.length; i++)
			            {
			                jsonResult.append(arrayResult[i].toJSON());
			                if (i == (arrayResult.length - 1))
			                {
			                    break;
			                }
			                jsonResult.append(separator);
			            }
			            jsonResult.append("]");
		        	}
		        	// array of primitives (not a valid return value for current implementation code).
                    // return result as null.
		        	else
		        	{
		        		jsonResult.append("null");
		        	}
		        }
		        else
		        {
		            IJSONSerializable result = (IJSONSerializable)this.result;
		            jsonResult.append(result.toJSON());
		        }
	        } else {
	        	jsonResult.append("null");
	        }
	        
	        jsonResult.append(separator);
	
	        jsonResult.append("\"error\":");
	        if (this.error != null)
	        {
	            RpcError err = (RpcError)error;
	            jsonResult.append(err.toJSON());
	        }
	        else
	        {
	            jsonResult.append("null");
	        }
	        jsonResult.append("}");
		} catch (Exception ex) {
			throw new QasException("InternalerrorException", -32603,
					new String[] { "Internal error" });
		}
        return jsonResult.toString();
	}
}