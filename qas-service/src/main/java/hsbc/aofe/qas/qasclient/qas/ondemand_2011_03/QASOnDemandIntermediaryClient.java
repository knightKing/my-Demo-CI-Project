package hsbc.aofe.qas.qasclient.qas.ondemand_2011_03;

import org.codehaus.xfire.XFireFactory;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;

import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

public class QASOnDemandIntermediaryClient {

	private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName, Endpoint> endpoints;
    private Service service0;

	public QASOnDemandIntermediaryClient()
    {
        endpoints = new HashMap<QName, Endpoint>();
        create0();
        Endpoint QASOnDemandLocalEndpointEP = service0.addEndpoint(new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemandLocalEndpoint"), new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemandLocalBinding"), "xfire.local://OnDemandIntermediaryV2");
        endpoints.put(new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemandLocalEndpoint"), QASOnDemandLocalEndpointEP);
        //Endpoint QASOnDemandEP = service0.addEndpoint(new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemand"), new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemand"), "http://ei-uk-int01:89/ProWebIntermediaryV2/ProOnDemandService.asmx");
        Endpoint QASOnDemandEP = service0.addEndpoint(new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemand"), new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemand"), "https://ws2.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx");
        endpoints.put(new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemand"), QASOnDemandEP);
    }

    public Object getEndpoint(Endpoint endpoint)
    {
    	try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name)
    {
        Endpoint endpoint = (Endpoint)endpoints.get(name);
        if(endpoint == null)
            throw new IllegalStateException("No such endpoint!");
        else
            return getEndpoint(endpoint);
    }

    public Collection<Endpoint> getEndpoints()
    {
        return endpoints.values();
    }

    private void create0()
    {
        org.codehaus.xfire.transport.TransportManager tm = XFireFactory.newInstance().getXFire().getTransportManager();
        HashMap<String, Boolean> props = new HashMap<String, Boolean>();
        props.put("annotations.allow.interface", Boolean.valueOf(true));
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create(QAPortType.class, props);
        @SuppressWarnings("unused")
		org.codehaus.xfire.soap.Soap11Binding soap11binding = asf.createSoap11Binding(service0, new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemandLocalBinding"), "urn:xfire:transport:local");
        soap11binding = asf.createSoap11Binding(service0, new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemand"), "http://schemas.xmlsoap.org/soap/http");
    }

    public QAPortType getQASOnDemandLocalEndpoint()
    {
        return (QAPortType)getEndpoint(new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemandLocalEndpoint"));
    }

    public QAPortType getQASOnDemandLocalEndpoint(String url)
    {
    	QAPortType var = getQASOnDemandLocalEndpoint();
        Client.getInstance(var).setUrl(url);
        return var;
    }

    public QAPortType getQAPortType()
    {
        return (QAPortType)getEndpoint(new QName("http://www.qas.com/OnDemand-2011-03", "QASOnDemand"));
    }

    public QAPortType getQAPortType(String url)
    {
    	QAPortType var = getQAPortType();
        Client.getInstance(var).setUrl(url);
        return var;
    }
    
}
