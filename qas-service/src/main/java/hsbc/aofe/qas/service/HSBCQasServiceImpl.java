package hsbc.aofe.qas.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;

import hsbc.aofe.qas.config.ProxyConfigProperties;
import hsbc.aofe.qas.config.QASConfigProperties;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by sahil.verma on 5/13/2017.
 */
@Slf4j
@Service
public class HSBCQasServiceImpl implements HSBCQasService {

    @Autowired
    QASConfigProperties qasConfigProperties;

    @Autowired
    ProxyConfigProperties proxyConfigProperties;

    @Override
    public String getAddressFor(final String searchForPinCode) throws Exception {
        log.debug("Inside getAddressFor method");
    	String pickList = null;
        BufferedReader br = null;
		HttpClient httpClient = new HttpClient();
        log.debug("getAddressFor : is proxy enabled : " + proxyConfigProperties.getEnableProxy());
		if (proxyConfigProperties.getEnableProxy().equalsIgnoreCase("true")) {
            HostConfiguration config = httpClient.getHostConfiguration();
            config.setProxy(proxyConfigProperties.getProxyHost(), Integer.parseInt(proxyConfigProperties.getProxyPort()));
            log.debug("getAddressFor : proxy host : " + proxyConfigProperties.getProxyHost());
            log.debug("getAddressFor : proxy port : " + proxyConfigProperties.getProxyPort());
            String username = proxyConfigProperties.getProxyUsername();
            String password = proxyConfigProperties.getProxyPassword();
            
            Credentials credentials = new UsernamePasswordCredentials(username, password);
            AuthScope authScope = new AuthScope(proxyConfigProperties.getProxyHost(), Integer.parseInt(proxyConfigProperties.getProxyPort()));        
            httpClient.getState().setProxyCredentials(authScope, credentials);
        }
    	
    
        String soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ond=\"http://www.qas.com/OnDemand-2011-03\"><soapenv:Header><ond:QAQueryHeader><ond:QAAuthentication></ond:QAAuthentication></ond:QAQueryHeader></soapenv:Header><soapenv:Body><ond:QASearch><ond:Country>SGF</ond:Country><ond:Engine Flatten=\"true\" Intensity=\"Close\" Threshold=\"1000\" Timeout=\"10000\">Singleline</ond:Engine><ond:Layout>QADEFAULT</ond:Layout><ond:Search>"+searchForPinCode+"</ond:Search></ond:QASearch></soapenv:Body></soapenv:Envelope>";
        PostMethod methodPost = new PostMethod("https://ws3.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx?WSDL");
        methodPost.setRequestBody(soapRequest);
        methodPost.setRequestHeader("Content-Type", "text/xml");
        methodPost.setRequestHeader("Auth-Token", qasConfigProperties.getLicenseInfo().getAuthToken());
        try {
            int returnCode = httpClient.executeMethod(methodPost);
            log.debug("getAddressFor : status code : " + returnCode);
			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
				methodPost.getResponseBodyAsString();
			} else {
				log.debug("Inside Impl returnCode: " + returnCode);
				br = new BufferedReader(new InputStreamReader(methodPost.getResponseBodyAsStream()));
				String readLine;
				String response = null;
				while (((readLine = br.readLine()) != null)) {
					response = readLine;
				}
				log.debug("getAddressFor : response : " + response);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = null;
				Document doc = null;
				try {
					dBuilder = dbFactory.newDocumentBuilder();
					InputSource inputSource = new InputSource(new StringReader(response));
					doc = dBuilder.parse(inputSource);
					doc.getDocumentElement().normalize();
					log.debug("Root element :" + doc.getDocumentElement().getNodeName());
					log.error("Root element :" + doc.getDocumentElement().getNodeName());
					NodeList nList = doc.getElementsByTagName("PicklistEntry");

					log.debug("Inside Main for loop of DOM parser");
					Node nNode = nList.item(0);
					log.debug("\nCurrent Element :" + nNode.getNodeName());
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						pickList = eElement.getElementsByTagName("Picklist").item(0).getTextContent();

					}
				} catch (ParserConfigurationException | SAXException | IOException e) {
					log.error("Exception : " + e);
				}

			}
        } catch (Exception e) {
        	log.debug("Inside Impl returnCode: Exception" + e);
        } finally {
        	log.debug("Inside Impl returnCode: finally");
            methodPost.releaseConnection();
            if (br != null)  {
            	try {
                    br.close();
                } catch (Exception fe) {
                    log.error("Exception : " + fe);
                }
            }
        }    
        String result = pickList != null ? pickList.split(",")[0] : pickList;
        log.debug("getAddressFor : address retrieved : " + result);
        return toJSON(result);
    }

    private String toJSON(Object object) {
        if (object == null) {
            return "{}";
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "{}";
    }

}