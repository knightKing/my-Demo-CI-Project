package hsbc.aofe.repository.common;

import hsbc.aofe.domain.Applicant;

import java.util.List;

public interface ApplicantRepositoryService {

	Applicant findApplicantById(Long id);

	List<Applicant> findAllApplicants();

	Applicant updateApplicant(String arn, Applicant applicant);

	Applicant updateBeneficiary(String arn, Applicant applicant);

	void deleteApplicantById(Long id);

	List<Applicant> saveApplicant(List<Applicant> applicants, String arn);

	Applicant savePrimaryApplicant(String arn, Applicant applicant);

	void deleteSuppApplicant(String arn, int index);

	void deleteAllSuppApplicants(String arn);

	void saveVerifiedIncome(String arn, int verifiedIncome, boolean isMatch);

	void updateIncomeFlag(String arn, boolean isMatch);

	void updateEmploymentFlag(String arn, boolean isMatch);

}
