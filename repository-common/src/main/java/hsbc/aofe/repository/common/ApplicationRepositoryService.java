package hsbc.aofe.repository.common;

import com.fasterxml.jackson.databind.JsonNode;
import hsbc.aofe.domain.*;
import org.springframework.validation.BindingResult;

import java.io.IOException;
import java.util.List;

public interface ApplicationRepositoryService {

    public List<Application> findAllApplications();

    public Application createApplication(String uid, String channelAuthority, Application application);

    public void deleteApplicationById(String arn);

    public Application findApplicationByARN(String arn);

    public void createPrimaryApplDoc(String arn, String documentGroup, List<Document> documents);
    
    public List<Document> getSuppApplicantDocBySuppId(String arn, long supplimentaryId);

    public void updateSupplementaryApplDoc(String idOfApplication, long supplementaryId, String documentGroup, List<Document> documents);

    public void deletePrimaryApplDoc(String arn, String documentGroup, long index);

    public void deleteSupplementaryApplDoc(String arn, long supplementaryId, String documentGroup, long imageIndex);

    public void saveStaffDetails(String arn, ApplicationStaffDetails staffDetails);
    
    public Image getImageOfSuppl(String arn, long supplimentaryId, long index,String documentGroup);
    
    public Image getImageOfPrimary(String arn, String documentGroup, long index);
    
    Long getApplicationIDByARN(String arn);

    void updateApplicationStatus(String arn, NgStatus status);

    Application updateApplication(String arn, JsonNode patchToApply, BindingResult bindingResult, String channel) throws IOException;
    
    public void updateApplicationWithAoRefNumber(String aoRefNum, String arn);
    
    public String getApplicationStatus(int idType, String idValue);

    void setEssentialDataPreFilledIndicator(boolean essentialDataPreFilledIndicator,String arn);

}
