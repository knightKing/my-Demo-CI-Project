package hsbc.aofe.repository.common;

import java.util.List;

import hsbc.aofe.domain.CreditCard;

/*****************************************
 * Created by sahil.verma on 6/6/2017.
 *****************************************
 */
public interface CCRepositoryService {

    /**
     * @return all the credit card's available in the master-table CREDITCARDMASTER
     */
    List<CreditCard> getAllCreditCards();

	List<CreditCard> getCreditCardsForDigitalJourney();
}
