package hsbc.aofe.repository.common;

import java.util.List;

import hsbc.aofe.domain.DashboardListing;
import hsbc.aofe.domain.Remarks;
import hsbc.aofe.domain.RemarksType;

public interface DashboardListingRepositoryService {

	public DashboardListing findAllApplications(List<String> sort, List<String> filter, String uid, int offset, int pageLimit);

	public String updateFulfilmentAgent(String fulfilmentAgent, String arn);

	public void addRemarks(String arn, Remarks remarks, RemarksType type);

	public List<Remarks> getRemarksByType(String arn, RemarksType type);
}
