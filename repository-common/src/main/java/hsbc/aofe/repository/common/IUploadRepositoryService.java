package hsbc.aofe.repository.common;

import java.util.List;

import hsbc.aofe.iupload.domain.IUploadDetail;
import hsbc.aofe.iupload.domain.IUploadRequestData;

public interface IUploadRepositoryService {

    /**
     * This method is used to return ARNS from
     * {@link hsbc.aofe.iupload.entities.CCApplicationEntity} based on the input
     * status
     *
     * @param status
     * @return List of ARN
     */
    List<String> getARNsExcludingIUploadARNs(String status);

	/**
	 * This method is used to POST IUploadRequestBody data to IUploadEntity
	 * 
	 * @param iUploadRequestBody
	 */
	void saveStatus(IUploadRequestData iUploadRequestBody);

	/**
	 * This method is used to return IUploadDetail
	 * 
	 * @param arn
	 * @return
	 */
	IUploadDetail findApplicationByARN(String arn); 

}
