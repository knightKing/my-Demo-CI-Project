package hsbc.aofe.repository.common;


import java.util.List;

import hsbc.aofe.iccm.domain.NotificationRequest;

public interface IccmRepositoryService {

    void saveIccm(String event, String arn, String channel, String uid, List<String> remarks, NotificationRequest notificationRequest);
    
    public NotificationRequest getIccmNotification(String event, String arn, String channel, String uid, List<String> remarks);
    
    void saveFollowUpApplications();
    
    List<NotificationRequest> getFollowUpApplications();
    
    void updateSuccessIccm(NotificationRequest notificationRequest);
    
    void updateFailedIccm(NotificationRequest notificationRequest);

}
