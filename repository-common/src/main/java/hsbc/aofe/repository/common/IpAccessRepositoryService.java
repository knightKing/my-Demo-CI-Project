package hsbc.aofe.repository.common;

import java.sql.Timestamp;
import java.util.List;

import hsbc.aofe.domain.IpAccessDetails;

public interface IpAccessRepositoryService {

	public void saveIpAddressDetails(String ipAddress, Timestamp accessTime, String url) ;
	
	public List<IpAccessDetails> getIpAddressDetails(String ipAddress);

}
