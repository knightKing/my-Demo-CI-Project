package hsbc.aofe.repository.common;

import hsbc.aofe.domain.VerifiedIncomeCalcParameters;

public interface OcrRepositoryService {
	public VerifiedIncomeCalcParameters getOcrdata(String arn);
			
}
