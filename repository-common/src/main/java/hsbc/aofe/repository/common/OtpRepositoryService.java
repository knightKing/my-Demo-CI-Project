package hsbc.aofe.repository.common;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface OtpRepositoryService {

	public String otpValidate(String arn, String otp);

	public String generateOneTimePassword(String arn) throws InvalidKeyException, NoSuchAlgorithmException;
	
}
