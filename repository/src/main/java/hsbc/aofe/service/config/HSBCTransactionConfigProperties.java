package hsbc.aofe.service.config;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "hsbc.hsbc-hibernate-config")
@Validated
public class HSBCTransactionConfigProperties {

    @NotBlank
    private String dbDialect;
    @NotBlank
    private String dbShowSql;
    @NotBlank
    private String dbFormatSql;
    @NotBlank
    private String dbUseSqlComments;
    private String dbHbm2ddlAuto;

}
