package hsbc.aofe.service.config;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class JPATransactionConfig {

    @Autowired
    private DataSource dataSource;
    @Autowired
    HSBCTransactionConfigProperties transactionConfigProperties;


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setJpaProperties(hibernateProperties());
        entityManagerFactoryBean.setPackagesToScan(new String[]{"hsbc.aofe.service"});
        return entityManagerFactoryBean;
    }

    @Bean(name = "transactionManager")
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", transactionConfigProperties.getDbDialect());
        properties.put("hibernate.show_sql", transactionConfigProperties.getDbShowSql());
        properties.put("hibernate.format_sql", transactionConfigProperties.getDbFormatSql());
        properties.put("hibernate.use_sql_comments", transactionConfigProperties.getDbShowSql());
        if (StringUtils.isNotEmpty(transactionConfigProperties.getDbHbm2ddlAuto()))
            properties.put("hibernate.hbm2ddl.auto", transactionConfigProperties.getDbHbm2ddlAuto());
        return properties;
    }

}
