package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "ARNCCBRIDGE", catalog = "")
public class ARNCcBridgeEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3317141965010438024L;

	@EmbeddedId
	private ARNCcBridgePK primaryKey;

	private Long limit;
	private String nameOnCard;
	private String bankToAssignCreditLimit;
	private String acTfrBeneficiaryAcName;
	@Column(length = 19)
	private String acTfrBeneficiaryAcNumber;
	private String acTfrBeneficiaryBankName;
	private String isSelected;

	@Column(precision = 20, scale = 0)
	private BigInteger amtToBeTransfered;
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ccIssued", referencedColumnName = "id")
	private CreditCardMasterEntity creditCardIssuedByCcId;

}
