package hsbc.aofe.service.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

/*****************************************
 * Created by shreya on 5/31/2017.
 *****************************************
 */

@Data
@Embeddable
public class ARNCcBridgePK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4995976817127286356L;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ccId", referencedColumnName = "id")
	private CreditCardMasterEntity creditCardMasterByCcId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "applicantId", referencedColumnName = "id")
	private ApplicantDetailsEntity applicantByApplicantId;

}
