package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "ADDRESS", catalog = "")
public class AddressEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7438191955203014116L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String doorNumber;
    private String l2Street;
    private String l3Unit;
    private String city;
    private String postalCode;
    private LocalDateTime createdOn;
    private String createdBy;
    private LocalDateTime lastModifiedOn;
    private String lastModifiedBy;

    @ManyToOne
    @JoinColumn(name = "country", referencedColumnName = "id")
    private CountryListMasterEntity countryListMasterByCountry;

    @OneToMany(mappedBy = "primaryKey.addressId", cascade = CascadeType.ALL)
    private List<ApplicantAddressEntity> applicantAddressesById;

}
