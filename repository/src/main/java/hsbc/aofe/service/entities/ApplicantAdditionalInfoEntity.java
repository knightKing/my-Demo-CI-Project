package hsbc.aofe.service.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@Entity
@EqualsAndHashCode(exclude = {"applicantByAplicantId"})
/*@Check(constraints = "isMultipleNationality in ('N','Y') AND "
        + "maritalStatus in ('Single','Married','Widowed','Divorced') AND "
		+ "correspondenceOn in ('Residential','Office') AND " + "holdingPublicPosition in ('Y','N')")*/
@Table(name = "APPLICANTADDITIONALINFO", catalog = "")
public class ApplicantAdditionalInfoEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8148535592761559731L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String isMultipleNationality;
    private String nationality2;
    private String nationality3;
    private String taxResidenceCountry1;
    private String taxResidenceCountry2;
    private String taxResidenceCountry3;
    private Long noOfDependents;
    /* private Long educationLevel; */
    private String maritalStatus;
    /* private Long homeownership; */
    private String correspondenceOn;

    private String dialCodeHomePhone;
    /* @Pattern(regexp = "[[:digit:]]{15}") */
    private Long homePhone;
    /*
     * @Pattern(regexp = "[[:digit:]]{15}")
     */
    private Long officePhone;
    private Long officeExtn;
    /* private Long employmentType; */

    @Column(name = "ISRESIADDSAMEASPRIMARY")
    private String isResidentialAddressSameAsPrimary;

    @Column(name = "IS_RES_ADDS_SAMEAS_PERMANENT")
    private String residentialAddIdenticalToPermAddress;

    private Long monthsAtResidentialAddress;
    private Long monthsToEmpPassExpiry;
    private Long annualIncome;
    private Long lengthOfService;
    private String occupation;
    private String position;
    private String industryType;
    private String companyName;
    private String officeAddress;
    private Long officePostalCode;
    private Long timeAtPreviousEmployer;
    private String holdingPublicPosition;
    private String publicPositionDetails;
    private String autoDebitFromAccountNumber;
    private String mother_MaidenName;
    private Integer verifiedIncome;

    @ManyToOne
    @JoinColumn(name = "PASSPORT_COUNTRY", referencedColumnName = "id")
    private CountryListMasterEntity passportCountry;

    @Column(name = "PASSPORT_NUMBER")
    private String passportNumber;

    @Column(name = "ASSOCIATEOFPUBPOSITIONHOLDER")
    private String associateOfPublicPositionHolder;

    @Column(name = "ASSOCIATEOFPUBPOSHOLDETAILS")
    private String associateOfPublicPositionHolderDetails;
    private String introducedByHSBCCardHolder;

    @Column(name = "INTRODUCEDBYHSBCREFID")
    private String introducedByHSBCCardHolderReferrerID;
    private LocalDateTime createdOn;
    private String createdBy;
    private LocalDateTime lastModifiedOn;
    private String lastModifiedBy;


    @OneToOne
    @JoinColumn(name = "ApplicantId")
    private ApplicantDetailsEntity applicantByAplicantId;

    @ManyToOne
    @JoinColumn(name = "educationlevel", referencedColumnName = "id")
    private EducationalLevelMasterEntity educationallevelmasterByEducationlevel;

    @ManyToOne
    @JoinColumn(name = "homeownership", referencedColumnName = "id")
    private HomeOwnershipMasterEntity homeownershipmasterByHomeownership;

    @ManyToOne
    @JoinColumn(name = "employmenttype", referencedColumnName = "id")
    private JobTypeMasterEntity jobtypemasterByEmploymenttype;

}
