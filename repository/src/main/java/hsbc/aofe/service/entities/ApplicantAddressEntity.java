package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*****************************************
 * Created by sahil.verma on 5/24/2017.
 * Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@EqualsAndHashCode(exclude = {"primaryKey"})
@Entity
@Table(name = "APPLICANT_ADDRESS", catalog = "")
public class ApplicantAddressEntity implements Serializable {

    private static final long serialVersionUID = 1388183646984829491L;

    @EmbeddedId
    private ApplicantAddressPK primaryKey;

    @ManyToOne
    @JoinColumn(name = "type", referencedColumnName = "id")
    private AddressTypeMasterEntity type;

    @Column(name = "CREATED_ON")
    private LocalDateTime createdOn;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "LAST_MODIFIED_ON")
    private LocalDateTime lastModifiedOn;

    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

}
