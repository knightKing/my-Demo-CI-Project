package hsbc.aofe.service.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*****************************************
 * Created by shreya on 5/30/2017.
 *****************************************
 */

@Data
@EqualsAndHashCode( exclude = { "addressId", "applicationId", "applicantId"})
@Embeddable
public class ApplicantAddressPK implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = -5558522415464059721L;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "addressId", referencedColumnName = "id")
	private AddressEntity addressId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "applicationId", referencedColumnName = "id")
	private CCApplicationEntity applicationId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "applicantId", referencedColumnName = "id")
	private ApplicantDetailsEntity applicantId;
}
