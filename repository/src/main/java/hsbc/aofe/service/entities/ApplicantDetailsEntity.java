package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017. 
 * Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@Entity
/*@Check(constraints = "gender in ('N','Y')")*/
@Table(name = "APPLICANTDETAILS", catalog = "")
public class ApplicantDetailsEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4294661321662779326L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String salutation;
    private String email; //regexp_like(email, '[[:alnum:]]+@[[:alnum:]]+\.[[:alnum:]]')
    private String dialCodeMobile;
    private Long mobile; //regexp_like(mobile, '[[:digit:]]{10}')
    private String nationality;
    private String firstName;
    private String middleName;
    private String lastName;
    private String otherName;
    private String gender;
    private LocalDate dateOfBirth;
    /*private Long countryOfBirth;*/
    /*private Long idDoctype;*/
    private String idValue;
    /*private Long countryOfIdIssue;*/
    private LocalDate idIssueDate;
    private LocalDate idExpiryDate;
    private String consentContactMe;
    private String consentPersonalData;
    private String consentMarketing;
    private String consentAccountOpening;
    private String sharedCreditLimit;
    private String balanceTransferOnCreditCard;
    private LocalDateTime createdOn;
    private String createdBy;
    private LocalDateTime lastModifiedOn;
    private String lastModifiedBy;

    @OneToOne(mappedBy = "applicantByAplicantId", cascade = CascadeType.ALL)
    private ApplicantAdditionalInfoEntity appAddInfo;

    @ManyToOne
    @JoinColumn(name = "countryOfBirth", referencedColumnName = "id")
    private CountryListMasterEntity countryListMasterByCountryOfBirth;

    @ManyToOne
    @JoinColumn(name = "countryOfIdIssue", referencedColumnName = "id")
    private CountryListMasterEntity countryListMasterByCountryOfIdIssue;

    @ManyToOne
    @JoinColumn(name = "idDoctype", referencedColumnName = "id")
    private DocumentsMasterEntity documentsMasterByIdDocType;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "applicantId", referencedColumnName = "id")
    private List<AddressEntity> addressesById;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "applicantDetailsByApplicantId")
    private List<CCApplicationEntity> ccapplicationsById;


    @OneToMany(mappedBy = "primaryKey.applicantId", cascade = CascadeType.MERGE)
    private List<SuppapplicantEntity> suppApplicantsById;

    @OneToMany(mappedBy = "primaryKey.applicantId", cascade = CascadeType.ALL)
    private List<ApplicantAddressEntity> applicantAddressesById;

    @OneToMany(mappedBy = "primaryKey.applicantByApplicantId", cascade = CascadeType.ALL)
    private List<ARNCcBridgeEntity> arnCcBridgesById;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "applicantId", referencedColumnName = "id")
    private List<ApplicantDocumentEntity> applicantDocumentsById;
}
