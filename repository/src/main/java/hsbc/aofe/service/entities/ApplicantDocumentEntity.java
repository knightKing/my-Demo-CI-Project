package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name = "APPLICANT_DOCUMENT", catalog = "")
public class ApplicantDocumentEntity implements Serializable {


    private static final long serialVersionUID = -3940258916154140185L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long noOfImages;
    private String remarks;
    private LocalDateTime createdOn;
    private String createdBy;
    private LocalDateTime lastModifiedOn;
    private String lastModifiedBy;


    @ManyToOne
    @JoinColumn(name = "documentType", referencedColumnName = "id")
    private DocumentsMasterEntity documentsMasterByDocumentType;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "documentId", referencedColumnName = "id")
    private List<ImageEntity> imageById;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "appDocId", referencedColumnName = "id")
    private List<DocumentExtractionEntity> documentExtractionEntity;


}
