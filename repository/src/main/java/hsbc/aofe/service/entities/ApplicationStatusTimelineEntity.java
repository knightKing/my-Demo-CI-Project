package hsbc.aofe.service.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "APPLICATIONSTATUSTIMELINE", catalog = "")
public class ApplicationStatusTimelineEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1121655716281390716L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /* private Long applicationId; */
    /* private Long ngStatus; */
    private String remarks;
    private LocalDateTime createdOn;
    private String createdBy;
    private String assignedTo;
    private String ao_status;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ngStatus", referencedColumnName = "id")
    private NGStatusMasterEntity ngStatusMasterByNgStatus;

    @ManyToOne
    @JoinColumn(name = "applicationId", referencedColumnName = "id")
    private CCApplicationEntity ccApplicationEntity;


    @Override
    public String toString() {
        return "ApplicationStatusTimelineEntity{" +
                "id=" + id +
                ", remarks='" + remarks + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", ngStatusMasterByNgStatus=" + ngStatusMasterByNgStatus +
                '}';
    }
}
