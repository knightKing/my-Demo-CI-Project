package hsbc.aofe.service.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017.
 *****************************************
 */

/*@Data
@EqualsAndHashCode(exclude = {"applicationByAplicationId"})*/

@Getter
@Setter
@NoArgsConstructor
@Entity
/*@Check(constraints = "allocateExistingLimitToNew in ('N','Y') AND " + "cancelExistingCard in ('N','Y') AND "
        + "createAutoDebitFromAccount in ('N','Y') AND " + "shouldWeContactForBalanaceTfr in ('N','Y')")*/
@Table(name = "CCAPPLICATIONADDITIONALINFO", catalog = "")
public class CCApplicationAdditionalInfoEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4611145929215265297L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String allocateExistingLimitToNew;
    private String existingCcnumber;
    private String atmLinkedAccount;
    private String createAutoDebitFromAccount;
    private Long howMuchAutoDebit;
    private String receiveEStatement;
    private String agreeToGenericConsent;
    private String transactingOnMyOwn;
    private String wouldLikeToOpenNewAccount;
    private LocalDateTime createdOn;
    private String createdBy;
    private LocalDateTime lastModifiedOn;
    private String lastModifiedBy;
    private String incomeAutomatedCalcIndicator;
    private String employmentCheckIndicator;
    private String documentCompleteIndicator;
    private String essentialDataIndicator;

    @OneToOne
    @JoinColumn(name = "ApplicationId")
    private CCApplicationEntity applicationByAplicationId;

}
