package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@EqualsAndHashCode(exclude = { "applicantDetailsByApplicantId" })
@Entity
/*
 * @Check(constraints = "limitNeededForHowManyCards in (0,1,2) AND " +
 * "ao_status in ('CREATED','APPROVED','REJECTED','SUBMITTED-TO-AO','INPROCESS-TO-NG')"
 * )
 */
@Table(name = "CCAPPLICATION", catalog = "")
public class CCApplicationEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3065416395782266955L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String arn;
	// private Long applicantId;
	private String referalId;
	private String ao_status;
	private String aoRefNum;
	private LocalDateTime aoSubmissionDate;
	private String fulfilmentAgent;
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

	@OneToOne(mappedBy = "applicationByAplicationId", cascade = CascadeType.ALL)
	private CCApplicationAdditionalInfoEntity ccAppAddInfo;

	@ManyToOne
	@JoinColumn(name = "applicantId", referencedColumnName = "id")
	private ApplicantDetailsEntity applicantDetailsByApplicantId;

	@OneToMany(mappedBy = "primaryKey.applicationId", cascade = CascadeType.MERGE)
	private List<SuppapplicantEntity> suppApplicantsById;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "initiatedBy", referencedColumnName = "id")
	private ChannelMasterEntity channelMasterById;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "ccApplicationEntity")
	@Column(name = "applicationId")
	private List<ApplicationStatusTimelineEntity> ccapplicationsStatusById;

	@OneToMany(mappedBy = "primaryKey.applicationId", cascade = CascadeType.ALL)
	private List<ApplicantAddressEntity> applicantAddressesById;

	/*@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "applicationId", referencedColumnName = "id")*/
	@OneToOne(mappedBy = "ccapplicationStaffById", cascade = CascadeType.ALL)
	private StaffDetailsEntity staffByApplicationId;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "ccApplicationEntity")
	@Column(name = "arn")
	private List<RemarksEntity> remarksByarn;
	
}
