package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Check;

import lombok.Data;

/*****************************************
 * Created by shreya on 6/1/2017.
 *****************************************
 */

@Data
@Entity
/*@Check(constraints = "name in ('CUSTOMER','BRANCH','ROADSHOW','TELESALES','TPSA')")*/
@Table(name = "CHANNELMASTER", catalog = "")
public class ChannelMasterEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6505333026130252024L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String remarks;
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

}
