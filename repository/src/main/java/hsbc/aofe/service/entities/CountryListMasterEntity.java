package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017
 *****************************************
 */

@Data
@Entity
@Table(name = "COUNTRYLISTMASTER", catalog = "")
public class CountryListMasterEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1980068713772208796L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String code;
	private String name;
	private Time createdOn;
	private String createdBy;
	private Time lastModifiedOn;
	private String lastModifiedBy;

}
