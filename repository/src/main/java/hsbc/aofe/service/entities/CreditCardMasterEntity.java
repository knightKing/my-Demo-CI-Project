package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "CREDITCARDMASTER", catalog = "")
public class CreditCardMasterEntity implements Serializable {

	private static final long serialVersionUID = 4205744802288372552L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, precision = 0)
	private long id;

	@Basic
	@Column(name = "KEY", nullable = true, length = 10)
	private String key;

	@Basic
	@Column(name = "NAME", nullable = true, length = 500)
	private String name;

	@Basic
	@Column(name = "TYPE", nullable = true, length = 300)
	private String type;

	@Basic
	@Column(name = "IMAGEURL", nullable = true, length = 500)
	private String imageUrl;

	@Basic
	@Column(name = "ANNUAL_FEE", nullable = true, precision = 0)
	private Long annualFee;

	@Basic
	@Column(name = "MIN_SALARY", nullable = true, precision = 0)
	private Long minSalary;

	@Basic
	@Column(name = "REWARD_POINTS", nullable = true, precision = 0)
	private Long rewardPoints;

	@Basic
	@Column(name = "REWARD_POINT_SCALE", nullable = true, precision = 0)
	private Long rewardPointScale;

	@Basic
	@Column(name = "COMPLIMENTARY_STUFF", nullable = true, length = 20)
	private String complimentaryStuff;

	@Basic
	@Column(name = "ANNUAL_FEE_CURRENCY", nullable = true, length = 10)
	private String annualFeeCurrency;

	@Basic
	@Column(name = "LIMIT", nullable = true, precision = 0)
	private Long limit;

	@Basic
	@Column(name = "LIMIT_MINIMUM_OF_INFO", nullable = true, precision = 0)
	private Long limitMinimumOfInfo;

	@Basic
	@Column(name = "CREATEDBY", nullable = true, length = 100)
	private String createdBy;

	@Basic
	@Column(name = "CREATEDON", nullable = true)
	private LocalDateTime createdOn;

	@Basic
	@Column(name = "LASTMODIFIEDBY", nullable = true, length = 100)
	private String lastModifiedBy;

	@Basic
	@Column(name = "LASTMODIFIEDON", nullable = true)
	private LocalDateTime lastModifiedOn;
	
	@Basic
	@Column(name = "ELIGIBLEFORCUSTOMER", nullable = true)
	private String eligibleForCustomer;

	
	@OneToMany(mappedBy = "primaryKey.creditCardMasterByCcId", cascade = CascadeType.ALL)
	private List<ARNCcBridgeEntity> arnccBridgesById;

}
