package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "DOCGROUPMASTER", catalog = "")
public class DocGroupMasterEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7498754828532132152L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String remarks;
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

	@OneToMany(mappedBy = "docGroupMasterByDocGroup", cascade = CascadeType.ALL)
	private List<DocumentsMasterEntity> documentsMastersById;

	@Override
	public String toString() {
		return "DocGroupMasterEntity{" +
				"id=" + id +
				", name='" + name + '\'' +
				", remarks='" + remarks + '\'' +
				", createdOn=" + createdOn +
				", createdBy='" + createdBy + '\'' +
				", lastModifiedOn=" + lastModifiedOn +
				", lastModifiedBy='" + lastModifiedBy +
				'}';
	}
}
