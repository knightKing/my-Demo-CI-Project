package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Check;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 5/26/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "DOCUMENT_EXTRACTION", catalog = "")
public class DocumentExtractionEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4826567752359653704L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private Long appDocId;
	private String fieldName;
	private String extractedValue;
	private String isChanged;
	private String changedValue;
	private String changedBy;
	private LocalDateTime changedOn;
	private LocalDateTime createdOn;
	private String createdBy;

/*	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id")
	private ApplicantDocumentEntity id;*/

}
