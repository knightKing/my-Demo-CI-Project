package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Check;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017. Updated by shreya on 6/12/2017.
 *****************************************
 */

@Data
@Entity
/*@Check(constraints = "extractionRequired in ('N','Y')")*/
@Table(name = "DOCUMENTSMASTER", catalog = "")
public class DocumentsMasterEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8649978431388465276L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String key;
	/* private Long docGroup; */
	private String extractionRequired;
	private Time createdOn;
	private String createdBy;
	private Time lastModifiedOn;
	private String lastModifiedBy;

	/*@OneToMany(mappedBy = "documentsMasterByIdDocType", cascade = CascadeType.ALL)
	private List<ApplicantDetailsEntity> applicantDetailsById;*/

	/*@OneToMany(mappedBy = "documentsMasterByDocumentType", cascade = CascadeType.ALL)
	private List<ApplicationDocumentEntity> applicationDocumentsById;*/

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "docGroup", referencedColumnName = "id")
	private DocGroupMasterEntity docGroupMasterByDocGroup;

}
