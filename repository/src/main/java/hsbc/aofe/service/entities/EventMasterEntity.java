package hsbc.aofe.service.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "EVENTMASTER", catalog = "")
public class EventMasterEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1432863885566095712L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "event_Key")
	private String eventKey;

	@ManyToOne
	@JoinColumn(name = "template_Id", referencedColumnName = "id")
	private TemplateMasterEntity templateMasterEntity;

}
