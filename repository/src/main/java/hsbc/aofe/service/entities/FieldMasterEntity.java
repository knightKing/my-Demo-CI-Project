package hsbc.aofe.service.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "FIELDMASTER", catalog = "")
public class FieldMasterEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5190693010553585269L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "FIELD_NAME")
	private String fieldName;
}
