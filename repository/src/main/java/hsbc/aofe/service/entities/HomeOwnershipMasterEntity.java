package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "HOMEOWNERSHIPMASTER", catalog = "")
public class HomeOwnershipMasterEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2142813393055453826L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String label;
	private String remarks;
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

}
