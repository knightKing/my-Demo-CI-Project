package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "IUpload", catalog = "")
public class IUploadEntity implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 5430719597324968926L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String arn;
	private String uploadstatus;
	private String foldername;
	private LocalDateTime createdon;

	/*@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "arn", referencedColumnName = "arn")
	private CCApplicationEntity iUploadArn;
*/
}
