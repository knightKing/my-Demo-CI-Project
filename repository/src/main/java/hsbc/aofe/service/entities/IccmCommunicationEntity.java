package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import hsbc.aofe.domain.IccmCommunicationStatus;
import lombok.Data;

@Data
@Entity
@Table(name = "ICCMCOMMUNICATION", catalog = "")
public class IccmCommunicationEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -835923192680800958L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String arn;
	private Long mobile;
	private String email;
	private String communicationText;
	private LocalDateTime sendWhen;
	
	@Enumerated
	private IccmCommunicationStatus status;
	
	private int retryAttempts;
	private int priority;

}
