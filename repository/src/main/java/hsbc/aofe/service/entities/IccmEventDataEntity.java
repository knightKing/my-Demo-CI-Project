package hsbc.aofe.service.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "ICCM_EVENT_DATA", catalog = "")
public class IccmEventDataEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7259046581214896133L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String value;

    @ManyToOne
    @JoinColumn(name = "TEMPLATE_FIELD_ID", referencedColumnName = "id")
    private TemplateFieldsEntity templateFieldsEntity;

}
