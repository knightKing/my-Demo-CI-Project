package hsbc.aofe.service.entities;

import hsbc.aofe.iccm.domain.IccmCommunicationStatus;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@NamedStoredProcedureQuery(name = "GenerateChaserEvents", 
procedureName = "GenerateChaserEvents",
parameters = {
   @StoredProcedureParameter(mode = ParameterMode.IN, name = "Chaser1Frequency", type = Long.class),
   @StoredProcedureParameter(mode = ParameterMode.IN, name = "Chaser2Frequency", type = Long.class),
   @StoredProcedureParameter(mode = ParameterMode.IN, name = "Chaser3Frequency", type = Long.class),
   @StoredProcedureParameter(mode = ParameterMode.IN, name = "CancellationFrequency", type = Long.class),
})	  
@Table(name = "ICCM_EVENT", catalog = "")
public class IccmEventEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -393703978948651559L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated
    private IccmCommunicationStatus status;

    @Column(name = "RETRY_COUNT")
    private Long retryCount;

    @Column(name = "SEND_WHEN")
    private LocalDateTime sendWhen;

    @CreationTimestamp
    private Timestamp createdOn;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ICCM_EVENT_ID", referencedColumnName = "id")
    private List<IccmEventDataEntity> eventData;

    @ManyToOne
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "id")
    private EventMasterEntity eventMasterEntity;

    @ManyToOne
    @JoinColumn(name = "ARN", referencedColumnName = "arn")
    private CCApplicationEntity ccApplicationEntityByArn;

    @ManyToOne
    @JoinColumn(name = "INITIATED_BY", referencedColumnName = "id")
    private ChannelMasterEntity channelMasterById;

    @Column(name = "STAFF_ID")
    private String staffId;
    
    @Column(name = "TRACKING_FIELD")
    private String trackingField;
    
    @Column( name = "NOTIFICATION_TYPE")
    private String notificationType;

}
