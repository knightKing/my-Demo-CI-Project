package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "IPACCESSDETAILS", catalog = "")
public class IpAccessDetailsEntity  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2353380612100228899L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String ipAddress;
	private String url;
	private Timestamp accessTime;

}
