package hsbc.aofe.service.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "IPBLOCKSTATUS", catalog = "")
public class IpBlockStatusEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7317123147330730631L;


	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    private String ipAddress;
    private String isBlocked;
    
}
