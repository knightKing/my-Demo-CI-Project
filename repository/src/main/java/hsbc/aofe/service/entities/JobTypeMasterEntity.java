package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017.
 *****************************************
 */

@Data
@Entity
@Table(name = "JOBTYPEMASTER", catalog = "")
public class JobTypeMasterEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7235446040288803639L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String code;
	private String name;
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

}
