package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Check;

import lombok.Data;

/*****************************************
 * Created by sahil.verma on 5/24/2017.
 *****************************************
 */

@Data
@Entity
/*@Check(constraints = "key in ('IN-PROGRESS','SUBMITTED-TO-AO','REJECTED','SAVED','PURGED')")*/
@Table(name = "NGSTATUSMASTER", catalog = "")
public class NGStatusMasterEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9002818444253688337L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String key;
	private String remarks;
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

}
