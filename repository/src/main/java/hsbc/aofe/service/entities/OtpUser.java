package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "OTP")
public class OtpUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4879056780062364181L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String arn;
	
	@Column(name="OTPGEN")		
	private String otp;
	
	
	@Column(name="TIME")
	private LocalDateTime createdOn;
	private String flag;

}
