package hsbc.aofe.service.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "REMARKS", catalog = "")
public class RemarksEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4094862315746437481L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String dateTime;
	private String remarks;
	private String staffId;
	private String type;

	@ManyToOne
	@JoinColumn(name = "arn", referencedColumnName = "arn")
	private CCApplicationEntity ccApplicationEntity;
}
