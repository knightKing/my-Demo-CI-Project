/**
 * 
 */
package hsbc.aofe.service.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author newgen
 *
 */
@Data
@EqualsAndHashCode( exclude = "ccapplicationStaffById" )
@Entity
@Table(name = "STAFFDETAILS", catalog = "")
public class StaffDetailsEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5476653917528835420L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String sourceCode;
	private String voucherId;
	private String staffId;
	private String ghoCode;
	private String marketSectorCode;
	private String splInstIndicator;
	private Long preApprovedLimit;
	private String location;
	private String ecpfAuthorized;
	private String referralId; 
	
	@OneToOne
    @JoinColumn(name = "ApplicationId")
    private CCApplicationEntity ccapplicationStaffById;

}
