package hsbc.aofe.service.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import hsbc.aofe.domain.RelationshipWithPrimary;
import lombok.Data;

@Data
@Entity
@Table(name = "SUPPAPPLICANTS", catalog = "")
public class SuppapplicantEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 882382943423391941L;

	@EmbeddedId
	private SuppapplicantPK primaryKey;
	private String relationWithPrimaryApplicant;
	/* private String ccId; */
	private LocalDateTime createdOn;
	private String createdBy;
	private LocalDateTime lastModifiedOn;
	private String lastModifiedBy;

}
