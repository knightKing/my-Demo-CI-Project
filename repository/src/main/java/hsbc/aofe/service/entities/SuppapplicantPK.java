package hsbc.aofe.service.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*****************************************
 * Created by shreya on 5/31/2017.
 *****************************************
 */

@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class SuppapplicantPK implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1043967034292726485L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "applicationId", referencedColumnName = "id")
    private CCApplicationEntity applicationId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "applicantId", referencedColumnName = "id")
    private ApplicantDetailsEntity applicantId;

}
