package hsbc.aofe.service.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "TEMPLATEFIELDSMASTER", catalog = "")
public class TemplateFieldsEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6830118589214956009L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "template_Id", referencedColumnName = "id")
	private TemplateMasterEntity templateMasterEntity;

	@ManyToOne
	@JoinColumn(name = "field_Id", referencedColumnName = "id")
	private FieldMasterEntity fieldMasterEntity;

}
