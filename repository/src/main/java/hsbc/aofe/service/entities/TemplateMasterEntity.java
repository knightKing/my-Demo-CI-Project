package hsbc.aofe.service.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "TEMPLATEMASTER", catalog = "")
public class TemplateMasterEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -770555131438675971L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String key;
	
}
