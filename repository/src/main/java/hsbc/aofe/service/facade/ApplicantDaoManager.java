package hsbc.aofe.service.facade;

import hsbc.aofe.domain.Applicant;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.Document;
import hsbc.aofe.service.entities.ApplicantDetailsEntity;
import hsbc.aofe.service.entities.ApplicantDocumentEntity;
import hsbc.aofe.service.entities.CCApplicationEntity;

import java.util.List;

public interface ApplicantDaoManager {

    Application manageFindApplicantById(Long id);

    List<Application> manageFindAllApplicants();

    Applicant manageUpdateApplicant(String arn, Applicant applicant);

    Applicant manageUpdateBeneficiary(String arn, Applicant applicant);

    void manageDeleteApplicantById(Long id);

    Application createApplicantUsingApplication(String uid, String channelAuthority, Application application);

    List<ApplicantDetailsEntity> findApplicantByIdTypeAndIdValueAndDialCodeMobileAndMobileAndEmail
            (String idType, String idValue, String dialCodeMobile, Long mobile, String email);

    List<Applicant> manageSaveApplicant(List<Applicant> applicants, String arn);

    List<ApplicantDocumentEntity> createPrimaryApplDoc(String arn, String documentGroup, List<Document> documents);

    List<ApplicantDocumentEntity> createSupplementaryApplDoc(String idOfApplication, long supplementaryId, String documentGroup, List<Document> documents);

    Applicant savePrimaryApplicant(String arn, Applicant applicant);

    ApplicantDetailsEntity updateApplicantWithAddressAndARNCCBridgeEntry
            (Applicant applicant, ApplicantDetailsEntity applicantDetailsEntity, CCApplicationEntity ccApplicationEntity);

    void deleteSuppApplicant(String arn, int index);

    List<ApplicantDetailsEntity> findApplicantByIdTypeAndIdValue(String idType, String idValue);

    void deleteAllSuppApplicants(String arn);

    void saveVerifiedIncome(String arn, int verifiedIncome, boolean isMatch);

    void updateIncomeFlag(String arn, boolean isMatch);

    void updateEmploymentFlag(String arn, boolean isMatch);

}
