package hsbc.aofe.service.facade;

import com.google.common.collect.Lists;
import hsbc.aofe.domain.*;
import hsbc.aofe.exception.SameSupplementaryAndPrimaryIdValueException;
import hsbc.aofe.service.entities.*;
import hsbc.aofe.service.jparepository.*;
import hsbc.aofe.service.transformer.Transformation;
import hsbc.aofe.util.CommonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;
import java.util.stream.Collectors;


@Repository
public class ApplicantDaoManagerImpl implements ApplicantDaoManager {

    private ApplicantDetailsRepository applicantRepository;
    private ApplicantAdditionalInfoRepository applicantAdditionalInfoRepository;
    private ApplicantAddressRepository applicantAddressRepository;
    private CreditCardMasterRepository creditCardMasterRepository;
    private ArnCcBridgeRepository arnCcBridgeRepository;
    private EducationalLevelMasterRepository educationalLevelMasterRepository;
    private HomeOwnershipMasterRepository homeOwnershipMasterRepository;
    private JobTypeMasterRepository jobTypeMasterRepository;
    private AddressTypeMasterRepository addressTypeMasterRepository;
    private CountryListMasterRepository countryListMasterRepository;
    private NGStatusMasterRepository ngStatusMasterRepository;
    private DocumentsMasterRepository documentsMasterRepository;
    private CCApplicationRepository ccApplicationRepository;
    private SuppApplicantRepository suppApplicantRepository;
    private CCApplicationAdditionalInfoRepository ccApplicationAdditionalInfoRepository;
    private AddressRepository addressRepository;
    private ChannelMasterRepository channelMasterRepository;
    private TransactionTemplate transactionTemplate;

    @Autowired
    public ApplicantDaoManagerImpl(ApplicantDetailsRepository applicantRepository,
                                   ApplicantAdditionalInfoRepository applicantAdditionalInfoRepository,
                                   ApplicantAddressRepository applicantAddressRepository,
                                   CreditCardMasterRepository creditCardMasterRepository, ArnCcBridgeRepository arnCcBridgeRepository,
                                   EducationalLevelMasterRepository educationalLevelMasterRepository,
                                   HomeOwnershipMasterRepository homeOwnershipMasterRepository,
                                   JobTypeMasterRepository jobTypeMasterRepository, AddressTypeMasterRepository addressTypeMasterRepository,
                                   CountryListMasterRepository countryListMasterRepository, NGStatusMasterRepository ngStatusMasterRepository,
                                   DocumentsMasterRepository documentsMasterRepository, CCApplicationRepository ccApplicationRepository,
                                   SuppApplicantRepository suppApplicantRepository,
                                   CCApplicationAdditionalInfoRepository ccApplicationAdditionalInfoRepository,
                                   AddressRepository addressRepository, ChannelMasterRepository channelMasterRepository,
                                   TransactionTemplate transactionTemplate) {
        this.applicantRepository = applicantRepository;
        this.applicantAdditionalInfoRepository = applicantAdditionalInfoRepository;
        this.applicantAddressRepository = applicantAddressRepository;
        this.creditCardMasterRepository = creditCardMasterRepository;
        this.arnCcBridgeRepository = arnCcBridgeRepository;
        this.educationalLevelMasterRepository = educationalLevelMasterRepository;
        this.homeOwnershipMasterRepository = homeOwnershipMasterRepository;
        this.jobTypeMasterRepository = jobTypeMasterRepository;
        this.addressTypeMasterRepository = addressTypeMasterRepository;
        this.countryListMasterRepository = countryListMasterRepository;
        this.ngStatusMasterRepository = ngStatusMasterRepository;
        this.documentsMasterRepository = documentsMasterRepository;
        this.ccApplicationRepository = ccApplicationRepository;
        this.suppApplicantRepository = suppApplicantRepository;
        this.ccApplicationAdditionalInfoRepository = ccApplicationAdditionalInfoRepository;
        this.addressRepository = addressRepository;
        this.channelMasterRepository = channelMasterRepository;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public List<ApplicantDetailsEntity> findApplicantByIdTypeAndIdValueAndDialCodeMobileAndMobileAndEmail
            (String idType, String idValue, String dialCodeMobile, Long mobile, String email) {
        return applicantRepository.findByDocumentsMasterByIdDocTypeKeyAndIdValueAndDialCodeMobileAndMobileAndEmail(idType, idValue, dialCodeMobile, mobile, email);
    }

    @Override
    public List<ApplicantDetailsEntity> findApplicantByIdTypeAndIdValue(String idType, String idValue) {
        return applicantRepository.findByDocumentsMasterByIdDocTypeKeyAndIdValue(idType, idValue);
    }

    @Override
    public Application manageFindApplicantById(Long id) {
        return findAndTransformApplicant(applicantRepository.findOne(id));
    }

    @Override
    public List<Application> manageFindAllApplicants() {
        return applicantRepository.findAll().stream().map(a -> findAndTransformApplicant(a))
                .collect(Collectors.toList());
    }

    @Override
    public Applicant manageUpdateApplicant(String arn, Applicant applicant) {
        // Find Application By Arn
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        ApplicantDetailsEntity applicantDetailsEntity = ccApplicationEntity.getApplicantDetailsByApplicantId();
        ApplicantDetailsEntity applicantDetailsEntity1 = updateApplicantWithAddressAndARNCCBridgeEntry(applicant, applicantDetailsEntity, ccApplicationEntity);
        CCApplicationEntity ccApplicationEntity1 = applicantDetailsEntity1.getCcapplicationsById().get(0);
        Application application = Transformation.transformApplicationEntityToApplication(ccApplicationEntity1);

        List<AddressEntity> addressEntities = ccApplicationEntity1.getApplicantDetailsByApplicantId().getAddressesById();

        if (addressEntities != null && !addressEntities.isEmpty()) {
            List<Address> addresses = convertToAddressList(addressEntities);

            if (application.getPrimaryApplicant().getAdditionalInfo() == null) {
                ApplicantAdditionalInfo applicantAdditionalInfo = new ApplicantAdditionalInfo();
                application.getPrimaryApplicant().setAdditionalInfo(applicantAdditionalInfo);
            }
            application.getPrimaryApplicant().getAdditionalInfo().setAddress(addresses);
        }
        return application.getPrimaryApplicant();
    }


    private List<Address> convertToAddressList(List<AddressEntity> addressEntities) {
        return addressEntities.stream()
                .sorted(Comparator.comparing(x -> x.getId()))
                .map(a -> Transformation.transferAddressEntityTOAddress(a)).collect(Collectors.toList());
    }

    @Override
    public ApplicantDetailsEntity updateApplicantWithAddressAndARNCCBridgeEntry(Applicant applicant,
                                                                                ApplicantDetailsEntity applicantDetailsEntity, CCApplicationEntity ccApplicationEntity) {
        return transactionTemplate.execute(status -> {
            // Populate Applicant
            ApplicantDetailsEntity applicantDetailsEntityModified = applicantDetailsEntity;
            applicantDetailsEntityModified = populateApplicantEntity(applicantDetailsEntityModified, applicant);

            List<AddressEntity> addressEntities = new ArrayList<>();
            List<AddressEntity> addressesById1 = applicantDetailsEntityModified.getAddressesById();

            if (addressesById1 != null && !addressesById1.isEmpty()) {
                addressRepository.delete(addressesById1);
            }
            // Populate ApplicantAdditionalInfoEntity
            addressEntities = populateAddressEntities(applicant);
            applicantDetailsEntityModified.setAddressesById(addressEntities);

            ApplicantAdditionalInfoEntity applicantAdditionalInfoEntity = applicantDetailsEntityModified
                    .getAppAddInfo();
            if (applicantAdditionalInfoEntity != null) {
                applicantDetailsEntityModified
                        .setAppAddInfo(populateApplicantAdditionalInfoEntity(applicantAdditionalInfoEntity, applicant));
            } else {
                applicantAdditionalInfoEntity = new ApplicantAdditionalInfoEntity();
                applicantDetailsEntityModified
                        .setAppAddInfo(populateApplicantAdditionalInfoEntity(applicantAdditionalInfoEntity, applicant));
            }
            applicantAdditionalInfoEntity.setApplicantByAplicantId(applicantDetailsEntityModified);
            /*
             * List<AddressEntity> addressEntities = new ArrayList<>();
			 *
			 * //TODO: Populate Address List<Address> applicantAddresses = null;
			 * if (applicant.getAdditionalInfo() != null) { applicantAddresses =
			 * applicant.getAdditionalInfo().getAddress();
			 *
			 *
			 * ApplicantAdditionalInfo additionalInfo =
			 * applicant.getAdditionalInfo(); if (applicantAddresses != null &&
			 * !applicantAddresses.isEmpty()) {
			 *
			 * List<AddressEntity> addressesById =
			 * applicantDetailsEntity.getAddressesById();
			 *
			 * if (addressesById == null) addressesById = new ArrayList<>();
			 *
			 * if (addressesById.isEmpty()) {
			 * applicantDetailsEntity.setAddressesById(addressEntities); for
			 * (int i = 0; i < additionalInfo.getAddress().size(); i++) {
			 * AddressEntity addressEntity = new AddressEntity();
			 * addressesById.add(addressEntity); } }
			 *
			 *
			 * for (int i = 0; i < additionalInfo.getAddress().size(); i++) {
			 * Address curAddress = additionalInfo.getAddress().get(i);
			 *
			 * AddressEntity addressEntity = addressesById.get(i); if
			 * (addressEntity == null) { addressEntity = new AddressEntity(); }
			 * AddressEntity addressEntity1 =
			 * Transformation.populateAddressEntity(addressEntity, curAddress);
			 * addressEntities.add(addressEntity1); }
			 *
			 * }
			 *
			 * if (applicantDetailsEntity.getAddressesById() != null &&
			 * !applicantDetailsEntity.getAddressesById().isEmpty()) {
			 * List<AddressEntity> addressEntitiesList = new ArrayList<>(); }
			 *
			 * if (applicant.getAdditionalInfo().getAddress() != null) {
			 * addressEntities = populateCountryForAddress(applicant,
			 * addressEntities);
			 * applicantDetailsEntity.setAddressesById(addressEntities); } }
			 */

            // update applicant
            applicantDetailsEntityModified = applicantRepository.saveAndFlush(applicantDetailsEntityModified);

            // Populate applicantAddressEntities (Composite Keys: ApplicantId,
            // ApplicationId, AddressId)
            // TODO: populate this
            /*
             * if (applicant.getAdditionalInfo().getAddress() != null) {
			 * List<ApplicantAddressEntity> applicantAddressEntities =
			 * populateApplicantAddress(applicant); for (int i = 0; i <
			 * applicantAddressEntities.size(); i++) { ApplicantAddressEntity a
			 * = applicantAddressEntities.get(i);
			 * a.getPrimaryKey().setApplicationId(ccApplicationEntity);
			 * a.getPrimaryKey().setApplicantId(applicantDetailsEntity);
			 * a.getPrimaryKey().setAddressId(addressEntities.get(i)); }
			 *
			 *
			 * //save applicantAddressEntities
			 * applicantAddressRepository.save(applicantAddressEntities);
			 */

            List<ApplicantAddressEntity> applicantAddressEntityList = new ArrayList<>();
            if (applicant.getAdditionalInfo() != null && applicant.getAdditionalInfo().getAddress() != null) {
                if (!applicant.getAdditionalInfo().getAddress().isEmpty()) {
                    for (int i = 0; i < applicant.getAdditionalInfo().getAddress().size(); i++) {

                        ApplicantAddressEntity applicantAddressEntity = new ApplicantAddressEntity();
                        applicantAddressEntity = Transformation.populateApplicantAddressEntity(applicantAddressEntity,
                                applicant.getAdditionalInfo().getAddress().get(i));
                        if (applicant.getAdditionalInfo().getAddress().get(i).getAddressType() != null) {
                            applicantAddressEntity.setType(getAddressTypeMaster(
                                    applicant.getAdditionalInfo().getAddress().get(i).getAddressType().getValue()));
                        }

                        applicantAddressEntity.getPrimaryKey().setApplicationId(ccApplicationEntity);
                        applicantAddressEntity.getPrimaryKey().setApplicantId(applicantDetailsEntity);
                        applicantAddressEntity.getPrimaryKey().setAddressId(addressEntities.get(i));
                        ApplicantAddressEntity addressEntity = applicantAddressRepository.saveAndFlush(applicantAddressEntity);
                        applicantAddressEntityList.add(addressEntity);
                    }
                    // applicantAddressRepository.save(applicantAddressEntityList);
                }
            }

            for (ApplicantAddressEntity applicantAddressEntity : applicantAddressEntityList) {

                AddressEntity addressId = applicantAddressEntity.getPrimaryKey().getAddressId();
                AddressEntity addressFound = applicantDetailsEntity.getAddressesById().stream()
                        .filter(x -> x.getId().equals(addressId.getId()))
                        .findFirst().get();
                List<ApplicantAddressEntity> applicantAddressEntities = new ArrayList<>();
                applicantAddressEntities.add(applicantAddressEntity);
                addressFound.setApplicantAddressesById(applicantAddressEntities);
            }

            List<ARNCcBridgeEntity> arnCcBridgeEntities = populateArnCCBridgeEntity(applicant, applicantDetailsEntity);
            // Save ARNCcBridgeEntity

            arnCcBridgeEntities.forEach(a -> arnCcBridgeRepository.saveAndFlush(a));
            // arnCcBridgeRepository.save(arnCcBridgeEntities);

            return applicantDetailsEntity;
        });
    }

    @Override
    public void manageDeleteApplicantById(Long id) {
        applicantAdditionalInfoRepository.delete(id);
        applicantRepository.delete(id);
    }

    private boolean isSupplementaryWithSameIdValue(List<Applicant> applicants) {
        for (int i = 0; i < applicants.size() - 1; i++) {
            if (applicants.get(i).getIdValue() != null && applicants.get(i + 1).getIdValue() != null) {
                if (applicants.get(i).getIdDocType().equals(applicants.get(i + 1).getIdDocType())
                        && applicants.get(i).getIdValue().equalsIgnoreCase(applicants.get(i + 1).getIdValue())) {
                    return true;
                }
            }
        }
        return false;
    }


    private boolean isPrimaryAndSupplimentaryWithSameIdValue(ApplicantDetailsEntity applicantDetailsEntity, List<Applicant> applicants) {
        for (Applicant tempApplicant : applicants) {
            if (tempApplicant.getIdValue() != null
                    && applicantDetailsEntity.getDocumentsMasterByIdDocType().getKey().equalsIgnoreCase(tempApplicant.getIdDocType().getValue())
                    && applicantDetailsEntity.getIdValue().equalsIgnoreCase(tempApplicant.getIdValue())) {
                return true;
            }
        }
        return false;
    }

    private ApplicantDetailsEntity getPopulatedApplicant(ApplicantDetailsEntity applicantEntity, Applicant applicant) {
        return (applicantEntity == null) ? populateApplicantEntity(new ApplicantDetailsEntity(), applicant) : populateApplicantEntity(applicantEntity, applicant);
    }

    private ApplicantDetailsEntity getApplicantsFromDbToUpdate(List<SuppapplicantEntity> suppapplicantEntities, Applicant applicant) {
        ApplicantDetailsEntity applicantDetailsEntity = null;
        if (CollectionUtils.isNotEmpty(suppapplicantEntities)) {
            for (SuppapplicantEntity suppapplicantEntity : suppapplicantEntities) {
                if (suppapplicantEntity.getPrimaryKey().getApplicantId().getId().equals(applicant.getId())) {
                    return suppapplicantEntity.getPrimaryKey().getApplicantId();
                }
            }
        }
        return applicantDetailsEntity;
    }

    private void updateAndPopulateApplicantAdditionalInfo(Applicant applicant, ApplicantDetailsEntity applicantEntity) {
        if (applicant.getAdditionalInfo() != null) {
            ApplicantAdditionalInfoEntity applicantAdditionalInfoEntity = applicantEntity.getAppAddInfo();
            applicantAdditionalInfoEntity = (applicantEntity != null && applicantEntity.getAppAddInfo() != null)
                    ? populateApplicantAdditionalInfoEntity(applicantAdditionalInfoEntity, applicant)
                    : populateApplicantAdditionalInfoEntity(new ApplicantAdditionalInfoEntity(), applicant);
            applicantEntity.setAppAddInfo(applicantAdditionalInfoEntity);
            applicantAdditionalInfoEntity.setApplicantByAplicantId(applicantEntity);
        }
    }

    private ApplicantAddressEntity updateIsResidentialAddressSameAsPrimary(
            Applicant applicant, ApplicantDetailsEntity tempApplicantEntity, List<AddressEntity> addressEntities) {

        AddressEntity addressEntityResidential = null;
        ApplicantAddressEntity applicantAddressEntityResidential = null;
        if (applicant.getAdditionalInfo() != null
                && applicant.getAdditionalInfo().isResidentialAddressSameAsPrimary()
                && CollectionUtils.isNotEmpty(tempApplicantEntity.getAddressesById())) {

            List<ApplicantAddressEntity> applicantAddressEntityListPrimary = tempApplicantEntity
                    .getApplicantAddressesById().stream()
                    .filter(a -> AddressType.RESIDENTIAL.getValue().equalsIgnoreCase(a.getType().getType()))
                    .collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(applicantAddressEntityListPrimary)) {

                ApplicantAddressEntity applicantAddressEntityPrimary = applicantAddressEntityListPrimary.get(0);
                AddressEntity addressEntityPrimary = applicantAddressEntityPrimary.getPrimaryKey().getAddressId();
                if (CollectionUtils.isNotEmpty(addressEntities)) {
                    List<AddressEntity> suppAddress = addressEntities.stream()
                            .filter(a -> AddressType.RESIDENTIAL.getValue().equalsIgnoreCase(a.getApplicantAddressesById().get(0).getType().getType()))
                            .collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(suppAddress)) {
                        addressEntityResidential = generateNewAddressEntityByAddressEntity(addressEntityPrimary, suppAddress.get(0));
                        applicantAddressEntityResidential = suppAddress.get(0).getApplicantAddressesById().get(0);
                    }
                } else {
                    addressEntityResidential = generateNewAddressEntityByAddressEntity(addressEntityPrimary, new AddressEntity());
                    applicantAddressEntityResidential = Transformation.populateApplicantAddressEntity(new ApplicantAddressEntity(), new Address());
                    applicantAddressEntityResidential.setType(applicantAddressEntityPrimary.getType());
                    addressEntities.add(addressEntityResidential);
                }
            }
        }
        return applicantAddressEntityResidential;
    }

    private void saveAllApplicantAddresses(Applicant applicant, CCApplicationEntity ccApplicationEntity,
                                           ApplicantDetailsEntity applicantEntity, List<AddressEntity> addressEntities) {

        if (applicant.getAdditionalInfo() != null && CollectionUtils.isNotEmpty(applicant.getAdditionalInfo().getAddress())) {
            for (int i = 0; i < applicant.getAdditionalInfo().getAddress().size(); i++) {

                List<ApplicantAddressEntity> applicantAddressEntityList = new ArrayList<>();
                ApplicantAddressEntity applicantAddressEntity = Transformation.populateApplicantAddressEntity(
                        new ApplicantAddressEntity(), applicant.getAdditionalInfo().getAddress().get(i));
                AddressType addressType = applicant.getAdditionalInfo().getAddress().get(i).getAddressType();
                applicantAddressEntity.setType((addressType != null) ? getAddressTypeMaster(addressType.getValue()) : null);

                applicantAddressEntity.getPrimaryKey().setApplicationId(ccApplicationEntity);
                applicantAddressEntity.getPrimaryKey().setApplicantId(applicantEntity);
                applicantAddressEntity.getPrimaryKey().setAddressId(addressEntities.get(i));

                applicantAddressEntityList.add(applicantAddressEntity);
                applicantAddressRepository.saveAndFlush(applicantAddressEntity);
                //addressEntities.get(i).setApplicantAddressesById(applicantAddressEntityList);
            }
        }
    }

    private List<AddressEntity> getAddressEntityList(ApplicantDetailsEntity applicantEntity) {
        return (applicantEntity.getAddressesById() == null) ? new ArrayList<>() : applicantEntity.getAddressesById();
    }

    @Override
    public List<Applicant> manageSaveApplicant(List<Applicant> applicants, String arn) {

        // get Application by arn.
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);

        // get Primary Applicant in DB from Application.
        ApplicantDetailsEntity tempApplicantEntity = ccApplicationEntity.getApplicantDetailsByApplicantId();

        // Check whether Primary Applicant and Supplementary Applicants have same IdValue or not.
        if (isPrimaryAndSupplimentaryWithSameIdValue(tempApplicantEntity, applicants)) {
            throw new SameSupplementaryAndPrimaryIdValueException("Primary IdValue cannot be same as Supplementary IdValue.");
        }

        // Check whether Supplementary Applicants have same IdValue or not.
        if (isSupplementaryWithSameIdValue(applicants)) {
            throw new SameSupplementaryAndPrimaryIdValueException("Two Supplementary Applicants cannot have same IdValue.");
        }

        // get Supplementary Applicants from DB by Application.
        List<SuppapplicantEntity> suppapplicantEntities = ccApplicationEntity.getSuppApplicantsById();


        if (CollectionUtils.isNotEmpty(applicants)) {
            applicants.stream().forEach(applicant -> {

                // check whether the Supplementary Applicants from UI and Db are same or not.
                ApplicantDetailsEntity applicantEntity = getApplicantsFromDbToUpdate(suppapplicantEntities, applicant);

                // update Supplementary Applicant in ApplicantDetails Table.
                applicantEntity = getPopulatedApplicant(applicantEntity, applicant);

                // update Additional Info of Supplementary Applicant.
                updateAndPopulateApplicantAdditionalInfo(applicant, applicantEntity);

                // populate Address Entities.
                List<AddressEntity> addressEntities = getAddressEntityList(applicantEntity);

                if (applicant.getAdditionalInfo() != null && applicant.getAdditionalInfo().getAddress() != null) {
                    addressEntities = populateAndUpdateAddresses(addressEntities, applicant);
                }

                // update IsResidentialAddressSameAsPrimary for Supplementary Applicant.
                ApplicantAddressEntity applicantAddressEntityResidential = updateIsResidentialAddressSameAsPrimary(applicant, tempApplicantEntity, addressEntities);

                applicantEntity.setAddressesById(addressEntities);

                // Save Supplementary Applicant in ApplicantDetailsEntity.
                applicantEntity = applicantRepository.saveAndFlush(applicantEntity);
                applicant.setId(applicantEntity.getId());

                // Save ApplicantAddress Type for Supplementary Address.
                saveAllApplicantAddresses(applicant, ccApplicationEntity, applicantEntity, addressEntities);

                // Save ApplicantAddressEntity in case of Residential Address Same as Primary.
                if (applicant.getAdditionalInfo() != null
                        && applicant.getAdditionalInfo().isResidentialAddressSameAsPrimary()
                        && CollectionUtils.isNotEmpty(tempApplicantEntity.getAddressesById())) {

                    List<ApplicantAddressEntity> applicantAddressEntityListPrimary = tempApplicantEntity.getApplicantAddressesById().stream()
                            .filter(a -> AddressType.RESIDENTIAL.getValue().equalsIgnoreCase(a.getType().getType()))
                            .collect(Collectors.toList());

                    if (CollectionUtils.isNotEmpty(applicantAddressEntityListPrimary)) {

                        applicantAddressEntityResidential.getPrimaryKey().setAddressId(addressEntities.get(0));
                        applicantAddressEntityResidential.getPrimaryKey().setApplicantId(applicantEntity);
                        applicantAddressEntityResidential.getPrimaryKey().setApplicationId(ccApplicationEntity);
                        applicantAddressRepository.saveAndFlush(applicantAddressEntityResidential);

                    }
                }

                List<ARNCcBridgeEntity> arnCcBridgeEntities = populateArnCCBridgeEntity(applicant, applicantEntity);
                arnCcBridgeEntities.forEach(arnCcBridgeEntity -> arnCcBridgeRepository.saveAndFlush(arnCcBridgeEntity));

                SuppapplicantPK suppapplicantPK = new SuppapplicantPK();
                suppapplicantPK.setApplicantId(applicantEntity);
                suppapplicantPK.setApplicationId(ccApplicationEntity);
                SuppapplicantEntity suppapplicantEntity = Transformation
                        .populateSuppapplicantEntity(new SuppapplicantEntity(), suppapplicantPK, applicant);
                suppApplicantRepository.saveAndFlush(suppapplicantEntity);
            });
        }

        int suppApplicantToBeDeleted = suppapplicantEntities.size() - applicants.size();
        int i = applicants.size() + 1;
        if (suppApplicantToBeDeleted > 0) {
            for (; i <= suppapplicantEntities.size(); i++) {
                deleteSuppApplicant(arn, i);
            }
        }

        return applicants;
    }

    private List<AddressEntity> populateAndUpdateAddresses(List<AddressEntity> addressEntities, Applicant applicant) {
        List<Address> addresses = applicant.getAdditionalInfo().getAddress();
        if (addressEntities.isEmpty()) {
            addressEntities.add(new AddressEntity());
        }
        for (int i = 0; i < addresses.size(); i++) {
            AddressEntity addressEntity = addressEntities.get(i);
            addressEntity = (addressEntities.get(i) == null)
                    ? Transformation.populateAddressEntity(new AddressEntity(), addresses.get(i))
                    : Transformation.populateAddressEntity(addressEntity, addresses.get(i));
        }

        populateCountryForAddress(applicant, addressEntities);

        if (addressEntities.size() - addresses.size() > 0) {
            for (int i = addresses.size() - 1; i < addressEntities.size(); i++) {
                applicantAddressRepository.delete(addressEntities.get(i).getApplicantAddressesById());
                addressRepository.delete(addressEntities.get(i));
            }
        }
        return addressEntities;
    }

    private AddressEntity generateNewAddressEntityByAddressEntity(AddressEntity addressEntityPrimary, AddressEntity addressEntitySupp) {
        addressEntitySupp.setDoorNumber(addressEntityPrimary.getDoorNumber());
        addressEntitySupp.setL2Street(addressEntityPrimary.getL2Street());
        addressEntitySupp.setL3Unit(addressEntityPrimary.getL3Unit());
        addressEntitySupp.setCity(addressEntityPrimary.getCity());
        addressEntitySupp.setCountryListMasterByCountry(addressEntityPrimary.getCountryListMasterByCountry());
        addressEntitySupp.setPostalCode(addressEntityPrimary.getPostalCode());
        addressEntitySupp.setCreatedOn(addressEntityPrimary.getCreatedOn());
        addressEntitySupp.setCreatedBy(addressEntityPrimary.getCreatedBy());
        addressEntitySupp.setLastModifiedBy(addressEntityPrimary.getLastModifiedBy());
        addressEntitySupp.setLastModifiedOn(addressEntityPrimary.getLastModifiedOn());
        return addressEntitySupp;
    }

    @Override
    public Applicant savePrimaryApplicant(String arn, Applicant applicant) {

        Applicant modifiedApplicant = manageUpdateApplicant(arn, applicant);
        /*
         * ApplicantDetailsEntity applicantEntity = populateApplicantEntity(new
		 * ApplicantDetailsEntity(), applicant); ApplicantAdditionalInfoEntity
		 * applicantAdditionalInfoEntity =
		 * populateApplicantAdditionalInfoEntity(new
		 * ApplicantAdditionalInfoEntity(), applicant); List<AddressEntity>
		 * addressEntities = populateAddressEntities(applicant);
		 * applicantEntity.setAppAddInfo(applicantAdditionalInfoEntity);
		 * applicantEntity.setAddressesById(addressEntities);
		 * List<ApplicantDocumentEntity> applicationDocumentEntities =
		 * populateDocuments(applicant);
		 * applicantEntity.setApplicantDocumentsById(applicationDocumentEntities
		 * ); applicantEntity = applicantRepository.save(applicantEntity);
		 */
        return modifiedApplicant;

    }

    @Override
    @Transactional
    public Application createApplicantUsingApplication(String uid, String channelAuthority, Application application) {
        Applicant applicant = application.getPrimaryApplicant();

        // Populate ApplicantEntity
        ApplicantDetailsEntity applicantEntity = populateApplicantEntity(new ApplicantDetailsEntity(), applicant);

        // Populate ApplicantAdditionalInfoEntity
        ApplicantAdditionalInfo additionalInfo = applicant.getAdditionalInfo();
        AddressEntity addressEntity = null;

        List<Address> applicantAddress = Lists.newArrayList();
        if (additionalInfo != null) {
            if (additionalInfo.getAddress() != null)
                applicantAddress.addAll(additionalInfo.getAddress());
            ApplicantAdditionalInfoEntity applicantAdditionalInfoEntity = populateApplicantAdditionalInfoEntity(new ApplicantAdditionalInfoEntity(), applicant);
            if (CollectionUtils.isNotEmpty(applicantAddress)) {
                addressEntity = Transformation.populateAddressEntity(new AddressEntity(), applicantAddress.get(0));
                CountryCode residentialAddrCountry = applicantAddress.get(0).getCountry();
                if (residentialAddrCountry != null) {
                    addressEntity.setCountryListMasterByCountry(countryListMasterRepository.findByCode(residentialAddrCountry.getValue()));
                }
                applicantEntity.setAddressesById(Arrays.asList(addressEntity));
            }
            applicantEntity.setAppAddInfo(applicantAdditionalInfoEntity);
            applicantAdditionalInfoEntity.setApplicantByAplicantId(applicantEntity);
        }

        // Populate CCApplicationEntity
        CCApplicationEntity ccApplicationEntity = Transformation.populateApplicationEntity(new CCApplicationEntity(),
                application);

        // Populate ApplicationStatusTimeline To In-Progress
        ccApplicationEntity.setCcapplicationsStatusById(populateTimeline(ccApplicationEntity, channelAuthority));

        // Populate StaffDetailsEntity for UID as staffId from session.
        StaffDetailsEntity staffDetailsEntity = populateStaffId(uid);
        staffDetailsEntity.setCcapplicationStaffById(ccApplicationEntity);
        ccApplicationEntity.setStaffByApplicationId(staffDetailsEntity);

        // Populate StaffDetailsEntity for UID as staffId from session.
        ccApplicationEntity.setChannelMasterById(getChannelMasterEntityByName(channelAuthority));

        // Populate applicationDocumentEntities
        List<ApplicantDocumentEntity> applicationDocumentEntities = populateDocuments(applicant);
        applicantEntity.setApplicantDocumentsById(applicationDocumentEntities);

        // Bidirectional mapping of ApplicantDetailsEntity and
        // CCApplicationEntity
        List<CCApplicationEntity> ccApplicationEntities = new ArrayList<>();
        ccApplicationEntities.add(ccApplicationEntity);
        applicantEntity.setCcapplicationsById(ccApplicationEntities);
        ccApplicationEntities.get(0).setApplicantDetailsByApplicantId(applicantEntity);

        // Save ApplicantDetails
        applicantEntity = applicantRepository.save(applicantEntity);


        // Applicant Address
        if (CollectionUtils.isNotEmpty(applicantAddress)) {
            Address applicantAddressReceived = applicantAddress.get(0);
            ApplicantAddressEntity applicantAddressEntity = Transformation.populateApplicantAddressEntity(new ApplicantAddressEntity(), applicantAddressReceived);

            applicantAddressEntity.setType(getAddressTypeMaster(applicantAddress.get(0).getAddressType().getValue()));
            applicantAddressEntity.getPrimaryKey().setAddressId(addressEntity);
            applicantAddressEntity.getPrimaryKey().setApplicantId(applicantEntity);
            applicantAddressEntity.getPrimaryKey().setApplicationId(ccApplicationEntity);
            applicantAddressRepository.save(applicantAddressEntity);
        }

        // save CCApplicationAdditionalInfoEntity
        CCApplicationAdditionalInfoEntity ccApplicationAdditionalInfoEntity = Transformation
                .populateApplicationAdditionalInfoEntity(new CCApplicationAdditionalInfoEntity(), application);
        ccApplicationAdditionalInfoEntity.setApplicationByAplicationId(ccApplicationEntity);
        ccApplicationAdditionalInfoEntity = ccApplicationAdditionalInfoRepository
                .save(ccApplicationAdditionalInfoEntity);

        List<ARNCcBridgeEntity> arnCcBridgeEntities = populateArnCCBridgeEntity(applicant, applicantEntity);

        // Save ARNCcBridgeEntity
        arnCcBridgeRepository.save(arnCcBridgeEntities);

        // return application
        return application;
    }

    private StaffDetailsEntity populateStaffId(String uid) {
        StaffDetailsEntity staffDetailsEntity = new StaffDetailsEntity();
        staffDetailsEntity.setStaffId(uid);
        return staffDetailsEntity;
    }

    private ChannelMasterEntity getChannelMasterEntityByName(String channelAuthority) {
        return channelMasterRepository.findByName(channelAuthority);
    }

    private AddressTypeMasterEntity getAddressTypeMaster(String type) {
        return addressTypeMasterRepository.findByType(type);
    }

    private List<ARNCcBridgeEntity> populateArnCCBridgeEntity(Applicant applicant,
                                                              ApplicantDetailsEntity applicantEntity) {
        // Populate ARNCcBridgeEntity
        List<ARNCcBridgeEntity> arnCcBridgeEntities = applicant.getCards().stream()
                .map(a -> Transformation.populateARNCcBridgeEntity(new ARNCcBridgeEntity(), a))
                .collect(Collectors.toList());

        // Populate List<CreditCardMasterEntity>
        List<CreditCardMasterEntity> creditCardMasterEntities = applicant.getCards().stream()
                .map(a -> findCreditCardByKey(a)).collect(Collectors.toList());

        // Populate NameOnCard
        if (applicant.getAdditionalInfo() != null) {
            arnCcBridgeEntities.forEach(a -> a.setNameOnCard(applicant.getAdditionalInfo().getNameOnCard()));
        }

        // Populate ArnCcBridgeEntities (Composite keys : ApplicantId,
        // CreediCardId)
        for (int i = 0; i < arnCcBridgeEntities.size(); i++) {
            ARNCcBridgeEntity a = arnCcBridgeEntities.get(i);
            a.getPrimaryKey().setApplicantByApplicantId(applicantEntity);
            a.getPrimaryKey().setCreditCardMasterByCcId(creditCardMasterEntities.get(i));
        }

        if (applicant.getApplicantDeclarations() != null)
            if (applicant.getApplicantDeclarations().getBalanceTransferOnCreditCard() != null
                    && applicant.getApplicantDeclarations().getBalanceTransferOnCreditCard() == true)
                arnCcBridgeEntities = Transformation.populateBalanceTransfer(arnCcBridgeEntities, applicant);

        return arnCcBridgeEntities;
    }

    private ApplicantDetailsEntity populateApplicantEntity(ApplicantDetailsEntity applicantDetailsEntity,
                                                           Applicant applicant) {

        applicantDetailsEntity = Transformation.populateApplicantEntity(applicantDetailsEntity, applicant);
        ApplicantAdditionalInfo additionalInfo = applicant.getAdditionalInfo();

        ApplicantDetailsEntity finalApplicantDetailsEntity = applicantDetailsEntity;
        CommonUtils.resolveIfNull(() -> additionalInfo.getCountryOfBirth().getValue())
                .ifPresent(x -> finalApplicantDetailsEntity
                        .setCountryListMasterByCountryOfBirth(countryListMasterRepository.findByCode(x)));
        CommonUtils.resolveIfNull(() -> additionalInfo.getCountryOfIdIssue().getValue())
                .ifPresent(x -> finalApplicantDetailsEntity
                        .setCountryListMasterByCountryOfIdIssue(countryListMasterRepository.findByCode(x)));
        CommonUtils.resolveIfNull(() -> applicant.getIdDocType()).ifPresent(x -> finalApplicantDetailsEntity
                .setDocumentsMasterByIdDocType(documentsMasterRepository.findByKey(x.getValue())));
        return finalApplicantDetailsEntity;
    }

    private ApplicantAdditionalInfoEntity populateApplicantAdditionalInfoEntity(
            ApplicantAdditionalInfoEntity applicantAdditionalInfoEntity, Applicant applicant) {
        if (applicantAdditionalInfoEntity != null) {
            applicantAdditionalInfoEntity = Transformation
                    .populateApplicantAdditionalInfoEntity(applicantAdditionalInfoEntity, applicant);
            ApplicantAdditionalInfoEntity finalApplicantAdditionalInfoEntity = applicantAdditionalInfoEntity;
            CommonUtils.resolveIfNull(() -> applicant.getAdditionalInfo().getEducationLevel())
                    .ifPresent(x -> finalApplicantAdditionalInfoEntity.setEducationallevelmasterByEducationlevel(
                            educationalLevelMasterRepository.findByLabel(x.getValue())));

            CommonUtils.resolveIfNull(() -> applicant.getAdditionalInfo().getHomeOwnership())
                    .ifPresent(x -> finalApplicantAdditionalInfoEntity.setHomeownershipmasterByHomeownership(
                            homeOwnershipMasterRepository.findByLabel(x.getValue())));

            CommonUtils.resolveIfNull(() -> applicant.getAdditionalInfo().getPassportCountry().getValue())
                    .ifPresent(x -> finalApplicantAdditionalInfoEntity
                            .setPassportCountry(countryListMasterRepository.findByCode(x)));

            CommonUtils.resolveIfNull(() -> applicant.getAdditionalInfo().getEmploymentType())
                    .ifPresent(x -> finalApplicantAdditionalInfoEntity
                            .setJobtypemasterByEmploymenttype(jobTypeMasterRepository.findByName(x.getValue())));
            applicantAdditionalInfoEntity = finalApplicantAdditionalInfoEntity;
        }
        return applicantAdditionalInfoEntity;
    }

    private List<AddressEntity> populateAddressEntities(Applicant applicant) {
        List<AddressEntity> addressEntities = null;
        if (applicant.getAdditionalInfo() != null && applicant.getAdditionalInfo().getAddress() != null) {

            if (applicant.getAdditionalInfo().isResidentialAddIdenticalToPermAddress() && applicant.getType().equals(ApplicantType.PRIMARY)) {
                applicant.getAdditionalInfo().getAddress().removeIf(address -> AddressType.PERMANENT.equals(address.getAddressType()));
                List<Address> addresses = applicant.getAdditionalInfo().getAddress().stream()
                        .filter(a -> AddressType.RESIDENTIAL.equals(a.getAddressType()))
                        .collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(addresses)) {
                    Address address = new Address();
                    address = addresses.get(0);
                    address = isResidentialAddressSameAsPermanent(address);
                    applicant.getAdditionalInfo().getAddress().add(address);
                }
            }


            addressEntities = applicant.getAdditionalInfo().getAddress().stream()
                    .map(a -> Transformation.populateAddressEntity(new AddressEntity(), a))
                    .collect(Collectors.toList());
            populateCountryForAddress(applicant, addressEntities);
        }
        return addressEntities;
    }


    private Address isResidentialAddressSameAsPermanent(Address address) {

        Address permanedntAddress = new Address();
        permanedntAddress.setAddressType(AddressType.PERMANENT);

        permanedntAddress.setLine1Address(address.getLine1Address());
        permanedntAddress.setLine2Address(address.getLine2Address());
        permanedntAddress.setLine3Address(address.getLine3Address());
        permanedntAddress.setCity(address.getCity());
        permanedntAddress.setCountry(address.getCountry());
        permanedntAddress.setPostalCode(address.getPostalCode());
        return permanedntAddress;
    }

    private List<AddressEntity> populateCountryForAddress(Applicant applicant, List<AddressEntity> addressEntities) {
        for (int i = 0; i < addressEntities.size(); i++) {
            if (applicant.getAdditionalInfo().getAddress() != null) {
                CountryCode country = applicant.getAdditionalInfo().getAddress().get(i).getCountry();
                if (country != null)
                    addressEntities.get(i)
                            .setCountryListMasterByCountry(countryListMasterRepository.findByCode(country.getValue()));
            }
        }
        return addressEntities;
    }

    private List<ApplicationStatusTimelineEntity> populateTimeline(CCApplicationEntity ccApplicationEntity, String channel) {
        List<ApplicationStatusTimelineEntity> applicationStatusTimelineEntities = new ArrayList<>();
        ApplicationStatusTimelineEntity applicationStatusTimelineEntity = Transformation.populateApplicationStatusTimelineEntity();
        if (ChannelType.CU.getChannel().equalsIgnoreCase(channel)) {
            applicationStatusTimelineEntity.setNgStatusMasterByNgStatus(ngStatusMasterRepository.findByKey(NgStatus.PENDINGWITHCUSTOMER.getValue()));
        } else {
            applicationStatusTimelineEntity.setNgStatusMasterByNgStatus(ngStatusMasterRepository.findByKey(NgStatus.INPROGRESS.getValue()));
        }
        applicationStatusTimelineEntity.setCcApplicationEntity(ccApplicationEntity);
        applicationStatusTimelineEntities.add(applicationStatusTimelineEntity);
        return applicationStatusTimelineEntities;
    }

    private List<ApplicantAddressEntity> populateApplicantAddress(Applicant applicant) {
        List<ApplicantAddressEntity> applicantAddressEntities = applicant.getAdditionalInfo().getAddress().stream()
                .map(a -> Transformation.populateApplicantAddressEntity(new ApplicantAddressEntity(), a))
                .collect(Collectors.toList());
        for (int i = 0; i < applicantAddressEntities.size(); i++) {
            if (applicant.getAdditionalInfo().getAddress() != null) {
                applicantAddressEntities.get(i).setType(addressTypeMasterRepository
                        .findOne(applicant.getAdditionalInfo().getAddress().get(i).getAddressType().getId()));
            }
        }
        return applicantAddressEntities;
    }

    private List<ApplicantDocumentEntity> populateDocuments(Applicant applicant) {
        List<ApplicantDocumentEntity> applicationDocumentEntities = Transformation
                .populateApplicationDocumentEntity(applicant.getDocuments());
        for (int i = 0; i < applicationDocumentEntities.size(); i++) {
            if (applicant.getDocuments().get(i).getDocName() != null) {
                applicationDocumentEntities.get(i).setDocumentsMasterByDocumentType(
                        documentsMasterRepository.findByKey(applicant.getDocuments().get(i).getDocName().getValue()));
            }
        }
        return applicationDocumentEntities;
    }

    private Application findAndTransformApplicant(ApplicantDetailsEntity applicantDetailsEntity) {
        Application application = Transformation
                .transformApplicationEntityToApplication(applicantDetailsEntity.getCcapplicationsById().get(0));
        List<Address> addresses = applicantDetailsEntity.getAddressesById().stream()
                .map(a -> Transformation.transferAddressEntityTOAddress(a)).collect(Collectors.toList());

        // application.getPrimaryApplicant().setAdditionalInfo(Transformation.transformApplicantAdditionalInfoEntityToApplicant(applicantDetailsEntity));
        application.getPrimaryApplicant().getAdditionalInfo().setAddress(addresses);
        // application.setPrimaryApplicant(Transformation.transformApplicantEntityToApplicant(applicantDetailsEntity));
        return application;
    }

    private CreditCardMasterEntity findCreditCardByKey(CreditCard creditCard) {
        return creditCardMasterRepository.findByKey(creditCard.getKey());
    }

    /**
     * This Function inserts the document for a primary applicant. TODO make
     * this function generic to both primary & supplimentary.
     */
    @Override
    public List<ApplicantDocumentEntity> createPrimaryApplDoc(String arn, String documentGroup,
                                                              List<Document> documents) {
        List<ApplicantDocumentEntity> applicationDocumentEntities = Transformation
                .populateApplicationDocumentEntity(documents);
        for (int i = 0; i < applicationDocumentEntities.size(); i++) {
            applicationDocumentEntities.get(i).setDocumentsMasterByDocumentType(
                    documentsMasterRepository.findByKey(documents.get(i).getDocName().getValue()));
        }
        return applicationDocumentEntities;
    }

    @Override
    public List<ApplicantDocumentEntity> createSupplementaryApplDoc(String arn, long supplementaryId,
                                                                    String documentGroup, List<Document> documents) {
        List<ApplicantDocumentEntity> applicationDocumentEntities = Transformation
                .populateApplicationDocumentEntity(documents);
        for (int i = 0; i < applicationDocumentEntities.size(); i++) {
            applicationDocumentEntities.get(i).setDocumentsMasterByDocumentType(
                    documentsMasterRepository.findByKey(documents.get(i).getDocName().getValue()));
        }
        return applicationDocumentEntities;
    }

    @Override
    public Applicant manageUpdateBeneficiary(String arn, Applicant applicant) {
        // Find Application By Arn
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);

        // get the Primary Applicant
        ApplicantDetailsEntity applicantDetailsEntity = ccApplicationEntity.getApplicantDetailsByApplicantId();

        if (applicant.getApplicantDeclarations() != null) {
            applicantDetailsEntity.setBalanceTransferOnCreditCard(
                    CommonUtils.convertToString(applicant.getApplicantDeclarations().getBalanceTransferOnCreditCard()));
            // Populate

            if (applicant.getApplicantDeclarations().getBalanceTransferOnCreditCard())
                applicantDetailsEntity = populateBeneficiaryDetails(applicantDetailsEntity, applicant);
        }

        // Save
        applicantRepository.save(applicantDetailsEntity);

        return null;
    }

    private ApplicantDetailsEntity populateBeneficiaryDetails(ApplicantDetailsEntity applicantDetailsEntity,
                                                              Applicant applicant) {

        List<ARNCcBridgeEntity> arnCcBridgeEntities = Transformation
                .populateBalanceTransfer(applicantDetailsEntity.getArnCcBridgesById(), applicant);

        arnCcBridgeRepository.save(arnCcBridgeEntities);

        applicantDetailsEntity.setArnCcBridgesById(arnCcBridgeEntities);

        return applicantDetailsEntity;
    }

    @Override
    public void deleteSuppApplicant(String arn, int index) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);

        if (ccApplicationEntity.getSuppApplicantsById() != null
                && !ccApplicationEntity.getSuppApplicantsById().isEmpty()) {

            List<SuppapplicantEntity> suppapplicantEntities = ccApplicationEntity.getSuppApplicantsById();
            Collections.sort(suppapplicantEntities, (s1, s2) -> s1.getPrimaryKey().getApplicantId().getId()
                    .compareTo(s2.getPrimaryKey().getApplicantId().getId()));

            /**
             * According to contract index = 1 is pointing to 0th element, so to
             * get each element here I am using index-1
             */
            SuppapplicantEntity suppapplicantEntity = suppapplicantEntities.get(index - 1);
            SuppapplicantEntity suppApplicant = suppApplicantRepository
                    .getSuppApplicant(suppapplicantEntity.getPrimaryKey().getApplicantId().getId());
            if (suppApplicant != null) {
                suppApplicantRepository.delete(suppApplicant);

            }

            List<ARNCcBridgeEntity> arnCcBridges = arnCcBridgeRepository
                    .getSuppApplicant(suppapplicantEntity.getPrimaryKey().getApplicantId().getId());
            for (ARNCcBridgeEntity arnCcBridge : arnCcBridges) {
                if (arnCcBridge != null) {
                    arnCcBridgeRepository.delete(arnCcBridge);
                }
            }

            Long id = suppapplicantEntity.getPrimaryKey().getApplicantId().getId();
            List<ApplicantAddressEntity> applicantAddressEntities = applicantAddressRepository.getApplicantAddresses(id);
            applicantAddressEntities.forEach(a -> applicantAddressRepository.delete(a));

            ApplicantDetailsEntity applicantDetails = applicantRepository
                    .getSuppApplicant(suppapplicantEntity.getPrimaryKey().getApplicantId().getId());
            if (applicantDetails != null) {
                applicantRepository.delete(applicantDetails);
            }
        } else {
            // Log.error("No suppApplicant available for this arn");
        }

    }

    @Override
    public void deleteAllSuppApplicants(String arn) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        if (ccApplicationEntity.getSuppApplicantsById() != null
                && !ccApplicationEntity.getSuppApplicantsById().isEmpty()) {

            for (SuppapplicantEntity suppapplicantEntity : ccApplicationEntity.getSuppApplicantsById()) {
                SuppapplicantEntity suppApplicant = suppApplicantRepository
                        .getSuppApplicant(suppapplicantEntity.getPrimaryKey().getApplicantId().getId());
                if (suppApplicant != null) {
                    suppApplicantRepository.delete(suppApplicant);

                }

                List<ARNCcBridgeEntity> arnCcBridges = arnCcBridgeRepository
                        .getSuppApplicant(suppapplicantEntity.getPrimaryKey().getApplicantId().getId());
                for (ARNCcBridgeEntity arnCcBridge : arnCcBridges) {
                    if (arnCcBridge != null) {
                        arnCcBridgeRepository.delete(arnCcBridge);
                    }
                }

                ApplicantDetailsEntity applicantDetails = applicantRepository
                        .getSuppApplicant(suppapplicantEntity.getPrimaryKey().getApplicantId().getId());
                if (applicantDetails != null) {
                    applicantRepository.delete(applicantDetails);
                }
            }
        }
    }

    @Override
    public void saveVerifiedIncome(String arn, int verifiedIncome, boolean isMatch) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        CCApplicationAdditionalInfoEntity ccAppAddInfo = ccApplicationEntity.getCcAppAddInfo();
        ccAppAddInfo.setEmploymentCheckIndicator(CommonUtils.convertToString(isMatch));
        ccAppAddInfo.setIncomeAutomatedCalcIndicator(CommonUtils.convertToString(isMatch));
        ApplicantAdditionalInfoEntity appAddInfo = ccApplicationEntity.getApplicantDetailsByApplicantId().getAppAddInfo();
        appAddInfo.setVerifiedIncome(verifiedIncome);
        ccApplicationRepository.save(ccApplicationEntity);
    }

    @Override
    public void updateIncomeFlag(String arn, boolean isMatch) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        CCApplicationAdditionalInfoEntity ccAppAddInfo = ccApplicationEntity.getCcAppAddInfo();
        ccAppAddInfo.setIncomeAutomatedCalcIndicator(CommonUtils.convertToString(isMatch));
        ApplicantAdditionalInfoEntity appAddInfo = ccApplicationEntity.getApplicantDetailsByApplicantId().getAppAddInfo();
        appAddInfo.setVerifiedIncome(0);
        ccApplicationRepository.save(ccApplicationEntity);

    }

    @Override
    public void updateEmploymentFlag(String arn, boolean isMatch) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        CCApplicationAdditionalInfoEntity ccAppAddInfo = ccApplicationEntity.getCcAppAddInfo();
        ccAppAddInfo.setEmploymentCheckIndicator(CommonUtils.convertToString(isMatch));
        ccAppAddInfo.setIncomeAutomatedCalcIndicator(CommonUtils.convertToString(isMatch));
        ApplicantAdditionalInfoEntity appAddInfo = ccApplicationEntity.getApplicantDetailsByApplicantId().getAppAddInfo();
        appAddInfo.setVerifiedIncome(0);
        ccApplicationRepository.save(ccApplicationEntity);
    }


}
