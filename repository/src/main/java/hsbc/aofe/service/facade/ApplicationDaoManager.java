package hsbc.aofe.service.facade;

import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.ApplicationStaffDetails;
import hsbc.aofe.service.entities.CCApplicationEntity;

import java.util.List;

public interface ApplicationDaoManager {

    public Application manageFindApplicationById(Long id);

    public List<Application> manageFindAllApplications();

    public Application manageCreateApplication(Application application);

    public void manageDeleteApplicationById(String arn);

    public Application findApplicationByArn(String arn);

    Long findApplicationIdByARN(String arn);

    Application convertCCApplicationToApplication(CCApplicationEntity ccApplicationEntity);
    
/*    public void saveStaffDetails(String arn, ApplicationStaffDetails staffDetails);*/

}
