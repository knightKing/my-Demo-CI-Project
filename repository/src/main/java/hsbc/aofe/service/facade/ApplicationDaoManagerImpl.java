package hsbc.aofe.service.facade;

import hsbc.aofe.domain.Address;
import hsbc.aofe.domain.Applicant;
import hsbc.aofe.domain.ApplicantAdditionalInfo;
import hsbc.aofe.domain.Application;
import hsbc.aofe.service.entities.AddressEntity;
import hsbc.aofe.service.entities.CCApplicationAdditionalInfoEntity;
import hsbc.aofe.service.entities.CCApplicationEntity;
import hsbc.aofe.service.entities.SuppapplicantEntity;
import hsbc.aofe.service.jparepository.CCApplicationAdditionalInfoRepository;
import hsbc.aofe.service.jparepository.CCApplicationRepository;
import hsbc.aofe.service.transformer.Transformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ApplicationDaoManagerImpl implements ApplicationDaoManager {

    private CCApplicationRepository ccApplicationRepository;
    private CCApplicationAdditionalInfoRepository ccApplicationAdditionalInfoRepository;

    @Autowired
    public ApplicationDaoManagerImpl(CCApplicationRepository ccApplicationRepository,
                                     CCApplicationAdditionalInfoRepository ccApplicationAdditionalInfoRepository) {
        this.ccApplicationRepository = ccApplicationRepository;
        this.ccApplicationAdditionalInfoRepository = ccApplicationAdditionalInfoRepository;
    }

    @Override
    public Application manageCreateApplication(Application application) {
        CCApplicationEntity ccApplicationEntity = new CCApplicationEntity();
        ccApplicationEntity = Transformation.populateApplicationEntity(ccApplicationEntity, application);
        ccApplicationEntity = ccApplicationRepository.save(ccApplicationEntity);
        //application.setId(ccApplicationEntity.getId());
        CCApplicationAdditionalInfoEntity ccApplicationAdditionalInfoEntity = new CCApplicationAdditionalInfoEntity();
        ccApplicationAdditionalInfoEntity = Transformation.populateApplicationAdditionalInfoEntity(ccApplicationAdditionalInfoEntity, application);
        ccApplicationAdditionalInfoEntity = ccApplicationAdditionalInfoRepository.save(ccApplicationAdditionalInfoEntity);
        return application;
    }

    @Override
    public Application manageFindApplicationById(Long id) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findOne(id);
        /*Application application = Transformation.transformApplicationEntityToApplication(ccApplicationEntity);
        CCApplicationAdditionalInfoEntity ccApplicationAdditionalInfoEntity = ccApplicationAdditionalInfoRepository.findOne(id);
		return Transformation.transformApplicationAdditionalInfoEntityToApplication(application, ccApplicationAdditionalInfoEntity);*/
        return Transformation.transformApplicationEntityToApplication(ccApplicationEntity);
    }

    @Override
    public List<Application> manageFindAllApplications() {
        List<CCApplicationEntity> ccApplicationEntities = ccApplicationRepository.findAll();
        List<Application> applications = ccApplicationEntities.stream()
                .map(a -> findApplicationByArn(a.getArn()))
                .collect(Collectors.toList());
        return applications;
    }

    @Override
    public void manageDeleteApplicationById(String arn) {
        long id = ccApplicationRepository.findByArn(arn).getId();
        ccApplicationAdditionalInfoRepository.delete(id);
        ccApplicationRepository.delete(id);
    }

    @Override
    public Application findApplicationByArn(String arn) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        Application application = convertCCApplicationToApplication(ccApplicationEntity);

        return application;
    }

    public Application convertCCApplicationToApplication(CCApplicationEntity ccApplicationEntity) {
        Application application = Transformation.transformApplicationEntityToApplication(ccApplicationEntity);

        List<AddressEntity> addressEntities = ccApplicationEntity.getApplicantDetailsByApplicantId().getAddressesById();
        if (addressEntities != null && !addressEntities.isEmpty()) {
            List<Address> addresses = convertToAddressList(addressEntities);

            if (application.getPrimaryApplicant().getAdditionalInfo() == null) {
                ApplicantAdditionalInfo applicantAdditionalInfo = new ApplicantAdditionalInfo();
                application.getPrimaryApplicant().setAdditionalInfo(applicantAdditionalInfo);
            }
            application.getPrimaryApplicant().getAdditionalInfo().setAddress(addresses);
            if(ccApplicationEntity.getApplicantDetailsByApplicantId().getAppAddInfo()==null){
                application.getPrimaryApplicant().getAdditionalInfo().setVerifiedIncome(null);
            }  else {
                application.getPrimaryApplicant().getAdditionalInfo().setVerifiedIncome(ccApplicationEntity.getApplicantDetailsByApplicantId().getAppAddInfo().getVerifiedIncome());
            }
        }

        List<SuppapplicantEntity> suppapplicantEntities = ccApplicationEntity.getSuppApplicantsById();
        if(suppapplicantEntities != null && !suppapplicantEntities.isEmpty()) {
            List<Applicant> suppApplicants = convertToSupplementaryApplicant(suppapplicantEntities);

            for(int i=0; i<suppApplicants.size(); i++) {
                List<AddressEntity> suppAddressEntities = suppapplicantEntities.get(i).getPrimaryKey().getApplicantId().getAddressesById();
                if(suppAddressEntities != null && !suppAddressEntities.isEmpty()) {
                    List<Address> suppAddresses = convertToAddressList(suppAddressEntities);

                    if(application.getSupplementaryApplicant() == null) {
                        application.setSupplementaryApplicant(suppApplicants);
                    }
                    Applicant suppApplicant = application.getSupplementaryApplicant().get(i);
                    if(suppApplicant.getAdditionalInfo() == null) {
                        ApplicantAdditionalInfo applicantAdditionalInfo = new ApplicantAdditionalInfo();
                        suppApplicant.setAdditionalInfo(applicantAdditionalInfo);
                    }
                    suppApplicant.getAdditionalInfo().setAddress(suppAddresses);
                }
            }
            suppApplicants = suppApplicants.stream().sorted(Comparator.comparing(a -> a.getId())).collect(Collectors.toList());
            application.setSupplementaryApplicant(suppApplicants);

        }

        return application;
    }

    private List<Applicant> convertToSupplementaryApplicant(List<SuppapplicantEntity> suppapplicantEntities) {
        return suppapplicantEntities.stream().map(a -> Transformation.transformApplicantEntityToApplicant(a.getPrimaryKey().getApplicantId())).collect(Collectors.toList());
    }

    private List<Address> convertToAddressList(List<AddressEntity> addressEntities) {
        return addressEntities.stream()
                .sorted(Comparator.comparing(x -> x.getId()))
                .map(a -> Transformation.transferAddressEntityTOAddress(a)).collect(Collectors.toList());
    }

    @Override
    public Long findApplicationIdByARN(String arn) {
        return ccApplicationRepository.findIdByArn(arn);
    }

/*	@Override
	public void saveStaffDetails(String arn, ApplicationStaffDetails staffDetails) {
		// TODO Auto-generated method stub
		
	}*/


    /*@Override
    public Application findApplicationByArn(String arn) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        Application application = convertCCApplicationToApplication(ccApplicationEntity);

        return application;
    }

    public Application convertCCApplicationToApplication(CCApplicationEntity ccApplicationEntity) {
        Application application = Transformation.transformApplicationEntityToApplication(ccApplicationEntity);
        if (ccApplicationEntity.getApplicantDetailsByApplicantId().getAddressesById() != null
                && !ccApplicationEntity.getApplicantDetailsByApplicantId().getAddressesById().isEmpty()) {
            List<Address> addresses = ccApplicationEntity.getApplicantDetailsByApplicantId().getAddressesById()
                    .stream()
                    .map(a -> Transformation.transferAddressEntityTOAddress(a))
                    .collect(Collectors.toList());

            if (application.getPrimaryApplicant().getAdditionalInfo() == null) {
                ApplicantAdditionalInfo applicantAdditionalInfo = new ApplicantAdditionalInfo();
                application.getPrimaryApplicant().setAdditionalInfo(applicantAdditionalInfo);
            }
            application.getPrimaryApplicant().getAdditionalInfo().setAddress(addresses);
        }
        return application;
    }

    @Override
    public Long findApplicationIdByARN(String arn) {
        return ccApplicationRepository.findIdByArn(arn);
    }*/

}
