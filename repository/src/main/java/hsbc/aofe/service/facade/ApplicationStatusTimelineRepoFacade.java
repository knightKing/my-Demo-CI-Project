package hsbc.aofe.service.facade;

import hsbc.aofe.service.entities.ApplicationStatusTimelineEntity;
import hsbc.aofe.service.jparepository.ApplicationStatusTimelineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*****************************************
 * Created by sahil.verma on 7/1/2017.
 *****************************************
 */
@Component
public class ApplicationStatusTimelineRepoFacade {

    private ApplicationStatusTimelineRepository applicationStatusTimelineRepository;

    @Autowired
    public ApplicationStatusTimelineRepoFacade(ApplicationStatusTimelineRepository applicationStatusTimelineRepository) {
        this.applicationStatusTimelineRepository = applicationStatusTimelineRepository;
    }

    public void saveApplicationStatusTimeline(ApplicationStatusTimelineEntity statusTimelineEntity) {
        applicationStatusTimelineRepository.save(statusTimelineEntity);
    }

}
