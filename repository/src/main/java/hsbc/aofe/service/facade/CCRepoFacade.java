package hsbc.aofe.service.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.service.entities.CreditCardMasterEntity;
import hsbc.aofe.service.jparepository.CreditCardMasterRepository;
import hsbc.aofe.service.transformer.Transformation;

@Component
public class CCRepoFacade {


    private CreditCardMasterRepository cardMasterRepository;

    @Autowired
    public CCRepoFacade(CreditCardMasterRepository cardMasterRepository) {
        this.cardMasterRepository = cardMasterRepository;
    }

    public List<CreditCard> getAllCreditCards() {

        final List<CreditCardMasterEntity> creditCardMasterEntities = cardMasterRepository.findAll();
        return Transformation.transformCreditCardMasterEntityToCreditCard(creditCardMasterEntities);
    }
    
    public List<CreditCard> getCreditCardsForDigitalJourney() {

        final List<CreditCardMasterEntity> creditCardMasterEntities = cardMasterRepository.findByEligibleForCustomer("Y");
        return Transformation.transformCreditCardMasterEntityToCreditCard(creditCardMasterEntities);
    }
}
