package hsbc.aofe.service.facade;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hsbc.aofe.domain.*;
import hsbc.aofe.exception.InvalidOperationException;
import hsbc.aofe.service.entities.CCApplicationEntity;
import hsbc.aofe.service.entities.RemarksEntity;
import hsbc.aofe.service.jparepository.CCApplicationRepository;
import hsbc.aofe.service.jparepository.DashboardListingRepository;
import hsbc.aofe.service.jparepository.RemarksRepository;
import hsbc.aofe.service.transformer.Transformation;

@Component
public class DashboardListingDaoManagerImpl implements DashboardListingDaoManager {

    DashboardListingRepository dashboardListingRepository;
    CCApplicationRepository ccApplicationRepository;
    RemarksRepository remarksRepository;

    @Autowired
    public DashboardListingDaoManagerImpl(DashboardListingRepository dashboardListingRepository,
                                          CCApplicationRepository ccApplicationRepository, RemarksRepository remarksRepository) {
        this.dashboardListingRepository = dashboardListingRepository;
        this.ccApplicationRepository = ccApplicationRepository;
        this.remarksRepository = remarksRepository;
    }

    @Override
    public DashboardListing manageFindAllApplications(List<String> sortList, List<String> filter, String uid,
                                                      int offset, int pageLimit) {

        String sortedBy = sortApplications(sortList);
        String filteredBy = filterApplications(filter, uid);

        int start = pageLimit * (offset - 1);
        int end = pageLimit * offset;

        List<Dashboard> dashboardDetails = dashboardListingRepository.getApplications(sortedBy, filteredBy, uid, start, end);
        DashboardListing dashboardListing = new DashboardListing();
        dashboardListing.setDashboards(dashboardDetails);
        if(!dashboardDetails.isEmpty()){
        dashboardListing.setTotalCount(dashboardDetails.get(0).getTotalCount());}

        return dashboardListing;
    }

    public String sortApplications(List<String> sortList) {

        StringBuilder sortBy = new StringBuilder();
        for (String sort : sortList) {
            String[] sortValues = sort.split("_");
            String sortedValues = sortValues[0].toUpperCase() + "  " + sortValues[1];
            sortBy.append(sortedValues + ",");

        }

        int sortByLastIndex = sortBy.lastIndexOf(",");
        sortBy.deleteCharAt(sortByLastIndex);

        return sortBy.toString();
    }

    public String filterApplications(List<String> filterList, String uid) {

        StringBuilder filterBy = new StringBuilder();

        String[] columnList = {"CM.NAME", "NG.KEY", "SGHAO.PROD_STAT_CDE"};

        if (filterList != null) {

            for (String filter : filterList) {

                String[] filterValues = filter.split("_");
                
                if(filterValues.length != 3){
                	throw new InvalidOperationException("Invalid operation. Please send correct data");
                }             

                if ("common".equalsIgnoreCase(filterValues[0])) {

                    filterBy.append(" AND (");
                    filterBy.append("LOWER( " +DashboardColumnDetails.valueOf("CUSTOMERNAME").getColumnName() + ")");
                    filterBy.append(" " + DashboardFilterOperators.valueOf(filterValues[1]).getOperator() + " ");
                    filterBy.append("LOWER( '%" + filterValues[2] + "%') OR ");

                    filterBy.append("LOWER( " + DashboardColumnDetails.valueOf("IDVALUE").getColumnName() + ")");
                    filterBy.append(" " + DashboardFilterOperators.valueOf(filterValues[1]).getOperator() + " ");
                    filterBy.append("LOWER( '%" + filterValues[2] + "%' ) OR ");

                    if (("default".equals(uid))) {
                        filterBy.append("LOWER( " + DashboardColumnDetails.valueOf("STAFFID").getColumnName() + ")");
                        filterBy.append(" " + DashboardFilterOperators.valueOf(filterValues[1]).getOperator() + " ");
                        filterBy.append("LOWER( '%" + filterValues[2] + "%' ) OR ");
                    }
                    
                    filterBy.append("LOWER( " + DashboardColumnDetails.valueOf("MOBILE").getColumnName() + ")");
                    filterBy.append(" " + DashboardFilterOperators.valueOf(filterValues[1]).getOperator() + " ");
                    filterBy.append("LOWER( '%" + filterValues[2] + "%' ) OR ");
                    
                    filterBy.append("LOWER( " + DashboardColumnDetails.valueOf("EMAIL").getColumnName() + ")");
                    filterBy.append(" " + DashboardFilterOperators.valueOf(filterValues[1]).getOperator() + " ");
                    filterBy.append("LOWER( '%" + filterValues[2] + "%' ) OR ");
                                        
                    filterBy.append("LOWER( " + DashboardColumnDetails.valueOf("AOREFNO").getColumnName() + ")" );
                    filterBy.append(" " + DashboardFilterOperators.valueOf(filterValues[1]).getOperator() + " ");
                    filterBy.append("LOWER( '%" + filterValues[2] + "%' ) )");
                } else if (!filterValues[2].equals("ALL")) {

                    String columnName = DashboardColumnDetails.valueOf(filterValues[0]).getColumnName();

                    filterBy.append(" AND ");

                    filterBy.append("LOWER( " +  columnName + ")");

                    filterBy.append(" " + DashboardFilterOperators.valueOf(filterValues[1]).getOperator() + " ");

                    if (Arrays.asList(columnList).contains(columnName)) {
                        if (columnName.equals("CM.NAME")) {
                            filterBy.append("LOWER( '" + ChannelType.valueOf(filterValues[2]).getChannel() + "') ");
                        } else if (columnName.equals("SGHAO.PROD_STAT_CDE")) {
                            filterBy.append("LOWER(  '" + AoStatus.valueOf(filterValues[2]).getAoStatus() + "' )");
                        } else {

                            filterBy.append("LOWER(  '" + NgStatus.valueOf(filterValues[2]).getValue() + "' )");
                        }
                    }
                }
                if ("FFAGENTNAME".equalsIgnoreCase(filterValues[0])) {
                	
                    filterBy.append(" LOWER( '%" + filterValues[2] + "%' ) ");

                }
            }
        }

        return filterBy.toString();
    }

    @Override
    public String updateFulfilmentAgent(String fulfilmentAgent, String arn) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        ccApplicationEntity.setFulfilmentAgent(fulfilmentAgent);
        ccApplicationRepository.save(ccApplicationEntity);
        return ccApplicationEntity.getFulfilmentAgent();
    }

    @Override
    public void addRemarks(String arn, Remarks remarks, RemarksType type) {
        CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        String localDateTime = LocalDateTime.now().toString();
        remarks.getRemark().stream().forEach(remark -> {
        	RemarksEntity remarksEntity = new RemarksEntity();
            remarksEntity.setCcApplicationEntity(ccApplicationEntity);
            remarksEntity.setDateTime(localDateTime);
            remarksEntity.setStaffId(remarks.getStaffId());
            remarksEntity.setType(type.getValue());
            remarksEntity.setRemarks(remark);
            remarksRepository.save(remarksEntity);	
        });
    }

    @Override
    public List<Remarks> getRemarksByType(String arn, RemarksType type) {
        List<RemarksEntity> remarksEntities = remarksRepository.findRemarksByType(arn, type.getValue());
        Collections.sort(remarksEntities, (o1, o2) -> o1.getDateTime().compareTo(o2.getDateTime()));

        List<Remarks> remarksList = new ArrayList<>();

        remarksEntities.stream().forEach(remarksEntity -> remarksList
                .add(Transformation.tranformRemarksEntityToRemarks(new Remarks(), remarksEntity)));

        return remarksList;

    }
}
