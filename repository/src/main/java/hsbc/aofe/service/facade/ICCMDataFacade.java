package hsbc.aofe.service.facade;

import hsbc.aofe.service.entities.ARNCcBridgeEntity;
import hsbc.aofe.service.entities.CCApplicationEntity;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/*****************************************
 * Created by sahil.verma on 8/4/2017.
 *****************************************
 */
@Data
public class ICCMDataFacade {

    private CCApplicationEntity application;
    protected static final Map<String, String> FIELD_METHOD_MAPPING = new HashMap<>();

    static {
        FIELD_METHOD_MAPPING.put("MOBILE", "getMobile");
        FIELD_METHOD_MAPPING.put("ARN", "getArn");
        FIELD_METHOD_MAPPING.put("IDVALUE", "getIdValue");
        FIELD_METHOD_MAPPING.put("PRODUCTCARD2", "getProductCard2");
        FIELD_METHOD_MAPPING.put("PRODUCTCARD1", "getProductCard1");
        FIELD_METHOD_MAPPING.put("FIRSTNAME", "getFirstName");
        FIELD_METHOD_MAPPING.put("MIDDLENAME", "getMiddleName");
        FIELD_METHOD_MAPPING.put("LASTNAME", "getLastName");
        FIELD_METHOD_MAPPING.put("EMAIL", "getEmail");
        FIELD_METHOD_MAPPING.put("SALUTATION", "getSalutation");
        FIELD_METHOD_MAPPING.put("DIALCODEMOBILE", "getDialCodeMobile");
    }

    public ICCMDataFacade(CCApplicationEntity application) {
        this.application = application;
    }


    public String getSalutation() {
        return this.application.getApplicantDetailsByApplicantId().getSalutation();
    }

    public String getFirstName() {
        return this.application.getApplicantDetailsByApplicantId().getFirstName();
    }

    public String getMiddleName() {
        return this.application.getApplicantDetailsByApplicantId().getMiddleName();
    }

    public String getLastName() {
        return this.application.getApplicantDetailsByApplicantId().getLastName();
    }

    public String getEmail() {
        return this.application.getApplicantDetailsByApplicantId().getEmail();
    }

    public String getProductCard1() {
        ARNCcBridgeEntity arnCcBridgeEntity = this.application.getApplicantDetailsByApplicantId().getArnCcBridgesById().get(0);
        String card1Name = arnCcBridgeEntity.getPrimaryKey().getCreditCardMasterByCcId().getName();
        return getModifiedCardName(card1Name);
    }

    public String getProductCard2() {
        String card2Name = null;
        if (this.application.getApplicantDetailsByApplicantId().getArnCcBridgesById().size() > 1) {
            card2Name = this.application.getApplicantDetailsByApplicantId().getArnCcBridgesById().get(1).getPrimaryKey().getCreditCardMasterByCcId().getName();
            card2Name = getModifiedCardName(card2Name);
        }
        return card2Name;
    }

    public Long getMobile() {
        return this.application.getApplicantDetailsByApplicantId().getMobile();
    }

    public String getArn() {
        return this.application.getArn();
    }

    public String getIdValue() {
        return this.application.getApplicantDetailsByApplicantId().getIdValue();
    }

    public String getDialCodeMobile() {
        return this.application.getApplicantDetailsByApplicantId().getDialCodeMobile();
    }

    private static String getModifiedCardName(String cardName) {
        String[] cardNameElements = cardName.split(" ");
        StringBuilder modifiedCardName = new StringBuilder();
        if (cardNameElements.length == 4)
            modifiedCardName.append(cardNameElements[1]);
        else {
            modifiedCardName.append(cardNameElements[1] + " ");
            modifiedCardName.append(cardNameElements[2]);
        }
        return modifiedCardName.toString();
    }
}
