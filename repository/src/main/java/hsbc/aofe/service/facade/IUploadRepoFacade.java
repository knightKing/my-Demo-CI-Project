package hsbc.aofe.service.facade;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.DocumentGroup;
import hsbc.aofe.domain.DocumentName;
import hsbc.aofe.iupload.domain.IUploadDetail;
import hsbc.aofe.iupload.domain.IUploadImage;
import hsbc.aofe.iupload.domain.IUploadRequestData;
import hsbc.aofe.service.entities.ApplicantDetailsEntity;
import hsbc.aofe.service.entities.ApplicantDocumentEntity;
import hsbc.aofe.service.entities.CCApplicationEntity;
import hsbc.aofe.service.entities.IUploadEntity;
import hsbc.aofe.service.entities.SuppapplicantEntity;
import hsbc.aofe.service.jparepository.CCApplicationRepository;
import hsbc.aofe.service.jparepository.IUploadRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IUploadRepoFacade {

	private CCApplicationRepository ccApplicationRepository;
	private IUploadRepository iUploadRepository;

	@Autowired
	public IUploadRepoFacade(CCApplicationRepository ccApplicationRepository, IUploadRepository iUploadRepository) {
		this.ccApplicationRepository = ccApplicationRepository;
		this.iUploadRepository = iUploadRepository;
	}

	/**
	 * This method is used to return ARNS from {@link CCApplicationEntity} based
	 * on the input status
	 *
	 * @param status
	 * @return List of ARN
	 */
	public List<String> getARNsExcludingIUploadARNs(String status) {
		log.debug("Inside getARNsExcludingIUploadARNs with status {}.", status);
		List<String> arnByAOStatus = ccApplicationRepository.findARNByNGStatusExcludingIUploadARN(status);
		log.debug("Arns retrieved {}.", arnByAOStatus);
		return arnByAOStatus;
	}

	/**
	 * This method is used to save IUploadRequestData to IUpload table
	 * 
	 * @param iccRequestBody
	 */
	public void saveStatus(IUploadRequestData iccRequestBody) {
		log.debug("Inside saveStatus");
		IUploadEntity iUploadEntity = new IUploadEntity();
		iUploadEntity.setArn(iccRequestBody.getArn());
		iUploadEntity.setFoldername(iccRequestBody.getFolderName());
		iUploadEntity.setUploadstatus(iccRequestBody.getUploadStatus());
		LocalDateTime localDateTime = LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault());
		iUploadEntity.setCreatedon(localDateTime);
		iUploadRepository.save(iUploadEntity);
	}

	public IUploadDetail findApplicationByARN(String arn) {
		CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
		IUploadDetail iUploadDetail = new IUploadDetail();
		iUploadDetail.setId(arn);
		iUploadDetail.setAoRefNumber(ccApplicationEntity.getAoRefNum());

		iUploadDetail
				.setIdType(Arrays.asList(DocumentName.values()).stream()
						.filter(a -> a.getValue()
								.equalsIgnoreCase(ccApplicationEntity.getApplicantDetailsByApplicantId()
										.getDocumentsMasterByIdDocType().getKey()))
						.collect(Collectors.toList()).get(0));

		iUploadDetail.setIdValue(ccApplicationEntity.getApplicantDetailsByApplicantId().getIdValue());

		ApplicantDetailsEntity primaryApplicantDetails = ccApplicationEntity.getApplicantDetailsByApplicantId();
		List<IUploadImage> iUploadImages = new ArrayList<>();
		Long primaryApplicantId = primaryApplicantDetails.getId();
		String primaryApplicantName = getCompleteName(primaryApplicantDetails.getFirstName(),
				primaryApplicantDetails.getMiddleName(), primaryApplicantDetails.getLastName());
		List<ApplicantDocumentEntity> primaryapplicantDocumentEntities = primaryApplicantDetails
				.getApplicantDocumentsById();
		primaryapplicantDocumentEntities.stream().forEach(applicantDocumentEntity -> {
			applicantDocumentEntity.getImageById().stream().forEach(image -> {
				IUploadImage iUploadImage = new IUploadImage();
				iUploadImage.setApplicantId(primaryApplicantId);
				iUploadImage.setApplicantName(primaryApplicantName);
				iUploadImage
						.setDocumentGroup(Arrays.asList(DocumentGroup.values()).stream()
								.filter(a -> a.getValue()
										.equals(applicantDocumentEntity.getDocumentsMasterByDocumentType()
												.getDocGroupMasterByDocGroup().getName()))
								.collect(Collectors.toList()).get(0));
				iUploadImage.setImageIndex(image.getImageIndex());
				iUploadImage.setImageName(image.getName());
				iUploadImage.setPhoto(image.getPhoto());
				iUploadImages.add(iUploadImage);
			});
		});

		List<SuppapplicantEntity> suppapplicantEntities = ccApplicationEntity.getSuppApplicantsById();
		if (CollectionUtils.isNotEmpty(suppapplicantEntities)) {
			suppapplicantEntities.stream().forEach(suppapplicantEntity -> {
				ApplicantDetailsEntity supplementaryApplicantDetailsEntity = suppapplicantEntity.getPrimaryKey()
						.getApplicantId();
				Long supplementaryApplicantId = supplementaryApplicantDetailsEntity.getId();
				String supplementaryApplicantName = getCompleteName(supplementaryApplicantDetailsEntity.getFirstName(),
						supplementaryApplicantDetailsEntity.getMiddleName(),
						supplementaryApplicantDetailsEntity.getLastName());
				List<ApplicantDocumentEntity> supplementaryApplicantDocumentEntities = supplementaryApplicantDetailsEntity
						.getApplicantDocumentsById();
				supplementaryApplicantDocumentEntities.stream().forEach(supplementaryApplicantDocumentEntity -> {
					supplementaryApplicantDocumentEntity.getImageById().stream().forEach(image -> {
						IUploadImage iUploadImage = new IUploadImage();
						iUploadImage.setApplicantId(supplementaryApplicantId);
						iUploadImage.setApplicantName(supplementaryApplicantName);
						iUploadImage.setDocumentGroup(Arrays.asList(DocumentGroup.values()).stream()
								.filter(a -> a.getValue()
										.equals(supplementaryApplicantDocumentEntity.getDocumentsMasterByDocumentType()
												.getDocGroupMasterByDocGroup().getName()))
								.collect(Collectors.toList()).get(0));
						iUploadImage.setImageIndex(image.getImageIndex());
						iUploadImage.setImageName(image.getName());
						iUploadImage.setPhoto(image.getPhoto());
						iUploadImages.add(iUploadImage);
					});
				});
			});
		}
		iUploadDetail.setImages(iUploadImages);
		return iUploadDetail;
	}

	private String getCompleteName(String firstName, String middleName, String lastName) {

	  StringJoiner completeName = new StringJoiner(" ");
	  if (StringUtils.isNotEmpty(firstName)) {
	   completeName.add(firstName);
	  }
	  if (StringUtils.isNotEmpty(middleName)) {
	   completeName.add(middleName);
	  }
	  if (StringUtils.isNotEmpty(lastName)) {
	   completeName.add(lastName);
	  }
	  return completeName.toString();
	 }
}
