package hsbc.aofe.service.facade;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.ChannelType;
import hsbc.aofe.domain.IccmEvent;
import hsbc.aofe.domain.NgStatus;
import hsbc.aofe.iccm.config.ICCMConfigProperties;
import hsbc.aofe.iccm.domain.IccmCommunicationStatus;
import hsbc.aofe.iccm.domain.NotificationRequest;
import hsbc.aofe.service.entities.ApplicationStatusTimelineEntity;
import hsbc.aofe.service.entities.CCApplicationEntity;
import hsbc.aofe.service.entities.EventMasterEntity;
import hsbc.aofe.service.entities.IccmEventDataEntity;
import hsbc.aofe.service.entities.IccmEventEntity;
import hsbc.aofe.service.entities.TemplateFieldsEntity;
import hsbc.aofe.service.jparepository.CCApplicationRepository;
import hsbc.aofe.service.jparepository.ChannelMasterRepository;
import hsbc.aofe.service.jparepository.EventMasterRepository;
import hsbc.aofe.service.jparepository.IccmEventDataRepository;
import hsbc.aofe.service.jparepository.IccmEventRepository;
import hsbc.aofe.service.jparepository.TemplateFieldsRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IccmRepoFacade {

    private static final String OTP = "OTP";
    private final int SECONDS_IN_A_DAY = 86400;
    private static final String PRIORITY = "H";
    private static final String FAREMARK = "FAREMARKLINE";
    private static final String IDVALUE = "IDVALUE";
    private static final String MOBILE = "MOBILE";
    private static final String DIALCODEMOBILE = "DIALCODEMOBILE";
    private static final String EMAIL = "EMAIL";
    private static final String SALUTATION = "SALUTATION";
    private static final String FIRSTNAME = "FIRSTNAME";
    private static final String MIDDLENAME = "MIDDLENAME";
    private static final String LASTNAME = "LASTNAME";
    private static final String PRODUCTCARD1 = "PRODUCTCARD1";
    private static final String PRODUCTCARD2 = "PRODUCTCARD2";
    private static final String FAREMARKLINE1 = "FAREMARKLINE1";
    private static final String FAREMARKLINE2 = "FAREMARKLINE2";
    private static final String FAREMARKLINE3 = "FAREMARKLINE3";
    private static final String FAREMARKLINE4 = "FAREMARKLINE4";
    private static final String FAREMARKLINE5 = "FAREMARKLINE5";

    private EventMasterRepository eventMasterRepository;
    private IccmEventRepository iccmEventRepository;
    private TemplateFieldsRepository templateFieldsRepository;
    private CCApplicationRepository ccApplicationRepository;
    private ChannelMasterRepository channelMasterRepository;
    private IccmEventDataRepository iccmEventDataRepository;
    private ICCMConfigProperties iCCMConfigProperties;

    public IccmRepoFacade(EventMasterRepository eventMasterRepository,
    						IccmEventRepository iccmEventRepository,
    						TemplateFieldsRepository templateFieldsRepository,
    						CCApplicationRepository ccApplicationRepository,
    						ChannelMasterRepository channelMasterRepository,
    						IccmEventDataRepository iccmEventDataRepository,
    						ICCMConfigProperties iCCMConfigProperties) {
        this.eventMasterRepository = eventMasterRepository;
        this.iccmEventRepository = iccmEventRepository;
        this.templateFieldsRepository = templateFieldsRepository;
        this.ccApplicationRepository = ccApplicationRepository;
        this.channelMasterRepository = channelMasterRepository;
        this.iccmEventDataRepository = iccmEventDataRepository;
        this.iCCMConfigProperties = iCCMConfigProperties;
    }

    /**
     * @param event
     * @param arn
     * @param channel
     * @param uidOtp,  user id, but it contains OTP in case of customer
     * @param remarks, fulfillment or power user remarks
     * @return
     */
    public NotificationRequest populateIccmNotificationRequest(String event, String arn, String channel, String uidOtp, List<String> remarks) {

        // notification request that is used to generate iccm request message
        NotificationRequest notificationRequest = new NotificationRequest();
        // to get template
        EventMasterEntity eventMasterEntity = eventMasterRepository.findByEventKey(event);
        final String template = eventMasterEntity.getTemplateMasterEntity().getKey();

        // get application for an ARN
        CCApplicationEntity applicationbyArn = ccApplicationRepository.findByArn(arn);

        // fetch all the fields corresponding to a template
        List<TemplateFieldsEntity> templateFieldsEntities = templateFieldsRepository.findByTemplateMasterEntityKey(template);

        // create and populate temp map
        Map<String, String> templateFieldsValueMap = getTemplateFieldsMap(templateFieldsEntities);

        // if remarks present, then populate templateFieldsValueMap with it
        if (CollectionUtils.isNotEmpty(remarks)) {
            populateRemarks(remarks, templateFieldsValueMap);
        }

        // map db fields with application object and populate map with values from application object
        templateFieldsValueMap = fetchFieldsValue(applicationbyArn, templateFieldsValueMap);

        // transform to Domain
        populateNotificationRequest(templateFieldsValueMap, notificationRequest);

        // populate notification request
        if (OTP.equalsIgnoreCase(event)) {
            notificationRequest.setOtp(uidOtp);
        }
        notificationRequest.setIccmTemplate(template);
        notificationRequest.setArn(arn);
        notificationRequest.setSendWhen(LocalDateTime.now());
        notificationRequest.setStatus(IccmCommunicationStatus.SEND.toString());
        notificationRequest.setRetryAttempts(0L);
        notificationRequest.setPriority(PRIORITY);
        notificationRequest.setTrackingField(arn + Instant.now().toEpochMilli());
        try {
			Thread.sleep(10L);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			e.printStackTrace();
		}
        return notificationRequest;
    }
    
    private Map<String, String> getTemplateFieldsMap(List<TemplateFieldsEntity> templateFieldsEntities) {
    	Map<String, String> templateFieldsValueMap = new HashMap<>();
    	for (TemplateFieldsEntity templateFieldsEntity : templateFieldsEntities) {
        	String fieldName = templateFieldsEntity.getFieldMasterEntity().getFieldName();
			templateFieldsValueMap.put(fieldName, null);
		}
    	return templateFieldsValueMap;
    }


    public IccmEventEntity saveIccm(String event, String arn, String channel, String uidOtp, List<String> remarks, NotificationRequest notificationRequest) {

    	// To populate and save iccmEventEntity
        IccmEventEntity iccmEventEntity = new IccmEventEntity();
        EventMasterEntity eventMasterEntity = eventMasterRepository.findByEventKey(event);
        
        // Application from DB by ARN.
        CCApplicationEntity applicationbyArn = ccApplicationRepository.findByArn(arn);

        final String template = eventMasterEntity.getTemplateMasterEntity().getKey();
        List<TemplateFieldsEntity> templateFieldsEntities = templateFieldsRepository.findByTemplateMasterEntityKey(template);

        Map<String, String> templateFieldsValueMap = getTemplateFieldsMap(templateFieldsEntities);

        // Populate remarks for if not null and not empty.
        if (CollectionUtils.isNotEmpty(remarks)) {
            populateRemarks(remarks, templateFieldsValueMap);
        }
        
        // In case of OTP (event), populate otp. Otherwise populate userId of the logged in user.
        if (OTP.equalsIgnoreCase(event)) {
        	templateFieldsValueMap.put(OTP, uidOtp);
        } else {
            iccmEventEntity.setStaffId(uidOtp);
        }

        // fetch field values
        templateFieldsValueMap = fetchFieldsValue(applicationbyArn, templateFieldsValueMap);

        // populate Iccm_Event
        List<IccmEventDataEntity> iccmEventDataEntities = populateIccmEventData(templateFieldsEntities, templateFieldsValueMap);

        if (ChannelType.S.getChannel().equalsIgnoreCase(channel)) {
            iccmEventEntity.setSendWhen(LocalDateTime.now().plusMinutes(iCCMConfigProperties.getIccmSchedularConfig().getRoadshowDelay()));
            iccmEventEntity.setNotificationType("ROADSHOW");
            iccmEventEntity.setStatus(IccmCommunicationStatus.NEW);
        } else {
            iccmEventEntity.setSendWhen(LocalDateTime.now());
            iccmEventEntity.setStatus((notificationRequest.getStatus() == null) ? null 
            		: Arrays.asList(IccmCommunicationStatus.values()).stream()
            				.filter(a -> a.toString().equalsIgnoreCase(notificationRequest.getStatus()))
            						.collect(Collectors.toList()).get(0));
            
        }
        
        //iccmEventDataRepository.save(iccmEventDataEntities);
        iccmEventEntity.setEventData(iccmEventDataEntities);
        iccmEventEntity.setEventMasterEntity(eventMasterEntity);
        iccmEventEntity.setChannelMasterById(channelMasterRepository.findByName(channel));
        iccmEventEntity.setCcApplicationEntityByArn(applicationbyArn);
        iccmEventEntity.setRetryCount(0L);
        iccmEventEntity.setTrackingField(notificationRequest.getTrackingField());
        return iccmEventRepository.save(iccmEventEntity);
        
    }

    public List<IccmEventEntity> saveFollowUpApplicationsInIccmEvent() {
    	
    	//ccApplicationRepository.followUpNamedStoredProcedure(1, 1, 1);

        final Long firstFollowUp = iCCMConfigProperties.getIccmSchedularConfig().getFirstFollowUp();
        final Long secondFollwUp = iCCMConfigProperties.getIccmSchedularConfig().getSecondFollowUp();
        final Long thirdFollowUp = iCCMConfigProperties.getIccmSchedularConfig().getThirdFollowUp();
        final Long cancellationFollowUp = iCCMConfigProperties.getIccmSchedularConfig().getCancellationFollowUp();

        iccmEventRepository.followUpNamedStoredProcedure(firstFollowUp, secondFollwUp, thirdFollowUp, cancellationFollowUp);
        
        // get all the applications in db
        /*List<CCApplicationEntity> dbApplications = (List<CCApplicationEntity>) ccApplicationRepository.findAll();

        List<CCApplicationEntity> filteredApplications = dbApplications.stream()
                .filter(a -> NgStatus.PENDINGWITHCUSTOMER.getValue().equalsIgnoreCase(getLatestStatus(a).getNgStatusMasterByNgStatus().getKey()))
                .filter(a -> (getApplicationAge(a).getSeconds() >= SECONDS_IN_A_DAY * firstFollowUp && getApplicationAge(a).getSeconds() < SECONDS_IN_A_DAY * (firstFollowUp + 1))
                        || (getApplicationAge(a).getSeconds() >= SECONDS_IN_A_DAY * secondFollwUp && getApplicationAge(a).getSeconds() < SECONDS_IN_A_DAY * (secondFollwUp + 1))
                        || (getApplicationAge(a).getSeconds() >= SECONDS_IN_A_DAY * thirdFollowUp && getApplicationAge(a).getSeconds() < SECONDS_IN_A_DAY * (thirdFollowUp + 1))
                        || (getApplicationAge(a).getSeconds() >= SECONDS_IN_A_DAY * cancellationFollowUp && getApplicationAge(a).getSeconds() < SECONDS_IN_A_DAY * (cancellationFollowUp + 1)))
                .collect(Collectors.toList());

        List<IccmEventEntity> iccmEventEntities = filteredApplications.stream()
                .map(a -> setIccmEventForEmail(a))
                .collect(Collectors.toList());

        List<IccmEventEntity> iccmEventEntitiesSms = filteredApplications.stream()
                .map(a -> setIccmEventForSms(a))
                .collect(Collectors.toList());

        iccmEventEntities.addAll(iccmEventEntitiesSms);

        for (IccmEventEntity iccmEventEntity : iccmEventEntities) {
            String arn = iccmEventEntity.getCcApplicationEntityByArn().getArn();
            try {
				Thread.sleep(10L);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				e.printStackTrace();
			}
            int random = (int) (Math.random()*1000);
            iccmEventEntity.setTrackingField(arn + (Instant.now().toEpochMilli() + random));
            iccmEventEntity.setStatus(IccmCommunicationStatus.NEW);
            iccmEventEntity.setRetryCount(0L);
            iccmEventEntity.setSendWhen(LocalDateTime.now().plus(random, ChronoUnit.MILLIS));

            List<TemplateFieldsEntity> templateFieldsEntities =
                    templateFieldsRepository.findByTemplateMasterEntityKey(iccmEventEntity.getEventMasterEntity().getTemplateMasterEntity().getKey());

            Map<String, String> templateFieldsValueMap = getTemplateFieldsMap(templateFieldsEntities);

            // fetch field values
            templateFieldsValueMap = fetchFieldsValue(iccmEventEntity.getCcApplicationEntityByArn(), templateFieldsValueMap);

            // populate Iccm_Event
            List<IccmEventDataEntity> iccmEventDataEntities = populateIccmEventData(templateFieldsEntities, templateFieldsValueMap);

            iccmEventDataRepository.save(iccmEventDataEntities);
            iccmEventEntity.setEventData(iccmEventDataEntities);
            iccmEventRepository.saveAndFlush(iccmEventEntity);
        }*/

        //	TODO :use the procedure to save iccm event and iccm event data. 
        return null;
    }
    
    public List<NotificationRequest> getFollowUpApplications() {

        List<IccmEventEntity> iccmEventEntities = iccmEventRepository.findAll();
        iccmEventEntities = iccmEventEntities.stream()
                .filter(a -> ((a.getStatus() == IccmCommunicationStatus.NEW || a.getStatus() == IccmCommunicationStatus.FAIL)
                        && a.getRetryCount() <= 2
                        && (Duration.between(a.getSendWhen(), LocalDateTime.now()).getSeconds() >= 0)))
                .collect(Collectors.toList());

        List<NotificationRequest> notificationRequests = new ArrayList<>();
        for (IccmEventEntity iccmEventEntity : iccmEventEntities) {

            NotificationRequest notificationRequest = new NotificationRequest();
            List<IccmEventDataEntity> iccmEventDataEntities = iccmEventEntity.getEventData();

            // below fields are required in case of email only
            if (IccmEvent.CANCELLATIONEMAIL.getValue().equalsIgnoreCase(iccmEventEntity.getEventMasterEntity().getEventKey())
                    || IccmEvent.FOLLOWUPEMAIL.getValue().equalsIgnoreCase(iccmEventEntity.getEventMasterEntity().getEventKey())
                    || (IccmEvent.SENDTOCUSTOMEREMAIL.getValue().equalsIgnoreCase(iccmEventEntity.getEventMasterEntity().getEventKey())
                    		&& ChannelType.S.getChannel().equalsIgnoreCase(iccmEventEntity.getCcApplicationEntityByArn().getChannelMasterById().getName()))) {
                notificationRequest.setEmail(getIccmEventDataValues(iccmEventDataEntities, EMAIL));
                notificationRequest.setSalutation(getIccmEventDataValues(iccmEventDataEntities, SALUTATION));
                notificationRequest.setFirstName(getIccmEventDataValues(iccmEventDataEntities, FIRSTNAME));
                notificationRequest.setMiddleName(getIccmEventDataValues(iccmEventDataEntities, MIDDLENAME));
                notificationRequest.setLastName(getIccmEventDataValues(iccmEventDataEntities, LASTNAME));
                notificationRequest.setProductCard1(getIccmEventDataValues(iccmEventDataEntities, PRODUCTCARD1));
                notificationRequest.setProductCard2(getIccmEventDataValues(iccmEventDataEntities, PRODUCTCARD2));
            }
            notificationRequest.setArn(iccmEventEntity.getCcApplicationEntityByArn().getArn());
            notificationRequest.setIdValue(getIccmEventDataValues(iccmEventDataEntities, IDVALUE));
            notificationRequest.setMobile(getIccmEventDataValues(iccmEventDataEntities, MOBILE));
            notificationRequest.setDialCodeMobile(getIccmEventDataValues(iccmEventDataEntities, DIALCODEMOBILE));
            notificationRequest.setTrackingField(iccmEventEntity.getTrackingField());
            notificationRequest.setIccmTemplate(iccmEventEntity.getEventMasterEntity().getTemplateMasterEntity().getKey());
            notificationRequest.setRetryAttempts(iccmEventEntity.getRetryCount());

            IccmEvent iccmEvent = Arrays.stream(IccmEvent.values())
                    .filter(a -> iccmEventEntity.getEventMasterEntity().getEventKey().equalsIgnoreCase(a.getValue()))
                    .findFirst()
                    .get();


            notificationRequest.setEvent(iccmEvent);
            notificationRequests.add(notificationRequest);
        }
        return notificationRequests;
    }
    
    public IccmEventEntity updateSuccessIccm(NotificationRequest notificationRequest) {
    	try {
    		IccmEventEntity iccmEventEntity = iccmEventRepository.findByTrackingField(notificationRequest.getTrackingField());
            iccmEventEntity.setStatus(IccmCommunicationStatus.SEND);
            return iccmEventRepository.save(iccmEventEntity);
		} catch (Exception e) {
			log.error("ICCM EMAIL/SMS service error for Same Tracking Fields in DB {}.", notificationRequest);
		}
        return null;
    }

    public IccmEventEntity updateFailedIccm(NotificationRequest notificationRequest) {
    	try {
    		IccmEventEntity iccmEventEntity = iccmEventRepository.findByTrackingField(notificationRequest.getTrackingField());
            iccmEventEntity.setStatus(IccmCommunicationStatus.FAIL);
            iccmEventEntity.setRetryCount(notificationRequest.getRetryAttempts() + 1);
            return iccmEventRepository.save(iccmEventEntity);
		} catch (Exception e) {
			log.error("ICCM EMAIL/SMS service error for Same Tracking Fields in DB {}.", notificationRequest);
		}
        return null;
    }

    private NotificationRequest populateNotificationRequest(Map<String, String> templateFieldsValueMap, NotificationRequest notificationRequest) {
        if (templateFieldsValueMap.containsKey(IDVALUE)) {
            notificationRequest.setIdValue(templateFieldsValueMap.get(IDVALUE));
        }
        if (templateFieldsValueMap.containsKey(MOBILE)) {
            notificationRequest.setMobile(templateFieldsValueMap.get(MOBILE));
        }
        if (templateFieldsValueMap.containsKey(DIALCODEMOBILE)) {
            notificationRequest.setDialCodeMobile(templateFieldsValueMap.get(DIALCODEMOBILE));
        }
        if (templateFieldsValueMap.containsKey(EMAIL)) {
            notificationRequest.setEmail(templateFieldsValueMap.get(EMAIL));
        }
        if (templateFieldsValueMap.containsKey(SALUTATION)) {
            notificationRequest.setSalutation(templateFieldsValueMap.get(SALUTATION));
        }
        if (templateFieldsValueMap.containsKey(FIRSTNAME)) {
            notificationRequest.setFirstName(templateFieldsValueMap.get(FIRSTNAME));
        }
        if (templateFieldsValueMap.containsKey(MIDDLENAME)) {
            notificationRequest.setMiddleName(templateFieldsValueMap.get(MIDDLENAME));
        }
        if (templateFieldsValueMap.containsKey(LASTNAME)) {
            notificationRequest.setLastName(templateFieldsValueMap.get(LASTNAME));
        }
        if (templateFieldsValueMap.containsKey(FAREMARKLINE1)) {
            notificationRequest.setFaremarkLine1(templateFieldsValueMap.get(FAREMARKLINE1));
        }
        if (templateFieldsValueMap.containsKey(FAREMARKLINE2)) {
            notificationRequest.setFaremarkLine2(templateFieldsValueMap.get(FAREMARKLINE2));
        }
        if (templateFieldsValueMap.containsKey(FAREMARKLINE3)) {
            notificationRequest.setFaremarkLine3(templateFieldsValueMap.get(FAREMARKLINE3));
        }
        if (templateFieldsValueMap.containsKey(FAREMARKLINE4)) {
            notificationRequest.setFaremarkLine4(templateFieldsValueMap.get(FAREMARKLINE4));
        }
        if (templateFieldsValueMap.containsKey(FAREMARKLINE5)) {
            notificationRequest.setFaremarkLine5(templateFieldsValueMap.get(FAREMARKLINE5));
        }
        if (templateFieldsValueMap.containsKey(PRODUCTCARD1)) {
            notificationRequest.setProductCard1(templateFieldsValueMap.get(PRODUCTCARD1));
        }
        if (templateFieldsValueMap.containsKey(PRODUCTCARD2)) {
            notificationRequest.setProductCard2(templateFieldsValueMap.get(PRODUCTCARD2));
        }
        return notificationRequest;
    }

    private List<IccmEventDataEntity> populateIccmEventData(List<TemplateFieldsEntity> templateFieldsEntities, Map<String, String> templateFieldsValueMap) {
        List<IccmEventDataEntity> iccmEventDataEntities = new ArrayList<>();
        for (TemplateFieldsEntity templateFieldsEntity : templateFieldsEntities) {
            IccmEventDataEntity iccmEventDataEntity = new IccmEventDataEntity();
            iccmEventDataEntity.setTemplateFieldsEntity(templateFieldsEntity);
            iccmEventDataEntity.setValue(templateFieldsValueMap.get(templateFieldsEntity.getFieldMasterEntity().getFieldName()));
            iccmEventDataEntities.add(iccmEventDataEntity);
        }
        return iccmEventDataEntities;
    }

    private Map<String, String> fetchFieldsValue(CCApplicationEntity application, Map<String, String> templateFieldsValueMap) {

        ICCMDataFacade iccmDataFacade = new ICCMDataFacade(application);
        templateFieldsValueMap.entrySet().stream()
                .filter(x -> StringUtils.isEmpty(x.getValue()))
                .filter(x -> ICCMDataFacade.FIELD_METHOD_MAPPING.containsKey(x.getKey()))
                .forEach(k -> {
                    final String methodName = ICCMDataFacade.FIELD_METHOD_MAPPING.get(k.getKey());
                    try {
                        Method method = ICCMDataFacade.class.getMethod(methodName);
                        templateFieldsValueMap.put(k.getKey(), Objects.toString(method.invoke(iccmDataFacade)));
                    } catch (Exception e) {
                        log.error("Error: while fetching value for {}.", k);
                        e.printStackTrace();
                    }
                });
        return templateFieldsValueMap;
    }

    private Map<String, String> populateRemarks(List<String> remarks, Map<String, String> templateFieldsValueMap) {
        for (int i = 0; i < remarks.size(); i++) {
            templateFieldsValueMap.put(FAREMARK + String.valueOf(i + 1), remarks.get(i));
        }
        return templateFieldsValueMap;
    }

    private IccmEventEntity setIccmEventForEmail(CCApplicationEntity ccApplicationEntity) {
    	final Long cancellationFollowUp = iCCMConfigProperties.getIccmSchedularConfig().getCancellationFollowUp();
        IccmEventEntity iccmEventEntity = new IccmEventEntity();
        if (getApplicationAge(ccApplicationEntity).getSeconds() >= SECONDS_IN_A_DAY * iCCMConfigProperties.getIccmSchedularConfig().getCancellationFollowUp()) {
            iccmEventEntity.setEventMasterEntity(eventMasterRepository.findByEventKey(IccmEvent.CANCELLATIONEMAIL.getValue()));
        } else {
            iccmEventEntity.setEventMasterEntity(eventMasterRepository.findByEventKey(IccmEvent.FOLLOWUPEMAIL.getValue()));
        }
        iccmEventEntity.setCcApplicationEntityByArn(ccApplicationEntity);
        return iccmEventEntity;
    }

    private IccmEventEntity setIccmEventForSms(CCApplicationEntity ccApplicationEntity) {
    	final Long cancellationFollowUp = iCCMConfigProperties.getIccmSchedularConfig().getCancellationFollowUp();
        IccmEventEntity iccmEventEntity = new IccmEventEntity();
        if (getApplicationAge(ccApplicationEntity).getSeconds() >= SECONDS_IN_A_DAY * iCCMConfigProperties.getIccmSchedularConfig().getCancellationFollowUp()) {
            iccmEventEntity.setEventMasterEntity(eventMasterRepository.findByEventKey(IccmEvent.CANCELLATIONSMS.getValue()));
        } else {
            iccmEventEntity.setEventMasterEntity(eventMasterRepository.findByEventKey(IccmEvent.FOLLOWUPSMS.getValue()));
        }
        iccmEventEntity.setCcApplicationEntityByArn(ccApplicationEntity);
        return iccmEventEntity;
    }

    private ApplicationStatusTimelineEntity getLatestStatus(CCApplicationEntity ccApplicationEntity) {
        List<ApplicationStatusTimelineEntity> applicationStatusTimelineEntities = ccApplicationEntity.getCcapplicationsStatusById();
        applicationStatusTimelineEntities.sort(Comparator.comparing(ApplicationStatusTimelineEntity::getCreatedOn).reversed());
        return applicationStatusTimelineEntities.get(0);
    }

    private Duration getApplicationAge(CCApplicationEntity ccApplicationEntity) {
        return Duration.between(getLatestStatus(ccApplicationEntity).getCreatedOn(), LocalDateTime.now());
    }

    

    private String getIccmEventDataValues(List<IccmEventDataEntity> iccmEventDataEntities, String field) {
        if (!iccmEventDataEntities.contains(null))
            iccmEventDataEntities = iccmEventDataEntities.stream().filter(a -> a.getTemplateFieldsEntity().getFieldMasterEntity().getFieldName().equalsIgnoreCase(field))
                    .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(iccmEventDataEntities)) {
            return null;
        }
        return iccmEventDataEntities.get(0).getValue();
    }

    


}
