package hsbc.aofe.service.facade;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import hsbc.aofe.domain.IpAccessDetails;
import hsbc.aofe.service.entities.IpAccessDetailsEntity;
import hsbc.aofe.service.jparepository.IpAccessDetailsRepository;
import hsbc.aofe.service.transformer.Transformation;

public class IpAccessDaoManagerImpl implements IpAccessDaoManager {
	
	IpAccessDetailsRepository ipAccessDetailsRepository;
	
	@Autowired
	public IpAccessDaoManagerImpl(IpAccessDetailsRepository ipAccessDetailsRepository){
		
		this.ipAccessDetailsRepository = ipAccessDetailsRepository;
	}

	@Override
	public void saveIpAddressDetails(String ipAddress, Timestamp accessTime, String url) {
		
		IpAccessDetailsEntity ipAccessDetailsEntity = Transformation.populateIpAccessDetailsEntity(ipAddress,accessTime, url);

		ipAccessDetailsRepository.save(ipAccessDetailsEntity);
		
	}
	
	
	public List<IpAccessDetails> getIpAddressDetails(String ipAddress) {
		
		
	List<IpAccessDetailsEntity> ipAccessDetailsEntity = ipAccessDetailsRepository.findByIpAddress(ipAddress);
	
	List<IpAccessDetails> ipAccessDetails =  Transformation.transformIpAccessDetailsEntityTOIpAccessDetails(ipAccessDetailsEntity);
	
	return ipAccessDetails;
				
	}

}
