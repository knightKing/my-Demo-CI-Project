package hsbc.aofe.service.facade;

import hsbc.aofe.service.entities.NGStatusMasterEntity;
import hsbc.aofe.service.jparepository.NGStatusMasterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*****************************************
 * Created by sahil.verma on 7/1/2017.
 *****************************************
 */
@Component
public class NGStatusRepoFacade {

    private NGStatusMasterRepository ngStatusMasterRepository;

    @Autowired
    public NGStatusRepoFacade(NGStatusMasterRepository ngStatusMasterRepository) {
        this.ngStatusMasterRepository = ngStatusMasterRepository;
    }

    public NGStatusMasterEntity getNGStatusEntityByStatus(String Status) {
        return ngStatusMasterRepository.findByKey(Status);
    }

}
