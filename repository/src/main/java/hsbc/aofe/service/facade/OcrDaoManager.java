package hsbc.aofe.service.facade;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import hsbc.aofe.domain.Document;
import hsbc.aofe.domain.VerifiedIncomeCalcParameters;


public interface OcrDaoManager {

	public VerifiedIncomeCalcParameters getOcrdata(String arn);
}
