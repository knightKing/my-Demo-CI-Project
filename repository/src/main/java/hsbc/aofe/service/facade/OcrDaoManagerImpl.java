package hsbc.aofe.service.facade;

import hsbc.aofe.domain.VerifiedIncomeCalcParameters;
import hsbc.aofe.service.entities.ApplicantDetailsEntity;
import hsbc.aofe.service.entities.ApplicantDocumentEntity;
import hsbc.aofe.service.entities.DocumentExtractionEntity;
import hsbc.aofe.service.jparepository.ApplicantDetailsRepository;
import hsbc.aofe.util.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
@Slf4j
@Component
public class OcrDaoManagerImpl implements OcrDaoManager {

	private ApplicantDetailsRepository applicantDetailsRepository;

	@Autowired
	public OcrDaoManagerImpl(ApplicantDetailsRepository applicantDetailsRepository) {
		this.applicantDetailsRepository = applicantDetailsRepository;
	}

	@Override
	public VerifiedIncomeCalcParameters getOcrdata(String arn) {
		LocalDate dateOfIssue = LocalDate.now();
		ApplicantDetailsEntity primaryApplicant = applicantDetailsRepository.getPrimaryApplicant(arn);
		final List<ApplicantDocumentEntity> applicantDocumentsById = primaryApplicant.getApplicantDocumentsById();
		for (ApplicantDocumentEntity doc : applicantDocumentsById) {
			final String documentName = doc.getDocumentsMasterByDocumentType().getKey();
			log.debug("The documentType when VerifiedIncomeCalcParameters are extrtacted is" + documentName);
			if (documentName.equalsIgnoreCase("NRIC")) {
				final List<DocumentExtractionEntity> documentExtractionEntity = doc.getDocumentExtractionEntity();
				if (!documentExtractionEntity.isEmpty()) {
					log.debug("The size of documentExtractionEntity fetched from databse is"
							+ documentExtractionEntity.size());
					for (DocumentExtractionEntity extraction : documentExtractionEntity) {
						if ("doi".equalsIgnoreCase(extraction.getFieldName())) {
							final String extractedDate = extraction.getExtractedValue();
							log.debug("The nric issue date extracted from documentExtraction is " + extractedDate);
							dateOfIssue = CommonUtils.convertStringWithDiffFormatToLocalDate(extractedDate);
							log.debug(
									"The nric issue date extracted from documentExtraction after commonUtils is used is "
											+ dateOfIssue);
						}
					}
				}
			}
		}
		VerifiedIncomeCalcParameters incomeCalcParameters = setVerifiedIncomeCalculation(primaryApplicant, dateOfIssue);
		return incomeCalcParameters;
	}

	private VerifiedIncomeCalcParameters setVerifiedIncomeCalculation(ApplicantDetailsEntity primaryApplicant,
																	  LocalDate dateOfIssue) {
		VerifiedIncomeCalcParameters incomeCalcParameters = new VerifiedIncomeCalcParameters();
		incomeCalcParameters.setDateOfBirth(primaryApplicant.getDateOfBirth());
		incomeCalcParameters.setNationality(primaryApplicant.getNationality());
		incomeCalcParameters.setIdValue(primaryApplicant.getIdValue());
		incomeCalcParameters
				.setEmploymenttype(primaryApplicant.getAppAddInfo().getJobtypemasterByEmploymenttype().getName());
		incomeCalcParameters.setDateOfIssue(dateOfIssue);
		return incomeCalcParameters;
	}

}
