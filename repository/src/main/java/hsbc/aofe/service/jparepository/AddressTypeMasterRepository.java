package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.AddressTypeMasterEntity;

@Repository
public interface AddressTypeMasterRepository extends JpaRepository<AddressTypeMasterEntity, Long> {

    AddressTypeMasterEntity findByType(String type);

}
