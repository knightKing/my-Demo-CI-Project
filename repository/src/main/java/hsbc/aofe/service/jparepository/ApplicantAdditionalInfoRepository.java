package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.ApplicantAdditionalInfoEntity;

@Repository
public interface ApplicantAdditionalInfoRepository extends JpaRepository<ApplicantAdditionalInfoEntity, Long> {

}
