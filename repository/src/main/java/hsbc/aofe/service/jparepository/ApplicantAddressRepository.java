package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.ApplicantAddressEntity;
import hsbc.aofe.service.entities.ApplicantAddressPK;

@Repository
public interface ApplicantAddressRepository extends JpaRepository<ApplicantAddressEntity, ApplicantAddressPK> {

	@Query(value="select * from APPLICANT_ADDRESS where APPLICANTID = :id", nativeQuery=true)
    public List<ApplicantAddressEntity> getApplicantAddresses(@Param("id") Long id);
	
}
