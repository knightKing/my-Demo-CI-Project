package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.ApplicantDetailsEntity;

@Repository
public interface ApplicantDetailsRepository extends JpaRepository<ApplicantDetailsEntity, Long>{

	public List<ApplicantDetailsEntity> findByDocumentsMasterByIdDocTypeKeyAndIdValueAndDialCodeMobileAndMobileAndEmail(String key, String idValue,
														String dialCodeMobile, Long mobile, String email);
	
	public List<ApplicantDetailsEntity> findByDocumentsMasterByIdDocTypeKeyAndIdValue(String key, String idValue);

	@Query(value="select * from APPLICANTDETAILS where ID = :id", nativeQuery=true)
	public ApplicantDetailsEntity getSuppApplicant(@Param("id") Long id);

	
	@Query(value="select app.* from APPLICANTDETAILS app INNER JOIN CCAPPLICATION cc ON app.ID = cc.APPLICANTID "
			+ "where cc.ARN=:arn " , nativeQuery=true)
	public ApplicantDetailsEntity getPrimaryApplicant(@Param("arn") String arn);
	
/*	@Query(value="SELECT DATEOFBIRTH,NATIONALITY,IDVALUE,aadi.EMPLOYMENTTYPE FROM APPLICANTDETAILS aad INNER JOIN "
			+ " APPLICANTADDITIONALINFO aadi ON aadi.APPLICANTID=aad.ID "
			+ " INNER JOIN  CCAPPLICATION cc ON cc.APPLICANTID = aad.ID "
			+ " where cc.arn= :arn ",nativeQuery=true)
	public VerifiedIncomeCalcParameters getVerifiedIncomeParameters(@Param("arn") String arn);*/
}
 