package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.ApplicantDocumentEntity;

@Repository
public interface ApplicationDocumentRepository extends JpaRepository<ApplicantDocumentEntity, Long> {

	@Query(value = "select ad.* from APPLICANT_DOCUMENT ad INNER JOIN CCAPPLICATION cc ON cc.APPLICANTID = ad.APPLICANTID "
			+ "INNER JOIN SUPPAPPLICANTS sa ON sa.APPLICANTID = ad.APPLICANTID INNER JOIN CCAPPLICATION cc "
			+ "ON cc.APPLICANTID=ad.APPLICANTID INNER JOIN DOCUMENTSMASTER dm ON dm.ID= ad.DOCUMENTTYPE INNER JOIN DOCGROUPMASTER dgm "
			+ "ON dgm.ID = dm.DOCGROUP " + "WHERE cc.ARN=:arn AND cc.APPLICANTID "
			+ "=( select (MIN(APPLICANTID)-1) min from CCAPPLICATION where ARN=:arn ) + :id "
			+ "AND UPPER(dgm.NAME)=UPPER(:docGroup) ", nativeQuery = true)
	public List<ApplicantDocumentEntity> fetchSuppDocForAnSuppId(@Param("arn") String arn,
			@Param("id") long supplementaryId, @Param("docGroup") String docGroup);

}
