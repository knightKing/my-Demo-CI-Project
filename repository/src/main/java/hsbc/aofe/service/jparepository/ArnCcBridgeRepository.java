package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.ARNCcBridgeEntity;
import hsbc.aofe.service.entities.ARNCcBridgePK;

@Repository
public interface ArnCcBridgeRepository extends JpaRepository<ARNCcBridgeEntity, ARNCcBridgePK> {

	@Query(value = "select * from ARNCCBRIDGE where APPLICANTID = :id", nativeQuery = true)
	List<ARNCcBridgeEntity> getSuppApplicant(@Param("id") Long id);

}
