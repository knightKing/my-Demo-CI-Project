package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.CCApplicationAdditionalInfoEntity;

@Repository
public interface CCApplicationAdditionalInfoRepository extends JpaRepository<CCApplicationAdditionalInfoEntity, Long> {

}
