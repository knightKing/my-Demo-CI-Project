package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.CCApplicationEntity;

@Repository
public interface CCApplicationRepository extends JpaRepository<CCApplicationEntity, Long> {

	/**
	 * @param ngStatus
	 *            <p>
	 *            This can take values specified in CCAPPLICATION constrainst
	 *            </p>
	 * @return {@link List } of arns which are present in CCAPPLICATION with
	 *         status {@param ngStatus} and not presentin IUPLOAD
	 */
	@Query(value = "SELECT ARN FROM (SELECT NG.KEY AS NGSTATUS, CC_APP.ARN FROM CCAPPLICATION CC_APP LEFT JOIN (SELECT ASL.* FROM (SELECT APPLICATIONID, MAX(CREATEDON) MAX_CREATED_ON \n"
			+ "FROM APPLICATIONSTATUSTIMELINE GROUP BY APPLICATIONID) R LEFT JOIN APPLICATIONSTATUSTIMELINE ASL ON R.APPLICATIONID = ASL.APPLICATIONID AND R.MAX_CREATED_ON = ASL.CREATEDON \n"
			+ ") APP_STATUS ON CC_APP.ID = APP_STATUS.APPLICATIONID LEFT JOIN NGSTATUSMASTER NG ON APP_STATUS.NGSTATUS=NG.ID WHERE CC_APP.ARN     IS NOT NULL GROUP BY NG.KEY, CC_APP.ARN\n"
			+ ") TEMP WHERE NGSTATUS = :ngStatus MINUS\n" + "SELECT iu.arn FROM IUPLOAD iu", nativeQuery = true)
	List<String> findARNByNGStatusExcludingIUploadARN(@Param("ngStatus") String ngStatus);

	/**
	 * @param arn
	 * @return {@link CCApplicationEntity}
	 */
	CCApplicationEntity findByArn(String arn);

	@Query(value = "SELECT cc.id FROM CCAPPLICATION cc WHERE cc.arn = :ARN", nativeQuery = true)
	Long findIdByArn(@Param("ARN") String arn);

	@Query(value = "select cc.* from ccapplication cc where cc.arn= :arn ", nativeQuery = true)
	CCApplicationEntity getApplicationViaArn(@Param("arn") String arn);
	
	@Query(value = "select nsm.key as key from ("
			+ "select tl.ngstatus as status from ccapplication ccapp "
			+ "join applicantdetails appd on ccapp.applicantid = appd.id "
			+ "join applicationstatustimeline tl on tl.APPLICATIONID = ccapp.id "
			+ "where appd.iddoctype = :idType and appd.idvalue = :idValue order by tl.CREATEDON desc ) t join ngstatusmaster nsm on nsm.id = t.status where rownum = 1", nativeQuery = true)
	String getApplicationStatus(@Param("idType") int idType, @Param("idValue") String idValue);
	
}
