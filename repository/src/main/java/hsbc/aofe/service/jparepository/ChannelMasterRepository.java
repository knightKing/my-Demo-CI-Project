package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.ChannelMasterEntity;

@Repository
public interface ChannelMasterRepository extends JpaRepository<ChannelMasterEntity, Long> {

	public ChannelMasterEntity findByName(String name);
	
}
