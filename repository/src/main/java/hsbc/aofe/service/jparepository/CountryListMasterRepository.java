package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.CountryListMasterEntity;

@Repository
public interface CountryListMasterRepository extends JpaRepository<CountryListMasterEntity, Long> {

	public CountryListMasterEntity findByName(String name);
	
	public CountryListMasterEntity findByCode(String code);
	
}
