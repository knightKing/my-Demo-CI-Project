package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.CCApplicationEntity;
import hsbc.aofe.service.entities.CreditCardMasterEntity;

@Repository
public interface CreditCardMasterRepository extends JpaRepository<CreditCardMasterEntity, Long> {

    /**
     * @param aoStatus <p>
     *                 This can take values specified in CCAPPLICATION constrainst
     *                 </p>
     * @return {@link List } of arns which are present in CCAPPLICATION with
     * status {@param aoStatus} and not presentin IUPLOAD
     */
    @Query(value = "SELECT cc.arn\n" + "FROM CCAPPLICATION cc WHERE cc.ao_status = :aoStatus\n" + "MINUS\n"
            + "SELECT iu.arn FROM IUPLOAD iu", nativeQuery = true)
    List<String> findARNByAOStatusExcludingIUploadARN(@Param("aoStatus") String aoStatus);

    CreditCardMasterEntity findByKey(String key);
    
    List<CreditCardMasterEntity> findByEligibleForCustomer(String eligibleForCustomer);

}
