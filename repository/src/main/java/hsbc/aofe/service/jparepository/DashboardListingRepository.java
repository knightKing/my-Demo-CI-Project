package hsbc.aofe.service.jparepository;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hsbc.aofe.domain.Dashboard;


@Component
public class DashboardListingRepository {


    SessionFactory sessionFactory;

    @Autowired
    public DashboardListingRepository(EntityManagerFactory factory) {
        if (factory.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        this.sessionFactory = factory.unwrap(SessionFactory.class);
    }

    public List<Dashboard> getApplications(String sort, String filteredBy, String uid, int start, int end) {
        Session session = sessionFactory.openSession();

        String uidCondition = "";

        if (!"default".equals(uid)) {
            uidCondition = " AND sd.STAFFID= '" + uid +"'";
        }

        Query query = null;
        List<Dashboard> objectlist = null;
        try {  	
        	
        	String querystr = "SELECT * from (SELECT list.*, ROW_NUMBER() OVER ( ORDER BY "+ sort +") AS SEQNUM \n " +
        			"           FROM ( SELECT  (dense_rank() OVER ( ORDER BY ARN ) + dense_rank() OVER ( ORDER BY ARN DESC ) - 1) as TOTALCOUNT, \n " +
        			"           CUSTOMERNAME, IDVALUE, CARDNAME, \n " +
        			"           (CASE WHEN 'default' = '" +uid +"' THEN CHANNEL ELSE null  END) AS CHANNEL, \n " +
        			"           (CASE WHEN 'default' = '" +uid +"' THEN STAFFID ELSE null  END) AS STAFFID, \n " +
        			"           APPLICATIONCREATEDON, NGSTATUS, AOREFNO, AOSTATUS, AOSUBMISSINDATE, \n " +
        			"           (CASE  WHEN 'default' = '" +uid +"' THEN FFAGENTNAME  ELSE null  END) AS FFAGENTNAME, \n " +
        			"           ARN, EMAIL, MOBILE, DIALCODEMOBILE FROM ( SELECT CONCAT(APP_DETAILS.FIRSTNAME, CONCAT(' ', APP_DETAILS.LASTNAME)) AS CUSTOMERNAME, \n " +
        			"           APP_DETAILS.IDVALUE, APP_DETAILS.EMAIL, APP_DETAILS.MOBILE, APP_DETAILS.DIALCODEMOBILE, LISTAGG(CC_M.KEY, ',') WITHIN  GROUP (ORDER BY CC_M.KEY) AS CARDNAME , \n " +
        			"           CM.NAME AS CHANNEL, SD.STAFFID AS STAFFID, CC_APP.CREATEDON AS APPLICATIONCREATEDON , \n " +
        			"           NG.KEY AS NGSTATUS, CC_APP.AOREFNUM AS AOREFNO , SGHAO.PROD_STAT_CDE AS AOSTATUS, \n " +
        			"           CC_APP.AOSUBMISSIONDATE AS AOSUBMISSINDATE, \n " +
        			"           CC_APP.FULFILMENTAGENT AS FFAGENTNAME, CC_APP.ARN FROM APPLICANTDETAILS APP_DETAILS \n " + 
        			"           LEFT JOIN  CCAPPLICATION CC_APP ON APP_DETAILS.ID= CC_APP.APPLICANTID \n " +                      
        			"           LEFT JOIN  STAFFDETAILS SD ON CC_APP.ID = SD.APPLICATIONID \n " + 
        			"           LEFT JOIN  CHANNELMASTER CM   ON CC_APP.INITIATEDBY = CM.ID \n " +                    
        			"           LEFT JOIN  ARNCCBRIDGE ARN  ON APP_DETAILS.ID = ARN.APPLICANTID \n " +   
        			" 			LEFT JOIN  SGHAO_APPL SGHAO ON REGEXP_SUBSTR(CC_APP.AOREFNUM, '[0-9]*$', 1) = SGHAO.ARR_ID_APP  \n " +
        			"           LEFT JOIN  CREDITCARDMASTER CC_M  ON ARN.CCID = CC_M.ID  \n " +                          
        			"           LEFT JOIN (SELECT ASL.* FROM ( SELECT APPLICATIONID, MAX(CREATEDON) MAX_CREATED_ON \n " +       
        			"           FROM APPLICATIONSTATUSTIMELINE   GROUP BY APPLICATIONID)R   \n " +       
        			"           INNER JOIN APPLICATIONSTATUSTIMELINE ASL  ON R.APPLICATIONID = ASL.APPLICATIONID \n " +                            
        			"           AND R.MAX_CREATED_ON = ASL.CREATEDON )  APP_STATUS  ON CC_APP.ID = APP_STATUS.APPLICATIONID \n " +                         
        			"           LEFT JOIN NGSTATUSMASTER NG  ON APP_STATUS.NGSTATUS=NG.ID \n " +                        
        			"           WHERE CC_APP.ARN IS NOT NULL "  + uidCondition + " " + filteredBy + "\n" +
        			"           GROUP BY APP_DETAILS.FIRSTNAME, APP_DETAILS.LASTNAME, APP_DETAILS.IDVALUE, CM.NAME, \n " + 
        			"           SD.STAFFID, CC_APP.CREATEDON, NG.KEY, CC_APP.AOREFNUM, SGHAO.PROD_STAT_CDE, CC_APP.AOSUBMISSIONDATE,\n " + 
        			"           CC_APP.FULFILMENTAGENT, CC_APP.ARN, APP_DETAILS.EMAIL, APP_DETAILS.MOBILE, APP_DETAILS.DIALCODEMOBILE ) )list) WHERE SEQNUM >" + start + " AND SEQNUM<=" + end ;
         
            query = session.createSQLQuery(querystr)
                    .addScalar("totalCount")
            		.addScalar("customerName")
                    .addScalar("idValue")
                    .addScalar("cardName")
                    .addScalar("channel")
                    .addScalar("staffId")
                    .addScalar("applicationCreatedOn")
                    .addScalar("ngStatus")
                    .addScalar("aoRefNo")
                    .addScalar("aoStatus")
                    .addScalar("aoSubmissinDate")
                    .addScalar("ffAgentName")
                    .addScalar("arn")
                    .addScalar("email")
                    .addScalar("mobile")
                    .addScalar("dialCodeMobile")
                    .setResultTransformer(Transformers.aliasToBean(Dashboard.class));

            objectlist = query.list();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objectlist;

    }


}
