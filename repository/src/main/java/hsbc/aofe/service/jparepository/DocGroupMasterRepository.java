package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.DocGroupMasterEntity;

@Repository
public interface DocGroupMasterRepository extends JpaRepository<DocGroupMasterEntity, Long> {

}
