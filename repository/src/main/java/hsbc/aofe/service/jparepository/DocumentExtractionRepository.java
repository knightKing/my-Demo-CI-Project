package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.DocumentExtractionEntity;

@Repository
public interface DocumentExtractionRepository extends JpaRepository<DocumentExtractionEntity, Long> {

}
