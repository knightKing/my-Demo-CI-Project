package hsbc.aofe.service.jparepository;

import hsbc.aofe.service.entities.DocumentsMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentsMasterRepository extends JpaRepository<DocumentsMasterEntity, Long> {

	public DocumentsMasterEntity findByKey(String key);

	/**
	 * @param id
	 * @return key for the input id
	 */
	@Query(value = "SELECT dm.key\n" + "FROM DOCUMENTSMASTER dm WHERE dm.id = :id\n", nativeQuery = true)
	String findKeyById(@Param("id") Long id);
	
}
