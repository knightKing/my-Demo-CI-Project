package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.EducationalLevelMasterEntity;

@Repository
public interface EducationalLevelMasterRepository extends JpaRepository<EducationalLevelMasterEntity, Long> {

	public EducationalLevelMasterEntity findByLabel(String label);
	
}
