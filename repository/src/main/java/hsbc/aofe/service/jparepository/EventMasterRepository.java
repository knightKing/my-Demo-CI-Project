package hsbc.aofe.service.jparepository;

import hsbc.aofe.service.entities.EventMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventMasterRepository extends JpaRepository<EventMasterEntity, Long> {

	EventMasterEntity findByEventKey(String eventKey);
	
}
