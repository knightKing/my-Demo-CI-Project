package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.HomeOwnershipMasterEntity;

@Repository
public interface HomeOwnershipMasterRepository extends JpaRepository<HomeOwnershipMasterEntity, Long> {

	public HomeOwnershipMasterEntity findByLabel(String label);
	
}
