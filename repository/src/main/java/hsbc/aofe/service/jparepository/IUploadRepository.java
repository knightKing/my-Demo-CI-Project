package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.IUploadEntity;

@Repository
public interface IUploadRepository extends JpaRepository<IUploadEntity, Long> {

}
