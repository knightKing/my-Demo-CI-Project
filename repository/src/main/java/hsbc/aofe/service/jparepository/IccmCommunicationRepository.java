package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.IccmCommunicationEntity;

@Repository
public interface IccmCommunicationRepository extends JpaRepository<IccmCommunicationEntity, Long> {

}
