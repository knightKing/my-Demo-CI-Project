package hsbc.aofe.service.jparepository;

import hsbc.aofe.service.entities.IccmEventDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IccmEventDataRepository extends JpaRepository<IccmEventDataEntity, Long> {

}
