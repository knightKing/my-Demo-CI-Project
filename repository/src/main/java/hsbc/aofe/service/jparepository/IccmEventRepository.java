package hsbc.aofe.service.jparepository;

import hsbc.aofe.service.entities.IccmEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IccmEventRepository extends JpaRepository<IccmEventEntity, Long> {

	IccmEventEntity findByTrackingField(String trackingField);
	
	@Procedure(name = "GenerateChaserEvents", procedureName = "GenerateChaserEvents" )
	void followUpNamedStoredProcedure(@Param("Chaser1Frequency") Long chaser1Frequency, @Param("Chaser2Frequency") Long chaser2Frequency,
			@Param("Chaser3Frequency") Long chaser3Frequency, @Param("CancellationFrequency") Long cancellationFrequency);
	
}
