package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.ImageEntity;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

	@Query(value="select im.* from IMAGE im INNER JOIN APPLICANT_DOCUMENT ad ON im.DOCUMENTID = ad.ID INNER JOIN APPLICANTDETAILS apd "
			+ "ON apd.ID = ad.APPLICANTID "
			+ "INNER JOIN SUPPAPPLICANTS sp ON sp.APPLICANTID =ad.APPLICANTID INNER JOIN CCAPPLICATION cc ON cc.APPLICANTID=ad.APPLICANTID "
			+ "INNER JOIN DOCUMENTSMASTER dm ON dm.ID= ad.DOCUMENTTYPE INNER JOIN DOCGROUPMASTER dgm ON dgm.ID = dm.DOCGROUP "
			+ "where cc.ARN=:arn "
			+ "AND cc.APPLICANTID "
			+ "=( select (MIN(APPLICANTID)-1) min from CCAPPLICATION where ARN=:arn ) + :suppId "
			+ "AND UPPER(dgm.NAME)= UPPER(:docGroup)" , nativeQuery=true)
	public List<ImageEntity> fetchImagesOfSuppById(@Param("arn") String arn ,@Param("suppId")long suppId,@Param("docGroup")String docGroup);
}
