package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hsbc.aofe.service.entities.IpAccessDetailsEntity;

public interface IpAccessDetailsRepository extends JpaRepository<IpAccessDetailsEntity, Long> {
	
	List<IpAccessDetailsEntity> findByIpAddress(String ipAddress);

}
