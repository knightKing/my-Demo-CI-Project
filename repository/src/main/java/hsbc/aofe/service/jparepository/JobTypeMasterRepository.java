package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.JobTypeMasterEntity;

@Repository
public interface JobTypeMasterRepository extends JpaRepository<JobTypeMasterEntity, Long> {

	public JobTypeMasterEntity findByName(String name);
	
}
