package hsbc.aofe.service.jparepository;

import hsbc.aofe.service.entities.NGStatusMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NGStatusMasterRepository extends JpaRepository<NGStatusMasterEntity, Long> {

    /**
     * @param key is the application status
     * @return
     */
    NGStatusMasterEntity findByKey(String key);

}
