package hsbc.aofe.service.jparepository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.OtpUser;


@Repository
public interface OtpRepository extends JpaRepository<OtpUser, Integer> {
	
	public OtpUser findByArnAndOtp(String arn,String otp );
	
	public OtpUser findByArn(String arn);

}