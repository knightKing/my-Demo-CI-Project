package hsbc.aofe.service.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.RemarksEntity;

@Repository
public interface RemarksRepository extends JpaRepository<RemarksEntity, Long> {

	@Query(value = "SELECT * FROM REMARKS r WHERE r.arn = :arn AND r.type = :type", nativeQuery=true)
	List<RemarksEntity> findRemarksByType(@Param("arn") String arn, @Param("type") String type);

}
