package hsbc.aofe.service.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hsbc.aofe.service.entities.SuppapplicantEntity;
import hsbc.aofe.service.entities.SuppapplicantPK;

@Repository
public interface SuppApplicantRepository extends JpaRepository<SuppapplicantEntity, SuppapplicantPK> {
	
	 /**
     * 
     * @param arn
     */
      
    @Query(value="select CREATEDBY,CREATEDON,LASTMODIFIEDBY,LASTMODIFIEDON,RELATIONWITHPRIMARYAPPLICANT,APPLICANTID,APPLICATIONID from "
    		+ "(select sa.*, row_number() over (order by sa.applicantid) as seqnum from "
    		+ "SUPPAPPLICANTS sa INNER JOIN CCAPPLICATION cc ON cc.ID=sa.APPLICATIONID WHERE cc.ARN=:arn order by sa.applicantid) "
    		+ "where seqnum=:seqnum "
    		,nativeQuery = true)
    public SuppapplicantEntity fetchSupplementaryApplByArn(@Param("arn") String arn ,@Param("seqnum") long suppId);

	@Query(value="select * from SUPPAPPLICANTS where APPLICANTID = :id", nativeQuery=true)
    public SuppapplicantEntity getSuppApplicant(@Param("id") Long id);
	
}
