package hsbc.aofe.service.jparepository;

import hsbc.aofe.service.entities.TemplateFieldsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TemplateFieldsRepository extends JpaRepository<TemplateFieldsEntity, Long> {

	List<TemplateFieldsEntity> findByTemplateMasterEntityKey(String key);
	
}
