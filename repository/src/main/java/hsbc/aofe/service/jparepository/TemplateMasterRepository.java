package hsbc.aofe.service.jparepository;

import hsbc.aofe.service.entities.TemplateMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateMasterRepository extends JpaRepository<TemplateMasterEntity, Long> {

}
