package hsbc.aofe.service.service;

import hsbc.aofe.domain.Applicant;
import hsbc.aofe.repository.common.ApplicantRepositoryService;
import hsbc.aofe.service.facade.ApplicantDaoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicantRepoServiceImpl implements ApplicantRepositoryService {

    private ApplicantDaoManager applicantDaoManager;

    @Autowired
    public ApplicantRepoServiceImpl(ApplicantDaoManager applicantDaoManager) {
        this.applicantDaoManager = applicantDaoManager;
    }

    @Override
    public Applicant findApplicantById(Long id) {
        //return applicantDaoManager.manageFindApplicantById(id);
        return null;
    }

    @Override
    public List<Applicant> findAllApplicants() {
        //return applicantDaoManager.manageFindAllApplicants();
        return null;
    }

    @Override
    public Applicant updateApplicant(String arn, Applicant applicant) {
        return applicantDaoManager.manageUpdateApplicant(arn, applicant);
    }

    @Override
    public void deleteApplicantById(Long id) {
        applicantDaoManager.manageDeleteApplicantById(id);
    }

    @Override
    public List<Applicant> saveApplicant(List<Applicant> applicants, String arn) {
        return applicantDaoManager.manageSaveApplicant(applicants, arn);
    }

    @Override
    public Applicant savePrimaryApplicant(String arn, Applicant applicant) {
        return applicantDaoManager.savePrimaryApplicant(arn, applicant);
    }
    
    @Override
  	public Applicant updateBeneficiary(String arn, Applicant applicant) {
  		return applicantDaoManager.manageUpdateBeneficiary(arn, applicant);
  	}

    @Override
	public void deleteSuppApplicant(String arn, int index) {
		applicantDaoManager.deleteSuppApplicant(arn, index);
	}

	@Override
	public void deleteAllSuppApplicants(String arn) {
		applicantDaoManager.deleteAllSuppApplicants(arn);
	}

	@Override
	public void saveVerifiedIncome(String arn, int verifiedIncome,boolean isMatch) {
		applicantDaoManager.saveVerifiedIncome(arn, verifiedIncome,isMatch);
		
	}

	@Override
	public void updateIncomeFlag(String arn, boolean isMatch) {
		applicantDaoManager.updateIncomeFlag(arn,isMatch);
		
	}

    @Override
    public void updateEmploymentFlag(String arn, boolean isMatch) {
        applicantDaoManager.updateEmploymentFlag(arn,isMatch);
    }

}
