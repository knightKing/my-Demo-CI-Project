package hsbc.aofe.service.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonPatch;
import hsbc.aofe.domain.*;
import hsbc.aofe.exception.*;
import hsbc.aofe.repository.common.ApplicationRepositoryService;
import hsbc.aofe.service.entities.*;
import hsbc.aofe.service.facade.ApplicantDaoManager;
import hsbc.aofe.service.facade.ApplicationDaoManager;
import hsbc.aofe.service.facade.ApplicationStatusTimelineRepoFacade;
import hsbc.aofe.service.facade.NGStatusRepoFacade;
import hsbc.aofe.service.jparepository.*;
import hsbc.aofe.service.transformer.Transformation;
import hsbc.aofe.util.CommonUtils;
import hsbc.aofe.util.GenArn;
import hsbc.aofe.util.UserUtil;
import hsbc.aofe.validationgroups.BranchJourneyGroup;
import hsbc.aofe.validationgroups.CustomerJourneyGroup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@Slf4j
@Service
public class ApplicationRepoServiceImpl implements ApplicationRepositoryService {

	private static final String INPROGRESS = "IN-PROGRESS";
	private static final String PENDINGFORREVIEW = "PENDING-FOR-REVIEW";
	private static final String SUBMITTEDTOAO = "SUBMITTED-TO-AO";
	private static final String PENDINGWITHCUSTOMER = "PENDING-WITH-CUSTOMER";
	
    private ApplicationDaoManager applicationDaoManager;
    private ApplicantDaoManager applicantDaoManager;
    private CCApplicationRepository ccApplicationRepository;
    private ApplicantDetailsRepository applicantDetailsRepository;
    private ImageRepository imageRepository;
    private SuppApplicantRepository suppApplicantRepository;
    private NGStatusRepoFacade ngStatusRepoFacade;
    private ApplicationStatusTimelineRepoFacade statusTimelineRepoFacade;
    private ApplicationDocumentRepository applicationDocRepo;

    @Autowired
    public ApplicationRepoServiceImpl(ApplicationDaoManager applicationDaoManager,
                                      ApplicantDaoManager applicantDaoManager, CCApplicationRepository ccApplicationRepository,
                                      ApplicantDetailsRepository applicantDetailsRepository, ImageRepository imageRepository,
                                      SuppApplicantRepository suppApplicantRepository, NGStatusRepoFacade ngStatusRepoFacade,
                                      ApplicationStatusTimelineRepoFacade statusTimelineRepoFacade,
                                      ApplicationDocumentRepository applicationDocRepo) {
        this.applicationDaoManager = applicationDaoManager;
        this.applicantDaoManager = applicantDaoManager;
        this.ccApplicationRepository = ccApplicationRepository;
        this.applicantDetailsRepository = applicantDetailsRepository;
        this.imageRepository = imageRepository;
        this.suppApplicantRepository = suppApplicantRepository;
        this.ngStatusRepoFacade = ngStatusRepoFacade;
        this.statusTimelineRepoFacade = statusTimelineRepoFacade;
        this.applicationDocRepo = applicationDocRepo;
    }

    @Override
    public Application findApplicationByARN(String arn) {

        // Long applicantId =
        // ccApplicationRepository.findByArn(arn).getApplicantDetailsByApplicantId().getId();
        // return this.findApplicationById(applicantId);
        // return applicantDaoManager.manageFindApplicantById(applicantId);
        return applicationDaoManager.findApplicationByArn(arn);
    }

    @Override
    public List<Application> findAllApplications() {
        return applicationDaoManager.manageFindAllApplications();
    }

    @Override
    public Application createApplication(String uid, String channelAuthority, Application application) {
        isApplicationUnique(application);
        application.setArn(generateArn());
        return applicantDaoManager.createApplicantUsingApplication(uid, channelAuthority, application);
    }

    @Override
    public void deleteApplicationById(String arn) {
        applicationDaoManager.manageDeleteApplicationById(arn);
    }

    private String generateArn() {
        String arn;
        CCApplicationEntity ccApplicationEntity;
        do {
            arn = String.valueOf(GenArn.genArn(6));
            ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        } while (ccApplicationEntity != null);
        return arn;
    }

    private void isApplicationUnique(Application application) {

		Applicant applicant = application.getPrimaryApplicant();
		List<ApplicantDetailsEntity> applicantDetailsEntityList = applicantDaoManager.findApplicantByIdTypeAndIdValue(
				applicant.getIdDocType().getValue(), applicant.getIdValue().toUpperCase());

        if(isApplicantListNullOrEmpty(applicantDetailsEntityList)) {
    		return;
    	}
        
        for(ApplicantDetailsEntity applicantDetailsEntity : applicantDetailsEntityList) {
        	if (applicantDetailsEntity == null) {
                return;
            }
            List<CCApplicationEntity> ccApplicationEntities = applicantDetailsEntity.getCcapplicationsById();
            
            if(ccApplicationEntities != null && !ccApplicationEntities.isEmpty()) {
            	for (int i = 0; i < ccApplicationEntities.size(); i++) {
                    applicationUniquenessRulesApplied(ccApplicationEntities.get(0), applicant);
                }
            }
        }
    }
    
    
    private void applicationUniquenessRulesApplied(CCApplicationEntity ccApplicationEntity, Applicant applicant) {
    	
    	final String UniqueExceptionMessage = "We are Unable to Process Your Application. Please Call 1800 HSBC NOW (4722).";
    	List<ApplicationStatusTimelineEntity> applicationStatusTimelineEntities = ccApplicationEntity.getCcapplicationsStatusById();
    	ApplicantDetailsEntity applicantDetailsEntity = ccApplicationEntity.getApplicantDetailsByApplicantId();
        applicationStatusTimelineEntities.sort(Comparator.comparing(ApplicationStatusTimelineEntity::getCreatedOn).reversed());
        if (INPROGRESS.equalsIgnoreCase(applicationStatusTimelineEntities.get(0).getNgStatusMasterByNgStatus().getKey())) {
            // Case 1
            throw new ApplicationAlreadyInProgressException(UniqueExceptionMessage);
        }
        if (PENDINGFORREVIEW.equalsIgnoreCase(applicationStatusTimelineEntities.get(0).getNgStatusMasterByNgStatus().getKey())) {
            // Case 2
            throw new ApplicationPendingForReviewException(UniqueExceptionMessage);
        }
        if (PENDINGWITHCUSTOMER.equalsIgnoreCase(applicationStatusTimelineEntities.get(0).getNgStatusMasterByNgStatus().getKey())) {
        	
        	if(applicantDetailsEntity.getEmail().equalsIgnoreCase(applicant.getEmail())
        			&& applicantDetailsEntity.getDialCodeMobile().equalsIgnoreCase(applicant.getDialCodeMobile())
        			&& applicantDetailsEntity.getMobile().equals(applicant.getMobile())) {
        		
        		throw new ApplicationPendingWithCustomerWithSameEmailIdvalueAndMobileException("We Notice that You have an Application IN-Progress."
        				+ " To Make any change to Your Application, Please Click Continue.");
        	}
        	throw new ApplicationPendingWithCustomerException(UniqueExceptionMessage);
        }
        if (SUBMITTEDTOAO.equalsIgnoreCase(applicationStatusTimelineEntities.get(0).getNgStatusMasterByNgStatus().getKey())) {
            // Case 3
            List<CreditCard> creditCards = applicant.getCards();
            for (int j = 0; j < creditCards.size(); j++) {
                String key = ccApplicationEntity.getApplicantDetailsByApplicantId().getArnCcBridgesById().get(j).getPrimaryKey().getCreditCardMasterByCcId().getKey();
                if ((creditCards.get(j).getKey().equalsIgnoreCase(key)) 
                		&& (Period.between(ccApplicationEntity.getCreatedOn().toLocalDate(), LocalDate.now()).getDays() <= 93)) {
                    throw new ApplicationSubmittedToAOException(UniqueExceptionMessage);
                }
            }
        }
    }
    
    private boolean isApplicantListNullOrEmpty(List<ApplicantDetailsEntity> applicantDetailsEntityList) {
    	if(applicantDetailsEntityList == null || applicantDetailsEntityList.isEmpty()) {
    		return true;
    	}
    	return false;
    }
    
    /**
	 * This function basically inserts all the documents of a primary applicant
	 * in the DataBase.
	 */
	@Override
	public void createPrimaryApplDoc(String arn, String documentGroup, List<Document> documents) {

		List<ApplicantDocumentEntity> applicantDocumentsEntity = applicantDaoManager.createPrimaryApplDoc(arn,
				documentGroup, documents);
		ApplicantDetailsEntity primaryApplicantDetailsByArn = ccApplicationRepository.findByArn(arn)
				.getApplicantDetailsByApplicantId();
		List<ApplicantDocumentEntity> dataBaseAppDoc = primaryApplicantDetailsByArn.getApplicantDocumentsById();
		if (dataBaseAppDoc.isEmpty()) {
			dataBaseAppDoc.addAll(applicantDocumentsEntity);
		} else {
			updateDocOrCreateNewDoc(applicantDocumentsEntity, dataBaseAppDoc);
		}
		applicationDocRepo.save(dataBaseAppDoc);
	}

	@Override
	public void deletePrimaryApplDoc(String arn, String documentGroup, long index) {
		CCApplicationEntity ccApplicationEntity=ccApplicationRepository.findByArn(arn);
		log.debug("Delete Primary Applicant and set employmentflag as n & verifiedIncome as zero");
		updateEmploymentFlag(false,ccApplicationEntity);
		ApplicantDetailsEntity primaryApplicantDetailsByArn = ccApplicationEntity.getApplicantDetailsByApplicantId();
		List<ImageEntity> updatedImages;
		int count = 0;
		for (ApplicantDocumentEntity iterable_applicant : primaryApplicantDetailsByArn.getApplicantDocumentsById()) {
			String iterableDocGroup = iterable_applicant.getDocumentsMasterByDocumentType()
					.getDocGroupMasterByDocGroup().getName();
			updatedImages = new ArrayList();
			updatedImages.addAll(iterable_applicant.getImageById());
			count = 0;
			for (ImageEntity iterable_image : updatedImages) {
				if (iterableDocGroup.equalsIgnoreCase(documentGroup) && index == iterable_image.getImageIndex()) {
					iterable_applicant.getImageById().remove(iterable_image);
					count++;
				}
			}
			iterable_applicant.setNoOfImages(updatedImages.size() - count);
		}
		applicantDetailsRepository.save(primaryApplicantDetailsByArn);
	}


	private void updateEmploymentFlag(boolean isMatch,CCApplicationEntity ccApplicationEntity) {
		CCApplicationAdditionalInfoEntity ccAppAddInfo = ccApplicationEntity.getCcAppAddInfo();
		ccAppAddInfo.setIncomeAutomatedCalcIndicator(null);
		ccAppAddInfo.setEmploymentCheckIndicator(null);
		ApplicantAdditionalInfoEntity appAddInfo = ccApplicationEntity.getApplicantDetailsByApplicantId().getAppAddInfo();
		appAddInfo.setVerifiedIncome(null);
		ccApplicationRepository.save(ccApplicationEntity);
	}

	/**
	 * This functions deletes an image from a supplementary document.
	 */
	@Override
	public void deleteSupplementaryApplDoc(String arn, long supplementaryId, String documentGroup, long imageIndex) {
		SuppapplicantEntity supplApplByArn = suppApplicantRepository.fetchSupplementaryApplByArn(arn, supplementaryId);
		List<ImageEntity> updatedImages;
		if (supplApplByArn != null) {
			int count = 0;
			for (ApplicantDocumentEntity applicationDocumentEntity : supplApplByArn.getPrimaryKey().getApplicantId()
					.getApplicantDocumentsById()) {
				String iterableDocGroup = applicationDocumentEntity.getDocumentsMasterByDocumentType()
						.getDocGroupMasterByDocGroup().getName();
				updatedImages = new ArrayList();
				updatedImages.addAll(applicationDocumentEntity.getImageById());
				count = 0;
				for (ImageEntity entity : updatedImages) {
					if (iterableDocGroup.equalsIgnoreCase(documentGroup) && entity.getImageIndex() == imageIndex) {
						// imageRepository.delete(entity);
						applicationDocumentEntity.getImageById().remove(entity);
						count++;
					}
				}
				applicationDocumentEntity.setNoOfImages(updatedImages.size() - count);
			}
		}
		suppApplicantRepository.save(supplApplByArn);
	}

	@Override
	public Application updateApplication(String arn, JsonNode patchToApply, BindingResult bindingResult, String channel) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		final CCApplicationEntity ccApplicationCurrentState = ccApplicationRepository.findByArn(arn);
		List<AddressEntity> primaryApplicantAddress = ccApplicationCurrentState.getApplicantDetailsByApplicantId().getAddressesById();
		if(CollectionUtils.isNotEmpty(primaryApplicantAddress))
		{
			List<AddressEntity> sortedPrimaryApplicantAddress = primaryApplicantAddress.stream()
					.sorted(Comparator.comparing(a -> a.getId()))
					.collect(Collectors.toList());
			ccApplicationCurrentState.getApplicantDetailsByApplicantId().setAddressesById(sortedPrimaryApplicantAddress);
		}

		Application applicationCurrentState = applicationDaoManager.convertCCApplicationToApplication(ccApplicationCurrentState);
		JsonNode applicationCurrentState_Node = mapper.valueToTree(applicationCurrentState);
		final JsonNode modifiedApplication_Node = JsonPatch.apply(patchToApply, applicationCurrentState_Node);
		final Application modifiedApplication = mapper.treeToValue(modifiedApplication_Node, Application.class);

		//validation code
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<Object>> violations = null;
		
		if ("CUSTOMER".equals(channel)) {
			violations = validator.validate(modifiedApplication, BranchJourneyGroup.class, CustomerJourneyGroup.class, Default.class );
		} else if ("BRANCH".equals(channel)) {
			violations = validator.validate(modifiedApplication, BranchJourneyGroup.class, Default.class);
		}

		if (CollectionUtils.isNotEmpty(violations)) {
			for (ConstraintViolation<Object> constraintViolation : violations) {
				String objectName = String.valueOf(constraintViolation.getPropertyPath());
				String fieldName = String.valueOf(constraintViolation.getPropertyPath());
				String defaultMessage = constraintViolation.getMessage();
				if (fieldName.contains("address")) {
					String addressArray = fieldName.substring(fieldName.indexOf("address"),
							fieldName.indexOf("address") + 10);
					int index = Integer
							.valueOf(addressArray.substring(addressArray.indexOf("[") + 1, addressArray.indexOf("]")));
					fieldName = modifiedApplication.getPrimaryApplicant().getAdditionalInfo().getAddress().get(index)
							.getAddressType().getValue() + "|" + fieldName;
				}
				if (fieldName.contains("cards")) {
					String cardsArray = fieldName.substring(fieldName.indexOf("cards"), fieldName.indexOf("cards") + 8);
					int index = Integer
							.valueOf(cardsArray.substring(cardsArray.indexOf("[") + 1, cardsArray.indexOf("]")));
					fieldName = modifiedApplication.getPrimaryApplicant().getCards().get(index).getKey() + "|" + fieldName;
				}
				FieldError fieldError = new FieldError(objectName, fieldName, defaultMessage);
				bindingResult.addError(fieldError);
			}
		}
		
		if(bindingResult.hasErrors()){
			throw new InvalidRequestException("Invalid Request Body", bindingResult);
		}
		
		
		// Applicant modification ==>> Applicant, Applicant Additional Infor,
		// Address, ARN CC Brige
		CCApplicationEntity applicationToBeSaved = updateApplicationEntity(arn, ccApplicationCurrentState,
				modifiedApplication);

		// application ==>> Application , Application Additional Info
		CCApplicationEntity save = ccApplicationRepository.saveAndFlush(applicationToBeSaved);

		// Supllementory ==>>
		List<Applicant> suppApplicants = modifiedApplication.getSupplementaryApplicant();
		if (CollectionUtils.isNotEmpty(suppApplicants)) {

			/*List<SuppapplicantEntity> suppApplicantDetailsEntities = ccApplicationCurrentState.getSuppApplicantsById();
			if (suppApplicantDetailsEntities == null) {
				suppApplicantDetailsEntities = new ArrayList<>();
			}

			ApplicantDetailsEntity appDetail;


			
			 * while((suppApplicants.size() -
			 * suppApplicantDetailsEntities.size()) > 0) {
			 * suppApplicantDetailsEntities.add(suppapplicantEntity); }
			 

			for (int i = 0; i < suppApplicants.size(); i++) {

				appDetail = applicantDaoManager.updateApplicantWithAddressAndARNCCBridgeEntry(suppApplicants.get(i),
						getApplicantDetailsFromSuppApplicantDetailsAndApplication(suppApplicantDetailsEntities, i),
						ccApplicationCurrentState);

				SuppapplicantEntity suppAppEntity = Transformation.populateSuppapplicantEntity(
						new SuppapplicantEntity(), new SuppapplicantPK(), suppApplicants.get(i));
				suppAppEntity.getPrimaryKey().setApplicantId(appDetail);
				suppAppEntity.getPrimaryKey().setApplicationId(ccApplicationCurrentState);
				suppApplicantRepository.saveAndFlush(suppAppEntity);

				if (suppApplicantDetailsEntities.size() < (i + 1)) {
					suppApplicantDetailsEntities.add(i, suppAppEntity);
				} else {
					suppApplicantDetailsEntities.set(i, suppAppEntity);
				}
			}

			if ((suppApplicantDetailsEntities.size() - suppApplicants.size()) > 0) {
				for (int i = suppApplicants.size(); i < suppApplicantDetailsEntities.size(); i++) {
					applicantDetailsRepository.delete(
							getApplicantDetailsFromSuppApplicantDetailsAndApplication(suppApplicantDetailsEntities, i));
				}
			}*/
			applicantDaoManager.manageSaveApplicant(suppApplicants, arn);
		}

		return modifiedApplication;
	}

	private ApplicantDetailsEntity getApplicantDetailsFromSuppApplicantDetailsAndApplication(
			List<SuppapplicantEntity> suppapplicantEntities, int index) {
		if (suppapplicantEntities.isEmpty() || (suppapplicantEntities.size() < (index + 1))) {
			return new ApplicantDetailsEntity();
		}
		return suppapplicantEntities.get(index).getPrimaryKey().getApplicantId();
	}

	private CCApplicationEntity updateApplicationEntity(String arn, CCApplicationEntity ccApplicationEntity,
			Application application) {

		Applicant updatedApplicant = applicantDaoManager.manageUpdateApplicant(arn, application.getPrimaryApplicant());
		application.setPrimaryApplicant(updatedApplicant);
		CCApplicationEntity transformedApplicationEntity = Transformation.populateApplicationEntity(ccApplicationEntity,
				application);
		CCApplicationAdditionalInfoEntity ccApplicationAdditionalInfoEntity = Transformation
				.populateApplicationAdditionalInfoEntity(ccApplicationEntity.getCcAppAddInfo(), application);
		transformedApplicationEntity.setCcAppAddInfo(ccApplicationAdditionalInfoEntity);
		return transformedApplicationEntity;
	}

	@Override
	public Long getApplicationIDByARN(String arn) {
		return applicationDaoManager.findApplicationIdByARN(arn);
	}

	@Override
	public void updateApplicationStatus(String arn, NgStatus status) {

		Long applicationID = applicationDaoManager.findApplicationIdByARN(arn);
		NGStatusMasterEntity ngStatusEntity = ngStatusRepoFacade.getNGStatusEntityByStatus(status.getValue());

		CCApplicationEntity applicationEntity = new CCApplicationEntity();
		applicationEntity.setId(applicationID);

		ApplicationStatusTimelineEntity timelineEntity = new ApplicationStatusTimelineEntity();
		timelineEntity.setCreatedOn(LocalDateTime.now());
		timelineEntity.setCreatedBy(UserUtil.getUser());
		timelineEntity.setRemarks(null);
		timelineEntity.setAo_status(null);
		timelineEntity.setNgStatusMasterByNgStatus(ngStatusEntity);
		timelineEntity.setCcApplicationEntity(applicationEntity);
		statusTimelineRepoFacade.saveApplicationStatusTimeline(timelineEntity);
	}

	/**
	 * This function updates the supplementary documents based on a
	 * supplementary id and document Group
	 */
	@Override
	public void updateSupplementaryApplDoc(String arn, long supplementaryId, String documentGroup,
			List<Document> uiDocuments) {
		List<ApplicantDocumentEntity> updateDocumentsEntity = applicantDaoManager.createSupplementaryApplDoc(arn,
				supplementaryId, documentGroup, uiDocuments);
		/*
		 * List<ApplicantDocumentEntity> fetchSuppDocForAnSuppId =
		 * applicationDocRepo.fetchSuppDocForAnSuppId(arn, supplementaryId,
		 * documentGroup);
		 */
		SuppapplicantEntity fetchSupplementaryApplByArn = suppApplicantRepository.fetchSupplementaryApplByArn(arn,
				supplementaryId);

		// when there is no doc in db
		if(fetchSupplementaryApplByArn!=null){
		List<ApplicantDocumentEntity> applicantDocumentsById = fetchSupplementaryApplByArn.getPrimaryKey()
				.getApplicantId().getApplicantDocumentsById();
		
		if (applicantDocumentsById.isEmpty()) {
			applicantDocumentsById.addAll(updateDocumentsEntity);

		} else {
			/**
			 * This code basically updates the image of an supp doc as well as
			 * add an image into an existing docGroup || document
			 */
			List<ApplicantDocumentEntity> fetchSuppDocForAnSuppId = fetchSupplementaryApplByArn.getPrimaryKey()
					.getApplicantId().getApplicantDocumentsById();

			updateDocOrCreateNewDoc(updateDocumentsEntity, fetchSuppDocForAnSuppId);
		}
		
		suppApplicantRepository.save(fetchSupplementaryApplByArn);
		}
		// TODO Throw an exception here as because no supp found here

	}

	/**
	 * @param uiDocumentsToEntity
	 * @param fetchDbDoc
	 */
	private void updateDocOrCreateNewDoc(List<ApplicantDocumentEntity> uiDocumentsToEntity,
			List<ApplicantDocumentEntity> fetchDbDoc) {
		boolean docNameNotMatch = true;
		for (ApplicantDocumentEntity uiDoc : uiDocumentsToEntity) {
			docNameNotMatch = true;
			String uiDocName = uiDoc.getDocumentsMasterByDocumentType().getKey();
			List<ImageEntity> uiImage = uiDoc.getImageById();
			for (ApplicantDocumentEntity dbDocument : fetchDbDoc) {
				String dbKey = dbDocument.getDocumentsMasterByDocumentType().getKey();
				List<ImageEntity> dbImageById = dbDocument.getImageById();

				if (uiDocName.equalsIgnoreCase(dbKey)) {
					compareDomainVDatabaseDoc(uiImage, dbImageById);
					docNameNotMatch = false;
				}
			}
			if (docNameNotMatch) {
				fetchDbDoc.add(uiDoc);
			}
		}
	}

	private final void compareDomainVDatabaseDoc(List<ImageEntity> uiImage, List<ImageEntity> dbImages) {
		boolean matchFound = false;
		List<ImageEntity> newImageAddedFromDomain = new ArrayList<>();
		for (ImageEntity domainImage : uiImage) {
			matchFound = false;
			for (ImageEntity imageEntity : dbImages) {
				if (domainImage.getImageIndex() == imageEntity.getImageIndex()) {
					imageEntity.setPhoto(domainImage.getPhoto());
					matchFound = true;
				}
			}
			if (!matchFound) {
				newImageAddedFromDomain.add(domainImage);
			}
		}
		dbImages.addAll(newImageAddedFromDomain);
	}

	@Override
	public void saveStaffDetails(String arn, ApplicationStaffDetails staffDetails) {

		CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);

		StaffDetailsEntity staffDetailsEntity=ccApplicationEntity.getStaffByApplicationId();
		
		Transformation.populateStaffDetailsEntity(staffDetails,
				staffDetailsEntity);
		
		staffDetailsEntity.setCcapplicationStaffById(ccApplicationEntity);
		ccApplicationEntity.setStaffByApplicationId(staffDetailsEntity);

		ccApplicationRepository.save(ccApplicationEntity);

	}

	@Override
	public Image getImageOfSuppl(String arn, long supplementaryId, long index, String documentGroup) {
		SuppapplicantEntity supplApplByArn = suppApplicantRepository.fetchSupplementaryApplByArn(arn, supplementaryId);
		if (supplApplByArn != null) {
			for (ApplicantDocumentEntity suppDocuments : supplApplByArn.getPrimaryKey().getApplicantId()
					.getApplicantDocumentsById()) {
				String iterableDocGroup = suppDocuments.getDocumentsMasterByDocumentType().getDocGroupMasterByDocGroup()
						.getName();
				for (ImageEntity suppImage : suppDocuments.getImageById()) {
					if (iterableDocGroup.equalsIgnoreCase(documentGroup) && suppImage.getImageIndex() == index) {
						Image image = Transformation.transformImageEntityToImage(suppImage);
						return image;
					}
				}
			}
		}
		return null;

	}

	@Override
	public Image getImageOfPrimary(String arn, String documentGroup, long index) {
		ApplicantDetailsEntity primaryApplicant = applicantDetailsRepository.getPrimaryApplicant(arn);
		if (primaryApplicant != null) {
			List<ApplicantDocumentEntity> applicantDocuments = primaryApplicant.getApplicantDocumentsById();
			for (ApplicantDocumentEntity documentEntity : applicantDocuments) {
				String iterableDocGroup = documentEntity.getDocumentsMasterByDocumentType()
						.getDocGroupMasterByDocGroup().getName();
				for (ImageEntity primaryImage : documentEntity.getImageById()) {
					if (documentGroup.equalsIgnoreCase(iterableDocGroup) && primaryImage.getImageIndex() == index) {
						Image image = Transformation.transformImageEntityToImage(primaryImage);
						return image;
					}
				}
			}
		}
		return null;
	}

	@Override
	public void updateApplicationWithAoRefNumber(String aoRefNum, String arn) {
		CCApplicationEntity ccApplicationEntity = ccApplicationRepository.getApplicationViaArn(arn);
		ccApplicationEntity.setAoRefNum(aoRefNum);
		log.debug("Update the document complete indicator to true");
		ccApplicationEntity.getCcAppAddInfo().setDocumentCompleteIndicator(CommonUtils.convertToString(true));
		ccApplicationRepository.save(ccApplicationEntity);
	}

	@Override
	public List<Document> getSuppApplicantDocBySuppId(String arn, long supplimentaryId) {
		SuppapplicantEntity supplApplByArn = suppApplicantRepository.fetchSupplementaryApplByArn(arn, supplimentaryId);
		List<Document> documents=new ArrayList<>();
		if (supplApplByArn != null) {
			List<ApplicantDocumentEntity> documentEntity = supplApplByArn.getPrimaryKey().getApplicantId()
					.getApplicantDocumentsById();

			documentEntity.stream().forEach(p -> documents.add(Transformation.transformApplicationDocumentEntityToDocument(p)));					
			if (!documents.isEmpty()) {
				return documents;
			}
		}
		return documents;
	}

	@Override
	public String getApplicationStatus(int idType, String idValue) {
		return ccApplicationRepository.getApplicationStatus(idType, idValue);
	}

	@Override
	public void setEssentialDataPreFilledIndicator(boolean essentialDataPreFilledIndicator,String arn) {
		final CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
		if(ccApplicationEntity.getCcAppAddInfo()!=null) {
			if(ccApplicationEntity.getCcAppAddInfo().getEssentialDataIndicator()==null){
				ccApplicationEntity.getCcAppAddInfo().setEssentialDataIndicator(CommonUtils.convertToString(essentialDataPreFilledIndicator));
				ccApplicationRepository.save(ccApplicationEntity);
			}
			else if (!false == CommonUtils.convertToBoolean(ccApplicationEntity.getCcAppAddInfo().getEssentialDataIndicator())) {
				ccApplicationEntity.getCcAppAddInfo().setEssentialDataIndicator(CommonUtils.convertToString(essentialDataPreFilledIndicator));
				ccApplicationRepository.save(ccApplicationEntity);
			}
		}
		
	}

}
