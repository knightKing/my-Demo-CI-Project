package hsbc.aofe.service.service;

import java.util.List;

import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.repository.common.CCRepositoryService;
import hsbc.aofe.service.facade.CCRepoFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*****************************************
 * Created by sahil.verma on 6/6/2017.
 *****************************************
 */
@Service
public class CCRepositoryServiceImpl implements CCRepositoryService {

    private CCRepoFacade ccRepoFacade;

    @Autowired
    public CCRepositoryServiceImpl(CCRepoFacade ccRepoFacade) {
        this.ccRepoFacade = ccRepoFacade;
    }


    /**
     * @return all the credit card's available in the master-table CREDITCARDMASTER
     */
    @Override
    public List<CreditCard> getAllCreditCards() {
        return ccRepoFacade.getAllCreditCards();
    }
    
    /**
     * @return all the credit card's available in the digital journey
     */
    @Override
    public List<CreditCard> getCreditCardsForDigitalJourney() {
        return ccRepoFacade.getCreditCardsForDigitalJourney();
    }

}
