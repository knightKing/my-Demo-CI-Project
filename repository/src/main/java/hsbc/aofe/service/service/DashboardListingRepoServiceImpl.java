package hsbc.aofe.service.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.DashboardListing;
import hsbc.aofe.domain.Document;
import hsbc.aofe.domain.DocumentGroup;
import hsbc.aofe.domain.DocumentName;
import hsbc.aofe.domain.Image;
import hsbc.aofe.domain.Remarks;
import hsbc.aofe.domain.RemarksType;
import hsbc.aofe.repository.common.ApplicationRepositoryService;
import hsbc.aofe.repository.common.DashboardListingRepositoryService;
import hsbc.aofe.service.facade.DashboardListingDaoManager;

@Service
public class DashboardListingRepoServiceImpl implements DashboardListingRepositoryService {

	DashboardListingDaoManager dashboardListingDaoManager;
	ApplicationRepositoryService applicationRepositoryService;

	@Autowired
	public DashboardListingRepoServiceImpl(DashboardListingDaoManager dashboardListingDaoManager,
			ApplicationRepositoryService applicationRepositoryService) {
		this.dashboardListingDaoManager = dashboardListingDaoManager;
		this.applicationRepositoryService = applicationRepositoryService;
	}

	@Override
	public DashboardListing findAllApplications(List<String> sort, List<String> filter, String uid, int offset,
			int pageLimit) {
		return dashboardListingDaoManager.manageFindAllApplications(sort, filter, uid, offset, pageLimit);

	}

	@Override
	public String updateFulfilmentAgent(String fulfilmentAgent, String arn) {
		return dashboardListingDaoManager.updateFulfilmentAgent(fulfilmentAgent, arn);
	}

	@Override
	public void addRemarks(String arn, Remarks remarks, RemarksType type) {
		dashboardListingDaoManager.addRemarks(arn, remarks, type);
		if (type.equals(RemarksType.FOR_FA_DECLARATION)) {
			File faDeclarationPDF = createPdfForFADeclaration(remarks);
			Document document = createPDFDocument(faDeclarationPDF);
			List<Document> documents = new ArrayList<>();
			documents.add(document);
			applicationRepositoryService.createPrimaryApplDoc(arn, DocumentGroup.DECLARATION.getValue(), documents);
		}
	}

	private Document createPDFDocument(File faDeclarationPDF) {
		// converting file to byte array
		byte[] bytesArray = new byte[(int) faDeclarationPDF.length()];
		FileInputStream fis;
		try {
			fis = new FileInputStream(faDeclarationPDF);
			fis.read(bytesArray);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Image image = new Image();
		image.setName("FFDeclarations.pdf");
		image.setIndex(1);
		image.setExtracted(false);
		image.setImage(bytesArray);
		List<Image> images = new ArrayList<>();
		images.add(image);

		Document document = new Document();
		document.setDocGroup(DocumentGroup.DECLARATION);
		document.setDocName(DocumentName.FFDECLARATION);
		document.setNumberOfImages(1);
		document.setImages(images);

		return document;
	}

	private File createPdfForFADeclaration(Remarks remarks) {
		File faDeclaration = null;
		try {
			faDeclaration = File.createTempFile("faDeclaration", ".pdf");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		PDDocument doc = null;
		try {
			doc = new PDDocument();
			PDPage page = new PDPage();
			doc.addPage(page);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);

			PDFont pdfFont = PDType1Font.COURIER;
			float fontSize = 12;
			float leading = 1.5f * fontSize;

			PDRectangle mediabox = page.getMediaBox();
			float margin = 72;
			float width = mediabox.getWidth() - 2 * margin;
			float startX = mediabox.getLowerLeftX() + margin;
			float startY = mediabox.getUpperRightY() - margin;

			List<String> lines = new ArrayList<String>();
			lines.add("Fulfilment Agent ID : " + remarks.getStaffId());
			lines.add("Time : " + remarks.getDateTime());
			String declaration = "Declaration : " + remarks.getRemark().get(0);
			int lastSpace = -1;
			while (declaration.length() > 0) {
				int spaceIndex = declaration.indexOf(' ', lastSpace + 1);
				if (spaceIndex < 0)
					spaceIndex = declaration.length();
				String subString = declaration.substring(0, spaceIndex);
				float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
				if (size > width) {
					if (lastSpace < 0)
						lastSpace = spaceIndex;
					subString = declaration.substring(0, lastSpace);
					float lineWithoutSpaceSize = 0.0f;
					try {
						lineWithoutSpaceSize = fontSize * pdfFont.getStringWidth(subString) / 1000;
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (lineWithoutSpaceSize > width) {
						Float noOfLinesDecimalValue = lineWithoutSpaceSize / width;
						int noOfLines = noOfLinesDecimalValue.intValue() + 1;
						int index = subString.length() / noOfLines;
						int begin = 0;
						int end = index;
						for (int i = 0; i < noOfLines; i++) {
							String subStringWithoutSpace = subString.substring(begin, end);
							begin = end;
							end += index;
							lines.add(subStringWithoutSpace);
						}
					} else {
						lines.add(subString);
					}
					declaration = declaration.substring(lastSpace).trim();
					lastSpace = -1;
				} else if (spaceIndex == declaration.length()) {
					lines.add(declaration);
					declaration = "";
				} else {
					lastSpace = spaceIndex;
				}
			}
			contentStream.beginText();
			contentStream.setFont(pdfFont, fontSize);
			contentStream.newLineAtOffset(startX, startY);
			for (String line : lines) {
				contentStream.showText(line);
				contentStream.newLineAtOffset(0, -leading);
			}
			contentStream.endText();
			contentStream.close();
			doc.save(faDeclaration);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				try {
					doc.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return faDeclaration;
	}

	@Override
	public List<Remarks> getRemarksByType(String arn, RemarksType type) {
		return dashboardListingDaoManager.getRemarksByType(arn, type);
	}
}
