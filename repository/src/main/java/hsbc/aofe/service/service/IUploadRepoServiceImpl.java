package hsbc.aofe.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import hsbc.aofe.iupload.domain.IUploadDetail;
import hsbc.aofe.repository.common.IUploadRepositoryService;
import hsbc.aofe.service.facade.IUploadRepoFacade;

/*****************************************
 * Created by sahil.verma on 6/1/2017.
 *****************************************
 */
@Service
public class IUploadRepoServiceImpl implements IUploadRepositoryService {


    @Autowired
    private IUploadRepoFacade iUploadRepoFacade;
    @Autowired
	private TransactionTemplate transactionTemplate;

    /**
     * This method is used to return ARNS from

     * {@link hsbc.aofe.iupload.entities.CCApplicationEntity} based on the input
     * status
     *
     * @param status
     * @return List of ARN
     */
    @Override
    public List<String> getARNsExcludingIUploadARNs(String status) {
        return iUploadRepoFacade.getARNsExcludingIUploadARNs(status);
    }

	@Override
	public void saveStatus(hsbc.aofe.iupload.domain.IUploadRequestData iccRequestBody) {
		iUploadRepoFacade.saveStatus(iccRequestBody);
	}

	@Override
	public IUploadDetail findApplicationByARN(String arn) {
		return transactionTemplate.execute(status -> {
			IUploadDetail iUploadDetail = iUploadRepoFacade.findApplicationByARN(arn);
			return iUploadDetail;
		});
	}
	
}
