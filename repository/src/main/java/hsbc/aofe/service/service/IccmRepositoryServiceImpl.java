package hsbc.aofe.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import hsbc.aofe.iccm.domain.NotificationRequest;
import hsbc.aofe.repository.common.IccmRepositoryService;
import hsbc.aofe.service.facade.IccmRepoFacade;

@Service
public class IccmRepositoryServiceImpl implements IccmRepositoryService {

    private IccmRepoFacade iccmRepoFacade;
    
    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    public IccmRepositoryServiceImpl(IccmRepoFacade iccmRepoFacade) {
        this.iccmRepoFacade = iccmRepoFacade;
    }

    @Override
    public void saveIccm(String event, String arn, String channel, String uid, List<String> remarks, NotificationRequest notificationRequest) {
        iccmRepoFacade.saveIccm(event, arn, channel, uid, remarks, notificationRequest);
    }

	@Override
	public NotificationRequest getIccmNotification(String event, String arn, String channel, String uid, List<String> remarks) {
		return iccmRepoFacade.populateIccmNotificationRequest(event, arn, channel, uid, remarks);
	}

	@Override
	public void saveFollowUpApplications() {
		transactionTemplate.execute(status -> {
			return iccmRepoFacade.saveFollowUpApplicationsInIccmEvent();
		});
	}

	@Override
	public List<NotificationRequest> getFollowUpApplications() {
		return transactionTemplate.execute(status -> {
			return iccmRepoFacade.getFollowUpApplications();
		});
	}

	@Override
	public void updateSuccessIccm(NotificationRequest notificationRequest) {
		transactionTemplate.execute(status -> {
			return iccmRepoFacade.updateSuccessIccm(notificationRequest);
		});
	}

	@Override
	public void updateFailedIccm(NotificationRequest notificationRequest) {
		transactionTemplate.execute(status -> {
			return iccmRepoFacade.updateFailedIccm(notificationRequest);
		});
	}
	
}
