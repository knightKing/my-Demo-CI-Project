package hsbc.aofe.service.service;

import java.sql.Timestamp;
import java.util.List;

import hsbc.aofe.domain.IpAccessDetails;
import hsbc.aofe.repository.common.IpAccessRepositoryService;
import hsbc.aofe.service.facade.IpAccessDaoManager;

public class IpAccessRepoServiceImpl implements IpAccessRepositoryService{
	
	IpAccessDaoManager ipAccessDaoManager ;

	@Override
	public void saveIpAddressDetails(String ipAddress, Timestamp accessTime, String url) {
		
		ipAccessDaoManager.saveIpAddressDetails(ipAddress, accessTime, url);
		
	}

	@Override
	public List<IpAccessDetails> getIpAddressDetails(String ipAddress) {
		
		return ipAccessDaoManager.getIpAddressDetails(ipAddress);
	}
	

}
