package hsbc.aofe.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hsbc.aofe.domain.VerifiedIncomeCalcParameters;
import hsbc.aofe.repository.common.OcrRepositoryService;
import hsbc.aofe.service.facade.OcrDaoManager;

@Component
public class OcrRepositoryServiceImpl implements OcrRepositoryService {

	OcrDaoManager ocrDaoManager;

	@Autowired
	public OcrRepositoryServiceImpl(OcrDaoManager ocrDaoManager) {
		this.ocrDaoManager = ocrDaoManager;
	}

	@Override
	public VerifiedIncomeCalcParameters getOcrdata(String arn) {
		return ocrDaoManager.getOcrdata(arn);		
	}

}
