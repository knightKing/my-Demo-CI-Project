package hsbc.aofe.service.service;

import hsbc.aofe.exception.ApplicationDoesNotExistException;
import hsbc.aofe.exception.InvalidOtpException;
import hsbc.aofe.repository.common.OtpRepositoryService;
import hsbc.aofe.service.entities.OtpUser;
import hsbc.aofe.service.jparepository.OtpRepository;
import hsbc.aofe.util.TimeBasedOneTimePasswordGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.KeyGenerator;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
@Slf4j
@Service
public class OtpRepoServiceImpl implements OtpRepositoryService {

	private OtpRepository otpRepository;
	private TimeBasedOneTimePasswordGenerator timeBasedOneTimePasswordGenerator;
	
	@Autowired
	public OtpRepoServiceImpl(OtpRepository otpRepository,
							TimeBasedOneTimePasswordGenerator timeBasedOneTimePasswordGenerator) {
		this.otpRepository = otpRepository;
		this.timeBasedOneTimePasswordGenerator = timeBasedOneTimePasswordGenerator;
	}
	
	@Override
	public String otpValidate(String arn, String otp) {

		OtpUser user = otpRepository.findByArn(arn);
		
		if(user == null) {
			throw new ApplicationDoesNotExistException("Application Not Found.");
		}

		Duration d = Duration.between(user.getCreatedOn(), LocalDateTime.now());
		  if (d.getSeconds() < 900  && user.getOtp().equals(otp)) {
		   log.debug("OTP is Valid");
		   user.setFlag("VERIFIED");
		   otpRepository.save(user);
		   //return applicationDaoManager.findApplicationByArn(arn);
		   return arn;
		  }
		   log.debug("Invalid OTP");
		   user.setFlag("INVALID");
		   otpRepository.save(user);
		   throw new InvalidOtpException("Invalid Otp");
	}
	

	@Override
	public String generateOneTimePassword(String arn) throws InvalidKeyException, NoSuchAlgorithmException {
		Date now = new Date();

		final Key secretKey;

		final KeyGenerator keyGenerator = KeyGenerator.getInstance(timeBasedOneTimePasswordGenerator.getAlgorithm());

		// SHA-1 and SHA-256 prefer 64-byte (512-bit) keys; SHA512 prefers
		// 128-byte keys
		keyGenerator.init(512);

		secretKey = keyGenerator.generateKey();

		String otpGen = String.format("%06d", timeBasedOneTimePasswordGenerator.generateOneTimePassword(secretKey, now));

		OtpUser otpUser = otpRepository.findByArn(arn);
		if(otpUser == null) {
			otpUser = new OtpUser();
			otpUser.setArn(arn);
		}
		otpUser.setCreatedOn(LocalDateTime.now());
		otpUser.setFlag("GENERATED");
		otpUser.setOtp(otpGen);
		otpRepository.save(otpUser);
		return otpGen;
	}
	
}
