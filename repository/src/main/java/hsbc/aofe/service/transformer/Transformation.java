package hsbc.aofe.service.transformer;

import hsbc.aofe.domain.*;
import hsbc.aofe.iccm.domain.IccmCommunication;
import hsbc.aofe.service.entities.*;
import hsbc.aofe.util.CommonUtils;
import hsbc.aofe.util.UserUtil;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.DozerBeanMapper;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Transformation {

	public static AddressEntity populateAddressEntity(AddressEntity addressEntity, Address address) {
		addressEntity.setDoorNumber(address.getLine1Address());
		addressEntity.setL2Street(address.getLine2Address());
		addressEntity.setL3Unit(address.getLine3Address());
		addressEntity.setCity(address.getCity());
		addressEntity.setPostalCode(address.getPostalCode());
		addressEntity.setCreatedOn(LocalDateTime.now());
		addressEntity.setCreatedBy(UserUtil.getUser());
		addressEntity.setLastModifiedBy(UserUtil.getUser());
		addressEntity.setLastModifiedOn(LocalDateTime.now());
		return addressEntity;
	}

	public static Address transferAddressEntityTOAddress(AddressEntity addressEntity) {
		Address address = new Address();
		address.setId(addressEntity.getId());
		address.setLine1Address(addressEntity.getDoorNumber());
		address.setLine2Address(addressEntity.getL2Street());
		address.setLine3Address(addressEntity.getL3Unit());
		address.setCity(addressEntity.getCity());
		CommonUtils.resolveIfNull(() -> addressEntity.getCountryListMasterByCountry())
				.ifPresent(a -> address.setCountry(CountryCode.valueOf(a.getName())));
		address.setPostalCode(addressEntity.getPostalCode());
		if (addressEntity.getApplicantAddressesById() != null && !addressEntity.getApplicantAddressesById().isEmpty()) {
			CommonUtils
					.resolveIfNull(
							() -> addressEntity.getApplicantAddressesById().get(0).getType().getType().toUpperCase())
					.ifPresent(x -> address.setAddressType(AddressType.valueOf(x)));
		}
		return address;
	}

	public static ApplicantDetailsEntity populateApplicantEntity(ApplicantDetailsEntity applicantDetailsEntity,
			Applicant applicant) {

		// applicantDetailsEntity.setId(applicant.getId());
		setApplicantDetailsEntity(applicantDetailsEntity, applicant);
		ApplicantAdditionalInfo additionalInfo = applicant.getAdditionalInfo();
		if (additionalInfo != null) {
			setApplicantAdditionalInfo(applicantDetailsEntity, additionalInfo);
		}
		ApplicantDeclarations applicantDeclarations = applicant.getApplicantDeclarations();
		if (applicantDeclarations != null) {
			setApplicantDeclarations(applicantDetailsEntity, applicantDeclarations);
		}
		return applicantDetailsEntity;
	}

	public static Applicant transformApplicantEntityToApplicant(ApplicantDetailsEntity applicantDetailsEntity) {
		// ApplicantDetailsEntity applicantDetailsEntity =
		// applicationEntity.getApplicantDetailsByApplicantId();
		Applicant applicant = new Applicant();
		applicant.setId(applicantDetailsEntity.getId());
		applicant.setSalutation((applicantDetailsEntity.getSalutation() == null) ? null
				: Title.valueOf(applicantDetailsEntity.getSalutation()));
		if (applicantDetailsEntity.getCcapplicationsById() == null
				|| applicantDetailsEntity.getCcapplicationsById().isEmpty()) {
			applicant.setType(ApplicantType.SUPPLEMENTARY);
		} else {
			applicant.setType(ApplicantType.PRIMARY);
		}
		applicant.setEmail(applicantDetailsEntity.getEmail());
		applicant.setMobile(applicantDetailsEntity.getMobile());
		applicant.setDialCodeMobile(applicantDetailsEntity.getDialCodeMobile());
		Name name = new Name();
		name.setFirstName(applicantDetailsEntity.getFirstName());
		name.setMiddleName(applicantDetailsEntity.getMiddleName());
		name.setLastName(applicantDetailsEntity.getLastName());
		applicant.setName(name);
		applicant.setGender(applicantDetailsEntity.getGender());
		applicant.setDateOfBirth(CommonUtils.convertLocalDateToString(applicantDetailsEntity.getDateOfBirth()));
		if (applicantDetailsEntity.getDocumentsMasterByIdDocType() != null) {
			String documentKeyPresentInDB = applicantDetailsEntity.getDocumentsMasterByIdDocType().getKey();
			applicant.setIdDocType((documentKeyPresentInDB == null) ? null
					: Arrays.asList(DocumentName.values()).stream()
							.filter(a -> a.getValue().equalsIgnoreCase(documentKeyPresentInDB))
							.collect(Collectors.toList()).get(0));
		}
		applicant.setIdValue(applicantDetailsEntity.getIdValue());
		applicant.setGender(applicantDetailsEntity.getGender());

		if (applicantDetailsEntity.getAppAddInfo() != null) {
			applicant.setAdditionalInfo(transformApplicantAdditionalInfoEntityToApplicant(applicantDetailsEntity));
		}

		if (applicantDetailsEntity.getArnCcBridgesById() != null) {
			applicant.setCards(applicantDetailsEntity.getArnCcBridgesById().stream()
					.map(a -> transformCreditCardMasterEntityToCreditCard(a)).collect(Collectors.toList()));
		}
		if (applicantDetailsEntity.getApplicantDocumentsById() != null
				&& !applicantDetailsEntity.getApplicantDocumentsById().isEmpty()) {
			List<Document> documents = applicantDetailsEntity.getApplicantDocumentsById().stream()
					.map(a -> transformApplicationDocumentEntityToDocument(a)).collect(Collectors.toList());

			applicant.setDocuments(documents);
		}
		ApplicantDeclarations applicantDeclarations = new ApplicantDeclarations();

		applicantDeclarations.setConsentContactMe((applicantDetailsEntity.getConsentContactMe() == null) ? false
				: CommonUtils.convertToBoolean(applicantDetailsEntity.getConsentContactMe()));
		applicantDeclarations.setConsentPersonalData((applicantDetailsEntity.getConsentPersonalData() == null) ? false
				: CommonUtils.convertToBoolean(applicantDetailsEntity.getConsentPersonalData()));
		applicantDeclarations.setConsentMarketing((applicantDetailsEntity.getConsentMarketing() == null) ? false
				: CommonUtils.convertToBoolean(applicantDetailsEntity.getConsentMarketing()));
		applicantDeclarations.setConsentAccountOpening((applicantDetailsEntity.getConsentAccountOpening() == null)
				? false : CommonUtils.convertToBoolean(applicantDetailsEntity.getConsentAccountOpening()));
		applicantDeclarations.setSharedCreditLimit((applicantDetailsEntity.getSharedCreditLimit() == null) ? false
				: CommonUtils.convertToBoolean(applicantDetailsEntity.getSharedCreditLimit()));
		applicantDeclarations
				.setBalanceTransferOnCreditCard((applicantDetailsEntity.getBalanceTransferOnCreditCard() == null) ? null
						: CommonUtils.convertToBoolean(applicantDetailsEntity.getBalanceTransferOnCreditCard()));

		if (applicantDeclarations.getBalanceTransferOnCreditCard() != null
				&& applicantDeclarations.getBalanceTransferOnCreditCard()) {
			BeneficiaryDetails beneficiaryDetails = transformBeneficiaryDetails(
					applicantDetailsEntity.getArnCcBridgesById());
			applicant.setBeneficiaryDetails(beneficiaryDetails);
		}

		if (applicantDetailsEntity.getSuppApplicantsById() != null
				&& !applicantDetailsEntity.getSuppApplicantsById().isEmpty()) {
			String relationWithPrimary = applicantDetailsEntity.getSuppApplicantsById().stream()
					.filter(a -> a.getPrimaryKey().getApplicantId().getId() == applicant.getId())
					.collect(Collectors.toList()).get(0).getRelationWithPrimaryApplicant();
			if (applicant.getAdditionalInfo() != null) {
				applicant.getAdditionalInfo()
						.setRelationshipWithPrimary((relationWithPrimary == null) ? null
								: Arrays.asList(RelationshipWithPrimary.values()).stream()
										.filter(a -> a.getValue().equalsIgnoreCase(relationWithPrimary))
										.collect(Collectors.toList()).get(0));
			}
		}
		applicant.setApplicantDeclarations(applicantDeclarations);
		return applicant;
	}

	public static BeneficiaryDetails transformBeneficiaryDetails(List<ARNCcBridgeEntity> arnCcBridgeEntities) {

		BeneficiaryDetails beneficiaryDetails = new BeneficiaryDetails();

		List<ARNCcBridgeEntity> bridgeEntities = filterARNCcBridge(arnCcBridgeEntities);
		if (CollectionUtils.isNotEmpty(bridgeEntities)) {
			ARNCcBridgeEntity arnCcBridgeEntity = bridgeEntities.get(0);
			beneficiaryDetails.setCcKey(arnCcBridgeEntity.getPrimaryKey().getCreditCardMasterByCcId().getKey());
			beneficiaryDetails.setBeneficiaryBank(arnCcBridgeEntity.getAcTfrBeneficiaryBankName() == null ? null
					: Arrays.asList(BankNameForBalanceTransfer.values()).stream()
							.filter(a -> a.getValue().equalsIgnoreCase(arnCcBridgeEntity.getAcTfrBeneficiaryBankName()))
							.collect(Collectors.toList()).get(0));

			CommonUtils.resolveIfNull(() -> arnCcBridgeEntity.getAcTfrBeneficiaryAcName())
					.ifPresent(x -> beneficiaryDetails.setBeneficiaryAccountName(x));
			CommonUtils.resolveIfNull(() -> arnCcBridgeEntity.getAcTfrBeneficiaryAcNumber())
					.ifPresent(x -> beneficiaryDetails.setBeneficiaryAccountNumber(x));
			CommonUtils.resolveIfNull(() -> arnCcBridgeEntity.getAmtToBeTransfered())
					.ifPresent(x -> beneficiaryDetails.setAmountToBeTransferred(x));
		}
		return beneficiaryDetails;
	}

	public static List<ARNCcBridgeEntity> filterARNCcBridge(List<ARNCcBridgeEntity> arnCcBridgeEntities) {

		return arnCcBridgeEntities.stream().filter(a -> (a.getIsSelected() != null)).collect(Collectors.toList());
	}

	public static ApplicantAdditionalInfoEntity populateApplicantAdditionalInfoEntity(
			ApplicantAdditionalInfoEntity applicantAdditionalInfoEntity, Applicant applicant) {

		ApplicantAdditionalInfo applicantAdditionalInfo = applicant.getAdditionalInfo();
		if (applicantAdditionalInfo != null) {

			CommonUtils
					.resolveIfNull(() -> CommonUtils.convertToString(applicantAdditionalInfo.isMultipleNationality()))
					.ifPresent(applicantAdditionalInfoEntity::setIsMultipleNationality);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getPassportNumber())
					.ifPresent(applicantAdditionalInfoEntity::setPassportNumber);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getNationality2().getValue())
					.ifPresent(applicantAdditionalInfoEntity::setNationality2);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getNationality3().getValue())
					.ifPresent(applicantAdditionalInfoEntity::setNationality3);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getTaxResidenceCountry1().getValue())
					.ifPresent(applicantAdditionalInfoEntity::setTaxResidenceCountry1);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getTaxResidenceCountry2().getValue())
					.ifPresent(applicantAdditionalInfoEntity::setTaxResidenceCountry2);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getTaxResidenceCountry3().getValue())
					.ifPresent(applicantAdditionalInfoEntity::setTaxResidenceCountry3);
			CommonUtils.resolveIfNull(() -> Long.valueOf(applicantAdditionalInfo.getNoOfDependents().getCode()))
					.ifPresent(applicantAdditionalInfoEntity::setNoOfDependents);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getMaritalStatus().getValue())
					.ifPresent(applicantAdditionalInfoEntity::setMaritalStatus);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getCorrespondenceOn())
					.ifPresent(applicantAdditionalInfoEntity::setCorrespondenceOn);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getHomePhone())
					.ifPresent(applicantAdditionalInfoEntity::setHomePhone);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getOfficePhone())
					.ifPresent(applicantAdditionalInfoEntity::setOfficePhone);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getOfficeExtn())
					.ifPresent(applicantAdditionalInfoEntity::setOfficeExtn);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getMonthsToEmpPassExpiry())
					.ifPresent(applicantAdditionalInfoEntity::setMonthsToEmpPassExpiry);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getAnnualIncome())
					.ifPresent(applicantAdditionalInfoEntity::setAnnualIncome);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getLengthOfService())
					.ifPresent(applicantAdditionalInfoEntity::setLengthOfService);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getOccupation())
					.ifPresent(occupation -> applicantAdditionalInfoEntity.setOccupation(occupation.getValue()));
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getPosition())
					.ifPresent(applicantAdditionalInfoEntity::setPosition);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getIndustryType().toString())
					.ifPresent(applicantAdditionalInfoEntity::setIndustryType);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getCompanyName())
					.ifPresent(applicantAdditionalInfoEntity::setCompanyName);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getOfficePostalCode())
					.ifPresent(applicantAdditionalInfoEntity::setOfficePostalCode);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getTimeAtPreviousEmployer())
					.ifPresent(applicantAdditionalInfoEntity::setTimeAtPreviousEmployer);
			CommonUtils
					.resolveIfNull(
							() -> CommonUtils.convertToString(applicantAdditionalInfo.getHoldingPublicPosition()))
					.ifPresent(applicantAdditionalInfoEntity::setHoldingPublicPosition);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getPublicPositionDetails())
					.ifPresent(applicantAdditionalInfoEntity::setPublicPositionDetails);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getAutoDebitFromAccountNumber())
					.ifPresent(applicantAdditionalInfoEntity::setAutoDebitFromAccountNumber);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getMothersMaidenName())
					.ifPresent(applicantAdditionalInfoEntity::setMother_MaidenName);
			CommonUtils.resolveIfNull(
					() -> CommonUtils.convertToString(applicantAdditionalInfo.getAssociateOfPublicPositionHolder()))
					.ifPresent(applicantAdditionalInfoEntity::setAssociateOfPublicPositionHolder);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getAssociateOfPublicPositionHolderDetails())
					.ifPresent(applicantAdditionalInfoEntity::setAssociateOfPublicPositionHolderDetails);

			CommonUtils
					.resolveIfNull(
							() -> CommonUtils.convertToString(applicantAdditionalInfo.isIntroducedByHSBCCardHolder()))
					.ifPresent(applicantAdditionalInfoEntity::setIntroducedByHSBCCardHolder);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getIntroducedByHSBCCardHolderReferrerID())
					.ifPresent(applicantAdditionalInfoEntity::setIntroducedByHSBCCardHolderReferrerID);
			CommonUtils.resolveIfNull(
					() -> CommonUtils.convertToString(applicantAdditionalInfo.isResidentialAddressSameAsPrimary()))
					.ifPresent(applicantAdditionalInfoEntity::setIsResidentialAddressSameAsPrimary);
			CommonUtils.resolveIfNull(
					() -> CommonUtils.convertToString(applicantAdditionalInfo.isResidentialAddIdenticalToPermAddress()))
					.ifPresent(applicantAdditionalInfoEntity::setResidentialAddIdenticalToPermAddress);
			CommonUtils.resolveIfNull(() -> applicantAdditionalInfo.getMonthsAtResidentialAddress())
					.ifPresent(applicantAdditionalInfoEntity::setMonthsAtResidentialAddress);
			if (applicantAdditionalInfo.getHomePhone() != null) {
				applicantAdditionalInfoEntity.setDialCodeHomePhone(applicantAdditionalInfo.getDialCodeHomePhone());
			}
			applicantAdditionalInfoEntity.setCreatedOn(LocalDateTime.now());
			applicantAdditionalInfoEntity.setCreatedBy(UserUtil.getUser());
			applicantAdditionalInfoEntity.setLastModifiedOn(LocalDateTime.now());
			applicantAdditionalInfoEntity.setLastModifiedBy(UserUtil.getUser());

			// applicantAdditionalInfoEntity.setEducationlevel(applicant.getAdditionalInfo().getEducationlevel().getId());
			/*
			 * applicantAdditionalInfoEntity.setHomeownership(applicant.
			 * getAdditionalInfo().getHomeownership().getId());
			 */
			/*
			 * applicantAdditionalInfoEntity.setEmploymenttype(applicant.
			 * getAdditionalInfo().getEmploymenttype().getId());
			 */
			/*
			 * applicantAdditionalInfoEntity.setOfficeAddress(
			 * appAddInfo.getAddress().stream() .filter(a ->
			 * a.getAddressType().getId() == 4) .findFirst() .toString());
			 */
		}
		return applicantAdditionalInfoEntity;
	}

	public static ApplicantAdditionalInfo transformApplicantAdditionalInfoEntityToApplicant(
			ApplicantDetailsEntity applicantDetailsEntity) {
		// ApplicantDetailsEntity applicantDetailsEntity =
		// applicationEntity.getApplicantDetailsByApplicantId();
		ApplicantAdditionalInfoEntity applicantAdditionalInfoEntity = applicantDetailsEntity.getAppAddInfo();

		ApplicantAdditionalInfo applicantAdditionalInfo = new ApplicantAdditionalInfo();

		applicantAdditionalInfo.setMothersMaidenName(applicantAdditionalInfoEntity.getMother_MaidenName());
		if (applicantAdditionalInfoEntity.getIsMultipleNationality() != null) {
			applicantAdditionalInfo.setMultipleNationality(
					CommonUtils.convertToBoolean(applicantAdditionalInfoEntity.getIsMultipleNationality()));
		}

		String maritalStatus = applicantAdditionalInfoEntity.getMaritalStatus();
		if (maritalStatus != null) {
			applicantAdditionalInfo.setMaritalStatus(Arrays.asList(MaritalStatus.values()).stream()
					.filter(a -> a.getValue().equalsIgnoreCase(maritalStatus)).collect(Collectors.toList()).get(0));
		}

		String nationality2 = applicantAdditionalInfoEntity.getNationality2();
		applicantAdditionalInfo.setNationality2((nationality2 == null) ? null
				: Arrays.asList(CountryCode.values()).stream().filter(a -> a.getValue().equalsIgnoreCase(nationality2))
						.collect(Collectors.toList()).get(0));

		if (applicantAdditionalInfoEntity.getPassportCountry() != null) {
			String passportCountry = applicantAdditionalInfoEntity.getPassportCountry().getCode();
			applicantAdditionalInfo.setPassportCountry((passportCountry == null) ? null
					: Arrays.asList(CountryCode.values()).stream()
							.filter(a -> a.getValue().equalsIgnoreCase(passportCountry)).collect(Collectors.toList())
							.get(0));
		}

		String nationality3 = applicantAdditionalInfoEntity.getNationality3();
		applicantAdditionalInfo.setNationality3((nationality3 == null) ? null
				: Arrays.asList(CountryCode.values()).stream().filter(a -> a.getValue().equalsIgnoreCase(nationality3))
						.collect(Collectors.toList()).get(0));

		String taxResidenceCountry1 = applicantAdditionalInfoEntity.getTaxResidenceCountry1();
		applicantAdditionalInfo.setTaxResidenceCountry1((taxResidenceCountry1 == null) ? null
				: Arrays.asList(CountryCode.values()).stream()
						.filter(a -> a.getValue().equalsIgnoreCase(taxResidenceCountry1)).collect(Collectors.toList())
						.get(0));

		String taxResidenceCountry2 = applicantAdditionalInfoEntity.getTaxResidenceCountry2();
		applicantAdditionalInfo.setTaxResidenceCountry2((taxResidenceCountry2 == null) ? null
				: Arrays.asList(CountryCode.values()).stream().filter(
						a -> a.getValue().equalsIgnoreCase(applicantAdditionalInfoEntity.getTaxResidenceCountry2()))
						.collect(Collectors.toList()).get(0));

		String taxResidenceCountry3 = applicantAdditionalInfoEntity.getTaxResidenceCountry3();
		applicantAdditionalInfo.setTaxResidenceCountry3((taxResidenceCountry3 == null) ? null
				: Arrays.asList(CountryCode.values()).stream()
						.filter(a -> a.getValue().equalsIgnoreCase(taxResidenceCountry3)).collect(Collectors.toList())
						.get(0));

		applicantAdditionalInfo.setNoOfDependents(applicantAdditionalInfoEntity.getNoOfDependents() == null ? null
				: Arrays.asList(NumberOfDependents.values()).stream()
						.filter(a -> Long.valueOf(a.getCode()) == applicantAdditionalInfoEntity.getNoOfDependents())
						.collect(Collectors.toList()).get(0));

		if (applicantAdditionalInfoEntity.getEducationallevelmasterByEducationlevel() != null) {
			String education = applicantAdditionalInfoEntity.getEducationallevelmasterByEducationlevel().getLabel();

			applicantAdditionalInfo.setEducationLevel(education == null ? null
					: Arrays.asList(EducationalLevel.values()).stream()
							.filter(a -> a.getValue().equalsIgnoreCase(education)).collect(Collectors.toList()).get(0));
		}
		
		if (applicantAdditionalInfoEntity.getHomeownershipmasterByHomeownership() != null) {
			String home = applicantAdditionalInfoEntity.getHomeownershipmasterByHomeownership().getLabel();

			applicantAdditionalInfo.setHomeOwnership((home == null) ? null
					: Arrays.asList(HomeOwnership.values()).stream().filter(a -> a.getValue().equalsIgnoreCase(home))
							.collect(Collectors.toList()).get(0));
		}

		// applicantAdditionalInfo.setHomeownership(HomeOwnership.valueOf(applicantAdditionalInfoEntity.getHomeownershipmasterByHomeownership().getLabel()));
		applicantAdditionalInfo.setCorrespondenceOn(applicantAdditionalInfoEntity.getCorrespondenceOn());
		applicantAdditionalInfo.setHomePhone(applicantAdditionalInfoEntity.getHomePhone());
		applicantAdditionalInfo.setDialCodeHomePhone(applicantAdditionalInfoEntity.getDialCodeHomePhone());
		applicantAdditionalInfo.setOfficePhone(applicantAdditionalInfoEntity.getOfficePhone());
		applicantAdditionalInfo.setOfficeExtn(applicantAdditionalInfoEntity.getOfficeExtn());
		applicantAdditionalInfo.setPassportNumber(applicantAdditionalInfoEntity.getPassportNumber());
		applicantAdditionalInfo.setResidentialAddIdenticalToPermAddress(
				CommonUtils.convertToBoolean(applicantAdditionalInfoEntity.getResidentialAddIdenticalToPermAddress()));

		if (applicantAdditionalInfoEntity.getJobtypemasterByEmploymenttype() != null) {
			String employment = applicantAdditionalInfoEntity.getJobtypemasterByEmploymenttype().getName();
			applicantAdditionalInfo.setEmploymentType((employment == null) ? null
					: Arrays.asList(EmployementStatus.values()).stream()
							.filter(a -> a.getValue().equalsIgnoreCase(employment)).collect(Collectors.toList())
							.get(0));
		}

		// applicantAdditionalInfo.setEmploymenttype(EmployementStatus.valueOf(applicantAdditionalInfoEntity.getJobtypemasterByEmploymenttype().getName()));
		applicantAdditionalInfo.setMonthsToEmpPassExpiry(applicantAdditionalInfoEntity.getMonthsToEmpPassExpiry());
		applicantAdditionalInfo.setAnnualIncome(applicantAdditionalInfoEntity.getAnnualIncome());
		applicantAdditionalInfo.setLengthOfService(applicantAdditionalInfoEntity.getLengthOfService());

		String occupationPresentInDB = applicantAdditionalInfoEntity.getOccupation();
		applicantAdditionalInfo.setOccupation((occupationPresentInDB == null) ? null
				: Arrays.asList(Occupation.values()).stream()
						.filter(a -> a.getValue().equalsIgnoreCase(occupationPresentInDB)).collect(Collectors.toList())
						.get(0));

		applicantAdditionalInfo.setPosition(applicantAdditionalInfoEntity.getPosition());
		if (applicantAdditionalInfoEntity.getIndustryType() != null) {
			applicantAdditionalInfo
					.setIndustryType(NatureOfBusiness.valueOf(applicantAdditionalInfoEntity.getIndustryType()));
		}
		applicantAdditionalInfo.setCompanyName(applicantAdditionalInfoEntity.getCompanyName());
		// applicant.getAdditionalInfo().setResidentialAddIdenticalToPermAddress(residentialAddIdenticalToPermAddress);
		// no value in db.
		// applicant.setAddress(address);
		applicantAdditionalInfo.setOfficePostalCode(applicantAdditionalInfoEntity.getOfficePostalCode());
		applicantAdditionalInfo.setTimeAtPreviousEmployer(applicantAdditionalInfoEntity.getTimeAtPreviousEmployer());
		applicantAdditionalInfo.setHoldingPublicPosition(
				CommonUtils.convertToBoolean(applicantAdditionalInfoEntity.getHoldingPublicPosition()));
		applicantAdditionalInfo.setPublicPositionDetails(applicantAdditionalInfoEntity.getPublicPositionDetails());
		applicantAdditionalInfo
				.setAutoDebitFromAccountNumber(applicantAdditionalInfoEntity.getAutoDebitFromAccountNumber());
		if (applicantDetailsEntity.getCountryListMasterByCountryOfBirth() != null) {
			String countryListMasterByCountryOfBirth = applicantDetailsEntity.getCountryListMasterByCountryOfBirth()
					.getName();
			applicantAdditionalInfo.setCountryOfBirth((countryListMasterByCountryOfBirth == null) ? null
					: Arrays.asList(CountryCode.values()).stream()
							.filter(a -> a.getValue().equalsIgnoreCase(countryListMasterByCountryOfBirth))
							.collect(Collectors.toList()).get(0));
		}

		applicantAdditionalInfo.setOtherName(applicantDetailsEntity.getOtherName());
		if (applicantDetailsEntity.getArnCcBridgesById() != null
				&& !applicantDetailsEntity.getArnCcBridgesById().isEmpty()) {
			applicantAdditionalInfo.setNameOnCard(applicantDetailsEntity.getArnCcBridgesById().get(0).getNameOnCard());
		}
		// applicant.setRelationshipWithPrimary(relationshipWithPrimary); no
		// value in db.
		String nationality = applicantDetailsEntity.getNationality();
		applicantAdditionalInfo.setNationality((nationality == null) ? null
				: Arrays.asList(CountryCode.values()).stream().filter(a -> a.getValue().equalsIgnoreCase(nationality))
						.collect(Collectors.toList()).get(0));

		applicantAdditionalInfo.setIdIssueDate(applicantDetailsEntity.getIdIssueDate());
		applicantAdditionalInfo.setIdExpiryDate(applicantDetailsEntity.getIdExpiryDate());
		if (applicantDetailsEntity.getCountryListMasterByCountryOfIdIssue() != null) {
			String countryListMasterByCountryOfIdIssue = applicantDetailsEntity.getCountryListMasterByCountryOfIdIssue()
					.getName();
			applicantAdditionalInfo.setCountryOfIdIssue((countryListMasterByCountryOfIdIssue == null) ? null
					: Arrays.asList(CountryCode.values()).stream()
							.filter(a -> a.getValue().equalsIgnoreCase(countryListMasterByCountryOfIdIssue))
							.collect(Collectors.toList()).get(0));
		}
		applicantAdditionalInfo.setAssociateOfPublicPositionHolder(
				CommonUtils.convertToBoolean(applicantAdditionalInfoEntity.getAssociateOfPublicPositionHolder()));
		applicantAdditionalInfo.setAssociateOfPublicPositionHolderDetails(
				applicantAdditionalInfoEntity.getAssociateOfPublicPositionHolderDetails());
		if (applicantAdditionalInfoEntity.getIntroducedByHSBCCardHolder() != null) {
			applicantAdditionalInfo.setIntroducedByHSBCCardHolder(
					CommonUtils.convertToBoolean(applicantAdditionalInfoEntity.getIntroducedByHSBCCardHolder()));
		}

		applicantAdditionalInfo.setIntroducedByHSBCCardHolderReferrerID(
				applicantAdditionalInfoEntity.getIntroducedByHSBCCardHolderReferrerID());
		/*
		 * List<Document> documents =
		 * applicantDetailsEntity.getApplicantDocumentsById() .stream() .map(a
		 * -> transformApplicationDocumentEntityToDocument(a))
		 * .collect(Collectors.toList());
		 * applicantAdditionalInfo.setDocuments(documents);
		 */
		applicantAdditionalInfo.setResidentialAddressSameAsPrimary(
				CommonUtils.convertToBoolean(applicantAdditionalInfoEntity.getIsResidentialAddressSameAsPrimary()));
		applicantAdditionalInfo
				.setMonthsAtResidentialAddress(applicantAdditionalInfoEntity.getMonthsAtResidentialAddress());
		return applicantAdditionalInfo;
	}

	public static CCApplicationEntity populateApplicationEntity(CCApplicationEntity ccApplicationEntity,
			Application application) {

		ccApplicationEntity.setId(application.getId());
		ccApplicationEntity.setArn(application.getArn());
		// ccApplicationEntity.setAo_status(ao_status);
		ccApplicationEntity.setCreatedBy(UserUtil.getUser());
		ccApplicationEntity.setCreatedOn(LocalDateTime.now());
		ccApplicationEntity.setLastModifiedBy(UserUtil.getUser());
		ccApplicationEntity.setLastModifiedOn(LocalDateTime.now());
		return ccApplicationEntity;
	}

	public static CCApplicationAdditionalInfoEntity populateApplicationAdditionalInfoEntity(
			CCApplicationAdditionalInfoEntity ccApplicationAdditionalInfoEntity, Application application) {
		ccApplicationAdditionalInfoEntity
				.setAllocateExistingLimitToNew(CommonUtils.convertToString(application.isAllocateExistingLimitToNew()));
		ccApplicationAdditionalInfoEntity.setExistingCcnumber(application.getExistingCardNumber());
		ccApplicationAdditionalInfoEntity.setAtmLinkedAccount(application.getAtmLinkedAcNumber());
		ccApplicationAdditionalInfoEntity
				.setCreateAutoDebitFromAccount(CommonUtils.convertToString(application.isCreateAutoDebitFromAccount()));
		ccApplicationAdditionalInfoEntity.setHowMuchAutoDebit(application.getHowMuchAutoDebit());
		ccApplicationAdditionalInfoEntity
				.setReceiveEStatement(CommonUtils.convertToString(application.isReceiveEStatement()));
		ccApplicationAdditionalInfoEntity
				.setAgreeToGenericConsent(CommonUtils.convertToString(application.isAgreeToGenericConsent()));
		ccApplicationAdditionalInfoEntity
				.setTransactingOnMyOwn(CommonUtils.convertToString(application.isTransactingOnMyOwn()));
		// ccApplicationAdditionalInfoEntity.setWouldLikeToOpenNewAccount(String.valueOf(application.isWouldLikeToOpenNewAccount()));
		ccApplicationAdditionalInfoEntity.setCreatedOn(LocalDateTime.now());
		ccApplicationAdditionalInfoEntity.setCreatedBy(UserUtil.getUser());
		ccApplicationAdditionalInfoEntity.setLastModifiedBy(UserUtil.getUser());
		ccApplicationAdditionalInfoEntity.setLastModifiedOn(LocalDateTime.now());
		return ccApplicationAdditionalInfoEntity;
	}

	public static Application transformApplicationEntityToApplication(CCApplicationEntity ccApplicationEntity) {

		Application application = new Application();
		application.setId(ccApplicationEntity.getId());
		application.setArn(ccApplicationEntity.getArn());
		application.setAoRefNumber(ccApplicationEntity.getAoRefNum());
		application.setPrimaryApplicant(
				transformApplicantEntityToApplicant(ccApplicationEntity.getApplicantDetailsByApplicantId()));
		if(ccApplicationEntity.getChannelMasterById() != null) {
			String initiateApplication = ccApplicationEntity.getChannelMasterById().getName();
			List<ChannelType> channelList = Arrays.asList(ChannelType.values()).stream()
					.filter(a -> a.getChannel().equalsIgnoreCase(initiateApplication))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(channelList)) {
				application.setInitiatedBy(channelList.get(0));
			}
		}
		application.setAllocateExistingLimitToNew(
				CommonUtils.convertToBoolean(ccApplicationEntity.getCcAppAddInfo().getAllocateExistingLimitToNew()));
		application.setExistingCardNumber(ccApplicationEntity.getCcAppAddInfo().getExistingCcnumber());
		application.setAtmLinkedAcNumber(ccApplicationEntity.getCcAppAddInfo().getAtmLinkedAccount());
		application.setCreateAutoDebitFromAccount(
				CommonUtils.convertToBoolean(ccApplicationEntity.getCcAppAddInfo().getCreateAutoDebitFromAccount()));
		application.setHowMuchAutoDebit(ccApplicationEntity.getCcAppAddInfo().getHowMuchAutoDebit());
		application.setStaffDetails(ccApplicationEntity.getStaffByApplicationId() == null ? null
				: transformStaffDetails(ccApplicationEntity.getStaffByApplicationId()));
		if(ccApplicationEntity.getCcAppAddInfo().getDocumentCompleteIndicator()==null){
			application.setDocCompletenessIndicator(false);
		}
		else {
			application.setDocCompletenessIndicator(CommonUtils.convertToBoolean(ccApplicationEntity.getCcAppAddInfo().getDocumentCompleteIndicator()));
		}
		if(ccApplicationEntity.getCcAppAddInfo().getEmploymentCheckIndicator()==null){
			application.setEmploymentCheckIndicator(false);
		}
		else {
			application.setEmploymentCheckIndicator(CommonUtils.convertToBoolean(ccApplicationEntity.getCcAppAddInfo().getEmploymentCheckIndicator()));
		}
		if(ccApplicationEntity.getCcAppAddInfo().getEssentialDataIndicator()==null){
			application.setEssentialDataPreFilledIndicator(true);
		}   else {
			application.setEssentialDataPreFilledIndicator(CommonUtils.convertToBoolean(ccApplicationEntity.getCcAppAddInfo().getEssentialDataIndicator()));
		}
		if(ccApplicationEntity.getCcAppAddInfo().getIncomeAutomatedCalcIndicator()==null ){
			application.setIncomeAutomatedCalcIndicator(false);
		}
		else {
			application.setIncomeAutomatedCalcIndicator(CommonUtils.convertToBoolean(ccApplicationEntity.getCcAppAddInfo().getIncomeAutomatedCalcIndicator()));
		}
		// application.setSupplementaryApplicant(supplementaryApplicant);
		// application.setStaffDetails(staffDetails);
		return application;
	}

	public static ApplicantAddressEntity populateApplicantAddressEntity(ApplicantAddressEntity applicantAddressEntity,
			Address address) {
		ApplicantAddressPK applicantAddressPK = new ApplicantAddressPK();
		applicantAddressEntity.setPrimaryKey(applicantAddressPK);
		// applicantAddressEntity.setType(address.getAddressType().getId());
		applicantAddressEntity.setCreatedBy(UserUtil.getUser());
		applicantAddressEntity.setCreatedOn(LocalDateTime.now());
		applicantAddressEntity.setLastModifiedBy(UserUtil.getUser());
		applicantAddressEntity.setLastModifiedOn(LocalDateTime.now());
		return applicantAddressEntity;
	}

	public static ApplicationStatusTimelineEntity populateApplicationStatusTimelineEntity() {
		ApplicationStatusTimelineEntity applicationStatusTimelineEntity = new ApplicationStatusTimelineEntity();
		// applicationStatusTimelineEntity.setNgstatus(NgStatus.INPROGRESS.getId());
		applicationStatusTimelineEntity.setCreatedBy(UserUtil.getUser());
		applicationStatusTimelineEntity.setCreatedOn(LocalDateTime.now());
		return applicationStatusTimelineEntity;
	}

	public static ARNCcBridgeEntity populateARNCcBridgeEntity(ARNCcBridgeEntity arnCcBridgeEntity,
			CreditCard creditCard) {

		ARNCcBridgePK arnCcBridgePK = arnCcBridgeEntity.getPrimaryKey();
		if (arnCcBridgePK == null)
			arnCcBridgePK = new ARNCcBridgePK();

		arnCcBridgeEntity.setPrimaryKey(arnCcBridgePK);
		arnCcBridgeEntity.setLimit(creditCard.getLimit());
		arnCcBridgeEntity.setCreatedBy(UserUtil.getUser());
		arnCcBridgeEntity.setLastModifiedBy(UserUtil.getUser());
		arnCcBridgeEntity.setCreatedOn(LocalDateTime.now());
		arnCcBridgeEntity.setLastModifiedOn(LocalDateTime.now());

		if (creditCard.getCardDeclaration() != null) {
			arnCcBridgeEntity.setBankToAssignCreditLimit(
					CommonUtils.convertToString(creditCard.getCardDeclaration().isBankToAssignCreditLimit()));
		}

		return arnCcBridgeEntity;
	}

	public static CreditCard transformCreditCardMasterEntityToCreditCard(ARNCcBridgeEntity arnCcBridgeEntity) {
		CreditCardMasterEntity creditCardMasterEntity = arnCcBridgeEntity.getPrimaryKey().getCreditCardMasterByCcId();
		CreditCard creditCard = new CreditCard();
		CardDeclaration cardDeclaration = new CardDeclaration();
		creditCard.setKey(creditCardMasterEntity.getKey());
		creditCard.setName(creditCardMasterEntity.getName());
		creditCard.setImageURL(creditCardMasterEntity.getImageUrl());
		creditCard.setType(creditCardMasterEntity.getType());
		creditCard.setMinSalary(creditCardMasterEntity.getMinSalary());
		creditCard.setRewardPoints(creditCardMasterEntity.getRewardPoints());
		creditCard.setRewardPointScale(creditCardMasterEntity.getRewardPointScale());
		creditCard.setComplimentaryStuff(creditCardMasterEntity.getComplimentaryStuff());
		creditCard.setAnnualFee(creditCardMasterEntity.getAnnualFee());
		creditCard.setAnnualFeeCurrency(creditCardMasterEntity.getAnnualFeeCurrency());
		creditCard.setLimit(arnCcBridgeEntity.getLimit());
		creditCard.setLimitMinimumOfInfo(creditCardMasterEntity.getLimitMinimumOfInfo());
		if (arnCcBridgeEntity.getBankToAssignCreditLimit() != null) {
			cardDeclaration.setBankToAssignCreditLimit(
					CommonUtils.convertToBoolean(arnCcBridgeEntity.getBankToAssignCreditLimit()));
			creditCard.setCardDeclaration(cardDeclaration);
		}
		return creditCard;
	}

	public static Document transformApplicationDocumentEntityToDocument(
			ApplicantDocumentEntity applicationDocumentEntity) {
		Document document = new Document();
		String docGroup = applicationDocumentEntity.getDocumentsMasterByDocumentType().getDocGroupMasterByDocGroup()
				.getName();
		document.setDocGroup(Arrays.asList(DocumentGroup.values()).stream()
				.filter(a -> a.getValue().equalsIgnoreCase(docGroup)).collect(Collectors.toList()).get(0));

		String docName = applicationDocumentEntity.getDocumentsMasterByDocumentType().getKey();
		document.setDocName(Arrays.asList(DocumentName.values()).stream()
				.filter(a -> a.getValue().equalsIgnoreCase(docName)).collect(Collectors.toList()).get(0));
		document.setNumberOfImages(applicationDocumentEntity.getNoOfImages());
		List<Image> images = applicationDocumentEntity.getImageById().stream()
				.map(a -> transformImageEntityToImageExcludingPhoto(a)).collect(Collectors.toList());
		document.setImages(images);
		return document;
	}

	public static Document transformApplicationDocumentEntityToDocumentWithPhoto(
			ApplicantDocumentEntity applicationDocumentEntity) {
		Document document = new Document();
		String docGroup = applicationDocumentEntity.getDocumentsMasterByDocumentType().getDocGroupMasterByDocGroup()
				.getName();
		document.setDocGroup(Arrays.asList(DocumentGroup.values()).stream()
				.filter(a -> a.getValue().equalsIgnoreCase(docGroup)).collect(Collectors.toList()).get(0));

		String docName = applicationDocumentEntity.getDocumentsMasterByDocumentType().getKey();
		document.setDocName(Arrays.asList(DocumentName.values()).stream()
				.filter(a -> a.getValue().equalsIgnoreCase(docName)).collect(Collectors.toList()).get(0));
		document.setNumberOfImages(applicationDocumentEntity.getNoOfImages());
		List<Image> images = applicationDocumentEntity.getImageById().stream().map(a -> transformImageEntityToImage(a))
				.collect(Collectors.toList());
		document.setImages(images);
		return document;
	}

	/**
	 * This Function populates the ApplicationDocumentEntity
	 *
	 * @param documents
	 * @return
	 */
	public static List<ApplicantDocumentEntity> populateApplicationDocumentEntity(List<Document> documents) {
		List<ApplicantDocumentEntity> applicationDocEntity = new ArrayList<>();
		if (documents != null)
			for (Document doc : documents) {
				ApplicantDocumentEntity applicationDocumentEntity = new ApplicantDocumentEntity();
				applicationDocumentEntity.setNoOfImages(doc.getNumberOfImages());
				applicationDocumentEntity.setCreatedBy(UserUtil.getUser());
				applicationDocumentEntity.setCreatedOn(LocalDateTime.now());
				applicationDocumentEntity.setLastModifiedBy(UserUtil.getUser());
				applicationDocumentEntity.setLastModifiedOn(LocalDateTime.now());
				applicationDocumentEntity.setImageById(populateImageEntity(doc));
				applicationDocumentEntity.setDocumentExtractionEntity(populateDocumentExtractionEntity(documents));
				applicationDocEntity.add(applicationDocumentEntity);
			}
		return applicationDocEntity;
	}

	public static List<DocumentExtractionEntity> populateDocumentExtractionEntity(List<Document> documents) {
		List<DocumentExtractionEntity> docExtractionEntityList = new ArrayList<>();
		DocumentExtractionEntity documentExtractionEntity;
		if (documents != null)
			for (Document doc : documents) {
				for (Image image : doc.getImages()) {
					final List<OcrData> ocrExtracted = image.getOcrExtracted();
					if (CollectionUtils.isNotEmpty(ocrExtracted))
					{
						for (OcrData data : image.getOcrExtracted()) {
							documentExtractionEntity = new DocumentExtractionEntity();
							documentExtractionEntity.setFieldName(data.getKey());
							documentExtractionEntity.setExtractedValue(data.getValue());
							documentExtractionEntity.setChangedOn(LocalDateTime.now());
							documentExtractionEntity.setCreatedOn(LocalDateTime.now());
							// documentExtractionEntity.setIsChanged("No");
							documentExtractionEntity.setCreatedBy(UserUtil.getUser());
							docExtractionEntityList.add(documentExtractionEntity);
						}
					}

				}
			}
		return docExtractionEntityList;
	}

	/*
	 * public static OcrData populateOcrData(Item item) { OcrData ocrData = new
	 * OcrData(); String itemValue = item.getValue(); switch
	 * (item.getName().toUpperCase()) {
	 * 
	 * case "IDENTITY CARD NO.": ocrData.setIdentity_card_no(itemValue); break;
	 * case "NAME": ocrData.setName(itemValue); break;
	 * 
	 * case "Race": ocrData.setRace(itemValue); break; case "DATE OF BIRTH":
	 * ocrData.setDate_of_birth(itemValue); break; case "SEX":
	 * ocrData.setSex(itemValue); break; case "COUNTRY OF BIRTH":
	 * ocrData.setCountry_of_birth(itemValue); break; } return null;
	 * 
	 * }
	 */

	public static List<ImageEntity> populateImageEntity(Document document) {
		List<ImageEntity> imageEntities = new ArrayList<>();
		for (int i = 0; i < document.getImages().size(); i++) {
			ImageEntity entity = new ImageEntity();
			entity.setName(document.getImages().get(i).getName());
			entity.setIsExtracted(CommonUtils.convertToString(document.getImages().get(i).isExtracted()));
			entity.setImageIndex(document.getImages().get(i).getIndex());
			entity.setPhoto(document.getImages().get(i).getImage());
			entity.setCreatedBy(UserUtil.getUser());
			entity.setLastModifiedBy(UserUtil.getUser());
			entity.setCreatedOn(LocalDateTime.now());
			entity.setLastModifiedOn(LocalDateTime.now());
			imageEntities.add(entity);
		}
		return imageEntities;
	}

	public static Image transformImageEntityToImage(ImageEntity imageEntity) {
		Image image = new Image();
		image.setName(imageEntity.getName());
		image.setImage(imageEntity.getPhoto());
		image.setIndex(imageEntity.getImageIndex());
		image.setExtracted(CommonUtils.convertToBoolean(imageEntity.getIsExtracted()));
		return image;
	}

	public static Image transformImageEntityToImageExcludingPhoto(ImageEntity imageEntity) {
		Image image = new Image();
		image.setName(imageEntity.getName());
		image.setIndex(imageEntity.getImageIndex());
		image.setExtracted(CommonUtils.convertToBoolean(imageEntity.getIsExtracted()));
		return image;
	}

	public static SuppapplicantEntity populateSuppapplicantEntity(SuppapplicantEntity suppapplicantEntity,
			SuppapplicantPK suppapplicantPK, Applicant applicant) {
		suppapplicantEntity.setPrimaryKey(suppapplicantPK);
		CommonUtils.resolveIfNull(() -> applicant.getAdditionalInfo().getRelationshipWithPrimary().getValue())
				.ifPresent(suppapplicantEntity::setRelationWithPrimaryApplicant);
		suppapplicantEntity.setCreatedBy(UserUtil.getUser());
		suppapplicantEntity.setCreatedOn(LocalDateTime.now());
		suppapplicantEntity.setLastModifiedBy(UserUtil.getUser());
		suppapplicantEntity.setLastModifiedOn(LocalDateTime.now());
		return suppapplicantEntity;
	}

	public static List<ARNCcBridgeEntity> populateBalanceTransfer(List<ARNCcBridgeEntity> arnCcBridgeEntities,
			Applicant applicant) {
		for (ARNCcBridgeEntity arnCcBridgeEntity : arnCcBridgeEntities) {
			CreditCardMasterEntity creditCardMasterByCcId = arnCcBridgeEntity.getPrimaryKey()
					.getCreditCardMasterByCcId();
			if (creditCardMasterByCcId != null && applicant.getBeneficiaryDetails() != null
					&& applicant.getBeneficiaryDetails().getCcKey() != null
					&& applicant.getBeneficiaryDetails().getCcKey().equals(creditCardMasterByCcId.getKey())) {
				arnCcBridgeEntity.setIsSelected(CommonUtils
						.convertToString(applicant.getApplicantDeclarations().getBalanceTransferOnCreditCard()));
				arnCcBridgeEntity
						.setAcTfrBeneficiaryAcName(applicant.getBeneficiaryDetails().getBeneficiaryAccountName());
				arnCcBridgeEntity
						.setAcTfrBeneficiaryAcNumber(applicant.getBeneficiaryDetails().getBeneficiaryAccountNumber());
				CommonUtils.resolveIfNull(() -> applicant.getBeneficiaryDetails().getBeneficiaryBank())
						.ifPresent(x -> arnCcBridgeEntity.setAcTfrBeneficiaryBankName(x.getValue()));
				arnCcBridgeEntity.setAmtToBeTransfered(applicant.getBeneficiaryDetails().getAmountToBeTransferred());
			}
		}
		return arnCcBridgeEntities;
	}

	private static void setApplicantDetailsEntity(ApplicantDetailsEntity applicantDetailsEntity, Applicant applicant) {
		CommonUtils.resolveIfNull(() -> applicant.getSalutation().getValue())
				.ifPresent(applicantDetailsEntity::setSalutation);
		CommonUtils.resolveIfNull(() -> applicant.getEmail())
				.ifPresent(email -> applicantDetailsEntity.setEmail(applicant.getEmail().toLowerCase()));
		applicantDetailsEntity.setMobile(applicant.getMobile());
		applicantDetailsEntity.setDialCodeMobile(applicant.getDialCodeMobile());
		CommonUtils.resolveIfNull(() -> applicant.getName().getFirstName())
				.ifPresent(applicantDetailsEntity::setFirstName);
		CommonUtils.resolveIfNull(() -> applicant.getName().getMiddleName())
				.ifPresent(applicantDetailsEntity::setMiddleName);
		CommonUtils.resolveIfNull(() -> applicant.getName().getLastName())
				.ifPresent(applicantDetailsEntity::setLastName);
		applicantDetailsEntity.setGender(applicant.getGender());
		CommonUtils.resolveIfNull(() -> applicant.getDateOfBirth())
				.ifPresent(dob -> applicantDetailsEntity.setDateOfBirth(CommonUtils.convertStringToLocalDate(dob)));
		CommonUtils.resolveIfNull(() -> applicant.getIdValue())
				.ifPresent(idValue -> applicantDetailsEntity.setIdValue(applicant.getIdValue().toUpperCase()));
		applicantDetailsEntity.setCreatedOn(LocalDateTime.now());
		applicantDetailsEntity.setCreatedBy(UserUtil.getUser());
		applicantDetailsEntity.setLastModifiedBy(UserUtil.getUser());
		applicantDetailsEntity.setLastModifiedOn(LocalDateTime.now());
	}

	private static void setApplicantAdditionalInfo(ApplicantDetailsEntity applicantDetailsEntity,
			ApplicantAdditionalInfo additionalInfo) {
		CommonUtils.resolveIfNull(() -> additionalInfo.getNationality().getValue())
				.ifPresent(applicantDetailsEntity::setNationality);
		applicantDetailsEntity.setOtherName(additionalInfo.getOtherName());
		applicantDetailsEntity.setIdIssueDate(additionalInfo.getIdIssueDate());
		applicantDetailsEntity.setIdExpiryDate(additionalInfo.getIdExpiryDate());
	}

	private static void setApplicantDeclarations(ApplicantDetailsEntity applicantDetailsEntity,
			ApplicantDeclarations applicantDeclarations) {
		applicantDetailsEntity
				.setConsentContactMe(CommonUtils.convertToString(applicantDeclarations.isConsentContactMe()));
		applicantDetailsEntity
				.setConsentPersonalData(CommonUtils.convertToString(applicantDeclarations.isConsentPersonalData()));
		applicantDetailsEntity
				.setConsentMarketing(CommonUtils.convertToString(applicantDeclarations.isConsentMarketing()));
		applicantDetailsEntity
				.setConsentAccountOpening(CommonUtils.convertToString(applicantDeclarations.isConsentAccountOpening()));
		applicantDetailsEntity
				.setSharedCreditLimit(CommonUtils.convertToString(applicantDeclarations.isSharedCreditLimit()));
		applicantDetailsEntity.setBalanceTransferOnCreditCard(
				CommonUtils.convertToString(applicantDeclarations.getBalanceTransferOnCreditCard()));
	}

	public static StaffDetailsEntity populateStaffDetailsEntity(ApplicationStaffDetails applicationStaffDetails,
			StaffDetailsEntity staffDetailsEntity) {

		staffDetailsEntity.setSourceCode(applicationStaffDetails.getSourceCode().getValue());
		staffDetailsEntity.setGhoCode(applicationStaffDetails.getGhoCode().getValue());
		staffDetailsEntity.setMarketSectorCode(applicationStaffDetails.getMarketSectorCode().getValue());
		CommonUtils.resolveIfNull(() -> applicationStaffDetails.getVoucherCode())
				.ifPresent(staffDetailsEntity::setVoucherId);
		CommonUtils.resolveIfNull(() -> applicationStaffDetails.getReferralId())
				.ifPresent(staffDetailsEntity::setReferralId);
		CommonUtils.resolveIfNull(() -> applicationStaffDetails.getSplInstIndicator().getValue())
				.ifPresent(staffDetailsEntity::setSplInstIndicator);
		CommonUtils.resolveIfNull(() -> applicationStaffDetails.getPreApprovedLimit())
				.ifPresent(staffDetailsEntity::setPreApprovedLimit);
		CommonUtils.resolveIfNull(() -> applicationStaffDetails.getLocation())
				.ifPresent(staffDetailsEntity::setLocation);
		CommonUtils.resolveIfNull(() -> CommonUtils.convertToString(applicationStaffDetails.getEcpfAuthorized()))
				.ifPresent(staffDetailsEntity::setEcpfAuthorized);

		return staffDetailsEntity;
	}

	public static ApplicationStaffDetails transformStaffDetails(StaffDetailsEntity staffDetailsEntity) {
		ApplicationStaffDetails applicationStaffDetails = new ApplicationStaffDetails();

		applicationStaffDetails.setSourceCode(staffDetailsEntity.getSourceCode() == null ? null
				: Arrays.asList(SourceCode.values()).stream()
						.filter(a -> a.getValue().equalsIgnoreCase(staffDetailsEntity.getSourceCode()))
						.collect(Collectors.toList()).get(0));

		applicationStaffDetails.setGhoCode(staffDetailsEntity.getGhoCode() == null ? null
				: Arrays.asList(GhoClassification.values()).stream()
						.filter(a -> a.getValue().equalsIgnoreCase(staffDetailsEntity.getGhoCode()))
						.collect(Collectors.toList()).get(0));

		applicationStaffDetails.setMarketSectorCode(staffDetailsEntity.getMarketSectorCode() == null ? null
				: Arrays.asList(MarketSectorCode.values()).stream()
						.filter(a -> a.getValue().equalsIgnoreCase(staffDetailsEntity.getMarketSectorCode()))
						.collect(Collectors.toList()).get(0));

		applicationStaffDetails.setSplInstIndicator(staffDetailsEntity.getSplInstIndicator() == null ? null
				: Arrays.asList(SpecialInstructionIndicator.values()).stream()
						.filter(a -> a.getValue().equalsIgnoreCase(staffDetailsEntity.getSplInstIndicator()))
						.collect(Collectors.toList()).get(0));
		
		applicationStaffDetails
				.setVoucherCode((staffDetailsEntity.getVoucherId() == null) ? null : staffDetailsEntity.getVoucherId());
		applicationStaffDetails.setReferralId(
				(staffDetailsEntity.getReferralId() == null) ? null : staffDetailsEntity.getReferralId());
		applicationStaffDetails.setPreApprovedLimit(
				(staffDetailsEntity.getPreApprovedLimit() == null) ? null : staffDetailsEntity.getPreApprovedLimit());
		applicationStaffDetails
				.setLocation((staffDetailsEntity.getLocation() == null) ? null : staffDetailsEntity.getLocation());
		applicationStaffDetails.setEcpfAuthorized((staffDetailsEntity.getEcpfAuthorized() == null) ? null
				: CommonUtils.convertToBoolean(staffDetailsEntity.getEcpfAuthorized()));

		return applicationStaffDetails;
	}

	public static List<CreditCard> transformCreditCardMasterEntityToCreditCard(
			List<CreditCardMasterEntity> creditCardMasterEntities) {
		List<CreditCard> creditCards = new ArrayList<>();
		creditCardMasterEntities.stream().forEach(creditCardMasterEntity -> {
			CreditCard creditCard = new CreditCard();
			creditCard.setAnnualFee(creditCardMasterEntity.getAnnualFee());
			creditCard.setAnnualFeeCurrency(creditCardMasterEntity.getAnnualFeeCurrency());
			creditCard.setComplimentaryStuff(creditCardMasterEntity.getComplimentaryStuff());
			creditCard.setImageURL(creditCardMasterEntity.getImageUrl());
			creditCard.setKey(creditCardMasterEntity.getKey());
			creditCard.setLimit(creditCardMasterEntity.getLimit());
			creditCard.setLimitMinimumOfInfo(creditCardMasterEntity.getLimitMinimumOfInfo());
			creditCard.setMinSalary(creditCardMasterEntity.getMinSalary());
			creditCard.setName(creditCardMasterEntity.getName());
			creditCard.setRewardPoints(creditCardMasterEntity.getRewardPoints());
			creditCard.setRewardPointScale(creditCardMasterEntity.getRewardPointScale());
			creditCard.setType(creditCardMasterEntity.getType());
			creditCard.setCardDeclaration(new CardDeclaration());
			creditCards.add(creditCard);
		});
		return creditCards;
	}

	public static IccmCommunication transformIccmCommunication(IccmCommunicationEntity iccmCommunicationEntity) {
		IccmCommunication iccmCommunication = new IccmCommunication();
		iccmCommunication.setArn(iccmCommunicationEntity.getArn());
		iccmCommunication.setMobile(iccmCommunicationEntity.getMobile());
		iccmCommunication.setEmail(iccmCommunicationEntity.getEmail());
		iccmCommunication.setCommunicationText(iccmCommunicationEntity.getCommunicationText());
		iccmCommunication.setSendWhen(iccmCommunicationEntity.getSendWhen());
		iccmCommunication.setStatus(iccmCommunication.getStatus());
		iccmCommunication.setRetryAttempts(iccmCommunicationEntity.getRetryAttempts());
		iccmCommunication.setPriority(iccmCommunicationEntity.getPriority());
		return iccmCommunication;
	}

	public static Remarks tranformRemarksEntityToRemarks(Remarks remarks, RemarksEntity remarksEntity) {
		remarks.setDateTime(remarksEntity.getDateTime());
		remarks.setRemark(Arrays.asList(remarksEntity.getRemarks()));
		remarks.setStaffId(remarksEntity.getStaffId());
		return remarks;
	}

	public static IpAccessDetailsEntity populateIpAccessDetailsEntity(String ipAddress, Timestamp accessTime,
			String url) {

		IpAccessDetailsEntity ipAccessDetailsEntity = new IpAccessDetailsEntity();

		ipAccessDetailsEntity.setIpAddress(ipAddress);
		ipAccessDetailsEntity.setUrl(url);
		ipAccessDetailsEntity.setAccessTime(accessTime);

		return ipAccessDetailsEntity;
	}

	public static List<IpAccessDetails> transformIpAccessDetailsEntityTOIpAccessDetails(
			List<IpAccessDetailsEntity> ipAccessDetailsEntity) {
		List<IpAccessDetails> ipAccessDetailList = new ArrayList<>();

		IpAccessDetails ipAccessDetails;
		DozerBeanMapper mapper = new DozerBeanMapper();
		for (IpAccessDetailsEntity ipDetails : ipAccessDetailsEntity) {

			ipAccessDetails = mapper.map(ipDetails, IpAccessDetails.class);
			ipAccessDetailList.add(ipAccessDetails);
		}

		return ipAccessDetailList;
	}
}
