package hsbc.aofe.service;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;

import hsbc.aofe.domain.Address;
import hsbc.aofe.domain.ApplicantAdditionalInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonPatch;

import hsbc.aofe.domain.Applicant;
import hsbc.aofe.domain.Application;
import hsbc.aofe.exception.InvalidOperationException;
import hsbc.aofe.exception.InvalidRequestDataException;
import hsbc.aofe.exception.InvalidRequestException;
import hsbc.aofe.repository.common.ApplicantRepositoryService;
import hsbc.aofe.repository.common.ApplicationRepositoryService;
import hsbc.aofe.service.jparepository.CCApplicationRepository;
import hsbc.aofe.validationgroups.BranchJourneyGroup;
import hsbc.aofe.validationgroups.CustomerJourneyGroup;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class ApplicantServiceImpl implements hsbc.aofe.service.ApplicantService {

    private ApplicantRepositoryService applicantRepositoryService;
    private ApplicationRepositoryService applicationRepositoryService;
    private CCApplicationRepository ccApplicationRepository;

    @Autowired
    public ApplicantServiceImpl(ApplicantRepositoryService applicantRepositoryService, ApplicationRepositoryService applicationRepositoryService,
                                CCApplicationRepository ccApplicationRepository) {
        this.applicantRepositoryService = applicantRepositoryService;
        this.applicationRepositoryService = applicationRepositoryService;
        this.ccApplicationRepository = ccApplicationRepository;
    }

    @Override
    public Applicant findApplicantById(Long id) {
        //return applicantDaoManager.manageFindApplicantById(id);
        return null;
    }

    @Override
    public List<Applicant> findAllApplicants() {
        //return applicantDaoManager.manageFindAllApplicants();
        return null;
    }

    @Override
    public Applicant updateApplicant(String arn, Applicant applicant) {
        return applicantRepositoryService.updateApplicant(arn, applicant);
    }

    @Override
    public void deleteApplicantById(Long id) {
        /*applicantRepositoryService.deleteApplicantById(id);*/
    }


    @Override
    public List<Applicant> saveApplicant(List<Applicant> applicants, String arn) {
        log.debug("Supplimentary Applicants: {}", applicants);
        if (isOnlyIdTypeOrIdValueEnteredForSupplementary(applicants)) {
            throw new InvalidOperationException("Please Enter IdType and Idvalue both.");
        }
        return applicantRepositoryService.saveApplicant(applicants, arn);
    }

    private boolean isOnlyIdTypeOrIdValueEnteredForSupplementary(List<Applicant> applicants) {
        for (Applicant applicant : applicants) {
            if ((applicant.getIdDocType() != null && applicant.getIdValue() == null) || (applicant.getIdDocType() == null && applicant.getIdValue() != null)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Applicant savePrimaryApplicant(final String arn, final JsonNode patchToApply, BindingResult bindingResult, String channel) throws IOException, InvalidRequestDataException {

        ObjectMapper mapper = new ObjectMapper();
        final Application applicationByARN = getApplicationByARN(arn);
        final Applicant primaryApplicant = applicationByARN.getPrimaryApplicant();
        ApplicantAdditionalInfo additionalInfo = primaryApplicant.getAdditionalInfo();
        if (additionalInfo != null) {
            if (CollectionUtils.isNotEmpty(additionalInfo.getAddress())) {
                List<Address> sortedAddress = additionalInfo.getAddress().stream()
                        .sorted(Comparator.comparing(a -> a.getId()))
                        .collect(Collectors.toList());
                additionalInfo.setAddress(sortedAddress);
            }
        }

        JsonNode applicantCurrentState = mapper.valueToTree(primaryApplicant);

        final JsonNode modifiedApplicantNode = JsonPatch.apply(patchToApply, applicantCurrentState);
        final Applicant modifiedPrimaryApplicant = mapper.treeToValue(modifiedApplicantNode, Applicant.class);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Object>> violations = null;

        if ("CUSTOMER".equals(channel)) {
            violations = validator.validate(modifiedPrimaryApplicant, BranchJourneyGroup.class, CustomerJourneyGroup.class, Default.class);
        } else if ("BRANCH".equals(channel)) {
            violations = validator.validate(modifiedPrimaryApplicant, BranchJourneyGroup.class, Default.class);
        }

        if (CollectionUtils.isNotEmpty(violations)) {
            for (ConstraintViolation<Object> constraintViolation : violations) {
                String objectName = String.valueOf(constraintViolation.getPropertyPath());
                String fieldName = String.valueOf(constraintViolation.getPropertyPath());
                String defaultMessage = constraintViolation.getMessage();
                if (fieldName.contains("address")) {
                    String addressArray = fieldName.substring(fieldName.indexOf("address"),
                            fieldName.indexOf("address") + 10);
                    int index = Integer
                            .valueOf(addressArray.substring(addressArray.indexOf("[") + 1, addressArray.indexOf("]")));
                    fieldName = modifiedPrimaryApplicant.getAdditionalInfo().getAddress().get(index)
                            .getAddressType().getValue() + "|" + fieldName;
                }
                if (fieldName.contains("cards")) {
                    String cardsArray = fieldName.substring(fieldName.indexOf("cards"), fieldName.indexOf("cards") + 8);
                    int index = Integer
                            .valueOf(cardsArray.substring(cardsArray.indexOf("[") + 1, cardsArray.indexOf("]")));
                    fieldName = modifiedPrimaryApplicant.getCards().get(index).getKey() + "|" + fieldName;
                }
                FieldError fieldError = new FieldError(objectName, fieldName, defaultMessage);
                bindingResult.addError(fieldError);
            }
        }

        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid Request Body", bindingResult);
        }

        Applicant applicantPersisted = applicantRepositoryService.savePrimaryApplicant(arn, modifiedPrimaryApplicant);
        return applicantPersisted;
    }

    @Override
    public Applicant updateBeneficiary(String arn, Applicant applicant) {
        return applicantRepositoryService.updateBeneficiary(arn, applicant);
    }

    private Application getApplicationByARN(String arn) throws InvalidRequestDataException {
        final Application applicationByARN = applicationRepositoryService.findApplicationByARN(arn);
        if (applicationByARN == null)
            throw new InvalidRequestDataException("No Application found for ARN: " + arn);
        return applicationByARN;
    }

    @Override
    public void deleteSuppApplicant(String arn, int index) {
        applicantRepositoryService.deleteSuppApplicant(arn, index);
    }

    @Override
    public void deleteAllSuppApplicants(String arn) {
        applicantRepositoryService.deleteAllSuppApplicants(arn);
    }

}
