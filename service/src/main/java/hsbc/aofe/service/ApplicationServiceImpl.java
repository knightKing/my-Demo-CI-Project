package hsbc.aofe.service;

import com.fasterxml.jackson.databind.JsonNode;
import hsbc.aofe.domain.*;
import hsbc.aofe.exception.ICAPException;
import hsbc.aofe.exception.IccmRequestTimedOutException;
import hsbc.aofe.exception.InvalidRequestDataException;
import hsbc.aofe.exception.OcrException;
import hsbc.aofe.exception.SizeOverflowException;
import hsbc.aofe.iccm.config.ICCMConfigProperties;
import hsbc.aofe.iccm.contract.IccmContractService;
import hsbc.aofe.iccm.service.Notification;
import hsbc.aofe.repository.common.ApplicationRepositoryService;
import hsbc.aofe.service.config.IcapConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@Service
@Slf4j
public class ApplicationServiceImpl implements hsbc.aofe.service.ApplicationService {

    private static final double MAX_UPLOAD_IMAGE_SIZE_IN_MB = 3;
    private boolean SCAN_FILE = true;
    private static final Predicate<Document> IS_IMAGE_PRESENT_PREDICATE = doc -> (isNotEmpty(doc.getImages()));
    private static final Predicate<Image> IS_PHOTO_PRESENT_PREDICATE = image -> (ArrayUtils.isEmpty(image.getImage()));
    private static final String ECPF = "ECPF";
    private boolean isVirusNotPresent=false;
    private static final String SENDTOCUSTOMER = "SENDTOCUSTOMER_";
    private static final String EMAIL = "EMAIL";
    private static final String SMS = "SMS";
    private static final String SUBMITFORREVIEW = "SUBMIT-FOR-REVIEW_";
    private static final String SAVEANDEXIT = "SAVEANDEXIT_";
    
    @Autowired
    private ICCMConfigProperties iccmConfigProperties;
    
    @Autowired
    private IccmContractService iccmContractService;

    @Autowired
    @Qualifier("emailService")
    private Notification emailNotification;
    
    @Autowired
    @Qualifier("smsService")
    private Notification smsNotification;
    
    @Autowired
    private IcapConfiguration icapConfiguration;

    @Autowired
    ApplicationRepositoryService applicationRepoService;

    @Autowired
    private OcrService ocrService;

    @Override
    public List<Application> findAllApplications() {
        return applicationRepoService.findAllApplications();
    }

	@Override
	public Application createApplication(String uid, String channelAuthority, Application application) {
		Application applicationPersisted = applicationRepoService.createApplication(uid, channelAuthority, application);

		if (CollectionUtils.isNotEmpty(applicationPersisted.getPrimaryApplicant().getDocuments())) {
			List<Document> documentsWithImageByteArray = applicationPersisted.getPrimaryApplicant().getDocuments();
			List<Document> documentsExludingImageByteArray = documentsWithImageByteArray.stream()
					.map(document -> excludeImageByteArray(document)).collect(Collectors.toList());
			applicationPersisted.getPrimaryApplicant().setDocuments(documentsExludingImageByteArray);
		}
		return applicationPersisted;

	}

    @Override
    public void deleteApplicationById(String arn) {
        applicationRepoService.deleteApplicationById(arn);
    }

    @Override
    public Application findApplicationByARN(String arn) {
        return applicationRepoService.findApplicationByARN(arn);
    }

    @Override
    public List<Document> getPrimaryApplicantDocuments(String arn) {
        Application application = applicationRepoService.findApplicationByARN(arn);
        List<Document> documents = application.getPrimaryApplicant().getDocuments();

        if (documents != null) {
            for (Document document : documents) {
                for (Image image : document.getImages())
                    image.setImage(null);
            }
        }
        return documents;

    }

    @Override
    public List<Document> getSuppApplicantDocBySuppId(String arn, long supplimentaryId) {
        return applicationRepoService.getSuppApplicantDocBySuppId(arn, supplimentaryId);

    }

    @Override
	public List<Document> createPrimaryApplDoc(String arn, String documentGroup, List<Document> documents,HttpSession session)
			throws SizeOverflowException, OcrException, IOException, ICAPException {
    	isVirusNotPresent=checkForIcapResponse(documents);
		if(isVirusNotPresent){
			// Validate image size is less than maximum allowed size
			validateDocumentSize(documents);
			OcrProcessedException ocrException = scanImageForOcr(documents, arn,session);
			log.debug("OcrException is "+ocrException.getException());
			//log.debug("At createPrimaryApplDoc the ocrExtracted data is " +documents);
			applicationRepoService.createPrimaryApplDoc(arn, documentGroup, documents);
			if (ocrException.getException() != null) {
				throw ocrException.getException();
			}
			return setDocImageNull(documents);
		}
		log.debug("Size of ocrExtracted is after extraction is " + documents.get(0).getImages().get(0).getOcrExtracted().size());
		return setDocImageNull(documents);
		
	}

	private boolean checkForIcapResponse(List<Document> documents) throws IOException, ICAPException {
		if (icapConfiguration.isActivateIcap()) {
			if(!scanImageForVirus(documents)){				
				throw new ICAPException("Virus is present is in the document.Documnet upload Failed");
			}
			
		}
		return true;
	}

	private boolean scanImageForVirus(List<Document> documents) throws IOException, ICAPException {
		ICAP icap = new ICAP(icapConfiguration.getUri(), icapConfiguration.getPort(),
				icapConfiguration.getIcapService());
		for (Document doc : documents) {
			for (Image image : doc.getImages()) {
				return icap.scanFile(image.getImage());
			}
		}
		return false;

	}

	private OcrProcessedException scanImageForOcr(List<Document> documents, String arn, HttpSession session) {
		OcrException ocrException = null;
		List<OcrData> ocrResult=new ArrayList<>();
		OcrProcessedException ocrProcessedException = new OcrProcessedException();
		for (Document doc : documents) {
			String docType = doc.getDocName().getValue();
			switch (docType) {
			case "ECPF":
				try {
					for (Image image : doc.getImages()) {						
						try {
							ocrResult = ocrService.getEcpfOcrdata(image, docType, arn, session);
							log.debug("The ocrExtracted result size is "+ocrResult.size());
							image.setOcrExtracted(ocrResult);
						} catch (OcrException e) {
							ocrException = e;
							ocrProcessedException.setException(ocrException);
						}
					}
				} catch (ParserConfigurationException | SAXException | IOException e) {
					e.printStackTrace();
				}
				break;
				
			}

		}
		return ocrProcessedException;
	}

	private List<Document> setDocImageNull(List<Document> document) {
		for (Document doc : document) {
			for (Image image : doc.getImages()) {
				image.setImage(null);
			}
		}
		return document;
	}

    @Override
    public void deletePrimaryApplDoc(String arn, String documentGroup, long index) {
        applicationRepoService.deletePrimaryApplDoc(arn, documentGroup, index);
    }

    @Override
    public void updateApplicationStatus(String arn, NgStatus status, String uid, String channel) {
        String event;
        List<String> remarks = null;
        try {
            if (NgStatus.PENDINGWITHCUSTOMER.equals(status)) {
                event = SENDTOCUSTOMER;
                iccmContractService.saveIccm(event + EMAIL , arn, channel, uid, remarks);
                iccmContractService.saveIccm(event + SMS , arn, channel, uid, remarks);
            }
            if (NgStatus.PENDINGFORREVIEW.equals(status)) {
                event = SUBMITFORREVIEW;
                iccmContractService.saveIccm(event + EMAIL , arn, channel, uid, remarks);
                iccmContractService.saveIccm(event + SMS , arn, channel, uid, remarks);
            }
        } catch (Exception e) {
        	if ("true".equalsIgnoreCase(iccmConfigProperties.getEnableProduction())) {
				throw new IccmRequestTimedOutException("Iccm Request Timed Out.");
			}
            log.error("ICCM EMAIL/SMS service error for {}.", arn);
        }
        applicationRepoService.updateApplicationStatus(arn, status);
    }
    
    @Override
	public void applicationSaveAndExit(String arn, String uid, String channel) {
		String event = SAVEANDEXIT;
		List<String> remarks = null;
		try {
			iccmContractService.saveIccm(event + EMAIL, arn, channel, uid, remarks);
			iccmContractService.saveIccm(event + SMS, arn, channel, uid, remarks);
		} catch (Exception e) {
			if ("true".equalsIgnoreCase(iccmConfigProperties.getEnableProduction())) {
				throw new IccmRequestTimedOutException("Iccm Request Timed Out.");
			}
			log.error("ICCM EMAIL/SMS service error for {}.", arn);
		}
	}

    @Override
    public void setEssentialDataPreFilledIndicator(boolean essentialDataPreFilledIndicator, String arn) {
        applicationRepoService.setEssentialDataPreFilledIndicator(essentialDataPreFilledIndicator, arn);
    }

    @Override
    public void deleteSupplementaryApplDoc(String arn, long supplementaryId, String documentGroup, long imageIndex) {
        applicationRepoService.deleteSupplementaryApplDoc(arn, supplementaryId, documentGroup, imageIndex);

    }

    @Override
    public Image getPrimaryApplicantImages(String arn, String documentGroup, long index) {
        return applicationRepoService.getImageOfPrimary(arn, documentGroup, index);
    }

    @Override
    public void updateSupplementaryApplDoc(String idOfApplication, long supplementaryId, String documentGroup,
                                           List<Document> documents) throws SizeOverflowException, IOException, ICAPException {
    	isVirusNotPresent=checkForIcapResponse(documents);
		if(isVirusNotPresent){
        // Validate image size is less than maximum allowed size
        validateDocumentSize(documents);
        applicationRepoService.updateSupplementaryApplDoc(idOfApplication, supplementaryId, documentGroup, documents);
		}
    }

    @Override
    public Application updateApplication(String arn, JsonNode patchToApply, BindingResult bindingResult, String channel)
            throws IOException, InvalidRequestDataException {
        return applicationRepoService.updateApplication(arn, patchToApply, bindingResult, channel);
    }

    @Override
    public void saveStaffDetails(String arn, ApplicationStaffDetails staffDetails) {

        applicationRepoService.saveStaffDetails(arn, staffDetails);

    }

    @Override
    public Image getSuppApplicantImageById(String arn, long supplimentaryId, long index, String documentGroup) {
        return applicationRepoService.getImageOfSuppl(arn, supplimentaryId, index, documentGroup);

    }

    private void validateDocumentSize(List<Document> documents) {
        documents.stream().filter(IS_IMAGE_PRESENT_PREDICATE)
                .flatMap(document -> document.getImages().stream().filter(IS_PHOTO_PRESENT_PREDICATE))
                .forEach(ApplicationServiceImpl::maximumAllowedImageSizeCheck);
    }

    private static void maximumAllowedImageSizeCheck(Image image) throws SizeOverflowException {
        byte[] photo = image.getImage();
        double sizeOfImageInMB = Double.valueOf(photo.length) / (1024 * 1024); // (1024
        // *
        // 1024)
        // =>
        // bytes
        // to
        // mb
        if (sizeOfImageInMB > MAX_UPLOAD_IMAGE_SIZE_IN_MB) {
            throw new SizeOverflowException("Attached File Size is greated than 3MB");
        }
    }

	private Document excludeImageByteArray(Document document) {
		List<Image> documentImages = document.getImages();
		if (CollectionUtils.isNotEmpty(documentImages))
			for (Image documentImage : documentImages) {
				documentImage.setImage(null);
			}
		return document;
	}


    @Override
    public String getApplicationStatus(String idType, String idValue) {
        DocumentName docName = DocumentName.valueOf(idType);

        return applicationRepoService.getApplicationStatus((int) docName.getId(), idValue);
    }

	

}
