package hsbc.aofe.service;

import hsbc.aofe.domain.OCRContentResource;
import hsbc.aofe.domain.VerifiedIncomeCalcParameters;
import hsbc.aofe.exception.OcrException;
import hsbc.aofe.service.validator.ECPFValidator;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Triplet;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiFunction;

/*****************************************
 * Created by NEWGEN SOFTWARE LTD.
 *****************************************
 */
@Slf4j
public class CalculateIncome {

    public static final String EMPLOYEMENT_STATUS = "FULL-TIME";
    public static final String NATIONALITY = "Singapore";

    private static final BiFunction<Map.Entry<LocalDate, Triplet<LocalDate, BigDecimal, String>>, BigDecimal, BigDecimal> MONTHLY_INCOME_FUNCTION = (
            record, applicantContributionRate) -> calulateCurrentMonthIncome(record.getValue().getValue1(),
            applicantContributionRate);

    public static String getLatestEmployerContribution(Map<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedTransformData) {
        TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = (TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>>) extractedTransformData;
        String latestEmployerContribution = extractedData.firstEntry().getValue().getValue2();
        return latestEmployerContribution;
    }

    public BigDecimal calculateVerifiedIncome(
            Map<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedTransformData, String CPF_NUMBER,
            VerifiedIncomeCalcParameters verIncomeCalc) {

        CalculateIncome calculateIncome = new CalculateIncome();
        // extracted data from IPS service.
        TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = (TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>>) extractedTransformData;
        OCRContentResource ocrContentResource = calculateIncome.buildOCRContentResource(extractedData, CPF_NUMBER,
                verIncomeCalc);

        ECPFValidator ecpfValidator = new ECPFValidator();
        final boolean result = ecpfValidator.test(ocrContentResource);
        if (!result) {
            throw new OcrException("CPF criteria are not met and income is not calculated.However,please proceed with the submission.And at the back end VEIO will be triggered in this case");
        }
        final BigDecimal calculateAnnualIncome = calculateAnnualIncome(ocrContentResource, ecpfValidator);
        log.debug("CalculatedAnnualIncome in bigDecimal is "+calculateAnnualIncome);
        final int i = calculateAnnualIncome.compareTo(BigDecimal.valueOf(25000));
        if (i == 1) {
            return calculateAnnualIncome;
        } else {
            throw new OcrException("CPF criteria are not met and income is not calculated.However,please proceed with the submission.And at the back end VEIO will be triggered in this case");
        }
    }

    public <T> OCRContentResource buildOCRContentResource(T contentResource, final String cpfAccountNumber,
                                                          VerifiedIncomeCalcParameters verIncomeCalc) {

        return OCRContentResource.builder().dateOfBirth(verIncomeCalc.getDateOfBirth())
                .employmentStatus(verIncomeCalc.getEmploymenttype()).extractedData(contentResource)
                .nationality(verIncomeCalc.getNationality()).NRIC_number(verIncomeCalc.getIdValue())
                .NRIC_issueDate(verIncomeCalc.getDateOfIssue()).CPF_Account_Number(cpfAccountNumber).build();
    }

    private static void checkLatestMonthIncomeLessThen50PercentOfMedian(OCRContentResource ocrContentResource,
                                                                        ECPFValidator ecpfValidator,List<BigDecimal> monthlyIncomeList){
        log.debug("Inside function checkLatestMonthIncomeLessThen50PercentOfMedian");
        log.warn("Inside function checkLatestMonthIncomeLessThen50PercentOfMedian");
        final TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> considered6MonthsData = ecpfValidator
                .getCONSIDERED_MONTHS_DATA();
        final Map.Entry<LocalDate, Triplet<LocalDate, BigDecimal, String>> lastEntry = considered6MonthsData.lastEntry();
        final Period applicantAge = Period.between(ocrContentResource.getDateOfBirth(), LocalDate.now());
        log.debug("Applicant age is "+applicantAge.getYears());
        final BigDecimal applicantContributionRate = getApplicantContributionRate(applicantAge);
        BigDecimal accumulatedMonthlyIncome = new BigDecimal("0.00");
        final BigDecimal latestMonthIncome = calulateCurrentMonthIncome(lastEntry.getValue().getValue1(),
                applicantContributionRate);
        log.debug("Latest monthly income is "+latestMonthIncome);
        BigDecimal median =getMedianOfMonthlyIncome(monthlyIncomeList);
        log.debug("Median of monthly income is"+median);
        final BigDecimal medianMultiplied = median.multiply(new BigDecimal("0.5"));
        final int compareValue = latestMonthIncome.compareTo(medianMultiplied);
        if(compareValue!=1){
             throw new OcrException("CPF criteria are not met and income is not calculated.However,please proceed with the submission.And at the back end VEIO will be triggered in this case");
        }
    }
    private static BigDecimal calculateAnnualIncome(OCRContentResource ocrContentResource,
                                                    ECPFValidator ecpfValidator) {
        List<BigDecimal> monthlyIncomeList = calculateMontlyIncome(ecpfValidator, ocrContentResource.getDateOfBirth());
        checkLatestMonthIncomeLessThen50PercentOfMedian(ocrContentResource,ecpfValidator,monthlyIncomeList);
        BigDecimal median = getMedianOfMonthlyIncome(monthlyIncomeList);
        if (median != null) {
            BigDecimal applicantAnnualIncome = median.multiply(new BigDecimal("12.00"));
            return applicantAnnualIncome;
        }
        return median;

    }

    private static BigDecimal getMedianOfMonthlyIncome(List<BigDecimal> monthlyIncomeList) {
        int monthlyIncomeListSize = monthlyIncomeList.size();
        int midElementIndex = monthlyIncomeListSize / 2;
        BigDecimal medianIncome = null;
        if (!monthlyIncomeList.isEmpty()) {
            if (monthlyIncomeListSize % 2 == 0) {
                return monthlyIncomeList.get(midElementIndex).add(monthlyIncomeList.get(midElementIndex - 1))
                        .divide(new BigDecimal("2.00"));
            } else {
                return monthlyIncomeList.get(midElementIndex);
            }
        }
        log.debug("Median of Monthly Income is"+medianIncome);
        return medianIncome;
    }

    private static List<BigDecimal> calculateMontlyIncome(ECPFValidator ecpfValidator, LocalDate dateOfBirth) {

        final TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> considered6MonthsData = ecpfValidator
                .getCONSIDERED_MONTHS_DATA();
        final Period applicantAge = Period.between(dateOfBirth, LocalDate.now());
        log.debug("Applicant age is "+applicantAge.getYears());
        final BigDecimal applicantContributionRate = getApplicantContributionRate(applicantAge);
        BigDecimal accumulatedMonthlyIncome = new BigDecimal("0.00");

        final List<BigDecimal> monthlyIncomeList = new ArrayList<>();
        for (Map.Entry<LocalDate, Triplet<LocalDate, BigDecimal, String>> r : considered6MonthsData.entrySet()) {
            BigDecimal apply = MONTHLY_INCOME_FUNCTION.apply(r, applicantContributionRate);
            monthlyIncomeList.add(apply);
        }
        monthlyIncomeList.sort(null);
        return monthlyIncomeList;
    }

    private static final BigDecimal getApplicantContributionRate(Period applicantAge) {
        BigDecimal applicantContributionRate;
        if (applicantAge.getYears() == 55)
            applicantContributionRate = new BigDecimal("26.00");
        else
            applicantContributionRate = new BigDecimal("37.00");
        return applicantContributionRate;
    }

    private static BigDecimal calulateCurrentMonthIncome(BigDecimal curMonthContribution,
                                                         BigDecimal applicantContributionRate) {
        // monthly income = monthly contribution / contribution rate
        return curMonthContribution.multiply(new BigDecimal("100")).divide(applicantContributionRate, 2);
    }

}
