package hsbc.aofe.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.ChannelType;
import hsbc.aofe.domain.Dashboard;
import hsbc.aofe.domain.ExportDocument;
import hsbc.aofe.domain.NgStatus;
import hsbc.aofe.service.facade.DashboardListingDaoManagerImpl;
import hsbc.aofe.service.jparepository.DashboardListingRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreateExcelSheetServiceImpl implements CreateExcelSheetService {

	private DashboardListingRepository dashboardListingRepository;
	private DashboardListingDaoManagerImpl dashboardListingDaoManagerImpl;

	@Autowired
	public CreateExcelSheetServiceImpl(DashboardListingRepository dashboardListingRepository,
			DashboardListingDaoManagerImpl dashboardListingDaoManagerImpl) {
		this.dashboardListingRepository = dashboardListingRepository;
		this.dashboardListingDaoManagerImpl = dashboardListingDaoManagerImpl;
	}

	@Override
	public ExportDocument createCompleteExcelSheet(List<String> sortList, List<String> filterList, String uid,
			String channelAuthority, String userName) throws IOException {
		log.debug("Inside createCompleteExcelSheet method");

		String sortedBy = dashboardListingDaoManagerImpl.sortApplications(sortList);
		String filteredBy = dashboardListingDaoManagerImpl.filterApplications(filterList, uid);

		log.debug(
				"createCompleteExcelSheet : uid = " + uid + " sortedBy = " + sortedBy + " filteredBy = " + filteredBy);

		List<Dashboard> dashboardListTemp = dashboardListingRepository.getApplications(sortedBy, filteredBy, uid, 0,
				10);
		int totalApplications = 0;
		if (CollectionUtils.isNotEmpty(dashboardListTemp)) {
			totalApplications = dashboardListTemp.get(0).getTotalCount().intValue();
		}

		log.debug("createCompleteExcelSheet : totalApplications = " + totalApplications);

		List<Dashboard> dashboardList = dashboardListingRepository.getApplications(sortedBy, filteredBy, uid, 0,
				totalApplications);

		String excelSheetName = generateSheetName(userName);
		log.debug("createCompleteExcelSheet : ExcelSheet name = " + excelSheetName);

		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(excelSheetName + ".xlsx");

		addExcelSheetHeader(xssfWorkbook, xssfSheet, channelAuthority);
		addExcelSheetRows(xssfSheet, dashboardList, channelAuthority);

		File excelFile = new File(excelSheetName + ".xlsx");
		FileOutputStream outputStream = new FileOutputStream(excelFile);
		xssfWorkbook.write(outputStream);

		byte[] excelBytes = new byte[(int) excelFile.length()];
		FileInputStream fis = new FileInputStream(excelFile);
		fis.read(excelBytes);
		fis.close();

		ExportDocument exportDocument = new ExportDocument();
		exportDocument.setDocument(excelBytes);
		exportDocument.setDocName(excelSheetName);
		exportDocument.setDocType("Excel");
		log.debug("createCompleteExcelSheet method finished.");
		return exportDocument;
	}

	private void addExcelSheetRows(XSSFSheet xssfSheet, List<Dashboard> dashboardList, String channelAuthority) {
		log.debug("Inside addExcelSheetRows method");
		log.debug("addExcelSheetRows : Adding " + dashboardList.size() + " rows");

		List<String> channelList = new ArrayList<>();
		channelList.add("BRANCH");
		channelList.add("TELE_SALES");
		channelList.add("ROAD_SHOW");

		int rowCount = 1;
		for (Dashboard dashboard : dashboardList) {
			XSSFRow xssfRow = xssfSheet.createRow(rowCount++);
			int columnCount = 0;
			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getIdValue()));
			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getCustomerName()));
			if (!channelList.contains(channelAuthority)) {
				xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getStaffId()));
			}

			Timestamp applicationCreatedOnTimeStamp = dashboard.getApplicationCreatedOn();
			String foramttedCreatedInDate = null;
			if (applicationCreatedOnTimeStamp != null) {
				Date createdOnDate = new Date();
				createdOnDate.setTime(applicationCreatedOnTimeStamp.getTime());
				foramttedCreatedInDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a").format(createdOnDate);
			}

			xssfRow.createCell(columnCount++)
					.setCellValue(replaceNullWithBlank(String.valueOf(foramttedCreatedInDate)));

			Timestamp aoSubmissionDateTimeStamp = dashboard.getAoSubmissinDate();
			String foramttedAoSubmissionDate = null;
			if (aoSubmissionDateTimeStamp != null) {
				Date aoSubmissionDate = new Date();
				aoSubmissionDate.setTime(aoSubmissionDateTimeStamp.getTime());
				foramttedAoSubmissionDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a").format(aoSubmissionDate);
			}

			xssfRow.createCell(columnCount++)
					.setCellValue(replaceNullWithBlank(String.valueOf(foramttedAoSubmissionDate)));

			List<NgStatus> ngStatusList = Arrays.asList(NgStatus.values()).stream()
					.filter(a -> a.getValue().equalsIgnoreCase(dashboard.getNgStatus())).collect(Collectors.toList());

			String ngStatus = null;
			if (CollectionUtils.isNotEmpty(ngStatusList)) {
				ngStatus = ngStatusList.get(0).getMeaning();
			}

			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(ngStatus));
			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getAoStatus()));
			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getAoRefNo()));
			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getCardName()));
			if (!channelList.contains(channelAuthority)) {
				xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getChannel()));
				xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getFfAgentName()));
			}
			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(dashboard.getEmail()));
			xssfRow.createCell(columnCount++).setCellValue(replaceNullWithBlank(
					"+" + dashboard.getDialCodeMobile() + " " + String.valueOf(dashboard.getMobile())));
		}
		log.debug("addExcelSheetRows method finished");
	}

	private void addExcelSheetHeader(XSSFWorkbook xssfWorkbook, XSSFSheet xssfSheet, String channelAuthority) {
		log.debug("Inside addExcelSheetHeader method");
		List<String> channelList = new ArrayList<>();
		channelList.add(ChannelType.BR.getChannel());
		channelList.add(ChannelType.TS.getChannel());
		channelList.add(ChannelType.S.getChannel());

		XSSFRow header = xssfSheet.createRow(0);
		int columnCount = 0;
		header.createCell(columnCount++).setCellValue("Identification ID");
		header.createCell(columnCount++).setCellValue("Customer Name");
		if (!channelList.contains(channelAuthority)) {
			header.createCell(columnCount++).setCellValue("Staff Id");
		}
		header.createCell(columnCount++).setCellValue("Application Date");
		header.createCell(columnCount++).setCellValue("AO Date");
		header.createCell(columnCount++).setCellValue("Application Status");
		header.createCell(columnCount++).setCellValue("AO Status");
		header.createCell(columnCount++).setCellValue("AO Reference Number");
		header.createCell(columnCount++).setCellValue("Card Applied");
		if (!channelList.contains(channelAuthority)) {
			header.createCell(columnCount++).setCellValue("Channel");
			header.createCell(columnCount++).setCellValue("Fullfilment Agent");
		}
		header.createCell(columnCount++).setCellValue("Email");
		header.createCell(columnCount++).setCellValue("Mobile");
		log.debug("addExcelSheetHeader method finished");
	}

	private String generateSheetName(String userName) {
		log.debug("Inside generateSheetName method");
		LocalDate localDate = LocalDate.now();
		String day = String.valueOf(localDate.getDayOfMonth());
		day = day.length() == 1 ? "0" + day : day;
		String month = String.valueOf(localDate.getMonthValue());
		month = month.length() == 1 ? "0" + month : month;
		String year = String.valueOf(localDate.getYear());
		log.debug("generateSheetName : sheet name = " + userName + "_NGAPPLIST_" + day + month + year);
		log.debug("generateSheetName method finished");
		return userName + "_NGAPPLIST_" + day + month + year;
	}

	private String replaceNullWithBlank(String value) {
		if (value == null || value.equalsIgnoreCase("null")) {
			return "";
		}
		return value;
	}
}
