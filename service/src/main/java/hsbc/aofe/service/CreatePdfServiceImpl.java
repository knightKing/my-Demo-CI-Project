package hsbc.aofe.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.Address;
import hsbc.aofe.domain.AddressType;
import hsbc.aofe.domain.Applicant;
import hsbc.aofe.domain.ApplicantAdditionalInfo;
import hsbc.aofe.domain.Application;
import hsbc.aofe.domain.BankNameForBalanceTransfer;
import hsbc.aofe.domain.BeneficiaryDetails;
import hsbc.aofe.domain.CountryCode;
import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.domain.Document;
import hsbc.aofe.domain.DocumentGroup;
import hsbc.aofe.domain.DocumentName;
import hsbc.aofe.domain.EmployementStatus;
import hsbc.aofe.domain.HomeOwnership;
import hsbc.aofe.domain.Image;
import hsbc.aofe.domain.NatureOfBusiness;
import hsbc.aofe.domain.Occupation;
import hsbc.aofe.domain.RelationshipWithPrimary;
import hsbc.aofe.repository.common.ApplicationRepositoryService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CreatePdfServiceImpl implements CreatePdfService {

	@Autowired
	private ApplicationRepositoryService applicationRepositoryService;

	private final String YEARS = "years";
	private final String MONTHS = "months";
	private final float CONTENT_SIZE = 10;
	private final float HEADER_SIZE = 12;
	private final float LEADING = 1.5f * CONTENT_SIZE;
	private final float MARGIN = 72;
	private final PDFont CONTENT_FONT = PDType1Font.COURIER;
	private final PDFont HEADER_FONT = PDType1Font.COURIER_BOLD;
	private static int lineCount;
	private static float width;

	@Override
	public void createApplicationPdf(String arn) {
		Image applicationPdf = applicationRepositoryService.getImageOfPrimary(arn, DocumentGroup.APPLICATION.getValue(),
				1);
		if (applicationPdf == null) {
			log.debug("Inside createApplicationPdf method with arn : " + arn);
			Application application = applicationRepositoryService.findApplicationByARN(arn);
			log.debug("Inside createApplicationPdf method with application object : " + application);
			File applicationPdfFile = constructPdfForApplication(application);
			Document document = createPDFDocument(applicationPdfFile);
			List<Document> documents = new ArrayList<>();
			documents.add(document);
			applicationRepositoryService.createPrimaryApplDoc(arn, DocumentGroup.APPLICATION.getValue(), documents);
		}
	}

	private Document createPDFDocument(File pdfFile) {
		// converting file to byte array
		byte[] bytesArray = new byte[(int) pdfFile.length()];
		FileInputStream fis;
		try {
			fis = new FileInputStream(pdfFile);
			fis.read(bytesArray);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Image image = new Image();
		image.setName("application.pdf");
		image.setIndex(1);
		image.setExtracted(false);
		image.setImage(bytesArray);
		List<Image> images = new ArrayList<>();
		images.add(image);

		Document document = new Document();
		document.setDocGroup(DocumentGroup.APPLICATION);
		document.setDocName(DocumentName.APPLICATIONPDF);
		document.setNumberOfImages(1);
		document.setImages(images);

		return document;
	}

	private String replaceNullWithBlank(String value) {
		return value == null ? " " : value;
	}

	private Map<String, Long> getYearsAndMonths(Long months) {
		Map<String, Long> yearMonthMap = new HashMap<>();
		yearMonthMap.put(YEARS, months / 12);
		yearMonthMap.put(MONTHS, months % 12);
		return yearMonthMap;
	}

	private File constructPdfForApplication(Application application) {
		File applicationPdf = null;
		try {
			applicationPdf = File.createTempFile("application", ".pdf");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		PDDocument doc = null;
		try {
			doc = new PDDocument();
			PDPage page = new PDPage();
			doc.addPage(page);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);
			PDRectangle mediabox = page.getMediaBox();

			width = mediabox.getWidth() - 2 * MARGIN;
			float startX = mediabox.getLowerLeftX() + MARGIN;
			float startY = mediabox.getUpperRightY() - MARGIN;

			// Adding header for primary card holder basic details
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_SIZE);
			contentStream.newLineAtOffset(startX, startY);
			contentStream.showText("Primary Card Holder Details");
			contentStream.endText();
			lineCount++;
			contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width, startY - lineCount * LEADING);

			// Adding primary card holder details
			List<String> primaryApplicantBasicDetails = setPrimaryApplicantBasicDetails(application);
			contentStream = handleEndOfThePage(primaryApplicantBasicDetails, doc, contentStream, startX, startY);

			// Adding header for primary card holder additional details
			contentStream = handleEndOfThePageForHeader(3, doc, contentStream, startY);
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_SIZE);
			lineCount++;
			contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
			contentStream.showText("Additional Details");
			contentStream.newLineAtOffset(0, -LEADING);
			contentStream.showText("More About Myself");
			contentStream.endText();
			lineCount += 2;
			contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width, startY - lineCount * LEADING);

			// Adding primary card holder additional details
			List<String> primaryApplicantMoreAboutMyselfDetails = setPrimaryApplicantMoreAboutMyselfDetails(
					application);
			contentStream = handleEndOfThePage(primaryApplicantMoreAboutMyselfDetails, doc, contentStream, startX,
					startY);

			// Adding header for primary card holder about my family details
			contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_SIZE);
			contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
			contentStream.newLineAtOffset(0, -LEADING);
			contentStream.showText("About My Family");
			contentStream.endText();
			lineCount += 2;
			contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width, startY - lineCount * LEADING);

			// Adding primary card holder about my family details
			List<String> primaryApplicantAboutMyFamilyDetails = setPrimaryApplicantAboutMyFamilyDetails(application);
			contentStream = handleEndOfThePage(primaryApplicantAboutMyFamilyDetails, doc, contentStream, startX,
					startY);

			// Adding header for primary card holder where I Live details
			contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_SIZE);
			contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
			contentStream.newLineAtOffset(0, -LEADING);
			contentStream.showText("Where I live");
			contentStream.endText();
			lineCount += 2;
			contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width, startY - lineCount * LEADING);

			// Adding primary card holder where I Live details
			List<String> primaryApplicantWhereILive = setPrimaryApplicantWhereILiveDetails(application);
			contentStream = handleEndOfThePage(primaryApplicantWhereILive, doc, contentStream, startX, startY);

			// Adding header for primary card holder about my Job details
			contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_SIZE);
			contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
			contentStream.newLineAtOffset(0, -LEADING);
			contentStream.showText("About My Job");
			contentStream.endText();
			lineCount += 2;
			contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width, startY - lineCount * LEADING);

			// Adding primary card holder about my Job details
			List<String> primaryApplicantAboutMyJob = setPrimaryApplicantAboutMyJobDetails(application);
			contentStream = handleEndOfThePage(primaryApplicantAboutMyJob, doc, contentStream, startX, startY);

			// Adding header for primary card holder My Existing Relationship
			// details
			contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_SIZE);
			contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
			contentStream.newLineAtOffset(0, -LEADING);
			contentStream.showText("My Existing Relationship");
			contentStream.endText();
			lineCount += 2;
			contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width, startY - lineCount * LEADING);

			// Adding primary card holder My Existing Relationship details
			List<String> primaryApplicantMyExistingRelationship = setPrimaryApplicantMyExistingRelationship(
					application);
			contentStream = handleEndOfThePage(primaryApplicantMyExistingRelationship, doc, contentStream, startX,
					startY);

			// Adding header for primary card holder My credit limit details
			contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_SIZE);
			contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
			contentStream.newLineAtOffset(0, -LEADING);
			contentStream.showText("My Credit Limit");
			contentStream.endText();
			lineCount += 2;
			contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width, startY - lineCount * LEADING);

			// Adding primary card holder My credit limit details
			List<String> primaryApplicantMyCreditLimit = setPrimaryApplicantMyCreditLimit(application);
			contentStream = handleEndOfThePage(primaryApplicantMyCreditLimit, doc, contentStream, startX, startY);

			if (application.getPrimaryApplicant().getApplicantDeclarations().getBalanceTransferOnCreditCard() != null
					&& application.getPrimaryApplicant().getApplicantDeclarations().getBalanceTransferOnCreditCard()) {
				// Adding header for primary card holder Balance Transfer
				// Details
				contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
				contentStream.beginText();
				contentStream.setFont(HEADER_FONT, HEADER_SIZE);
				contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
				contentStream.newLineAtOffset(0, -LEADING);
				contentStream.showText("Balance Transfer");
				contentStream.endText();
				lineCount += 2;
				contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width,
						startY - lineCount * LEADING);

				// Adding primary card holder Balance Transfer Details
				List<String> primaryApplicantBalanceTrasfer = setPrimaryApplicantBalanceTransfer(application);
				contentStream = handleEndOfThePage(primaryApplicantBalanceTrasfer, doc, contentStream, startX, startY);
			}

			List<Applicant> supplementaryApplicants = application.getSupplementaryApplicant();
			if (CollectionUtils.isNotEmpty(supplementaryApplicants)) {

				for (int supplementaryApplicantIndex = 0; supplementaryApplicantIndex < supplementaryApplicants
						.size(); supplementaryApplicantIndex++) {

					Applicant supplementaryApplicant = supplementaryApplicants.get(supplementaryApplicantIndex);
					int displayValueOfSupplementaryApplicant = supplementaryApplicantIndex + 1;

					// Adding header for supplementary card holder details
					contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
					contentStream.beginText();
					contentStream.setFont(HEADER_FONT, HEADER_SIZE);
					contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
					contentStream.newLineAtOffset(0, -LEADING);
					contentStream
							.showText("Supplementary Card Holder " + displayValueOfSupplementaryApplicant + " details");
					contentStream.endText();
					lineCount += 2;
					contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width,
							startY - lineCount * LEADING);

					// Adding supplementary card holder details
					List<String> supplementaryCardHolderBasicDetails = setSupplementaryCardHolderBasicDetails(
							supplementaryApplicant);
					contentStream = handleEndOfThePage(supplementaryCardHolderBasicDetails, doc, contentStream, startX,
							startY);

					// Adding header for supplementary card holder more about
					// myself
					// details
					contentStream = handleEndOfThePageForHeader(3, doc, contentStream, startY);
					contentStream.beginText();
					contentStream.setFont(HEADER_FONT, HEADER_SIZE);
					lineCount++;
					contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
					contentStream.showText("Supplementary Card Holder " + displayValueOfSupplementaryApplicant
							+ " additional details");
					contentStream.newLineAtOffset(0, -LEADING);
					contentStream.showText("More About Myself");
					contentStream.endText();
					lineCount += 2;
					contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width,
							startY - lineCount * LEADING);

					// Adding supplementary card holder more about myself
					// details
					List<String> supplementaryCardHolderMoreAboutMyselfDetails = setSupplementaryCardHolderMoreAboutMyselfDetails(
							supplementaryApplicant);
					contentStream = handleEndOfThePage(supplementaryCardHolderMoreAboutMyselfDetails, doc,
							contentStream, startX, startY);

					// Adding header for supplementary card holder where I live
					// details
					contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
					contentStream.beginText();
					contentStream.setFont(HEADER_FONT, HEADER_SIZE);
					contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
					contentStream.newLineAtOffset(0, -LEADING);
					contentStream.showText("Where I live");
					contentStream.endText();
					lineCount += 2;
					contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width,
							startY - lineCount * LEADING);

					// Adding supplementary card holder where I live details
					List<String> supplementaryCardHolderWhereILiveDetails = setSupplementaryCardHolderWhereILiveDetails(
							supplementaryApplicant);
					contentStream = handleEndOfThePage(supplementaryCardHolderWhereILiveDetails, doc, contentStream,
							startX, startY);

					// Adding header for supplementary card holder about my job
					// details
					contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
					contentStream.beginText();
					contentStream.setFont(HEADER_FONT, HEADER_SIZE);
					contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
					contentStream.newLineAtOffset(0, -LEADING);
					contentStream.showText("About My Job");
					contentStream.endText();
					lineCount += 2;
					contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width,
							startY - lineCount * LEADING);

					// Adding supplementary card holder about my job details
					List<String> supplementaryCardHolderAboutMyJobDetails = setSupplementaryApplicantAboutMyJobDetails(
							supplementaryApplicant);
					contentStream = handleEndOfThePage(supplementaryCardHolderAboutMyJobDetails, doc, contentStream,
							startX, startY);

					// Adding header for supplementary card holder my existing
					// relationship details
					contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
					contentStream.beginText();
					contentStream.setFont(HEADER_FONT, HEADER_SIZE);
					contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
					contentStream.newLineAtOffset(0, -LEADING);
					contentStream.showText("My Existing Relationship");
					contentStream.endText();
					lineCount += 2;
					contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width,
							startY - lineCount * LEADING);

					// Adding supplementary card holder my existing relationship
					// details
					List<String> supplementaryCardHolderMyExistingRelationshipDetails = setSupplementaryApplicantMyExistingRelationshipDetails(
							supplementaryApplicant);
					contentStream = handleEndOfThePage(supplementaryCardHolderMyExistingRelationshipDetails, doc,
							contentStream, startX, startY);

					// Adding header for supplementary card holder my credit
					// limit
					// details
					contentStream = handleEndOfThePageForHeader(2, doc, contentStream, startY);
					contentStream.beginText();
					contentStream.setFont(HEADER_FONT, HEADER_SIZE);
					contentStream.newLineAtOffset(startX, startY - lineCount * LEADING);
					contentStream.newLineAtOffset(0, -LEADING);
					contentStream.showText("My Credit Limit");
					contentStream.endText();
					lineCount += 2;
					contentStream.drawLine(startX, startY - lineCount * LEADING, startX + width,
							startY - lineCount * LEADING);

					// Adding supplementary card holder my credit limit details
					List<String> supplementaryCardHolderMyCreditLimitDetails = setSupplementaryApplicantMyCreditLimitDetails(
							supplementaryApplicant);
					contentStream = handleEndOfThePage(supplementaryCardHolderMyCreditLimitDetails, doc, contentStream,
							startX, startY);
				}
			}

			contentStream.close();
			doc.save(applicationPdf);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				try {
					doc.close();
					lineCount = 0;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return applicationPdf;
	}

	private List<String> setSupplementaryApplicantMyCreditLimitDetails(Applicant supplementaryApplicant) {
		List<String> supplementaryApplicantMyCreditDetails = new LinkedList<>();
		supplementaryApplicantMyCreditDetails = aligningStringBasedOnWidth(supplementaryApplicantMyCreditDetails,
				"I agree to the credit limit stated in the application form or prescribed by HSBC (as the case may be) which will be shared between my primary credit card account and all supplementary card accounts : "
						+ "Yes");
		return supplementaryApplicantMyCreditDetails;
	}

	private List<String> setSupplementaryApplicantMyExistingRelationshipDetails(Applicant supplementaryApplicant) {
		List<String> supplementaryApplicantMyExistingRelationshipDetails = new LinkedList<>();

		RelationshipWithPrimary relationshipWithPrimary = supplementaryApplicant.getAdditionalInfo()
				.getRelationshipWithPrimary();
		supplementaryApplicantMyExistingRelationshipDetails = aligningStringBasedOnWidth(
				supplementaryApplicantMyExistingRelationshipDetails,
				"My relationship with main card applicant : " + relationshipWithPrimary.getValue());

		Boolean isHoldingProminentPublicPosition = supplementaryApplicant.getAdditionalInfo()
				.getHoldingPublicPosition();
		if (isHoldingProminentPublicPosition != null) {
			String holdingProminentPublicPosition = isHoldingProminentPublicPosition ? "Yes" : "No";
			supplementaryApplicantMyExistingRelationshipDetails = aligningStringBasedOnWidth(
					supplementaryApplicantMyExistingRelationshipDetails,
					"I am/was holding a prominent public position : " + holdingProminentPublicPosition);

			if (isHoldingProminentPublicPosition) {
				String publicPositionDetails = supplementaryApplicant.getAdditionalInfo().getPublicPositionDetails();
				supplementaryApplicantMyExistingRelationshipDetails = aligningStringBasedOnWidth(
						supplementaryApplicantMyExistingRelationshipDetails,
						"Please provide details : " + replaceNullWithBlank(publicPositionDetails));
			}
		}

		Boolean isAssociateOfPublicPositionHolder = supplementaryApplicant.getAdditionalInfo()
				.getAssociateOfPublicPositionHolder();
		if (isAssociateOfPublicPositionHolder != null) {
			String associateOfPublicPositionHolder = isAssociateOfPublicPositionHolder ? "Yes" : "No";
			supplementaryApplicantMyExistingRelationshipDetails = aligningStringBasedOnWidth(
					supplementaryApplicantMyExistingRelationshipDetails,
					"I am a relative or close associate of someone who is/was (a) holding a promine nt public position and/or (b) a staff/director of HSBC or HSBC Group : "
							+ associateOfPublicPositionHolder);

			if (isAssociateOfPublicPositionHolder) {
				supplementaryApplicantMyExistingRelationshipDetails = aligningStringBasedOnWidth(
						supplementaryApplicantMyExistingRelationshipDetails,
						"Please provide details : " + replaceNullWithBlank(supplementaryApplicant.getAdditionalInfo()
								.getAssociateOfPublicPositionHolderDetails()));
			}
		}

		return supplementaryApplicantMyExistingRelationshipDetails;
	}

	private List<String> setSupplementaryApplicantAboutMyJobDetails(Applicant supplementaryApplicant) {
		List<String> supplementaryApplicantAboutMyJobDetails = new LinkedList<>();

		ApplicantAdditionalInfo supplementaryApplicantAdditionalInfo = supplementaryApplicant.getAdditionalInfo();

		EmployementStatus employmentStatus = supplementaryApplicantAdditionalInfo.getEmploymentType();

		List<EmployementStatus> employmentStatusWithoutAdditionalDetails = new ArrayList<>();
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.STUDENT);
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.HOUSEWIFE);
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.RETIRED);
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.UNEMPLOYED);

		if (employmentStatus != null) {
			supplementaryApplicantAboutMyJobDetails = aligningStringBasedOnWidth(
					supplementaryApplicantAboutMyJobDetails, "Employment Status : " + employmentStatus.getMeaning());

			if (!employmentStatusWithoutAdditionalDetails.contains(employmentStatus)) {
				Occupation occupation = supplementaryApplicantAdditionalInfo.getOccupation();
				if (occupation != null) {
					supplementaryApplicantAboutMyJobDetails = aligningStringBasedOnWidth(
							supplementaryApplicantAboutMyJobDetails, "Occupation : " + occupation.getMeaning());
				}

				supplementaryApplicantAboutMyJobDetails = aligningStringBasedOnWidth(
						supplementaryApplicantAboutMyJobDetails, "Company Name : "
								+ replaceNullWithBlank(supplementaryApplicantAdditionalInfo.getCompanyName()));
			}
		}
		return supplementaryApplicantAboutMyJobDetails;
	}

	private List<String> setSupplementaryCardHolderWhereILiveDetails(Applicant supplementaryApplicant) {
		List<String> supplementaryApplicantWhereILiveDetails = new LinkedList<>();

		boolean isResidentialAddressSameAsPrimary = supplementaryApplicant.getAdditionalInfo()
				.isResidentialAddressSameAsPrimary();
		String residentialAddressSameAsPrimary = isResidentialAddressSameAsPrimary ? "Yes" : "No";
		supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(supplementaryApplicantWhereILiveDetails,
				"Is the residential address same as the primary card applicant's ? : "
						+ residentialAddressSameAsPrimary);

		if (!isResidentialAddressSameAsPrimary) {
			List<Address> supplementaryApplicantAddresses = supplementaryApplicant.getAdditionalInfo().getAddress();
			List<Address> residentialAddressList = supplementaryApplicantAddresses.stream()
					.filter(address -> address.getAddressType().equals(AddressType.RESIDENTIAL))
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(residentialAddressList)) {
				Address residentialAddress = residentialAddressList.get(0);
				supplementaryApplicantWhereILiveDetails.add("Residential Address");
				supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(
						supplementaryApplicantWhereILiveDetails, "Country : " + residentialAddress.getCountry().getMeaning());
				if (CountryCode.SG.equals(residentialAddress.getCountry())) {
					supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(
							supplementaryApplicantWhereILiveDetails,
							"Postal Code : " + replaceNullWithBlank(residentialAddress.getPostalCode()));
					supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(
							supplementaryApplicantWhereILiveDetails,
							"Number | Street Name : " + replaceNullWithBlank(residentialAddress.getLine1Address()));
					supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(
							supplementaryApplicantWhereILiveDetails,
							"Unit Number : " + replaceNullWithBlank(residentialAddress.getLine2Address()));
				} else {
					supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(
							supplementaryApplicantWhereILiveDetails,
							"Address line 1 : " + replaceNullWithBlank(residentialAddress.getLine1Address()));
					supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(
							supplementaryApplicantWhereILiveDetails,
							"Address line 2 : " + replaceNullWithBlank(residentialAddress.getLine2Address()));
					supplementaryApplicantWhereILiveDetails = aligningStringBasedOnWidth(
							supplementaryApplicantWhereILiveDetails,
							"Address line 3 : " + replaceNullWithBlank(residentialAddress.getLine3Address()));
				}
			}
		}
		return supplementaryApplicantWhereILiveDetails;
	}

	private List<String> setSupplementaryCardHolderMoreAboutMyselfDetails(Applicant supplementaryApplicant) {
		List<String> supplementaryApplicantMoreAboutMyselfDetails = new LinkedList<>();

		String dialCode = supplementaryApplicant.getAdditionalInfo().getDialCodeHomePhone();
		Long homePhone = supplementaryApplicant.getAdditionalInfo().getHomePhone();
		String homePhoneWithDialCode = "";

		if (dialCode != null && homePhone != null) {
			homePhoneWithDialCode = "+" + dialCode + " " + homePhone;
		}

		supplementaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(
				supplementaryApplicantMoreAboutMyselfDetails, "Home / office phone number : " + homePhoneWithDialCode);

		CountryCode nationality = supplementaryApplicant.getAdditionalInfo().getNationality();
		if (nationality != null) {
			supplementaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(
					supplementaryApplicantMoreAboutMyselfDetails, "Nationality : " + nationality.getMeaning());
		}

		DocumentName supplementaryApplicantDocType = supplementaryApplicant.getIdDocType();
		if (supplementaryApplicantDocType != null
				&& supplementaryApplicantDocType.equals(DocumentName.EMPLOYMENT_PASS)) {
			supplementaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(
					supplementaryApplicantMoreAboutMyselfDetails,
					"Passport Number : " + supplementaryApplicant.getAdditionalInfo().getPassportNumber());
			CountryCode passportCountry = supplementaryApplicant.getAdditionalInfo().getPassportCountry();
			if (passportCountry != null) {
				supplementaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(
						supplementaryApplicantMoreAboutMyselfDetails,
						"Country of Passport Issuance : " + passportCountry.getMeaning());
			}
		}
		return supplementaryApplicantMoreAboutMyselfDetails;
	}

	private List<String> setSupplementaryCardHolderBasicDetails(Applicant applicant) {
		List<String> supplementaryApplicantBasicDetails = new LinkedList<>();

		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Title : " + applicant.getSalutation());
		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"FirstName : " + replaceNullWithBlank(applicant.getName().getFirstName()));

		if (applicant.getName().getMiddleName() != null) {
			supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
					"MiddleName : " + replaceNullWithBlank(applicant.getName().getMiddleName()));
		}

		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"LastName : " + replaceNullWithBlank(applicant.getName().getLastName()));

		String haveFormerName = applicant.getAdditionalInfo().getOtherName() != null ? "Yes" : "No";
		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Have former/other name : " + haveFormerName);

		if ("Yes".equals(haveFormerName)) {
			supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
					"Former / other name : " + replaceNullWithBlank(applicant.getAdditionalInfo().getOtherName()));
		}

		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Name to appear on card : " + replaceNullWithBlank(applicant.getAdditionalInfo().getNameOnCard()));
		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Gender : " + replaceNullWithBlank(applicant.getGender()));

		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Date of Birth : " + replaceNullWithBlank(applicant.getDateOfBirth()));

		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Identification Type : " + applicant.getIdDocType());

		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Identification Document Number : " + replaceNullWithBlank(applicant.getIdValue()));

		supplementaryApplicantBasicDetails = aligningStringBasedOnWidth(supplementaryApplicantBasicDetails,
				"Mobile phone number : +" + applicant.getDialCodeMobile() + " " + applicant.getMobile());

		return supplementaryApplicantBasicDetails;
	}

	private List<String> setPrimaryApplicantBasicDetails(Application application) {
		List<String> primaryApplicantBasicDetails = new LinkedList<>();

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Title : " + application.getPrimaryApplicant().getSalutation());
		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"FirstName : " + replaceNullWithBlank(application.getPrimaryApplicant().getName().getFirstName()));

		if (application.getPrimaryApplicant().getName().getMiddleName() != null) {
			primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails, "MiddleName : "
					+ replaceNullWithBlank(application.getPrimaryApplicant().getName().getMiddleName()));
		}

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"LastName : " + replaceNullWithBlank(application.getPrimaryApplicant().getName().getLastName()));

		String haveFormerName = application.getPrimaryApplicant().getAdditionalInfo().getOtherName() != null ? "Yes"
				: "No";
		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Have former/other name : " + haveFormerName);

		if ("Yes".equals(haveFormerName)) {
			primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
					"Former / other name : " + replaceNullWithBlank(
							application.getPrimaryApplicant().getAdditionalInfo().getOtherName()));
		}

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Name to appear on card : "
						+ replaceNullWithBlank(application.getPrimaryApplicant().getAdditionalInfo().getNameOnCard()));
		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Gender : " + replaceNullWithBlank(application.getPrimaryApplicant().getGender()));

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Date of Birth : " + replaceNullWithBlank(application.getPrimaryApplicant().getDateOfBirth()));

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Identification Type : " + application.getPrimaryApplicant().getIdDocType());

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Identification Document Number : "
						+ replaceNullWithBlank(application.getPrimaryApplicant().getIdValue()));

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Mobile phone number : +" + application.getPrimaryApplicant().getDialCodeMobile() + " "
						+ application.getPrimaryApplicant().getMobile());

		primaryApplicantBasicDetails = aligningStringBasedOnWidth(primaryApplicantBasicDetails,
				"Personal e-mail address : " + replaceNullWithBlank(application.getPrimaryApplicant().getEmail()));

		return primaryApplicantBasicDetails;
	}

	private List<String> setPrimaryApplicantMoreAboutMyselfDetails(Application application) {
		List<String> primaryApplicantMoreAboutMyselfDetails = new LinkedList<>();

		String dialCode = application.getPrimaryApplicant().getAdditionalInfo().getDialCodeHomePhone();
		Long homePhone = application.getPrimaryApplicant().getAdditionalInfo().getHomePhone();
		String homePhoneWithDialCode = "";
		if (dialCode != null && homePhone != null) {
			homePhoneWithDialCode = "+" + dialCode + " " + homePhone;
		}
		primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
				"Home / office phone number : " + homePhoneWithDialCode);
		primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
				"Education level : "
						+ application.getPrimaryApplicant().getAdditionalInfo().getEducationLevel().getValue());

		primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
				"Country of birth : "
						+ application.getPrimaryApplicant().getAdditionalInfo().getCountryOfBirth().getMeaning());

		CountryCode nationality = application.getPrimaryApplicant().getAdditionalInfo().getNationality();
		if (nationality != null) {
			primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
					"Nationality : " + nationality.getMeaning());
		}
		if (application.getPrimaryApplicant().getAdditionalInfo().isMultipleNationality()) {
			CountryCode nationality2 = application.getPrimaryApplicant().getAdditionalInfo().getNationality2();
			if (nationality2 != null) {
				primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(
						primaryApplicantMoreAboutMyselfDetails, "Nationality 2 : " + nationality2.getMeaning());
			}
			CountryCode nationality3 = application.getPrimaryApplicant().getAdditionalInfo().getNationality3();
			if (nationality3 != null) {
				primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(
						primaryApplicantMoreAboutMyselfDetails, "Nationality 3 : " + nationality3.getMeaning());
			}
		}

		DocumentName primaryApplicantDocType = application.getPrimaryApplicant().getIdDocType();
		if (primaryApplicantDocType != null && primaryApplicantDocType.equals(DocumentName.EMPLOYMENT_PASS)) {
			primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
					"Passport Number : " + application.getPrimaryApplicant().getAdditionalInfo().getPassportNumber());
			CountryCode passportCountry = application.getPrimaryApplicant().getAdditionalInfo().getPassportCountry();
			if (passportCountry != null) {
				primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(
						primaryApplicantMoreAboutMyselfDetails,
						"Country of Passport Issuance : " + passportCountry.getMeaning());
			}

		}

		CountryCode taxResidenceCountry1 = application.getPrimaryApplicant().getAdditionalInfo()
				.getTaxResidenceCountry1();
		if (taxResidenceCountry1 != null) {
			primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
					"Country of tax residence : " + taxResidenceCountry1.getMeaning());
		}
		CountryCode taxResidenceCountry2 = application.getPrimaryApplicant().getAdditionalInfo()
				.getTaxResidenceCountry2();
		if (taxResidenceCountry2 != null) {
			primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
					"Country of tax residence 2 : " + taxResidenceCountry2.getMeaning());
		}
		CountryCode taxResidenceCountry3 = application.getPrimaryApplicant().getAdditionalInfo()
				.getTaxResidenceCountry3();
		if (taxResidenceCountry3 != null) {
			primaryApplicantMoreAboutMyselfDetails = aligningStringBasedOnWidth(primaryApplicantMoreAboutMyselfDetails,
					"Country of tax residence 3 : " + taxResidenceCountry3.getMeaning());
		}

		return primaryApplicantMoreAboutMyselfDetails;
	}

	private List<String> setPrimaryApplicantAboutMyFamilyDetails(Application application) {
		List<String> primaryApplicantAboutMyFamilyDetails = new LinkedList<>();

		primaryApplicantAboutMyFamilyDetails = aligningStringBasedOnWidth(primaryApplicantAboutMyFamilyDetails,
				"Marital Status : "
						+ application.getPrimaryApplicant().getAdditionalInfo().getMaritalStatus().getValue());
		primaryApplicantAboutMyFamilyDetails = aligningStringBasedOnWidth(primaryApplicantAboutMyFamilyDetails,
				"Number of dependent : "
						+ application.getPrimaryApplicant().getAdditionalInfo().getNoOfDependents().getCode());
		primaryApplicantAboutMyFamilyDetails = aligningStringBasedOnWidth(primaryApplicantAboutMyFamilyDetails,
				"Mother's Maiden Name : " + replaceNullWithBlank(
						application.getPrimaryApplicant().getAdditionalInfo().getMothersMaidenName()));

		return primaryApplicantAboutMyFamilyDetails;
	}

	private List<String> setPrimaryApplicantWhereILiveDetails(Application application) {
		List<String> primaryApplicantWhereILive = new LinkedList<>();
		List<Address> primaryApplicantAddresses = application.getPrimaryApplicant().getAdditionalInfo().getAddress();

		List<Address> residentialAddressList = primaryApplicantAddresses.stream()
				.filter(address -> address.getAddressType().equals(AddressType.RESIDENTIAL))
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(residentialAddressList)) {
			Address residentialAddress = residentialAddressList.get(0);
			primaryApplicantWhereILive.add("Residential Address");
			primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
					"Country : " + residentialAddress.getCountry().getMeaning());
			if (CountryCode.SG.equals(residentialAddress.getCountry())) {
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Postal Code : " + replaceNullWithBlank(residentialAddress.getPostalCode()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Number | Street Name : " + replaceNullWithBlank(residentialAddress.getLine1Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Unit Number : " + replaceNullWithBlank(residentialAddress.getLine2Address()));
			} else {
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 1 : " + replaceNullWithBlank(residentialAddress.getLine1Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 2 : " + replaceNullWithBlank(residentialAddress.getLine2Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 3 : " + replaceNullWithBlank(residentialAddress.getLine3Address()));
			}
		}

		HomeOwnership homeOwnership = application.getPrimaryApplicant().getAdditionalInfo().getHomeOwnership();
		if (homeOwnership != null) {
			primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
					"Home Ownership : " + homeOwnership.getMeaning());
		}

		Long monthsAtResidentialAddress = application.getPrimaryApplicant().getAdditionalInfo()
				.getMonthsAtResidentialAddress();
		Map<String, Long> yearMonthMap = getYearsAndMonths(monthsAtResidentialAddress);
		StringBuilder displayYearMonth = new StringBuilder();
		if (yearMonthMap.get(YEARS) <= 1) {
			displayYearMonth.append(yearMonthMap.get(YEARS) + " year ");
		} else {
			displayYearMonth.append(yearMonthMap.get(YEARS) + " years ");
		}
		if (yearMonthMap.get(MONTHS) <= 1) {
			displayYearMonth.append(yearMonthMap.get(MONTHS) + " month");
		} else {
			displayYearMonth.append(yearMonthMap.get(MONTHS) + " months");
		}

		primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
				"Time at Residential Address : " + displayYearMonth);
		List<Address> formerAddressList = primaryApplicantAddresses.stream()
				.filter(address -> address.getAddressType().equals(AddressType.PREVIOUS)).collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(formerAddressList)) {
			Address formerAddress = formerAddressList.get(0);
			primaryApplicantWhereILive.add("Former address");
			primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
					"Country : " + formerAddress.getCountry().getMeaning());
			if (CountryCode.SG.equals(formerAddress.getCountry())) {
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Postal Code : " + replaceNullWithBlank(formerAddress.getPostalCode()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Number | Street Name : " + replaceNullWithBlank(formerAddress.getLine1Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Unit Number : " + replaceNullWithBlank(formerAddress.getLine2Address()));
			} else {
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 1 : " + replaceNullWithBlank(formerAddress.getLine1Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 2 : " + replaceNullWithBlank(formerAddress.getLine2Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 3 : " + replaceNullWithBlank(formerAddress.getLine3Address()));
			}
		}

		boolean isResidentialAddressIdenticalToPermAddress = application.getPrimaryApplicant().getAdditionalInfo()
				.isResidentialAddIdenticalToPermAddress();
		String residentialAddressIdenticalToPermAddress = isResidentialAddressIdenticalToPermAddress ? "Yes" : "No";
		primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
				"Is your residential address same as your correspondence address: "
						+ residentialAddressIdenticalToPermAddress);

		List<Address> permanentAddressList = primaryApplicantAddresses.stream()
				.filter(address -> address.getAddressType().equals(AddressType.PERMANENT)).collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(permanentAddressList)) {
			Address permanentAddress = permanentAddressList.get(0);
			primaryApplicantWhereILive.add("Correspondence address");
			primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
					"Country : " + permanentAddress.getCountry().getMeaning());
			if (CountryCode.SG.equals(permanentAddress.getCountry())) {
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Postal Code : " + replaceNullWithBlank(permanentAddress.getPostalCode()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Number | Street Name : " + replaceNullWithBlank(permanentAddress.getLine1Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Unit Number : " + replaceNullWithBlank(permanentAddress.getLine2Address()));
			} else {
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 1 : " + replaceNullWithBlank(permanentAddress.getLine1Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 2 : " + replaceNullWithBlank(permanentAddress.getLine2Address()));
				primaryApplicantWhereILive = aligningStringBasedOnWidth(primaryApplicantWhereILive,
						"Address line 3 : " + replaceNullWithBlank(permanentAddress.getLine3Address()));
			}
		}

		return primaryApplicantWhereILive;
	}

	private List<String> setPrimaryApplicantAboutMyJobDetails(Application application) {
		List<String> primaryApplicantAboutMyJob = new LinkedList<>();
		ApplicantAdditionalInfo primaryApplicantAdditionalInfo = application.getPrimaryApplicant().getAdditionalInfo();

		EmployementStatus employmentStatus = primaryApplicantAdditionalInfo.getEmploymentType();

		List<EmployementStatus> employmentStatusWithoutAdditionalDetails = new ArrayList<>();
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.STUDENT);
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.HOUSEWIFE);
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.RETIRED);
		employmentStatusWithoutAdditionalDetails.add(EmployementStatus.UNEMPLOYED);

		if (employmentStatus != null) {
			primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
					"Employment Status : " + employmentStatus.getMeaning());

			if (!employmentStatusWithoutAdditionalDetails.contains(employmentStatus)) {
				NatureOfBusiness natureOfBusiness = primaryApplicantAdditionalInfo.getIndustryType();
				if (natureOfBusiness != null) {
					primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
							"Industry type / nature of business : " + natureOfBusiness);
				}

				Occupation occupation = primaryApplicantAdditionalInfo.getOccupation();
				if (occupation != null) {
					primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
							"Occupation : " + occupation.getMeaning());
				}

				primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
						"Position / job title : " + replaceNullWithBlank(primaryApplicantAdditionalInfo.getPosition()));
				Long lengthOfService = primaryApplicantAdditionalInfo.getLengthOfService();

				Map<String, Long> yearMonthMap = getYearsAndMonths(lengthOfService);
				StringBuilder displayYearMonth = new StringBuilder();
				if (yearMonthMap.get(YEARS) <= 1) {
					displayYearMonth.append(yearMonthMap.get(YEARS) + " year ");
				} else {
					displayYearMonth.append(yearMonthMap.get(YEARS) + " years ");
				}
				if (yearMonthMap.get(MONTHS) <= 1) {
					displayYearMonth.append(yearMonthMap.get(MONTHS) + " month");
				} else {
					displayYearMonth.append(yearMonthMap.get(MONTHS) + " months");
				}

				primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
						"Length of service : " + displayYearMonth);

				if (lengthOfService < 24) {
					Long timeAtPreviousEmployer = primaryApplicantAdditionalInfo.getTimeAtPreviousEmployer();
					if (timeAtPreviousEmployer != null) {
						Map<String, Long> yearMonthMapForPreviousEmployer = getYearsAndMonths(timeAtPreviousEmployer);
						StringBuilder displayYearMonthForPreviousEmployer = new StringBuilder();
						if (yearMonthMapForPreviousEmployer.get(YEARS) <= 1) {
							displayYearMonthForPreviousEmployer
									.append(yearMonthMapForPreviousEmployer.get(YEARS) + " year ");
						} else {
							displayYearMonthForPreviousEmployer
									.append(yearMonthMapForPreviousEmployer.get(YEARS) + " years ");
						}
						if (yearMonthMapForPreviousEmployer.get(MONTHS) <= 1) {
							displayYearMonthForPreviousEmployer
									.append(yearMonthMapForPreviousEmployer.get(MONTHS) + " month");
						} else {
							displayYearMonthForPreviousEmployer
									.append(yearMonthMapForPreviousEmployer.get(MONTHS) + " months");
						}
						primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
								"Time at Your Previous Employer : " + displayYearMonthForPreviousEmployer);
					}
				}
				
				if(application.getPrimaryApplicant().getIdDocType().equals(DocumentName.EMPLOYMENT_PASS)){
					Long monthsToEmpPassExpiry = primaryApplicantAdditionalInfo.getMonthsToEmpPassExpiry();
					if (monthsToEmpPassExpiry != null) {
						Map<String, Long> yearMonthMapForEmpPassExpiry = getYearsAndMonths(monthsToEmpPassExpiry);
						StringBuilder displayYearMonthForEmpPassExpiry = new StringBuilder();
						if (yearMonthMapForEmpPassExpiry.get(YEARS) <= 1) {
							displayYearMonthForEmpPassExpiry
									.append(yearMonthMapForEmpPassExpiry.get(YEARS) + " year ");
						} else {
							displayYearMonthForEmpPassExpiry
									.append(yearMonthMapForEmpPassExpiry.get(YEARS) + " years ");
						}
						if (yearMonthMapForEmpPassExpiry.get(MONTHS) <= 1) {
							displayYearMonthForEmpPassExpiry
									.append(yearMonthMapForEmpPassExpiry.get(MONTHS) + " month");
						} else {
							displayYearMonthForEmpPassExpiry
									.append(yearMonthMapForEmpPassExpiry.get(MONTHS) + " months");
						}
						primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
								"Years & Months to Employment Permit Expiry : " + displayYearMonthForEmpPassExpiry);
					}
				}

				primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
						"Annual income : " + primaryApplicantAdditionalInfo.getAnnualIncome());

				// office address
				primaryApplicantAboutMyJob.add("Office Address");
				primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
						"Company Name : " + replaceNullWithBlank(primaryApplicantAdditionalInfo.getCompanyName()));

				List<Address> addresses = primaryApplicantAdditionalInfo.getAddress().stream()
						.filter(address -> address.getAddressType().equals(AddressType.OFFICE))
						.collect(Collectors.toList());

				if (CollectionUtils.isNotEmpty(addresses)) {
					Address officeAddress = addresses.get(0);
					CountryCode officeCountry = officeAddress.getCountry();
					if (officeCountry != null) {
						primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
								"Country : " + officeCountry.getMeaning());
								
						if (CountryCode.SG.equals(officeCountry)) {
							primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
									"Postal Code : " + officeAddress.getPostalCode());
							primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
									"Number | Street Name : " + officeAddress.getLine1Address());
							primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
									"Unit Number : " + officeAddress.getLine2Address());
						} else {
							primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
									"Address line 1 : " + officeAddress.getLine1Address());
							primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
									"Address line 2 : " + officeAddress.getLine2Address());
							primaryApplicantAboutMyJob = aligningStringBasedOnWidth(primaryApplicantAboutMyJob,
									"Address line 3 : " + officeAddress.getLine3Address());
						}
					}
				}
			}
		}
		return primaryApplicantAboutMyJob;
	}

	private List<String> setPrimaryApplicantMyExistingRelationship(Application application) {
		List<String> primaryApplicantMyExistingRelationship = new LinkedList<>();
		Boolean isHoldingProminentPublicPosition = application.getPrimaryApplicant().getAdditionalInfo()
				.getHoldingPublicPosition();
		if (isHoldingProminentPublicPosition != null) {
			String holdingProminentPublicPosition = isHoldingProminentPublicPosition ? "Yes" : "No";
			primaryApplicantMyExistingRelationship = aligningStringBasedOnWidth(primaryApplicantMyExistingRelationship,
					"I am/was holding a prominent public position : " + holdingProminentPublicPosition);
			if (isHoldingProminentPublicPosition) {
				String publicPositionDetails = application.getPrimaryApplicant().getAdditionalInfo()
						.getPublicPositionDetails();
				primaryApplicantMyExistingRelationship = aligningStringBasedOnWidth(
						primaryApplicantMyExistingRelationship,
						"Please provide details : " + replaceNullWithBlank(publicPositionDetails));
			}
		}

		Boolean isAssociateOfPublicPositionHolder = application.getPrimaryApplicant().getAdditionalInfo()
				.getAssociateOfPublicPositionHolder();
		if (isAssociateOfPublicPositionHolder != null) {
			String associateOfPublicPositionHolder = isAssociateOfPublicPositionHolder ? "Yes" : "No";
			primaryApplicantMyExistingRelationship = aligningStringBasedOnWidth(primaryApplicantMyExistingRelationship,
					"I am a relative or close associate of someone who is/was (a) holding a promine nt public position and/or (b) a staff/director of HSBC or HSBC Group : "
							+ associateOfPublicPositionHolder);
			if (isAssociateOfPublicPositionHolder) {
				primaryApplicantMyExistingRelationship = aligningStringBasedOnWidth(
						primaryApplicantMyExistingRelationship,
						"Please provide details : " + replaceNullWithBlank(application.getPrimaryApplicant()
								.getAdditionalInfo().getAssociateOfPublicPositionHolderDetails()));
			}
		}

		boolean isIntroducedByHSBCCardHolder = application.getPrimaryApplicant().getAdditionalInfo()
				.isIntroducedByHSBCCardHolder();
		String introducedByHSBCCardHolder = isIntroducedByHSBCCardHolder ? "Yes" : "No";
		primaryApplicantMyExistingRelationship = aligningStringBasedOnWidth(primaryApplicantMyExistingRelationship,
				"I was referred by an existing HSBC credit cardholder : " + introducedByHSBCCardHolder);
		if (isIntroducedByHSBCCardHolder) {
			primaryApplicantMyExistingRelationship = aligningStringBasedOnWidth(primaryApplicantMyExistingRelationship,
					"Please enter the ID number of your referrer : " + replaceNullWithBlank(application
							.getPrimaryApplicant().getAdditionalInfo().getIntroducedByHSBCCardHolderReferrerID()));
		}
		return primaryApplicantMyExistingRelationship;
	}

	private List<String> setPrimaryApplicantMyCreditLimit(Application application) {
		List<String> primaryApplicantMyCreditLimit = new LinkedList<>();
		primaryApplicantMyCreditLimit.add("Would you like the bank to assign your credit limit ?");
		List<CreditCard> primaryApplicantCards = application.getPrimaryApplicant().getCards();

		for (CreditCard creditCard : primaryApplicantCards) {
			boolean isBankToAssignCreditLimit = creditCard.getCardDeclaration().isBankToAssignCreditLimit();
			String bankToAssignCreditLimit = isBankToAssignCreditLimit ? "Yes" : "No";
			primaryApplicantMyCreditLimit = aligningStringBasedOnWidth(primaryApplicantMyCreditLimit,
					creditCard.getName() + " : " + bankToAssignCreditLimit);
			if (!isBankToAssignCreditLimit) {
				primaryApplicantMyCreditLimit = aligningStringBasedOnWidth(primaryApplicantMyCreditLimit,
						"Preferred credit limit : " + creditCard.getLimit());
			}
		}
		return primaryApplicantMyCreditLimit;
	}

	private List<String> setPrimaryApplicantBalanceTransfer(Application application) {
		BeneficiaryDetails beneficiaryDetails = application.getPrimaryApplicant().getBeneficiaryDetails();
		List<String> primaryApplicantBalancetransfer = new LinkedList<>();

		primaryApplicantBalancetransfer = aligningStringBasedOnWidth(primaryApplicantBalancetransfer,
				"Beneficiary Account Name : " + replaceNullWithBlank(beneficiaryDetails.getBeneficiaryAccountName()));
		
		primaryApplicantBalancetransfer = aligningStringBasedOnWidth(primaryApplicantBalancetransfer,
				"Beneficiary Account Number : " + String.valueOf(beneficiaryDetails.getBeneficiaryAccountNumber()));

		BankNameForBalanceTransfer bankNameForBalanceTransfer = beneficiaryDetails.getBeneficiaryBank();
		primaryApplicantBalancetransfer = aligningStringBasedOnWidth(primaryApplicantBalancetransfer,
				"Beneficiary Bank / Issue Number : " + bankNameForBalanceTransfer.getMeaning());
		primaryApplicantBalancetransfer = aligningStringBasedOnWidth(primaryApplicantBalancetransfer,
				"Amount to be transferred (min S$1,000) : " + beneficiaryDetails.getAmountToBeTransferred());
		return primaryApplicantBalancetransfer;
	}

	private void addLinesToPdf(List<String> lines, PDPageContentStream contentStream, float startX, float startY)
			throws IOException {

		contentStream.beginText();
		contentStream.setFont(CONTENT_FONT, CONTENT_SIZE);
		lineCount++;
		contentStream.newLineAtOffset(0, -lineCount * LEADING);
		contentStream.newLineAtOffset(startX, startY);
		for (String line : lines) {
			contentStream.showText(line);
			contentStream.newLineAtOffset(0, -LEADING);
		}
		contentStream.endText();
	}

	private List<String> aligningStringBasedOnWidth(List<String> lines, String line) {
		int lastSpace = -1;
		while (line.length() > 0) {
			int spaceIndex = line.indexOf(' ', lastSpace + 1);
			if (spaceIndex < 0)
				spaceIndex = line.length();
			String subString = line.substring(0, spaceIndex);
			float size = 0.0f;
			try {
				size = CONTENT_SIZE * CONTENT_FONT.getStringWidth(subString) / 1000;
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (size > width) {
				if (lastSpace < 0)
					lastSpace = spaceIndex;
				subString = line.substring(0, lastSpace);
				float lineWithoutSpaceSize = 0.0f;
				try {
					lineWithoutSpaceSize = CONTENT_SIZE * CONTENT_FONT.getStringWidth(subString) / 1000;
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (lineWithoutSpaceSize > width) {
					Float noOfLinesDecimalValue = lineWithoutSpaceSize / width;
					int noOfLines = noOfLinesDecimalValue.intValue() + 1;
					int index = subString.length() / noOfLines;
					int begin = 0;
					int end = index;
					for (int i = 0; i < noOfLines; i++) {
						String subStringWithoutSpace = subString.substring(begin, end);
						begin = end;
						end += index;
						lines.add(subStringWithoutSpace);
					}
				} else {
					lines.add(subString);
				}
				line = line.substring(lastSpace).trim();
				lastSpace = -1;
			} else if (spaceIndex == line.length()) {
				lines.add(line);
				line = "";
			} else {
				lastSpace = spaceIndex;
			}
		}

		return lines;
	}

	private PDPageContentStream handleEndOfThePage(List<String> lines, PDDocument doc,
			PDPageContentStream contentStream, float startX, float startY) throws IOException {
		float availableSpace = startY - lineCount * LEADING - MARGIN;
		float requiredSpace = lines.size() * LEADING;

		if (availableSpace > 0) {
			if (availableSpace < requiredSpace) {
				PDPage newPage = new PDPage();
				doc.addPage(newPage);
				PDPageContentStream newContentStream = new PDPageContentStream(doc, newPage);
				List<String> previousPageLines = new LinkedList<>();
				List<String> currentPageLines = new LinkedList<>();

				Float temp = availableSpace / LEADING;
				int linesToBeInsertedOnPreviousPage = temp.intValue();
				for (int i = 0; i < linesToBeInsertedOnPreviousPage; i++) {
					previousPageLines.add(lines.get(i));
				}
				for (int i = linesToBeInsertedOnPreviousPage; i < lines.size(); i++) {
					currentPageLines.add(lines.get(i));
				}
				addLinesToPdf(previousPageLines, contentStream, startX, startY);
				contentStream.close();
				lineCount = 0;
				addLinesToPdf(currentPageLines, newContentStream, startX, startY);
				lineCount += currentPageLines.size();
				return newContentStream;
			} else {
				addLinesToPdf(lines, contentStream, startX, startY);
				lineCount += lines.size();
				return contentStream;
			}
		} else {
			PDPage newPage = new PDPage();
			doc.addPage(newPage);
			PDPageContentStream newContentStream = new PDPageContentStream(doc, newPage);
			contentStream.close();
			lineCount = 0;
			return newContentStream;
		}

	}

	private PDPageContentStream handleEndOfThePageForHeader(int noOfLines, PDDocument doc,
			PDPageContentStream contentStream, float startY) throws IOException {
		float availableSpace = startY - lineCount * LEADING - MARGIN;
		float requiredSpace = noOfLines * LEADING;
		if (availableSpace > 0) {
			if (availableSpace < requiredSpace) {
				PDPage newPage = new PDPage();
				doc.addPage(newPage);
				PDPageContentStream newContentStream = new PDPageContentStream(doc, newPage);
				contentStream.close();
				lineCount = 0;
				return newContentStream;
			} else {
				return contentStream;
			}
		} else {
			PDPage newPage = new PDPage();
			doc.addPage(newPage);
			PDPageContentStream newContentStream = new PDPageContentStream(doc, newPage);
			contentStream.close();
			lineCount = 0;
			return newContentStream;
		}

	}

}
