package hsbc.aofe.service;

import java.util.List;

import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.repository.common.CCRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*****************************************
 * Created by sahil.verma on 6/6/2017.
 *****************************************
 */
@Service
public class CreditCardServiceImpl implements CreditCardService {

    CCRepositoryService ccRepositoryService;

    @Autowired
    public CreditCardServiceImpl(CCRepositoryService ccRepositoryService) {
        this.ccRepositoryService = ccRepositoryService;
    }

    /**
     * This will return all the credit card's available in the master-table CREDITCARDMASTER
     */
    @Override
    public List<CreditCard> getAllCreditCards() {
        return ccRepositoryService.getAllCreditCards();
    }

	@Override
	public List<CreditCard> getCardsForDigitalJourney() {
		return ccRepositoryService.getCreditCardsForDigitalJourney();
	}
}
