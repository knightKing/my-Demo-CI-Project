package hsbc.aofe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.DashboardListing;
import hsbc.aofe.domain.Remarks;
import hsbc.aofe.domain.RemarksType;
import hsbc.aofe.exception.IccmRequestTimedOutException;
import hsbc.aofe.iccm.config.ICCMConfigProperties;
import hsbc.aofe.iccm.contract.IccmContractService;
import hsbc.aofe.iccm.service.Notification;
import hsbc.aofe.repository.common.DashboardListingRepositoryService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DashboardListingServiceImpl implements DashboardListingService {

	private static final String SENDTOCUSTOMER = "SENDTOCUSTOMER_";
    private static final String EMAIL = "EMAIL-";
    private static final String SMS= "SMS-";
    private static final String FFAGENT = "FFAGENT"; 
	
    private DashboardListingRepositoryService dashboardListingService;
    
    @Autowired
    private ICCMConfigProperties iccmConfigProperties;

    @Autowired
    private IccmContractService iccmContractService;
    @Autowired
    @Qualifier("emailService")
    private Notification emailNotification;

    @Autowired
    @Qualifier("smsService")
    private Notification smsNotification;

    @Autowired
    public DashboardListingServiceImpl(DashboardListingRepositoryService dashboardListingService) {
        this.dashboardListingService = dashboardListingService;
    }

    @Override
    public DashboardListing findAllApplications(List<String> sort, List<String> filter, String uid, int offset, int pageLimit) {
        return dashboardListingService.findAllApplications(sort, filter, uid, offset, pageLimit);
    }

    @Override
    public String updateFulfilmentAgent(String fulfilmentAgent, String arn) {
        return dashboardListingService.updateFulfilmentAgent(fulfilmentAgent, arn);
    }

    @Override
    public void addRemarks(String arn, Remarks remarks, RemarksType type) {
        dashboardListingService.addRemarks(arn, remarks, type);
    }

    @Override
    public List<Remarks> getRemarksByType(String arn, RemarksType type) {
        return dashboardListingService.getRemarksByType(arn, type);
    }
    
    @Override
    public void callIccm(String arn, RemarksType type, String uid, String channel, List<String> remarks) {
        String event;
        try {
            if (RemarksType.FOR_FA_SEND_TO_CUSTOMER.equals(type)) {
                event = SENDTOCUSTOMER;
                iccmContractService.saveIccm(event + EMAIL + FFAGENT, arn, channel, uid, remarks);
                iccmContractService.saveIccm(event + SMS + FFAGENT, arn, channel, uid, remarks);
            }
        } catch (Exception e) {
        	if ("true".equalsIgnoreCase(iccmConfigProperties.getEnableProduction())) {
				throw new IccmRequestTimedOutException("Iccm Request Timed Out.");
			}
            log.error("ICCM service error for {}.", arn);
        }
    }

}
