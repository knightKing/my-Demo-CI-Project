package hsbc.aofe.service;

import java.sql.Timestamp;
import java.util.List;

import hsbc.aofe.domain.IpAccessDetails;
import hsbc.aofe.repository.common.IpAccessRepositoryService;

public class IpAccessServiceImpl implements IpAccessService {
	
	private IpAccessRepositoryService ipAccessRepositoryService;

	@Override
	public void saveIpAddressDetails(String ipAddress, Timestamp accessTime, String url) {
		
		ipAccessRepositoryService.saveIpAddressDetails(ipAddress, accessTime, url);
		
	}
	
	@Override
	public List<IpAccessDetails> getIpAddressDetails(String ipAddress){
		
		return	ipAccessRepositoryService.getIpAddressDetails(ipAddress);
	}

	
}
