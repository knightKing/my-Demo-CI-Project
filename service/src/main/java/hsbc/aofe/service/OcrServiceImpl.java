package hsbc.aofe.service;

import com.google.common.collect.Multimap;
import hsbc.aofe.domain.*;
import hsbc.aofe.exception.OcrException;
import hsbc.aofe.ocr.extractor.OcrExtractor;
import hsbc.aofe.repository.common.ApplicantRepositoryService;
import hsbc.aofe.repository.common.OcrRepositoryService;
import hsbc.aofe.service.entities.ApplicantAdditionalInfoEntity;
import hsbc.aofe.service.entities.CCApplicationEntity;
import hsbc.aofe.service.jparepository.CCApplicationRepository;
import hsbc.aofe.service.utility.SampleDomParser;
import hsbc.aofe.service.validator.ECPFValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.javatuples.Triplet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class OcrServiceImpl implements OcrService {

    private final OcrExtractor ocrExtractor;
    private final OcrRepositoryService ocrRepositoryService;
    private BigDecimal calcVerIncome;
    private int calculatedVerifiedIncome;
    private TransformedData cpfTransform = null;
    private ApplicantRepositoryService applicantRepoService;
    private CCApplicationRepository ccApplicationRepository;
	private OcrData firstNameData = new OcrData();
	private OcrData middleNameData = new OcrData();
	private OcrData lastNameData = new OcrData();
	private final String BINTI = "BINTI";
	private final String BIN = "BIN";

    @Autowired
    public OcrServiceImpl(final OcrRepositoryService ocrRepositoryService,
                          final ApplicantRepositoryService applicantRepoServiceImpl, OcrExtractor ocrExtractor,CCApplicationRepository ccApplicationRepository) {
        this.ocrRepositoryService = ocrRepositoryService;
        this.applicantRepoService = applicantRepoServiceImpl;
        this.ocrExtractor = ocrExtractor;
        this.ccApplicationRepository=ccApplicationRepository;
    }

    @Override
    public List<OcrData> getEcpfOcrdata(Image image, String docType, String arn, HttpSession currentSession)
            throws ParserConfigurationException, SAXException, IOException {
        log.debug("Inside function getEcpfOcrdata");
        List<OcrData> listOcrData = new ArrayList<>();
        OcrData ocrData = new OcrData();
        ECPFValidator ecpfValidator = new ECPFValidator();
        CalculateIncome calculateIncome = new CalculateIncome();
        Optional<String> extractedData = ocrExtractor.extractFrom(image.getImage(), docType, String.class);
        if (extractedData.isPresent()) {
            image.setExtracted(true);
            log.debug("Extracted data is present now we are");
            String extractedOcrData = extractedData.get();
            if (!extractedData.get().isEmpty()) {
                log.debug("Get verifiedIncomeCalcParameters from database a repo hit is made");
                VerifiedIncomeCalcParameters verifiedIncomeParametersFromDatabase = ocrRepositoryService.getOcrdata(arn);
                log.debug("verifiedIncomeCalcParameters from database ", verifiedIncomeParametersFromDatabase.getEmploymenttype());
                //CpfExtraction cpfExtraction = new CpfExtraction();
                //cpfTransform = cpfExtraction.cpfTransform(extractedOcrData);
                SampleDomParser sampleDomParser=new SampleDomParser();
                cpfTransform=sampleDomParser.populateTransformedData(extractedOcrData);
                if (cpfTransform != null) {
                    log.debug("cpfTransform.getCpfAccountNumber() {},cpfTransform.getExtractedData() are {}", cpfTransform.getCpfAccountNumber(),
                            cpfTransform.getExtractedData());
                    if (cpfTransform.getCpfAccountNumber() != null && cpfTransform.getExtractedData() != null) {
                        log.debug("the if condition if cpfAccountNumber is !=null is passed");
                        return checkEcpfValidationRule(currentSession, listOcrData, ocrData, ecpfValidator, calculateIncome, verifiedIncomeParametersFromDatabase,arn);
                    }
                }
            }
    }
        return listOcrData;
    }

    private List<OcrData> checkEcpfValidationRule(HttpSession currentSession, List<OcrData> listOcrData,
                                                  OcrData ocrData, ECPFValidator ecpfValidator, CalculateIncome calculateIncome,
                                                  VerifiedIncomeCalcParameters verifiedIncomeParametersFromDatabase,String arn) {
        OcrData data=new OcrData();
        boolean isMatched=false;
        log.debug("Inside function checkEcpfValidationRule");
        
        currentSession.setAttribute("transformedData", cpfTransform);

        log.debug("Tranformed extracted data is set in the session");

        TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedDataMap = (TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>>) cpfTransform
                .getExtractedData();
        OCRContentResource<?> ocrContentResource = calculateIncome.buildOCRContentResource(extractedDataMap,
                cpfTransform.getCpfAccountNumber(), verifiedIncomeParametersFromDatabase);

        // applying business rules
        boolean cpfEqualityNric = applyCpfEqualsNricRule(ecpfValidator, calculateIncome,
                verifiedIncomeParametersFromDatabase, extractedDataMap);

        log.debug("cpfEqualityNric validation rule value is" + cpfEqualityNric);

        boolean hasLatestData = applyLatestDataInEcpfRule(ecpfValidator, ocrContentResource);
        log.debug("HasLatestData validation rule value is" + hasLatestData);
        if (!cpfEqualityNric) {
            throw new OcrException(
                    "Attached eCPF document doesn't match with customer's NRIC ID. Please check and retry");
        }
        if (!hasLatestData) {
            throw new OcrException("Attached eCPF is outdated.Please follow up with customer for alternate income document");
        }
        final String latestEmployerContribution = CalculateIncome.getLatestEmployerContribution(cpfTransform.getExtractedData());
        final CCApplicationEntity ccApplicationEntity = ccApplicationRepository.findByArn(arn);
        final ApplicantAdditionalInfoEntity appAddInfo = ccApplicationEntity.getApplicantDetailsByApplicantId().getAppAddInfo();
        if(appAddInfo.getCompanyName().equalsIgnoreCase(latestEmployerContribution)){
            isMatched=true;
        }
        ocrData.setKey("employerContribution");
        ocrData.setValue(latestEmployerContribution);
        data.setKey("isMatched");
        data.setValue(String.valueOf(isMatched));
        log.debug("Ocrd empoloyer contribution value is" + ocrData.getValue());
        listOcrData.add(ocrData);
        listOcrData.add(data);
        return listOcrData;

    }

    private boolean applyLatestDataInEcpfRule(ECPFValidator ecpfValidator, OCRContentResource<?> ocrContentResource) {
        boolean hasLatestData = ecpfValidator.forMonthsHasLatestDataPredicate(ocrContentResource);
        log.debug("Cpf has latestData =" + hasLatestData);
        return hasLatestData;
    }

    private boolean applyCpfEqualsNricRule(ECPFValidator ecpfValidator, CalculateIncome calculateIncome,
                                           VerifiedIncomeCalcParameters verifiedIncomeParametersFromDatabase,
                                           TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedDataMap) {
        boolean cpfEqualityNric = ecpfValidator.checkCPFEqualityWithNRIC(calculateIncome.buildOCRContentResource(
                extractedDataMap, cpfTransform.getCpfAccountNumber(), verifiedIncomeParametersFromDatabase));
        log.debug("Cpf equality with nric value is " + cpfEqualityNric);
        return cpfEqualityNric;
    }

    private void modifyExtractedDataAccToRule(List<Image> images) {
        for (Image image : images) {
            if (image.getOcrExtracted() != null && !image.getOcrExtracted().isEmpty()) {
                List<OcrData> ocrExtracted = image.getOcrExtracted();
                Map<String, String> extractedDataMap = ocrExtracted.stream()
                        .collect(Collectors.toMap(OcrData::getKey, OcrData::getValue, (p1, p2) -> p1 + p2));
                if (extractedDataMap.containsKey("race")) {
                    log.debug("Extracted data have key race");
                    if (extractedDataMap.get("race").equalsIgnoreCase("CHINESE")) {
                        String name = extractedDataMap.get("name");
                        log.debug("Name before swapping is" + name);
                        String[] splitted = extractedDataMap.get("name").split("\\s+");
                        if (splitted.length > 1) {
                            String lastword = splitted[splitted.length - 1];
                            String firstword = splitted[0];
                            splitted[0] = lastword;
                            splitted[splitted.length - 1] = firstword;
                            name = Arrays.toString(splitted).replaceAll("[\\[\\]\\,]", "");
                            String finalName = name;
                            ocrExtracted.forEach(a -> {
                                if (a.getKey().equalsIgnoreCase("name")) {
                                    a.setValue(finalName);
                                }
                            });
                            log.debug("changed name if race is chinese is " + name);
                        }
                    }

                } else if (extractedDataMap.containsKey("address")) {
                    log.debug("Extracted data map have key address");
                    String change = extractedDataMap.get("address");
                    log.debug("address is" + change);
                    Matcher matcher = Pattern.compile("(?<!\\d)\\d{6}(?!\\d)").matcher(change);
                    if (matcher.find()) {
                        OcrData data = new OcrData();
                        data.setKey("postalcode");
                        data.setValue(matcher.group());
                        ocrExtracted.add(data);
                    }
                    String changedAddress = change.replaceAll("(?<!\\d)\\d{6}(?!\\d)", "");
                    ocrExtracted.forEach(a -> {
                        if (a.getKey().equalsIgnoreCase("address")) {
                            a.setValue(changedAddress);
                        }
                    });


                }
            }
        }

    }
    
    private void modifyNameAccordingToRules(List<Image> images) {

    	for (Image image : images) {
    		if (image.getOcrExtracted() != null && !image.getOcrExtracted().isEmpty()) {
    			List<OcrData> ocrExtracted = image.getOcrExtracted();
    			Map<String, String> extractedDataMap = ocrExtracted.stream()
    					.collect(Collectors.toMap(OcrData::getKey, OcrData::getValue, (p1, p2) -> p1 + p2));
    			if (extractedDataMap.containsKey("name")) {
    				final String name = extractedDataMap.get("name");
    				final String[] splitName = name.split(" ");
    				int splitNameLength = splitName.length;    				
					
					if (name.contains("BINTE")||name.contains(BINTI) || name.contains(BIN)) {
    					ifRule3OfNameIsPresent(splitNameLength, splitName, name, ocrExtracted);
    				} else
    					ifRule3OfNameNotPresent(splitNameLength, splitName, name, ocrExtracted);
    			}
    		}
    	}
    }
	
    
	
	private void ifRule3OfNameNotPresent(int length, String[] splitName, String name, List<OcrData> ocrExtracted) {
		if (length > 1) {
			int beginIndex = name.lastIndexOf(splitName[0]);
			int endIndex = name.indexOf(splitName[length - 1]);
			switch (length) {
			case 1:
				firstNameData.setKey("firstName");
				firstNameData.setValue(splitName[0]);
				ocrExtracted.add(firstNameData);
				break;

			case 2:
				firstNameData.setKey("firstName");
				firstNameData.setValue(splitName[0]);
				lastNameData.setKey("lastName");
				lastNameData.setValue(splitName[length - 1]);
				ocrExtracted.add(firstNameData);
				ocrExtracted.add(lastNameData);
				break;

			case 3:
				firstNameData.setKey("firstName");
				firstNameData.setValue(splitName[0]);
				middleNameData.setKey("middleName");
				middleNameData.setValue(splitName[1]);
				lastNameData.setKey("lastName");
				lastNameData.setValue(splitName[length - 1]);
				setOcrExtractedAfterModification(ocrExtracted);
				break;
			default:				
				firstNameData.setKey("firstName");
				firstNameData.setValue(splitName[0]);
				middleNameData.setKey("middleName");
				middleNameData.setValue(name.substring(beginIndex+ splitName[0].length() , endIndex).trim());
				lastNameData.setKey("lastName");
				lastNameData.setValue(splitName[length - 1]);
				setOcrExtractedAfterModification(ocrExtracted);
				break;
			}
		}
	}
	
	private void ifRule3OfNameIsPresent(int length, String[] splitName, String name, List<OcrData> ocrExtracted) {
		int beginIndex = 0;
		int lengthOfNames=0;
		for(String names:splitName){
			if(names.equalsIgnoreCase(BINTI)||names.equalsIgnoreCase(BIN)||names.equalsIgnoreCase("BINTE")){
				middleNameData.setKey("middleName");
				middleNameData.setValue(names);
				beginIndex = name.lastIndexOf(names);	
				lengthOfNames=names.length();
			}
		}		
		firstNameData.setKey("firstName");
		firstNameData.setValue(name.substring(0,beginIndex));
		lastNameData.setKey("lastName");
		lastNameData.setValue(name.substring(lengthOfNames+beginIndex,name.length()).trim());
		setOcrExtractedAfterModification(ocrExtracted);
	}

	private void setOcrExtractedAfterModification(List<OcrData> ocrExtracted) {
		ocrExtracted.add(firstNameData);
		ocrExtracted.add(lastNameData);
		ocrExtracted.add(middleNameData);
	}

    @Override
    public OcrProcessedException saveMatchedEcpfCompanyName(String arn, boolean isMatched, HttpSession session) {
        OcrProcessedException ocrProcessedException = new OcrProcessedException();
        OcrException ocrException = null;
        if (isMatched) {
            log.debug("Employer matched");
            CalculateIncome calculateIncome = new CalculateIncome();
            String cpfAccountNumber = null;
            Map<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = new TreeMap<>();
            VerifiedIncomeCalcParameters verifiedIncomeParametersFromDatabase = ocrRepositoryService.getOcrdata(arn);
            applicantRepoService.updateEmploymentFlag(arn, isMatched);
            log.debug("verifiedIncomeParametersFromDatabase ",
                    verifiedIncomeParametersFromDatabase.getDateOfBirth() + " "
                            + verifiedIncomeParametersFromDatabase.getNationality() + " "
                            + verifiedIncomeParametersFromDatabase.getEmploymenttype());
            if (verifiedIncomeParametersFromDatabase != null) {
                Object sessionAttribute = session.getAttribute("transformedData");
                if (sessionAttribute instanceof TransformedData) {
                    extractedData = ((TransformedData) sessionAttribute).getExtractedData();
                    cpfAccountNumber = ((TransformedData) sessionAttribute).getCpfAccountNumber();
                }
                log.debug("Extracted data from session is" + extractedData + "cpfAccountNumber is" + cpfAccountNumber);
                try {
                    calcVerIncome = calculateIncome.calculateVerifiedIncome(extractedData, cpfAccountNumber,
                            verifiedIncomeParametersFromDatabase);
                    log.debug("Calculated Verified Income in BigDecimal" + calcVerIncome);
                    if (calcVerIncome != null) {
                        calculatedVerifiedIncome = calcVerIncome.intValue();
                        log.debug("Calculated Verified Income in integer" + calcVerIncome);
                        applicantRepoService.saveVerifiedIncome(arn, calculatedVerifiedIncome, true);
                        log.debug("Flags updated EmploymentCheck and verified income saved");
                    }
                } catch (OcrException e) {
                    applicantRepoService.updateIncomeFlag(arn, false);
                    ocrException = e;
                    ocrProcessedException.setException(ocrException);
                }
            }
            return ocrProcessedException;
        }
        applicantRepoService.updateEmploymentFlag(arn, isMatched);
        ocrException=new OcrException("CPF criteria are not met and income is not calculated.However,please proceed with the submission.And at the back end VEIO will be triggered in this case");
        ocrProcessedException.setException(ocrException);
        return ocrProcessedException;
    }

    @Override
    public List<Image> getOcrdData(List<Image> images, String docType) {
        Image nricFront = null;
        Image nricBack = null;
        if (CollectionUtils.isNotEmpty(images) && images.size() == 2) {
            for (Image image : images) {
                if (image.getIndex() == 1) {
                    nricFront = image;
                } else {
                    nricBack = image;
                }
            }
            Optional<Object> extractImages = ocrExtractor.extractImages(nricFront.getImage(),
                    nricBack.getImage(), docType, Object.class);
            if (extractImages.isPresent()) {
                setImageParameters(images);
                Multimap<String, Map<String, String>> multimap = (Multimap) extractImages.get();
                populateNricFrontMap(nricFront, multimap);
                populateNricBackMap(nricBack, multimap);
                modifyExtractedDataAccToRule(images);
                modifyNameAccordingToRules(images);
            }
        }
        return images;
    }

	private void populateNricBackMap(Image nricBack, Multimap<String, Map<String, String>> multimap) {
		if (multimap.asMap().get("2") != null) {
		    Collection<Map<String, String>> nricBackMap = multimap.asMap().get("2");
		    nricBack.setOcrExtracted(populateOcrData(nricBackMap));
		}
	}

	private void populateNricFrontMap(Image nricFront, Multimap<String, Map<String, String>> multimap) {
		if (multimap.asMap().get("1") != null) {
		    Collection<Map<String, String>> nricFrontMap = multimap.asMap().get("1");
		    nricFront.setOcrExtracted(populateOcrData(nricFrontMap));
		}
	}

	private void setImageParameters(List<Image> images) {
		images.forEach(action -> {
		    action.setExtracted(true);
		    action.setImage(null);
		}
		);
	}

    private List<OcrData> populateOcrData(Collection<Map<String, String>> inputMap) {
        List<OcrData> ocrDatas = new ArrayList<>();
        for (Map<String, String> entry : inputMap) {
            OcrData data = null;
            String s = entry.keySet().stream().findFirst().get();
            Supplier<Stream<OcrData>> ocrDataStream = () ->
                    ocrDatas.stream().filter(x -> x.getKey().equals(s));

            boolean isKeyAlreadyExist = ocrDataStream.get().count() == 1;
            if (isKeyAlreadyExist) {
                ocrDataStream.get().forEach(x -> x.setValue(x.getValue() + entry.get(s)));
            } else {
                data = new OcrData();
                data.setKey(s);
                data.setValue(entry.get(s));
                ocrDatas.add(data);
            }
        }
        return ocrDatas;

    }

}
