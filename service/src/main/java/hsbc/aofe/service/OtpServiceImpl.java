package hsbc.aofe.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import hsbc.aofe.domain.NgStatus;
import hsbc.aofe.exception.ApplicationDoesNotExistException;
import hsbc.aofe.exception.IccmRequestTimedOutException;
import hsbc.aofe.iccm.config.ICCMConfigProperties;
import hsbc.aofe.iccm.contract.IccmContractService;
import hsbc.aofe.iccm.service.Notification;
import hsbc.aofe.repository.common.OtpRepositoryService;
import hsbc.aofe.service.entities.ApplicantDetailsEntity;
import hsbc.aofe.service.entities.ApplicationStatusTimelineEntity;
import hsbc.aofe.service.entities.CCApplicationEntity;
import hsbc.aofe.service.facade.ApplicantDaoManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OtpServiceImpl implements OtpService {

    private static final String OTP = "OTP";
    private static final String CUSTOMER = "CUSTOMER";

    private OtpRepositoryService otpRepositoryService;
    private ApplicantDaoManager applicantDaoManager;
    private IccmContractService iccmContractService;
    @Autowired
    private ICCMConfigProperties iccmConfigProperties;

    @Autowired
    @Qualifier("otpService")
    private Notification otpNotification;

    @Autowired
    public OtpServiceImpl(OtpRepositoryService otpRepositoryService,
                          ApplicantDaoManager applicantDaoManager,
                          IccmContractService iccmContractService) {
        this.otpRepositoryService = otpRepositoryService;
        this.applicantDaoManager = applicantDaoManager;
        this.iccmContractService = iccmContractService;
    }

    @Override
    public String otpValidate(String idType, String idValue, String dialCodeMobile, Long mobile, String email, String otp) {
        String arn = getApplicationToResume(idType, idValue, dialCodeMobile, mobile, email);
        return otpRepositoryService.otpValidate(arn, otp);
    }

    @Override
    public String generateOneTimePassword(String idType, String idValue, String dialCodeMobile, Long mobile, String email)
            throws InvalidKeyException, NoSuchAlgorithmException {
        String arn = getApplicationToResume(idType, idValue, dialCodeMobile, mobile, email);
        String otp = otpRepositoryService.generateOneTimePassword(arn);
        try {
        	// remarks : null for OTP
            iccmContractService.saveIccm(OTP, arn, CUSTOMER, otp, null); 
        } catch (Exception e) {
        	if ("true".equalsIgnoreCase(iccmConfigProperties.getEnableProduction())) {
				throw new IccmRequestTimedOutException("Iccm Request Timed Out.");
			}
            log.error("ICCM service error during OTP generation for ARN: {}.", arn);
        }
        return arn;
    }

    @Override
    public String getApplicationToResume(String idType, String idValue, String dialCodeMobile, Long mobile, String email) {
    	
    	final String UniqueExceptionMessage = "We are Unable to Process Your Application. Please Call 1800 HSBC NOW (4722).";
        String lowerCaseEmail = email.toLowerCase();
        String upperCaseIdValue = idValue.toUpperCase();

        List<ApplicantDetailsEntity> applicantDetailsEntityList = applicantDaoManager.findApplicantByIdTypeAndIdValueAndDialCodeMobileAndMobileAndEmail
                (idType, upperCaseIdValue, dialCodeMobile, mobile, lowerCaseEmail);

        if (isApplicantListNullOrEmpty(applicantDetailsEntityList)) {
            throw new ApplicationDoesNotExistException(UniqueExceptionMessage);
        }

        for (ApplicantDetailsEntity applicantDetailsEntity : applicantDetailsEntityList) {
            if (applicantDetailsEntity == null) {
                throw new ApplicationDoesNotExistException(UniqueExceptionMessage);
            }
            List<CCApplicationEntity> ccApplicationEntities = applicantDetailsEntity.getCcapplicationsById();

            if (ccApplicationEntities != null && !ccApplicationEntities.isEmpty()) {
                CCApplicationEntity ccApplicationEntity = ccApplicationEntities.get(0);
                List<ApplicationStatusTimelineEntity> applicationStatusTimelineEntities = ccApplicationEntity.getCcapplicationsStatusById();
                applicationStatusTimelineEntities.sort(Comparator.comparing(ApplicationStatusTimelineEntity::getCreatedOn).reversed());
                if (NgStatus.PENDINGWITHCUSTOMER.getValue().equalsIgnoreCase(applicationStatusTimelineEntities.get(0).getNgStatusMasterByNgStatus().getKey())) {
                    return ccApplicationEntity.getArn();
                }
            }
        }
        throw new ApplicationDoesNotExistException(UniqueExceptionMessage);
    }


    private boolean isApplicantListNullOrEmpty(List<ApplicantDetailsEntity> applicantDetailsEntityList) {
        if (applicantDetailsEntityList == null || applicantDetailsEntityList.isEmpty()) {
            return true;
        }
        return false;
    }

}
