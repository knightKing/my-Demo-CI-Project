package hsbc.aofe.service.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "icap.server-configuration")
public class IcapConfiguration {

	String uri;
	int port;
	String icapService;
	boolean activateIcap;

}
