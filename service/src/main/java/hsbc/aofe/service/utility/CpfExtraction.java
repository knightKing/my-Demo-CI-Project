package hsbc.aofe.service.utility;

import hsbc.aofe.domain.TransformedData;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Triplet;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
public class CpfExtraction extends DefaultHandler {

    private boolean isForMonthNode = false;
    private boolean isPadOnNode = false;
    private boolean isAmountNode = false;
    private boolean isEmployerContributionNode = false;
    private boolean isRNode = false;
    private boolean isRDNode = false;
    private boolean isFDNode = false;
    private boolean isCpfAccNo = false;
    private String numValue = null;
    private String cpf_accountNumber;
    private int count = 0;
    private Map<String, LocalDate> forMonthMap = new TreeMap<>();
    private Map<String, LocalDate> paidOnMap = new HashMap<>();
    private Map<String, BigDecimal> amountMap = new HashMap<>();
    private Map<String, String> employerContributionMap = new HashMap<>();
    private TransformedData data = new TransformedData();

    public TransformedData cpfTransform(String args) throws ParserConfigurationException, SAXException, IOException {
        log.debug("Inside function cpfTransform here xml response is parsed");
        log.error("Inside function cpfTransform here xml response is parsed");
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        byte[] bytes=args.getBytes("UTF-8");
        InputStream is = new ByteArrayInputStream(bytes);
        saxParser.parse(is, this);

        Map<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = new TreeMap<>(Collections.reverseOrder());

        forMonthMap.keySet().forEach(key -> {
            log.debug("formonthmap key is " + forMonthMap.get(key));
            LocalDate forMonthValue = forMonthMap.get(key);
            log.debug("amountMap key is " + amountMap.get(key));
            log.error("amountMap key is " + amountMap.get(key));
            BigDecimal currentAmount = amountMap.get(key);
            Optional<Triplet<LocalDate, BigDecimal, String>> preValue = Optional
                    .ofNullable(extractedData.get(forMonthValue));
            log.error("Extracted data map size is" + extractedData.size());
            log.debug("Extracted data map size is" + extractedData.size());
            boolean isExtractedMapContainsGreaterAmountThenCurrentAmount = extractedData.containsKey(forMonthValue)
                    && preValue.isPresent() && currentAmount.compareTo(preValue.get().getValue1()) == -1;

            if (isExtractedMapContainsGreaterAmountThenCurrentAmount) {
                extractedData.put(forMonthValue, Triplet.with(extractedData.get(forMonthValue).getValue0(),
                        preValue.get().getValue1(), extractedData.get(forMonthValue).getValue2()));

            } else {
                extractedData.put(forMonthMap.get(key),
                        Triplet.with(paidOnMap.get(key), amountMap.get(key), employerContributionMap.get(key)));
            }
        });
        data.setExtractedData(extractedData);
        data.setCpfAccountNumber(cpf_accountNumber);
        log.debug("TransformedData is"+data.getCpfAccountNumber()+" Size of extracted data after transformation is "+data.getExtractedData().size());
        log.error("TransformedData is"+data.getCpfAccountNumber()+" Size of extracted data after transformation is "+data.getExtractedData().size());
        return data;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "F":
                switch (attributes.getValue("FN").toUpperCase()) {
                    case "FOR MONTH":
                        isForMonthNode = true;
                        break;
                    case "PAID ON":
                        isPadOnNode = true;
                        break;
                    case "AMOUNT":
                        isAmountNode = true;
                        break;
                    case "EMPLOYER CONTRIBUTION":
                        isEmployerContributionNode = true;
                        break;
                    case "CPF ACCOUNT NUMBER":
                        isCpfAccNo = true;
                        break;
                    default:
                        break;
                }
                break;
            case "R":
                isRNode = true;
                numValue = attributes.getValue("NUM");
                break;
            case "RD":
                isRDNode = true;
                break;
            case "FD":
                isFDNode = true;
                break;
            default:
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        switch (qName) {
            case "RD":
                isRDNode = false;
                break;
            case "R":
                isRNode = false;
                break;
            case "F":
                isForMonthNode = false;
                isPadOnNode = false;
                isAmountNode = false;
                isEmployerContributionNode = false;
                break;
            case "FD":
                isFDNode = false;
                break;
            default:
                break;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
       /* String str="";
        for(char character:ch){
                   str+=character;
        }*/
       /* log.debug("Character array is +"+str);
        log.error("Character array is +"+str);*/
        log.debug("Start value is " +start);
        log.error("Start value is " +start);
        log.debug("length value is " +length);
        log.error("length value is " +length);
        String valueFetchedFromXml = new String(ch, start, length);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        log.debug("size is of valueFetchedFromXml is " + valueFetchedFromXml.length());
        log.debug("size is of valueFetchedFromXml is ", valueFetchedFromXml.length());
        if (isForMonthNode && isRNode && isRDNode) {
            StringBuilder forMonthValueWithDateAdded = new StringBuilder("01 " + valueFetchedFromXml.substring(0, 1)
                    + valueFetchedFromXml.substring(1, valueFetchedFromXml.length()).toLowerCase());
            log.debug("Dates in case of for Month when parsed with dateFormatter is  "+LocalDate.parse(forMonthValueWithDateAdded.toString(), formatter));
            log.error("Dates in case of for Month when parsed with dateFormatter is  "+LocalDate.parse(forMonthValueWithDateAdded.toString(), formatter));
            forMonthMap.put(numValue, LocalDate.parse(forMonthValueWithDateAdded.toString(), formatter));
            log.error("formonthMap size in characters function is " + forMonthMap.size());
            log.debug("formonthmap size in characters function is " + forMonthMap.size());
        }
        if (isPadOnNode && isRNode && isRDNode) {
            log.debug("Dates in case of Paid On is "+valueFetchedFromXml);
            log.error("Dates in case of Paid On is "+valueFetchedFromXml);
            paidOnMap.put(numValue, LocalDate.parse(valueFetchedFromXml, formatter));
            log.debug("Dates in case of Paid On when parsed with dateFormatter is  "+LocalDate.parse(valueFetchedFromXml, formatter));
            log.error("Dates in case of Paid On when parsed with dateFormatter is  "+LocalDate.parse(valueFetchedFromXml, formatter));
        }
        if (isAmountNode && isRNode && isRDNode) {
            BigDecimal amountValueInBigDecimal = new BigDecimal(valueFetchedFromXml.replace(",", ""));
            amountMap.put(numValue, amountValueInBigDecimal);
        }
        if (isEmployerContributionNode && isRNode && isRDNode) {
            employerContributionMap.put(numValue, valueFetchedFromXml);
        }
        if (isCpfAccNo && isFDNode && count <= 0) {
            log.debug("Inside calculation of cpfAccountNumber"+count);
            log.error("Inside calculation of cpfAccountNumber"+count);
            cpf_accountNumber = valueFetchedFromXml;
            count++;
        }
    }

}