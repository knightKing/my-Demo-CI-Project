package hsbc.aofe.service.utility;

import hsbc.aofe.domain.TransformedData;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Triplet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
public class SampleDomParser {
    private Map<String, LocalDate> forMonthMap = new TreeMap<>();
    private Map<String, LocalDate> paidOnMap = new HashMap<>();
    private Map<String, BigDecimal> amountMap = new HashMap<>();
    private Map<String, String> employerContributionMap = new HashMap<>();
    private TransformedData data = new TransformedData();
    private String cpfAccountNumber;
    private Map<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = new TreeMap<>(Collections.reverseOrder());

    private void domParser(String args) {
        log.debug("Inside domParser for parsing XML ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        //File fXmlFile = new File("D:\\ecpfResponse.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            // byte[] bytes=args.getBytes("UTF-8");
            InputSource inputSource = new InputSource(new StringReader(args));
            //InputStream is = new ByteArrayInputStream(bytes);
            doc = dBuilder.parse(inputSource);
            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();
            log.debug("Root element :" + doc.getDocumentElement().getNodeName());
            log.error("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("F");

            log.debug("-----------------------------------------------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                log.debug("Inside Main for loop of DOM parser");
                Node nNode = nList.item(temp);
                log.debug("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    final String fn = eElement.getAttribute("FN");
                    if ("CPF Account Number".equalsIgnoreCase(fn)) {
                        log.debug("FN is : " + fn);
                        cpfAccountNumber = eElement.getElementsByTagName("FD").item(0).getTextContent();
                        log.debug("FD is : " + cpfAccountNumber);
                    } else if ("For Month".equalsIgnoreCase(fn)) {
                        log.debug("FN is : " + fn);
                        final NodeList fd = eElement.getElementsByTagName("FD");
                        for (int i = 0; i < fd.getLength(); i++) {
                            Node fdNode = fd.item(i);
                            Element fdElement = (Element) fdNode;
                            NodeList RList = fdElement.getElementsByTagName("R");
                            for (int j = 0; j < RList.getLength(); j++) {
                                final Node num = RList.item(j).getAttributes().getNamedItem("NUM");
                                final String rdNodeValue = RList.item(j).getTextContent().trim();
                                StringBuilder forMonthValueWithDateAdded = new StringBuilder("01 " + rdNodeValue.substring(0, 1)
                                        + rdNodeValue.substring(1, rdNodeValue.length()).toLowerCase());
                                log.debug("Using DOM parser the date in forMonth before dateFormatter is" + forMonthValueWithDateAdded);
                                log.error("Using DOM parser the date in forMonth before dateFormatter is" + forMonthValueWithDateAdded);
                                final LocalDate localDate = LocalDate.parse(forMonthValueWithDateAdded, formatter);
                                forMonthMap.put(num.getNodeValue(), localDate);
                                log.debug("Using DOM parser the date in forMonth after dateFormatter is" + localDate);
                                log.error("Using DOM parser the date in forMonth after dateFormatter is" + localDate);
                            }

                        }
                    } else if ("Paid On".equalsIgnoreCase(fn)) {
                        log.debug("FN is : " + fn);
                        final NodeList fd = eElement.getElementsByTagName("FD");
                        for (int i = 0; i < fd.getLength(); i++) {
                            Node fdNode = fd.item(i);
                            Element fdElement = (Element) fdNode;
                            NodeList RList = fdElement.getElementsByTagName("R");
                            for (int j = 0; j < RList.getLength(); j++) {
                                final Node num = RList.item(j).getAttributes().getNamedItem("NUM");
                                final String rdNodeValue = RList.item(j).getTextContent().trim();
                                log.debug("Using DOM parser the date in Paid On before dateFormatter is" + rdNodeValue);
                                log.error("Using DOM parser the date in Paid On before dateFormatter is" + rdNodeValue);
                                final LocalDate localDate = LocalDate.parse(rdNodeValue, formatter);
                                paidOnMap.put(num.getNodeValue(), localDate);
                                log.debug("Using DOM parser the date in Paid On after dateFormatter is" + LocalDate.parse(rdNodeValue, formatter));
                                log.error("Using DOM parser the date in Paid On after dateFormatter is" + LocalDate.parse(rdNodeValue, formatter));

                            }

                        }
                    } else if ("Amount".equalsIgnoreCase(fn)) {
                        log.debug("FN is : " + fn);
                        final NodeList fd = eElement.getElementsByTagName("FD");
                        for (int i = 0; i < fd.getLength(); i++) {
                            Node fdNode = fd.item(i);
                            Element fdElement = (Element) fdNode;
                            NodeList RList = fdElement.getElementsByTagName("R");
                            for (int j = 0; j < RList.getLength(); j++) {
                                final Node num = RList.item(j).getAttributes().getNamedItem("NUM");
                                final String rdNodeValue = RList.item(j).getTextContent();
                                BigDecimal bigDecimalNodeValue = new BigDecimal(rdNodeValue.replace(",", ""));
                                log.debug("Using DOM parser Amount is" + rdNodeValue);
                                log.error("Using DOM parser Amount is" + rdNodeValue);
                                amountMap.put(num.getNodeValue(), bigDecimalNodeValue);
                            }

                        }
                    } else if ("Employer Contribution".equalsIgnoreCase(fn)) {
                        log.debug("FN is : " + fn);
                        final NodeList fd = eElement.getElementsByTagName("FD");
                        for (int i = 0; i < fd.getLength(); i++) {
                            Node fdNode = fd.item(i);
                            Element fdElement = (Element) fdNode;

                            NodeList RList = fdElement.getElementsByTagName("R");
                            for (int j = 0; j < RList.getLength(); j++) {
                                final Node num = RList.item(j).getAttributes().getNamedItem("NUM");
                                final String rdNodeValue = RList.item(j).getTextContent();
                                log.debug("Employer Contribution" + rdNodeValue);
                                log.error("Employer Contribution" + rdNodeValue);
                                employerContributionMap.put(num.getNodeValue(), rdNodeValue);
                            }

                        }
                    }

                }
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    public TransformedData populateTransformedData(String xmlResponseFromIps) {
        domParser(xmlResponseFromIps);
        log.debug("Inside populateTransformedData after parsing xml through DOM");
        log.error("Inside populateTransformedData after parsing xml through DOM");
       // Map<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = new TreeMap<>(Collections.reverseOrder());
        forMonthMap.keySet().forEach(key -> {
            log.debug("formonthmap key is " + forMonthMap.get(key));
            LocalDate forMonthValue = forMonthMap.get(key);
            log.debug("amountMap key is " + amountMap.get(key));
            log.error("amountMap key is " + amountMap.get(key));
            BigDecimal currentAmount = amountMap.get(key);
            Optional<Triplet<LocalDate, BigDecimal, String>> preValue = Optional
                    .ofNullable(extractedData.get(forMonthValue));
            log.error("Extracted data map size is" + extractedData.size());
            log.debug("Extracted data map size is" + extractedData.size());
            boolean isExtractedMapContainsGreaterAmountThenCurrentAmount = extractedData.containsKey(forMonthValue)
                    && preValue.isPresent() && currentAmount.compareTo(preValue.get().getValue1()) == -1;

            if (isExtractedMapContainsGreaterAmountThenCurrentAmount) {
                extractedData.put(forMonthValue, Triplet.with(extractedData.get(forMonthValue).getValue0(),
                        preValue.get().getValue1(), extractedData.get(forMonthValue).getValue2()));

            } else {
                extractedData.put(forMonthMap.get(key),
                        Triplet.with(paidOnMap.get(key), amountMap.get(key), employerContributionMap.get(key)));
            }
        });
        data.setExtractedData(extractedData);
        data.setCpfAccountNumber(cpfAccountNumber);
        log.debug("TransformedData is" + data.getCpfAccountNumber() + " Size of extracted data after transformation is " + data.getExtractedData().size());
        log.error("TransformedData is" + data.getCpfAccountNumber() + " Size of extracted data after transformation is " + data.getExtractedData().size());
        return data;
    }

}
