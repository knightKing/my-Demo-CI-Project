package hsbc.aofe.service.validator;

import hsbc.aofe.domain.OCRContentResource;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Triplet;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.*;
import java.util.function.Predicate;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/*****************************************
 * Created by NEWGEN SOFTWARE LTD.
 *****************************************
 */
@Slf4j
public class ECPFValidator extends GenericValidator<OCRContentResource> {

	public static final String EMPLOYEMENT_STATUS = "FULL-TIME";
	public static final String NATIONALITY = "SG";

	private final Predicate<OCRContentResource> APPLICANT_AGE_PREDICATE = ECPFValidator::applicantAgePredicate;
	private final Predicate<OCRContentResource> CONTRIBUTION_FOR_LATEST_6MONTHS_PREDICATE = this::contributionForLatest6MonthsPredicate;
	private final Predicate<OCRContentResource> EMPLOYER_CONTRIBUTION_MUST_BE_SAME_FOR_LATEST6MONTHS_PREDICATE = this::employerContributionMustBeSameForLatest6MonthsPredicate;
	private final Predicate<OCRContentResource> EMPLOYEMENT_STATUS_PREDICATE = resource -> EMPLOYEMENT_STATUS
			.equals(resource.getEmploymentStatus());
	private final Predicate<OCRContentResource> NRIC_NUMBER_EXIST_PREDICATE = resource -> resource
			.getNRIC_number() != null;
	private final Predicate<OCRContentResource> CPF_EQUALS_NRIC_PREDICATE = this::checkCPFEqualityWithNRIC;
	private final Predicate<OCRContentResource> NRIC_ISSUE_DATE_PREDICATE = this::NRICIssueDatePredicate;
	private final Predicate<OCRContentResource> FORMONTHS_HAS_LATESTDATA_PREDICATE = this::forMonthsHasLatestDataPredicate;
	private final static List<Predicate<OCRContentResource>> VALIDATORS = new ArrayList<>();
	private final TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> CONSIDERED_MONTHS_DATA = new TreeMap<>();

	public ECPFValidator() {
		//VALIDATORS.add(EMPLOYEMENT_STATUS_PREDICATE);
		VALIDATORS.add(NRIC_NUMBER_EXIST_PREDICATE);
		VALIDATORS.add(NRIC_ISSUE_DATE_PREDICATE);
		VALIDATORS.add(APPLICANT_AGE_PREDICATE);
		VALIDATORS.add(CONTRIBUTION_FOR_LATEST_6MONTHS_PREDICATE);
		VALIDATORS.add(EMPLOYER_CONTRIBUTION_MUST_BE_SAME_FOR_LATEST6MONTHS_PREDICATE);
		validators = Collections.unmodifiableList(VALIDATORS);
		
		final Map<Predicate<OCRContentResource>, String> map = new HashMap<>();
		map.put(APPLICANT_AGE_PREDICATE, "APPLICANT_AGE_PREDICATE is false");
		map.put(CONTRIBUTION_FOR_LATEST_6MONTHS_PREDICATE, "CONTRIBUTION_FOR_LATEST_6MONTHS_PREDICATE is false");
		map.put(EMPLOYEMENT_STATUS_PREDICATE, "EMPLOYEMENT_STATUS_PREDICATE is false");
		map.put(EMPLOYER_CONTRIBUTION_MUST_BE_SAME_FOR_LATEST6MONTHS_PREDICATE,
				"EMPLOYER_CONTRIBUTION_MUST_BE_SAME_FOR_LATEST6MONTHS_PREDICATE is false");
		map.put(NRIC_ISSUE_DATE_PREDICATE, "NRIC_ISSUE_DATE_PREDICATE is false");
		predicateErrorMessageMap = Collections.unmodifiableMap(map);
	}

	public boolean checkCPFEqualityWithNRIC(final OCRContentResource resource) {
		log.debug("In function checkCPFEqualityWithNRIC");
		return resource.getCPF_Account_Number().equals(resource.getNRIC_number());
	}

	private boolean NRICIssueDatePredicate(OCRContentResource resource) {
		log.debug("In function NRICIssueDatePredicate");
		log.error("In function NRICIssueDatePredicate");
		log.debug("Nationality for NRIC {} found {}" + resource.getNRIC_number() + "nationality"
				+ resource.getNationality());
		return resource.getNationality().equals(NATIONALITY) ? TRUE
				: isPrHolderHavingNRICForMoreThan3Years(resource.getNRIC_issueDate());
	}

	/**
	 * verify if applicant is PR holder then NRIC issue date must be greater
	 * than 4 years
	 *
	 * @param nricIssueDate
	 * @return
	 */
	private static boolean isPrHolderHavingNRICForMoreThan3Years(LocalDate nricIssueDate) {
		log.debug("In function isPrHolderHavingNRICForMoreThan3Years");

			Period nricIssuedSince = Period.between(nricIssueDate, LocalDate.now());
			//From dataBase a LocalDate is fetched which doesn't have seconds or precision upto nanoSeconds Duration.between()
			// will throw an unsupported unit seconds exception.That's y i am commenting this code.
			//Duration calculateDurationIncaseOfExact4Years = Duration.between(nricIssueDate, LocalDate.now());
			log.debug("isPrHolderHavingNRICForMoreThan4Years the years value is" + nricIssuedSince.getYears());
			if (nricIssuedSince.getYears() < 4) {
				log.debug("nricIssued is less then 4 years");
				return FALSE;
			} else {
				log.debug("isPrHolderHavingNRICForMoreThan4Years for case true for get months"
						+ nricIssuedSince.getMonths() + "getDays" + nricIssuedSince.getDays());
				return nricIssuedSince.getYears() > 4 ? TRUE
						: (nricIssuedSince.getMonths() > 1 || nricIssuedSince.getDays() > 1);
			}
	}

	private static boolean applicantAgePredicate(OCRContentResource resource) {
		log.debug("In function applicantAgePredicate");
		Period applicantAge = Period.between(resource.getDateOfBirth(), LocalDate.now());
		log.debug("applicant Age is " + applicantAge.getYears());
		if (applicantAge.getYears() < 55)
			return TRUE;
		else
			return applicantAge.getYears() == 55 ? (applicantAge.getMonths() == 0 && applicantAge.getDays() == 0)
					: FALSE;
	}

	public boolean forMonthsHasLatestDataPredicate(OCRContentResource resource) {
		log.debug("In function forMonthsHasLatestDataPredicate");
		TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = getExtractedData(resource);
		Map.Entry<LocalDate, Triplet<LocalDate, BigDecimal, String>> ecpfLatestEntry = extractedData.firstEntry();
		Period ecpfLatestEntrySince = Period.between(ecpfLatestEntry.getKey(), LocalDate.now());
		log.debug("forMonthsHasLatestData does ecpf has latest entry the years are" + ecpfLatestEntrySince.getYears() + " months "
				+ ecpfLatestEntrySince.getMonths());
		if (ecpfLatestEntrySince.getYears() == 0) {
			if (ecpfLatestEntrySince.getMonths() <= 3){
				log.debug("ecpf has Latest Entry true");
				return TRUE;
			}
			else{
				log.debug("ecpf doesn't have Latest Entry");
				return (ecpfLatestEntrySince.getMonths() == 3) ? ecpfLatestEntrySince.getDays() == 0 : FALSE;
			}
		} else
			return FALSE;
	}

	private boolean contributionForLatest6MonthsPredicate(OCRContentResource resource) {
		log.debug("In function contributionForLatest6MonthsPredicate");
		TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> extractedData = getExtractedData(resource);
		Month previousMonth = extractedData.firstEntry().getKey().getMonth();
		int monthCount = 1;
		boolean isCurrentMonthContinuationToPreviousMonth = true;
		log.debug("contributionForLatest6MonthsPredicate monthcount is " + monthCount);
		for (Map.Entry<LocalDate, Triplet<LocalDate, BigDecimal, String>> entry : extractedData.entrySet()) {
			if (monthCount > 5) {
				log.debug("contributionForLatest6MonthsPredicate monthcount is" + monthCount + " is true" );
				return TRUE;
			}
			Month currentMonth = entry.getKey().getMonth();                          //current month is coming wrong
			CONSIDERED_MONTHS_DATA.put(entry.getKey(), entry.getValue());
			if (!currentMonth.equals(previousMonth)) {
				log.debug("contributionForLatest6MonthsPredicate currentmonth not equals previousmonth"
						+ currentMonth.equals(previousMonth));
				isCurrentMonthContinuationToPreviousMonth = previousMonth.minus(1).equals(currentMonth);
				log.debug("isCurrentMonthContinuationToPreviousMonth value is " +isCurrentMonthContinuationToPreviousMonth);
				previousMonth = entry.getKey().getMonth();
				monthCount++;
			}
			if (!isCurrentMonthContinuationToPreviousMonth) {
				CONSIDERED_MONTHS_DATA.clear();
				log.debug("the current month is not same to previous month"
						+ isCurrentMonthContinuationToPreviousMonth);
				return FALSE;
			}
		}
		return FALSE;
	}

	private boolean employerContributionMustBeSameForLatest6MonthsPredicate(OCRContentResource resource) {
		log.debug("In function employerContributionMustBeSameForLatest6MonthsPredicate");
		final String employerContribution = CONSIDERED_MONTHS_DATA.firstEntry().getValue().getValue2();
		for (Map.Entry<LocalDate, Triplet<LocalDate, BigDecimal, String>> entry : CONSIDERED_MONTHS_DATA.entrySet())
			if (!entry.getValue().getValue2().equals(employerContribution)) {
				log.debug("employerContributionMustBeSameForLatest6MonthsPredicate"
						+ !entry.getValue().getValue2().equals(employerContribution));
				return FALSE;
			}
		return TRUE;
	}

	private static TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> getExtractedData(
			OCRContentResource resource) {
		log.debug("In function getExtractedData");
		return (TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>>) resource.getExtractedData();
	}

	public TreeMap<LocalDate, Triplet<LocalDate, BigDecimal, String>> getCONSIDERED_MONTHS_DATA() {
		log.debug("In function getCONSIDERED_MONTHS_DATA");
		return CONSIDERED_MONTHS_DATA;
	}
}