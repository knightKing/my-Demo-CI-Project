package hsbc.aofe.service.validator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import hsbc.aofe.domain.OCRContentResource;
import lombok.Data;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

/*****************************************
 * Created by NEWGEN SOFTWARE LTD.
 *****************************************
 */
@Slf4j
@Data
public class GenericValidator<T> implements Predicate<T> {
	
    protected List<Predicate<T>> validators = new LinkedList<>();
    
    protected Map<Predicate<OCRContentResource>, String> predicateErrorMessageMap ;
    
    protected String errorMessage ;
    /**
     * Evaluates this predicate on the given argument.
     *
     * @param o the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    @Override
    public boolean test(final T toValidate) {
    	
    	/*return validators.stream()
                .allMatch(predicate -> 
                predicate.test(toValidate));*/
    	 return validators.stream().allMatch(predicate -> {
        	boolean result = predicate.test(toValidate);
			return result;
        });
    }

}