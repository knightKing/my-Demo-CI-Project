package hsbc.aofe;

public enum AOFEStatus {
	
	APPLICATION_ALREADY_PENDING_WITH_CUSTOMER(1005,"Application Already Pending With Customer."),
	APPLICATION_ALREADY_PENDING_WITH_CUSTOMER_WITH_SAME_EMAIL_IDVALUE_AND_MOBILE(1000, "Application Already Pending With Customer with Same Email, IdValue And Mobile."),
	APPLICATION_ALREADY_IN_PROGRESS(1001,"Application Already In Progress"),
	APPLICATION_ALREADY_PENDING_FOR_REVIEW(1002,"Application Already Pending For Reviw"),
	APPLICATION_ALREADY_SUBMITTED_TO_AO(1003,"Application Already Submitted To AO"),
	ICAP_GENERAL_EXCEPTION(1004,"Virus is present is in the document.Documnet upload Failed"),
	AUTHENTICATION_FAILURE_EXCEPTION(2000,"Authentication Failed"),
	PROBABLE_SPAM_ATTACK(2001,"Probable Spam or Robotic Attack"),
	AO_SUBMISSION_FAILURE(4101,"Error with subsystem or validation failure"),
	AO_CONNECTION_FAILURE(4100,"Could not connect to Host");


	private final int value;

	private final String reasonPhrase;


	AOFEStatus(int value, String reasonPhrase) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
	}


	/**
	 * Return the integer value of this status code.
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Return the reason phrase of this status code.
	 */
	public String getReasonPhrase() {
		return this.reasonPhrase;
	}
	
	/**
	 * Return a string representation of this status code.
	 */
	@Override
	public String toString() {
		return Integer.toString(this.value);
	}

}