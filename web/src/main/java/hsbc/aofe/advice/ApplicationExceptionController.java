package hsbc.aofe.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import hsbc.aofe.AOFEStatus;
import hsbc.aofe.domain.ErrorResponse;
import hsbc.aofe.exception.AOException;
import hsbc.aofe.exception.ApplicationAlreadyInProgressException;
import hsbc.aofe.exception.ApplicationDoesNotExistException;
import hsbc.aofe.exception.ApplicationPendingForReviewException;
import hsbc.aofe.exception.ApplicationPendingWithCustomerException;
import hsbc.aofe.exception.ApplicationPendingWithCustomerWithSameEmailIdvalueAndMobileException;
import hsbc.aofe.exception.ApplicationSubmittedToAOException;
import hsbc.aofe.exception.ICAPException;
import hsbc.aofe.exception.IccmRequestTimedOutException;
import hsbc.aofe.exception.InvalidOperationException;
import hsbc.aofe.exception.InvalidOtpException;
import hsbc.aofe.exception.OcrException;
import hsbc.aofe.exception.ProbableRoboticSpamException;
import hsbc.aofe.exception.RequestAlreadyExistException;
import hsbc.aofe.exception.SameSupplementaryAndPrimaryIdValueException;

@ControllerAdvice
public class ApplicationExceptionController {


    @ExceptionHandler(RequestAlreadyExistException.class)
    public ResponseEntity<ErrorResponse> typeMismatch(RequestAlreadyExistException e) {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(System.currentTimeMillis());
        errorResponse.setError("Request Already Exist.");
        errorResponse.setStatus(HttpStatus.CONFLICT.value());
        errorResponse.setMessage(e.getField());
        errorResponse.setPath("/");
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(ApplicationDoesNotExistException.class)
    public ResponseEntity<ErrorResponse> notFound(ApplicationDoesNotExistException e) {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(System.currentTimeMillis());
        errorResponse.setError("Application Does Not Exist.");
        errorResponse.setStatus(HttpStatus.NO_CONTENT.value());
        errorResponse.setMessage(e.getField());
        errorResponse.setPath("/");
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_ACCEPTABLE);
    }
    
    @ExceptionHandler(InvalidOtpException.class)
    public ResponseEntity<ErrorResponse> notFound(InvalidOtpException e) {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(System.currentTimeMillis());
        errorResponse.setError("Invalid Otp.");
        errorResponse.setStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
        errorResponse.setMessage(e.getField());
        errorResponse.setPath("/");
        return new ResponseEntity<>(errorResponse, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
    }
    
    @ExceptionHandler(InvalidOperationException.class)
    public ResponseEntity<ErrorResponse> notFound(InvalidOperationException e) {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(System.currentTimeMillis());
        errorResponse.setError("Invalid Request.");
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        errorResponse.setMessage(e.getField());
        errorResponse.setPath("/");
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(SameSupplementaryAndPrimaryIdValueException.class)
    public ResponseEntity<ErrorResponse> sameIdValue(SameSupplementaryAndPrimaryIdValueException e) {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(System.currentTimeMillis());
        errorResponse.setError("Primary IdValue cannot be Same as Supplementary IdValue.");
        errorResponse.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
        errorResponse.setMessage(e.getField());
        errorResponse.setPath("/");
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_ACCEPTABLE);
    }
    
	@ExceptionHandler(OcrException.class)
	public ResponseEntity<ErrorResponse> ocrException(OcrException e) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(HttpStatus.CONFLICT.value());
		errorResponse.setMessage(e.getMessage());		
		return new ResponseEntity<>(errorResponse,HttpStatus.CONFLICT);

	}
	
	
	@ExceptionHandler(ApplicationSubmittedToAOException.class)
	public ResponseEntity<ErrorResponse> handleApplicationSubmittedToAOException(ApplicationSubmittedToAOException e) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.APPLICATION_ALREADY_SUBMITTED_TO_AO.value());
		errorResponse.setMessage(e.getMessage());		
		return new ResponseEntity<>(errorResponse,HttpStatus.NOT_ACCEPTABLE);

	}
	
	@ExceptionHandler(ApplicationPendingWithCustomerException.class)
	public ResponseEntity<ErrorResponse> handleApplicationPendingWithCustomerException(ApplicationPendingWithCustomerException e) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.APPLICATION_ALREADY_PENDING_WITH_CUSTOMER.value());
		errorResponse.setMessage(e.getMessage());		
		return new ResponseEntity<>(errorResponse,HttpStatus.NOT_ACCEPTABLE);

	}
	
	@ExceptionHandler(ApplicationPendingWithCustomerWithSameEmailIdvalueAndMobileException.class)
	public ResponseEntity<ErrorResponse> handleApplicationPendingWithCustomerToResume(ApplicationPendingWithCustomerWithSameEmailIdvalueAndMobileException e) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.APPLICATION_ALREADY_PENDING_WITH_CUSTOMER_WITH_SAME_EMAIL_IDVALUE_AND_MOBILE.value());
		errorResponse.setMessage(e.getMessage());		
		return new ResponseEntity<>(errorResponse,HttpStatus.NOT_ACCEPTABLE);

	}
	
	@ExceptionHandler(ApplicationPendingForReviewException.class)
	public ResponseEntity<ErrorResponse> handleApplicationPendingForReviewException(ApplicationPendingForReviewException e) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.APPLICATION_ALREADY_PENDING_FOR_REVIEW.value());
		errorResponse.setMessage(e.getMessage());		
		return new ResponseEntity<>(errorResponse,HttpStatus.NOT_ACCEPTABLE);

	}
	
	@ExceptionHandler(ApplicationAlreadyInProgressException.class)
	public ResponseEntity<ErrorResponse> handleApplicationAlreadyInProgressException(ApplicationAlreadyInProgressException e) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.APPLICATION_ALREADY_IN_PROGRESS.value());
		errorResponse.setMessage(e.getMessage());		
		return new ResponseEntity<>(errorResponse,HttpStatus.NOT_ACCEPTABLE);

	}
	
	@ExceptionHandler(ICAPException.class)
	public ResponseEntity<ErrorResponse> handleIcapException(ICAPException e){
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.ICAP_GENERAL_EXCEPTION.value());
		errorResponse.setMessage(e.getMessage());		
		return new ResponseEntity<>(errorResponse,HttpStatus.NOT_ACCEPTABLE);	
		
	}
	
	@ExceptionHandler(AuthenticationException.class)
	public ResponseEntity<ErrorResponse> handleAuthenticationException(AuthenticationException e){
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.AUTHENTICATION_FAILURE_EXCEPTION.value());
		errorResponse.setMessage("Authentication failed");		
		return new ResponseEntity<>(errorResponse,HttpStatus.UNAUTHORIZED);	
		
	}
	
	@ExceptionHandler(ProbableRoboticSpamException.class)
	public ResponseEntity<ErrorResponse> handleProbableRoboticSpamException(ProbableRoboticSpamException e){
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(AOFEStatus.PROBABLE_SPAM_ATTACK.value());
		errorResponse.setMessage("Probable Robotic Spam");		
		return new ResponseEntity<>(errorResponse,HttpStatus.FORBIDDEN);	
		
	}
	
	@ExceptionHandler(IccmRequestTimedOutException.class)
	public ResponseEntity<ErrorResponse> handleIccmException(IccmRequestTimedOutException e){
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getMessage());
		errorResponse.setPath("/");
		errorResponse.setStatus(HttpStatus.REQUEST_TIMEOUT.value());
		errorResponse.setMessage("Iccm Request Timed Out.");		
		return new ResponseEntity<>(errorResponse,HttpStatus.REQUEST_TIMEOUT);	
		
	}
	
	@ExceptionHandler(AOException.class)
	public ResponseEntity<ErrorResponse> handleAOException(AOException e) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTimestamp(System.currentTimeMillis());
		errorResponse.setError(e.getErrorString());
		errorResponse.setPath("/");
		errorResponse.setMessage(e.getMessage());
	
		switch (e.getErrorCode()) {
		case 4100:
			errorResponse.setStatus(AOFEStatus.AO_CONNECTION_FAILURE.value());
			return new ResponseEntity<>(errorResponse, HttpStatus.SERVICE_UNAVAILABLE);
		case 4101:
			errorResponse.setStatus(AOFEStatus.AO_SUBMISSION_FAILURE.value());
			return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
		default:
			errorResponse.setStatus(AOFEStatus.AO_SUBMISSION_FAILURE.value());
			return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
		}

	}
	
	
}
