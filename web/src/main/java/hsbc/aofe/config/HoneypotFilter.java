package hsbc.aofe.config;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import hsbc.aofe.advice.ApplicationExceptionController;
import hsbc.aofe.controllers.UserController;
import hsbc.aofe.exception.ProbableRoboticSpamException;

/**
 * A generic filter for security. I will check token present in the header.
 *
 */
@Component
@Order(1)
public class HoneypotFilter implements Filter {

	@Value("${captcha.renderTimestamp}")
	private String render_timestamp = "5000";

	@Value("${captcha.switch}")
	private String captchaSwitch = "true";

	WebConfigProperties webConfigProperties;
	@Autowired
	ApplicationExceptionController excpController;
	@Autowired
	UserController loginController;

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String reqMethod = request.getMethod();
		String requestURI = request.getRequestURI();
		Long allowedTimeRange = Long.valueOf(render_timestamp);
		
		request.getSession().setMaxInactiveInterval(20*60);

		WebApplicationContext springContext = WebApplicationContextUtils
				.getWebApplicationContext(req.getServletContext());
		this.webConfigProperties = springContext.getBean(WebConfigProperties.class);
		this.excpController = springContext.getBean(ApplicationExceptionController.class);
		this.loginController = springContext.getBean(UserController.class);

		List<String> allowedMethods = webConfigProperties.getRequestMethodTypeList();
		String honeyPotVarName = webConfigProperties.getHoneypotVariableName();

		if (Boolean.valueOf(captchaSwitch) && !requestURI.contains("/resources/")) {

			if (!isWhiteListed(requestURI) && allowedMethods.contains(reqMethod)) {
				/*if (null == request.getParameter("render_timestamp")) {
					throw new AccessDeniedException("Request not allowed");
				}
				Long longFromUI = new Long(request.getParameter("render_timestamp"));
				Long longAtBackend = System.currentTimeMillis();

				if ((longAtBackend - longFromUI) > allowedTimeRange) {
					throw new AccessDeniedException("Request not allowed");
				}*/

				if (null == request.getParameter(honeyPotVarName)
						|| StringUtils.isNotEmpty(request.getParameter(honeyPotVarName))) {
					excpController.handleProbableRoboticSpamException(new ProbableRoboticSpamException("Request not allowed"));
					response.sendRedirect("/hsbcaofe/users/robotic/failure");
					//throw new ProbableRoboticSpamException("Request not allowed");
				}

			}

		}
		filterChain.doFilter(request, response);

	}

	public String convertObjectToJson(Object object) throws JsonProcessingException {
		if (object == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

	private boolean isWhiteListed(String strippedURL) {
		boolean isWhiteListed = false;
		
		for(String url : webConfigProperties.getWhiteList()){
			if(strippedURL.matches(url)){
				isWhiteListed = true;
				break;
			}
		}
		
		return isWhiteListed;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}
