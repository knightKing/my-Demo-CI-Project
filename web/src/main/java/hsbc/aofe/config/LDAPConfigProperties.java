package hsbc.aofe.config;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "hsbc.ldap.authority-string")
@Validated
public class LDAPConfigProperties {

	private String BRANCH;

	private String TELESALES;

	private String ROADSHOW;

	private String FFAGENT;

	private String POWERUSER;

	private String TPSA;

	private String CUSTOMER;

	private String ANONYMOUS;

	private String ENV;

	private String userSearchFilter;

	private String userSearchBase;
	
	private String groupSearchFilter;
	
	private String groupSearchBase;
	
	private String userDnPatterns;
	
	private String rolePrefix;
	
}
