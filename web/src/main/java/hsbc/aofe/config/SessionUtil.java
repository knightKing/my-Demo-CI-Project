package hsbc.aofe.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapAuthority;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import hsbc.aofe.dto.UserDetails;

public class SessionUtil {

	public static UserDetails getLoggedInUserDetails() throws HttpSessionRequiredException {
		if (session().getAttribute("loggedInUser") == null) {
			throw new HttpSessionRequiredException("Session has been Expired. Please Login Again.");
		}
		return (UserDetails) session().getAttribute("loggedInUser");
	}

	public static HttpSession session() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(true); // true == allow create
	}

	public static UserDetails getAnonymousUserDetails() {

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ANONYMOUS"));

		Authentication auth = new AnonymousAuthenticationToken("anonuser", "ANON", authorities);
		auth.setAuthenticated(true);
		SecurityContextHolder.getContext().setAuthentication(auth);

		return new UserDetails("Anonymous", "", authorities);
	}
	
	public static UserDetails setBranchUserDetails() {

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_BRANCH"));

		//Authentication auth = new AnonymousAuthenticationToken("branchuser", "BRANCH", authorities);
		
		Authentication auth = new UsernamePasswordAuthenticationToken("branchUser","BRANCH", authorities);
		//auth.setAuthenticated(true);
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		UserDetails details = new UserDetails("BNH123456,ou=users,dc=newgen,dc=co,dc=in", "", authorities);
		
		session().setAttribute("loggedInUser", details);

		return details;
	}
	
	public static boolean isDigitallJourney() throws HttpSessionRequiredException {
		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		Collection<LdapAuthority> authorities = userDetails.getRoles();

		for (Iterator iterator = authorities.iterator(); iterator.hasNext();) {
			LdapAuthority authority = (LdapAuthority) iterator.next();

			if ("CUSTOMER".equalsIgnoreCase(authority.getAuthority())) {
				return true;
			}
		}

		return false;
	}

}
