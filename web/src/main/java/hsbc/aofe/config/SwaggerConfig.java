package hsbc.aofe.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@Profile("development")
@EnableSwagger2
public class SwaggerConfig {

    /**
     * cofiguration related to swagger implementation
     *
     * @return Docket
     */
    @Bean
    public Docket hsbcApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("hsbc.aofe.controllers"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(hsbcAPIMetaData());
    }

    /**
     * Api metadata information
     *
     * @return ApiInfo
     */
    private ApiInfo hsbcAPIMetaData() {
        ApiInfo apiInfo = new ApiInfo(
                "HSBC REST API",
                "HSBC REST API for AO front-end",
                "1.0-SNAPSHOT",
                "Terms of contract",
                new Contact("Newgen Software Ltd.", "http://www.newgensoft.com/", "sahil.verma@newgen.co.in"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0");
        return apiInfo;
    }

}
