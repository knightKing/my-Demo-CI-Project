package hsbc.aofe.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "hsbcWebSecurityConfig")
public class WebConfigProperties {
	
	private String allowedURLString;
	private String requestMethod;
	private String honeypotVariableName;
	
	private List<String> whiteList = new ArrayList<String>();
	private List<String> requestMethodTypeList = new ArrayList<String>();
	
	
	@PostConstruct
	public void generateAllowedURLList(){
	   	String[] urlArr = this.allowedURLString.split(",");
    	    	
    	whiteList.addAll(Arrays.asList(urlArr));
    	
    	String[] methodTypeArr = this.requestMethod.split(",");
    	requestMethodTypeList.addAll(Arrays.asList(methodTypeArr));
    }

}
