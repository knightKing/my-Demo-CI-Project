package hsbc.aofe.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.header.writers.StaticHeadersWriter;

import hsbc.aofe.dto.UserDetails;
import lombok.Data;

@Configuration
@EnableWebSecurity
@Data
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Value("${ldapUrl}")
    private String ldapUrl;

    @Value("${hsbc.ldap.authority-string.ENV}")
    private String ldapEnv;
    
    @Value("${hsbc.ldap.authority-string.userSearchFilter}")
    private String userSearchFilter;
    
    @Value("${hsbc.ldap.authority-string.userSearchBase}")
    private String userSearchBase;
    
    @Value("${hsbc.ldap.authority-string.groupSearchFilter}")
    private String groupSearchFilter;
    
    @Value("${hsbc.ldap.authority-string.groupSearchBase}")
    private String groupSearchBase;
    
    @Value("${hsbc.ldap.authority-string.userDnPatterns}")
    private String userDnPatterns;
    
    @Value("${hsbc.ldap.authority-string.rolePrefix}")
    private String rolePrefix;
    
    @Value("${hsbc.ldap.authority-string.managerDn}")
    private String managerDn;
    
    @Value("${hsbc.ldap.authority-string.managerPassword}")
    private String managerPassword;
    
    @Value("${hsbc.ldap.authority-string.BRANCH}")
    private String BRANCH;
    
    @Value("${hsbc.ldap.authority-string.FFAGENT}")
    private String FFAGENT;
    
    @Value("${hsbc.ldap.authority-string.TELESALES}")
    private String TELESALES;
    
    @Value("${hsbc.ldap.authority-string.ROADSHOW}")
    private String ROADSHOW;
    
    @Value("${hsbc.ldap.authority-string.POWERUSER}")
    private String POWERUSER;
    
    @Value("${hsbc.ldap.authority-string.TPSA}")
    private String TPSA;
    
    @Value("${hsbc.ldap.authority-string.CUSTOMER}")
    private String CUSTOMER;
    
    @Value("${hsbc.ldap.authority-string.ANONYMOUS}")
    private String ANONYMOUS;
    
    @Value("${hsbc.ldap.authority-string.ENV}")
    private String ENV;
    
    //TODO: this needs to be configured so that all above variables can be deleted.
    @Autowired
    private LDAPConfigProperties ldapConfigProperties;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests().antMatchers("/","/login","/resources/**","/users/**","/otp/**","/users/login/ANONYMOUS").permitAll();
        
        http.headers().defaultsDisabled()
        .addHeaderWriter(new StaticHeadersWriter("Cache-Control"," no-cache,max-age=0, must-revalidate"))
        .addHeaderWriter(new StaticHeadersWriter("Expires","0"));

        /*http.authorizeRequests().antMatchers("/login").permitAll();
        http.authorizeRequests().antMatchers("/resources/**").permitAll();
        http.authorizeRequests().antMatchers("/users/login/ANONYMOUS/**").permitAll();
        http.authorizeRequests().antMatchers("/otp/**").permitAll();*/


        /**
         * good code starts
         */
        

        http.requiresChannel().and().authorizeRequests().antMatchers("/applications/**",
        		"/ao/**",
        		"/config/cards/CREDIT/**",
        		"/dashboard/**",
        		"/ocr/**",
        		"/iupload/**",
        		"/qas/address/**").hasAnyAuthority(TPSA.toUpperCase(),CUSTOMER.toUpperCase(),BRANCH.toUpperCase(),TELESALES.toUpperCase(),ROADSHOW.toUpperCase(),FFAGENT.toUpperCase(),POWERUSER.toUpperCase())
        .and().authorizeRequests().anyRequest().authenticated().and()/*.addFilterBefore(new HoneypotFilter(), UsernamePasswordAuthenticationFilter.class)*/
                .formLogin()
                .successHandler(new AuthenticationSuccessHandler() {

                    RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                                        Authentication authentication) throws IOException, ServletException {
                    	
                    	request.getSession().setMaxInactiveInterval(20*60);

                        if (authentication.isAuthenticated()) {
                            redirectStrategy.sendRedirect(request, response, "/users/me");

                        } else {
                            redirectStrategy.sendRedirect(request, response, "/newFile.html");
                        }

                    }

                }).failureHandler(new AuthenticationFailureHandler() {
					
					@Override
					public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
							AuthenticationException exception) throws IOException, ServletException {
						RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
						
						redirectStrategy.sendRedirect(request, response, "/users/login/failure");
					}
				});

		/*
         * http.authorizeRequests().antMatchers("/newfile.html").permitAll().and
		 * ().anonymous().authorities("ROLE_ANON")
		 * .principal(getAnonymousUserDetails());
		 */

        AuthenticationProvider provider = new AuthenticationProvider() {

            @Override
            public boolean supports(Class<?> authentication) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                return new UsernamePasswordAuthenticationToken("Anonymous", "", new ArrayList<>());
            }
        };

        // http.authorizeRequests().antMatchers("/newfile.html").permitAll().and().anonymous().authenticationProvider(provider);

        /**
         * good code ends rest else is to be deleted but not now.
         */

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	this.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(LDAPConfigProperties.class);
    	ldapConfigProperties = this.getApplicationContext().getBean(LDAPConfigProperties.class);
    	
        if (ldapEnv.equalsIgnoreCase("ldap")) {
            auth.ldapAuthentication().userSearchFilter(userSearchFilter).userSearchBase(userSearchBase)
                    .groupSearchFilter(groupSearchFilter).groupSearchBase(groupSearchBase).userDnPatterns(userDnPatterns)
                    .rolePrefix(rolePrefix).contextSource().url(ldapUrl).managerDn(managerDn).managerPassword(managerPassword);
            /*auth.ldapAuthentication().userSearchFilter("(uid={0})").userSearchBase("ou=users")
            .groupSearchFilter("member={0}").groupSearchBase("ou=groups").userDnPatterns("uid={0},ou=users")
            .rolePrefix("").contextSource().url(ldapUrl);*/

        } else {
            
            auth.inMemoryAuthentication().withUser("BNH123456").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_1").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_2").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_3").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_4").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_5").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_6").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_7").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_8").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_9").password("newgen").authorities(BRANCH.toUpperCase());
            auth.inMemoryAuthentication().withUser("branchuser_10").password("newgen").authorities(BRANCH.toUpperCase());
            
            auth.inMemoryAuthentication().withUser("TNH123456").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_1").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_2").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_3").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_4").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_5").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_6").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_7").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_8").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_9").password("newgen").authorities(TELESALES.toUpperCase());
            auth.inMemoryAuthentication().withUser("tsuser_10").password("newgen").authorities(TELESALES.toUpperCase());
            
            auth.inMemoryAuthentication().withUser("rssuser_1").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_2").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_3").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_4").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_5").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_6").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_7").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_8").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_9").password("newgen").authorities(ROADSHOW.toUpperCase());
            auth.inMemoryAuthentication().withUser("rssuser_10").password("newgen").authorities(ROADSHOW.toUpperCase());
            
            auth.inMemoryAuthentication().withUser("FNH123456").password("newgen").authorities(FFAGENT.toUpperCase());
            auth.inMemoryAuthentication().withUser("ffagent_1").password("newgen").authorities(FFAGENT.toUpperCase());
            auth.inMemoryAuthentication().withUser("ffagent_2").password("newgen").authorities(FFAGENT.toUpperCase());
            auth.inMemoryAuthentication().withUser("ffagent_3").password("newgen").authorities(FFAGENT.toUpperCase());
            auth.inMemoryAuthentication().withUser("ffagent_4").password("newgen").authorities(FFAGENT.toUpperCase());
            auth.inMemoryAuthentication().withUser("ffagent_5").password("newgen").authorities(FFAGENT.toUpperCase());
            auth.inMemoryAuthentication().withUser("ffagent_6").password("newgen").authorities(FFAGENT.toUpperCase());
            auth.inMemoryAuthentication().withUser("ffagent_7").password("newgen").authorities(FFAGENT.toUpperCase());
            
            auth.inMemoryAuthentication().withUser("PNH123456").password("newgen").authorities(POWERUSER.toUpperCase());
            auth.inMemoryAuthentication().withUser("poweruser1").password("newgen").authorities(POWERUSER.toUpperCase());
            auth.inMemoryAuthentication().withUser("poweruser2").password("newgen").authorities(POWERUSER.toUpperCase());
            auth.inMemoryAuthentication().withUser("poweruser3").password("newgen").authorities(POWERUSER.toUpperCase());
        }
    }

    UserDetails getAnonymousUserDetails() {

        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ANON"));

        Authentication auth = new AnonymousAuthenticationToken("anonuser", "ANON", authorities);
        // auth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(auth);

        return new UserDetails("Anonymous", "", authorities);
    }

}

