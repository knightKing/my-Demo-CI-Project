package hsbc.aofe.config.security;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AuthenticatedUserDetails implements Serializable {

    private static final long serialVersionUID = 6745705517725940701L;
    private String principalName;
    private List<Roles> roles;
    private CsrfTokenImpl csrfToken;

}
