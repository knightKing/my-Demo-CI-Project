
package hsbc.aofe.config.security;

import lombok.Data;
import org.springframework.security.web.csrf.CsrfToken;

import java.io.Serializable;

@Data
public class CsrfTokenImpl implements CsrfToken, Serializable {

    private String token;
    private String parameterName;
    private String headerName;

    @Override
    public String getHeaderName() {
        // TODO Auto-generated method stub
        return headerName;
    }

    @Override
    public String getParameterName() {
        // TODO Auto-generated method stub
        return parameterName;
    }

    @Override
    public String getToken() {
        // TODO Auto-generated method stub
        return token;
    }

}
