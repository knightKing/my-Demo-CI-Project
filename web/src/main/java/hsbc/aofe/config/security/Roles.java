package hsbc.aofe.config.security;

import lombok.Data;

import java.io.Serializable;

@Data
public class Roles implements Serializable {

    private String authority;
}
