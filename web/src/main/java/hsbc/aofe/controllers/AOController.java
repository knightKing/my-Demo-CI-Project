package hsbc.aofe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import hsbc.aofe.ao.contract.AOService;
import hsbc.aofe.service.AOServiceImpl;

@RestController
@RequestMapping("/ao")
public class AOController{
	
//static AOServiceImpl aoServiceAo = new AOServiceImpl(null);
	
/*	@Autowired
	private AOService aoService;*/
/*	private AOServiceImpl serviceAo;

	public AOController(AOService aoService){
		this.aoService = aoService;
	}*/
	
	private AOService aoService;
	
	@Autowired
	public AOController(AOService aoService){
		this.aoService = aoService;
	}
	

	@RequestMapping(value = "/applications/{arn}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<String> initiateAoSubmission(@PathVariable String arn) {
		String aoReferenceNumber = aoService.initiateAoSubmission(arn);
		String json = toJSON(aoReferenceNumber);
		return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	private String toJSON(Object object) {
        if (object == null) {
            return "{}";
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "{}";
    }
	/*public static void main(String[] args) {
	
		aoServiceAo.initiateAoSubmission("ftuHE7");
}*/
}
