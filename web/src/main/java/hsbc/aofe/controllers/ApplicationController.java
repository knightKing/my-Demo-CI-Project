package hsbc.aofe.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;
import hsbc.aofe.ao.contract.AOService;
import hsbc.aofe.config.SessionUtil;
import hsbc.aofe.config.WebSecurity;
import hsbc.aofe.domain.*;
import hsbc.aofe.dto.ApplicationStatus;
import hsbc.aofe.dto.UserDetails;
import hsbc.aofe.exception.*;
import hsbc.aofe.service.ApplicantService;
import hsbc.aofe.service.ApplicationService;
import hsbc.aofe.service.CreatePdfService;
import hsbc.aofe.service.OcrService;
import hsbc.aofe.validationgroups.BranchJourneyGroup;
import hsbc.aofe.validationgroups.CustomerJourneyGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Api(value = "Application-api", description = "Operations related to a application", tags = { "Application-Api" })
@RestController
@RequestMapping("/applications")
public class ApplicationController {

	private ApplicationService applicationService;
	private ApplicantService applicantService;
	private AOService aoService;
	private OcrService ocrService;
	private CreatePdfService createPdfService;
	private WebSecurity webSecurity;

	public ApplicationController(ApplicationService applicationService, ApplicantService applicantService,
			AOService aoService, OcrService ocrService, CreatePdfService createPdfService,WebSecurity webSecurity) {
		this.applicationService = applicationService;
		this.applicantService = applicantService;
		this.aoService = aoService;
		this.ocrService = ocrService;
		this.createPdfService = createPdfService;
		this.webSecurity=webSecurity;
	}

	/**
	 * This GET Request gets all applications
	 *
	 * @return List<Application>
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public List<Application> getAllApplications() {
		return applicationService.findAllApplications();
	}

	/**
	 * This GET Request gets an application corresponding an arn
	 *
	 * @param arn
	 * @return Application
	 */
	@RequestMapping(value = "/{arn}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public Application getApplicationByARN(@PathVariable("arn") String arn) {
		return applicationService.findApplicationByARN(arn);
	}

	/**
	 * This Post request creates an application in the database.
	 *
	 * @param application
	 * @param bindingResult
	 * @return Application
	 * @throws HttpSessionRequiredException
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public Application createApplication(
			@RequestParam(value = "mandatoryFlag", defaultValue = "false") boolean mandatoryFlag,
			@RequestBody Application application, BindingResult bindingResult) throws HttpSessionRequiredException {

		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		String uid = userDetails.getUid();
		String channelAuthority = Iterables.get(userDetails.getRoles(), 0).getAuthority();

		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<Object>> violations = null;

		if ("CUSTOMER".equals(channelAuthority)) {
			violations = validator.validate(application, BranchJourneyGroup.class, CustomerJourneyGroup.class,
					Default.class);
		} else if ("BRANCH".equals(channelAuthority)) {
			violations = validator.validate(application, BranchJourneyGroup.class, Default.class);
		}

		if (CollectionUtils.isNotEmpty(violations)) {
			for (ConstraintViolation<Object> constraintViolation : violations) {
				String objectName = String.valueOf(constraintViolation.getPropertyPath());
				String fieldName = String.valueOf(constraintViolation.getPropertyPath());
				String defaultMessage = constraintViolation.getMessage();
				if (fieldName.contains("address")) {
					String addressArray = fieldName.substring(fieldName.indexOf("address"),
							fieldName.indexOf("address") + 10);
					int index = Integer
							.valueOf(addressArray.substring(addressArray.indexOf("[") + 1, addressArray.indexOf("]")));
					fieldName = application.getPrimaryApplicant().getAdditionalInfo().getAddress().get(index)
							.getAddressType().getValue() + "|" + fieldName;
				}
				if (fieldName.contains("cards")) {
					String cardsArray = fieldName.substring(fieldName.indexOf("cards"), fieldName.indexOf("cards") + 8);
					int index = Integer
							.valueOf(cardsArray.substring(cardsArray.indexOf("[") + 1, cardsArray.indexOf("]")));
					fieldName = application.getPrimaryApplicant().getCards().get(index).getKey() + "|" + fieldName;
				}
				FieldError fieldError = new FieldError(objectName, fieldName, defaultMessage);
				bindingResult.addError(fieldError);
			}
		}

		if (bindingResult.hasErrors()) {
			throw new InvalidRequestException("Invalid Request Body", bindingResult);
		}

		return applicationService.createApplication(uid, channelAuthority, application);
	}

	/**
	 * This DELETE Request deletes an application corresponding an arn
	 *
	 * @param arn
	 * @return Application
	 */
	@RequestMapping(value = "/{arn}", method = RequestMethod.DELETE, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public void deleteApplicationById(@PathVariable("arn") String arn) {
		applicationService.deleteApplicationById(arn);
	}

	@ApiOperation(value = "Modify primary applicant")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list of arns"),
    @ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/{arn}/applicants/PRIMARY", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateApplicant(@PathVariable("arn") String arn, @RequestBody PatchRequest patchRequest, @RequestParam String action,
			BindingResult bindingResult,@RequestParam(value = "essentialDataPreFilledIndicator" ,required = false) boolean essentialDataPreFilledIndicator) throws IOException, InvalidRequestDataException, HttpSessionRequiredException {

		Applicant primaryApplicant = null;
		if (patchRequest == null)
			throw new InvalidRequestDataException("Please send valid data in PATCH Request.");

		boolean isApplicationShouldBeSentToCustomer = patchRequest.isToCustomer();
		boolean isApplicationShouldBeSentToAO = patchRequest.isToAO();
		boolean isApplicationShouldBeSentForReview = patchRequest.isForReview();

		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		String channel = Iterables.get(userDetails.getRoles(), 0).getAuthority();
		String uid = userDetails.getUid();

		final String fullFillment = UserController.rolesMap.get(webSecurity.getFFAGENT().toUpperCase());

		if ("FFAGENT".equalsIgnoreCase(fullFillment) && !patchRequest.getPatch().isNull()) {
			applicationService.setEssentialDataPreFilledIndicator(essentialDataPreFilledIndicator,arn);
		}
		else if ("FFAGENT".equalsIgnoreCase(fullFillment)) {
			applicationService.setEssentialDataPreFilledIndicator(essentialDataPreFilledIndicator,arn);
		}
		if (!patchRequest.getPatch().isNull()) {
			primaryApplicant = applicantService.savePrimaryApplicant(arn, patchRequest.getPatch(), bindingResult, channel);
		}
		
		if ("customer".equalsIgnoreCase(channel) && "saveAndExit".equalsIgnoreCase(action)) {
			applicationService.applicationSaveAndExit(arn, uid, channel);
		}

		if (isApplicationShouldBeSentToCustomer)
			applicationService.updateApplicationStatus(arn, NgStatus.PENDINGWITHCUSTOMER, uid, channel);
		if (isApplicationShouldBeSentToAO) {
			String aoReferenceNumber = aoService.initiateAoSubmission(arn);
			applicationService.updateApplicationStatus(arn, NgStatus.SUBMITTEDTOAO, uid, channel);
			return ResponseEntity.ok(toJSON(aoReferenceNumber));
		}
		if (isApplicationShouldBeSentForReview) {
			applicationService.updateApplicationStatus(arn, NgStatus.PENDINGFORREVIEW, uid, channel);
			return ResponseEntity.ok(toJSON(NgStatus.PENDINGFORREVIEW));
		}
		return ResponseEntity.ok(primaryApplicant);
	}

	@ApiOperation(value = "Modify application")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list of arns"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/{arn}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> updateApplication(@PathVariable("arn") String arn, @RequestBody PatchRequest patchRequest, @RequestParam String action,
			BindingResult bindingResult,@RequestParam(value = "essentialDataPreFilledIndicator",required = false) boolean essentialDataPreFilledIndicator) throws IOException, InvalidRequestDataException, HttpSessionRequiredException {

		Application application = null;
		if (patchRequest == null)
			throw new InvalidRequestDataException("Please send valid data in PATCH Request.");

		boolean isApplicationShouldBeSentToCustomer = patchRequest.isToCustomer();
		boolean isApplicationShouldBeSentToAO = patchRequest.isToAO();
		boolean isApplicationShouldBeSentForReview = patchRequest.isForReview();

		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		String channel = Iterables.get(userDetails.getRoles(), 0).getAuthority();
		String uid = userDetails.getUid();

		final String fullFillment = UserController.rolesMap.get(webSecurity.getFFAGENT().toUpperCase());
		if ("FFAGENT".equalsIgnoreCase(fullFillment) && !patchRequest.getPatch().isNull()) {
			applicationService.setEssentialDataPreFilledIndicator(essentialDataPreFilledIndicator, arn);
		}
		if (!patchRequest.getPatch().isNull()) {
			application = applicationService.updateApplication(arn, patchRequest.getPatch(), bindingResult, channel);
		}
		
		if ("customer".equalsIgnoreCase(channel) && "saveAndExit".equalsIgnoreCase(action)) {
			applicationService.applicationSaveAndExit(arn, uid, channel);
		}

		if (isApplicationShouldBeSentToAO) {
			String aoReferenceNumber = aoService.initiateAoSubmission(arn);
			applicationService.updateApplicationStatus(arn, NgStatus.SUBMITTEDTOAO, uid, channel);
			return ResponseEntity.ok(toJSON(aoReferenceNumber));
		}
		if (isApplicationShouldBeSentForReview) {
			createPdfService.createApplicationPdf(arn);
			applicationService.updateApplicationStatus(arn, NgStatus.PENDINGFORREVIEW, uid, channel);
			return ResponseEntity.ok(toJSON(NgStatus.PENDINGFORREVIEW));
		}
		if (isApplicationShouldBeSentToCustomer)
			applicationService.updateApplicationStatus(arn, NgStatus.PENDINGWITHCUSTOMER, uid, channel);

		return ResponseEntity.ok(application);
	}

	/**
	 * This PATCH Request updates Balance transfer details
	 *
	 * @param arn
	 * @throws HttpSessionRequiredException
	 */
	@RequestMapping(value = "/{arn}/applicants/PRIMARY/balance", method = RequestMethod.PATCH, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public Applicant updateBeneficiaryDetails(@PathVariable("arn") String arn, @RequestBody Applicant applicant,
			BindingResult bindingResult) throws HttpSessionRequiredException {

		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		String channel = Iterables.get(userDetails.getRoles(), 0).getAuthority();

		/*final String fullFillment = UserController.rolesMap.get(webSecurity.getFFAGENT().toUpperCase());
		if ("FFAGENT".equalsIgnoreCase(fullFillment)) {
			applicationService.setEssentialDataPreFilledIndicator(essentialDataPreFilledIndicator, arn);
		}*/
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<Object>> violations = null;

		if ("CUSTOMER".equals(channel)) {
			violations = validator.validate(applicant, BranchJourneyGroup.class, CustomerJourneyGroup.class,
					Default.class);
		} else if ("BRANCH".equals(channel)) {
			violations = validator.validate(applicant, BranchJourneyGroup.class, Default.class);
		}

		if (CollectionUtils.isNotEmpty(violations)) {
			for (ConstraintViolation<Object> constraintViolation : violations) {
				String objectName = String.valueOf(constraintViolation.getPropertyPath());
				String fieldName = String.valueOf(constraintViolation.getPropertyPath());
				String defaultMessage = constraintViolation.getMessage();
				if (fieldName.contains("address")) {
					String addressArray = fieldName.substring(fieldName.indexOf("address"),
							fieldName.indexOf("address") + 10);
					int index = Integer
							.valueOf(addressArray.substring(addressArray.indexOf("[") + 1, addressArray.indexOf("]")));
					fieldName = applicant.getAdditionalInfo().getAddress().get(index)
							.getAddressType().getValue() + "|" + fieldName;
				}
				if (fieldName.contains("cards")) {
					String cardsArray = fieldName.substring(fieldName.indexOf("cards"), fieldName.indexOf("cards") + 8);
					int index = Integer
							.valueOf(cardsArray.substring(cardsArray.indexOf("[") + 1, cardsArray.indexOf("]")));
					fieldName = applicant.getCards().get(index).getKey() + "|" + fieldName;
				}
				FieldError fieldError = new FieldError(objectName, fieldName, defaultMessage);
				bindingResult.addError(fieldError);
			}
		}

		if (bindingResult.hasErrors()) {
			throw new InvalidRequestException("Invalid Request Body", bindingResult);
		}

		return applicantService.updateBeneficiary(arn, applicant);
	}

	@ApiOperation(value = "For an Primary applicant type it returns an array of documents without the image payload")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list of documents"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/{id}/applicants/PRIMARY/documents", method = RequestMethod.GET)
	public ResponseEntity<List<Document>> getAllDocuments(@PathVariable("id") String arn) {
		return new ResponseEntity<>(applicationService.getPrimaryApplicantDocuments(arn), HttpStatus.OK);
	}

	@ApiOperation(value = "For an Primary applicant type it returns an array of documents")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list of documents"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping(produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@RequestMapping(value = "/{id}/applicants/PRIMARY/documents/{DOCGROUP}/{IMAGEINDEX}", method = RequestMethod.GET)
	public ResponseEntity<Image> getPrimaryApplImage(@PathVariable("id") String arn,
			@PathVariable("DOCGROUP") String documentgroup, @PathVariable("IMAGEINDEX") long index) {
		return new ResponseEntity<>(applicationService.getPrimaryApplicantImages(arn, documentgroup, index),
				HttpStatus.OK);
	}

	@ApiOperation(value = "For a particular Supplimentary Applicant it returns a list of documents")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list of documents"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping(produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@RequestMapping(value = "/{id}/applicants/SUPPLEMENTARY/{supplementaryId}/documents", method = RequestMethod.GET)
	public ResponseEntity<List<Document>> getSupplementaryDocuments(@PathVariable("id") String idOfApplication,
			@PathVariable("supplementaryId") long supplimentaryId) {
		List<Document> suppApplicantDocBySuppId = applicationService.getSuppApplicantDocBySuppId(idOfApplication,
				supplimentaryId);
		return new ResponseEntity<>(suppApplicantDocBySuppId, HttpStatus.OK);
	}

	@ApiOperation(value = "For a particular Supplimentary Applicant it returns a list of documents")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list of documents"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping(produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@RequestMapping(value = "/{id}/applicants/SUPPLEMENTARY/{supplementaryId}/documents/{DOCGROUP}/{IMAGEINDEX}", method = RequestMethod.GET)
	public ResponseEntity<Image> getSupplementaryImageById(@PathVariable("id") String arn,
			@PathVariable("supplementaryId") long supplimentaryId, @PathVariable("IMAGEINDEX") long index,
			@PathVariable("DOCGROUP") String documentGroup) {
		Image suppApplicantImage = applicationService.getSuppApplicantImageById(arn, supplimentaryId, index,
				documentGroup);
		if (suppApplicantImage != null) {
			return new ResponseEntity<>(suppApplicantImage, HttpStatus.OK);
		}
		return ResponseEntity.ok(null);

	}

	@ApiOperation(value = "For a primary Applicant it saves a list of documents")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list of documents"),
			@ApiResponse(code = 500, message = "The saving of resource failed due to internal server error") })
	@RequestMapping(value = "/{id}/applicants/PRIMARY/documents/{DOCGROUP}", method = RequestMethod.POST)
	public List<Document> createPrimaryAplicantDocuments(@PathVariable("id") String idOfApplication,
			@PathVariable("DOCGROUP") String documentgroup, @RequestBody List<Document> documents,
			@RequestParam(value = "toCustomer", defaultValue = "false") String toCustomer)
			throws InvalidRequestDataException, HttpSessionRequiredException, SizeOverflowException, OcrException,
			IOException, ICAPException {
		HttpSession session = SessionUtil.session();
		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		boolean isApplicationShouldBeSentToCustomer = isApplicationShouldBeSentToCustomer(toCustomer);
		String channel = Iterables.get(userDetails.getRoles(), 0).getAuthority();
		String uid = userDetails.getUid();
		if (isApplicationShouldBeSentToCustomer)
			applicationService.updateApplicationStatus(idOfApplication, NgStatus.PENDINGWITHCUSTOMER, uid, channel);
		return applicationService.createPrimaryApplDoc(idOfApplication, documentgroup, documents, session);

	}

	@ApiOperation(value = "For a Supplementary Applicant it updates a list of documents")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated an image of a document"),
			@ApiResponse(code = 500, message = "The saving of resource failed due to internal server error") })
	@GetMapping(consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@RequestMapping(value = "/{id}/applicants/SUPPLEMENTARY/{supplementaryId}/documents/{DOCGROUP}", method = RequestMethod.POST)
	public void updateSupplementaryAplicantDocuments(@PathVariable("id") String idOfApplication,
			@PathVariable("supplementaryId") long supplementaryId, @PathVariable("DOCGROUP") String documentgroup,
			@RequestBody List<Document> documents) throws SizeOverflowException, IOException, ICAPException {
		applicationService.updateSupplementaryApplDoc(idOfApplication, supplementaryId, documentgroup, documents);
	}

	@ApiOperation(value = "For a primary Applicant it deletes an image from a document")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted an image"),
			@ApiResponse(code = 500, message = "The deleting of resource failed due to internal server error") })
	@RequestMapping(value = "/{id}/applicants/PRIMARY/documents/{DOCGROUP}/{IMAGEINDEX}", method = RequestMethod.DELETE)
	public void deletePrimaryAplicantDocuments(@PathVariable("id") String idOfApplication,
			@PathVariable("DOCGROUP") String documentgroup, @PathVariable("IMAGEINDEX") long index) {
		applicationService.deletePrimaryApplDoc(idOfApplication, documentgroup, index);
	}

	@ApiOperation(value = "It saves details of a supplemetary Applicant ")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Supplementary Applicant details sucessfully inserted"),
			@ApiResponse(code = 500, message = "The saving of resource failed due to internal server error") })
	@RequestMapping(value = "/{arn}/applicants/supplementary", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<List<Applicant>> saveSuppApplicant(@RequestBody List<Applicant> applicants, @RequestParam String action,
			@PathVariable String arn, BindingResult bindingResult) throws HttpSessionRequiredException {

		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		applicants.stream().forEach(applicant -> {
			Set<ConstraintViolation<Object>> violations = validator.validate(applicant, Default.class);
			if (CollectionUtils.isNotEmpty(violations)) {
				for (ConstraintViolation<Object> constraintViolation : violations) {
					String objectName = String.valueOf(constraintViolation.getPropertyPath());
					String fieldName = String.valueOf(constraintViolation.getPropertyPath());
					String defaultMessage = constraintViolation.getMessage();
					if (fieldName.contains("address")) {
						String addressArray = fieldName.substring(fieldName.indexOf("address"),
								fieldName.indexOf("address") + 10);
						int index = Integer
								.valueOf(addressArray.substring(addressArray.indexOf("[") + 1, addressArray.indexOf("]")));
						fieldName = applicant.getAdditionalInfo().getAddress().get(index)
								.getAddressType().getValue() + "|" + fieldName;
					}
					if (fieldName.contains("cards")) {
						String cardsArray = fieldName.substring(fieldName.indexOf("cards"), fieldName.indexOf("cards") + 8);
						int index = Integer
								.valueOf(cardsArray.substring(cardsArray.indexOf("[") + 1, cardsArray.indexOf("]")));
						fieldName = applicant.getCards().get(index).getKey() + "|" + fieldName;
					}
					FieldError fieldError = new FieldError(objectName, fieldName, defaultMessage);
					bindingResult.addError(fieldError);
				}
			}
		});

		if (bindingResult.hasErrors()) {
			throw new InvalidRequestException("Invalid Request Body", bindingResult);
		}

		List<Applicant> suppApplicants = applicantService.saveApplicant(applicants, arn);
		
		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		String channel = Iterables.get(userDetails.getRoles(), 0).getAuthority();
		String uid = userDetails.getUid();
		
		if ("customer".equalsIgnoreCase(channel) && "saveAndExit".equalsIgnoreCase(action)) {
			applicationService.applicationSaveAndExit(arn, uid, channel);
		}
		return new ResponseEntity<>(suppApplicants, HttpStatus.OK);
	}

	@ApiOperation(value = "It deletes a supplemetary applicant for provided arn and at provided index")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Supplementary Applicant deleted sucessfully"),
			@ApiResponse(code = 500, message = "The deletion of resource failed due to internal server error") })
	@RequestMapping(value = "/{arn}/applicants/supplementary/{index}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteSuppApplicant(@PathVariable String arn, @PathVariable int index) {
		applicantService.deleteSuppApplicant(arn, index);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@ApiOperation(value = "It deletes all supplemetary applicant for provided arn")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "All supplementary applicant deleted sucessfully"),
			@ApiResponse(code = 500, message = "The deletion of resource failed due to internal server error") })
	@RequestMapping(value = "/{arn}/applicants/supplementary/ALL", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAllSuppApplicant(@PathVariable String arn) {
		applicantService.deleteAllSuppApplicants(arn);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@ApiOperation(value = "For a supplementary Applicant it deletes an image from a document")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted an image"),
			@ApiResponse(code = 500, message = "The deleting of resource failed due to internal server error") })
	@RequestMapping(value = "/{id}/applicants/SUPPLEMENTARY/{supplementaryId}/documents/{DOCGROUP}/{IMAGEINDEX}", method = RequestMethod.DELETE)
	public void deleteSupplementaryAplicantDocuments(@PathVariable("id") String idOfApplication,
			@PathVariable("supplementaryId") long supplementaryId, @PathVariable("DOCGROUP") String documentgroup,
			@PathVariable("IMAGEINDEX") long imageIndex) {
		applicationService.deleteSupplementaryApplDoc(idOfApplication, supplementaryId, documentgroup, imageIndex);
	}

	private boolean isApplicationShouldBeSentToCustomer(String toCustomer) throws InvalidRequestDataException {
		try {
			return Boolean.parseBoolean(toCustomer) == Boolean.TRUE;
		} catch (Exception e) {
			throw new InvalidRequestDataException("Invalid toCustomer values!");
		}
	}

	@ApiOperation(value = "For an application save corresponding staff details")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully saved staff details"),
			@ApiResponse(code = 500, message = "The saving of resource failed due to internal server error") })
	@RequestMapping(value = "/{arn}/staff", method = RequestMethod.POST)
	public void saveApplicationStaffDetails(@PathVariable("arn") String arn, @RequestBody Application application,
			BindingResult bindingResult) throws HttpSessionRequiredException {

		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		String channel = Iterables.get(userDetails.getRoles(), 0).getAuthority();

		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<Object>> violations = null;

		if ("CUSTOMER".equals(channel)) {
			violations = validator.validate(application, BranchJourneyGroup.class, CustomerJourneyGroup.class,
					Default.class);
		} else if ("BRANCH".equals(channel)) {
			violations = validator.validate(application, BranchJourneyGroup.class, Default.class);
		}

		if (CollectionUtils.isNotEmpty(violations)) {
			for (ConstraintViolation<Object> constraintViolation : violations) {
				FieldError fieldError = new FieldError(String.valueOf(constraintViolation.getPropertyPath()),
						String.valueOf(constraintViolation.getPropertyPath()), constraintViolation.getMessage());
				bindingResult.addError(fieldError);
			}
		}

		if (bindingResult.hasErrors()) {
			throw new InvalidRequestException("Invalid Request Body", bindingResult);
		}

		ApplicationStaffDetails applicationStaffDetails = application.getStaffDetails();
		if (applicationStaffDetails != null) {
			applicationService.saveStaffDetails(arn, applicationStaffDetails);
		}
	}

	@ApiOperation(value = "When the fullFilment agent decides whether or not company Name is matched or not")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully saved"),
			@ApiResponse(code = 500, message = "The saving of resource failed due to internal server error") })
	@RequestMapping(value = "/{arn}/applicants/PRIMARY/documents/{DOCGROUP}/ECPF/{IMAGEINDEX}", method = RequestMethod.POST)
	public ResponseEntity<?> saveMatchedEcpfCompanyName(@PathVariable("arn") String arn,
			@RequestParam("isMatched") boolean isMatched) throws OcrException {

		HttpSession session = SessionUtil.session();
		OcrProcessedException saveMatchedEcpfCompanyNameException = ocrService.saveMatchedEcpfCompanyName(arn,
				isMatched, session);
		if (saveMatchedEcpfCompanyNameException != null && saveMatchedEcpfCompanyNameException.getException() != null) {
			throw saveMatchedEcpfCompanyNameException.getException();
		} else
			return new ResponseEntity<>(HttpStatus.OK);
	}

	public static String toJSON(Object object) {
		if (object == null) {
			return "{}";
		}
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(object);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{}";
	}

	@ApiOperation(value = "Returns application status for give id type and id value")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved application status"),
			@ApiResponse(code = 404, message = "No application found for this identification document") })
	@RequestMapping(value = "/{idtype}/{idvalue}/status", method = RequestMethod.GET)
	public ResponseEntity<ApplicationStatus> getApplicationStatus(@PathVariable("idtype") String idType,
			@PathVariable("idvalue") String idValue) {

		String status = applicationService.getApplicationStatus(idType, idValue);

		if (StringUtils.isEmpty(status)) {
			throw new ApplicationDoesNotExistException("No Application found for given identification documents");
		}

		NgStatus ngStatus = Arrays.asList(NgStatus.values()).stream().filter(a -> a.getValue().equalsIgnoreCase(status))
				.collect(Collectors.toList()).get(0);

		ApplicationStatus appStatus = new ApplicationStatus();
		appStatus.setStatus(ngStatus);

		return new ResponseEntity<ApplicationStatus>(appStatus, HttpStatus.OK);
	}

}
