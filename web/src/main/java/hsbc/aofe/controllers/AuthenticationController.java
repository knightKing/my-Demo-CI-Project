package hsbc.aofe.controllers;

import hsbc.aofe.config.CsrfTokenManager;
import hsbc.aofe.config.security.AuthenticatedUserDetails;
import hsbc.aofe.config.security.CsrfTokenImpl;
import hsbc.aofe.config.security.Roles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AuthenticationController {
    CsrfTokenManager csrfTokenManager = new CsrfTokenManager();
    CsrfTokenImpl csrf;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/userCredentials", method = RequestMethod.POST)
    @ResponseBody
    public AuthenticatedUserDetails authorizedUserDetails(@RequestParam(value = "username") String userName,
                                                          @RequestParam(value = "password") String password, HttpServletRequest request) {
        csrf = new CsrfTokenImpl();
        csrf.setToken(csrfTokenManager.generateToken());
        csrf.setHeaderName("X-CSRF-TOKEN");
        csrf.setParameterName("_csrf");
        AuthenticatedUserDetails authenticatedUserDetails = mockAuthenticatedUserDetails();
        authenticatedUserDetails.setCsrfToken(csrf);
        return authenticatedUserDetails;
    }

    public AuthenticatedUserDetails mockAuthenticatedUserDetails() {
        AuthenticatedUserDetails authenticatedUserDetails = new AuthenticatedUserDetails();
        Roles roles = new Roles();
        roles.setAuthority("POWERUSER");
        Roles roles1 = new Roles();
        roles1.setAuthority("ADMIN");
        List<Roles> users = new ArrayList<>();
        users.add(roles);
        users.add(roles1);
        authenticatedUserDetails.setPrincipalName("staff");
        authenticatedUserDetails.setRoles(users);
        return authenticatedUserDetails;
    }


}