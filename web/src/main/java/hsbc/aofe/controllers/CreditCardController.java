package hsbc.aofe.controllers;

import hsbc.aofe.config.SessionUtil;
import hsbc.aofe.domain.CreditCard;
import hsbc.aofe.service.CreditCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*****************************************
 * Created by sahil.verma on 6/6/2017.
 *****************************************
 */
@Api(value = "creditcard-api", description = "Operations pertaining to credit card", tags = {"Creditcard-Api"})
@RestController
@RequestMapping("config/cards/CREDIT")
public class CreditCardController {

    private CreditCardService creditCardService;

    @Autowired
    public CreditCardController(CreditCardService creditCardService) {
        this.creditCardService = creditCardService;
    }

    @ApiOperation(value = "Returns list of credit cards present in database")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list of arns"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @GetMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public List<CreditCard> getCreditCards() throws HttpSessionRequiredException {
    	if(!SessionUtil.isDigitallJourney()){
    		return creditCardService.getAllCreditCards();
    	}else{
    		return creditCardService.getCardsForDigitalJourney();
    	}
    }

}
