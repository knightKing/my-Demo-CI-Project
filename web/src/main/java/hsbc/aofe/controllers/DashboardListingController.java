package hsbc.aofe.controllers;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.ldap.userdetails.LdapAuthority;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Iterables;

import hsbc.aofe.config.SessionUtil;
import hsbc.aofe.domain.DashboardListing;
import hsbc.aofe.domain.ExportDocument;
import hsbc.aofe.domain.Remarks;
import hsbc.aofe.domain.RemarksType;
import hsbc.aofe.dto.UserDetails;
import hsbc.aofe.service.CreateExcelSheetService;
import hsbc.aofe.service.DashboardListingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Dashboard-api", description = "Operations related to a dashboard", tags = { "Dashboard-Api" })
@RestController
@RequestMapping("/dashboard")
public class DashboardListingController {

	private DashboardListingService dashboardListingService;
	private CreateExcelSheetService createExcelSheetService;

	@Autowired
	public DashboardListingController(DashboardListingService dashboardListingService,
			CreateExcelSheetService createExcelSheetService) {
		this.dashboardListingService = dashboardListingService;
		this.createExcelSheetService = createExcelSheetService;
	}

	@CrossOrigin
	@RequestMapping(value = "/applications/CONSICE", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public DashboardListing getAllApplications(
			@RequestParam(value = "sort", required = false, defaultValue = "applicationCreatedOn_DESC") List<String> sort,
			@RequestParam(value = "filter", required = false, defaultValue = "CHANNEL_eq_ALL , NGSTATUS_eq_ALL , AOSTATUS_eq_ALL") List<String> filter,
			@RequestParam(value = "pageOffSet", required = false, defaultValue = "1") Integer offset,
			@RequestParam(value = "pageLimit", required = false, defaultValue = "10") Integer pageLimit)
			throws HttpSessionRequiredException {
		if (offset == null) {
			offset = 1;
			pageLimit = 10;
		}

		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		Collection<LdapAuthority> authorities = userDetails.getRoles();

		String uid = "default";

		for (Iterator iterator = authorities.iterator(); iterator.hasNext();) {
			LdapAuthority authority = (LdapAuthority) iterator.next();

			if (!("POWERUSER".equalsIgnoreCase(authority.getAuthority())
					|| "FFAGENT".equalsIgnoreCase(authority.getAuthority()))) {
				uid = userDetails.getUid();
			}
		}
		return dashboardListingService.findAllApplications(sort, filter, uid, offset, pageLimit);
	}

	@ApiOperation(value = "It generates application listing documents ")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Document created sucessfully"),
			@ApiResponse(code = 500, message = "Document creation failed due to internal server error") })
	@RequestMapping(value = "/applications/EXPORT", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ExportDocument> createExcel(
			@RequestParam(value = "sort", required = false, defaultValue = "applicationCreatedOn_DESC") List<String> sortList,
			@RequestParam(value = "filter", required = false, defaultValue = "CHANNEL_eq_ALL , NGSTATUS_eq_ALL , AOSTATUS_eq_ALL") List<String> filterList,
			@RequestParam(value = "type", required = true) String type) throws HttpSessionRequiredException, IOException {
		
		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		Collection<LdapAuthority> authorities = userDetails.getRoles();

		String uid = "default";
		String userName = userDetails.getUid();
		String channelAuthority = null;

		for (Iterator iterator = authorities.iterator(); iterator.hasNext();) {
			LdapAuthority authority = (LdapAuthority) iterator.next();
			channelAuthority = authority.getAuthority();
			if (!("POWERUSER".equalsIgnoreCase(authority.getAuthority())
					|| "FFAGENT".equalsIgnoreCase(authority.getAuthority()))) {
				uid = userDetails.getUid();
			}
		}
		
		ExportDocument exportDocument = null;
		
		if("Excel".equalsIgnoreCase(type)){
			exportDocument = createExcelSheetService.createCompleteExcelSheet(sortList, filterList, uid, channelAuthority, userName);	
		}
		
		return new ResponseEntity<ExportDocument>(exportDocument, HttpStatus.OK);
	}

	@ApiOperation(value = "It saves remark for an arn ")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Remark Added sucessfully"),
			@ApiResponse(code = 500, message = "Addition of remark failed due to internal server error") })
	@RequestMapping(value = "/applications/{arn}/remarks/type/{type}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<?> addRemarks(@RequestBody Remarks remarks, @PathVariable String arn,
			@PathVariable RemarksType type) throws HttpSessionRequiredException {

		UserDetails userDetails = SessionUtil.getLoggedInUserDetails();
		String channel = Iterables.get(userDetails.getRoles(), 0).getAuthority();
		String uid = userDetails.getUid();
		if (CollectionUtils.isNotEmpty(remarks.getRemark())) {
			for (int i = 0; i < remarks.getRemark().size(); i++) {
				if (remarks.getRemark().get(i) == null || remarks.getRemark().get(i).trim().isEmpty()
						|| remarks.getRemark().get(i).length() > 255) {
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				}
			}
			if (!type.equals(RemarksType.FOR_FA_SEND_TO_CUSTOMER) && remarks.getRemark().size() > 1) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else if (type.equals(RemarksType.FOR_FA_SEND_TO_CUSTOMER) && remarks.getRemark().size() > 5) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else {
				if (RemarksType.FOR_FA_SEND_TO_CUSTOMER.equals(type)) {
					dashboardListingService.callIccm(arn, type, uid, channel, remarks.getRemark());
				}
				dashboardListingService.addRemarks(arn, remarks, type);
				return new ResponseEntity<>(HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "It provides list of remarks for an arn for a provided type")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Remark sucessfully retrieved"),
			@ApiResponse(code = 500, message = "Couldn't retrieve remark due to internal server error") })
	@RequestMapping(value = "/applications/{arn}/remarks/type/{type}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<List<Remarks>> getRemarksByType(@PathVariable String arn, @PathVariable RemarksType type) {
		List<Remarks> remarks = dashboardListingService.getRemarksByType(arn, type);
		return new ResponseEntity<>(remarks, HttpStatus.OK);
	}

	@ApiOperation(value = "It updates assignee name for an arn ")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Assignee Name updated sucessfully"),
			@ApiResponse(code = 500, message = "Updation of assignee name failed due to internal server error") })
	@RequestMapping(value = "/applications/{arn}/FULFILMENTAGENT/", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<?> updateFullfilmentAgent(@RequestBody String fulfilmentAgent, @PathVariable String arn) {
		String fulfilmentAgentName = dashboardListingService.updateFulfilmentAgent(fulfilmentAgent, arn);
		return ResponseEntity.ok(fulfilmentAgentName);
	}
}
