package hsbc.aofe.controllers;

import hsbc.aofe.domain.Document;
import hsbc.aofe.service.OcrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ocr")
public class DocumentUploadController {

	@Autowired
	OcrService ocrService;

	@RequestMapping(value = "/document", method = RequestMethod.POST)
	public List<Document> getExtractedDataOFImage(@RequestBody List<Document> document) {
		for (Document doc : document) {
			String docType = doc.getDocName().getValue();
			if (docType.equalsIgnoreCase("NRIC")) {
				ocrService.getOcrdData(doc.getImages(), docType);
				return document;
			} else if (docType.equalsIgnoreCase("EMPLOYMENT_PASS")) {
				ocrService.getOcrdData(doc.getImages(), docType);
				return document;
			}
		}
		return document;
	}
}
