package hsbc.aofe.controllers;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*****************************************
 * Created by sahil.verma
 *****************************************
 */

@Api(value = "log-api", description = "Get application logs", tags = "LOG-Api")
@RestController
@RequestMapping("config/")
public class LogController {

    @ApiOperation(value = "Returns application logs")
    @GetMapping(value = "app/logs/{numberOfLines}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getApplicationLogs(@RequestParam(value = "profile", defaultValue = "development") String profile,
                                                @RequestParam(value = "ao", defaultValue = "false") String isAoLogsRequired,
                                                @PathVariable("numberOfLines") String numberOfLinesToFetch) {
        StringBuilder applicationLogs = new StringBuilder();
        StringBuilder filePath = getFilePath(profile, isAoLogsRequired);
        try (Stream<String> stream = Files.lines(Paths.get(filePath.toString()))) {
            ArrayList<String> lines = stream.collect(Collectors.toCollection(ArrayList::new));
            Collections.reverse(lines);
            List<String> linesToBeReturned = lines.stream()
                    .limit(Long.valueOf(numberOfLinesToFetch))
                    .collect(Collectors.toList());
            Collections.reverse(linesToBeReturned);
            linesToBeReturned.stream()
                    .forEach(line -> applicationLogs.append(line).append("\n"));
        } catch (IOException e) {
            // left blank intentionally
        }
        return ResponseEntity.ok(applicationLogs);
    }

    private StringBuilder getFilePath(String profile, String isAoLogsRequired) {
        StringBuilder filePath = new StringBuilder(".").append(File.separator).append("logs")
                .append(File.separator).append("aofe").append(File.separator).append(profile).append(File.separator).append(profile);

        if (isAoLogsRequired.equalsIgnoreCase("true")) {
            filePath.append("_app_ao.log");
        } else {
            filePath.append("_app.log");
        }
        return filePath;
    }

}
