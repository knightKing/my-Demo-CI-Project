package hsbc.aofe.controllers;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import hsbc.aofe.config.SessionUtil;
import hsbc.aofe.dto.Otp;
import hsbc.aofe.dto.UserDetails;
import hsbc.aofe.exception.ApplicationDoesNotExistException;
import hsbc.aofe.service.OtpService;
import hsbc.aofe.service.entities.OtpUser;
import hsbc.aofe.service.jparepository.OtpRepository;


@RestController
@RequestMapping("/otp")
public class OtpController {

    private OtpService otpService;
    private OtpRepository otpRepository;

    @Autowired
    public OtpController(OtpService otpService,
                         OtpRepository otpRepository) {
        this.otpService = otpService;
        this.otpRepository = otpRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void genrateOtp(@RequestBody Otp otp) throws InvalidKeyException, NoSuchAlgorithmException {
        otpService.generateOneTimePassword(otp.getIdType().getValue(), otp.getIdValue(), otp.getDialCodeMobile(), otp.getMobile(), otp.getEmail());
    }


    @PostMapping(value = "/VALIDATE")
    public Otp validate(@RequestBody Otp otp) {

        //Since OTP service is to be used by customer only, we are setting role = customer when it validates.
        String role = "CUSTOMER";

        otp.setArn(otpService.otpValidate(otp.getIdType().getValue(), otp.getIdValue(), otp.getDialCodeMobile(), otp.getMobile(), otp.getEmail(), otp.getOtp()));


        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(role));

        org.springframework.security.core.userdetails.User anonUser = new User(role, "", authorities);

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(anonUser, "", authorities);
        // token.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(token);

        UserDetails details = new UserDetails("CUSTOMER", "", authorities);
        details.setRoles(UserController.createUserRoles(authorities));
        SessionUtil.session().setAttribute("loggedInUser", details);
        otp.setUser(details);

        return otp;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getOtp(@RequestParam("idType") String idType,
                         @RequestParam("idValue") String idValue,
                         @RequestParam("dialCodeMobile") String dialCodeMobile,
                         @RequestParam("mobile") Long mobile,
                         @RequestParam("email") String email) {
        String arn = otpService.getApplicationToResume(idType, idValue, dialCodeMobile, mobile, email);
        OtpUser otpUser = otpRepository.findByArn(arn);
        if (otpUser == null) {
            throw new ApplicationDoesNotExistException("Application for " + arn + " does not exist.");
        }
        return toJSON(otpUser.getOtp());
    }

    private String toJSON(Object object) {
        if (object == null) {
            return "{}";
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "{}";
    }

}
