package hsbc.aofe.controllers;


import hsbc.aofe.qas.service.HSBCQasService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*****************************************
 * Created by NEWGEN SOFTWARE LTD.
 *****************************************
 */

@Api(value = "qas-api", description = "Operations pertaining to QAS module", tags = "QAS-Api")
@RestController
@RequestMapping("qas/address")
@Slf4j
public class QASController {

    private HSBCQasService hsbcQasService;

    @Autowired
    public QASController(HSBCQasService hsbcQasService) {
        this.hsbcQasService = hsbcQasService;
    }

    @ApiOperation(value = "Returns address for a given postal code")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved address for given postal code"),
            @ApiResponse(code = 204, message = "No Address found for given postal code")
    })
    @GetMapping(value = "/postalcode/{searchForPostalCode}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAddressPickList(@PathVariable("searchForPostalCode") String searchForPostalCode) throws Exception {
        log.debug("AddressController Controller invocked with postal-code {}.", searchForPostalCode);
        String addressSuggestion = hsbcQasService.getAddressFor(searchForPostalCode);
        if (addressSuggestion == null || addressSuggestion.isEmpty()) {
            log.debug("No address suggestion found for postal-code {}.", searchForPostalCode);
            return ResponseEntity.noContent().build();
        }
       return ResponseEntity.ok(addressSuggestion);
    }

}
