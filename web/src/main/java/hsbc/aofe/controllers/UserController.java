package hsbc.aofe.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import hsbc.aofe.config.SessionUtil;
import hsbc.aofe.config.WebSecurity;
import hsbc.aofe.dto.UserDetails;
import hsbc.aofe.exception.ProbableRoboticSpamException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.http.MethodNotSupportedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.ldap.userdetails.LdapAuthority;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


@RestController
@RequestMapping("/users")
@Slf4j
@Component
public class UserController {

	@Autowired
	WebSecurity webSecurity;
	
	protected static BidiMap<String, String> rolesMap;
	
	@RequestMapping(value = "/me", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public UserDetails getLoggedInUserDetails(UsernamePasswordAuthenticationToken token, ModelMap model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		UserDetails details;

		if (webSecurity.getENV().equalsIgnoreCase("ldap")) {
			log.debug("connected to LDAP");
			LdapUserDetailsImpl ldapUserDetails = (LdapUserDetailsImpl) auth.getPrincipal();

			Collection<GrantedAuthority> authorities = ldapUserDetails.getAuthorities();

			//System.out.println(auth.getPrincipal());

			details = new UserDetails(ldapUserDetails.getDn(), "", ldapUserDetails.getAuthorities());
			details.setRoles(createUserRoles(authorities));
			details.setUid(parseUsername(details.getUsername()));
			log.debug(details.toString());
		} else if (webSecurity.getENV().equalsIgnoreCase("inmemory")) {
			log.debug("connected to Inmemory");
			
			org.springframework.security.core.userdetails.User ldapUserDetails = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
			
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.addAll(ldapUserDetails.getAuthorities());
			
			
			org.springframework.security.core.userdetails.User user = new User(ldapUserDetails.getUsername(), "", authorities);
			
			details = new UserDetails(user.getUsername(), "", authorities);
			details.setRoles(createUserRoles(authorities));
			details.setUid(parseUsername(details.getUsername()));
			log.debug(details.toString());
		}else{
			details = SessionUtil.getAnonymousUserDetails();
			log.debug(details.toString());
		}
		SessionUtil.session().setAttribute("loggedInUser", details);

		//System.out.println(SessionUtil.session().getAttribute("loggedInUser"));

		return details;

	}
	
	@RequestMapping(value = "/login/ANONYMOUS", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public UserDetails setLoggedInUserDetails(@RequestParam String role) throws MethodNotSupportedException {
		log.debug("doing anon authentication");
		if(role.equalsIgnoreCase("TPSA") || role.equalsIgnoreCase("CUSTOMER")){

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(role));

		org.springframework.security.core.userdetails.User anonUser = new User(role, "", authorities);

		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(anonUser, "", authorities);
		// token.setAuthenticated(true);
		SecurityContextHolder.getContext().setAuthentication(token);

		UserDetails details = new UserDetails(role, "", authorities);
		details.setRoles(createUserRoles(authorities));
		
		SessionUtil.session().setAttribute("loggedInUser", details);
		log.debug(details.toString());
		return details;
		}
		else{
			log.debug("no anon for this role is allowed.");
			throw new MethodNotSupportedException("Not allowed");
		}
	}
	

	@RequestMapping(value = "/login/failure", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<String> failureHandler() throws AuthenticationException {
		
		throw new AuthenticationException("Authentication Failed") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 4974885683404275344L;
		};
	}

	
	@RequestMapping(value = "/robotic/failure", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<String> roboticSpamHandler() throws AuthenticationException {
		
		throw new ProbableRoboticSpamException("Authentication Failed") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 4974885683404275344L;
		};
	}
	
	public static String toJSON(Object object) {
	        if (object == null) {
	            return "{}";
	        }
	        try {
	            ObjectMapper mapper = new ObjectMapper();
	            return mapper.writeValueAsString(object);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return "{}";
	    }
	
	public static List<LdapAuthority> createUserRoles(Collection<GrantedAuthority> authorities) {
		log.debug("in createUserRoles method, input param "+authorities);

		List<LdapAuthority> tempAuthList = new ArrayList<LdapAuthority>();

		for (Iterator<GrantedAuthority> iterator = authorities.iterator(); iterator.hasNext();) {
			GrantedAuthority grantedAuthority = (GrantedAuthority) iterator.next();
			log.debug("granted Authority:"+grantedAuthority);
			String tempAuth = null==grantedAuthority.getAuthority()?grantedAuthority.toString():grantedAuthority.getAuthority().toString();
			log.debug("temp auth:"+tempAuth);
			if (null == rolesMap.get(tempAuth.toUpperCase())) {
				continue;
			}
			LdapAuthority ldapAuthority = new LdapAuthority(rolesMap.get(tempAuth), tempAuth);
			tempAuthList.add(ldapAuthority);
		}
		log.debug("existing from createUserRoles method, auth list is "+tempAuthList);
		return tempAuthList;
	}

	private BidiMap<String, String> createRolesMap() {
		BidiMap<String, String> bidiMap = new DualHashBidiMap<>();
		bidiMap.put(webSecurity.getBRANCH().toUpperCase(), "BRANCH");
		bidiMap.put(webSecurity.getTELESALES().toUpperCase(), "TELE_SALES");
		bidiMap.put(webSecurity.getROADSHOW().toUpperCase(), "ROAD_SHOW");
		bidiMap.put(webSecurity.getFFAGENT().toUpperCase(), "FFAGENT");
		bidiMap.put(webSecurity.getPOWERUSER().toUpperCase(), "POWERUSER");
		bidiMap.put(webSecurity.getTPSA().toUpperCase(), "TPSA");
		bidiMap.put(webSecurity.getCUSTOMER().toUpperCase(), "CUSTOMER");
		bidiMap.put(webSecurity.getANONYMOUS().toUpperCase(), "ANONYMOUS");

		return bidiMap;
	}

	private String parseUsername(String uid) {
		String[] splitArray = uid.split("[,=]");
		try {
			return splitArray[1];

		} catch (ArrayIndexOutOfBoundsException excp) {
			return uid;
		}
	}

	@PostConstruct
	public void init() {
		this.rolesMap = createRolesMap();
	}

}
