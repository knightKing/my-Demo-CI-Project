package hsbc.aofe.dto;

import hsbc.aofe.domain.NgStatus;
import lombok.Data;

@Data
public class ApplicationStatus {
	private NgStatus status;	
}
