package hsbc.aofe.dto;

import lombok.Data;

@Data
public class FieldErrorResource {
	private String field;
	private String message;
}
