package hsbc.aofe.dto;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import hsbc.aofe.domain.DocumentName;
import lombok.Data;

@Data
@JsonIgnoreProperties
public class Otp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1871792063193704901L;

	private DocumentName idType;
	private String idValue;
	private String dialCodeMobile;
	private Long mobile;
	private String email;
	private String otp;
	private String arn;
	private UserDetails user;
	
	
}
