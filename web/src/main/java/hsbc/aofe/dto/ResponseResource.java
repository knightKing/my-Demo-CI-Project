package hsbc.aofe.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
public class ResponseResource<T> {
	private int status;
	private List<FieldErrorResource> fieldErrors;

	public ResponseResource(HttpStatus status) {
		this.status = status.value();
	}

}
