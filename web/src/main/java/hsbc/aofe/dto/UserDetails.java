package hsbc.aofe.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthority;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
public class UserDetails extends org.springframework.security.core.userdetails.User {

	/**
	   *
	   */
	private static final long serialVersionUID = -6345573046846184353L;
	
	public UserDetails(String username, String password, Collection<GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	// for example lets add some person data
	private String fullName;
	private String uid;

	Collection<LdapAuthority> roles;

}
