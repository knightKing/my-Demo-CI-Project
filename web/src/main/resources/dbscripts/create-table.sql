--CREATE TABLE
    create table ADDRESS (
        id number(19,0) generated as identity,
        city varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        doorNumber varchar2(255 char),
        l2Street varchar2(255 char),
        l3Unit varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        postalCode varchar2(255 char),
        country number(19,0),
        applicantId number(19,0),
        primary key (id)
    );

    create table ADDRESSTYPEMASTER (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        remarks varchar2(255 char),
        type varchar2(255 char),
        primary key (id)
    );

    create table APPLICANT_ADDRESS (
        CREATED_BY varchar2(255 char),
        CREATED_ON timestamp,
        LAST_MODIFIED_BY varchar2(255 char),
        LAST_MODIFIED_ON timestamp,
        applicantId number(19,0) not null,
        addressId number(19,0) not null,
        applicationId number(19,0) not null,
        type number(19,0),
        primary key (addressId, applicantId, applicationId)
    );

    create table APPLICANT_DOCUMENT (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        noOfImages number(19,0) not null,
        remarks varchar2(255 char),
        documentType number(19,0),
        applicantId number(19,0),
        primary key (id)
    );
	
    create table APPLICANTADDITIONALINFO (
        id number(19,0) generated as identity,
        annualIncome number(19,0),
        ASSOCIATEOFPUBPOSITIONHOLDER varchar2(255 char),
        ASSOCIATEOFPUBPOSHOLDETAILS varchar2(255 char),
        autoDebitFromAccountNumber varchar2(255 char),
        companyName varchar2(255 char),
        correspondenceOn varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        dialCodeHomePhone varchar2(255 char),
        holdingPublicPosition varchar2(255 char),
        homePhone number(19,0),
        industryType varchar2(255 char),
        introducedByHSBCCardHolder varchar2(255 char),
        INTRODUCEDBYHSBCREFID varchar2(255 char),
        isMultipleNationality varchar2(255 char),
        ISRESIADDSAMEASPRIMARY varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        lengthOfService number(19,0),
        maritalStatus varchar2(255 char),
        monthsAtResidentialAddress number(19,0),
        monthsToEmpPassExpiry number(19,0),
        mother_MaidenName varchar2(255 char),
        nationality2 varchar2(255 char),
        nationality3 varchar2(255 char),
        noOfDependents number(19,0),
        occupation varchar2(255 char),
        officeAddress varchar2(255 char),
        officeExtn number(19,0),
        officePhone number(19,0),
        officePostalCode number(19,0),
        PASSPORT_NUMBER varchar2(255 char),
        position varchar2(255 char),
        publicPositionDetails varchar2(255 char),
        IS_RES_ADDS_SAMEAS_PERMANENT varchar2(255 char),
        taxResidenceCountry1 varchar2(255 char),
        taxResidenceCountry2 varchar2(255 char),
        taxResidenceCountry3 varchar2(255 char),
        timeAtPreviousEmployer number(19,0),
        verifiedIncome number(10,0) not null,
        ApplicantId number(19,0),
        educationlevel number(19,0),
        homeownership number(19,0),
        employmenttype number(19,0),
        PASSPORT_COUNTRY number(19,0),
        primary key (id)
    );

    create table APPLICANTDETAILS (
        id number(19,0) generated as identity,
        balanceTransferOnCreditCard varchar2(255 char),
        consentAccountOpening varchar2(255 char),
        consentContactMe varchar2(255 char),
        consentMarketing varchar2(255 char),
        consentPersonalData varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        dateOfBirth date,
        dialCodeMobile varchar2(255 char),
        email varchar2(255 char),
        firstName varchar2(255 char),
        gender varchar2(255 char),
        idExpiryDate date,
        idIssueDate date,
        idValue varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        lastName varchar2(255 char),
        middleName varchar2(255 char),
        mobile number(19,0),
        nationality varchar2(255 char),
        otherName varchar2(255 char),
        salutation varchar2(255 char),
        sharedCreditLimit varchar2(255 char),
        countryOfBirth number(19,0),
        countryOfIdIssue number(19,0),
        idDoctype number(19,0),
        primary key (id)
    );

    create table APPLICATIONSTATUSTIMELINE (
        id number(19,0) generated as identity,
        ao_status varchar2(255 char),
        assignedTo varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        remarks varchar2(255 char),
        applicationId number(19,0),
        ngStatus number(19,0),
        primary key (id)
    );

    create table ARNCCBRIDGE (
        acTfrBeneficiaryAcName varchar2(255 char),
        acTfrBeneficiaryAcNumber number(19,0),
        acTfrBeneficiaryBankName varchar2(255 char),
        amtToBeTransfered number(20,0),
        bankToAssignCreditLimit varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        isSelected varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        limit number(19,0),
        nameOnCard varchar2(255 char),
        applicantId number(19,0) not null,
        ccId number(19,0) not null,
        ccIssued number(19,0),
        primary key (applicantId, ccId)
    );

    create table CCAPPLICATION (
        id number(19,0) generated as identity,
        aoRefNum varchar2(255 char),
        aoSubmissionDate timestamp,
        ao_status varchar2(255 char),
        arn varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        fulfilmentAgent varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        referalId varchar2(255 char),
        applicantId number(19,0),
        initiatedBy number(19,0),
        primary key (id)
    );

    create table CCAPPLICATIONADDITIONALINFO (
        id number(19,0) generated as identity,
        agreeToGenericConsent varchar2(255 char),
        allocateExistingLimitToNew varchar2(255 char),
        atmLinkedAccount varchar2(255 char),
        createAutoDebitFromAccount varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        employmentCheckIndicator varchar2(255 char),
        existingCcnumber varchar2(255 char),
        howMuchAutoDebit number(19,0),
        incomeAutomatedCalcIndicator varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        receiveEStatement varchar2(255 char),
        transactingOnMyOwn varchar2(255 char),
        wouldLikeToOpenNewAccount varchar2(255 char),
		documentCompleteIndicator varchar2(255 char),
        ApplicationId number(19,0),
        primary key (id)
    );
 
    create table CHANNELMASTER (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        name varchar2(255 char),
        remarks varchar2(255 char),
        primary key (id)
    );

    create table COUNTRYLISTMASTER (
        id number(19,0) generated as identity,
        code varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn date,
        lastModifiedBy varchar2(255 char),
        lastModifiedOn date,
        name varchar2(255 char),
        primary key (id)
    );

    create table CREDITCARDMASTER (
        ID number(19,0) generated as identity,
        ANNUAL_FEE number(19,0),
        ANNUAL_FEE_CURRENCY varchar2(10 char),
        COMPLIMENTARY_STUFF varchar2(20 char),
        CREATEDBY varchar2(100 char),
        CREATEDON timestamp,
        ELIGIBLEFORCUSTOMER varchar2(255 char),
        IMAGEURL varchar2(500 char),
        KEY varchar2(10 char),
        LASTMODIFIEDBY varchar2(100 char),
        LASTMODIFIEDON timestamp,
        LIMIT number(19,0),
        LIMIT_MINIMUM_OF_INFO number(19,0),
        MIN_SALARY number(19,0),
        NAME varchar2(500 char),
        REWARD_POINT_SCALE number(19,0),
        REWARD_POINTS number(19,0),
        TYPE varchar2(300 char),
        primary key (ID)
    );
 
    create table DOCGROUPMASTER (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        name varchar2(255 char),
        remarks varchar2(255 char),
        primary key (id)
    );

    create table DOCUMENT_EXTRACTION (
        id number(19,0) generated as identity,
        appDocId number(19,0),
        changedBy varchar2(255 char),
        changedOn timestamp,
        changedValue varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        extractedValue varchar2(255 char),
        fieldName varchar2(255 char),
        isChanged varchar2(255 char),
        primary key (id)
    );

    create table DOCUMENTSMASTER (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn date,
        extractionRequired varchar2(255 char),
        key varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn date,
        docGroup number(19,0),
        primary key (id)
    );

    create table EDUCATIONALLEVELMASTER (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        label varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        remarks varchar2(255 char),
        primary key (id)
    );

    create table EVENTMASTER (
        id number(19,0) generated as identity,
        event_Key varchar2(255 char),
        template_Id number(19,0),
        primary key (id)
    );

    create table FIELDMASTER (
        id number(19,0) generated as identity,
        FIELD_NAME varchar2(255 char),
        primary key (id)
    );

    create table HOMEOWNERSHIPMASTER (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        label varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        remarks varchar2(255 char),
        primary key (id)
    );

    create table ICCM_EVENT (
        id number(19,0) generated as identity,
        createdOn timestamp,
        RETRY_COUNT number(19,0),
        SEND_WHEN timestamp,
        STAFF_ID varchar2(255 char),
        status number(10,0),
        TRACKING_FIELD varchar2(255 char),
        ARN varchar2(255 char),
        INITIATED_BY number(19,0),
        EVENT_ID number(19,0),
        primary key (id)
    );
    
    create table ICCM_EVENT_DATA (
        id number(19,0) generated as identity,
        value varchar2(255 char),
        TEMPLATE_FIELD_ID number(19,0),
        ICCM_EVENT_ID number(19,0),
        primary key (id)
    );

    create table IMAGE (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        imageIndex number(19,0) not null,
        isExtracted varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        name varchar2(255 char),
        photo blob,
        documentId number(19,0),
        primary key (id)
    );

    create table IPACCESSDETAILS (
        id number(19,0) generated as identity,
        accessTime timestamp,
        ipAddress varchar2(255 char),
        url varchar2(255 char),
        primary key (id)
    );
 
    create table IPBLOCKSTATUS (
        id number(19,0) generated as identity,
        ipAddress varchar2(255 char),
        isBlocked varchar2(255 char),
        primary key (id)
    );

    create table IUpload (
        id number(19,0) generated as identity,
        arn varchar2(255 char),
        createdon timestamp,
        foldername varchar2(255 char),
        uploadstatus varchar2(255 char),
        primary key (id)
    );

    create table JOBTYPEMASTER (
        id number(19,0) generated as identity,
        code varchar2(255 char),
        createdBy varchar2(255 char),
        createdOn timestamp,
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        name varchar2(255 char),
        primary key (id)
    );

    create table NGSTATUSMASTER (
        id number(19,0) generated as identity,
        createdBy varchar2(255 char),
        createdOn timestamp,
        key varchar2(255 char),
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        remarks varchar2(255 char),
        primary key (id)
    );

    create table OTP (
        id number(10,0) generated as identity,
        arn varchar2(255 char),
        TIME timestamp,
        flag varchar2(255 char),
        OTPGEN varchar2(255 char),
        primary key (id)
    );

    create table REMARKS (
        id number(19,0) generated as identity,
        dateTime varchar2(255 char),
        remarks varchar2(255 char),
        staffId varchar2(255 char),
        type varchar2(255 char),
        arn varchar2(255 char),
        primary key (id)
    );

    create table STAFFDETAILS (
        id number(19,0) generated as identity,
        ecpfAuthorized varchar2(255 char),
        ghoCode varchar2(255 char),
        location varchar2(255 char),
        marketSectorCode varchar2(255 char),
        preApprovedLimit number(19,0),
        referralId varchar2(255 char),
        sourceCode varchar2(255 char),
        splInstIndicator varchar2(255 char),
        staffId varchar2(255 char),
        voucherId varchar2(255 char),
        ApplicationId number(19,0),
        primary key (id)
    );

    create table SUPPAPPLICANTS (
        createdBy varchar2(255 char),
        createdOn timestamp,
        lastModifiedBy varchar2(255 char),
        lastModifiedOn timestamp,
        relationWithPrimaryApplicant varchar2(255 char),
        applicantId number(19,0) not null,
        applicationId number(19,0) not null,
        primary key (applicantId, applicationId)
    );

    create table TEMPLATEFIELDSMASTER (
        id number(19,0) generated as identity,
        field_Id number(19,0),
        template_Id number(19,0),
        primary key (id)
    );
 
    create table TEMPLATEMASTER (
        id number(19,0) generated as identity,
        key varchar2(255 char),
        primary key (id)
    );
    
    CREATE TABLE sghao_appl 
  ( 
     arr_id_app      VARCHAR2(30), 
     prod_stat_cde   VARCHAR2(10), 
     prod_incl_dt_tm TIMESTAMP(6), 
     prod_appl_cde   VARCHAR2(10), 
     prod_acpt_cde   VARCHAR2(10) 
  ); 
	
-- ADD CONSTRAINTS
	
    alter table CCAPPLICATION 
        add constraint UK_soyhjwny5v904icrajdnxsqfv unique (arn);

    alter table ADDRESS 
        add constraint FK20e6s45xr6myk8oduyjeo4xqd 
        foreign key (country) 
        references COUNTRYLISTMASTER;

    alter table ADDRESS 
        add constraint FKb33lswt1jbvbsyseahqeqrkqt 
        foreign key (applicantId) 
        references APPLICANTDETAILS;

    alter table APPLICANT_ADDRESS 
        add constraint FKc2y1tp1bvbc0vuda0yghdujra 
        foreign key (applicantId) 
        references APPLICANTDETAILS;

    alter table APPLICANT_ADDRESS 
        add constraint FKtepbkrbfu89ldkac0duhaayqg 
        foreign key (addressId) 
        references ADDRESS;

    alter table APPLICANT_ADDRESS 
        add constraint FK53v2yioa6o9aagtyd6sy2m8m7 
        foreign key (applicationId) 
        references CCAPPLICATION;

    alter table APPLICANT_ADDRESS 
        add constraint FK8g7glbmgud1bv8pmd7csefoyt 
        foreign key (type) 
        references ADDRESSTYPEMASTER;

    alter table APPLICANT_DOCUMENT 
        add constraint FK6hcyhbbde9m3gh9hctoya8nuj 
        foreign key (documentType) 
        references DOCUMENTSMASTER;

    alter table APPLICANT_DOCUMENT 
        add constraint FKguj4ejgg246g4pehhuibcowkg 
        foreign key (applicantId) 
        references APPLICANTDETAILS;

    alter table APPLICANTADDITIONALINFO 
        add constraint FKjc5eo3ct0873ewpyt4edfrs1g 
        foreign key (ApplicantId) 
        references APPLICANTDETAILS;

    alter table APPLICANTADDITIONALINFO 
        add constraint FKsukdxnlqnccb3c05sggqs7h5l 
        foreign key (educationlevel) 
        references EDUCATIONALLEVELMASTER;

    alter table APPLICANTADDITIONALINFO 
        add constraint FKehovbyk528jnf6ididi5cuula 
        foreign key (homeownership) 
        references HOMEOWNERSHIPMASTER;

    alter table APPLICANTADDITIONALINFO 
        add constraint FKdsd08964ktspg7cx4v11dw9xv 
        foreign key (employmenttype) 
        references JOBTYPEMASTER;

    alter table APPLICANTADDITIONALINFO 
        add constraint FKheomnfgs7w9di59itet4k0l8f 
        foreign key (PASSPORT_COUNTRY) 
        references COUNTRYLISTMASTER;

    alter table APPLICANTDETAILS 
        add constraint FK9xesm0b2kjfcag773iogsafk8 
        foreign key (countryOfBirth) 
        references COUNTRYLISTMASTER;

    alter table APPLICANTDETAILS 
        add constraint FK9s94w3ntv8x18556xpaasnls8 
        foreign key (countryOfIdIssue) 
        references COUNTRYLISTMASTER;

    alter table APPLICANTDETAILS 
        add constraint FKt7v8xe35q4ucu1wqe1x2pnute 
        foreign key (idDoctype) 
        references DOCUMENTSMASTER;

    alter table APPLICATIONSTATUSTIMELINE 
        add constraint FKpdmv7hb2hqmem4o1nlheuws 
        foreign key (applicationId) 
        references CCAPPLICATION;

    alter table APPLICATIONSTATUSTIMELINE 
        add constraint FKlp53vrwr4qy83y2sco885f80e 
        foreign key (ngStatus) 
        references NGSTATUSMASTER;

    alter table ARNCCBRIDGE 
        add constraint FKdd8f4ghn5m1glol775y94bhvt 
        foreign key (applicantId) 
        references APPLICANTDETAILS;

    alter table ARNCCBRIDGE 
        add constraint FKebck0c8a2rwungfms3t9ewip 
        foreign key (ccId) 
        references CREDITCARDMASTER;

    alter table ARNCCBRIDGE 
        add constraint FK7jvh7shvf2up75up3j2rgf0ex 
        foreign key (ccIssued) 
        references CREDITCARDMASTER;

    alter table CCAPPLICATION 
        add constraint FKqpy31w6tt8nxy6kvjx81i67m2 
        foreign key (applicantId) 
        references APPLICANTDETAILS;

    alter table CCAPPLICATION 
        add constraint FKhfb1o7k8ighr9ahb4q69x8yxc 
        foreign key (initiatedBy) 
        references CHANNELMASTER;

    alter table CCAPPLICATIONADDITIONALINFO 
        add constraint FKo5crlnmt62wpxui4uximnecev 
        foreign key (ApplicationId) 
        references CCAPPLICATION;

    alter table DOCUMENT_EXTRACTION 
        add constraint FKofaes3w8mj7w30t80csvge3og 
        foreign key (appDocId) 
        references APPLICANT_DOCUMENT;

    alter table DOCUMENTSMASTER 
        add constraint FKot4qo9awt2kiy96sa9y9gri92 
        foreign key (docGroup) 
        references DOCGROUPMASTER;

    alter table EVENTMASTER 
        add constraint FKjl0vugs814krkywofxgsug2h9 
        foreign key (template_Id) 
        references TEMPLATEMASTER;

    alter table ICCM_EVENT 
        add constraint FK2pfj4qoy9avqpjicmhiijxpaf 
        foreign key (ARN) 
        references CCAPPLICATION (arn);

    alter table ICCM_EVENT 
        add constraint FK17b6d6hh340wtgbicp94wojn5 
        foreign key (INITIATED_BY) 
        references CHANNELMASTER;

    alter table ICCM_EVENT 
        add constraint FK9trgi9au5a7f8keguimeuem7p 
        foreign key (EVENT_ID) 
        references EVENTMASTER;

    alter table ICCM_EVENT_DATA 
        add constraint FK666jxjqbde8gkferqr9aatify 
        foreign key (TEMPLATE_FIELD_ID) 
        references TEMPLATEFIELDSMASTER;

    alter table ICCM_EVENT_DATA 
        add constraint FKnatcyj7dhg14p8vuevhfjs473 
        foreign key (ICCM_EVENT_ID) 
        references ICCM_EVENT;

    alter table IMAGE 
        add constraint FKjp6itjg7c759mnke5lxgaw5nx 
        foreign key (documentId) 
        references APPLICANT_DOCUMENT;

    alter table REMARKS 
        add constraint FK1k2dwgxac9g430us1kcj32pji 
        foreign key (arn) 
        references CCAPPLICATION (arn);

    alter table STAFFDETAILS 
        add constraint FKm2mnelqwlhv2a8doqwthp41xc 
        foreign key (ApplicationId) 
        references CCAPPLICATION;

    alter table SUPPAPPLICANTS 
        add constraint FKaraaocwwpae4diukgc2cxoeeq 
        foreign key (applicantId) 
        references APPLICANTDETAILS;

    alter table SUPPAPPLICANTS 
        add constraint FK34qx8nf46xvgeynswbe4c24s3 
        foreign key (applicationId) 
        references CCAPPLICATION;

    alter table TEMPLATEFIELDSMASTER 
        add constraint FKgy5wlpo3y5iy94apo929uplc4 
        foreign key (field_Id) 
        references FIELDMASTER;

    alter table TEMPLATEFIELDSMASTER 
        add constraint FKepc1biwlv4ic3t5d4tn3wn7r2 
        foreign key (template_Id) 
        references TEMPLATEMASTER;
commit;