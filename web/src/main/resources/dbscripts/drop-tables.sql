drop table ADDRESS cascade constraints;
drop table ADDRESSTYPEMASTER cascade constraints;
drop table APPLICANT_ADDRESS cascade constraints;
drop table APPLICANT_DOCUMENT cascade constraints;
drop table APPLICANTADDITIONALINFO cascade constraints;
drop table APPLICANTDETAILS cascade constraints;
drop table APPLICATIONSTATUSTIMELINE cascade constraints;
drop table ARNCCBRIDGE cascade constraints;
drop table CCAPPLICATION cascade constraints;
drop table CCAPPLICATIONADDITIONALINFO cascade constraints;
drop table CHANNELMASTER cascade constraints;
drop table COUNTRYLISTMASTER cascade constraints;
drop table CREDITCARDMASTER cascade constraints;
drop table DOCGROUPMASTER cascade constraints;
drop table DOCUMENT_EXTRACTION cascade constraints;
drop table DOCUMENTSMASTER cascade constraints;
drop table EDUCATIONALLEVELMASTER cascade constraints;
drop table EVENTMASTER cascade constraints;
drop table FIELDMASTER cascade constraints;
drop table HOMEOWNERSHIPMASTER cascade constraints;
drop table ICCM_EVENT cascade constraints;
drop table ICCM_EVENT_DATA cascade constraints;
drop table IMAGE cascade constraints;
drop table IPACCESSDETAILS cascade constraints;
drop table IPBLOCKSTATUS cascade constraints;
drop table IUpload cascade constraints;
drop table JOBTYPEMASTER cascade constraints;
drop table NGSTATUSMASTER cascade constraints;
drop table OTP cascade constraints;
drop table REMARKS cascade constraints;
drop table STAFFDETAILS cascade constraints;
drop table SUPPAPPLICANTS cascade constraints;
drop table TEMPLATEFIELDSMASTER cascade constraints;
drop table TEMPLATEMASTER cascade constraints;

drop table SGHAO_APPL cascade constraints; -- AO Prod table 

commit;